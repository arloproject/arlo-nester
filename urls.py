from django.conf.urls import *
from django.contrib import admin, auth
from django.contrib.auth import views as auth_views
from django.views import static
from django.conf import settings
import rest_framework.authtoken.views as rest_views

from tools import urls as tools_urls
from tools.views import index
from poetrytagging import urls as poetrytagging_urls
from metatagging import urls as metatagging_urls
from api import urls as api_urls

urlpatterns = [
    url(r'^admin/', include(admin.site.urls)),

    url(r'^accounts/login/$', auth_views.login),

    url(r'^tools/', include(tools_urls)),
    url(r'^poetrytagging/', include(poetrytagging_urls)),
    url(r'^metatagging/', include(metatagging_urls)),

    url(r'^$', index.index),

    url(r'^api/', include(api_urls)),
    url(r'^api-token-auth/', rest_views.obtain_auth_token),

    url(r'^changePassword$', auth.views.password_change, name='password_change'),
    url(r'^changePasswordDone$', auth.views.password_change_done, name='password_change_done'),
]

if settings.USE_STATIC_SERVE:
  urlpatterns += [
      url(r'^site_media/(?P<path>.*)$', static.serve, {'document_root': settings.MEDIA_ROOT, 'show_indexes':True}),
      url(r'^media/(?P<path>.*)$', static.serve, {'document_root': settings.MEDIA_ROOT, 'show_indexes':True}),
  ]
