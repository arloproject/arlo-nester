###
# Django Middle-Ware tools Specific to ARLO


###
# Context Processor to add a list of the logged-in user's Projects
# to the Template Context.
#
# Added to TEMPLATE_CONTEXT_PROCESSORS
#
# @param request A Django HttpRequest object
# @return An empty dictionary if the user is not logged in, else a dictionary
#         with one key (ArloUserProjects) containing a list of Project Objects.
#         { 'ArloUserProjects': [project, project,...] }

from django.conf import settings

from .models import Project


def ArloTemplateContextProcessor(request):
    data = {
        'ArloUrlPrefix': settings.URL_PREFIX,
        'ArloAPIv0Url':settings.ARLO_API_V0_URL,
      }
    if request.user.is_authenticated():
        data['ArloUserProjects'] = Project.objects.getUserProjects(request.user)
    return data
