import os
import logging
import csv
import datetime

from django.contrib.auth.decorators import login_required
from django.shortcuts import render
from django import forms
from django.conf import settings

import nesterSettings as nesterSettings
from tools.models import Project, TagSet, Tag
#from .viewMetaData import *


class CreateTranscriptForm(forms.Form):
  exportUserTags = forms.BooleanField(label='Export User Tags', required=False, initial=True)
  exportMachineTags = forms.BooleanField(label='Export Machine Tags', required=False, initial=False)
  tagSets = forms.ModelMultipleChoiceField(TagSet.objects.none(), label='Tag Sets', widget=forms.CheckboxSelectMultiple, required=True)


@login_required
def createTranscriptForTagSet(request, projectId):
  logging.info('Creating transcript (new) for projectId:' + projectId.__str__())
  user = request.user
  project = Project.objects.get(id=projectId)

  status = ''
  filePath = ''

  if request.method == 'POST':
    form = CreateTranscriptForm(request.POST)
    form.fields['tagSets'] = forms.ModelMultipleChoiceField(project.getTagSets(include_hidden=False), required=True)
    if form.is_valid():
      data = form.cleaned_data

      logging.debug("# form: " + form.__str__())
      logging.debug("# form.data: " + form.data.__str__())

      filePath = createCSVForTagSet(data['tagSets'], user, form)
      if filePath is None:
        status = "Unknown Error Occurred... check logs"
        filePath = ''
      else:
        status = "Success"

      logging.debug(status)

    else:
      logging.info("# form not valid")
      logging.debug("# form: " + form.__str__())
      logging.debug("# form.data: " + form.data.__str__())
  else:
    form = CreateTranscriptForm()
    form.fields['tagSets'] = forms.ModelMultipleChoiceField(project.getTagSets(include_hidden=False), required=True)
    #default return when request is GET and form.is_valid() is False

  return render(request, 'tools/createTranscript.html', {
                                'user':user,
                                'project': project,
                                'form': form,
                                'status': status,
                                'filePath': filePath,
                                'apacheRoot':settings.APACHE_ROOT,
                                })


## Generate a CSV file of Tags from a list of TagSets.
#
# @param tagSets A list of TagSet objects.
# @param user User object.
# @param form Populated CreateTranscriptForm from the interface with PitchTrace settings
# @return Path of the destination file, relative to MEDIA_ROOT

def createCSVForTagSet(tagSets, user, formSettings):
  logging.info("Creating CSV (new) File tagsets = " + repr(tagSets))

  ###
  # Open Temp File

  nowDateString = datetime.datetime.now().strftime('%Y%m%d-%H%M%S')
  fileName = nowDateString + '-transcript.csv'
  relFilePath = nesterSettings.getExportFilePath(user, fileName)  # relative to MEDIA_ROOT
  absFilePath = os.path.join(settings.MEDIA_ROOT, relFilePath)

  logging.debug('createCSVForTagSet(): Abs File Path: ' + absFilePath.__str__())
  try:
    FILE = open(absFilePath, 'w')
    w = csv.writer(FILE)
  except:
    logging.error("createCSVForTagSet(): Exception: file could not be opened (%s)", absFilePath)
    return None

  tags = Tag.objects.select_related('mediaFile','tagClass').filter(tagSet__in=tagSets).order_by('mediaFile__file', 'startTime')

  logging.info("Tag count = " + tags.count().__str__())

  ###
  # Get File Start Times

  logging.info("Building mediaFileStartTimes...")

  mediaFileStartTimes = dict()

  for tag in tags:
    mediaFile = tag.mediaFile
    if not (mediaFile.id in mediaFileStartTimes):
      mediaFileStartTimes[mediaFile.id] = mediaFile.realStartTime

  logging.info("Done Building mediaFileStartTimes")


  ###
  # Write Header

  try:
    w.writerow(('user',
               'tagID',
               'fileStartTime',
               'fileEndTime',
               'realStartTime',
               'realEndTime',
               'minFrequency',
               'maxFrequency',
               'tagClass',
               'confidence',
               'machineTagged?',
               'userTagged?',
               'parentTagID',
               'mediaFileID',
               'mediaFileAlias',
               'mediaRelativeFilePath'
               ))
  except:
    logging.warning('Exception: could not write first row')
    return None


  ###
  # Write out Tags

  data = formSettings.cleaned_data
  exportUserTags = data['exportUserTags']
  exportMachineTags = data['exportMachineTags']

  for tag in tags:

    saveTag = exportUserTags and tag.userTagged
    saveTag = saveTag or (exportMachineTags and tag.machineTagged)

    if saveTag:
        try:
          parentTagID = tag.parentTag.id
        except:
          parentTagID = -1

        fileStartTime = mediaFileStartTimes[tag.mediaFile.id]
        if fileStartTime != None:
          realStartTime = fileStartTime + datetime.timedelta(seconds=float(tag.startTime))
          realEndTime   = fileStartTime + datetime.timedelta(seconds=float(tag.endTime))
          realStartTime = realStartTime.strftime('%Y-%m-%d %H:%M:%S')
          realEndTime   = realEndTime.strftime('%Y-%m-%d %H:%M:%S')
        else:
          realStartTime = ''
          realEndTime   = ''

        w.writerow((tag.user.__str__(),
                     tag.id,
                     tag.startTime,
                     tag.endTime,
                     realStartTime,
                     realEndTime,
                     tag.minFrequency,
                     tag.maxFrequency,
                     tag.tagClass.className,
                     tag.strength,
                     tag.machineTagged.__str__(),
                     tag.userTagged.__str__(),
                     parentTagID,
                     tag.mediaFile.id,
                     tag.mediaFile.alias,
                     tag.mediaFile.file.name,
                     ))

  FILE.close()
  return relFilePath
