import datetime
import logging
from itertools import chain

from django.contrib.auth.models import User
from django.contrib.auth.decorators import login_required
from django.db import models
from django.db.models import Count
from django.http import HttpResponseRedirect, HttpResponseServerError, HttpResponseNotAllowed, HttpResponseForbidden, JsonResponse
from django.shortcuts import render, redirect
from django import forms
from django.conf import settings
from django.core.paginator import Paginator, EmptyPage, PageNotAnInteger

from tools.models import Project, ProjectTypes, MediaFile, TagClass, Tag, ProjectPermissions
from tools.views.viewMetaData import getMetaDataEntry

# TODO the circular import issue below may no longer exist
import userPermissions  # can't do a 'from ... import ...' since userPemsissions imports this module



## Get a list of TagClasses present within a TagSet
# @param tagSet A TagSet object
# @return A list of TagClass objects (as a Django QuerySet)

def getTagSetTagClasses(tagSet):
#  tags = Tag.objects.filter(tagSet=tagSet)
#  tagClasses = TagClass.objects.filter(tag__in=tags).distinct()
  tagClasses = TagClass.objects.filter(tag__tagSet=tagSet).distinct()
  logging.info("**************TagClasses Found" + repr(tagClasses))

  return tagClasses


## Create a new Project owned by 'user'
# @param user User object that owns the tag
# @param name The name of the new Project
# @param projectType A ProjectTypes object
# @param notes Notes to store with the object.
# @return The new Project object if successful, None if a conflicting Project name exists.

def createProject(user, name, projectType, notes=None):
  logging.debug("Creating Project for " + user.__str__() + " called " + name.__str__())
  try:
    project = Project.objects.get(user=user,name=name)
    logging.warning("Project already exists")
    return None
  except:
    date = datetime.datetime.now()
    project = Project(user=user,name=name,creationDate=date,type=projectType,notes=notes)
    project.save()
    return project


## Delete Multiple Projects
# Currently only AJAX
# @param request The Django HTTP request object
# @return JSON object

@login_required
def deleteMultipleProjects(request):
  user = request.user
  logging.info("Deleting Multiple Projects: user = " + user.__str__())

  projects = request.GET.get('projects', None).split("//")

  for projectID in projects:
    project = Project.objects.get(id=projectID)
    if (user != project.user):
      return HttpResponseForbidden("Forbidden - Only Project owner may delete.")
    else:
      project.delete()

  data = {
        'projects': projects,
    }

  return JsonResponse(data)

## Web interface call to delete a user's Project.
# Only the Project's owner may delete.
# Note: This may fail if the Project is referenced by other records in the
# database. In the future, we can extend this functionality to be more
# thorough, but this should be a rare operation so we can do it manually.
# @param request The Django HTTP request object.
# @param projectId Database ID of the Project to delete.
# @return Django HTTP response object, returning the 'projects'
# page on success, error page otherwise.

@login_required
def deleteProject(request, projectId):
  user = request.user
  project = Project.objects.get(id=projectId)

  # Check user permissions
  if (user != project.user):
    return HttpResponseForbidden("Forbidden - Only Project owner may delete.")

  logging.info("Deleting Project: user = " + user.__str__() + " projectId = " + projectId.__str__())
  project.delete()
  return projects(request)

## Display all Projects owned or accessible by a user (/tools/projects).
# @param request The Django HTTP request object
# @return Django HTTP response object

@login_required
def projects(request):
  user = request.user
  logging.info("Projects: user = " + user.__str__())

  ###
  # User's Own Projects

  userProjects = Project.objects.filter(user=user).order_by('name')
  userProjectInfos = []

  for project in userProjects:

    sharedUserInfos = []
    sharedUsers = project.getProjectUsers()
    for sharedUser in sharedUsers:
      sharedUserInfos.append({
        'user': sharedUser,
        'canRead': sharedUsers[sharedUser]['read'],
        'canWrite': sharedUsers[sharedUser]['write'],
        'canLaunch': sharedUsers[sharedUser]['launch_job'],
        'canAdmin': sharedUsers[sharedUser]['admin'],
        })

    numUserTags = Tag.objects.filter(tagClass__project=project,userTagged=True).count()
    numMachineTags = Tag.objects.filter(tagClass__project=project,machineTagged=True,userTagged=False).count()
    numTags = Tag.objects.filter(tagClass__project=project).count()

    userProjectInfos.append( {
      'project': project,
      'sharedUserInfos': sharedUserInfos,
      'numUserTags': numUserTags,
      'numMachineTags': numMachineTags,
      'numTags': numTags
      })

  ###
  # Shared Projects

  sharedProjectInfos = []

  for project in Project.objects.getUserProjects(user):
    if project.user != user:
      sharedProjectInfos.append({
        'project': project,
        'canRead': project.userHasPermissions(user, userPermissions.ARLO_PERMS.READ),
        'canWrite': project.userHasPermissions(user, userPermissions.ARLO_PERMS.WRITE),
        })


  # sharing form
  addUserToProjectForm = AddUserToProjectForm()
  addUserToProjectForm.fields['project'] = forms.ModelChoiceField(Project.objects.filter(user=user))

  return render(request, 'tools/projects.html',
                           {'user': user,
                            'userProjectInfos':userProjectInfos,
                            'sharedProjectInfos': sharedProjectInfos,
                            'apacheRoot':settings.APACHE_ROOT,
                            'addUserToProjectForm': addUserToProjectForm,
                            'section': 'projects',
                            })


## Django form for creating a new Project

class StartNewProjectForm(forms.Form):
  name = forms.CharField(max_length=255)
  projectType = forms.ModelChoiceField(ProjectTypes.objects.none(),label="Type")
  notes = forms.CharField(widget=forms.Textarea, required=False)


## Start a new Project.
# GET request returns the create Form, POST processes it.
# @param request The Django HTTP request object
# @return Django HTTP response object

@login_required
def startNewProject(request):
  user = request.user
  logging.info("Starting New Project: user = " + user.__str__())

  if request.method == 'POST': # If the form has been submitted...

    form = StartNewProjectForm(request.POST) # A form bound to the POST data
    form.fields['projectType'] = forms.ModelChoiceField(ProjectTypes.objects, label="Type")

    if form.is_valid(): # All validation rules pass
      name = form.cleaned_data['name']
      projectType = form.cleaned_data['projectType']
      notes = form.cleaned_data['notes']

      createProject(user, name, projectType, notes)

      return HttpResponseRedirect('projects')
    else:
      return render(request, 'tools/startNewProject.html', {
                            'form': form,
                            'apacheRoot':settings.APACHE_ROOT,
                            'user':user,
                            })

  else:
    form = StartNewProjectForm()
    form.fields['projectType'] = forms.ModelChoiceField(ProjectTypes.objects, label="Type")

    return render(request, 'tools/startNewProject.html', {
                            'form': form,
                            'apacheRoot':settings.APACHE_ROOT,
                            'user':user,
                            })


class ProjectMediaFileForm(forms.Form):
  project = forms.ModelChoiceField(Project.objects.none(), empty_label=None)
  #mediaFiles = forms.ModelMultipleChoiceField(MediaFile.objects.none())


## Add Audio Files to a Project
# GET request returns the form, POST processes it.
# @param request The Django HTTP request object
# @return Django HTTP response object

@login_required
def addAudioToProject(request, audioFileID):
  user = request.user
  logging.info("Adding Audio to Project: user = " + user.__str__())

  if request.method == 'POST': # If the form has been submitted...

    form = ProjectMediaFileForm(request.POST) # A form bound to the POST data
    form.fields['project'] = forms.ModelChoiceField(Project.objects.filter(user=user))
    form.fields['mediaFiles'] = forms.ModelMultipleChoiceField(MediaFile.objects.filter(user=user))

    if form.is_valid(): # All validation rules pass

      project = form.cleaned_data['project']
      mediaFiles = form.cleaned_data['mediaFiles']

      for mediaFile in mediaFiles:
        project.mediaFiles.add(mediaFile)
      project.save()

      return HttpResponseRedirect('/tools/projectFiles/' + str(project.id))

    else:
      return render(request, 'tools/addAudioToProject.html', {
                            'user':user,
                            'form': form,
                            'apacheRoot':settings.APACHE_ROOT,
                            })
  else:
    form = ProjectMediaFileForm()

    form.fields['project'] = forms.ModelChoiceField(Project.objects.filter(user=user))
    #form.fields['mediaFiles'] = forms.ModelMultipleChoiceField(MediaFile.objects.filter(user=user).order_by('alias'))

    return render(request, 'tools/addAudioToProject.html', {
                            'user':user,
                            'form': form,
                            'apacheRoot':settings.APACHE_ROOT,
                            'audioFileID': audioFileID,
                            'audioFile': MediaFile.objects.get(id=audioFileID)
                            })


## Add Multiple Audio Files to a Project
# Currently only AJAX
# @param request The Django HTTP request object
# @param fileIDs
# @return JSON object ... currently nothing useful here ... should probably verify

@login_required
def addMultipleAudioToProject(request):
  user = request.user
  logging.info("Adding Multiple Audio to Project: user = " + user.__str__())

  mediaFiles = request.GET.get('mediaFiles', None).split("//")
  projectID = request.GET.get('projectID', None)
  project = Project.objects.get(id=projectID)

  for mediaFileID in mediaFiles:
    mediaFile = MediaFile.objects.get(id=mediaFileID)
    project.mediaFiles.add(mediaFile)

  data = {
        'mediaFiles': mediaFiles,
        'projectID': projectID,
        'projectName': project.name,
    }

  return JsonResponse(data)


  ## Remove Audio File from Project
  # This is for AJAX requests -- Daniel had trouble splitting the response on the other view
  # @param request The Django HTTP request object
  # @param fileID
  # @param fileID
  # @return JSON Object with project and media file IDs

@login_required
def AJAXRemoveAudioFromProject(request):

  user = request.user
  logging.info("Removing Audio from Project: user = " + user.__str__())

  mediaFileID = request.GET.get('mediaFileID', None)
  projectID = request.GET.get('projectID', None)
  project = Project.objects.get(id=projectID)

  mediaFile = MediaFile.objects.get(id=mediaFileID)
  project.mediaFiles.remove(mediaFile)

  data = {
    'mediaFileID': mediaFileID,
    'projectID': projectID,
  }

  return JsonResponse(data)


## Remove an Audio File from a Project.
# @note Does not delete the mediaFile, just removes it from the Project
# @param request The Django HTTP request object
# @param projectID Database ID of the Project
# @param audioFileID Database ID of the mediaFile to remove
# @return Django HTTP response object

@login_required
def removeAudioFromProject(request,projectID,audioFileID):
  user = request.user
  logging.info("Removing Media Id " + str(audioFileID) + " from Project " + str(projectID) + " user = " + user.__str__())
  project = Project.objects.get(id=projectID)
  mediaFile = MediaFile.objects.get(id=audioFileID)

  if not project.userHasPermissions(user, userPermissions.ARLO_PERMS.READ):
    return HttpResponseForbidden("Forbidden")

  project.mediaFiles.remove(mediaFile)
  project.save()

  return projectFiles(request, project.id)


## Django Form for Sharing a Project with another user.
#

class AddUserToProjectForm(forms.Form):
  username = forms.CharField(max_length=255)
  project  = forms.ModelChoiceField(Project.objects.none(), empty_label=None, required=True)
  canRead  = forms.BooleanField(label='Read', initial=True, required=False)
  canWrite = forms.BooleanField(label='Write', initial=False, required=False)

## Add a user with read and/or write access to a Project. Expects a POST which should
# contain an AddUserToProjectForm. Only the Project owner may add users with this.
# @param request The Django HTTP request object
# @return Django HTTP response object (redirect to '/tools/projects' if successful)

@login_required
def addUserToProject(request):
  user = request.user
  logging.info("addUserToProject: user = " + user.__str__())

  if request.method != 'POST':
    return HttpResponseNotAllowed(['POST'])

  form = AddUserToProjectForm(request.POST)
  form.fields['project'] = forms.ModelChoiceField(Project.objects.filter(user=user))

  if (not (form.is_valid())):
    return HttpResponseServerError("Invalid Form ( " + str(form.errors) + " )")

  try:
    newUser = User.objects.get(username__exact = form.cleaned_data['username'])
  except:
    return HttpResponseServerError("Invalid User")

  project = form.cleaned_data['project']

  if (project.user != user):
    return HttpResponseForbidden("Forbidden")

  canRead = form.cleaned_data['canRead']
  canWrite = form.cleaned_data['canWrite']

  newPerm = ProjectPermissions(user=newUser, project=project, canRead=canRead, canWrite=canWrite)
  newPerm.save()

  return HttpResponseRedirect(settings.URL_PREFIX + "/tools/projects")


## Django Class to remove (un-share) a user from a Project.
#

class RemoveUserFromProjectForm(forms.Form):
  user_id    = forms.IntegerField()
  project_id = forms.IntegerField()

## Remove a User from a Project. This deletes the corresponding ProjectPermissions entry.
# @param request The Django HTTP request object
# @return Django HTTP response object (redirect to '/tools/projects' if successful)

@login_required
def removeUserFromProject(request):
  user = request.user
  logging.info("removeUserFromProject: user = " + user.__str__())

  if request.method != 'POST':
    return HttpResponseNotAllowed(['POST'])

  form = RemoveUserFromProjectForm(request.POST)
  if (not (form.is_valid())):
    return HttpResponseServerError("Invalid Form ( " + str(form.errors) + " )")

  try:
    project = Project.objects.get(id=form.cleaned_data['project_id'])
  except:
    return HttpResponseServerError("Invalid Project Specified")

  if (project.user != user):
    return HttpResponseForbidden("Forbidden")

  try:
    delUser = User.objects.get(id=form.cleaned_data['user_id'])
  except:
    return HttpResponseForbidden("Invalid User Specified")

  ProjectPermissions.objects.filter(user=delUser, project=project).delete()

  return HttpResponseRedirect(settings.URL_PREFIX + "/tools/projects")


## Displays the 'Project Files' Page.
# @param request The Django HTTP request object
# @param projectId The database ID of the Project to display
# @return Django HTTPResponse object

@login_required
def projectFiles(request, projectId):
  user=request.user
  logging.info("Retrieving Project Files: user = " + user.__str__() + " projectId = " + projectId.__str__())

  project = Project.objects.get(id=projectId)

  ## If removing files before processing page
  # TODO Validate write permissions on project before removing files
  if request.GET.get('removeMediaFiles', None):
    mediaFilesToRemove = request.GET.get('removeMediaFiles', None).split("//")
    for mediaFile in mediaFilesToRemove:
      project.mediaFiles.remove(mediaFile)
    project.save()

  ## If deleting files before processing page
  # TODO Validate permissions on each MediaFile before deleting
  if request.GET.get('deleteMediaFiles', None):
    mediaFilesToDelete = request.GET.get('deleteMediaFiles', None).split("//")
    for mediaFile in mediaFilesToDelete:
      MediaFile.objects.get(id=mediaFile).delete()


  tagSets = project.getTagSets(include_hidden=False)

  tagSetsList = []
  for tagSet in tagSets:
    numTags = Tag.objects.filter(tagSet=tagSet).count()

    tagSetsList.append( {
    'tagSet': tagSet,
    'numTags': numTags,
    })

  if not project.userHasPermissions(user, userPermissions.ARLO_PERMS.READ):
    return HttpResponseForbidden("Forbidden")

  mediaFiles = project.mediaFiles.all()

  ## Sort -- still working on how this should work across views.

  sort = request.GET.get('sort', None)
  if sort is None:
    sort = 'uploadDate'

  if sort == 'uploadDate':
    mediaFilesAll = mediaFiles.order_by(sort).reverse()
  elif sort == 'tagCount':
    mediaFilesAll = mediaFiles.annotate(tagCount = Count('tag'))
    mediaFilesAll = mediaFilesAll.order_by('-tagCount')
  elif sort == 'userTagCount':

    ## BLURGH... This is not optimal, but it works. The problem is that the annnotate function
    ## in the first query doesn't return objects with a zero value, so I just get everything that
    ## isn't returned in the first query and append it.

    mediaFiles_tagged = mediaFiles.annotate(
        userTagCount=models.Sum(
            models.Case(
                models.When(
                    tag__userTagged=True,
                    then=1,
                ), output_field=models.IntegerField()
            )
        )
    ).order_by('-userTagCount')

    mediaFiles_not_tagged = mediaFiles.exclude(id__in=[elem.id for elem in mediaFiles_tagged])

    mediaFilesAll = list(chain(mediaFiles_tagged, mediaFiles_not_tagged))

  elif sort == 'machineTagCount':

    mediaFiles_tagged = mediaFiles.annotate(
        userTagCount=models.Sum(
            models.Case(
                models.When(
                    tag__machineTagged=True,
                    then=1,
                ), output_field=models.IntegerField()
            )
        )
    ).order_by('-userTagCount')

    mediaFiles_not_tagged = mediaFiles.exclude(id__in=[elem.id for elem in mediaFiles_tagged])

    mediaFilesAll = list(chain(mediaFiles_tagged, mediaFiles_not_tagged))

  else:
    mediaFilesAll = mediaFiles.order_by(sort)


  totalFiles = len(mediaFilesAll)

  paginator = Paginator(mediaFilesAll, settings.ARLO_UI_PAGE_SIZE)

  page = request.GET.get('page')
  try:
      mediaFiles = paginator.page(page)
  except PageNotAnInteger:
      # If page is not an integer, deliver first page.
      mediaFiles = paginator.page(1)
  except EmptyPage:
      # If page is out of range (e.g. 9999), deliver last page of results.
      mediaFiles = paginator.page(paginator.num_pages)

  index  = mediaFiles.number #current page
  max_index = paginator.num_pages
  start_index = max(index - settings.ARLO_UI_PAGINATION_RANGE - 1, 0)
  end_index = min(index + settings.ARLO_UI_PAGINATION_RANGE, max_index)
  pageRange = paginator.page_range[start_index:end_index]

  MediaFileList = []
  for mediaFile in mediaFiles:

    numUserTags = Tag.objects.filter(mediaFile=mediaFile.id, tagClass__project=project,userTagged=True).count()
    numMachineTags = Tag.objects.filter(mediaFile=mediaFile.id,tagClass__project=project,machineTagged=True,userTagged=False).count()
    numTags = Tag.objects.filter(mediaFile=mediaFile.id,tagClass__project=project).count()

    MediaFileList.append( {
    'id': mediaFile.id,
    'library': mediaFile.library,
    'library_id': mediaFile.library_id,
    'active': mediaFile.active,
    'startTime': getMetaDataEntry(mediaFile, 'startTime'),
    'alias': mediaFile.alias,
    'fileURL': mediaFile.file.url,
    'sensor': mediaFile.sensor,
    'duration': round(float(getMetaDataEntry(mediaFile, 'durationInSeconds'))),
    'numUserTags': numUserTags,
    'numMachineTags': numMachineTags,
    'numTags': numTags
    })

  ## Display Options

  MediaFileListColumns = {
    'name': True,
    'duration': True,
    'library':True,
    'numUserTags': True,
    'numMachineTags': True,
    }

  listSortOptions = {
        'uploadDate': True,
        'alias': True,
        'duration': True,
        'userTagCount': True,
        'machineTagCount': True,
    }

  searchPrompt = 'Search files in project \'' + project.name + '\'...'
  allFilesLabel = 'project \'' + project.name + '\''

  return render(request, 'tools/projectFiles.html', {
    'project':project,
    'currentProjectId': project.id,
    'apacheRoot':settings.APACHE_ROOT,
    'user': user,
    'MediaFileList': MediaFileList,
    'MediaFileListColumns': MediaFileListColumns,
    'MediaFilesPaginator': mediaFiles,
    'fileListHeader': 'Project Audio Files',
    'tagSets': tagSetsList,
    'section': 'projects',
    'searchPrompt': searchPrompt,
    'allFilesLabel': allFilesLabel,
    'totalFiles': totalFiles,
    'listSort': sort,
    'listSortOptions': listSortOptions,
    'pageRange': pageRange,
    'lastPage': max_index,
     })



class EditProjectNotesForm(forms.Form):
  notes = forms.CharField(widget=forms.Textarea, required=False)


## Edit a Project's Notes.
# GET request returns the create Form, POST processes it.
# @param request The Django HTTP request object
# @param projectId Database ID of the Project to delete.
# @return Django HTTP response object

@login_required
def projectNotes(request, projectId):
  user = request.user
  logging.info("Project Notes: user = " + user.__str__() + " projectId: " + projectId.__str__())

  project = Project.objects.get(id=projectId)

  if not project.userHasPermissions(user, userPermissions.ARLO_PERMS.READ):
    return HttpResponseForbidden("Forbidden")

  if request.method == 'POST': # If the form has been submitted...

    if not project.userHasPermissions(user, userPermissions.ARLO_PERMS.WRITE):
      return HttpResponseForbidden("Forbidden")

    form = EditProjectNotesForm(request.POST)

    if form.is_valid(): # All validation rules pass
      notes = form.cleaned_data['notes']
      project.notes = notes
      project.save()

      return redirect('projectFiles', projectId=project.id)
    else:
      return render(request, 'tools/projectNotes.html', {
                            'form': form,
                            'project': project,
                            })

  else:
    form = EditProjectNotesForm(initial={'notes': project.notes})

    return render(request, 'tools/projectNotes.html', {
                            'form': form,
                            'project': project,
                            })
