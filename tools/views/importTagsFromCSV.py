from django.contrib.auth.decorators import login_required
from django.shortcuts import render
from django.http import HttpResponseForbidden
from django import forms

from tools.models import Project, TagSet, TagClass
from .userPermissions import ARLO_PERMS


class ImportTagForm(forms.Form):
  tagSet   = forms.ModelChoiceField(TagSet.objects.none())
  tagClass = forms.ModelChoiceField(TagClass.objects.none())


##
#
# The bulk of this work takes place in the JavaScript. Just display the tool here.
# @param request The Django HTTP request object
# @param projectId The database ID of the Project to which we are importing.
# @return Django HTTP response object

@login_required
def importTagsFromCSV(request, projectId):
  user = request.user
  project = Project.objects.get(id=projectId)

  # Check user permissions
  if not project.userHasPermissions(user, ARLO_PERMS.WRITE):
    return HttpResponseForbidden("Forbidden - You do not have write access to this Project.")

  form = ImportTagForm()
  form.fields['tagSet']   = forms.ModelChoiceField(project.getTagSets(include_hidden=False))
  form.fields['tagClass'] = forms.ModelChoiceField(project.getTagClasses())

  return render(request, 'tools/importTagsFromCSV.html',
                           {'user': user,
                            'project': project,
                            'form': form,
                            })
