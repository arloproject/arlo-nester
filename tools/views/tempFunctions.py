from django.contrib.auth.decorators import login_required
from django.http import JsonResponse

from tools.models import TagSet, Tag


## 'Expert' Tag Comparison.
# For every Tag in the compareTagSet, find all other user's Tags of the 
# same time, and generate a list of what it was tagged as. 
# @param request The Django HTTP request object
# @param audioID The Database ID of TagSet of Tags to compare against. 
# @return A JSON dictionary with links to the new spectra and various parameters to update on the web form.

# {
#   'compareTagSetId': 1,
#   'userTags': 
#     [
#       {
#         'compareTagId': 1,
#         'tags':
#           [
#             {
#               'tagId': 1,
#               'tagClassId': 1,
#               'tagClassName': "name",
#               'userId': 1,
#               'userName': "name",
#             },
#           ]
#       },
#     ]
# }
#

@login_required
def compareExpertTagging(request, compareTagSetId):
  
  # Get the TagSet
  compareTagSet = TagSet.objects.get(id=compareTagSetId)

  # Get the list of comparison Tags
  compareTags = Tag.objects.filter(tagSet=compareTagSet)

  userTags = []
  # For each comparison Tag, find Tags of the same time
  for cTag in compareTags:

    # find tags of the same time
    startTime = cTag.startTime
    endTime = cTag.endTime
    compareTagLength = endTime - startTime

    # look within a small range a bit
    startTimeLower = startTime - (compareTagLength * 0.10)
    startTimeUpper = startTime + (compareTagLength * 0.10)
    endTimeLower = endTime - (compareTagLength * 0.10)
    endTimeUpper = endTime + (compareTagLength * 0.10)

    tags = Tag.objects.filter(
      startTime__gt=startTimeLower,
      startTime__lt=startTimeUpper,
      endTime__gt=endTimeLower,
      endTime__lt=endTimeUpper,
      mediaFile=cTag.mediaFile)

    tagsList = []
    for tag in tags:
      # skip Tags from the comparison TagSet
      if (tag.tagSet != compareTagSet):
        tagsList.append({
            'tagId': tag.id,
            'tagClassId': tag.tagClass.id,
            'tagClassName': tag.tagClass.className,
            'userId': tag.user.id,
            'userName': tag.user.username
          })

    userTags.append({
      'compareTagId': cTag.id,
      'tags': tagsList
      })

  response_dict = {
    'compareTagSetId': compareTagSetId,
    'userTags': userTags,
    }
  return JsonResponse(response_dict)



