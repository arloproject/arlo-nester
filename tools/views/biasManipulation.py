import logging

from django import forms
from django.contrib.auth.decorators import login_required
from django.shortcuts import render
from django.http import HttpResponseRedirect, HttpResponseForbidden, HttpResponseNotAllowed, HttpResponseServerError, JsonResponse
from django.conf import settings
from django.views.decorators.http import require_http_methods

from tools.models import JobTypes, Jobs, JobLog, JobStatusTypes, JobParameters, Project
from .userPermissions import ARLO_PERMS

## Add parameters to a Job.
#
# @note This will ADD JobParameters, not overwrite or delete existing ones.
# @param job Job object to update
# @param params A dictionary of name: value-list pairs of parameters (e.g., {'param1':['value1a', 'value2a']} )
# @return True on success, False otherwise

def AddJobParameters(job, params):
  try:
    for name in params:
      for value in params[name]:
        logging.info("saving JobParameter : " + name.__str__() + " value = " + value.__str__())
        param = JobParameters(job=job, name=name, value=value.__str__())
        param.save()
    return True
  except:
    return False

## Get parameters of a Job.
#
# @param job Job object to update
# @return None if error, name: value-list dictionary otherwise (e.g., {'param1':['value1a','value2a']} )

def GetJobParameters(job):
  parametersDict = {}
  jobParameters = JobParameters.objects.filter(job=job)
  for parameter in jobParameters:
    name  = parameter.name
    value = parameter.value
    if not (name in parametersDict):
      parametersDict[name] = []
    parametersDict[name].append(value)
  return parametersDict


class SetJobStatusForm(forms.Form):
  jobStatus = forms.ModelChoiceField(JobStatusTypes.objects.all(), required=True)

## Set a Job's status from the Web UI.
# This form is shown by other functions, so only a POST is valid here.
# @note Currently only allow for Admin (staff) users.
# @param request The Django HTTP request object
# @param jobId The database id of the Job to update

@login_required
def setJobStatus(request, jobId):
  user = request.user
  logging.info("setJobStatus: user = " + user.__str__() + " jobId: " + jobId.__str__())

  if not user.is_staff:  # if staff, allow any update
    return HttpResponseForbidden("Forbidden - Admin only Function")

  if request.method != 'POST':
    return HttpResponseNotAllowed(['POST'])

  form = SetJobStatusForm(request.POST) # A form bound to the POST data
  if not form.is_valid():
    return HttpResponseServerError("Form Invalid")

  status = form.cleaned_data['jobStatus']
  job = Jobs.objects.get(id=jobId)

  logging.info("Updating jobId: " + jobId.__str__() + " to status: " + status.name)

  job.status = status
  job.save()

  jobLog = JobLog(job=job, message="Status Changed to '" + status.name + "' by user '" + user.username + "'")
  jobLog.save()

  return HttpResponseRedirect(settings.URL_PREFIX + "/tools/viewJob/" + jobId.__str__())

class SetJobPriorityForm(forms.Form):
  jobPriority = forms.IntegerField(required=True)


## Set a Job's, and all of it's child tasks', priority from the Web UI.
# This form is shown by other functions, so only a POST is valid here.
# @note Currently only allow for Admin (staff) users.
# @param request The Django HTTP request object
# @param jobId The database id of the Job to update
# @return Django HTTP response object

@login_required
def setJobPriority(request, jobId):
  user = request.user
  logging.info("setJobPriority: user = " + user.__str__() + " jobId: " + jobId.__str__())

  if not user.is_staff:  # if staff, allow any update
    return HttpResponseForbidden("Forbidden - Admin only Function")

  if request.method != 'POST':
    return HttpResponseNotAllowed(['POST'])

  form = SetJobPriorityForm(request.POST) # A form bound to the POST data
  if not form.is_valid():
    return HttpResponseServerError("Form Invalid")

  priority = form.cleaned_data['jobPriority']

  logging.info("Updating jobId: " + jobId.__str__() + " to priority: " + priority.__str__())

  # update parent job
  if Jobs.objects.filter(id=jobId).update(priority=priority) != 1:
    logging.error("setJobPriority did not update exactly one parent")

  # update child jobs
  numJobs = Jobs.objects.filter(parentJob=jobId).update(priority=priority)
  logging.info("setJobPriority: updated " + numJobs.__str__() + " child job")

  job = Jobs.objects.get(id=jobId)
  jobLog = JobLog(job=job, message="Priority Changed to '" + str(priority) + "' by user '" + user.username + "'")
  jobLog.save()

  return HttpResponseRedirect(settings.URL_PREFIX + "/tools/viewJob/" + jobId.__str__())

## Set all of a parent Job's queued children to Stopped.
# For all children, if status = 'Queued', update status = 'Stopped'.
# @param request The Django HTTP request object
# @param jobId The database id of the parent Job to update
# @return Django HTTP response object

@login_required
def stopQueuedChildTasks(request, parentJobId):
  user = request.user
  logging.info("stopQueuedChildTasks: user = " + user.__str__() + " parentJobId: " + parentJobId.__str__())

  parentJob = Jobs.objects.get(id=parentJobId)

  if not parentJob.userHasPermissions(user, ARLO_PERMS.WRITE):
    return HttpResponseForbidden("Forbidden")

  if request.method != 'POST':
    return HttpResponseNotAllowed(['POST'])

  stoppedStatus = JobStatusTypes.objects.get(name="Stopped")
  Jobs.objects.filter(parentJob=parentJob, status__name="Queued").update(status=stoppedStatus)

  jobLog = JobLog(job=parentJob, message="'Stop Queued Child Tasks' by user '" + user.username + "'")
  jobLog.save()

  return HttpResponseRedirect(settings.URL_PREFIX + "/tools/viewJob/" + parentJobId.__str__())

## Set all of a parent Job's stopped children to Queued.
# For all children, if status = 'Stopped', update status = 'Queued'.
# @param request The Django HTTP request object
# @param jobId The database id of the parent Job to update
# @return Django HTTP response object

@login_required
def queueStoppedChildTasks(request, parentJobId):
  user = request.user
  logging.info("queueStoppedChildTasks: user = " + user.__str__() + " parentJobId: " + parentJobId.__str__())

  parentJob = Jobs.objects.get(id=parentJobId)

  if not parentJob.userHasPermissions(user, ARLO_PERMS.WRITE):
    return HttpResponseForbidden("Forbidden")

  if request.method != 'POST':
    return HttpResponseNotAllowed(['POST'])

  queuedStatus = JobStatusTypes.objects.get(name="Queued")
  Jobs.objects.filter(parentJob=parentJob, status__name="Stopped").update(status=queuedStatus)

  jobLog = JobLog(job=parentJob, message="'Queue Stopped Child Tasks' by user '" + user.username + "'")
  jobLog.save()

  return HttpResponseRedirect(settings.URL_PREFIX + "/tools/viewJob/" + parentJobId.__str__())

## Set a Job to Queued, only if it is currently Stopped.
# @param request The Django HTTP request object
# @param jobId The database id of the Job to update
# @return Django HTTP response object

@login_required
def queueStoppedTask(request, jobId):
  user = request.user
  logging.info("queueStoppedTask: user = " + user.__str__() + " jobId: " + jobId.__str__())

  job = Jobs.objects.get(id=jobId)

  if not job.userHasPermissions(user, ARLO_PERMS.WRITE):
    return HttpResponseForbidden("Forbidden")

  if request.method != 'POST':
    return HttpResponseNotAllowed(['POST'])

  if job.status.name != "Stopped":
    return HttpResponseForbidden("Job Status != Stopped")

  queuedStatus = JobStatusTypes.objects.get(name="Queued")
  # QueueRunner status updates should be atomic...
  updatedRows = Jobs.objects.filter(id=job.id, status__name="Stopped").update(status=queuedStatus)

  if updatedRows != 1:
    logging.error("Failed updating job status: Rows= " + updatedRows.__str__())
    return HttpResponseServerError("Failed updating job status: Rows= " + updatedRows.__str__())

  jobLog = JobLog(job=job, message="Status Changed to 'Queued' by user '" + user.username + "'")
  jobLog.save()

  return HttpResponseRedirect(settings.URL_PREFIX + "/tools/viewJob/" + jobId.__str__())

## Set a Job to Stopped, only if it is currently Queued.
# @param request The Django HTTP request object
# @param jobId The database id of the Job to update
# @return Django HTTP response object

@login_required
def stopQueuedTask(request, jobId):
  user = request.user
  logging.info("stopQueuedTask: user = " + user.__str__() + " jobId: " + jobId.__str__())

  job = Jobs.objects.get(id=jobId)

  if not job.userHasPermissions(user, ARLO_PERMS.WRITE):
    return HttpResponseForbidden("Forbidden")

  if request.method != 'POST':
    return HttpResponseNotAllowed(['POST'])

  if job.status.name != "Queued":
    return HttpResponseForbidden("Job Status != Queued")

  stoppedStatus = JobStatusTypes.objects.get(name="Stopped")
  # QueueRunner status updates should be atomic...
  updatedRows = Jobs.objects.filter(id=job.id, status__name="Queued").update(status=stoppedStatus)

  if updatedRows != 1:
    logging.error("Failed updating job status: Rows= " + updatedRows.__str__())
    return HttpResponseServerError("Failed updating job status: Rows= " + updatedRows.__str__())

  jobLog = JobLog(job=job, message="Status Changed to 'Stopped' by user '" + user.username + "'")
  jobLog.save()

  return HttpResponseRedirect(settings.URL_PREFIX + "/tools/viewJob/" + jobId.__str__())



class SearchQueueTasksForm(forms.Form):
  hideCompletedTasks = forms.BooleanField(label='Hide Completed Tasks', required=False, initial=True)

## Search through the list of QueueTask Jobs, filtering based on the setting in
# the request's SearchQueueTasksForm.
# @NOTE Does not return child jobs, as these are assumed to fall under parents which are returned.
# @param request The Django HTTP request. Note that this expects a GET with a SearchQueueTasksForm
# @param projectId The database Id of a Project to search.
# @return JSON encoded dictionary {totalTasks, tasksReturned, tasksList}.
# tasksList is a list of dictionary {taskId, name, typeName, creationDate, status, statusName, projectId, projectName, numChildTasks, childTaskStatuses}
# childTaskStatuses is a list of dictionary {statusName, numTasks}

@login_required
def searchQueueTasks(request, projectId):
  user = request.user

  logging.info("Searching Queue Tasks: user = " + user.__str__() + " projectId: " + str(projectId))

  if request.method != 'GET':
    return HttpResponseNotAllowed(['GET'])

  project = Project.objects.get(id=projectId)

  # check user permissions
  if not project.userHasPermissions(user, ARLO_PERMS.READ):
    return HttpResponseForbidden("Forbidden")

  queueTaskForm = SearchQueueTasksForm(request.GET)
  if not queueTaskForm.is_valid():
    return HttpResponseServerError("Form Invalid")

  data = queueTaskForm.cleaned_data
  hideCompletedJobs = data['hideCompletedTasks']
  # NULL status means it's not a QueueTask
  # only return parent tasks in this query
  tasks = Jobs.objects.filter(project=project).exclude(status__isnull=True).filter(parentJob__isnull=True).order_by('-creationDate')

  statusCompleteId = JobStatusTypes.objects.get(name="Complete").id

  tasksList = []
  for qt in tasks:
    taskIsComplete = (qt.status.id == statusCompleteId) # track if is complete and can be hidden
    childTasks = Jobs.objects.filter(parentJob=qt)
    numChildTasks = childTasks.count()
    childTaskStatuses = []
    if numChildTasks:
      for jobStatus in JobStatusTypes.objects.all():
        numStatuses = childTasks.filter(status=jobStatus).count()
        if numStatuses:
          childTaskStatuses.append({'statusName':jobStatus.name, 'numTasks':numStatuses})
          if (jobStatus.id != statusCompleteId):
            taskIsComplete = False

    sProjectId = ""
    sProjectName = ""
    if qt.project:
      sProjectId = qt.project.id
      sProjectName = qt.project.name

    tasksDict = {
      'taskId': qt.id,
      'name': qt.name,
      'typeName': qt.type.name,
      'creationDate': qt.creationDate.strftime('%Y-%m-%d %H:%M:%S'),
      'priority': qt.priority,
      'status': qt.status.id,
      'statusName': qt.status.name,
      'projectId': sProjectId,
      'projectName': sProjectName,
      'numChildTasks':numChildTasks,
      'childTaskStatuses':childTaskStatuses
      }

    # if complete and hide completed tasks, don't add
    if not (taskIsComplete and hideCompletedJobs):
      tasksList.append(tasksDict)

  response_dict = {
    'totalTasks': len(tasksList),
    'tasksReturned': len(tasksList),
    'tasksList' : tasksList }

  return JsonResponse(response_dict)

@login_required
def manageJobs(request, projectId):
  user = request.user
  logging.info("Retrieving Jobs: user = " + user.__str__() + " projectId = " + projectId.__str__())

  project = Project.objects.get(id=projectId)

  # check user permissions
  if not project.userHasPermissions(user, ARLO_PERMS.READ):
    return HttpResponseForbidden("Forbidden")

  supervisedTagDiscoveryJobType = JobTypes.objects.get(name='SupervisedTagDiscovery')

  jobs = Jobs.objects.filter(user=user, project=project, wasDeleted=False, type=supervisedTagDiscoveryJobType).order_by('-wasDeleted', '-creationDate')

  # def dateSort(b1, b2):
  #   return cmp(b1[0].creationDate, b2[0].creationDate)
  #
  # historyList.sort(cmp=dateSort)

  return render(request, "tools/manageJobs.html", {
                'apacheRoot':settings.APACHE_ROOT,
                'user':user,
                'project':project,
                'jobs':jobs,
                'queueTasksSearchForm':SearchQueueTasksForm(),
                })


class RandomWindowTaggingForm(forms.Form):
  name = forms.CharField(label='Name', max_length=255, initial='default')
  numWindowsToTag = forms.IntegerField(label='Num Windows To Tag', initial=100)
  windowSizeInSeconds = forms.FloatField(label='Window Size in Seconds', initial=4,required=True)
  weight = forms.ChoiceField(choices=(("Time", "Time"), ("File", "File")), required=True)


@login_required
@require_http_methods(["GET"])
def randomWindowTagging(request, projectId):
  user = request.user
  project = Project.objects.get(id=projectId)

  # Perms Checking
  if not project.userHasPermissions(user, ARLO_PERMS.READ):
    return HttpResponseForbidden("Forbidden")

  return render(request, "tools/randomWindowTagging.html", {
    'project':project,
    'user':user,
    'apacheRoot':settings.APACHE_ROOT,
    })


## Get a list of Random Window Tasks in a Project.
# @param request The Django HTTP request. Note that this expects a GET method.
# @param projectId The database Id of a Project to search.
# @return JSON encoded list of dictionary [ {id, name, creationDate, numWindows, windowLength, weight } ].

@login_required
def getRandomWindowTasks(request, projectId):
  user = request.user
  logging.info("getRandomWindowTasks username = " + user.__str__() + " projectId = " + projectId.__str__())

  if request.method != 'GET':
    return HttpResponseNotAllowed(['GET'])

  user = request.user
  project = Project.objects.get(id=projectId)

  # check user permissions
  if not project.userHasPermissions(user, ARLO_PERMS.READ):
    return HttpResponseForbidden("Forbidden")

  taskList = []

  # Get old-style RandomWindowTagging job
  tasks = Jobs.objects.filter(project=project, type__name="RandomWindowTagging").order_by('-creationDate')
  for task in tasks:
    params = GetJobParameters(task)
    numWindows = params.get('numWindowsToTag', [''])[0]
    windowLength = params.get('windowSizeInSeconds', [''])[0]
    weight = params.get('weight', [''])[0]
    taskList.append({
      'id': task.id,
      'name': task.name,
      'creationDate': task.creationDate.strftime('%Y-%m-%d %H:%M:%S'),
      'numWindows': numWindows,
      'windowLength': windowLength,
      'weight': weight
    })

  # Get new Plugin based job
  tasks = Jobs.objects.filter(project=project, type__name="JobPlugin").order_by('-creationDate')
  for task in tasks:
    params = GetJobParameters(task)
    if params.get('PluginName', [''])[0] == 'RandomWindowAndSegmentingPlugin':
      if params.get('mode', [''])[0] == 'RandomWindow':
        numWindows = params.get('count', [''])[0]
        windowLength = params.get('sizeInSeconds', [''])[0]
        weight = params.get('weight', [''])[0]
        taskList.append({
          'id': task.id,
          'name': task.name,
          'creationDate': task.creationDate.strftime('%Y-%m-%d %H:%M:%S'),
          'numWindows': numWindows,
          'windowLength': windowLength,
          'weight': weight
        })

  return JsonResponse(taskList, safe=False)
