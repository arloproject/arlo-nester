import logging
import datetime
import os
import csv

from django.contrib.auth.decorators import login_required
from django.http import HttpResponseForbidden
from django.shortcuts import render
from django import forms
from django.conf import settings

import nesterSettings as nesterSettings
from tools.models import Library, MediaFileUserMetaDataField
from .userMetaData import getLibraryUserMetaDataFields, getMediaFileUserMetaDataDictionary
from .viewMetaData import getMediaFileMetaDataDictionary
from .userPermissions import ARLO_PERMS


fileMetaDataFieldsExportChoices = (
  ('fileSize', 'fileSize'),
  ('validRIFFsize', 'validRIFFsize'),
  ('validDATAsize', 'validDATAsize'),
  ('dataStartIndex', 'dataStartIndex'),
  ('dataSize', 'dataSize'),
  ('wFormatTag', 'wFormatTag'),
  ('numChannels', 'numChannels'),
  ('sampleRate', 'sampleRate'),
  ('nAvgBytesPerSec', 'nAvgBytesPerSec'),
  ('nBlockAlign', 'nBlockAlign'),
  ('bitsPerSample', 'bitsPerSample'),
  ('numBytesToTailPad', 'numBytesToTailPad'),
  ('numFrames', 'numFrames'),
  ('durationInSeconds', 'durationInSeconds'),
#  ('startTime', 'startTime'),
)

###
# Export Form

class exportLibraryMediaFileMetaDataForm(forms.Form):
  fileMetaData = forms.MultipleChoiceField(choices=fileMetaDataFieldsExportChoices, label='MediaFile MetaData Fields', widget=forms.SelectMultiple(attrs={'size':15}), required=False)
  userMetaData = forms.ModelMultipleChoiceField(MediaFileUserMetaDataField.objects.none(), label='UserMetaData Fields', widget=forms.SelectMultiple(attrs={'size':15}), required=False)


## Export MetaData from a Library.
#
# @param request The Django HTTP request object.
# @param libraryId The database Id of a Library from which to export MetaData
# @return Django HTTP response.

@login_required
def exportLibraryMediaFileMetaData(request, libraryId):
  logging.info('exportLibraryMediaFileMetaData LibraryId:' + libraryId.__str__())
  user = request.user
  library = Library.objects.get(id=libraryId)

  # check user permissions
  if not library.userHasPermissions(user, ARLO_PERMS.READ):
    return HttpResponseForbidden("Forbidden")

  status = None
  filePath = None

  if request.method == 'POST':
    form = exportLibraryMediaFileMetaDataForm(request.POST)
    form.fields['userMetaData'] = forms.ModelMultipleChoiceField(getLibraryUserMetaDataFields(library), label='UserMetaData Fields', widget=forms.SelectMultiple(attrs={'size':15}), required=False)

    if form.is_valid():
      data = form.cleaned_data

      filePath = createCSVForLibraryMetaDataExport(user, library, data['fileMetaData'], data['userMetaData'])

      if filePath is None:
        status = "Unknown Error Occurred... check logs"
        filePath = ''
      else:
        status = "Success"

  else: # GET
    form = exportLibraryMediaFileMetaDataForm()
    form.fields['userMetaData'] = forms.ModelMultipleChoiceField(getLibraryUserMetaDataFields(library), label='UserMetaData Fields', widget=forms.SelectMultiple(attrs={'size':15}), required=False)


  return render(request, 'tools/exportLibraryMetaData.html', {
                              'user': user,
                              'form': form,
                              'status': status,
                              'filePath': filePath,
                              'library': library
                              })


## Generate a CSV file of Library MetaData.
#
# @param user A User object.
# @param library A Library object.
# @param fileMetaDataFields A list of MediaFileMetaData names to include.
# @param userMetaDataFields A list of MediaFileUserMetaDataField names to include.
# @return Path of the destination file, relative to MEDIA_ROOT

def createCSVForLibraryMetaDataExport(user, library, fileMetaDataFields, userMetaDataFields):

  logging.info(
    "Creating CSV File for LibraryMetaDataExport: user = " + user.__str__() +
    " library = " + str(library) +
    " fileMetaDataFields = " + repr(fileMetaDataFields) +
    " userMetaDataFields = " + repr(userMetaDataFields))

  ###
  # Open File

  nowDateString = datetime.datetime.now().strftime('%Y%m%d-%H%M%S')
  fileName = str(library.id) + '-' + nowDateString + '-libraryMetaData.csv'
  relFilePath = nesterSettings.getExportFilePath(user, fileName)  # relative to MEDIA_ROOT
  absFilePath = os.path.join(settings.MEDIA_ROOT, relFilePath)

  logging.debug('createCSVForLibraryMetaDataExport(): Abs File Path: ' + absFilePath.__str__())
  try:
    FILE = open(absFilePath, 'w')
    w = csv.writer(FILE)
  except:
    logging.exception("createCSVForLibraryMetaDataExport(): file could not be opened (%s)", absFilePath)
    return None


  ###
  # Write Header

  columns = ['mediaFileId', 'mediaFileAlias',]

  for field in fileMetaDataFields:
    columns.append(str(field))

  for field in userMetaDataFields:
    columns.append(str(field))

  try:
    w.writerow(columns)
  except:
    logging.error('createCSVForLibraryMetaDataExport() could not write header')
    return None


  ###
  # Write out selected MetaData for each file in the Library

  for mediaFile in library.mediafile_set.all():
    dataFields = [str(mediaFile.id), mediaFile.alias]

    fileMetaData = getMediaFileMetaDataDictionary(mediaFile)
    for field in fileMetaDataFields:
      dataFields.append(fileMetaData.get(str(field)))

    userMetaData = getMediaFileUserMetaDataDictionary(mediaFile)
    for field in userMetaDataFields:
      dataFields.append(userMetaData.get(str(field)))

    try:
      w.writerow(dataFields)
    except Exception, e:
      logging.error('createCSVForLibraryMetaDataExport() could not write row to file for MediaFileId: ' + str(mediaFile.id))
      logging.exception(e)
      return None


  FILE.close()
  return relFilePath
