import os
import os.path
import logging
import csv
import datetime

from django.contrib.auth.decorators import login_required
from django.shortcuts import render
from django import forms
from django.conf import settings

import nesterSettings
from tools.models import TagClass, TagSet, Project, Tag
from .viewMetaData import getMetaDataEntry


class CreateFileTagReportForm(forms.Form):
  includeUserTags = forms.BooleanField(label='Include User Tags', required=False, initial=True)
  includeMachineTags = forms.BooleanField(label='Include Machine Tags', required=False, initial=False)
  tagSets = forms.ModelMultipleChoiceField(TagSet.objects.none(), widget=forms.CheckboxSelectMultiple, required=True)



@login_required
def createFileTagReport(request, projectId):
  logging.info('Creating File-Tag Report for projectId:' + projectId.__str__())
  user = request.user
  project = Project.objects.get(id=projectId)

  status = ''
  filePath = ''

  if request.method == 'POST':
    logging.info("# a #")
    form = CreateFileTagReportForm(request.POST)
    form.fields['tagSets'] = forms.ModelMultipleChoiceField(project.getTagSets(include_hidden=False), required=True)
    logging.info("# b #")
    if form.is_valid():
      logging.info("# c #")
      data = form.cleaned_data

      logging.info("# form: " + form.__str__())
      logging.info("# form.data: " + form.data.__str__())

      filePath = createCSVForFileTagReport(project, data['tagSets'], user, data['includeUserTags'], data['includeMachineTags'])
      if filePath is None:
        status = "Unknown Error Occurred... check logs"
        filePath = ''
      else:
        status = "Success"

      logging.info(status)

    else:
      logging.info("# form not valid")
      logging.info("# form: " + form.__str__())
      logging.info("# form.data: " + form.data.__str__())
  else:
    logging.info("# d #")
    form = CreateFileTagReportForm()
    form.fields['tagSets'] = forms.ModelMultipleChoiceField(project.getTagSets(include_hidden=False), required=True)
    #default return when request is GET and form.is_valid() is False

  description = """
    <p>Compiles a report (in CSV format) of the Tag Classes in each file.</p>
    <p>For each TagClass, reports the number of Tags, the total time of these Tags, and the fraction of the file tagged as this TagClass.</p>
    <p>Overlapping Tags are 'merged' when computing the time-tagged.</p>
    <p><strong>Note:</strong> For large collections, this can be very resource intensive to process, so please use caution with your settings :)</p>
    """

  return render(request, 'tools/createReport.html', {
                                'user':user,
                                'project': project,
                                'pageHeader':"File-Tags Report",
                                'form': form,
                                'status': status,
                                'filePath': filePath,
                                'apacheRoot':settings.APACHE_ROOT,
                                'description':description,
                                })


## Export a summary of Tags per File.
#
# @param project A Project object.
# @param tagSets A list of TagSets to include.
# @param user A User object.
# @param includeUserTags
# @param includeMachineTags
# @return Path of the destination file, relative to MEDIA_ROOT

def createCSVForFileTagReport(project, tagSets, user, includeUserTags, includeMachineTags):
  logging.info("Creating CSV For File-Tag Report: user = " + user.__str__())

  ###
  # Open File

  nowDateString = datetime.datetime.now().strftime('%Y%m%d-%H%M%S')
  fileName = str(project.id) + '-' + nowDateString + '-FileTags.csv'
  relFilePath = nesterSettings.getExportFilePath(user, fileName)  # relative to MEDIA_ROOT
  absFilePath = os.path.join(settings.MEDIA_ROOT, relFilePath)

  logging.debug('createCSVForFileTagReport(): Abs File Path: ' + absFilePath.__str__())
  try:
    FILE = open(absFilePath, 'w')
    w = csv.writer(FILE)
  except:
    logging.error("createCSVForFileTagReport(): Exception: file could not be opened (%s)", absFilePath)
    return None


  ###
  # Get Tag Classes

  projectTagClasses = TagClass.objects.filter(tag__tagSet__in=tagSets).distinct()


  ###
  # Write out File Header

  headerNames = ['FileId', 'FileAlias', 'FileDuration', 'Total-NumTags', 'Total-Time', 'Total-Fraction']

  for tagClass in projectTagClasses:
    headerNames.append(tagClass.displayName + "-NumTags")
    headerNames.append(tagClass.displayName + "-Time")
    headerNames.append(tagClass.displayName + "-Fraction")


  try:
    w.writerow(headerNames)
  except:
    logging.error('createCSVForFileTagReport(): could not write first row')
    return None


  ##################
  # Loop Over Files

  projectMediaFiles = project.getMediaFiles()

  for mediaFile in projectMediaFiles:
    mediaFileDuration = getMetaDataEntry(mediaFile, 'durationInSeconds')
    mediaFileDuration = float(mediaFileDuration)

    fileData = []
    fileData.append(mediaFile.id)
    fileData.append(mediaFile.alias)
    fileData.append(mediaFileDuration)

    #######################################
    #  Get Stats on All Tags in the File  #
    #######################################

    # Get Tags in File

    if includeUserTags and includeMachineTags:
      tags = Tag.objects.filter(
                     tagSet__in=tagSets,
                     mediaFile=mediaFile).order_by('startTime')
    elif includeUserTags:
      tags = Tag.objects.filter(
                     tagSet__in=tagSets,
                     userTagged=True,
                     mediaFile=mediaFile).order_by('startTime')
    elif includeMachineTags:
      tags = Tag.objects.filter(
                     tagSet__in=tagSets,
                     machineTagged=True,
                     mediaFile=mediaFile).order_by('startTime')
    else:
      return None

    # Count Tags
    fileData.append(tags.count())

    ###
    # Look for overlapping tags and 'merge' times

    tagTimes = [] # list of tuples (startTime, duration)

    # build list
    for tag in tags:
      tagTimes.append( (tag.startTime, tag.endTime - tag.startTime) )

    # already sorted by query above

    # now 'merge' overlapping tags
    i = 1
    while i < len(tagTimes):
      (prevStartTime, prevDuration) = tagTimes[i-1]
      (curStartTime, curDuration) = tagTimes[i]

      if (prevStartTime + prevDuration) < curStartTime:
        # no overlap, continue on
        i += 1
      elif (prevStartTime + prevDuration) >= (curStartTime + curDuration):
        # previous Tag completely encapsulates this one, discard the current
        del tagTimes[i]
      else:
        # only partially overlaps
        # we know where it starts...
        newStartTime = prevStartTime
        # newDuration is the endTime of the current - the startTime of the prev
        newDuration =  (curStartTime + curDuration) - prevStartTime

        tagTimes[i-1] = (newStartTime, newDuration)
        del tagTimes[i]

    ###
    # Compute Time in File

    totalTagTime = 0
    for (startTime, duration) in tagTimes:
      totalTagTime += duration

    fileFraction = totalTagTime / mediaFileDuration

    ###
    # Save the results

    fileData.append(totalTagTime.__str__())
    fileData.append(fileFraction.__str__())


    ###########################
    #  Get Tags per TagClass  #
    ###########################

    for tagClass in projectTagClasses:

      ###
      # Get Tags in File and TagClass

      if includeUserTags and includeMachineTags:
        tags = Tag.objects.filter(
                       tagSet__in=tagSets,
                       tagClass=tagClass,
                       mediaFile=mediaFile).order_by('startTime')
      elif includeUserTags:
        tags = Tag.objects.filter(
                       tagSet__in=tagSets,
                       userTagged=True,
                       tagClass=tagClass,
                       mediaFile=mediaFile).order_by('startTime')
      elif includeMachineTags:
        tags = Tag.objects.filter(
                       tagSet__in=tagSets,
                       machineTagged=True,
                       tagClass=tagClass,
                       mediaFile=mediaFile).order_by('startTime')
      else:
        return None

      ###
      # Count Tags

      logging.debug("Tag count = " + tags.count().__str__())

      fileData.append(tags.count())

      ###
      # Look for overlapping tags and 'merge' times

      tagTimes = [] # list of tuples (startTime, duration)

      # build list
      for tag in tags:
        tagTimes.append( (tag.startTime, tag.endTime - tag.startTime) )

      # already sorted by query above

      # now 'merge' overlapping tags
      i = 1
      while i < len(tagTimes):
        (prevStartTime, prevDuration) = tagTimes[i-1]
        (curStartTime, curDuration) = tagTimes[i]

        if (prevStartTime + prevDuration) < curStartTime:
          # no overlap, continue on
          i += 1
        elif (prevStartTime + prevDuration) >= (curStartTime + curDuration):
          # previous Tag completely encapsulates this one, discard the current
          del tagTimes[i]
        else:
          # only partially overlaps
          # we know where it starts...
          newStartTime = prevStartTime
          # newDuration is the endTime of the current - the startTime of the prev
          newDuration =  (curStartTime + curDuration) - prevStartTime

          tagTimes[i-1] = (newStartTime, newDuration)
          del tagTimes[i]

      ###
      # Compute Time in File

      totalTagTime = 0
      for (startTime, duration) in tagTimes:
        totalTagTime += duration

      fileFraction = totalTagTime / mediaFileDuration


      ###
      # Save the results

      fileData.append(totalTagTime.__str__())
      fileData.append(fileFraction.__str__())

    ###
    # Save Results for this File

    w.writerow(fileData)


  FILE.close()
  return relFilePath
