import datetime
import logging

from django.contrib.auth.decorators import login_required
from django.shortcuts import render
from django.http import HttpResponseRedirect, HttpResponseForbidden, HttpResponse, HttpResponseServerError
from django import forms
from django.conf import settings

from tools.models import *
import tools.Meandre as Meandre
from tools.Meandre import MeandreAction
from userPermissions import *
from project import *
from biasManipulation import AddJobParameters


class ExpertAgreementOptimizeForm(forms.Form):
  name = forms.CharField(label='Name', max_length=255, initial='default')
  randomWindowJobId = forms.IntegerField(label='randomWindowJobId')
#  tagClassesToSearch = forms.ModelMultipleChoiceField(TagClass.objects.none())
  sourceTagSets      = forms.ModelMultipleChoiceField(TagSet.objects.none(), widget=forms.CheckboxSelectMultiple, required=True)
  distanceWeightingPower = forms.FloatField(label='Distance Weighting Power', initial=10.0)
  classificationProbabilityThreshold = forms.FloatField(label='Classification Probability Threshold', initial=0.4)
#  destinationTagSet  = forms.ModelChoiceField(TagSet.objects.none(), empty_label=None, required=True)
#  numberOfTagsToDiscover = forms.IntegerField(label='Number Of Tags To Discover', initial=1)
#  REPRESENTATION BIAS
  numFrequencyBandsLower = forms.IntegerField(label='Num Frequency Bands Lower Bound', initial=128)
  numFrequencyBandsUpper = forms.IntegerField(label='Num Frequency Bands Upper Bound', initial=128)
  numTimeFramesPerSecondLower = forms.FloatField(label='Num Time Frames Per Second Lower Bound', initial=100.0)
  numTimeFramesPerSecondUpper = forms.FloatField(label='Num Time Frames Per Second Upper Bound', initial=100.0)
  dampingRatioLower = forms.FloatField(label='Damping Ratio Lower Bound', initial=0.02)
  dampingRatioUpper = forms.FloatField(label='Damping Ratio Upper Bound', initial=0.02)
  minFrequencyLower = forms.FloatField(label='Min Frequency Lower Bound', initial=20.0)
  minFrequencyUpper = forms.FloatField(label='Min Frequency Upper Bound', initial=120.0)
  maxFrequencyLower = forms.FloatField(label='Max Frequency Lower Bound', initial=12000.0)
  maxFrequencyUpper = forms.FloatField(label='Max Frequency Upper Bound', initial=20000.0)
#  spectraWeight = forms.FloatField(label='Spectra Weight', initial=1.0)
#  pitchWeight = forms.FloatField(label='Pitch Weight', initial=0.0)
#  averageEnergyWeight = forms.FloatField(label='Average Energy Weight', initial=0.0)
#  numExemplars = forms.IntegerField(label='Num Exemplars', initial=999999)
# SELECTION BIAS
#  maxOverlapFraction = forms.FloatField(label='maxOverlapFraction', initial=0.1)
#  minPerformance = forms.FloatField(label='minPerformance', initial=0.5)
# Optional
#  searchWithinTagClasses = forms.ModelMultipleChoiceField(TagClass.objects.none(), required=False)

class ExpertAgreementClassifyForm(forms.Form):
  name = forms.CharField(label='Name', max_length=255, initial='default')
  randomWindowJobId = forms.IntegerField(label='randomWindowJobId')
  sourceTagSets      = forms.ModelMultipleChoiceField(TagSet.objects.none(), widget=forms.CheckboxSelectMultiple, required=True)
  distanceWeightingPower = forms.FloatField(label='Distance Weighting Power', initial=10.0)
  classificationProbabilityThreshold = forms.FloatField(label='Classification Probability Threshold', initial=0.4)
#  REPRESENTATION BIAS
  numFrequencyBands = forms.IntegerField(label='Num Frequency Bands', initial=128)
  numTimeFramesPerSecond = forms.FloatField(label='Num Time Frames Per Second', initial=100.0)
  dampingRatio = forms.FloatField(label='Damping Ratio', initial=0.02)
  minFrequency = forms.FloatField(label='Min Frequency', initial=20.0)
  maxFrequency = forms.FloatField(label='Max Frequency', initial=12000.0)


## Launch a SupervisedTagDiscovery job using the QueueRunner job manager.
#
# GET request shows the form, POST processes it.
# @param request The Django HTTP request object
# @param projectId The database ID of the Project in which to run the discovery
# @param fromJob Optional Pre-populate the field with the settings from a previous job
# @return Django HTTP response object


@login_required
def expertAgreementClassificationQueueRunner(request, projectId, fromJob=0):
  # fromJob = If set on a GET, the default form parameters will be pulled from the existing job
  user = request.user
  logging.info("Starting Expert Agreement Classification (Queue Runner): user = " + user.__str__() +
    " projectId = " + projectId.__str__() +
    "  fromJob=" + fromJob.__str__())
  project = Project.objects.get(id=projectId)

  # check user Permissions
  if not project.userHasPermissions(user, ARLO_PERMS.WRITE):
    return HttpResponseForbidden("Forbidden")

  # Setup Forms
  optimizeForm = ExpertAgreementOptimizeForm()
  optimizeForm.fields['sourceTagSets'] = forms.ModelMultipleChoiceField(project.getTagSets(include_hidden=False), required=True)

  classifyForm = ExpertAgreementClassifyForm()
  classifyForm.fields['sourceTagSets'] = forms.ModelMultipleChoiceField(project.getTagSets(include_hidden=False), required=True)

  # GET Request
  if request.method != 'POST':
    return render(request, "tools/expertAgreementClassification.html", {
                              'user': user,
                              'apacheRoot': settings.APACHE_ROOT,
                              'optimizeForm': optimizeForm,
                              'classifyForm': classifyForm,
                              'project': project,
                              })


  ###
  # If here, we have a POST Request

  method = request.POST.get('method', None)

  if method == "Optimize":

    ###
    # Optimize Task

    optimizeForm = ExpertAgreementOptimizeForm(request.POST)
    optimizeForm.fields['sourceTagSets'] = forms.ModelMultipleChoiceField(project.getTagSets(include_hidden=False), required=True)

    if not optimizeForm.is_valid(): # All validation rules pass
      logging.info("Invalid Optimize Form Data")
      return render(request, "tools/expertAgreementClassification.html", {
                                'user':user,
                                'apacheRoot':settings.APACHE_ROOT,
                                'optimizeForm': optimizeForm,
                                'classifyForm': classifyForm,
                                })

    data = optimizeForm.cleaned_data

    # Start a new job, and setup it's parameters

    newJob = Jobs(
      user=user,
      project=project,
      creationDate=datetime.datetime.now(),
      type=JobTypes.objects.get(name="ExpertAgreementClassification"),
      name=data['name'],
      status=JobStatusTypes.objects.get(name="Unknown"),
      )
    newJob.save()

    srcTagSetsCsv = ""
    for ts in data['sourceTagSets']:
      srcTagSetsCsv += ts.id.__str__() + ","

    JobParameters(job=newJob, name='method', value='optimize').save()
    JobParameters(job=newJob, name='randomWindowJobId', value=data['randomWindowJobId']).save()
    JobParameters(job=newJob, name='distanceWeightingPower', value=data['distanceWeightingPower']).save()
    JobParameters(job=newJob, name='classificationProbabilityThreshold', value=data['classificationProbabilityThreshold']).save()
    JobParameters(job=newJob, name='numFrequencyBandsLower', value=data['numFrequencyBandsLower']).save()
    JobParameters(job=newJob, name='numFrequencyBandsUpper', value=data['numFrequencyBandsUpper']).save()
    JobParameters(job=newJob, name='numTimeFramesPerSecondLower', value=data['numTimeFramesPerSecondLower']).save()
    JobParameters(job=newJob, name='numTimeFramesPerSecondUpper', value=data['numTimeFramesPerSecondUpper']).save()
    JobParameters(job=newJob, name='dampingRatioLower', value=data['dampingRatioLower']).save()
    JobParameters(job=newJob, name='dampingRatioUpper', value=data['dampingRatioUpper']).save()
    JobParameters(job=newJob, name='minFrequencyLower', value=data['minFrequencyLower']).save()
    JobParameters(job=newJob, name='minFrequencyUpper', value=data['minFrequencyUpper']).save()
    JobParameters(job=newJob, name='maxFrequencyLower', value=data['maxFrequencyLower']).save()
    JobParameters(job=newJob, name='maxFrequencyUpper', value=data['maxFrequencyUpper']).save()
    JobParameters(job=newJob, name='sourceTagSets', value=srcTagSetsCsv).save()

    newJob.status = JobStatusTypes.objects.get(name="Queued")
    newJob.save()

    return HttpResponseRedirect(settings.URL_PREFIX + '/tools/manageJobs/' + project.id.__str__())


  elif method == "Classify":

    ###
    # Classify Task

    classifyForm = ExpertAgreementClassifyForm(request.POST)
    classifyForm.fields['sourceTagSets'] = forms.ModelMultipleChoiceField(project.getTagSets(include_hidden=False), required=True)

    if not classifyForm.is_valid(): # All validation rules pass
      logging.info("Invalid Classify Form Data")
      return render(request, "tools/expertAgreementClassification.html", {
                                'user':user,
                                'apacheRoot':settings.APACHE_ROOT,
                                'optimizeForm': optimizeForm,
                                'classifyForm': classifyForm,
                                })

    data = classifyForm.cleaned_data


    # Start a new job, and setup it's parameters

    newJob = Jobs(
      user=user,
      project=project,
      creationDate=datetime.datetime.now(),
      type=JobTypes.objects.get(name="ExpertAgreementClassification"),
      name=data['name'],
      status=JobStatusTypes.objects.get(name="Unknown"),
      )
    newJob.save()

    srcTagSetsCsv = ""
    for ts in data['sourceTagSets']:
      srcTagSetsCsv += ts.id.__str__() + ","

    JobParameters(job=newJob, name='method', value='classify').save()
    JobParameters(job=newJob, name='randomWindowJobId', value=data['randomWindowJobId']).save()
    JobParameters(job=newJob, name='distanceWeightingPower', value=data['distanceWeightingPower']).save()
    JobParameters(job=newJob, name='classificationProbabilityThreshold', value=data['classificationProbabilityThreshold']).save()
    JobParameters(job=newJob, name='numFrequencyBands', value=data['numFrequencyBands']).save()
    JobParameters(job=newJob, name='numTimeFramesPerSecond', value=data['numTimeFramesPerSecond']).save()
    JobParameters(job=newJob, name='dampingRatio', value=data['dampingRatio']).save()
    JobParameters(job=newJob, name='minFrequency', value=data['minFrequency']).save()
    JobParameters(job=newJob, name='maxFrequency', value=data['maxFrequency']).save()
    JobParameters(job=newJob, name='sourceTagSets', value=srcTagSetsCsv).save()

    newJob.status = JobStatusTypes.objects.get(name="Queued")
    newJob.save()

    return HttpResponseRedirect(settings.URL_PREFIX + '/tools/manageJobs/' + project.id.__str__())

  else:
    return HttpResponseServerError("Unknown Method")
