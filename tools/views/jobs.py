import logging

from django import forms
from django.contrib.auth.decorators import login_required
from django.shortcuts import render, redirect
from django.http import HttpResponseForbidden, HttpResponseNotAllowed, HttpResponseNotFound, HttpResponseServerError

from plugins.ArloPluginManager import ArloPluginManager
import settings as settings
from tools.models import JobTypes, Jobs, JobStatusTypes, JobParameters, Project
from .userPermissions import ARLO_PERMS


def _getJobTypeForm(data=None):
    # Get the installed plugins
    choices = []
    for plugin in ArloPluginManager.getJobPlugins():
        if plugin.plugin_object.showInLaunchUI():
            choices.append( (plugin.name, plugin.friendlyName) )

    # Build the form
    jobTypeForm = forms.Form(data)
    jobTypeForm.fields['jobTypeName'] = forms.ChoiceField(label='Job Type', choices=choices)
    return jobTypeForm


@login_required
def selectJobType(request, projectId):
    """
    Step 1, select a Job Plugin to run the Job, then we'll load it's settings form
    """
    user = request.user
    project = Project.objects.get(id=projectId)

    # check user permissions
    if not project.userHasPermissions(user, ARLO_PERMS.LAUNCH_JOB):
        return HttpResponseForbidden("Forbidden")

    if request.method == 'GET':
        jobTypeForm = _getJobTypeForm()

    elif request.method == 'POST':
        jobTypeForm = _getJobTypeForm(request.POST)

        if jobTypeForm.is_valid():
            data = jobTypeForm.cleaned_data
            jobTypeName = data['jobTypeName']
            return redirect('configureJob', projectId=projectId, jobTypeName=jobTypeName)

    else:
        return HttpResponseNotAllowed(['GET', 'POST'])

    return render(request, "tools/launchJob-selectJobType.html", {
                'apacheRoot':settings.APACHE_ROOT,
                'user':request.user,
                'form':jobTypeForm,
                'jobPlugins':ArloPluginManager.getJobPlugins(),
                })

@login_required
def configureJob(request, projectId, jobTypeName):
    project = Project.objects.get(id=projectId)

    # check user permissions
    if not project.userHasPermissions(request.user, ARLO_PERMS.LAUNCH_JOB):
        return HttpResponseForbidden("Forbidden")

    plugin = ArloPluginManager.getJobPluginByName(jobTypeName)
    if plugin is None:
        return HttpResponseNotFound("Job Plugin '{}' Not Found".format(jobTypeName))

    if request.method == 'GET':
        (form, templateName) = plugin.getSettingsForm(project=project, data=None)

    elif request.method == 'POST':
        (form, templateName) = plugin.getSettingsForm(project=project, data=request.POST)

        if form.is_valid():

            job_settings = plugin.parseSettingsForm(project=project, form=form)
            if job_settings is None:
                return HttpResponseServerError("Unknown Error Parsing Settings Form")

            # create Job
            newJob = Jobs(
                    user=request.user,
                    project=project,
                    name=job_settings.get('name'),
                    type=JobTypes.objects.get(name="JobPlugin"),
                    status=JobStatusTypes.objects.get(name="Unknown"),
                  )
            newJob.save()

            # save settings
            for k, v in job_settings.get('params', []):
                logging.info("saving JobParameter : " + k.__str__() + " value = " + v.__str__())
                param = JobParameters(job=newJob, name=k, value=v.__str__())
                param.save()

            # queue the job
            newJob.status = JobStatusTypes.objects.get(name="Queued")
            newJob.save()

            return redirect('viewJob', jobID = newJob.id)

    else:
        return HttpResponseNotAllowed(['GET', 'POST'])

    return render(request, templateName, {
        'apacheRoot':settings.APACHE_ROOT,
        'user':request.user,
        'form':form,
        })
