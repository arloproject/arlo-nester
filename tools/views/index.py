import logging

from django.contrib.auth import logout, login
from django.contrib.auth.decorators import login_required
from django.db.models import Count
from django.http import HttpResponse, HttpResponseRedirect, JsonResponse, HttpResponseForbidden
from django.shortcuts import render
from django.contrib.auth.models import User
from django.contrib.auth.forms import UserCreationForm
from django.middleware import csrf
from django.core.paginator import Paginator, EmptyPage, PageNotAnInteger
from django.views.decorators.http import require_http_methods

from django.conf import settings
from tools.models import DeleteUserForm, Project, Library, MediaFile, UserSettings
from tools.views.viewMetaData import getMetaDataEntry
import tools.Meandre as Meandre
from tools.Meandre import MeandreAction
from .userPermissions import ARLO_PERMS
from .audio import deleteAudioFile


## Render the main Index page
# @param request The Django HTTP request object
# @return Django HTTPResponse object

def index(request):
  from django.contrib.auth.forms import AuthenticationForm
  form = AuthenticationForm()
  return render(request, 'tools/index.html', {
      'login_form': form
  })

## Render the Admin options page
# @param request The Django HTTP request object
# @return Django HTTPResponse object

@login_required
def admin(request):

  user = request.user

  if not user.is_staff:
    return HttpResponse('Permission Denied')

  return render(request, 'tools/admin.html')

## Redirect to the User Home Page, which will force a login.
# @param request The Django HTTP request object
# @return Django HTTPResponse object

@login_required
def loginUser(request):
  return HttpResponseRedirect('userHome')


## Delete Media Files
# Currently only AJAX
# @param request The Django HTTP request object
# @return JSON object

@login_required
@require_http_methods(['POST'])
def deleteMediaFile(request):
    user = request.user

    mediaFileIdsToDelete = request.POST.get('files', None).split("//")
    mediaFilesToDelete = MediaFile.objects.filter(id__in=mediaFileIdsToDelete)

    # Check perms
    for mediaFile in mediaFilesToDelete:
        if not mediaFile.userHasPermissions(user, ARLO_PERMS.WRITE):
            return HttpResponseForbidden("Forbidden - You do not have write access to this MediaFile.")

    for mediaFile in mediaFilesToDelete:
        deleteAudioFile(mediaFile.id)

    data = {
      'files': mediaFileIdsToDelete,
    }

    return JsonResponse(data)


## Render the user's home page
# @param request The Django HTTP request object
# @return Django HTTPResponse object

@login_required
def userHome(request):

  user = request.user
  logging.info("Projects: user = " + user.__str__())

  userSettings = UserSettings.objects.get(user=user, name='default')

  ###
  # Get user media files

  mediaFilesAll = MediaFile.objects.filter(user=user).annotate(tagCount = Count('tag'))

  ## Sort -- still working on how this should work across views.
  sort = request.GET.get('sort', None)

  if sort is None:
    sort = 'uploadDate'

  if sort == 'uploadDate':
    mediaFilesAll = mediaFilesAll.order_by(sort).reverse()
  elif sort == 'tagCount':
    mediaFilesAll = mediaFilesAll.order_by('-tagCount')
  else:
    mediaFilesAll = mediaFilesAll.order_by(sort)

  ## This is wasteful, but for some reason if I tried to create the list from mediaFilesAll I was getting an error.
  mediaFilesAllIDs = MediaFile.objects.filter(user=user).values_list('id', flat=True)

  totalFiles = len(mediaFilesAll)

  #mediaFiles = mediaFiles[0:userSettings.maxNumAudioFilesToList]

  paginator = Paginator(mediaFilesAll, settings.ARLO_UI_PAGE_SIZE)

  page = request.GET.get('page')
  try:
      mediaFiles = paginator.page(page)
  except PageNotAnInteger:
      # If page is not an integer, deliver first page.
      mediaFiles = paginator.page(1)
  except EmptyPage:
      # If page is out of range (e.g. 9999), deliver last page of results.
      mediaFiles = paginator.page(paginator.num_pages)

  index  = mediaFiles.number #current page
  max_index = paginator.num_pages
  start_index = max(index - settings.ARLO_UI_PAGINATION_RANGE - 1, 0)
  end_index = min(index + settings.ARLO_UI_PAGINATION_RANGE, max_index)
  pageRange = paginator.page_range[start_index:end_index]

  MediaFileList = []
  for mediaFile in mediaFiles:
      projectList = []
      for project in mediaFile.getContainingProjects():
          projectList.append( {
            'name': project.name,
            'id': project.id
          })


      MediaFileList.append( {
        'id': mediaFile.id,
        'library': mediaFile.library,
        'library_id': mediaFile.library_id,
        'active': mediaFile.active,
        'startTime': getMetaDataEntry(mediaFile, 'startTime'),
        'alias': mediaFile.alias,
        'fileURL': mediaFile.file.url,
        'sensor': mediaFile.sensor,
        'projectList': projectList,
        'duration': round(float(getMetaDataEntry(mediaFile, 'durationInSeconds'))),
        'tagCount': mediaFile.tagCount,
        'uploadDate': mediaFile.uploadDate,
        })

  ###
  # Get user libraries.

  libraries = Library.objects.getUserLibraries(request.user)

  ###
  # User's Own Projects

  userProjects = Project.objects.filter(user=user).order_by('name')
  userProjectInfos = []

  for project in userProjects:

    sharedUserInfos = []
    sharedUsers = project.getProjectUsers()

    for sharedUser in sharedUsers:
      sharedUserInfos.append({
        'user': sharedUser,
        'canRead': project.userHasPermissions(sharedUser, ARLO_PERMS.READ),
        'canWrite': project.userHasPermissions(sharedUser, ARLO_PERMS.WRITE),
        })

    userProjectInfos.append( {
      'project': project,
      'sharedUserInfos': sharedUserInfos,
      })

  ###
  # Shared Projects

  sharedProjectInfos = []

  for project in Project.objects.getUserProjects(user):
    if project.user != user:
      sharedProjectInfos.append({
        'project': project,
        'canRead': project.userHasPermissions(user, ARLO_PERMS.READ),
        'canWrite': project.userHasPermissions(user, ARLO_PERMS.WRITE),
        })

  ## Display options
  MediaFileListColumns = {
    'name': True,
    'duration': True,
    'project': True,
    'library':True,
    'uploadDate': True,
    }

  listSortOptions = {
      'uploadDate': True,
      'alias': True,
      'duration': True
  }

  return render(request, "tools/userHome.html", {
    'user': user,
    'userProjects': userProjectInfos,
    'sharedProjectInfos': sharedProjectInfos,
    'libraries': libraries,
    'MediaFileList': MediaFileList,
    'searchPrompt': 'Search all files...',
    'MediaFileListColumns': MediaFileListColumns,
    'MediaFilesPaginator': mediaFiles,
    'totalFiles': totalFiles,
    'apacheRoot':settings.APACHE_ROOT,
    'fileListHeader': 'Audio Files',
    'section': 'audioFiles',
    'mediaFilesAllIDs': mediaFilesAllIDs,
    'allFilesLabel': 'all libraries',
    'listSort': sort,
    'listSortOptions': listSortOptions,
    'pageRange': pageRange,
    'lastPage': max_index,
    })

## Create a new user handler. GET request returns the UserCreationForm,
# and POST saves it. Note that the logged in user must be an Admin (Django
# 'staff' user).
# @param request The Django HTTP request object
# @return Django HTTPResponse object

@login_required
def createUser(request):
  user = request.user
  if not user.is_staff:
      return HttpResponse('Permission Denied')

  if request.method != 'POST':
      form = UserCreationForm()
      return render(request, "tools/createUser.html", {
        'form':form,
        'user':user,
        })

  # if here, POST request
  logging.info("Starting Create User")

  form = UserCreationForm(request.POST)
  if not form.is_valid():
    return render(request, "tools/createUser.html", {
      'form':form,
      'user':user,
      })

  # Add user
  new_user = form.save()
  logging.info("new_user.username = " + new_user.username.__str__())
  # Add userSettings
  userSettings = UserSettings()
  userSettings.user = new_user
  userSettings.save()

  return render(request, "tools/createUser.html", {
    'form':form,
    'user':user,
    'message': 'User created.'
    })


## Delete user handler. GET request returns the DeleteUserForm
# and POST removes it. Note that the logged in user must be an
# Admin (Django 'staff' user).
# @param request The Django HTTP request object
# @return Django HTTPResponse object

@login_required
def deleteUser(request):
  user = request.user

  if not user.is_staff:
      return HttpResponse('Permission Denied')

  logging.info("Starting Delete User")

  if request.method == 'POST':
      form = DeleteUserForm(request.POST)

      if form.is_valid():
          data = form.cleaned_data

          del_user = User.objects.get(username=data['username'])
          logging.info("del_user.username = " + del_user.username.__str__())
          del_user.delete()
          return HttpResponse('Successfully deleted User')
  else:
      form = DeleteUserForm()

  return render(request, "tools/deleteUser.html", {
    'form':form,
    'user':user,
    })


## Make a Meandre (Java) call to meandreTestCall(). Note that the logged
# in user must be an Admin (Django 'staff' user). Primarily used for debugging.
# @param request The Django HTTP request object
# @return Django HTTPResponse object

def meandreTestCall(request):
  user = request.user

  if not user.is_staff:
      return HttpResponse('Permission Denied')

  logging.info("Starting Meandre Test Call")
  results = Meandre.call(MeandreAction.meandreTestCall, {'id':1})
  logging.info(results[1])

  return render(request, 'tools/genericMessage.html', {
    'message': 'Meandre Test Call Result = ' + results[1],
    })


## Render a 'Login Failed' message
# @param request The Django HTTP request object
# @return Django HTTPResponse object

def loginFailed(request):
  return render(request, 'tools/genericMessage.html', {
    'message': 'Login Failed',
    })


## Logout the current user.
# @param request The Django HTTP request object
# @return Django HTTPResponse object

def logoutUser(request):
  logout(request)
  return HttpResponseRedirect('/')

## Login as another system user (without needing to re-authenticate). Note that
# the logged in user must be an Admin (Django 'staff' user). GET returns a
# user selection form (defined locally), POST completes the login.
# @param request The Django HTTP request object
# @return Django HTTPResponse object

@login_required
def loginAsUser(request):
  user = request.user

  if not user.is_staff:
      return HttpResponse('Permission Denied')

  if request.method == 'POST':

      newUsername = request.POST.get('newUser')
      newUser = User.objects.get(username__exact=newUsername)
      if newUser is not None:
        newUser.backend = 'django.contrib.auth.backends.ModelBackend'
        logging.info("Switch to User " + newUser.username)
        login(request, newUser)
        return HttpResponseRedirect('userHome')

      return HttpResponse ("Invalid User")
  else:
    # sResponse = "<form method='post' action=''> <input maxlength='30' type='text' name='newUser' /> <input type='submit' value='login' /> </form>"
    sResponse = "<form method='post' action=''>"
    sResponse += "<input type='hidden' name='csrfmiddlewaretoken' value='" + csrf.get_token(request) + "'>"
    sResponse += "<select name='newUser' /> "
    users = User.objects.all().order_by('username')
    for user in users:
      sResponse += "<option value='" + user.username + "'>" + user.username + "</option>"
    sResponse += "</select><input type='submit' value='login' /> </form>"
    return HttpResponse (sResponse)


## Add a queued "Test" task to the database
# @param request The Django HTTP request object
# @return Django HttpResponse object

# from biasManipulation import *

def addTestTaskToDb(request):

  return HttpResponse('Disabled')
#   return HttpResponseRedirect('/')
