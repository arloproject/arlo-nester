import os
import os.path
import logging
import urllib

from django import forms
from django.contrib.auth.decorators import login_required
from django.http import HttpResponseRedirect, HttpResponseForbidden, HttpResponseNotAllowed, HttpResponseServerError, JsonResponse
from django.shortcuts import render
from django.conf import settings
from django.core.exceptions import ObjectDoesNotExist

import nesterSettings as nesterSettings
from tools.models import MediaFile, UserSettings, VisualizeAudioForm, TagSet, Jobs, RandomWindow, JobParameters, CreateTagForm, TagClass
from .viewMetaData import getMetaDataEntry
from .userPermissions import ARLO_PERMS
from .spectra import generateSpectraImage, SpectraImageSettings, SpectraImage


## Visualize an audio file.
# Simple navigation interface, not tagging controls.
# @param request The Django HTTP request object
# @param audioID The Database ID of the audio file to visualize
# @return Django HTTP response object

@login_required
def visualizeAudio(request, audioID):
  user = request.user
  logging.info("Visualizing Audio: user = " + user.__str__() + " audioID = " + audioID.__str__())

  audioFile = MediaFile.objects.get(id=audioID)

  # Check user permissions
  if not audioFile.userHasPermissions(user, ARLO_PERMS.READ):
    return HttpResponseForbidden("Forbidden")

  userSettings = UserSettings.objects.get(user=user, name='default')

  audioSegmentFilePath = ""
  imageFilePath = ""
  playerWidth = ""
  imageWidth = ""
  imageHeight = ""
  spectraBorderWidth = ""
  spectraTopBorderHeight = ""
  timeScaleHeight = ""

  if request.method == 'POST': # If the form has been submitted...
    form = VisualizeAudioForm(request.POST) # A form bound to the POST data
    form.fields['tagSets'] = forms.ModelMultipleChoiceField(TagSet.objects.none(), required=False)

    if form.is_valid(): # All validation rules pass

      cleanedData = form.cleaned_data

      duration = cleanedData['endTime'] - cleanedData['startTime']
      method = form.data['method']
      logging.info("Method: " + method.__str__())

      ##
      # Navigation
      if method == 'Next':
        cleanedData['endTime'] += duration
        cleanedData['startTime'] += duration

      if (method == 'Back'):
        cleanedData['endTime'] -= duration
        cleanedData['startTime'] -= duration

        if (cleanedData['startTime'] < 0):
          offset = - cleanedData['startTime']
          cleanedData['startTime'] += offset
          cleanedData['endTime'] += offset

      if (method == 'Zoom In Time'):
        cleanedData['spectraNumFramesPerSecond'] = cleanedData['spectraNumFramesPerSecond'] * 2
        cleanedData['startTime'] += duration / 4
        cleanedData['endTime'] -= duration / 4

      if (method == 'Zoom Out Time'):
        cleanedData['spectraNumFramesPerSecond'] = cleanedData['spectraNumFramesPerSecond'] / 2
        cleanedData['startTime'] -= duration / 2
        cleanedData['endTime'] += duration / 2

        if (cleanedData['startTime'] < 0):
          offset = - cleanedData['startTime']
          cleanedData['startTime'] += offset
          cleanedData['endTime'] += offset

      if (method == 'Zoom In Freq'):
        cleanedData['spectraNumFrequencyBands'] = cleanedData['spectraNumFrequencyBands'] * 2

      if (method == 'Zoom Out Freq'):
        cleanedData['spectraNumFrequencyBands'] = cleanedData['spectraNumFrequencyBands'] / 2

      if (method == 'Increase Damping'):
        cleanedData['spectraDampingFactor'] = cleanedData['spectraDampingFactor'] * 2

      if (method == 'Decrease Damping'):
        cleanedData['spectraDampingFactor'] = cleanedData['spectraDampingFactor'] / 2

      ##
      # Settings
      endTime = cleanedData['endTime']
      startTime = cleanedData['startTime']
      gain = cleanedData['gain']
      spectraNumFramesPerSecond = cleanedData['spectraNumFramesPerSecond']
      spectraNumFrequencyBands = cleanedData['spectraNumFrequencyBands']
      spectraDampingFactor = cleanedData['spectraDampingFactor']
      spectraMinimumBandFrequency = cleanedData['spectraMinimumBandFrequency']
      spectraMaximumBandFrequency = cleanedData['spectraMaximumBandFrequency']
      loadAudio = cleanedData['loadAudio']


      ##
      # Spectra Settings
      spectraBorderWidth = 56
      spectraTopBorderHeight = 30
      timeScaleHeight = 22

      duration = endTime - startTime
      imageWidth = spectraBorderWidth * 2 + spectraNumFramesPerSecond * duration
      imageHeight = spectraNumFrequencyBands + spectraTopBorderHeight + timeScaleHeight

      playerWidth = imageWidth

      ###
      # Generate Spectra

      spectra_params = {
        'userSettings':userSettings,
        'audioFile': audioFile,

        'startTime': startTime,
        'endTime': endTime,
        'gain': gain,
        'numFramesPerSecond': spectraNumFramesPerSecond,
        'numFrequencyBands': spectraNumFrequencyBands,
        'dampingFactor': spectraDampingFactor,

        'minFrequency': spectraMinimumBandFrequency,
        'maxFrequency': spectraMaximumBandFrequency,

        'simpleNormalization': True,
        'showTimeScale': True,
        'showFrequencyScale': True,

        'borderWidth': spectraBorderWidth,
        'topBorderHeight': spectraTopBorderHeight,
        'timeScaleHeight': timeScaleHeight,
        'catalogSpectraNumFrequencyBands': spectraNumFrequencyBands, }

      (success, imageFilePath, audioSegmentFilePath)  = generateSpectraImage(**spectra_params)
      if not success:
        logging.error("createAudioSpectra Unknown Failure While Generating Spectra")
        return HttpResponseServerError("createAudioSpectra Unknown Spectra Generation Failure")


      form = VisualizeAudioForm(cleanedData)
      form.fields['tagSets'] = forms.ModelMultipleChoiceField(TagSet.objects.none(), required=False)
      firstTime = False


    else: # if form.is_valid
      defaultUserSettings = UserSettings.objects.get(user=user, name='default')
      loadAudio = defaultUserSettings.loadAudio
      firstTime = False

  else: # GET request

    defaultUserSettings = UserSettings.objects.get(user=user, name='default')
    spectraNumFramesPerSecond = defaultUserSettings.fileViewSpectraNumFramesPerSecond
    spectraNumFrequencyBands = defaultUserSettings.fileViewSpectraNumFrequencyBands
    spectraDampingFactor = defaultUserSettings.fileViewSpectraDampingFactor
    spectraMinimumBandFrequency = defaultUserSettings.fileViewSpectraMinimumBandFrequency
    spectraMaximumBandFrequency = defaultUserSettings.fileViewSpectraMaximumBandFrequency
    startTime = 0.0
    endTime = defaultUserSettings.fileViewWindowSizeInSeconds
    gain = 1.0
    loadAudio = defaultUserSettings.loadAudio


    form = VisualizeAudioForm(initial={'alias':audioFile.alias,
                                       'mediaFileId':audioFile.id,
                                       'startTime':startTime,
                                       'endTime':endTime,
                                       'gain' : gain,
                                       'spectraNumFramesPerSecond':spectraNumFramesPerSecond,
                                       'spectraNumFrequencyBands':spectraNumFrequencyBands,
                                       'spectraDampingFactor':spectraDampingFactor,
                                       'spectraMinimumBandFrequency':spectraMinimumBandFrequency,
                                       'spectraMaximumBandFrequency':spectraMaximumBandFrequency,
                                       'loadAudio':loadAudio,
                                       })
    form.fields['tagSets'] = forms.ModelMultipleChoiceField(TagSet.objects.none(), required=False)
    firstTime = True

  return render(request, 'tools/visualizeAudio.html', {
      'user':user,
      'loadAudio': loadAudio,
      'apacheRoot': settings.APACHE_ROOT,
      'firstTime': firstTime,
      'form': form,
      #'contextInfo':audioFile.staticUserMetaData.contextInfo
      'contextInfo':getMetaDataEntry(audioFile, 'contextInfo'),

      'audioSegmentFilePath': audioSegmentFilePath,
      'imageFilePath': imageFilePath,
      'playerWidth': playerWidth,
      'imageWidth': imageWidth,
      'imageHeight': imageHeight,

      'borderWidth':spectraBorderWidth,
      'topBorderHeight':spectraTopBorderHeight,
      'timeScaleHeight':timeScaleHeight,
      })


## Visualize RandomWindows. Step through a set of randomly
# generated vindows and tag.
# @param request The Django HTTP request object
# @param randomWindowTaggingBiasId The database Id of the random
# window job.
# @param trialIndex Which window are we on.
# @return Django HTTP response object

@login_required
def tagRandomWindow(request, randomWindowTaggingBiasID, trialIndex):

  user = request.user

  logging.info("Tag Random Window: user = " + user.__str__() + " randomWindowTaggingBiasID = " + randomWindowTaggingBiasID + " trialIndex = " + trialIndex)

  job = Jobs.objects.get(id=randomWindowTaggingBiasID)
  randomWindows = RandomWindow.objects.filter(randomWindowTaggingJob=job).order_by("id")
  project = job.project
  userSettings = UserSettings.objects.get(user=user, name='default')

  if not project.userHasPermissions(user, ARLO_PERMS.READ):
    return HttpResponseForbidden("Forbidden")

  # check if the requested index is past the end
  if (int(trialIndex) < 0):
    return HttpResponseRedirect(settings.URL_PREFIX + "/tools/projectFiles/" + str(project.id))

  count = 0
  try:
    # try the new Plugin based job parameters
    count = int(JobParameters.objects.get(job=job,name='count').value)
  except ObjectDoesNotExist:
    # fallback to the old job parameter
    count = int(JobParameters.objects.get(job=job,name='numWindowsToTag').value)
  if (int(trialIndex) >= count):
    return HttpResponseRedirect(settings.URL_PREFIX + "/tools/projectFiles/" + str(project.id))

  randomWindow = randomWindows[int(trialIndex)]

  ##
  # settings

  audioFile = randomWindow.mediaFile
  sampleRate = getMetaDataEntry(audioFile, 'sampleRate')

  ##
  # file paths

  splitPath = audioFile.file.name.split('/')
  audioFileName = splitPath[-1]

  audioSegmentFilePath = os.path.join(nesterSettings.getUsersSegmentCacheDirectory(audioFile), audioFileName)
  audioSegmentFilePath += 's' + randomWindow.startTime.__str__()
  audioSegmentFilePath += 'et' +  randomWindow.endTime.__str__() + '.segment.wav'
  audioSegmentFilePathQuoted = urllib.quote(audioSegmentFilePath)

  logging.info("###### splitPath = " + splitPath.__str__())
  logging.info("###### audioFileName = " + audioFileName.__str__())
  logging.info("###### audioSegmentFilePath = " + audioSegmentFilePath.__str__())

  ##
  # build forms

  spectraForm = VisualizeAudioForm({
    'alias':audioFile.alias,
    'mediaFileId': audioFile.id,
    'startTime' : randomWindow.startTime,
    'endTime' : randomWindow.endTime,
    'gain' : 1.0,
    'spectraNumFramesPerSecond': userSettings.fileViewSpectraNumFramesPerSecond,
    'spectraNumFrequencyBands': userSettings.fileViewSpectraNumFrequencyBands,
    'spectraDampingFactor': userSettings.fileViewSpectraDampingFactor,
    'spectraMinimumBandFrequency': userSettings.fileViewSpectraMinimumBandFrequency,
    'spectraMaximumBandFrequency': userSettings.fileViewSpectraMaximumBandFrequency,
    'loadAudio': userSettings.loadAudio,
    'tagSets': [l.pk for l in project.getTagSets(include_hidden=False)],
    })
  spectraForm.fields['tagSets'] = forms.ModelMultipleChoiceField(project.getTagSets(include_hidden=False), required=False)

  createTagForm = CreateTagForm()
  createTagForm.fields['tagSet'] = forms.ModelChoiceField(project.getTagSets(include_hidden=False))

  nextTrialIndex = int(trialIndex) + 1
  prevTrialIndex = int(trialIndex) - 1

  tagClasses = TagClass.objects.all().filter(project = project)

  return render(request, 'tools/tagRandomWindow.html', {
      'apacheRoot': settings.APACHE_ROOT,
      'project': project,
      'user':user,
      'loadAudio': userSettings.loadAudio,
      'audioSegmentFilePath': audioSegmentFilePathQuoted,
      'audioID': audioFile.id,
      'spectraForm': spectraForm,
      'createTagForm':createTagForm,
      'contextInfo':getMetaDataEntry(audioFile, 'contextInfo'),
      'sampleRate':sampleRate,
      'randomWindowTaggingJob': job,
      'prevTrialIndex':prevTrialIndex,
      'nextTrialIndex':nextTrialIndex,
      'tagClasses': tagClasses,
      })


## Create the Spectra displays for the web pages.
# This functions is called through AJAX and works directly with the
# VisualizeAudioForm to navigate through files. The form and this function
# work together to display, generate, and navigate spectra from a file.
# \note This expects a POST request with a VisualizeAudioForm
# @param request The Django HTTP request object
# @return A JSON dictionary with links to the new spectra and various parameters to update on the web form.

@login_required
def createAudioSpectra(request):
  user = request.user
  logging.info("Creating Audio Spectra: user = " + user.__str__())

  userSettings = UserSettings.objects.get(user=user, name='default')

  if request.method != "POST":
    return HttpResponseNotAllowed(['POST'])

  ###
  # Get Form Data

  form = VisualizeAudioForm(request.POST) # A form bound to the POST data
  # use all() here, as the form will be passed in with some options checked, so we can't use none()
  form.fields['tagSets'] = forms.ModelMultipleChoiceField(TagSet.objects.all(), required=False)

  if not form.is_valid(): # All validation rules pass
    return HttpResponseServerError("Form Invalid")

  data = form.cleaned_data
  audioFile = MediaFile.objects.get(id=data['mediaFileId'])

  ###
  # check perms

  if not audioFile.userHasPermissions(user, ARLO_PERMS.READ):
    return HttpResponseForbidden('Forbidden')

  ###
  # Navigation

  # If the form was submitted with one of the optional Navigation buttons,
  # adjust the spectra details as necessary.

  duration = data['endTime'] - data['startTime']

  try:
    method = form.data['method']
    logging.debug("method: " + method.__str__())
  except:
    method = None

  if (method == 'Forward'):
    data['endTime'] += duration
    data['startTime'] += duration

  if (method == 'Forward Half'):
    data['endTime'] += duration/2
    data['startTime'] += duration/2

  if (method == 'Back'):
    data['endTime'] -= duration
    data['startTime'] -= duration

    if (data['startTime'] < 0):
      offset = - data['startTime']
      data['startTime'] += offset
      data['endTime'] += offset

  if (method == 'Back Half'):
    data['endTime'] -= (duration/2)
    data['startTime'] -= (duration/2)

    if (data['startTime'] < 0):
      offset = - data['startTime']
      data['startTime'] += offset
      data['endTime'] += offset

  if (method == 'Zoom In Time'):
    data['spectraNumFramesPerSecond'] = data['spectraNumFramesPerSecond'] * 2
    data['startTime'] += duration / 4
    data['endTime'] -= duration / 4

  if (method == 'Zoom Out Time'):
    data['spectraNumFramesPerSecond'] = data['spectraNumFramesPerSecond'] / 2
    data['startTime'] -= duration / 2
    data['endTime'] += duration / 2

    if (data['startTime'] < 0):
      offset = - data['startTime']
      data['startTime'] += offset
      data['endTime'] += offset

  if (method == 'Zoom In Freq'):
    data['spectraNumFrequencyBands'] = data['spectraNumFrequencyBands'] * 2

  if (method == 'Zoom Out Freq'):
    data['spectraNumFrequencyBands'] = data['spectraNumFrequencyBands'] / 2

  if (method == 'Increase Damping'):
    data['spectraDampingFactor'] = data['spectraDampingFactor'] * 2

  if (method == 'Decrease Damping'):
    data['spectraDampingFactor'] = data['spectraDampingFactor'] / 2


  ##
  # Spectra Settings

  # Optional Context Window

  contextWindow = form.cleaned_data['contextWindow']
  if contextWindow is None:
    contextWindow = 0.0

  spectraStartTime = form.cleaned_data['startTime'] - contextWindow
  spectraEndTime   = form.cleaned_data['endTime'] + contextWindow

  spectraImageSettings = SpectraImageSettings(**{
    'userSettings': userSettings,
    'audioFile': audioFile,

    'startTime': spectraStartTime,
    'endTime': spectraEndTime,
    'gain': form.cleaned_data['gain'],
    'numFramesPerSecond': form.cleaned_data['spectraNumFramesPerSecond'],
    'numFrequencyBands': form.cleaned_data['spectraNumFrequencyBands'],
    'dampingFactor': form.cleaned_data['spectraDampingFactor'],

    'minFrequency': form.cleaned_data['spectraMinimumBandFrequency'],
    'maxFrequency': form.cleaned_data['spectraMaximumBandFrequency'],

    'catalogSpectraNumFrequencyBands': form.cleaned_data['spectraNumFrequencyBands'],
    })


  ###
  # Generate Spectra

  spectraImage = SpectraImage.generateSpectraImage(spectraImageSettings)
  if spectraImage is None:
    logging.error("createAudioSpectra Unknown Failure While Generating Spectra")
    return HttpResponseServerError("createAudioSpectra Unknown Spectra Generation Failure")

  imageFilePathQuoted = urllib.quote(spectraImage.imageFilePath)
  audioSegmentFilePathQuoted = urllib.quote(spectraImage.audioSegmentFilePath)


  ###
  # Generate Response

  response_dict = {
    'audioSegmentFilePath': audioSegmentFilePathQuoted,
    'imageFilePath': imageFilePathQuoted,
    'playerWidth': spectraImage.imageWidth,
    'imageWidth': spectraImage.imageWidth,
    'imageHeight': spectraImage.imageHeight,

    'spectraStartTime':spectraStartTime,
    'spectraEndTime':spectraEndTime,
    'formStartTime':form.cleaned_data['startTime'],
    'formEndTime':form.cleaned_data['endTime'],
    'startTime': form.cleaned_data['startTime'],
    'endTime': form.cleaned_data['endTime'],
    'contextWindowTime': contextWindow,
    'spectraNumFramesPerSecond':form.cleaned_data['spectraNumFramesPerSecond'],
    'spectraNumFrequencyBands':form.cleaned_data['spectraNumFrequencyBands'],
    'spectraDampingFactor':form.cleaned_data['spectraDampingFactor'],

    'gain' : form.cleaned_data['gain'],
    'minFrequency': form.cleaned_data['spectraMinimumBandFrequency'],
    'maxFrequency': form.cleaned_data['spectraMaximumBandFrequency'],
    'borderWidth': spectraImage.spectraImageSettings.borderWidth,
    'topBorderHeight': spectraImage.spectraImageSettings.topBorderHeight,
    'timeScaleHeight': spectraImage.spectraImageSettings.timeScaleHeight,
    }

  return JsonResponse(response_dict)
