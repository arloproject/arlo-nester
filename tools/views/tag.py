import datetime
import logging

from django.contrib.auth.decorators import login_required
from django.http import HttpResponse, HttpResponseRedirect, HttpResponseServerError, HttpResponseNotAllowed, HttpResponseForbidden, JsonResponse
from django.shortcuts import render
from django import forms
from django.conf import settings

from tools.models import Tag, TagSet, TagClass, Project, MediaFile, VisualizeAudioForm, UserSettings, CreateTagForm, NavigateTagClassForm, NewCreateTagForm
from .create import createTagClass
from .userPermissions import ARLO_PERMS


## Build a dictionary of Tag data from a Tag object.
# @param tag A Tag object
# @return A dictionary of data for the tag.

def tagInfo(tag):
  # try to get the 'real' time of the tag
  realTime = tag.mediaFile.realStartTime

  if realTime is None:
    realTime = ''
  else:
    realTime += datetime.timedelta(seconds=tag.startTime)
    realTime = realTime.strftime('%Y-%m-%d %H:%M:%S')

  results_dict = {
    'id': tag.id,
    'userId':tag.user.id,
    'username':tag.user.username,
    # 'creationDate':tag.creationDate, datetime.datetime structure not JSON-able, which is how we return through AJAX
    # tag class data
    'className':tag.tagClass.className,
    'displayName':tag.tagClass.displayName,
    'projectId': tag.tagClass.project.id,
    # tag example data
    'tagSetId':tag.tagSet.id,
    'mediaFileId':tag.mediaFile.id,
    'mediaFileAlias':tag.mediaFile.alias,
    'startTime': tag.startTime,
    'endTime': tag.endTime,
    'realTime': realTime,
    'minFrequency':tag.minFrequency,
    'maxFrequency':tag.maxFrequency,
    # back to Tag info
    'userTagged':tag.userTagged,
    'machineTagged':tag.machineTagged,
    'strength':tag.strength,
    }

  return results_dict

## Web interface wrapper around tagInfo().
# @param request The Django HTTP request object
# @param tagID The Database ID of the tag to retrieve
# @return A JSON encoded dictionary or the tagInfo() return

@login_required
def getTagInfo(request, tagID):
  # Default return list
  user = request.user
  logging.info("Getting Tag Info: user = " + user.__str__() + " tagID = " + tagID.__str__())

  # TODO permissions checking

  results_dict = {}

  if request.method != 'GET':
    return HttpResponseNotAllowed(['GET'])

  if request.is_ajax():
    try:
      tag = Tag.objects.get(id=tagID)
    except:
      logging.warning("Error Retrieving Tag Information")
      return HttpResponseServerError("Error Retrieving Tag Information")

    results_dict = tagInfo(tag)

  if results_dict is None:
    return HttpResponse()

  return JsonResponse(results_dict)


class ViewTagForm(forms.Form):
  className       = forms.CharField(label='Class', max_length=255, required=False)
  alias           = forms.CharField(label='alias', max_length=255, required=False)
  tagStartTime    = forms.FloatField(label='tagStartTime', required=False)
  tagEndTime      = forms.FloatField(label='tagEndTime', required=False)
  tagMinFrequency = forms.FloatField(label='tagMinFrequency', required=False)
  tagMaxFrequency = forms.FloatField(label='tagMaxFrequency', required=False)
  strength        = forms.FloatField(label='tagStrength', required=False)


## Visualize an individual Tag and its properties.
# GET request views the tag, POST updates the Tag data.
# @param request The Django HTTP Request.
# @param tagID The database ID of the tag to visualize.
# @return Django HTTP response

@login_required
def viewTag(request, tagID):
  user = request.user
  tag = Tag.objects.get(id=tagID)

  logging.info("Viewing Tag: user = " + user.__str__() + " tag = " + tag.__str__())

  userSettings = UserSettings.objects.get(user=user, name='default')

  spectraForm = VisualizeAudioForm({
    'alias':tag.mediaFile.alias,
    'mediaFileId':tag.mediaFile.id,
    'startTime' : tag.startTime,
    'endTime' : tag.endTime,
    'gain' : 1.0,
    'spectraNumFramesPerSecond': userSettings.fileViewSpectraNumFramesPerSecond,
    'spectraNumFrequencyBands': userSettings.fileViewSpectraNumFrequencyBands,
    'spectraDampingFactor': userSettings.fileViewSpectraDampingFactor,
    'spectraMinimumBandFrequency': tag.minFrequency,
    'spectraMaximumBandFrequency': tag.maxFrequency,
    'tagSets': None,
    'loadAudio': userSettings.loadAudio,
    'contextWindow': 0,
    })

  spectraForm.fields['tagSets'] = forms.ModelMultipleChoiceField(tag.tagSet.project.getTagSets(include_hidden=False), required=False)

  if request.method == 'POST':
    # user Perms - write
    if not tag.userHasPermissions(user, ARLO_PERMS.WRITE):  # check user permissions
      return HttpResponseForbidden("Forbidden")

    form = ViewTagForm(request.POST)

    if form.is_valid():
      logging.info("form.is_valid():")

      data = form.cleaned_data

      # TODO change tagClass to ID based lookup instead of name
      # TODO if tagClass doesn't exist, exception happens. Either catch it or create the new class
      newTagClass = TagClass.objects.get(project=tag.tagClass.project, className=data['className'])

      tag.tagClass = newTagClass
      tag.strength = data['strength']
      tag.startTime = data['tagStartTime']
      tag.endTime = data['tagEndTime']
      tag.minFrequency = data['tagMinFrequency']
      tag.maxFrequency = data['tagMaxFrequency']

      tag.save()
    else:
      # form is invalid - return the form, highlighting errors
      errorMsg = "Invalid Form Data"
      for key in form.errors.iterkeys():
        errorMsg = errorMsg + "\n" + str(key) + ": " + str(form.errors[key].as_text())
      return HttpResponse(errorMsg)

  else:
    # GET request, load the form with defaults

    # user Perms - Read
    if not tag.userHasPermissions(user, ARLO_PERMS.READ):  # check user permissions
      return HttpResponseForbidden("Forbidden")

    form = ViewTagForm({
          'className':tag.tagClass.className,
          'alias':tag.mediaFile.alias,
          'tagStartTime':tag.startTime,
          'tagEndTime':tag.endTime,
          'tagMinFrequency':tag.minFrequency,
          'tagMaxFrequency':tag.maxFrequency,
          'strength':tag.strength,
          })

    logging.debug(form.is_valid())

    data = form.cleaned_data

  project = tag.tagSet.project



  ##
  # Build Response

  return render(request, "tools/viewTag.html", {
    'apacheRoot': settings.APACHE_ROOT,
    'project': project,
    'tag':tag,
    'form':form,
    'spectraForm': spectraForm,
    'audioID':tag.mediaFile.id,
    })

## Redirect to a Tag's parent
# @param request The Django HTTP request object
# @param tagID The database ID of the child Tag
# @return Django HTTP Response, redirect to the parent tag.

@login_required
def viewParentTag(request, tagID):
  user = request.user
  logging.info("Viewing ParentTag: user = " + user.__str__() + " childTag: " + tagID.__str__())
  childTag = Tag.objects.get(id=tagID)

  # user perms
  if not childTag.userHasPermissions(user, ARLO_PERMS.READ):  # check user permissions
    return HttpResponseForbidden("Forbidden")

  tag = childTag.parentTag
  if tag is None:
    tag = childTag
  return HttpResponseRedirect(settings.URL_PREFIX + "/tools/viewTag/" + tag.id.__str__())

## Delete a tag.
# @param request The Django HTTP request object
# @param tagID The database ID of the tag to delete
# @return HTTP response object

@login_required
def deleteTag(request, tagID):
  user = request.user
  logging.info("Delete Tag: user = " + user.__str__() + " tagID: " + tagID.__str__())
  tag = Tag.objects.get(id=tagID)

  # user perms
  if not tag.userHasPermissions(user, ARLO_PERMS.WRITE):  # check user permissions
    return HttpResponseForbidden("Forbidden")

  tag.delete()

  return HttpResponse()

# 'Accept' a tag. Mark it as 'userTagged' = True.
#@param request The Django HTTP request object
# @param tagID The database ID of the tag to accept
# @return HTTP response object

@login_required
def acceptTag(request, tagID):
  user = request.user
  logging.info("Accept Tag: user = " + user.__str__() + " tagID: " + tagID.__str__())
  tag = Tag.objects.get(id=tagID)

  # user perms
  if not tag.userHasPermissions(user, ARLO_PERMS.WRITE):  # check user permissions
    return HttpResponseForbidden("Forbidden")

  tag.userTagged = True
  tag.save()

  return HttpResponse()

## Get a list of tags from a media file that are within a search window. i
# Tags need not be fully contained in the window; if any part of the tag
# is within the window it will be returned. Also, tags will be filtered for user
# permissions, i.e., the tags must be part of a project that the user has access to.
# @param request The Django request object
# @param audioID The database ID of the audio file
# @param startTime The start time of the search window. Seconds from the start of the file
# @param endTime The end time of the search window. Seconds from the start of the file
# @param minFrequency The minimum Frequency of the search window (Hz)
# @param maxFrequency The maximum Frequency of the search window (Hz)
# @return JSON encoded list of dictionaries, each dictionary as retruned by tagInfo()

@login_required
def getTagsInAudioSegment(request, audioID, startTime, endTime, minFrequency, maxFrequency):
  user = request.user
  logging.info("Getting Tags In Audio Segment: audioID = " + audioID.__str__())
  logging.info("Start Time = " + startTime.__str__() + " End Time = " + endTime.__str__() + \
  " minFreqency = " + minFrequency.__str__() + " maxFrequency = " + maxFrequency.__str__())

  audioFile = MediaFile.objects.get(id=audioID)

  if not audioFile.userHasPermissions(user, ARLO_PERMS.READ):
    return HttpResponseForbidden("Forbidden")

  results_list_of_dicts = []

#  startTime = float(startTime)
#  endTime = float(endTime)
#  minFrequency = float(minFrequency)
#  maxFrequency = float(maxFrequency)

  tags = Tag.objects.filter(mediaFile__id=audioID).exclude(
                            startTime__gt=endTime).exclude(
                            endTime__lt=startTime).exclude(
                            minFrequency__gt=maxFrequency).exclude(
                            maxFrequency__lt=minFrequency)

  logging.info("Number of Tags before checking perms = " + tags.count().__str__())

  for tag in tags:
    # permissions checking for each individual tag
    if tag.userHasPermissions(user, ARLO_PERMS.READ):
      logging.info("Tags perms good")
      results_list_of_dicts.append(tagInfo(tag))
    else:
      logging.info("Tags perms not so good")

  logging.info("Number of Tags after checking perms = " + str(len(results_list_of_dicts)))

  return JsonResponse(results_list_of_dicts, safe=False) # set !safe to enable the List


## Create a user tag.
# @param request The Django HTTP request. Must be a POST request
# @param projectId Database ID of the Project
# @param mediaFileId
# @return JSON encoded dictionary with 'Success' boolean and 'saveMessage'

@login_required
def createUserTag(request, projectID, mediaFileId):
  user = request.user
  logging.info("Creating Tag: user = " + user.__str__())

  if request.method != 'POST':
    return HttpResponseNotAllowed(['POST'])

  project = Project.objects.get(id=projectID)
  mediaFile = MediaFile.objects.get(id=mediaFileId)

  if not project.userHasPermissions(user, ARLO_PERMS.WRITE):  # check user permissions
    return HttpResponseForbidden("Forbidden")

  ### Verify audio file is in Project
  if not project in mediaFile.project_set.all():
    return HttpResponseForbidden("mediaFile not in Project")

  form = CreateTagForm(request.POST)

  # setup form fields
  logging.info("Before Value: " + form.data["tagSet"].__str__())
  form.fields['tagSet'] = forms.ModelChoiceField(project.getTagSets(include_hidden=True))  # not a display element, so include hidden
  logging.info("After Value: " + form.data["tagSet"].__str__())

  logging.debug("About to validate")
  if not form.is_valid():
    errorMsg = ""
    for key in form.errors.iterkeys():
      errorMsg = errorMsg + "\n" + str(key) + ": " + str(form.errors[key].as_text())
    return JsonResponse({
      'saveMessage':'Tag not Created... Invalid Form Data' + errorMsg,
      'Success':False})

  data = form.cleaned_data
  logging.debug("Validated")

  tagSet = data['tagSet']

  # check that the specified tagSet and mediaFile are in the passed Project
  if (not (tagSet.project.id == project.id)):
    return HttpResponseForbidden("tagSet not in Project")

  if not tagSet.userHasPermissions(user, ARLO_PERMS.WRITE):  # check user permissions
    return HttpResponseForbidden("Forbidden")

  try:
    tagClass = TagClass.objects.get(project=project, className=data['className'])
  except:
    tagClass = createTagClass(user, project, data['className'], data['displayName'])

  logging.debug("Tag Class aquired")

  tag = Tag(
      user=user,
      tagSet=tagSet,
      mediaFile=mediaFile,
      tagClass=tagClass,
      startTime=data['tagStartTime'],
      endTime=data['tagEndTime'],
      minFrequency=data['tagMinFrequency'],
      maxFrequency=data['tagMaxFrequency'],
      randomlyChosen=False,
      machineTagged=False,
      userTagged=True,
      strength = data['strength'])
  tag.save()

  logging.debug("Tag Created")

  return JsonResponse({'Success':True,'saveMessage':'Tag Created!'})


## Navigate through an audio file by jumping to the next, previous, first, or last tag.
# Expects a POST request with a NavigateTagClassForm. Return is a JSON dictionary, on
# success, {'success', 'tagID', 'startTime', 'endTime', 'userTagged', 'machineTagged'}. On
# error, {'success', 'errorMessage'}
# @param request the Django HTTP request.
# @param audioID The database id of the AudioFile
# @return JSON dictionary.

@login_required
def navigateTags(request, projectID, audioID):
  user = request.user
  logging.info("Navigating Tags: user = " + user.__str__())

  if request.method != 'POST':
    return HttpResponseNotAllowed(['POST'])

  # TODO user perms

  project = Project.objects.get(id=projectID)

  navigateForm = NavigateTagClassForm(request.POST)

  response_dict = {}

  if not navigateForm.is_valid():
    errorMsg = ""
    for key in navigateForm.errors.iterkeys():
      errorMsg = errorMsg + "\n" + str(key) + ": " + str(navigateForm.errors[key].as_text())
    return JsonResponse({
      'errorMessage':'Invalid Form Data' + errorMsg,
      'Success':False})


  data = navigateForm.cleaned_data

  ##
  # form settings
  method = navigateForm.data['method']
  startTime = data['navStartTime']
  endTime = data['navEndTime']

  # get candidate tag list
  if data['navigateTagClass'] != None:
    tags = Tag.objects.filter(mediaFile__id=audioID,
                              tagClass__project=project,
                              tagClass=data['navigateTagClass']).order_by('startTime')
  else:
    tags = Tag.objects.filter(mediaFile__id=audioID,
                              tagClass__project=project).order_by('startTime')

  if (tags.count() == 0):
    errorMsg = ""
    return JsonResponse({
      'errorMessage':'No Tags Found',
      'Success':False})


  # current tag
  try:
    origTag = Tag.objects.get(id=data['currentTagID'])
  except:
    origTag = tags[0]

  origTagStartTime = origTag.startTime

  if method == 'First':
    try:
      tag = tags[0]
    except:
      tag = origTag
  elif method == 'Last':
    newTags = tags.order_by('-startTime')
    try:
      tag = newTags[0]
    except:
      tag = origTag
  elif method == 'Next':
    newTags = tags.filter(startTime__gt=origTagStartTime).order_by('startTime')
    try:
      tag = newTags[0]
    except:
      tag = origTag
  elif method == 'Previous':
    newTags = tags.filter(startTime__lt=origTag.le.startTime).order_by('-startTime')
    try:
      tag = newTags[0]
    except:
      tag = origTag

  timeChange = tag.startTime - origTag.startTime

  if origTag.startTime >= float(startTime) and origTag.startTime <= float(endTime):
    newStartTime = float(startTime) + timeChange
    newEndTime = float(endTime) + timeChange
    duration = newEndTime - newStartTime
  else:
    duration = endTime - startTime
    timeLeft = duration - (tag.endTime - tag.startTime)
    newStartTime = tag.startTime - timeLeft / 2
    newEndTime = tag.endTime + timeLeft / 2

  if newStartTime < 0:
    newEndTime = duration
    newStartTime = 0

  response_dict = {'success':True,
                   'tagID':tag.id,
                   'startTime':newStartTime,
                   'endTime':newEndTime,
                   'userTagged': tag.userTagged,
                   'machineTagged': tag.machineTagged}

  return JsonResponse(response_dict)


# NEW createUserTag for use with MultiView
# difference is that audioFileID is passed in POST rather than URL
# probably eventually migrate all uses from existing createUserTag to this new usage
# @param request the Django HTTP request.
# @param projectID The database ID of the Project
# @return JSON dictionary.

@login_required
def newCreateUserTag(request, projectId):
  user = request.user
  project = Project.objects.get(id=projectId)

  logging.info("Creating Tag: user = " + user.__str__() +
    " projectId = " + str(projectId))

  # Perms Checking
  if not project.userHasPermissions(user, ARLO_PERMS.WRITE):  # check user permissions
    return HttpResponseForbidden("Forbidden")

  form = NewCreateTagForm(request.POST)
  form.fields['tagSet'] = forms.ModelChoiceField(project.getTagSets(include_hidden=True))  # not a display element, so include hidden

  logging.debug("About to validate")
  if form.is_valid():
    data = form.cleaned_data
    logging.debug("Validated")

    mediaFile = MediaFile.objects.get(id=data['audioFileId'])
    tagSet = data['tagSet']

    # Perms Checking
    if not tagSet.userHasPermissions(user, ARLO_PERMS.WRITE):  # check user permissions
      return HttpResponseForbidden("Forbidden")

    # check that the specified tagSet and mediaFile are in the passed Project
    if not (tagSet.project.id == project.id):
      return HttpResponseForbidden("tagSet not in project")
    if not project in mediaFile.project_set.all():
      return HttpResponseForbidden("mediaFile not in project")

    logging.info("" +
     "\n\taudioFile: " + mediaFile.__str__() +
     "\n\tdata['tagStartTime']: " + data['tagStartTime'].__str__() +
     "\n\tdata['tagEndTime']: " + data['tagEndTime'].__str__() +
     "\n\tdata['tagMinFrequency']: " + data['tagMinFrequency'].__str__() +
     "\n\tdata['tagMaxFrequency']: " + data['tagMaxFrequency'].__str__() + "\n")

    try:
      tagClass = TagClass.objects.get(project=project, className=data['className'])
    except:
      tagClass = createTagClass(user, project, data['className'], data['displayName'])

    logging.debug("Tag Class aquired")

    tag = Tag(
        user=user,
        tagClass=tagClass,
        mediaFile=mediaFile,
        startTime=data['tagStartTime'],
        endTime=data['tagEndTime'],
        minFrequency=data['tagMinFrequency'],
        maxFrequency=data['tagMaxFrequency'],
        randomlyChosen=False,
        machineTagged=False,
        userTagged=True,
        strength = data['strength'])

    tag.save()


    logging.debug("Tag Created")

    return JsonResponse({'Success':True,'saveMessage':'Tag Created!'})

  else:
    logging.error("CreateTagForm failed validation")
    #for k, v in form.errors.items():
    logging.error(dict((k, map(unicode, v)) for (k,v) in form.errors.iteritems() ))
    return JsonResponse({'Success':False,'saveMessage':'Tag not Created...'})


## Adjust the data of an existing tag.
# Must be a POST request.
# @note NOTE: Currently this adjusts only the time / frequency, though it
# uses the NewCreateTagForm, which contains more data.
# @param request the Django HTTP request.
# @param tagID The database ID of the tag to adjust
# @return JSON dictionary indicating success / fail.

@login_required
def adjustTag(request, tagId):
  user = request.user
  logging.info("Adjusting Tag: user = " + user.__str__() +
    " tag = " + str(tagId))

  tag = Tag.objects.get(id=tagId)

  # Check User Permissions
  if not tag.userHasPermissions(user, ARLO_PERMS.WRITE):
    return HttpResponseForbidden("Forbidden")

  if request.method != 'POST':
    return HttpResponseNotAllowed(['POST'])

  form = NewCreateTagForm(request.POST)

  # Not using these fields
  form['className'].field.required = False
  form['displayName'].field.required = False
  form['strength'].field.required = False
  form['audioFileId'].field.required = False
  form['tagSet'].field.required = False

  if not form.is_valid():
    logging.error("CreateTagForm failed validation in adjustTag()")
    #for k, v in form.errors.items():
    logging.error(dict((k, map(unicode, v)) for (k,v) in form.errors.iteritems() ))
    return JsonResponse({'saveMessage':'Tag not Saved - Invalid Form'})


  data = form.cleaned_data
  logging.debug("Validated")

  tag.startTime = data['tagStartTime']
  tag.endTime = data['tagEndTime']
  tag.minFrequency = data['tagMinFrequency']
  tag.maxFrequency = data['tagMaxFrequency']
  tag.save()

  logging.debug("Tag Updated")

  return JsonResponse({'saveMessage':'Tag Updated!'})


class DeleteTagsForm(forms.Form):
  deleteUserTags = forms.BooleanField(label='Delete User Tags', required=False, initial=False)
  deleteMachineTags = forms.BooleanField(label='Delete Machine Tags', required=False, initial=False)

  tagSets    = forms.ModelMultipleChoiceField(TagSet.objects.none(), widget=forms.CheckboxSelectMultiple, required=True)
  tagClasses = forms.ModelMultipleChoiceField(TagClass.objects.none(), widget=forms.CheckboxSelectMultiple, required=True)


## Web interface to delete tags. A GET will will display the
# DeleteTagsForm, and the subsequent POST will perform the actual delete.
# This presents a form that allows the user to filter tags to
# deleted.
# @param request The Django HTTP request object
# @param projectId The database ID of the Project.
# @return Django HTTP response object

@login_required
def deleteTags(request, projectId):
  user = request.user
  project = Project.objects.get(id=projectId)

  # Perms Checking
  if not project.userHasPermissions(user, ARLO_PERMS.READ):  # check user permissions
    return HttpResponseForbidden("Forbidden")

  if request.method == 'GET':
    if ('tagSets' in request.GET) or ('tagClasses' in request.GET):
      newdata = request.GET.copy()

      if not ('tagClasses' in request.GET):
        newdata.setlist('tagClasses', [tc.pk for tc in project.getTagClasses()])
      if not ('tagSets' in request.GET):
        newdata.setlist('tagSets', [l.pk for l in project.getTagSets(include_hidden=False)])

      form = DeleteTagsForm(newdata)
    else:
      form = DeleteTagsForm()

    form.fields['tagSets'] = forms.ModelMultipleChoiceField(project.getTagSets(include_hidden=False), required=True)
    form.fields['tagClasses'] = forms.ModelMultipleChoiceField(project.getTagClasses(), required=True)

    return render(request, 'tools/deleteTags.html', {
      'apacheRoot':settings.APACHE_ROOT,
      'user':user,
      'project': project,
      'form':form,
      })
  elif request.method == 'POST':
    form = DeleteTagsForm(request.POST)

    form.fields['tagSets'] = forms.ModelMultipleChoiceField(project.getTagSets(include_hidden=False), required=True)
    form.fields['tagClasses'] = forms.ModelMultipleChoiceField(project.getTagClasses(), required=True)

    if not form.is_valid():
      return render(request, 'tools/deleteTags.html', {
        'apacheRoot':settings.APACHE_ROOT,
        'user':user,
        'project': project,
        'form':form,
        })

    data = form.cleaned_data

    # Perms Checking - Ensure each TagSet has write perms
    for tagSet in data['tagSets']:
      if not tagSet.userHasPermissions(user, ARLO_PERMS.WRITE):
        return HttpResponseForbidden("Forbidden to TagSet {}".format(tagSet))

    logging.info("Deleting Tags: projectId: " + project.id.__str__())

    tags = getDeleteTagList(data)

    # update parentTag references to any of the tags about to be deleted
    # NOTE the use of the list in the subquery below. Trying to tos just
    # __=tags results in a MySQL subquery error. See the following:
    # https://docs.djangoproject.com/en/dev/ref/models/querysets/  ('in')
    # http://stackoverflow.com/questions/45494/mysql-error-1093-cant-specify-target-table-for-update-in-from-clause
    # ^ (for an example of what we were encountering)
    Tag.objects.filter(parentTag__in=list(tags.values_list('pk', flat=True))).update(parentTag=None)

    # delete the tags
    logging.info("Deleting Tags Count: " + str(tags.count()))
    tags.delete()

    return HttpResponseRedirect(settings.URL_PREFIX + "/tools/projectFiles/" + project.id.__str__())

  else:
    return HttpResponseNotAllowed(['GET', 'POST'])


## Given data from a DeleteTagsForm, generate the list of Tags that will
# be deleted.
# @param formData Data from a DeleteTagsForm (form.cleaned_data)
# @return A QuerySet of Tag objects

def getDeleteTagList(formData):
  try:
    deleteUserTags = formData['deleteUserTags']
  except:
    deleteUserTags = False

  try:
    deleteMachineTags = formData['deleteMachineTags']
  except:
    deleteMachineTags = False

  selectedTagSets = formData['tagSets']
  selectedTagClasses = formData['tagClasses']

  # filter by tagSets and classes
  tags = Tag.objects.filter(tagClass__in=selectedTagClasses, tagSet__in=selectedTagSets)
  if deleteUserTags and deleteMachineTags:
    pass  # allow all tags
  elif (not deleteUserTags) and deleteMachineTags:
    tags = tags.filter(userTagged=False,machineTagged=True)
  elif deleteUserTags and (not deleteMachineTags):
    tags = tags.filter(userTagged=True,machineTagged=False)
  else:
    # this *should* be empty, but you never know
    tags = tags.filter(userTagged=False,machineTagged=False)

  return tags


## AJAX call used to count the number of tags that a delete operation
# will remove. Must be a GET request containing DeleteTagsForm data.
# @param request The Django HTTP request object
# @param projectId The database ID of the Project.
# @return A JSON encoded dictionary with the tag count {'count'}

@login_required
def getDeleteTagCount(request, projectId):
  user = request.user
  project = Project.objects.get(id=projectId)

  if request.method != 'GET':
    return HttpResponseNotAllowed(['GET'])

  # Perms Checking
  if not project.userHasPermissions(user, ARLO_PERMS.READ):  # check user permissions
    return HttpResponseForbidden("Forbidden")

  form = DeleteTagsForm(request.GET)
  form.fields['tagSets'] = forms.ModelMultipleChoiceField(project.getTagSets(include_hidden=False), required=True)
  form.fields['tagClasses'] = forms.ModelMultipleChoiceField(project.getTagClasses(), required=True)

  if not form.is_valid():
    return JsonResponse({ 'status': "Invalid Form" })

  data = form.cleaned_data

  # Perms Checking - Ensure each TagSet has write perms
  for tagSet in data['tagSets']:
    if not tagSet.userHasPermissions(user, ARLO_PERMS.READ):
      return HttpResponseForbidden("Forbidden to TagSet {}".format(tagSet))

  tags = getDeleteTagList(data)

  return JsonResponse({ 'count':tags.count() })
