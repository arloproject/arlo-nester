import logging

from django.contrib.auth.decorators import login_required
from django.shortcuts import render
from django.core.exceptions import ObjectDoesNotExist
from django import forms
from django.conf import settings

from tools.models import UserSettings, ExtendedUserSettings, ExtendedUserSettingsValues


### Get the value of an ExtendedUserSetting from a UserSettings object.
#
# Looks for an ExtendedUserSetting for the specified UserSettings object - if
# one is not found, return the default value of the ExtendedUserSetting.
#
# @param name
#        Name of the ExtendedUserSetting we are retrieving.
# @param userSettings
#        UserSettings object containing the ExtendedUserSetting for which
#        we are searching.
# @return
#        String value of the ExtendedUserSetting, or the default if none
#        exists for this UserSettings Object. (Note: None Object is a possible
#        and valid response.)

def getExtendedUserSetting(name, userSettings):
  extendedUserSetting = ExtendedUserSettings.objects.get(name=name)
  try:
    return ExtendedUserSettingsValues.objects.get(userSettings=userSettings, setting=extendedUserSetting).value
  except ObjectDoesNotExist:
    return extendedUserSetting.default

### Save an ExtendedUserSetting for a UserSetting object.
#
# This will update an existing, or add a new, ExtendedUserSetting for a
# UserSetting object. Note that the 'name' of the setting must be an existing
# ExtendedUserSetting.
#
# @param name
#        Name of the ExtendedUserSetting we are setting.
# @param userSettings
#        UserSettings object containing the ExtendedUserSetting for which
#        we are searching.
# @param value
#        The value to set for this setting.

def saveExtendedUserSetting(name, userSettings, value):
  extendedUserSetting = ExtendedUserSettings.objects.get(name=name)
  try:
    eusv = ExtendedUserSettingsValues.objects.get(userSettings=userSettings, setting=extendedUserSetting)
    eusv.value = value
    eusv.save()
  except ObjectDoesNotExist:
    # no prior, create a new one
    eusv = ExtendedUserSettingsValues(
      userSettings = userSettings,
      setting = extendedUserSetting,
      value = value)
    eusv.save()




SPECTRA_PITCHTRACE_TYPE_CHOICES = (
  ("None",        "None"),
  ("Fundamental", "Fundamental"),
  ("MaxEnergy",   "Max Energy"),
)

SPECTRA_PITCHTRACE_COLOR_CHOICES = (
  ("Black", "Black"),
  ("White", "White"),
)

class UserSettingsForm(forms.Form):

  name = forms.CharField(label="Settings Name", max_length=255)

  windowSizeInSeconds = forms.FloatField(label='Window Size In Seconds', initial=4.0)
  spectraMinimumBandFrequency = forms.FloatField(label='Spectra Minimum Band Frequency', initial=40.0)
  spectraMaximumBandFrequency = forms.FloatField(label='Spectra Maximum Band Frequency', initial=8000.0)
  spectraDampingFactor = forms.FloatField(label='Spectra Damping Factor', initial=0.02)
  spectraNumFrequencyBands = forms.IntegerField(label='Spectra Number of Frequency Bands', initial=256)
  spectraNumFramesPerSecond = forms.FloatField(label='Spectra Number of Frames Per Second', initial=128)


  showSpectra = forms.BooleanField(label='Show Spectra?', required=False, initial=True)
  showWaveform = forms.BooleanField(label='Show Waveform?', required=False, initial=True)


  loadAudio = forms.BooleanField(label='Load Audio?', required=False, initial=True)

  maxNumAudioFilesToList = forms.IntegerField(label='Max Number of Media Files To List', initial=100)

  # for file view
  fileViewWindowSizeInSeconds = forms.FloatField(label='File View Window Size In Seconds', initial=4.0)
  fileViewSpectraMinimumBandFrequency = forms.FloatField(label='File View Spectra Minimum Band Frequency', initial=20.0)
  fileViewSpectraMaximumBandFrequency = forms.FloatField(label='File View Spectra Maximum Band Frequency', initial=20000.0)
  fileViewSpectraDampingFactor = forms.FloatField(label='File View Spectra Damping Factor', initial=0.02)
  fileViewSpectraNumFrequencyBands = forms.IntegerField(label='File View Spectra Number of Frequency Bands', initial=256)
  fileViewSpectraNumFramesPerSecond = forms.FloatField(label='File View Spectra Number of Frames Per Second', initial=128)

  # for catalog view
  catalogViewWindowSizeInSeconds = forms.FloatField(label='Catalog View Window Size In Seconds', initial=4.0)
  catalogViewSpectraMinimumBandFrequency = forms.FloatField(label='Catalog View Spectra Minimum Band Frequency', initial=20.0)
  catalogViewSpectraMaximumBandFrequency = forms.FloatField(label='Catalog View Spectra Maximum Band Frequency', initial=20000.0)
  catalogViewSpectraDampingFactor = forms.FloatField(label='Catalog View Spectra Damping Factor', initial=0.02)
  catalogViewSpectraNumFrequencyBands = forms.IntegerField(label='Catalog View Spectra Number of Frequency Bands', initial=256)
  catalogViewSpectraNumFramesPerSecond = forms.FloatField(label='Catalog View Spectra Number of Frames Per Second', initial=128)

  # for tag view
  tagViewWindowSizeInSeconds = forms.FloatField(label='Tag View Window Size In Seconds', initial=4.0)
  tagViewSpectraMinimumBandFrequency = forms.FloatField(label='Tag View Spectra Minimum Band Frequency', initial=20.0)
  tagViewSpectraMaximumBandFrequency = forms.FloatField(label='Tag View Spectra Maximum Band Frequency', initial=20000.0)
  tagViewSpectraDampingFactor = forms.FloatField(label='Tag View Spectra Damping Factor', initial=0.02)
  tagViewSpectraNumFrequencyBands = forms.IntegerField(label='Tag View Spectra Number of Frequency Bands', initial=256)
  tagViewSpectraNumFramesPerSecond = forms.FloatField(label='Tag View Spectra Number of Frames Per Second', initial=128)


  ###
  # Extended Settings

  # For PitchTrace

  spectraPitchTraceNumFramesPerSecond = forms.IntegerField(label='Num Time Frames per Second', initial=200)
  spectraPitchTraceNumFrequencyBands = forms.IntegerField(label='Number of Frequency Bands', initial=256)
  spectraPitchTraceDampingFactor = forms.FloatField(label='Damping Factor', initial=0.01)
  spectraPitchTraceMinimumBandFrequency = forms.FloatField(label='Spectra Minimum Frequency', initial=40.0)
  spectraPitchTraceMaximumBandFrequency = forms.FloatField(label='Spectra Maximum Frequency', initial=8000.0)

  spectraPitchTraceType             = forms.ChoiceField(widget=forms.Select,label="Show Pitch Trace", required=True, choices=SPECTRA_PITCHTRACE_TYPE_CHOICES, initial="None")
  spectraPitchTraceNumSamplePoints  = forms.IntegerField(label='Number of Sample Points', initial=1000)
  spectraPitchTraceColor            = forms.ChoiceField(widget=forms.Select,label="Color", required=True, choices=SPECTRA_PITCHTRACE_COLOR_CHOICES, initial="Black")
  spectraPitchTraceWidth            = forms.IntegerField(label='Size (Width in Pixels)', initial=1)
  spectraPitchTraceMinCorrelation   = forms.FloatField(label='Mininum Correlation Threshold (-1 to 1)', initial=0)
  spectraPitchTraceEntropyThreshold = forms.FloatField(label='Entropy Threshold', initial=0)
  spectraPitchTraceStartFreq        = forms.FloatField(label='Start Freuqency for Analysis', initial=0)
  spectraPitchTraceEndFreq          = forms.FloatField(label='End Frequency for Analysis', initial=0)
  spectraPitchTraceMinEnergyThreshold = forms.FloatField(label='Minimum Energy Threshold', initial=0)
  spectraPitchTraceTolerance          = forms.FloatField(label='Frequency Band Tolerance', initial=0.15)
  spectraPitchTraceInverseFreqWeight  = forms.FloatField(label='Inverse Frequency Weighting Power', initial=0)

  spectraPitchTraceMaxPathLengthPerTransition = forms.FloatField(label='Max Path Length Per Transition', initial=4)
  spectraPitchTraceWindowSize = forms.IntegerField(label='Window Size', initial=13)
  spectraPitchTraceExtendRangeFactor = forms.FloatField(label='Extended Range Factor', initial=2)

extended_settings_keys = [
  # Pitch Trace Spectra
  'spectraPitchTraceNumFramesPerSecond',
  'spectraPitchTraceNumFrequencyBands',
  'spectraPitchTraceDampingFactor',
  'spectraPitchTraceMinimumBandFrequency',
  'spectraPitchTraceMaximumBandFrequency',
  # Pitch Trace Settings
  'spectraPitchTraceType',
  'spectraPitchTraceNumSamplePoints',
  'spectraPitchTraceColor',
  'spectraPitchTraceWidth',
  'spectraPitchTraceMinCorrelation',
  'spectraPitchTraceEntropyThreshold',
  'spectraPitchTraceStartFreq',
  'spectraPitchTraceEndFreq',
  'spectraPitchTraceMinEnergyThreshold',
  'spectraPitchTraceTolerance',
  'spectraPitchTraceInverseFreqWeight',
  'spectraPitchTraceMaxPathLengthPerTransition',
  'spectraPitchTraceWindowSize',
  'spectraPitchTraceExtendRangeFactor',
  ]


## Display a list of UserSettings entries.
# @param request The Django HTTP request object
# @return Django HTTP response object

@login_required
def userSettings(request):
  user = request.user
  logging.info("Retrieving User Settings: user = " + user.__str__())

  try:
    userSettings = UserSettings.objects.filter(user=user).order_by('name')
  except:
    userSettings = []

  return render(request, 'tools/userSettings.html', {
     'settingsSet': userSettings,
     'user':user,
     'apacheRoot':settings.APACHE_ROOT,
     })


#@login_required
#def createSettings(request):
#  user = request.user
#  logging.info("Creating User Settings: user = " + user.__str__())
#
#  if request.method == 'POST': # If the form has been submitted...
#
#    form = UserSettingsForm(request.POST) # A form bound to the POST data
#
#    if form.is_valid():
#      data = form.cleaned_data
#
#      oldUserSettings = UserSettings.objects.filter(user=user,name=data['name'])
#
#      if oldUserSettings.count() > 0:
#        saveMessage = 'Duplicate -- Settings not Saved!'
#      else:
#        userSettings = UserSettings(
#          user=user,
#          name=data['name'],
#          windowSizeInSeconds=data['windowSizeInSeconds'],
#          spectraMinimumBandFrequency=data['spectraMinimumBandFrequency'],
#          spectraMaximumBandFrequency=data['spectraMaximumBandFrequency'],
#          spectraDampingFactor=data['spectraDampingFactor'],
#          spectraNumFrequencyBands=data['spectraNumFrequencyBands'],
#          spectraNumFramesPerSecond=data['spectraNumFramesPerSecond'])
#
#        try:
#          userSettings.save()
#          saveMessage = 'Settings Saved!'
#        except:
#          saveMessage = 'Settings Not Saved'
#          sys.stdout.flush()
#
#      return render_to_response('tools/createSettings.html',
#        {
#        'projects':Project.objects.filter(user=user),
#        'user':user,
#        'apacheRoot':settings.APACHE_ROOT,
#        'saveMessage':saveMessage,
#        'form':form})
#    else:
#      return render_to_response('tools/createSettings.html',
#        {
#        'projects':Project.objects.filter(user=user),
#        'user':user,
#        'apacheRoot':settings.APACHE_ROOT,
#        'form':form})
#  else:
#    userSettings = UserSettings.objects.get(user=user,name='default')
#    form = UserSettingsForm(initial=
#      {
#      'name':'',
#      'windowSizeInSeconds':userSettings.windowSizeInSeconds,
#      'spectraMinimumBandFrequency':userSettings.spectraMinimumBandFrequency,
#      'spectraMaximumBandFrequency':userSettings.spectraMaximumBandFrequency,
#      'spectraDampingFactor':userSettings.spectraDampingFactor,
#      'spectraNumFrequencyBands':userSettings.spectraNumFrequencyBands,
#      'spectraNumFramesPerSecond':userSettings.spectraNumFramesPerSecond})
#
#    return render_to_response('tools/createSettings.html',
#      {
#      'projects':Project.objects.filter(user=user),
#      'user':user,
#      'apacheRoot':settings.APACHE_ROOT,
#      'saveMessage':'',
#      'form':form})


## Display a UserSettings object, and edit.
# GET shows form, POST saves changes.
# @param request The Django HTTP request object
# @return Django HTTP response object

@login_required
def viewSettings(request, settingsName):
  user = request.user
  logging.info("Viewing Settings: user = " + user.__str__() + " settingsName = " + settingsName.__str__())

  if request.method == 'POST': # If the form has been submitted...

    form = UserSettingsForm(request.POST) # A form bound to the POST data

    if form.is_valid():

      userSettings = UserSettings.objects.get(user=user, name=settingsName)
      data = form.cleaned_data

      userSettings.windowSizeInSeconds = data['windowSizeInSeconds']
      userSettings.spectraMinimumBandFrequency = data['spectraMinimumBandFrequency']
      userSettings.spectraMaximumBandFrequency = data['spectraMaximumBandFrequency']
      userSettings.spectraDampingFactor = data['spectraDampingFactor']
      userSettings.spectraNumFrequencyBands = data['spectraNumFrequencyBands']
      userSettings.spectraNumFramesPerSecond = data['spectraNumFramesPerSecond']

      userSettings.fileViewWindowSizeInSeconds = data['fileViewWindowSizeInSeconds']
      userSettings.fileViewSpectraMinimumBandFrequency = data['fileViewSpectraMinimumBandFrequency']
      userSettings.fileViewSpectraMaximumBandFrequency = data['fileViewSpectraMaximumBandFrequency']
      userSettings.fileViewSpectraDampingFactor = data['fileViewSpectraDampingFactor']
      userSettings.fileViewSpectraNumFrequencyBands = data['fileViewSpectraNumFrequencyBands']
      userSettings.fileViewSpectraNumFramesPerSecond = data['fileViewSpectraNumFramesPerSecond']

      userSettings.catalogViewWindowSizeInSeconds = data['catalogViewWindowSizeInSeconds']
      userSettings.catalogViewSpectraMinimumBandFrequency = data['catalogViewSpectraMinimumBandFrequency']
      userSettings.catalogViewSpectraMaximumBandFrequency = data['catalogViewSpectraMaximumBandFrequency']
      userSettings.catalogViewSpectraDampingFactor = data['catalogViewSpectraDampingFactor']
      userSettings.catalogViewSpectraNumFrequencyBands = data['catalogViewSpectraNumFrequencyBands']
      userSettings.catalogViewSpectraNumFramesPerSecond = data['catalogViewSpectraNumFramesPerSecond']

      userSettings.tagViewWindowSizeInSeconds = data['tagViewWindowSizeInSeconds']
      userSettings.tagViewSpectraMinimumBandFrequency = data['tagViewSpectraMinimumBandFrequency']
      userSettings.tagViewSpectraMaximumBandFrequency = data['tagViewSpectraMaximumBandFrequency']
      userSettings.tagViewSpectraDampingFactor = data['tagViewSpectraDampingFactor']
      userSettings.tagViewSpectraNumFrequencyBands = data['tagViewSpectraNumFrequencyBands']
      userSettings.tagViewSpectraNumFramesPerSecond = data['tagViewSpectraNumFramesPerSecond']

      if data['showSpectra']:
        userSettings.showSpectra = True
      else:
        userSettings.showSpectra = False
      if data['showWaveform']:
        userSettings.showWaveform = True
      else:
        userSettings.showWaveform = False
      if data['loadAudio']:
        userSettings.loadAudio = True
      else:
        userSettings.loadAudio = False

      userSettings.maxNumAudioFilesToList = data['maxNumAudioFilesToList']

      ###
      # ExtendedUserSettings

      for k in extended_settings_keys:
        saveExtendedUserSetting(k, userSettings, data[k])

      try:
        userSettings.save()
        saveMessage = 'Settings Updated!'
      except:
        saveMessage = 'Settings Not Updated.'

      return render(request, 'tools/viewSettings.html', {
        'apacheRoot':settings.APACHE_ROOT,
        'saveMessage':saveMessage,
        'user':user,
        'form': form,
        })
    else:
      return render(request, 'tools/viewSettings.html', {
        'user':user,
        'apacheRoot':settings.APACHE_ROOT,
        'form': form,
        })
  else: # GET
    userSettings = UserSettings.objects.get(user=user, name=settingsName)

    initial_dict = {
      'name':userSettings.name,

      'windowSizeInSeconds':userSettings.windowSizeInSeconds,
      'spectraMinimumBandFrequency':userSettings.spectraMinimumBandFrequency,
      'spectraMaximumBandFrequency':userSettings.spectraMaximumBandFrequency,
      'spectraDampingFactor':userSettings.spectraDampingFactor,
      'spectraNumFrequencyBands':userSettings.spectraNumFrequencyBands,
      'spectraNumFramesPerSecond':userSettings.spectraNumFramesPerSecond,

      'fileViewWindowSizeInSeconds':userSettings.fileViewWindowSizeInSeconds,
      'fileViewSpectraMinimumBandFrequency':userSettings.fileViewSpectraMinimumBandFrequency,
      'fileViewSpectraMaximumBandFrequency':userSettings.fileViewSpectraMaximumBandFrequency,
      'fileViewSpectraDampingFactor':userSettings.fileViewSpectraDampingFactor,
      'fileViewSpectraNumFrequencyBands':userSettings.fileViewSpectraNumFrequencyBands,
      'fileViewSpectraNumFramesPerSecond':userSettings.fileViewSpectraNumFramesPerSecond,

      'catalogViewWindowSizeInSeconds':userSettings.catalogViewWindowSizeInSeconds,
      'catalogViewSpectraMinimumBandFrequency':userSettings.catalogViewSpectraMinimumBandFrequency,
      'catalogViewSpectraMaximumBandFrequency':userSettings.catalogViewSpectraMaximumBandFrequency,
      'catalogViewSpectraDampingFactor':userSettings.catalogViewSpectraDampingFactor,
      'catalogViewSpectraNumFrequencyBands':userSettings.catalogViewSpectraNumFrequencyBands,
      'catalogViewSpectraNumFramesPerSecond':userSettings.catalogViewSpectraNumFramesPerSecond,

      'tagViewWindowSizeInSeconds':userSettings.tagViewWindowSizeInSeconds,
      'tagViewSpectraMinimumBandFrequency':userSettings.tagViewSpectraMinimumBandFrequency,
      'tagViewSpectraMaximumBandFrequency':userSettings.tagViewSpectraMaximumBandFrequency,
      'tagViewSpectraDampingFactor':userSettings.tagViewSpectraDampingFactor,
      'tagViewSpectraNumFrequencyBands':userSettings.tagViewSpectraNumFrequencyBands,
      'tagViewSpectraNumFramesPerSecond':userSettings.tagViewSpectraNumFramesPerSecond,

      'showSpectra':userSettings.showSpectra,
      'showWaveform':userSettings.showWaveform,
      'loadAudio':userSettings.loadAudio,
      'maxNumAudioFilesToList':userSettings.maxNumAudioFilesToList,
      }

    ###
    # ExtendedUserSettings

    for k in extended_settings_keys:
      initial_dict[k] = getExtendedUserSetting(k, userSettings)

    form = UserSettingsForm(initial=initial_dict)

    return render(request, 'tools/viewSettings.html', {
        'user':user,
        'apacheRoot':settings.APACHE_ROOT,
        'form': form,
        })


## Delete a user's UserSettings object.
# @note Won't delete the object named 'default'
# @param request The Django HTTP request object
# @param settingsName The name of the settings object to delete.
# @return Django HTTP response object (userSettings())

@login_required
def deleteSettings(request, settingsName):
  user = request.user
  logging.info("Deleting User Settings: user = " + user.__str__() + " settingsName = " + settingsName.__str__())

  if(settingsName != 'default'):
    UserSettings.objects.filter(user=user, name=settingsName).delete()

  return userSettings(request)
