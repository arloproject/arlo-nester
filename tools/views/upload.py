import os
import datetime
import logging
import uuid
import hashlib

from django.contrib.auth.decorators import login_required
from django.http import HttpResponseRedirect, HttpResponseForbidden, HttpResponseServerError
from django.shortcuts import render
from django import forms
from django.conf import settings

import nesterSettings as nesterSettings
import tools.Meandre as Meandre
from tools.models import Project, Library, Jobs, JobTypes, JobParameters, JobStatusTypes, MediaFile, MediaTypes
from .userPermissions import ARLO_PERMS
from .viewMetaData import addOrUpdateMediaFileMetaData


## Django Form for Uploading MediaFiles

class UploadAudioFileForm(forms.Form):

  alias = forms.CharField(max_length=255, required=True)

  project  = forms.ModelChoiceField(Project.objects.none(), empty_label=None, required=True)
  library = forms.ModelChoiceField(Library.objects.none(), empty_label=None, required=True)

  file = forms.FileField(label='File Name')

  name = forms.CharField(max_length=255, required=False)
  startTime = forms.DateTimeField(required=False, label="Start Time")
  contextInfo = forms.CharField(widget=forms.Textarea, required=False, label="Context Info")


## Upload an audio file via web form.
# GET displays form, POST uploads file
# @param request The Django HTTP request object
# @return Django HTTP response object (redirect to audioFiles/ after upload,
# or display this upload form)

@login_required
def uploadAudioFile(request):
  user = request.user
  logging.info("Uploading Audio File : user = " + user.__str__())

  defaultLibrary = request.GET.get('library', '')
  defaultProject = request.GET.get('project', '')

  if request.method == 'POST':
    form = UploadAudioFileForm(request.POST, request.FILES)
    form.fields['project'] = forms.ModelChoiceField(Project.objects.getUserProjects(user))
    form.fields['library'] = forms.ModelChoiceField(Library.objects.getUserLibraries(user))

    if form.is_valid():
      data = form.cleaned_data

      alias = data['alias']
      if alias == '':
        logging.error("uploadAudioFile(): No Alias Provided")
        return HttpResponseServerError("Alias must be provided")

      project = form.cleaned_data['project']
      library = form.cleaned_data['library']

      if data['startTime'] == '':
        realStartTime = None
      else:
        realStartTime = data['startTime']

      # perms checking
      if not project.userHasPermissions(user, ARLO_PERMS.WRITE):
        return HttpResponseForbidden("Forbidden")
      if not library.userHasPermissions(user, ARLO_PERMS.WRITE):
        return HttpResponseForbidden("Forbidden")

      ###
      # File Paths
      mediaRelativeFilePath = nesterSettings.getMediaFilePath(library, request.FILES['file'].name)

      ###
      # Does the file already exist
      if MediaFile.objects.filter(file=mediaRelativeFilePath).count():
        logging.error("MediaFile already exists: " + mediaRelativeFilePath)
        return HttpResponseServerError("MediaFile already exists: " + mediaRelativeFilePath)

      ###
      # generate md5 sum
      md5 = hashlib.md5()
      for chunk in request.FILES['file'].chunks():
        md5.update(chunk)

      ###
      # Save Media File
      mediaFile = MediaFile(type=MediaTypes.objects.get(name = "audio"),
                            file=data['file'],
                            user=user,
                            alias=alias,
                            library=library,
                            realStartTime=realStartTime,
                            md5=md5.hexdigest(),
                            )
      mediaFile.save()
      project.mediaFiles.add(mediaFile)
      logging.info("Media File Created: id %s [%s]", mediaFile.id.__str__(), mediaFile.file.name)

      ###
      # Save blank WavFileMetaData
      addOrUpdateMediaFileMetaData(mediaFile, [
        ('contextInfo', data['contextInfo'], True),
        ('fileSize', '0', False),
        ('validRIFFsize', '0', False),
        ('validDATAsize', '0', False),
        ('dataStartIndex', '0', False),
        ('wFormatTag', '0', False),
        ('numChannels', '0', False),
        ('sampleRate', '0', False),
        ('nAvgBytesPerSec', '0', False),
        ('nBlockAlign', '0', False),
        ('bitsPerSample', '0', False),
        ('numBytesToTailPad', '0', False),
        ('numFrames', '0', False),
        ('durationInSeconds', '0', False),
        ('dataSize', '0', False)
      ])

      ###
      # Run the import in Adapt
      results = Meandre.call(Meandre.MeandreAction.analyzeAudioFile,{'id':mediaFile.id})
      if results[0]:
        logging.info("Create Audio File Menadre Call: " + results[1])
      else:
        logging.info("Create Audio File Menadre Call: " + results[1])

      return HttpResponseRedirect('userHome')

    else:
      logging.info("# form not valid - form errors: " + str((form.errors.__str__())))
      return render(request, 'tools/uploadAudioFile.html', {
                                'user':user,
                                'form': form,
                                'apacheRoot':settings.APACHE_ROOT,
                                })
  else: # GET
    logging.info("# d #")
    form = UploadAudioFileForm()
    form.fields['project'] = forms.ModelChoiceField(Project.objects.getUserProjects(user))
    form.fields['library'] = forms.ModelChoiceField(Library.objects.getUserLibraries(user))

    return render(request, 'tools/uploadAudioFile.html',
                               {'user':user,
                                'form': form,
                                'apacheRoot':settings.APACHE_ROOT,
                                'defaultLibrary': defaultLibrary,
                                'defaultProject': defaultProject,
                                })



class UploadAudioFileForm2(forms.Form):
  file     = forms.FileField(label='File Name')
  alias = forms.CharField(max_length=255, required=True)

  project  = forms.ModelChoiceField(Project.objects.none(), empty_label=None, required=True)
  library = forms.ModelChoiceField(Library.objects.none(), empty_label=None, required=True)

  useCustomBitSize = forms.BooleanField(label='Use Custom BitSize (Resample)', required=False, initial=False)
  bitSize    = forms.IntegerField(label='', initial=16, required=False)

  useCustomSampleRate = forms.BooleanField(label='Use Custom SampleRate (Resample)', required=False, initial=False)
  sampleRate = forms.IntegerField(label='', initial=50000, required=False)

  # Use a custom clean to only require text fields if checkboxes are selected
  def clean(self):
    cleaned_data = super(UploadAudioFileForm2, self).clean()

    useCustomBitSize = cleaned_data.get("useCustomBitSize")
    bitSize = cleaned_data.get("bitSize")
    useCustomSampleRate = cleaned_data.get("useCustomSampleRate")
    sampleRate = cleaned_data.get("sampleRate")

    if useCustomBitSize and (not bitSize):
      self._errors["bitSize"] = self.error_class(["Must Specify a Valid Bit Size"])

    if useCustomSampleRate and (not sampleRate):
      self._errors["sampleRate"] = self.error_class(["Must Specify a Valid Sample Rate"])

    # Always return the full collection of cleaned data.
    return cleaned_data



## Upload an audio file via web form. This new version queues the import using
# the QueueTask, and supports MP3 imports.
# GET displays form, POST uploads file
# @param request The Django HTTP request object
# @return Django HTTP response object (redirect to audioFiles/ after upload,
# or display this upload form)

@login_required
def uploadAudioFile2(request):
  user = request.user
  logging.info("Uploading Audio File2 : user = " + user.__str__())

  if request.method == 'POST':
    logging.info("# a #")
    form = UploadAudioFileForm2(request.POST, request.FILES)
    form.fields['project'] = forms.ModelChoiceField(Project.objects.getUserProjects(user))
    form.fields['library'] = forms.ModelChoiceField(Library.objects.getUserLibraries(user))

    logging.info("# b #")
    if form.is_valid():
      logging.info("# c #")
      data = form.cleaned_data
      alias = data['alias']

      project = data['project']
      if project is None:
        logging.error("No Project Selected")
        return render(request, 'tools/genericMessage.html', {
          'message':'ERROR: Must specify a Project',
          })
      library = data['library']

      useCustomBitSize = data['useCustomBitSize']
      useCustomSampleRate = data['useCustomSampleRate']


      # perms checking - check against the initial Project to which the file is being added
      if not project.userHasPermissions(user, ARLO_PERMS.WRITE):
        return HttpResponseForbidden("Forbidden")

      ###
      # filename and paths

      # get the uploaded file name
      file = request.FILES['file']
      name = file.name
      filebase, fileext = os.path.splitext(name)

      # target filename
      filePath = filebase + ".wav"

      # save the uploaded file
      relativeTempFilePath = "tempupload-" + uuid.uuid4().hex + fileext  # uuid just to get a temporary name
      absoluteTempFilePath = os.path.join(
          settings.MEDIA_ROOT, nesterSettings.getMediaFilePath(library, relativeTempFilePath))
      destination = open(absoluteTempFilePath, 'wb+')
      for chunk in file.chunks():
        destination.write(chunk)
      destination.close()

      # start job in unknown status so that it doesn't kick off 'til we're done
      j = Jobs(
        user=request.user,
        project=project,
        creationDate=datetime.datetime.now(),
        name='Import Media File:' + alias,
        type=JobTypes.objects.get(name="ImportAudioFile"),
        status=JobStatusTypes.objects.get(name="Unknown"),
        )
      j.save()

      # add job parameters
      JobParameters(job=j, name="sourceRelativeFilePath", value=nesterSettings.getMediaFilePath(library, relativeTempFilePath)).save()
      JobParameters(job=j, name="alias", value=data['alias']).save()
      JobParameters(job=j, name="projectId", value=str(project.id)).save()
      JobParameters(job=j, name="libraryId", value=str(library.id)).save()
      JobParameters(job=j, name="mediaRelativeFilePath", value=nesterSettings.getMediaFilePath(library, filePath)).save()

      if useCustomBitSize:
        JobParameters(job=j, name="outputBits", value=str(data['bitSize'])).save()
      if useCustomSampleRate:
        JobParameters(job=j, name="outputSampleRate", value=str(data['sampleRate'])).save()

      # ready for QueueRunner to pick it up
      j.status=JobStatusTypes.objects.get(name="Queued")
      j.save()

      return HttpResponseRedirect('viewJob/' + str(j.id))
    else:
      logging.info("# form not valid")
      logging.info("# form errors: " + str((form.errors.__str__())))
      logging.info("# form.data: " + form.data.__str__())
      logging.info("# e #")
      #form.fields['project'] = forms.ModelChoiceField(Project.objects.filter(user=user))
      return render(request, 'tools/uploadAudioFile2.html',
                               {'user':user,
                                'form': form,
                                'apacheRoot':settings.APACHE_ROOT,
                                })
  else: # GET
    logging.info("# d #")
    form = UploadAudioFileForm2()
    form.fields['project'] = forms.ModelChoiceField(Project.objects.getUserProjects(user))
    form.fields['library'] = forms.ModelChoiceField(Library.objects.getUserLibraries(user))


    return render(request, 'tools/uploadAudioFile2.html',
                               {'user':user,
                                'form': form,
                                'apacheRoot':settings.APACHE_ROOT,
                                })
