import logging

from django.shortcuts import render
from django.contrib.auth.decorators import login_required
from django.http import HttpResponseForbidden
from django.contrib.contenttypes.models import ContentType
from django.contrib.auth.models import User

from tools.models import (
                          ProjectPermissions,
                          Project,
                          MediaFile,
                          TagClass,
                          TagSet,
                          MediaFileMetaData,
                          Jobs,
                          JobLog,
                          JobResultFile,
                          RandomWindow,
                          JobParameters,
                          Library,
                          MediaFileUserMetaDataField,
                          MediaFileUserMetaData,
                          ArloUserGroup,
                          ArloPermission)


##
# Custom Exception for Errors Processing Permissions requests.
# This exception in not indicative of successful authorization,
# rather indicates an internal error in trying to determine
# authorizations.

class ArloPermissionsException(Exception):
    pass


class ARLO_PERMS(object):
    """
    Base class for ARLO Permissions solely to hold 'public' Constants.
    """
    # Permissions Constants (notice how we try to mimic *nix Perms here...)
    # NOTE: These are for bitwise arithmetic, so MUST be suitable values.
    LAUNCH_JOB = 0x01
    WRITE = 0x02
    READ = 0x04
    ADMIN = 0x08

    _FULL_ACCESS = LAUNCH_JOB | WRITE | READ | ADMIN


    ## Compute the internal 'permission value' (a bit-mapped field)
    # given individual permissions.
    # @param read
    # @param write
    # @param launch_job
    # @param admin
    # @return Integer 'effective permission' value.

    @classmethod
    def getPermissionValue(cls, read=False, write=False, launch_job=False, admin=False):
        perm = 0
        if read:
            perm += cls.READ
        if write:
            perm += cls.WRITE
        if launch_job:
            perm += cls.LAUNCH_JOB
        if admin:
            perm += cls.ADMIN
        return perm


    ## Given an ArloPermission object, compute the 'effective permission' value.
    # This is the bitwise-integer value used in internal logic.
    # @param arloPermission An ArloPermission object.
    # @return Integer 'effective permission' value.

    @classmethod
    def _computeFromArloPermission(cls, arloPermission):
        perm_value = 0
        if arloPermission.read:
            perm_value |= cls.READ
        if arloPermission.write:
            perm_value |= cls.WRITE
        if arloPermission.launch_job:
            perm_value |= cls.LAUNCH_JOB
        if arloPermission.admin:
            perm_value |= cls.ADMIN
        return perm_value


    ## Check whether the provided effective_permissions satisfy the
    # target_permissions.
    # @param effective_permissions The integer-bitwise effective permissions that are granted.
    # @param target_permissions The integer-bitwise Target permissions that must be satisfied.
    # @param admin_override If True, ADMIN perms implicitly grant all permissions.
    # @return True if effective_permissions satisfy target_permissions

    @classmethod
    def _comparePermissions(cls, effective_permissions, target_permissions, admin_override=True):
        if admin_override and (effective_permissions & cls.ADMIN):
            return True
        return target_permissions == (target_permissions & effective_permissions)



# TODO HERE

# Current Thoughts:
#
# Perhaps it's best for performance not to build up an entire object representing
# the full permissions for the object. Instead, individual user / object lookups
# may result in less DB hits. Caching could be accomplished on a
# (user/object/aggregate_perms) key.
#
# I'm guessing it's far more likely we'll see repeats of such a key rather than
# what would be gained from building up entire Target objects.


class ArloTargetObjectPermissions(ARLO_PERMS):
    """
    Permissions object built for an individual object. The target object
    will be one of the Nester Models which have User Permissions management,
    e.g., Library, Project, TagSet.

    TODO Should eventually make extensive use of caching to speed up lookups.
    NOTE that this caching would have to be coordinated amongst multiple
    processes, possibly on separate systems. That increases the complexity...

    This Class is used for mapping a target object to it's complete set of users.
    """

    # The target object that we are analyzing permissions against.
    _target_object = None

    # Record a list of users and their effective permissions. Note that this
    # aggregates all users either explicitly assigned or through ArloUserGroups.
    # Example:
    #   _users[user.id] = 7 # We OR the constants above, so this is READ | WRITE |+ LAUNCH_JOB
    _users = None

    # The default permissions for the Target.
    _default_permissions = 0

    ## Ensure we have a valid initialized permissions class.
    #
    # Should be called at the start of every member that checks
    # permissions. Raise ArloPermissionsException if not.

    def ensureValid(self):
        if self._target_object is None or self._users is None:
            message = "ArloBasePermissions - Tried to Access Invalid Instance"
            logging.error(message)
            raise ArloPermissionsException(message)


    ## Check the object Permissions.
    #
    # @param user A Django User object.
    # @param target_permissions Bitwise OR all requested permissions
    # @param enable_staff_override If True, 'staff' users receive ALL privileges.
    # @return True if ALL requested permissions are granted, False otherwise.

    def userHasPermissions(self, user, target_permissions, enable_staff_override=True):
        # This version checks the permissions against the pre-computed cache
        # that we store internally. Overridden versions may compute these
        # values on the fly.
        self.ensureValid()

        if enable_staff_override and user.is_staff:
            return True

        user_perms = self._users.get(user.id)

        if user_perms is None:
            # if the user is not found in our set, return default
            return self._comparePermissions(self._default_permissions, target_permissions)
        else:
            return self._comparePermissions(user_perms, target_permissions)


class ArloLibraryPermissions(ArloTargetObjectPermissions):

    ##
    # Initialize the permissions from the target Library object.
    #
    # Permissions are generated roughly following the following steps. Note
    # that these steps are short circuited per-user (i.e., if a user has
    # User-level perms, then Group perms will not apply).
    # @param target_object Django Library object.

    def __init__(self, target_object):
        # ensure we received a Library object
        if not isinstance(target_object, Library):
            raise TypeError("ArloLibraryPermissions received non-Library Type")

        super(ArloLibraryPermissions, self).__init__()

        library = target_object

        # save this last in case of improper initialization, as this is
        # used for the initialization validity assertions
        self._target_object = None

        # reset any cached perms
        self._users = {}

        ##
        # Run the checks in reverse order, such that the last items
        # have precedence

        # check for Group perms listed in ArloPermissions
        groupContentType = ContentType.objects.get_for_model(ArloUserGroup)
        for perm in library.permissions.filter(bearer_type=groupContentType):

            # compute the effective permissions
            perm_value = self._computeFromArloPermission(perm)
            # loop over each user in the group, and assign that permission
            # NOTE: Group permissions are unioned
            for user in perm.bearer_object.users.all():
                self._users[user.id] = self._users.get(user.id, 0) | perm_value


        # check for User perms listed in ArloPermissions
        userContentType = ContentType.objects.get_for_model(User)
        for perm in library.permissions.filter(bearer_type=userContentType):
            perm_value = self._computeFromArloPermission(perm)
            self._users[perm.bearer_id] = perm_value


        # library owner gets full access
        self._users[library.user.id] = self._FULL_ACCESS

        # compute the default permissions
        self._default_permissions = self.getPermissionValue(
                                            library.default_permission_read,
                                            library.default_permission_write,
                                            library.default_permission_launch_job,
                                            library.default_permission_admin)

        # save the object, indicating successful initialization
        self._target_object = library


class ArloProjectPermissions(ArloTargetObjectPermissions):


    # Unlike the base version, this version computes the permissions on
    # the fly rather than pre-computing and retrieving from a cache.

    def __init__(self, target_object):
        # ensure we received a Project object
        if not isinstance(target_object, Project):
            raise TypeError("ArloProjectPermissions received non-Project Type")

        super(ArloProjectPermissions, self).__init__()

        # set the Default permissions of the Target
        self._default_permissions = self.getPermissionValue(
                                    target_object.default_permission_read,
                                    target_object.default_permission_write,
                                    target_object.default_permission_launch_job,
                                    target_object.default_permission_admin)


        self._target_object = target_object

        # we aren't pre-computing permissions
        self._users = None


    def userHasPermissions(self, user, target_permissions, enable_staff_override=True):

        ##
        # Note that checks will short-circuit, so first found permission
        # takes precedence

        project = self._target_object

        if enable_staff_override and user.is_staff:
            return True

        # project owner gets full access
        if user == project.user:
            return True


        # TODO
        # TEMPORARY Also use our Old Style Rules:

        perms_value = 0

        try:
            if ProjectPermissions.objects.get(user=user, project=project).canRead:
                perms_value |= self.READ
            if ProjectPermissions.objects.get(user=user, project=project).canWrite:
                perms_value |= self.WRITE
        except:
            pass

        if self._comparePermissions(perms_value, target_permissions):
            return True


        # check for User perms listed in ArloPermissions
        userContentType = ContentType.objects.get_for_model(User)
        for perm in project.permissions.filter(bearer_type=userContentType):
            if perm.bearer_object == user:
                # computer effective perms, then check
                perm_value = self._computeFromArloPermission(perm)
                return self._comparePermissions(perm_value, target_permissions)

        # check for Group perms listed in ArloPermissions
        groupContentType = ContentType.objects.get_for_model(ArloUserGroup)
        perm_value = None
        for perm in project.permissions.filter(bearer_type=groupContentType):
            if user in perm.bearer_object.users.all():
                # compute the effective permissions
                if perm_value is None:
                    perm_value = 0
                perm_value = perm_value | self._computeFromArloPermission(perm)
        if perm_value is not None:
            return self._comparePermissions(perm_value, target_permissions)

        # fallback to Defaults
        return self._comparePermissions(self._default_permissions, target_permissions)

    ## Given a user, return their effective permissions.
    # Note: Staff (admin) status and Default Permissions are NOT considered here
    # @return Integer value of sum of permission constants from ARLO_PERMS
    def getUserPermissions(self, user):
        project = self._target_object

        # project owner gets full access
        if user == project.user:
            return ARLO_PERMS._FULL_ACCESS

        perms_value = 0

        # TEMPORARY Also use our Old Style Rules:
        try:
            perm = ProjectPermissions.objects.get(user=user, project=project)
            if perm.canRead:
                perms_value |= self.READ
            if perm.canWrite:
                perms_value |= self.WRITE
        except:
            pass

        # check for User perms listed in ArloPermissions
        userContentType = ContentType.objects.get_for_model(User)
        for perm in project.permissions.filter(bearer_type=userContentType):
            if perm.bearer_object == user:
                # computer effective perms, then check
                perms_value |= self._computeFromArloPermission(perm)

        # check for Group perms listed in ArloPermissions
        groupContentType = ContentType.objects.get_for_model(ArloUserGroup)
        for perm in project.permissions.filter(bearer_type=groupContentType):
            if user in perm.bearer_object.users.all():
                # compute the effective permissions
                perms_value |= self._computeFromArloPermission(perm)

        return perms_value

    ## Return a list of users and their permissions to the Project.
    # Note: Staff (admin) users are NOT enumerated here based on their staff
    #       status
    # Note: Default permissions on the Project are ignored
    # @return A dict of dicts, as {User: {'read': , 'write': 'launch_job':, 'admin':}, }
    #         User - auth.User Object
    def getProjectUsers(self):
        project = self._target_object

        # get a set of user_ids from the various possible methods
        user_ids = set()

        # include the owner
        user_ids.add(project.user.id)

        # Get users with direct perms
        userContentType = ContentType.objects.get_for_model(User)
        for perm in project.permissions.filter(bearer_type=userContentType):
            user_ids.add(perm.bearer_object.id)

        # Get users with group perms
        groupContentType = ContentType.objects.get_for_model(ArloUserGroup)
        for perm in project.permissions.filter(bearer_type=groupContentType):
            for user in perm.bearer_object.users.all():
                user_ids.add(user.id)

        # get users in old style
        for perm in ProjectPermissions.objects.filter(project=project):
            user_ids.add(perm.user.id)

        # loop over all the users, and get their perms
        user_perms = {}
        for user_id in user_ids:
            user = User.objects.get(pk=user_id)
            perms = self.getUserPermissions(user)
            user_perms[user] = {
                'read': bool(perms & self.READ),
                'write': bool(perms & self.WRITE),
                'launch_job': bool(perms & self.LAUNCH_JOB),
                'admin': bool(perms & self.ADMIN),
            }

        return user_perms


class ArloTagSetPermissions(ArloTargetObjectPermissions):


    # Unlike the base version, this version computes the permissions on
    # the fly rather than pre-computing and retrieving from a cache.

    def __init__(self, target_object):
        # ensure we received a TagSet object
        if not isinstance(target_object, TagSet):
            raise TypeError("ArloTagSetPermissions received non-TagSet Type")

        super(ArloTagSetPermissions, self).__init__()

        self._target_object = target_object

        # we aren't pre-computing permissions
        self._users = None


    def userHasPermissions(self, user, target_permissions, enable_staff_override=True):

        ##
        # Note that checks will short-circuit, so first found permission
        # takes precedence

        tagSet = self._target_object

        if enable_staff_override and user.is_staff:
            return True

        # owner gets full access
        if user == tagSet.user:
            return True

        # project Admins get full access to TagSets
        if tagSet.project.userHasPermissions(user, self.ADMIN, enable_staff_override):
            return True

        # check for User perms listed in ArloPermissions
        userContentType = ContentType.objects.get_for_model(User)
        for perm in tagSet.permissions.filter(bearer_type=userContentType):
            if perm.bearer_object == user:
                # computer effective perms, then check
                perm_value = self._computeFromArloPermission(perm)
                return self._comparePermissions(perm_value, target_permissions)


        # check for Group perms listed in ArloPermissions
        groupContentType = ContentType.objects.get_for_model(ArloUserGroup)
        perm_value = None
        for perm in tagSet.permissions.filter(bearer_type=groupContentType):
            if user in perm.bearer_object.users.all():
                # compute the effective permissions
                if perm_value is None:
                    perm_value = 0
                perm_value = perm_value | self._computeFromArloPermission(perm)
        if perm_value is not None:
            return self._comparePermissions(perm_value, target_permissions)

        # Finally, fallback to the TagSet's Project permissions
        return tagSet.project.userHasPermissions(user, target_permissions, enable_staff_override)


class ArloUserGroupPermissions(ArloTargetObjectPermissions):

    # Note that ArloUserGroup permissions are different than most
    # objects. We'll determine these by:
    #   read: user is in Group, or has Admin
    #   write: user has Admin
    #   launch_job: n/a
    #   admin: user owns the Group, or has been assigned Admin perms

    def __init__(self, target_object):
        # ensure we received a TagSet object
        if not isinstance(target_object, ArloUserGroup):
            raise TypeError("ArloUserGroupPermissions received non-ArloUserGroup Type: {}".format(type(target_object)))

        super(ArloUserGroupPermissions, self).__init__()

        self._target_object = target_object

        # we aren't pre-computing permissions
        self._users = None


    def userHasPermissions(self, user, target_permissions, enable_staff_override=True):

        arloUserGroup = self._target_object

        # Staff users
        if enable_staff_override and user.is_staff:
            return True

        # owner gets full access
        if user == arloUserGroup.createdBy:
            return True

        ###
        # does the user have Admin?

        group_type = ContentType.objects.get_for_model(ArloUserGroup)
        user_type = ContentType.objects.get_for_model(User)

        # does the User have perms directly
        if ArloPermission.objects.filter(target_id=arloUserGroup.id,target_type=group_type,admin=True,bearer_type=user_type,bearer_id=user.id).count() > 0:
            return True

        # is the User in any Groups that have perms
        for perm in ArloPermission.objects.filter(target_id=arloUserGroup.id,target_type=group_type,admin=True,bearer_type=group_type):
            if user in perm.bearer_object.users.all():
                return True


        ###
        # Check Read perms

        # If here, the only valid perm we're still searching for is READ
        if target_permissions != self.READ:
            return False

        if user in arloUserGroup.users.all():
            return True

        # does the User have perms directly
        if ArloPermission.objects.filter(target_id=arloUserGroup.id,target_type=group_type,read=True,bearer_type=user_type,bearer_id=user.id).count() > 0:
            return True

        # is the User in any Groups that have perms
        for perm in ArloPermission.objects.filter(target_id=arloUserGroup.id,target_type=group_type,read=True,bearer_type=group_type):
            if user in perm.bearer_object.users.all():
                return True

        return False


class ArloPermissionPermissions(ArloTargetObjectPermissions):

    # Note that ArloPermission permissions are different than most
    # objects. We'll determine these by:

# TODO
# These may prove to be rather difficult to generate permissions
# In most cases, these will be determined by the target object for the
# permission.

# Thoughts to consider:
#   - No issue when administering perms via UI, we can check the object
#   - API permissions will be tricky. Individual access we can probably
#     manage, but what about listing. Thinking maybe we don't have a list
#     method, and instead make a user / object query function. This would
#     need to query on User/Group/Library/Project/TagSet
#     (maybe a list_perms method on each of these objects?)


    def __init__(self, target_object):
        # ensure we received a TagSet object
        if not isinstance(target_object, ArloPermission):
            raise TypeError("ArloPermissionPermissions received non-ArloPermission Type: {}".format(type(target_object)))

        super(ArloPermissionPermissions, self).__init__()

        self._target_object = target_object

        # we aren't pre-computing permissions
        self._users = None


    def userHasPermissions(self, user, target_permissions, enable_staff_override=True):

        # Staff users
        if enable_staff_override and user.is_staff:
            return True

        arloPermission = self._target_object
        target = arloPermission.target_object

        # If the user has ADMIN privileges to the target object, it inherits
        if target.userHasPermissions(user, ARLO_PERMS.ADMIN):
            return True

        # If the user is the Bearer of the Permission, we can give READ
        if user == arloPermission.bearer_object:
            if self._comparePermissions(ARLO_PERMS.READ, target_permissions):
                return True

        # If target is a Group
        if isinstance(target, ArloUserGroup):
            # If the User is Admin of the Group, they get admin here
            if target.userHasPermissions(user, ARLO_PERMS.ADMIN):
                return True
            # If the user is in the Group, they get READ
            if user in target.users.all():
                if self._comparePermissions(ARLO_PERMS.READ, target_permissions):
                    return True

        return False






##############################
#                            #
#        Data Queries        #
#                            #
##############################


## Return a list of all Projects to which a user has admin access
# @param user A User Object
# @return A list of Project Objects (As a Django QuerySet)

def getUserAdminProjects(user):

  # Currently just Projects the User owns
  projects = Project.objects.filter(user=user)
  return projects



## Return a list of all MediaFiles to which a user has access.
# @param user A User Object
# @return A MediaFile QuerySet

def getUserMediaFiles(user):

  # get Projects that the user can access
  projects = Project.objects.getUserProjects(user)

  return MediaFile.objects.filter(project__in=projects).distinct()


## Return a list of all TagClasses to which a user has access.
# @param user A User Object
# @return A TagClass QuerySet

def getUserTagClasses(user):

  # get Projects that the user can access
  projects = Project.objects.getUserProjects(user)

  return TagClass.objects.filter(project__in=projects).distinct()


## Return a list of all TagSets to which a user has access.
# @param user A User Object
# @return A TagSet QuerySet

def getUserTagSets(user):

  # get Projects that the user can access
  projects = Project.objects.getUserProjects(user)

  return TagSet.objects.filter(project__in=projects).distinct()


## Return a list of all MediaFileMetaData to which a user has access.
# @param user A User Object
# @return A MediaFileMetaData QuerySet

def getUserMediaFileMetaData(user):

  # get MediaFiles that the user can access
  mediaFiles = getUserMediaFiles(user)

  return MediaFileMetaData.objects.filter(mediaFile__in=mediaFiles).distinct()


## Return a list of all Jobs to which a user has access.
# @param user A User Object
# @return A Jobs QuerySet

def getUserJobs(user):

  # get MediaFiles that the user can access
  projects = Project.objects.getUserProjects(user)

  return Jobs.objects.filter(project__in=projects).distinct()


## Return a list of all JobLogs to which a user has access.
# @param user A User Object
# @return A JobLog QuerySet

def getUserJobLogs(user):

  jobs = getUserJobs(user)

  return JobLog.objects.filter(job__in=jobs).distinct()


## Return a list of all JobResultFile to which a user has access.
# @param user A User Object
# @return A JobResultFile QuerySet

def getUserJobResultFiles(user):

  jobs = getUserJobs(user)

  return JobResultFile.objects.filter(job__in=jobs).distinct()


## Return a list of all RandomWindows to which a user has access. Permissions are
# derived from the containing Job.
# @param user A User Object
# @return A RandomWindow QuerySet

def getUserRandomWindows(user):

  jobs = getUserJobs(user)

  return RandomWindow.objects.filter(randomWindowTaggingJob__in=jobs).distinct()


## Return a list of all JobParameters to which a user has access. Permissions
# are derived from the containing Job.
# @param user A User Object
# @return A JobParameter QuerySet

def getUserJobParameters(user):

  jobs = getUserJobs(user)

  return JobParameters.objects.filter(job__in=jobs).distinct()


## Return a list of all MediaFileUserMetaDataFields to which a user has access.
# Permissions are derived from the containing Library.
# @param user A User Object
# @return A MediaFileUserMetaDataField QuerySet

def getUserMediaFileUserMetaDataFields(user):

  libraries = Library.objects.getUserLibraries(user)
  return MediaFileUserMetaDataField.objects.filter(library__in=libraries).distinct()


## Return a list of all MediaFileUserMetaData to which a user has access.
# @param user A User Object
# @return A MediaFileUserMetaData QuerySet

def getUserMediaFileUserMetaData(user):

  metaDataFields = getUserMediaFileUserMetaDataFields(user)
  return MediaFileUserMetaData.objects.filter(metaDataField__in=metaDataFields).distinct()


################################
#                              #
#        ArloPermission        #
#                              #
################################



# TODO
def getLibraryPermissions(library):
    return library.permissions.all()


##########################
#                        #
#        UI Views        #
#                        #
##########################


## View User's Groups, add new.
# @param request The Django HTTP request object
# @return Django HTTPResponse object

@login_required
def userGroupsView(request):
  user = request.user
  logging.info("userGroupsView: user = " + user.__str__())

  userGroups = ArloUserGroup.objects.getUserArloUserGroups(user)

  return render(request, 'tools/userGroups.html', {
#                          'form': form,
#                          'apacheRoot':settings.APACHE_ROOT,
                         'user':user,
                         'userGroups': userGroups,
                         })


## View or Edit an ArloUserGroup.
#
# @param request The Django HTTP request object
# @param arloUserGroupId Database ID of the ArloUserGroup object to edit.
# @return Django HTTPResponse object

@login_required
def arloUserGroup(request, arloUserGroupId):
  user = request.user
  logging.info("arloUserGroup: user = {} arloUserGroupId = {}".format(user.__str__(), str(arloUserGroupId)))

  arloUserGroup = ArloUserGroup.objects.get(pk=arloUserGroupId)

  if not arloUserGroup.userHasPermissions(user, ARLO_PERMS.READ):
    return HttpResponseForbidden("Forbidden - You do not have access to this ArloUserGroup.")

  group_type = ContentType.objects.get_for_model(ArloUserGroup)
  adminPerms = ArloPermission.objects.filter(target_id=arloUserGroup.id,target_type=group_type,admin=True)

  hasGroupAdmin = arloUserGroup.userHasPermissions(user, ARLO_PERMS.ADMIN)

  return render(request, 'tools/arloUserGroup.html', {
                         'user': user,
                         'arloUserGroup': arloUserGroup,
                         'hasGroupAdmin': hasGroupAdmin,
                         'adminPerms': adminPerms,
                         })
