import datetime
import logging

from django.contrib.auth.decorators import login_required
from django.shortcuts import render
from django.http import HttpResponseRedirect, HttpResponseForbidden, HttpResponse
from django import forms
from django.conf import settings

from tools.models import Project, JobParameters, Jobs, JobTypes, JobStatusTypes, TagSet, TagClass
import tools.Meandre as Meandre
from tools.Meandre import MeandreAction
from .userPermissions import ARLO_PERMS
from .biasManipulation import AddJobParameters, GetJobParameters


class SupervisedTagDiscoveryForm(forms.Form):
  name = forms.CharField(label='Name', max_length=255, initial='default')
  tagClassesToSearch = forms.ModelMultipleChoiceField(TagClass.objects.none())
  sourceTagSets      = forms.ModelMultipleChoiceField(TagSet.objects.none(), widget=forms.CheckboxSelectMultiple, required=True)
  destinationTagSet  = forms.ModelChoiceField(TagSet.objects.none(), empty_label=None, required=True)
  numberOfTagsToDiscover = forms.IntegerField(label='Number Of Tags To Discover', initial=1)
#  REPRESENTATION BIAS
  numFrequencyBands = forms.IntegerField(label='Num Frequency Bands', initial=128)
  numTimeFramesPerSecond = forms.FloatField(label='Num Time Frames Per Second', initial=100.0)
  dampingRatio = forms.FloatField(label='Damping Ratio', initial=0.02)
  minFrequency = forms.FloatField(label='Min Frequency', initial=60.0)
  maxFrequency = forms.FloatField(label='Max Frequency', initial=12000.0)
  spectraWeight = forms.FloatField(label='Spectra Weight', initial=1.0)
  pitchWeight = forms.FloatField(label='Pitch Weight', initial=0.0)
  averageEnergyWeight = forms.FloatField(label='Average Energy Weight', initial=0.0)
  numExemplars = forms.IntegerField(label='Num Exemplars', initial=999999)
# SELECTION BIAS
  maxOverlapFraction = forms.FloatField(label='maxOverlapFraction', initial=0.1)
  minPerformance = forms.FloatField(label='minPerformance', initial=0.5)
# Optional
  searchWithinTagClasses = forms.ModelMultipleChoiceField(TagClass.objects.none(), required=False)
  saveBestNTags          = forms.IntegerField(label="Save Best 'N' Tags per File", initial='', required=False)
  saveBestNTagsPerClass  = forms.IntegerField(label="Save Best 'N' Tags per TagClass per File", initial='', required=False)


## Launch a SupervisedTagDiscovery job using the QueueRunner job manager.
#
# GET request shows the form, POST processes it.
# @param request The Django HTTP request object
# @param projectId The database ID of the Project in which to run the discovery
# @param fromJob Optional Pre-populate the field with the settings from a previous job
# @return Django HTTP response object


@login_required
def supervisedTagDiscoveryQueueRunner(request, projectId, fromJob=0):
  # fromJob = If set on a GET, the default form parameters will be pulled from the existing job
  user = request.user
  logging.info("Starting Supervised Tag Discovery (Queue Runner): user = " + user.__str__() +
    " projectId = " + projectId.__str__() +
    "  fromJob=" + fromJob.__str__())
  project = Project.objects.get(id=projectId)

  # check user Permissions
  if not project.userHasPermissions(user, ARLO_PERMS.WRITE):
    return HttpResponseForbidden("Forbidden")

  if request.method == 'POST':
    logging.info("Starting Supervised Tag Discovery (Queue Runner): user = " + user.__str__() + " projectId = " + project.id.__str__())

    form = SupervisedTagDiscoveryForm(request.POST) # A form bound to the POST data
    form.fields['tagClassesToSearch'] = forms.ModelMultipleChoiceField(project.getTagClasses(), required=True)
    form.fields['sourceTagSets'] = forms.ModelMultipleChoiceField(project.getTagSets(include_hidden=False), required=True)
    form.fields['destinationTagSet'] = forms.ModelChoiceField(project.getTagSets(include_hidden=False), required=True)
    form.fields['searchWithinTagClasses'] = forms.ModelMultipleChoiceField(project.getTagClasses(), required=False)

    if form.is_valid(): # All validation rules pass
      data = form.cleaned_data

      tagClasses = data['tagClassesToSearch']

      # Restrict Search Within Selected Classes
      searchWithinTagClasses = data['searchWithinTagClasses']
      searchWithinTagClassesCsv = None
      if len(searchWithinTagClasses) > 0:
        searchWithinTagClassesCsv = ""
        for sc in searchWithinTagClasses:
          searchWithinTagClassesCsv += sc.id.__str__() + ","

      ####
      # Start a new job parent job, and setup it's parameters
      # Java will make the child jobs

      parentJob = Jobs(
        user=user,
        project=project,
        creationDate=datetime.datetime.now(),
        type=JobTypes.objects.get(name="SupervisedTagDiscoveryParent"),
        name=data['name'],
        status=JobStatusTypes.objects.get(name="Unknown"),
        )
      parentJob.save()

      srcTagSetsCsv = ""
      for ts in data['sourceTagSets']:
        srcTagSetsCsv += ts.id.__str__() + ","

      JobParameters(job=parentJob, name='numberOfTagsToDiscover', value=data['numberOfTagsToDiscover']).save()
      JobParameters(job=parentJob, name='numFrequencyBands', value=data['numFrequencyBands']).save()
      JobParameters(job=parentJob, name='numTimeFramesPerSecond', value=data['numTimeFramesPerSecond']).save()
      JobParameters(job=parentJob, name='dampingRatio', value=data['dampingRatio']).save()
      JobParameters(job=parentJob, name='minFrequency', value=data['minFrequency']).save()
      JobParameters(job=parentJob, name='maxFrequency', value=data['maxFrequency']).save()
      JobParameters(job=parentJob, name='spectraWeight', value=data['spectraWeight']).save()
      JobParameters(job=parentJob, name='pitchWeight', value=data['pitchWeight']).save()
      JobParameters(job=parentJob, name='averageEnergyWeight', value=data['averageEnergyWeight']).save()
      JobParameters(job=parentJob, name='numExemplars', value=data['numExemplars']).save()
      JobParameters(job=parentJob, name='maxOverlapFraction', value=data['maxOverlapFraction']).save()
      JobParameters(job=parentJob, name='minPerformance', value=data['minPerformance']).save()
      JobParameters(job=parentJob, name='sourceTagSets', value=srcTagSetsCsv).save()
      JobParameters(job=parentJob, name='destinationTagSet', value=data['destinationTagSet'].id).save()
      if searchWithinTagClassesCsv:
        JobParameters(job=parentJob, name='searchWithinTagClasses', value=searchWithinTagClassesCsv).save()

      # saveBestNTags
      if data['saveBestNTags']:
        if (int(data['saveBestNTags']) > 0):
          JobParameters(job=parentJob, name='saveBestNTags', value=data['saveBestNTags']).save()

      # saveBestNTagsPerClass
      if data['saveBestNTagsPerClass']:
        if (int(data['saveBestNTagsPerClass']) > 0):
          JobParameters(job=parentJob, name='saveBestNTagsPerClass', value=data['saveBestNTagsPerClass']).save()

      # Add in a JobParameter for each TagClass we are searching
      # (tagClassId,focusFactor,minimumMatchQuality)
      for tagClass in tagClasses:
        sValue = tagClass.id.__str__() + "," + "1" + "," + data['minPerformance'].__str__()
        JobParameters(job=parentJob, name='searchTagClass', value=sValue).save()

      logging.info("### 4 ###")

      parentJob.status = JobStatusTypes.objects.get(name="Queued")
      parentJob.save()


      return HttpResponseRedirect(settings.URL_PREFIX + '/tools/manageJobs/' + project.id.__str__())
    else:
      # invalid form data
      logging.info("Invalid Form Data")

      return render(request, "tools/supervisedTagDiscovery.html", {
                                'user':user,
                                'apacheRoot':settings.APACHE_ROOT,
                                'form':form,
                                })
  else:  # GET Request

#    if (fromJob):
#      logging.info("setting fromjob")
#
#      oldJob = Jobs.objects.get(id=fromJob)
#      oldJobParameters = GetJobParameters(fromJob)
#
#      form = SupervisedTagDiscoveryForm( initial={
#          'name' : oldJob.name,
##tagClassesToSearch
#          'sourceTagSets' : oldJobParameters['sourceTagSets'].split(','),
#          'destinationTagSet' : oldJobParameters['destinationTagSet'],
#          'numberOfTagsToDiscover' : oldJobParameters['numberOfTagsToDiscover'],
#          'numFrequencyBands' : oldJobParameters['numFrequencyBands'],
#          'numTimeFramesPerSecond' : oldJobParameters['numTimeFramesPerSecond'],
#          'dampingRatio' : oldJobParameters['dampingRatio'],
#          'minFrequency' : oldJobParameters['minFrequency'],
#          'maxFrequency' : oldJobParameters['maxFrequency'],
#          'spectraWeight' : oldJobParameters['spectraWeight'],
#          'pitchWeight' : oldJobParameters['pitchWeight'],
#          'averageEnergyWeight' : oldJobParameters['averageEnergyWeight'],
#          'numExemplars' : oldJobParameters['numExemplars'],
#          'maxOverlapFraction' : oldJobParameters['maxOverlapFraction'],
#          'minPerformance' : oldJobParameters['minPerformance']})
#      # TODO classes
#    else:
#      form = SupervisedTagDiscoveryForm()

    form = SupervisedTagDiscoveryForm()

    form.fields['tagClassesToSearch'] = forms.ModelMultipleChoiceField(project.getTagClasses(), required=True)
    form.fields['sourceTagSets'] = forms.ModelMultipleChoiceField(project.getTagSets(include_hidden=False), required=True)
    form.fields['destinationTagSet'] = forms.ModelChoiceField(project.getTagSets(include_hidden=False), required=True)
    form.fields['searchWithinTagClasses'] = forms.ModelMultipleChoiceField(project.getTagClasses(), required=False)

    return render(request, "tools/supervisedTagDiscovery.html", {
                              'user':user,
                              'apacheRoot':settings.APACHE_ROOT,
                              'form':form,
                              'project':project,
                              })





# WARNING WARNING WARNING #

# This Function is deprecated and unused - should probably delete

# WARNING WARNING WARNING #



## Launch a SupervisedTagDiscovery job
#
# @param request The Django HTTP request object
# @param projectId The database ID of the Project in which to run the discovery
# @param fromJob Optional Pre-populate the field with the settings from a previous job
# @return Django HTTP response object

@login_required
def supervisedTagDiscovery(request, projectId, fromJob=0):
  # fromJob = If set on a GET, the default form parameters will be pulled from the existing job
  user = request.user
  logging.info("Starting Supervised Tag Discovery: user = " + user.__str__() +
    " projectId = " + projectId.__str__() +
    "  fromJob=" + fromJob.__str__())
  project = Project.objects.get(id=projectId)

  # check user Permissions
  if not project.userHasPermissions(user, ARLO_PERMS.WRITE):
    return HttpResponseForbidden("Forbidden")

  if request.method == 'POST':
    logging.info("Starting Supervised Tag Discovery: user = " + user.__str__() + " projectId = " + project.id.__str__())

    form = SupervisedTagDiscoveryForm(request.POST) # A form bound to the POST data
    form.fields['tagClassesToSearch'] = forms.ModelMultipleChoiceField(project.getTagClasses(), required=True)
    form.fields['sourceTagSets'] = forms.ModelMultipleChoiceField(project.getTagSets(), required=True)
    form.fields['destinationTagSet'] = forms.ModelChoiceField(project.getTagSets(), required=True)

    if form.is_valid(): # All validation rules pass
      data = form.cleaned_data

      tagClasses = data['tagClassesToSearch']

      allTagClasses = project.getTagClasses()
      for tagClass in allTagClasses:
        if tagClass in tagClasses:
#          tagClass.focusFactor = 1
#          tagClass.matchQuality = data['minPerformance']
          tagClass.save()
        else:
#          tagClass.focusFactor = 0
          tagClass.save()

      ####
      # Start a new job, and setup it's parameters

      supervisedTagDiscoveryJob = Jobs(
        user=user,
        project=project,
        creationDate=datetime.datetime.now(),
        name=data['name'],
        type=JobTypes.objects.get(name="SupervisedTagDiscovery")
        )
      supervisedTagDiscoveryJob.save()

      srcTagSetsCsv = ""
      for ts in data['sourceTagSets']:
        srcTagSetsCsv += ts.id.__str__() + ","

      params = {
        'numberOfTagsToDiscover' : [data['numberOfTagsToDiscover']],
        'numFrequencyBands': [data['numFrequencyBands']],
        'numTimeFramesPerSecond': [data['numTimeFramesPerSecond']],
        'dampingRatio': [data['dampingRatio']],
        'minFrequency': [data['minFrequency']],
        'maxFrequency': [data['maxFrequency']],
        'spectraWeight': [data['spectraWeight']],
        'pitchWeight': [data['pitchWeight']],
        'averageEnergyWeight': [data['averageEnergyWeight']],
        'numExemplars': [data['numExemplars']],
        'maxOverlapFraction': [data['maxOverlapFraction']],
        'minPerformance': [data['minPerformance']],
        'sourceTagSets': [srcTagSetsCsv],
        'destinationTagSet': [data['destinationTagSet'].id],
        }

      if not AddJobParameters(supervisedTagDiscoveryJob, params):
        logging.info("FAILED adding job parameters")
        return HttpResponse("FAILED adding job parameters")

      logging.info("### 4 ###")


      results = Meandre.call(MeandreAction.startSupervisedTagDiscovery, {'id':str(supervisedTagDiscoveryJob.id)})
      if results[0]:
        logging.info("startSupervisedTagDiscovery Meandre call Succeeded - " + results[1])
      else:
        logging.info("startSupervisedTagDiscovery Meandre call failed - " + results[1])

      return HttpResponseRedirect(settings.URL_PREFIX + '/tools/manageJobs/' + project.id.__str__())
    else:
      # invalid form data
      logging.info("Invalid Form Data")

      return render(request, "tools/supervisedTagDiscovery.html", {
                                'user':user,
                                'apacheRoot':settings.APACHE_ROOT,
                                'form':form,
                                })
  else:  # GET Request

    if (fromJob):
      logging.info("setting fromjob")

      oldJob = Jobs.objects.get(id=fromJob)
      oldJobParameters = GetJobParameters(fromJob)

      form = SupervisedTagDiscoveryForm( initial={
          'name' : oldJob.name,
#tagClassesToSearch
          'sourceTagSets' : oldJobParameters['sourceTagSets'].split(','),
          'destinationTagSet' : oldJobParameters['destinationTagSet'],
          'numberOfTagsToDiscover' : oldJobParameters['numberOfTagsToDiscover'],
          'numFrequencyBands' : oldJobParameters['numFrequencyBands'],
          'numTimeFramesPerSecond' : oldJobParameters['numTimeFramesPerSecond'],
          'dampingRatio' : oldJobParameters['dampingRatio'],
          'minFrequency' : oldJobParameters['minFrequency'],
          'maxFrequency' : oldJobParameters['maxFrequency'],
          'spectraWeight' : oldJobParameters['spectraWeight'],
          'pitchWeight' : oldJobParameters['pitchWeight'],
          'averageEnergyWeight' : oldJobParameters['averageEnergyWeight'],
          'numExemplars' : oldJobParameters['numExemplars'],
          'maxOverlapFraction' : oldJobParameters['maxOverlapFraction'],
          'minPerformance' : oldJobParameters['minPerformance']})
      # TODO classes
    else:
      form = SupervisedTagDiscoveryForm()

    form.fields['tagClassesToSearch'] = forms.ModelMultipleChoiceField(project.getTagClasses(), required=True)
    form.fields['sourceTagSets'] = forms.ModelMultipleChoiceField(project.getTagSets(), required=True)
    form.fields['destinationTagSet'] = forms.ModelChoiceField(project.getTagSets(), required=True)


    return render(request, "tools/supervisedTagDiscovery.html", {
                              'user':user,
                              'apacheRoot':settings.APACHE_ROOT,
                              'form':form,
                              })
