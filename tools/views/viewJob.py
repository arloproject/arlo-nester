import logging

from django.contrib.auth.decorators import login_required
from django.http import HttpResponseForbidden
from django.shortcuts import render
from django.conf import settings

from tools.models import JobParameters, JobLog, Jobs, JobResultFile
from .userPermissions import ARLO_PERMS
from .biasManipulation import SetJobStatusForm, SetJobPriorityForm


## Display properties of a Job
# @param request The Django HTTP request object
# @param jobID The database id of the Job
# @return Django HTTPResponse object

@login_required
def viewJob(request,jobID):
  user = request.user
  logging.info("Viewing Job: user = " + user.__str__() + " jobID = " + jobID.__str__())
  job = Jobs.objects.get(id=jobID)

  # check user permissions
  if not job.userHasPermissions(user, ARLO_PERMS.READ):
    return HttpResponseForbidden

  jobParameters = JobParameters.objects.filter(job=job).order_by('name')
  jobLogs = JobLog.objects.filter(job=job).order_by('messageDate')
  jobResultFiles = JobResultFile.objects.filter(job=job).order_by('creationDate')

  childTasks = Jobs.objects.filter(parentJob=job)

  setJobStatusForm = SetJobStatusForm()
  setJobPriorityForm = SetJobPriorityForm()

  return render(request, 'tools/viewJob.html', {
      'apacheRoot':settings.APACHE_ROOT,
      'user':user,
      'job': job,
      'childTasks': childTasks,
      'jobParameters': jobParameters,
      'jobLogs': jobLogs,
      'jobResultFiles': jobResultFiles,
      'setJobStatusForm': setJobStatusForm,
      'setJobPriorityForm': setJobPriorityForm,
      'project': job.project,
      })
