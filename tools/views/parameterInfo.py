import logging

from django.contrib.auth.decorators import login_required
from django.shortcuts import render
from django.conf import settings


## Function used to provide context-sensitive help menus throughout the GUI.
# These are setup as '?' icons throughout the interface; clinking on them
# is captured by arlo.js, and a popup is displayed with data from this function.
# @param request The Django HTTP request object
# @param infoType The name of the info to return to the popup.
# @return Django HTTP response object (info.html)

@login_required
def getInfo(request, infoType):
  logging.info("Getting Info:")

  if infoType == 'framesPerSecond':
    content = 'The number of times per second in an audio file \n \
               that a line of spectra is computed.'
  elif infoType == 'frequencyBands':
    content = 'The number a divisions given to a spectra between\n \
               minFrequency and maxFrequency.'
  elif infoType == 'dampingRatio':
    content = infoType
  elif infoType == 'minFrequency':
    content = 'The minimum frequency the computer looks at when\n \
               discovering tags.'
  elif infoType == 'maxFrequency':
    content = 'The maximum frequency the computer looks at when\n \
               discovering tags.'
  elif infoType == 'minPerformance':
    content = 'The lowest similarity score that a canditate\n \
               audio segment can have to become a tag. If this\n \
               score does not exist in a group of random probes,\n \
               no machine tag will be created.'
  elif infoType == 'spectraWeight':
    content = 'The weight given in search to the complete spectra\n \
               a.k.a. what you see when you visualize audio files\n \
               in the ARLO system.'
  elif infoType == 'pitchWeight':
    content = 'The weight given in search to the pitch trace of\n \
               a user tag compared to a candidate tag.'
  elif infoType == 'pitchEnergyWeight':
    content = infoType
  elif infoType == 'averageEnergyWeight':
    content = infoType
  elif infoType == 'discoveryClasses':
    content = 'The tag classes to use when searching for new tags.'
  else:
    content = 'There is no information about this topic yet.'


  return render(request, "tools/info.html", {
    'apacheRoot': settings.APACHE_ROOT,
    'heading':infoType,
    'content':content,
    })
