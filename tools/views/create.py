import logging

from tools.models import TagClass


class EmptyStringException(Exception):
  def __str__(self):
    return "Cannot create object with empty string as it's name!"




## Add a new TagClass to a Project.
# @param user User Object
# @param project The Project Object to which the TagClass belongs
# @param className The Class Name of the new TagClass
# @param displayName The Display Name of the new TagClass
# @return The new TagClass object (None if failure)

def createTagClass(user, project, className, displayName):
  logging.info("Creating tag class")
  if className == '':
    raise EmptyStringException
  try:
    tagClass = TagClass.objects.get(project=project,className=className)
    logging.warning('TagClass with name already exists!')
    return None
  except:
    #because display name is optional, must do this
    if displayName == '':
      dn = className
    else:
      dn = displayName

    tagClass = TagClass(user=user,project=project,className=className,displayName=dn)
    tagClass.save()
    return tagClass
