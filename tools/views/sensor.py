import logging
import json

from django import forms
from django.contrib.auth.decorators import login_required
from django.http import JsonResponse, HttpResponseForbidden
from django.shortcuts import render
from django.conf import settings

from tools.models import Sensor, SensorArray
from .userPermissions import ARLO_PERMS


class AddSensorForm(forms.Form):
  name = forms.CharField(max_length=255, required=True)
  x = forms.FloatField(initial=0, required=True)
  y = forms.FloatField(initial=0, required=True)
  z = forms.FloatField(initial=0, required=True)


@login_required
def sensorArray(request, sensorArrayId):
  user = request.user
  logging.info("Sensor Array: user = " + user.__str__() + " id = " + sensorArrayId.__str__())

  messageList = []
  sensorArray = SensorArray.objects.get(id=sensorArrayId)

  # Check SensorArray Permissions
  if not sensorArray.userHasPermissions(user, ARLO_PERMS.READ):
    return HttpResponseForbidden("Forbidden - You do not have access to this SensorArray.")

  if request.method == "POST":
    if not sensorArray.userHasPermissions(user, ARLO_PERMS.WRITE):
      return HttpResponseForbidden("Forbidden - You do not have write access to this SensorArray.")

    # add a new sensor
    form = AddSensorForm(request.POST)
    if form.is_valid():
      data = form.cleaned_data
      newSensor = Sensor(
          sensorArray=sensorArray,
          name = data['name'],
          x = data['x'],
          y = data['y'],
          z = data['z'],
          layoutRow = -1,
          layoutColumn = -1)
      newSensor.save()
      messageList.append("Added New Sensor")
  else: # not POST
    form = AddSensorForm()

  sensors = Sensor.objects.filter(sensorArray=sensorArray)
  startLayoutIds = []
  startLayoutRows = []
  startLayoutCols = []
  for sensor in sensors:
    if ((sensor.layoutRow >= 0) and (sensor.layoutColumn >= 0)):
      startLayoutIds.append(sensor.id)
      startLayoutRows.append(sensor.layoutRow)
      startLayoutCols.append(sensor.layoutColumn)


  return render(request, 'tools/sensorArray.html', {
                            'user': user,
                            'messageList': messageList,
                            'addSensorForm': form,
                            'sensorArray': sensorArray,
                            'sensors': sensors,
                            'startLayoutNumRows': sensorArray.layoutRows,
                            'startLayoutNumColumns': sensorArray.layoutColumns,
                            'startLayoutIds': startLayoutIds,
                            'startLayoutRows': startLayoutRows,
                            'startLayoutCols': startLayoutCols,
                            'apacheRoot':settings.APACHE_ROOT,
                            })


def isInt(x):
  if x == int(x):
    return True
  return False


@login_required
def updateSensorArrayLayout(request):
  user = request.user

  # TODO
  # Switch to updating the Sensors directly from the interface via the API
  # rather than using this crazy function. ARLO-262

  #############
  # data validation

  if not request.is_ajax():
      return JsonResponse({'IsSuccess':0, 'Message':"Error: Not Ajax"})

  logging.info(request.body)
  data = json.loads(request.body)
  logging.info(data)
  if (
      (not 'sensorArrayId' in data)
      or (not 'rows' in data)
      or (not 'cols' in data)
      or (not 'noLayoutIds' in data)
      or (not 'layoutIds' in data)
      or (not 'layoutRows' in data)
      or (not 'layoutCols' in data)):
    logging.info("Failed")
    return JsonResponse({'IsSuccess':0, 'Message':"Error: Missing Data"})

  # ensure everything is an integer
  if (
      (not isInt(data['sensorArrayId']))
      or (not isInt(data['rows']))
      or (not isInt(data['cols']))):
    return JsonResponse({'IsSuccess':0, 'Message':"Error: Not Integer"})
  for id in data['noLayoutIds']:
    if (not isInt(id)):
      return JsonResponse({'IsSuccess':0, 'Message':"Error: Not Integer - noLayoutIds"})
  for id in data['layoutIds']:
    if (not isInt(id)):
      return JsonResponse({'IsSuccess':0, 'Message':"Error: Not Integer - layoutIds"})
  for id in data['layoutRows']:
    if (not isInt(id)):
      return JsonResponse({'IsSuccess':0, 'Message':"Error: Not Integer - layoutRowss"})
  for id in data['layoutCols']:
    if (not isInt(id)):
      return JsonResponse({'IsSuccess':0, 'Message':"Error: Not Integer - layoutCols"})


  # ensure length of arrays matches up
  if (len(data['layoutIds']) != len(data['layoutRows']) or len(data['layoutIds']) != len(data['layoutCols'])):
    return JsonResponse({'IsSuccess':0, 'Message':"Error: Invalid Data Lengths"})

  # ensure user has permission to the array
  sensorArray = SensorArray.objects.get(id=data['sensorArrayId'])

  if not sensorArray.userHasPermissions(user, ARLO_PERMS.READ):
    return HttpResponseForbidden("Forbidden - You do not have access to this SensorArray.")

  # ensure all sensors are in the same array
  for id in data['noLayoutIds']:
    sensor = Sensor.objects.get(id=id)
    if sensor.sensorArray.id != sensorArray.id:
      return JsonResponse({'IsSuccess':0, 'Message':"Error: Sensor Not In Array"})
  for id in data['layoutIds']:
    sensor = Sensor.objects.get(id=id)
    if sensor.sensorArray.id != sensorArray.id:
      return JsonResponse({'IsSuccess':0, 'Message':"Error: Sensor Not In Array"})

  # ensure all row / col numbers are less than num of rows
  for row in data['layoutRows']:
    if row >= data['rows']:
      return JsonResponse({'IsSuccess':0, 'Message':"Error: Row too large"})
  for col in data['layoutCols']:
    if col >= data['cols']:
      return JsonResponse({'IsSuccess':0, 'Message':"Error: Column too large"})

  ###################
  # run the update

  # save num rows/cols
  sensorArray.layoutRows = data['rows']
  sensorArray.layoutColumns = data['cols']
  sensorArray.save()

  # save the noLayout sensors
  for id in data['noLayoutIds']:
    sensor = Sensor.objects.get(id=id)
    sensor.layoutRow = -1
    sensor.layoutColumn = -1
    sensor.save()

  # save the sensors with layout
  for x in range(0, len(data['layoutIds'])):
    logging.info(str(x) + ": " + str(data['layoutIds'][x]) + " - " + str(data['layoutRows'][x]) + "," + str(data['layoutCols'][x]))
    sensor = Sensor.objects.get(id=data['layoutIds'][x])
    sensor.layoutRow = data['layoutRows'][x]
    sensor.layoutColumn = data['layoutCols'][x]
    sensor.save()

  return JsonResponse({'IsSuccess':1, 'Message':"Layout Saved"})
