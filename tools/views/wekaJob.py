import datetime
import logging

from django.contrib.auth.decorators import login_required
from django.shortcuts import render
from django.http import HttpResponseRedirect, HttpResponseForbidden
from django import forms
from django.conf import settings

from tools.models import TagSet, Project, Jobs, JobTypes, JobStatusTypes, JobParameters
from .userPermissions import ARLO_PERMS


class WekaSVMJobForm(forms.Form):
  name              = forms.CharField(label='Job Name', max_length=255, initial='Weka SVM')
  sourceTagSet      = forms.ModelChoiceField(TagSet.objects.none(), label="Source TagSet", empty_label=None, required=True)
  destinationTagSet = forms.ModelChoiceField(TagSet.objects.none(), label="Destination TagSet", empty_label=None, required=True)
  numFramesPerExample = forms.IntegerField(label='Number of Frames per Example', initial=1)
  complexityConstant = forms.FloatField(label='Complexity Constant', initial=1.0)

#  REPRESENTATION BIAS
  numFrequencyBands      = forms.IntegerField(label='Num Frequency Bands', initial=128)
  numTimeFramesPerSecond = forms.IntegerField(label='Num Time Frames Per Second', initial=100)
  dampingRatio           = forms.FloatField(label='Damping Ratio', initial=0.02)
  minFrequency           = forms.IntegerField(label='Min Frequency', initial=60)
  maxFrequency           = forms.IntegerField(label='Max Frequency', initial=12000)



## Launch a Weka job using the QueueRunner job manager.
#
# GET request shows the form, POST processes it.
# @param request The Django HTTP request object
# @param projectId The database ID of the Project in which to run the discovery
# @return Django HTTP response object


@login_required
def launchWekaJob(request, projectId):
  user = request.user

  logging.info("Launch Weka Job - user = " + user.__str__() +
    " projectId = " + projectId.__str__())

  project = Project.objects.get(id=projectId)

  # check user Permissions
  if not project.userHasPermissions(user, ARLO_PERMS.WRITE):
    return HttpResponseForbidden("Forbidden")

  # Setup Forms
  svmForm = WekaSVMJobForm()
  svmForm.fields['sourceTagSet'] = forms.ModelChoiceField(project.getTagSets(include_hidden=False), label="Source TagSet", required=True)
  svmForm.fields['destinationTagSet'] = forms.ModelChoiceField(project.getTagSets(include_hidden=False), label="Destination TagSet", required=True)

  # GET Request
  if request.method != 'POST':
    return render(request, "tools/launchWekaJob.html", {
                              'user': user,
                              'apacheRoot': settings.APACHE_ROOT,
                              'svmForm': svmForm,
                              'project': project,
                              })


  ###
  # If here, we have a POST Request

  # Setup a Weka SVM Job


  svmForm = WekaSVMJobForm(request.POST)
  svmForm.fields['sourceTagSet'] = forms.ModelChoiceField(project.getTagSets(include_hidden=False), label="Source TagSet", required=True)
  svmForm.fields['destinationTagSet'] = forms.ModelChoiceField(project.getTagSets(include_hidden=False), label="Destination TagSet", required=True)


  if not svmForm.is_valid(): # All validation rules pass
    logging.info("Invalid Form Data")
    return render(request, "tools/launchWekaJob.html", {
                              'user':user,
                              'apacheRoot':settings.APACHE_ROOT,
                              'svmForm': svmForm,
                              'project': project,
                              })

  data = svmForm.cleaned_data

  # Start a new job, and setup it's parameters

  newJob = Jobs(
    user=user,
    project=project,
    creationDate=datetime.datetime.now(),
    type=JobTypes.objects.get(name="WekaJobWrapper"),
    name=data['name'],
    status=JobStatusTypes.objects.get(name="Unknown"),
    )
  newJob.save()

  JobParameters(job=newJob, name='wekaFunction', value='svm').save()
  JobParameters(job=newJob, name='sourceTagSet', value=str(data['sourceTagSet'].id)).save()
  JobParameters(job=newJob, name='destinationTagSet', value=str(data['destinationTagSet'].id)).save()
  JobParameters(job=newJob, name='numFramesPerExample', value=str(data['numFramesPerExample'])).save()
  JobParameters(job=newJob, name='complexityConstant', value=str(data['complexityConstant'])).save()

  # Spectra Params
  JobParameters(job=newJob, name='numFrequencyBands', value=data['numFrequencyBands']).save()
  JobParameters(job=newJob, name='numTimeFramesPerSecond', value=data['numTimeFramesPerSecond']).save()
  JobParameters(job=newJob, name='dampingRatio', value=data['dampingRatio']).save()
  JobParameters(job=newJob, name='minFrequency', value=data['minFrequency']).save()
  JobParameters(job=newJob, name='maxFrequency', value=data['maxFrequency']).save()

  newJob.status = JobStatusTypes.objects.get(name="Queued")
  newJob.save()

  return HttpResponseRedirect(settings.URL_PREFIX + '/tools/manageJobs/' + project.id.__str__())
