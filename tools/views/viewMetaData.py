import logging
import re

from django.contrib.auth.decorators import login_required
from django.http import HttpResponseForbidden, JsonResponse
from django.shortcuts import render
from django import forms
from django.conf import settings

from tools.models import MediaFileMetaData, MediaFile, Sensor
from .userPermissions import ARLO_PERMS


# # # # # # # # # # # # #
# Gonna make several functions for accessing metaData
#
# Multiple access
# ---------------
#   getMediaFileMetaDataDictionary(mediaFile):
#      returns all metaData entries for a mediaFile as a dictionary
#      {name, value}
#   getMediaFileMetaData(mediaFile):
#      returns all metaData entries for a mediaFile as a list of tuples
#      [(name, value, userEditable)]
#
#   addOrUpdateMediaFileMetaData(mediaFile, data):
#      add, or update if records exist, a list of data for a file
#      data is a list of tuples [(name, value, userEditable)]
#      NOTE: will update existing records or add new... will not delete records
#      e.g., if records 1 and 2 exist, and 2 and 3 are passed to this
#      function, 1, 2, and 3 will exist at the end
#
# Individual Access
# -----------------
#   getMetaDataEntry(mediaFile, name):
#      return the value of the record
#      return None if not found (I think it will be None for NULL records too)
#
#   setMetaDataEntry(mediaFile, name, value):
#      Looks for an existing entry, and updates it. Will add if doesn't exist
#      ignore userEditable here


## Add, or update if records exist, a list of MetaData for a MediaFile
# @note will update existing records or add new... Will not delete records
# (e.g., if records 1 and 2 exist, and 2 and 3 are passed to this
# function, 1, 2, and 3 will exist at the end)
# @param mediaFile MediaFile Object
# @param data A list of tuples [(name, value, userEditable)} to set
# @return True on success

def addOrUpdateMediaFileMetaData(mediaFile, data):
  # data is a list of tuples (name, value, userEditable)
  logging.info("************************************")
  for (name, value, userEditable) in data:
    try:
      metaData = MediaFileMetaData.objects.get(mediaFile=mediaFile, name=name)
      logging.info("updating MetaData : " + name.__str__() + " value = " + value.__str__() + " editable = " + userEditable.__str__())
      metaData.value = value
      metaData.userEditable = userEditable
    except:
      logging.info("adding MetaData : " + name.__str__() + " value = " + value.__str__() + " editable = " + userEditable.__str__())
      metaData = MediaFileMetaData(mediaFile=mediaFile, name=name, value=value, userEditable=userEditable)
    metaData.save()

  return True


## returns all metaData entries for a mediaFile, as a list of tuples.
# @param mediaFile MediaFile Object
# @return List of metaData, as tuples (name, value, userEditable)

def getMediaFileMetaData(mediaFile):
  # returns as a list of tuples (name, value, userEditable)

  datalist = []
  data = MediaFileMetaData.objects.filter(mediaFile = mediaFile)
  for datum in data:
    datalist.append ( (datum.name, datum.value, datum.userEditable) )

  return datalist

## returns all metaData entries for a mediaFile, as a dictionary
# @note If there are multiple entries for a key (name) then this may fail.
# @param mediaFile MediaFile Object
# @return List of metaData, as a dictionary {'name': value}

def getMediaFileMetaDataDictionary(mediaFile):
  datadict = {}
  data = MediaFileMetaData.objects.filter(mediaFile = mediaFile)
  for datum in data:
    datadict[datum.name] = datum.value

  return datadict

## returns a specific metaData entry for a mediaFile
# @param mediaFile MediaFile Object
# @param name The MetaData entry's name (key)
# @return The MetaData's value, None if does not exist.

def getMetaDataEntry(mediaFile, name):
  # return the value of the record
  # return None if not found (I think it will be None for NULL records too)

  try:
    data = MediaFileMetaData.objects.get(mediaFile=mediaFile, name=name)
    return data.value
  except:
    logging.error("Couldn't find MetaData Entry '" + name + "' for file: " + str(mediaFile.id))
    return None

## Set a single MetaData entry for a file.
# Updates an existing entry, or add a new.
# @note This does not modify userEditable (will set to database
# default if creating a new entry).
# @param mediaFile MediaFile Object
# @param name The MetaData entry's name (key)
# @param value The value of the MetaData entry to set.
# @return True on success

def setMetaDataEntry(mediaFile, name, value):
  # Looks for an existing entry, and updates it. Will add if doesn't exist
  # ignore userEditable here

  try:
    data = MediaFileMetaData.objects.get(mediaFile=mediaFile, name=name)
    data.value = value
  except:
    logging.error("Couldn't find MetaData Entry '" + name + "' for file: " + mediaFile.id + "... adding.")
    data = MediaFileMetaData(mediaFile=mediaFile, name=name, value=value)

  data.save()
  return True

#
# # # # # # # # # #

## Guess the Start Time of a file based on it's name/alias.
# This is called by AJAX, and returns a JSON response. This is
# basically hardcoded for commonly used file naming conventions,
# and may not be anything too useful for general use. Perhaps in the
# future we can have user-specified Regex.
# @param request The Django HTTP request object
# @param audioID The database ID of a mediaFile
# @return Django HTTP response object, as a JSON dictionary
# {'success_string', 'date_time'}. success_string: "done" if success,
# "failed" otherwise. dateTime: String, formatted as "YYYY-MM-DD HH:MM:SS"

@login_required
def guessTimeFromName(request, audioID):
  mediaFileName = MediaFile.objects.get(id=audioID).alias

  success = 'failed'
  dateTime = ''

  res = re.findall('([0-9]{4})([01][0-9])([0-3][0-9])_([0-2][0-9])([0-5][0-9])_([0-5][0-9])', mediaFileName)
  if len(res) == 1:
    mediaFileName = ''
    success = 'done'
    res = res[0]
    dateTime = res[0] + "-" + res[1] + "-" + res[2] + " " + res[3] + ":" + res[4] + ":" + res[5]


  return JsonResponse({'success_string': success,
                   'date_time':dateTime})


class MetaDataForm(forms.Form):

  # Not actually MetaData, but part of the MediaFile
  active = forms.BooleanField(required=False)
  alias = forms.CharField(max_length=255, required=False)
  uploadDate = forms.DateTimeField(label="Upload Date", required=False)
  mediaRelativeFilePath = forms.CharField(label="Relative File Path", max_length=1024, required=False, widget=forms.TextInput(attrs={'readonly':'1'}))
  sensor = forms.ModelChoiceField(Sensor.objects.none(), required=False)

  # MetaData
  startTime = forms.DateTimeField(label="Start Time", required=False)

  contextInfo = forms.CharField(label="Context Info", widget=forms.Textarea(attrs={'width':'100%'}), required=False)

  fileSize = forms.IntegerField(label="File Size", required=False, widget=forms.TextInput(attrs={'readonly':'1'}))
  dataStartIndex = forms.IntegerField(label="Data Start Index", required=False, widget=forms.TextInput(attrs={'readonly':'1'}))
  dataSize = forms.IntegerField(label="Data Size", required=False, widget=forms.TextInput(attrs={'readonly':'1'}))
  wFormatTag = forms.IntegerField(label="W Format Tag", required=False, widget=forms.TextInput(attrs={'readonly':'1'}))
  numChannels = forms.IntegerField(label="Number of Channels", required=False, widget=forms.TextInput(attrs={'readonly':'1'}))
  sampleRate = forms.IntegerField(label="SampleRate", required=False, widget=forms.TextInput(attrs={'readonly':'1'}))
  nAvgBytesPerSec = forms.IntegerField(label="Average Bytes Per Second", required=False, widget=forms.TextInput(attrs={'readonly':'1'}))
  nBlockAlign = forms.IntegerField(label="Block Align", required=False, widget=forms.TextInput(attrs={'readonly':'1'}))
  bitsPerSample = forms.IntegerField(label="Bits Per Sample", required=False, widget=forms.TextInput(attrs={'readonly':'1'}))
  numBytesToTailPad = forms.IntegerField(label="Number of Bytes to Tail Pad", required=False, widget=forms.TextInput(attrs={'readonly':'1'}))
  numFrames = forms.IntegerField(label="Number of Frames", required=False, widget=forms.TextInput(attrs={'readonly':'1'}))
  durationInSeconds = forms.FloatField(label="Duration(in seconds)", required=False, widget=forms.TextInput(attrs={'readonly':'1'}))
  md5 = forms.CharField(max_length=32, required=False, widget=forms.TextInput(attrs={'size':'34', 'readonly':'1'}))


## View and Edit a Media File's Metadata.
# GET displays the form, POST saves data.
# @param request The Django HTTP request object
# @param audioID The Database ID of the audio file to display
# @return Django HTTP response object

@login_required
def viewMetadata(request, audioID):
  user = request.user
  logging.info("Viewing Meta Data: user = " + user.__str__() + " audioID = " + audioID.__str__())

  mediaFile = MediaFile.objects.get(id=audioID)

  # perms checking
  if not mediaFile.userHasPermissions(user, ARLO_PERMS.WRITE):
    return HttpResponseForbidden("Forbidden")

  if request.method == "POST":

    metaDataForm = MetaDataForm(request.POST)
    metaDataForm.fields['sensor'] = forms.ModelChoiceField(mediaFile.library.getSensors(), required=False)

    if not metaDataForm.is_valid():

      return render(request, 'tools/metadata.html',{
                              'apacheRoot':settings.APACHE_ROOT,
                              'user':user,
                              'form':metaDataForm,
                              'audioID':audioID,
                              'mediaFile': mediaFile,
                              'metaData':getMediaFileMetaData(mediaFile),
                              })


    # Update MediaFile

    data = metaDataForm.cleaned_data

    mediaFile.active = data['active']
    mediaFile.alias = data['alias']
    logging.info("Setting Sensor: %s", data['sensor'])
    mediaFile.sensor = data['sensor']
    if data['startTime'] == '':
      mediaFile.realStartTime = None
    else:
      mediaFile.realStartTime = data['startTime']
    mediaFile.save()

    # Update MetaData
    metaData = []
    metaData.append( ('contextInfo', data['contextInfo'], True) )
    addOrUpdateMediaFileMetaData(mediaFile, metaData)


  ##
  # Default return block - GET Request and after processing POST

  metaData = getMediaFileMetaDataDictionary(mediaFile)

  metaDataForm = MetaDataForm({'active':mediaFile.active,
                               'alias':mediaFile.alias,
                               'uploadDate':mediaFile.uploadDate,
                               'mediaRelativeFilePath':mediaFile.file.name,
                               'md5': mediaFile.md5,
                               'startTime':mediaFile.realStartTime,
                               'contextInfo':metaData.get('contextInfo', ''),
                               'fileSize':metaData.get('fileSize', ''),
                               'dataStartIndex':metaData.get('dataStartIndex', ''),
                               'dataSize':metaData.get('dataSize', ''),
                               'wFormatTag':metaData.get('wFormatTag', ''),
                               'numChannels':metaData.get('numChannels', ''),
                               'sampleRate':metaData.get('sampleRate', ''),
                               'nAvgBytesPerSec':metaData.get('nAvgBytesPerSec', ''),
                               'nBlockAlign':metaData.get('nBlockAlign', ''),
                               'bitsPerSample':metaData.get('bitsPerSample', ''),
                               'numBytesToTailPad':metaData.get('numBytesToTailPad', ''),
                               'numFrames':metaData.get('numFrames', ''),
                               'durationInSeconds':metaData.get('durationInSeconds', ''),
                               })
  metaDataForm.fields['sensor'] = forms.ModelChoiceField(mediaFile.library.getSensors(), required=False)
  if mediaFile.sensor:
    metaDataForm.data['sensor'] = mediaFile.sensor.id


  logging.info("MediaFile.Sensor: " + mediaFile.sensor.__str__())
  logging.info("select: " + metaDataForm.fields['sensor'].__str__())
  logging.info("getMediaFileMetaData(mediaFile) " + getMediaFileMetaData(mediaFile).__str__())

  return render(request, 'tools/metadata.html',{
                            'apacheRoot':settings.APACHE_ROOT,
                            'user':user,
                            'form':metaDataForm,
                            'audioID':audioID,
                            'mediaFile': mediaFile,
                            'metaData':getMediaFileMetaData(mediaFile),
                            })

###########################
###                     ###
### Removing Code Block ###
###                     ###
###########################

# TODO add this back at some point. Removing from Cassandra updates,
# as this is rarely used.

# @login_required
# def autoSetTimeAllFiles(request, projectName):
#   user=request.user
#   logging.info("autoSetTimeAllFiles: user = " + user.__str__() + " projectName = " + projectName.__str__())
#
#   project = Project.objects.get(user=user, name=projectName)
#   library = project.library
#
#   logging.info("library = " + library.__str__())
#   logging.info("library id = " + library.id.__str__())
#
#   libraryMediaFiles = library.mediaFiles.all()
#
#   for mediaFile in libraryMediaFiles:
#       mediaFileName = mediaFile.alias
#
#       res = re.findall('([0-9]{4})([01][0-9])([0-3][0-9])_([0-2][0-9])([0-5][0-9])_([0-5][0-9])', mediaFileName)
#       if len(res) == 1:
#         res = res[0]
#         dateTime = res[0] + "-" + res[1] + "-" + res[2] + " " + res[3] + ":" + res[4] + ":" + res[5]
#         mediaFile.realStartTime = datetime.datetime.strptime(dateTime, "%Y-%m-%d %H:%M:%S")
#         mediaFile.save()
#
#   return audioFiles(request, projectName)
