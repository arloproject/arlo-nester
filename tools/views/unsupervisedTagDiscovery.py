import datetime
import logging

from django.contrib.auth.decorators import login_required
from django.shortcuts import render
from django.http import HttpResponseRedirect, HttpResponseForbidden, HttpResponse
from django import forms
from django.conf import settings

from tools.models import Jobs, JobTypes, JobStatusTypes, Project, TagSet
import tools.Meandre as Meandre
from tools.Meandre import MeandreAction
from .userPermissions import ARLO_PERMS
from .biasManipulation import AddJobParameters


UNSUPERVISED_NORMALIZATION_CHOICES = (
  ("None",        "None"),
  ("Rank",        "Rank"),
  ("EqualEnergy", "Equal Energy"),
)

class UnsupervisedTagDiscoveryForm(forms.Form):
  name = forms.CharField(label='Name', max_length=255, initial='default')
  numberOfClusters = forms.IntegerField(label='Number Of Clusters', initial=10)
#  numberOfClustersToSave = forms.IntegerField(label='Number Of Clusters To Save', initial=50)
#  numberOfExamplesPerCluster = forms.IntegerField(label='Number Of Examples Per Cluster', initial=10)
#  windowSizeInSeconds = forms.FloatField(label='Window Size In Seconds', initial=1.0)
#  REPRESENTATION BIAS
  numFrequencyBands = forms.IntegerField(label='Num Frequency Bands', initial=128)
  numTimeFramesPerSecond = forms.FloatField(label='Num Time Frames Per Second', initial=250.0)
  dampingRatio = forms.FloatField(label='Damping Ratio', initial=0.02)
  minFrequency = forms.FloatField(label='Min Frequency', initial=80)
  maxFrequency = forms.FloatField(label='Max Frequency', initial=10000)
# FORMATION BIAS
  exchangeMode = forms.BooleanField(label='exchangeMode', initial=False, required=False)
  moveMode = forms.BooleanField(label='moveMode', initial=True, required=False)
  numOptimizationIterations = forms.IntegerField(label='numOptimizationIterations', initial=100000)  # in Python, integer >= 32 bits
  sourceTagSets      = forms.ModelMultipleChoiceField(TagSet.objects.none(), widget=forms.CheckboxSelectMultiple, required=True)
  destinationTagSet  = forms.ModelChoiceField(TagSet.objects.none(), empty_label=None, required=True)
  minNumSamples      = forms.IntegerField(label='Minimum Number of Samples', initial=100)

  forceRandomSeed    = forms.BooleanField(label='Force Random Seed', initial=True, required=False)
  userRandomSeed     = forms.IntegerField(label='Random Seed', initial=314159265358979, required=False)
  normalize          = forms.ChoiceField(widget=forms.Select,label="Normalization", required=False, choices=UNSUPERVISED_NORMALIZATION_CHOICES, initial="None")


## Launch an UnsupervisedTagDiscovery job using the QueueRunner job manager.
#
# GET request shows the form, POST processes it.
# @param request The Django HTTP request object
# @param projectId The database ID of the Project in which to run the discovery
# @return Django HTTP response object

@login_required
def unsupervisedTagDiscoveryQueueRunner(request, projectId):
  user = request.user
  logging.info("Starting Unsupervised Tag Discovery (Queue Runner): user = " + user.__str__()
    + " projectId = " + projectId.__str__())

  project = Project.objects.get(id=projectId)

  # check user Permissions
  if not project.userHasPermissions(user, ARLO_PERMS.WRITE):
    return HttpResponseForbidden("Forbidden")

  if request.method == 'POST': # If the form has been submitted...

    form = UnsupervisedTagDiscoveryForm(request.POST) # A form bound to the POST data
    form.fields['sourceTagSets'] = forms.ModelMultipleChoiceField(
        project.getTagSets(include_hidden=False),
        widget=forms.SelectMultiple(attrs={'size':15}),
        required=True)
    form.fields['destinationTagSet'] = forms.ModelChoiceField(project.getTagSets(include_hidden=False), required=True)

    if form.is_valid(): # All validation rules pass

      data = form.cleaned_data

      unsupervisedTagDiscoveryJob = Jobs(
        user=user,
        project=project,
        creationDate=datetime.datetime.now(),
        name=data['name'],
        type=JobTypes.objects.get(name="UnsupervisedTagDiscovery"),
        status=JobStatusTypes.objects.get(name="Unknown"),
        )
      unsupervisedTagDiscoveryJob.save()

      srcTagSetsCsv = ""
      for ts in data['sourceTagSets']:
        srcTagSetsCsv += ts.id.__str__() + ","

      params = {
# no longer used with David's revisions
#        'windowSizeInSeconds': data['windowSizeInSeconds'],
        'numberOfClusters': [data['numberOfClusters']],
#        'numberOfClustersToSave': data['numberOfClustersToSave'],
#        'numberOfExamplesPerCluster': data['numberOfExamplesPerCluster'],
        'numFrequencyBands': [data['numFrequencyBands']],
        'numTimeFramesPerSecond': [data['numTimeFramesPerSecond']],
        'dampingRatio': [data['dampingRatio']],
        'minFrequency': [data['minFrequency']],
        'maxFrequency': [data['maxFrequency']],
        'exchangeMode': [data['exchangeMode']],
        'moveMode': [data['moveMode']],
        'numOptimizationIterations': [data['numOptimizationIterations']],
        'sourceTagSets': [srcTagSetsCsv],
        'destinationTagSet': [data['destinationTagSet'].id],
        'minNumSamples': [data['minNumSamples']],
        }

      if data['forceRandomSeed']:
        params['forceRandomSeed'] = [data['userRandomSeed']]

      if data['normalize'] != "None":
        params['normalize'] = [data['normalize']]

      if not AddJobParameters(unsupervisedTagDiscoveryJob, params):
        logging.info("FAILED adding unsupervisedTagDiscovery job parameters")
        return HttpResponse("FAILED adding job parameters")

      logging.info("### 4 ###")

      unsupervisedTagDiscoveryJob.status = JobStatusTypes.objects.get(name="Queued")
      unsupervisedTagDiscoveryJob.save()

      return HttpResponseRedirect(settings.URL_PREFIX + '/tools/manageJobs/' + project.id.__str__())

    else:
      return render(request, 'tools/unsupervisedTagDiscovery.html', {
        'user':user,
        'apacheRoot':settings.APACHE_ROOT,
        'form':form,
        })

  else: # GET Request
    form = UnsupervisedTagDiscoveryForm()

    form.fields['sourceTagSets'] = forms.ModelMultipleChoiceField(
        project.getTagSets(include_hidden=False),
        widget=forms.SelectMultiple(attrs={'size':15}),
        required=True)
    form.fields['destinationTagSet'] = forms.ModelChoiceField(project.getTagSets(include_hidden=False), required=True)

    return render(request, 'tools/unsupervisedTagDiscovery.html', {
        'user':user,
        'apacheRoot':settings.APACHE_ROOT,
        'form':form,
        })


## Launch an UnsupervisedTagDiscovery job
#
# @param request The Django HTTP request object
# @param projectId The database ID of the Project in which to run the discovery
# @return Django HTTP response object

@login_required
def unsupervisedTagDiscovery(request, projectId):
  user = request.user
  logging.info("Starting Unsupervised Tag Discovery: user = " + user.__str__()
    + " projectId = " + projectId.__str__())

  project = Project.objects.get(id=projectId)

  # check user Permissions
  if not project.userHasPermissions(user, ARLO_PERMS.WRITE):
    return HttpResponseForbidden("Forbidden")

  if request.method == 'POST': # If the form has been submitted...

    form = UnsupervisedTagDiscoveryForm(request.POST) # A form bound to the POST data
    form.fields['sourceTagSets'] = forms.ModelMultipleChoiceField(project.getTagSets(include_hidden=False), required=True)
    form.fields['destinationTagSet'] = forms.ModelChoiceField(project.getTagSets(include_hidden=False), required=True)

    if form.is_valid(): # All validation rules pass

      data = form.cleaned_data

      unsupervisedTagDiscoveryJob = Jobs(
        user=user,
        project=project,
        creationDate=datetime.datetime.now(),
        name=data['name'],
        type=JobTypes.objects.get(name="UnsupervisedTagDiscovery")
        )
      unsupervisedTagDiscoveryJob.save()

      srcTagSetsCsv = ""
      for ts in data['sourceTagSets']:
        srcTagSetsCsv += ts.id.__str__() + ","

      params = {
# no longer used with David's revisions
#        'windowSizeInSeconds': data['windowSizeInSeconds'],
        'numberOfClusters': [data['numberOfClusters']],
#        'numberOfClustersToSave': data['numberOfClustersToSave'],
#        'numberOfExamplesPerCluster': data['numberOfExamplesPerCluster'],
        'numFrequencyBands': [data['numFrequencyBands']],
        'numTimeFramesPerSecond': [data['numTimeFramesPerSecond']],
        'dampingRatio': [data['dampingRatio']],
        'minFrequency': [data['minFrequency']],
        'maxFrequency': [data['maxFrequency']],
        'exchangeMode': [data['exchangeMode']],
        'moveMode': [data['moveMode']],
        'numOptimizationIterations': [data['numOptimizationIterations']],
        'sourceTagSets': [srcTagSetsCsv],
        'destinationTagSet': [data['destinationTagSet'].id],
        'minNumSamples': [data['minNumSamples']],
        }

      if not AddJobParameters(unsupervisedTagDiscoveryJob, params):
        logging.info("FAILED adding unsupervisedTagDiscovery job parameters")
        return HttpResponse("FAILED adding job parameters")

      logging.info("### 4 ###")

      results = Meandre.call(MeandreAction.startUnsupervisedTagDiscovery, {'id':str(unsupervisedTagDiscoveryJob.id)})
      if results[0]:
        logging.info("startUnsupervisedTagDiscovery Meandre call Succeeded - " + results[1])
      else:
        logging.info("startUnsupervisedTagDiscovery Meandre call failed - " + results[1])

      return HttpResponseRedirect(settings.URL_PREFIX + '/tools/catalog/' + project.id.__str__())

    else:
      return render(request, 'tools/unsupervisedTagDiscovery.html', {
        'user':user,
        'apacheRoot':settings.APACHE_ROOT,
        'form':form,
        })

  else: # GET Request
    form = UnsupervisedTagDiscoveryForm()

    form.fields['sourceTagSets'] = forms.ModelMultipleChoiceField(project.getTagSets(include_hidden=False), required=True)
    form.fields['destinationTagSet'] = forms.ModelChoiceField(project.getTagSets(include_hidden=False), required=True)

    return render(request, 'tools/unsupervisedTagDiscovery.html', {
        'user':user,
        'apacheRoot':settings.APACHE_ROOT,
        'form':form,
        })
