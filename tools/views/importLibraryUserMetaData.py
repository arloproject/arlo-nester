from django.contrib.auth.decorators import login_required
from django.shortcuts import render
from django.http import HttpResponseForbidden

from tools.models import Library
from .userPermissions import ARLO_PERMS


## Display the page to allow users to bulk-import MediaFileUserMetaData Entries.
#
# The bulk of this work takes place in the JavaScript. Just display the tool here.
# @param request The Django HTTP request object
# @param libraryId The database ID of the Library to which we are importing.
# @return Django HTTP response object

@login_required
def importLibraryUserMetaData(request, libraryId):
  user = request.user
  library = Library.objects.get(id=libraryId)

  # Check user permissions
  if not library.userHasPermissions(user, ARLO_PERMS.WRITE):
    return HttpResponseForbidden("Forbidden - You do not have write access to this Library.")

  return render(request, 'tools/importLibraryUserMetaData.html',
                           {'user': user,
                            'library':library,
                            })

