import logging
import urllib
import datetime

from django import forms
from django.contrib.auth.decorators import login_required
from django.http import HttpResponseForbidden, HttpResponseNotAllowed, HttpResponseServerError, JsonResponse
from django.shortcuts import render
from django.conf import settings

from tools.models import UserSettings, Project, TagSet, MediaFile, MediaFileMetaData, Tag, NavigateTagClassForm, NewCreateTagForm
# from .viewMetaData import *
from .userPermissions import ARLO_PERMS
from .spectra import generateSpectraImage


###
# Form for the visualizeMultiAudioFile page

class VisualizeMultiAudioForm(forms.Form):
  startDateTime = forms.DateTimeField(label='Start Date/Time (yyyy-mm-dd hh:mm:ss)',initial=datetime.datetime.now(),required=True)
  # endDateTime = forms.DateTimeField(label='End Date/Time (yyyy-mm-dd hh:mm:ss)',initial=datetime.datetime.now,required=True)
  duration = forms.FloatField(initial=3.0)
  gain = forms.FloatField(initial=1.0)
  spectraNumFramesPerSecond = forms.FloatField(initial=64)
  spectraNumFrequencyBands = forms.IntegerField(initial=256)
  spectraDampingFactor = forms.FloatField(initial=0.02)
  spectraMinimumBandFrequency = forms.FloatField(initial=20.0)
  spectraMaximumBandFrequency = forms.FloatField(initial=20000.0)
  loadAudio = forms.BooleanField(label='loadAudio?', required=False, initial=True)
  sensorArrayLayout = forms.BooleanField(label='SensorArray Layout', required=False, initial=False)
  tagSets = forms.ModelMultipleChoiceField(TagSet.objects.none(), widget=forms.CheckboxSelectMultiple, required=False)


## Visualize Multiple Audio Files at once.
# Includes interfaces for creating tags. GET and POST both return the same
# data, All loading and visualization is done through AJAX calls, this just
# sets up the basic parameters. An optional GET parameter, 'tagId', will
# jump the display to the corresponding Tag on load.
# @param request The Django HTTP request object
# @param projectId The Database ID of the Project
# @return Django HTTP response object

@login_required
def visualizeMultiProjectFile(request, projectId):
  user = request.user
  logging.info("Visualizing MultiProjectFile: user = " + user.__str__() +
    " projectId = " + projectId.__str__())

  project = Project.objects.get(id=projectId)

  # check user permissions
  if not project.userHasPermissions(user, ARLO_PERMS.READ):
    return HttpResponseForbidden("Forbidden")

  userSettings = UserSettings.objects.get(user=user, name='default')

  loadAudio = False

  ###
  # Jump To Tag

  jumpToTag = None
  jumpToTagRealStartTimeString = ""
  jumpToTagDuration = 0

  jumpToTagId = request.GET.get('tagId',-1)
  if jumpToTagId > 0:
    try:
      jumpToTag = Tag.objects.get(id=jumpToTagId)

      # check that Tag is in the Project
      if (jumpToTag.tagSet.project != project):
        logging.info("JumpToTag is not in Project!")
        jumpToTag = None
      else:
        # get real Start Time
        fileStartDate = jumpToTag.mediaFile.realStartTime
        if fileStartDate:
          tagRealStartTime = fileStartDate + datetime.timedelta(seconds=float(jumpToTag.startTime))
          jumpToTagRealStartTimeString = tagRealStartTime.strftime('%Y-%m-%d %H:%M:%S')
          jumpToTagDuration = jumpToTag.endTime - jumpToTag.startTime
        else:
          jumpToTag = None
    except:
      jumpToTag = None

  if jumpToTag is None:
      jumpToTagId = -1
      jumpToTagRealStartTimeString = ""
      jumpToTagDuration = 0


  ###
  # Settings Form

  settingsForm = VisualizeMultiAudioForm({
    'startDateTime' : datetime.datetime.now(),
    'duration' : userSettings.fileViewWindowSizeInSeconds,
    'gain' : 1.0,
    'spectraNumFramesPerSecond': userSettings.fileViewSpectraNumFramesPerSecond,
    'spectraNumFrequencyBands': userSettings.fileViewSpectraNumFrequencyBands,
    'spectraDampingFactor': userSettings.fileViewSpectraDampingFactor,
    'spectraMinimumBandFrequency': userSettings.fileViewSpectraMinimumBandFrequency,
    'spectraMaximumBandFrequency': userSettings.fileViewSpectraMaximumBandFrequency,
     'tagSets': [l.pk for l in project.getTagSets(include_hidden=False)],
    })
  settingsForm.fields['tagSets'] = forms.ModelMultipleChoiceField(project.getTagSets(include_hidden=False), required=False)

  ###
  # Spectra Settings

  spectraBorderWidth = 50
  spectraTopBorderHeight = 30
  timeScaleHeight = 22

  createTagForm = NewCreateTagForm()
  createTagForm.fields['tagSet'] = forms.ModelChoiceField(project.getTagSets(include_hidden=False))

  ###
  # Navigate TagClass Form

  navigateForm = NavigateTagClassForm()
  if jumpToTag != None:
    navigateForm.fields['navigateTagClass'] = forms.ModelChoiceField(
                                                project.getTagClasses(),
                                                required=False,
                                                initial = jumpToTag.tagClass
                                                )
    navigateForm.fields['currentTagID'] = forms.IntegerField('currentTagID',
                                                widget = forms.HiddenInput,
                                                required=False,
                                                initial = jumpToTag.id
                                                )
  else:
    navigateForm.fields['navigateTagClass'] = forms.ModelChoiceField(
                                                project.getTagClasses(),
                                                required=False,
                                                )
    navigateForm.fields['currentTagID'] = forms.IntegerField('currentTagID',
                                                widget = forms.HiddenInput,
                                                required=False,
                                                )


  return render(request, 'tools/visualizeMultiProjectFile.html', {
    'apacheRoot': settings.APACHE_ROOT,
    'user':user,
    'loadAudio': 'true' if loadAudio else 'false',
    'settingsForm': settingsForm,
    'createTagForm': createTagForm,
    'navigateForm':navigateForm,
    'borderWidth': spectraBorderWidth,
    'topBorderHeight': spectraTopBorderHeight,
    'timeScaleHeight': timeScaleHeight,
    'project': project,
    'jumpToTagId': jumpToTagId,
    'jumpToTagRealStartTimeString': jumpToTagRealStartTimeString,
    'jumpToTagDuration': jumpToTagDuration,
    })


## Similar to createAudioSpectra(), but includes the audioId in the URL.
# This functions is called through AJAX and works directly with the
# VisualizeMultiAudioForm. Must be a POST request.
# @param request The Django HTTP request object
# @param audioFileId The Database ID of the audio file to visualize
# @return Django HTTP response object

@login_required
def newCreateAudioSpectra(request, audioFileId):
  user = request.user
  logging.info("New Creating Audio Spectra: user = " + user.__str__() +
    " audioFileId = " + str(audioFileId))

  userSettings = UserSettings.objects.get(user=user, name='default')
  audioFile = MediaFile.objects.get(id=audioFileId)

  # check perms
  if not audioFile.userHasPermissions(user, ARLO_PERMS.READ):
    return HttpResponseForbidden('Forbidden')

  if request.method != "POST":
    return HttpResponseNotAllowed(['POST'])

  form = VisualizeMultiAudioForm(request.POST) # A form bound to the POST data
  # use all() here, as the form will be passed in with some options checked, so we can't use none()
  form.fields['tagSets'] = forms.ModelMultipleChoiceField(TagSet.objects.all(), required=False)

  if not form.is_valid(): # All validation rules pass
    logging.error(dict((k, map(unicode, v)) for (k,v) in form.errors.iteritems() ))
    return HttpResponseServerError("Form Invalid")

  # get audio file data
  # TODO Validation that fileStartDate exists here
  fileStartDate = audioFile.realStartTime
  # TODO should probably use the MetaData wrapper functions for this
  fileDurationInSeconds = MediaFileMetaData.objects.get(mediaFile__id=audioFileId, name='durationInSeconds').value

  # get form data
  data = form.cleaned_data

  ##
  # compute times

  # Python 2.7 NOTE
  # 2.7 adds timedelta.total_seconds(), a much easier way to do the below
  # however, since we're using Jython and not current on this version, stick with
  # the hard way for now

  spectraStartDate = data["startDateTime"]
  #spectraEndDate = data["endDateTime"]
  spectraEndDate = spectraStartDate + datetime.timedelta(seconds=float(data["duration"]))

  td = (spectraStartDate - fileStartDate)
  fileStartSeconds = float(td.seconds) + (float(td.microseconds) / 10**6) + td.days * 86400
  td = (spectraEndDate - fileStartDate)
  fileEndSeconds = float(td.seconds) + (float(td.microseconds) / 10**6) + td.days * 86400
  durationSeconds = fileEndSeconds - fileStartSeconds


  #####################
  # spectra parameters

  spectraBorderWidth = 50
  spectraTopBorderHeight = 30
  timeScaleHeight = 22

  spectraNumFramesPerSecond = data['spectraNumFramesPerSecond']
  spectraNumFrequencyBands  = data['spectraNumFrequencyBands']

  imageWidth = spectraBorderWidth * 2  +  spectraNumFramesPerSecond * durationSeconds
  imageHeight = spectraNumFrequencyBands + spectraTopBorderHeight + timeScaleHeight

  playerWidth = imageWidth


  ###
  # Generate Spectra

  spectra_params = {
    'userSettings':userSettings,
    'audioFile': audioFile,

    'startTime': fileStartSeconds,
    'endTime': fileEndSeconds,
    'gain': form.cleaned_data['gain'],
    'numFramesPerSecond': form.cleaned_data['spectraNumFramesPerSecond'],
    'numFrequencyBands': form.cleaned_data['spectraNumFrequencyBands'],
    'dampingFactor': form.cleaned_data['spectraDampingFactor'],

    'minFrequency': form.cleaned_data['spectraMinimumBandFrequency'],
    'maxFrequency': form.cleaned_data['spectraMaximumBandFrequency'],

    'simpleNormalization': True,
    'showTimeScale': True,
    'showFrequencyScale': True,

    'borderWidth': spectraBorderWidth,
    'topBorderHeight': spectraTopBorderHeight,
    'timeScaleHeight': timeScaleHeight,
    'catalogSpectraNumFrequencyBands': form.cleaned_data['spectraNumFrequencyBands'] }

  (success, imageFilePath, audioSegmentFilePath)  = generateSpectraImage(**spectra_params)
  if not success:
    logging.error("createAudioSpectra Unknown Failure While Generating Spectra")
    return HttpResponseServerError("createAudioSpectra Unknown Spectra Generation Failure")

  imageFilePathQuoted = urllib.quote(imageFilePath)
  audioSegmentFilePathQuoted = urllib.quote(audioSegmentFilePath)

  ###
  # Generate Response

  response_dict = {
    'audioSegmentFilePath': audioSegmentFilePathQuoted,
    'imageFilePath': imageFilePathQuoted,
    'playerWidth': playerWidth,
    'imageWidth': imageWidth,
    'imageHeight': imageHeight,
    'startTime': fileStartSeconds,
    'endTime': fileEndSeconds,
    'gain' : form.cleaned_data['gain'],
    'minFrequency': form.cleaned_data['spectraMinimumBandFrequency'],
    'maxFrequency': form.cleaned_data['spectraMaximumBandFrequency'],
    'borderWidth': spectraBorderWidth,
    'topBorderHeight': spectraTopBorderHeight,
    'timeScaleHeight': timeScaleHeight,
    'spectraNumFramesPerSecond':spectraNumFramesPerSecond,
    'spectraStartSeconds':fileStartSeconds,
    'spectraEndSeconds':fileEndSeconds,
    'fileEndSeconds':fileDurationInSeconds
    }

  return JsonResponse(response_dict)
