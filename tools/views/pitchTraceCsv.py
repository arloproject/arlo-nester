import os
import os.path
import logging
import csv
import datetime

from django.contrib.auth.decorators import login_required
from django.shortcuts import render
from django import forms
from django.conf import settings

import nesterSettings as nesterSettings
from tools.models import Project, MediaFile, UserSettings
#from viewMetaData import *
from tools.Meandre import AdaptInterface
#from .project import *
from .userSettings import getExtendedUserSetting

PITCH_TRACE_TYPE_CHOICES = (
    ("MAX_ENERGY", "MAX_ENERGY"),
    ("FUNDAMENTAL", "FUNDAMENTAL")
  )

class PitchTraceCsvForm(forms.Form):
  pitchTraceType  = forms.ChoiceField(choices=PITCH_TRACE_TYPE_CHOICES, required=True, initial="MAX_ENERGY")
  startTime       = forms.FloatField(label='File Start Time (seconds)',required=True, initial=0)
  endTime         = forms.FloatField(label='File End Time (seconds)',required=True, initial=60)
  numTimeFramesPerSecond = forms.IntegerField(label='Number of Time Frames per Second', required=True, initial=4)
  numFrequencyBands = forms.IntegerField(label='Number of Frequency Bands', required=True, initial=32)
  dampingFactor = forms.FloatField(label='Damping Factor', required=True, initial=0.02)
  spectraMinFrequency = forms.FloatField(label='Spectra Minimum Frequency', required=True, initial=20)
  spectraMaxFrequency = forms.FloatField(label='Spectra Maximum Frequency', required=True, initial=20000)
  #FUNDAMENTAL Params
  numSamplePoints     = forms.IntegerField(label='Number of Sample Points', required=True, initial=3)
  minCorrelation      = forms.FloatField(label='Minimum Correlation', required=True, initial=0.1)
  entropyThreshold    = forms.FloatField(label='Entropy Threshold', required=True, initial=0.1)
  pitchTraceStartFreq = forms.FloatField(label='Pitch Trace Start Frequency', required=True, initial=20)
  pitchTraceEndFreq   = forms.FloatField(label='Pitch Trace End Frequency', required=True, initial=20000)
  minEnergyThreshold  = forms.FloatField(label='Minimum Energy Threshold', required=True, initial=0.01)
  tolerance           = forms.FloatField(label='Tolerance', required=True, initial=0.5)
  inverseFreqWeight   = forms.FloatField(label='Inverse Frequency Weight', required=True, initial=1.0)
  maxPathLengthPerTransition = forms.FloatField(label='Maximum Path Length Per Transition', required=True, initial=10)
  windowSize          = forms.IntegerField(label='Window Size', required=True, initial=12)
  extendRangeFactor   = forms.FloatField(label='Extend Range Factor', required=True, initial=1.0)



@login_required
def createPitchTraceCsv(request, projectId, mediaFileId):
  logging.info('Creating Pitch Trace Csv For File:' + mediaFileId.__str__())
  user = request.user
  project = Project.objects.get(id=projectId)
  mediaFile = MediaFile.objects.get(id=mediaFileId)

  status = ''
  filePath = ''

  if request.method == 'POST':
    form = PitchTraceCsvForm(request.POST)

    if not form.is_valid():
      logging.info("# form not valid")
      logging.info("# form: " + form.__str__())
      logging.info("# form.data: " + form.data.__str__())

    data = form.cleaned_data

    logging.debug("# form: " + form.__str__())
    logging.debug("# form.data: " + form.data.__str__())

    filePath = createCSVForPitchTrace(mediaFile, user, form)

    if filePath is None:
      status = "Unknown Error Occurred... check logs"
      filePath = ''
    else:
      status = "Success"

    logging.info(status)


  else: # GET

    # Build up 'initial' params from the user settings.
    initial_params = {}

    # get the UserSettings object
    userSettings = UserSettings.objects.get(user=user, name='default')

    pt_type = getExtendedUserSetting('spectraPitchTraceType', userSettings)
    if pt_type == 'Fundamental':
      initial_params['pitchTraceType'] = 'FUNDAMENTAL'

    params_map = {
      # Spectra Settings
      'numTimeFramesPerSecond': 'spectraPitchTraceNumFramesPerSecond',
      'numFrequencyBands': 'spectraPitchTraceNumFrequencyBands',
      'dampingFactor': 'spectraPitchTraceDampingFactor',
      'spectraMinFrequency': 'spectraPitchTraceMinimumBandFrequency',
      'spectraMaxFrequency': 'spectraPitchTraceMaximumBandFrequency',
      #FUNDAMENTAL Params
      'numSamplePoints': 'spectraPitchTraceNumSamplePoints',
      'minCorrelation': 'spectraPitchTraceMinCorrelation',
      'entropyThreshold': 'spectraPitchTraceEntropyThreshold',
      'pitchTraceStartFreq': 'spectraPitchTraceStartFreq',
      'pitchTraceEndFreq': 'spectraPitchTraceEndFreq',
      'minEnergyThreshold': 'spectraPitchTraceMinEnergyThreshold',
      'tolerance': 'spectraPitchTraceTolerance',
      'inverseFreqWeight': 'spectraPitchTraceInverseFreqWeight',
      'maxPathLengthPerTransition': 'spectraPitchTraceMaxPathLengthPerTransition',
      'windowSize': 'spectraPitchTraceWindowSize',
      'extendRangeFactor': 'spectraPitchTraceExtendRangeFactor',
      }

    for form_key, settings_key in params_map.iteritems():
      value = getExtendedUserSetting(settings_key, userSettings)
      if value is not None:
        initial_params[form_key] = value

    form = PitchTraceCsvForm(initial=initial_params)
    #default return when request is GET and form.is_valid() is False

  return render(request, 'tools/pitchTraceCsv.html', {
                                'user':user,
                                'project': project,
                                'form': form,
                                'status': status,
                                'filePath': filePath,
                                'apacheRoot':settings.APACHE_ROOT,
                                })


## Retrieve a PitchTrace and generate a CSV file.
#
# @param mediaFile MediaFile object.
# @param user User object.
# @param form Populated PitchTraceCsvForm from the interface with PitchTrace settings
# @return Path of the destination file, relative to MEDIA_ROOT


def createCSVForPitchTrace(mediaFile, user, form):
  logging.info("Creating PitchTrace CSV File: mediaFileId = " + str(mediaFile.id))

  ###
  # Open Temp File

  nowDateString = datetime.datetime.now().strftime('%Y%m%d-%H%M%S')
  fileName = str(mediaFile.id) + '-' + nowDateString + '-PitchTrace.csv'
  relFilePath = nesterSettings.getExportFilePath(user, fileName)  # relative to MEDIA_ROOT
  absFilePath = os.path.join(settings.MEDIA_ROOT, relFilePath)

  logging.debug('createCSVForPitchTrace(): Abs File Path: ' + absFilePath.__str__())
  try:
    FILE = open(absFilePath, 'w')
    w = csv.writer(FILE)
  except:
    logging.error("createCSVForPitchTrace(): Exception: file could not be opened ({})".format(absFilePath))
    return None


  ###
  # Get PitchTrace Data

  data = form.cleaned_data

  adaptInterface = AdaptInterface()

  pitchTraceData = adaptInterface.GetAudioPitchTraceData(
    mediaFile,
    data['pitchTraceType'],
    data['startTime'],
    data['endTime'],
    data['numTimeFramesPerSecond'],
    data['numFrequencyBands'],
    data['dampingFactor'],
    data['spectraMinFrequency'],
    data['spectraMaxFrequency'],
    data['numSamplePoints'],
    data['minCorrelation'],
    data['entropyThreshold'],
    data['pitchTraceStartFreq'],
    data['pitchTraceEndFreq'],
    data['minEnergyThreshold'],
    data['tolerance'],
    data['inverseFreqWeight'],
    data['maxPathLengthPerTransition'],
    data['windowSize'],
    data['extendRangeFactor'])


  if pitchTraceData is None:
    return None

  ###
  # Write Header

  header = (
             'fileTime',
             'frequency',
             'energy',
           )

  # add settings
  header = header + ('',)
  header = header + ('MediaFile: {}'.format(mediaFile.id),)
  settings_keys = [
                    'pitchTraceType',
                    'numTimeFramesPerSecond',
                    'numFrequencyBands',
                    'dampingFactor',
                    'spectraMinFrequency',
                    'spectraMaxFrequency',
                  ]
  if data['pitchTraceType'] == 'FUNDAMENTAL':
    settings_keys.extend([
                    'numSamplePoints',
                    'minCorrelation',
                    'entropyThreshold',
                    'pitchTraceStartFreq',
                    'pitchTraceEndFreq',
                    'minEnergyThreshold',
                    'tolerance',
                    'inverseFreqWeight',
                    'maxPathLengthPerTransition',
                    'windowSize',
                    'extendRangeFactor',
                  ])
  for k in settings_keys:
    header = header + (k + ': {}'.format(data[k]),)

  try:
    w.writerow(header)
  except:
    logging.warning('createCSVForPitchTrace(): Exception: could not write first row')
    return None


  ###
  # Write out PitchTrace Data

  logging.info(repr(pitchTraceData))

  pitchTraceData = pitchTraceData['pitchTraceData']

  for frame in sorted(pitchTraceData['frames'].keys(), key=float):
    w.writerow((
      frame,
      pitchTraceData['frames'][frame]['frequency'],
      pitchTraceData['frames'][frame]['energy'],
      ))

  FILE.close()
  return relFilePath
