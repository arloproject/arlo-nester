import contextlib
import logging
import time
import timeit

from django.contrib.auth.decorators import login_required
from django.shortcuts import render
from django.http import HttpResponseForbidden
from django.db.models import Q
from django import forms
from django.conf import settings

from tools.models import UserSettings, Project, Tag, TagClass, TagSet, RenameTagClassForm
from .userPermissions import ARLO_PERMS
from .project import getTagSetTagClasses
from .spectra import generateSpectraImage


class CatalogForm(forms.Form):
  gain = forms.FloatField('gain', initial=1.0)
  numFramesPerSecond = forms.FloatField('numFramesPerSecond', initial=25.0)
  numFrequencyBands = forms.IntegerField('numFrequencyBands', initial=64)
  spectraDampingFactor = forms.FloatField('spectraDampingFactor', initial=0.02)

  tagsPerPage = forms.IntegerField(label='userTagsPerPage', initial=20)
  tagPageNumber = forms.IntegerField(label='userTagPageNumber', initial=1)

  haveClassesChanged = forms.IntegerField(label='haveClassesChanged', initial=0, widget=forms.HiddenInput, required=False)
  sortBy = forms.ChoiceField(label='sortBy',
    choices=[('strengthDown', 'Strength(Descending)'),
             ('strengthUp', 'Strength(Ascending)'),
             ('cron', 'Chronological Order'),
             ('realTime', 'Real Time')])

  showUserTags = forms.BooleanField(label='Show User Tags', required=False, initial=True)
  showMachineTags = forms.BooleanField(label='Show Machine Tags', required=False, initial=True)
  discoveryMode = forms.IntegerField(label='Discovery Mode', initial=0, widget=forms.HiddenInput, required=False)

  tagSets    = forms.ModelMultipleChoiceField(TagSet.objects.none(), widget=forms.CheckboxSelectMultiple, required=True)
  tagClasses = forms.ModelMultipleChoiceField(TagClass.objects.none(), widget=forms.CheckboxSelectMultiple, required=True)



## Web interface to display a 'catalog' of tags. A GET will will display the
# CatalogForm, and the subsequent POST will retrieve the data.
# @param request The Django HTTP request object
# @param projectId The database ID of the Project.
# @return Django HTTP response object

@login_required
def catalog(request, projectId):

  user = request.user
  logging.info("Retrieving Catalog: user = " + user.__str__() + " projectId = " + projectId.__str__())
  userSettings = UserSettings.objects.get(user=user, name='default')
  project = Project.objects.get(id=projectId)

  # Check Permissions
  if not project.userHasPermissions(user, ARLO_PERMS.READ):
    return HttpResponseForbidden("Forbidden")

  ###
  # tagClassList is our main return dataset
  # this is a list of Tuples
  #   [ (tagClassId, tagClassName, tags), ]
  #   tagClassId int
  #   tagClassName string
  #   tags - A list of dictionaries
  #     {
  #        'id':,
  #        'displayName':,
  #        'userTagged':,
  #        'machineTagged':,
  #        'imagePath':,
  #      }
  tagClassList = []

  tagClassIDList = []


  if request.method == 'POST': # If the form has been submitted...

    form = CatalogForm(request.POST) # A read-only form bound to the POST data

    # Select Form Fields
    form.fields['tagSets'] = forms.ModelMultipleChoiceField(project.getTagSets(include_hidden=False), required=True, widget=forms.SelectMultiple(attrs={'size':11}))
    form.fields['tagClasses'] = forms.ModelMultipleChoiceField(project.getTagClasses(), required=True, widget=forms.SelectMultiple(attrs={'size':19}))

    if form.is_valid(): # if all validation rules passed

      #######################
      # Settings

      # data converted to list, not passed back
      # data[] only used in the settings section, newData contains modified values
      data = form.cleaned_data

      gain = data['gain']
      spectraDampingFactor = data['spectraDampingFactor']
      numFramesPerSecond = data['numFramesPerSecond']
      numFrequencyBands = data['numFrequencyBands']
      sortBy = data['sortBy']
      tagsPerPage = data['tagsPerPage']

      ##
      # Navigation

      method = data.get('method')
      showUserTags = data.get('showUserTags', False)
      showMachineTags = data.get('showMachineTags', False)

      tagPageNumber = data['tagPageNumber']
      if data['haveClassesChanged'] == 0:  # this is updated by JavaScript
        if method == 'nextPage':
          tagPageNumber += 1
        if method == 'previousPage':
          if (data['tagPageNumber'] - 1) > 0:
            tagPageNumber -= 1

      else:
        tagPageNumber = 1


      # static spectra settings
      spectraBorderWidth = 0
      spectraTopBorderHeight = 0
      timeScaleHeight = 0

      ##
      # Get Selected Libraries, TagSets, and TagClasses

      selectedTagSets = data['tagSets']
      selectedTagClasses = data['tagClasses']


      ##
      # debug logging settings

      logging.debug("^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^")
      logging.debug("Catalog Form Post Data: " + str(data))
      logging.debug("Method:                 " + method.__str__())
      logging.debug("showUserTags:           " + showUserTags.__str__())
      logging.debug("showUserTags:           " + showUserTags.__str__())
      logging.debug("HaveClassesChanged:     " + data['haveClassesChanged'].__str__())
      logging.debug("tagPageNum:             " + tagPageNumber.__str__())
      logging.debug("tagsPerPage:            " + tagsPerPage.__str__())
      logging.debug("Gain:                   " + gain.__str__())
      logging.debug("spectraDampingFactor:   " + spectraDampingFactor.__str__())
      logging.debug("numFramesPerSecond:     " + numFramesPerSecond.__str__())
      logging.debug("numFrequencyBands:      " + numFrequencyBands.__str__())
      logging.debug('sortBy:                 ' + sortBy.__str__())
      logging.debug('TagSets:                ' + selectedTagSets.__str__())
      logging.debug('TagClasses:             ' + selectedTagClasses.__str__())

      #############################
      # Main loop to retrieve tags

      # FYI, this loop goes pretty much to the end...
      for tagClass in selectedTagClasses:

        tagClassIDList.append(tagClass.id)


        ###
        # Get Tags

        # NOTE the use of the select_related to also pull in the
        # mediaFile object. Optimization for the sort function
        with timed_execution("Querying Tags - {}".format(tagClass.className)):
          tags = Tag.objects.select_related('mediaFile').filter(
                                  tagClass=tagClass,
                                  tagSet__in=selectedTagSets,
                                  )
        # userTag / machineTag logic
        # Have to use Q to build an ORing query for logic to satisfy showUserTags
        # and showMachineTags fields in catalog form. Tried doing this with Q() in
        # the above query, but didn't get it working, and went the easy route
        if showUserTags and showMachineTags:
          tags = tags.filter(Q(userTagged=True) | Q(machineTagged=True))
        else:
          if showUserTags:
            tags = tags.filter(userTagged=True)
          elif showMachineTags:
            tags = tags.filter(machineTagged=True)
          else: # Guess we don't want any tags...
            tags = Tag.objects.none()

        ###
        # Sort Tags

        # NOTE - Possible Performance Issue - To compute an accurate page number /
        # position, we basically have to sort all tags first, then select the ones
        # for display. This may get out of hand for some projects

        # NOTE that this returns a regular list, not a querySet
        # change querysets into list to simplify manipulation later(in template)
        with timed_execution("Sort Tags"):
          tags = sortTags(tags, sortBy)


        ###
        # compute Tag Range / Page Numbers

        with timed_execution("Compute Tag Range / Page Numbers - {}".format(tagClass.className)):
          numTags = len(tags)
          logging.debug('Number of tags: ' + str(numTags))
          startTagIndex = tagsPerPage * (tagPageNumber - 1)
          endTagIndex = startTagIndex + tagsPerPage
          if numTags > 0:
            if startTagIndex >= 0 and endTagIndex <= numTags:
              tags = tags[startTagIndex:endTagIndex]
            elif startTagIndex < numTags and startTagIndex >= 0 and endTagIndex > numTags:
              tags = tags[startTagIndex:]
            else:
              tags = Tag.objects.none()
          else:
            tags = Tag.objects.none()

        logging.info("tagRange: " + startTagIndex.__str__() + " to " + endTagIndex.__str__())


        ###
        # Build Tag data return list for this class

        tagInfoList = []
        with timed_execution("Build Tag Spectras / Info - {}".format(tagClass.className)):
          for tag in tags:

            ###
            #  Build Spectra

            spectra_params = {
              'userSettings':userSettings,
              'audioFile': tag.mediaFile,

              'startTime': tag.startTime,
              'endTime': tag.endTime,
              'gain': gain,
              'numFramesPerSecond': numFramesPerSecond,
              'numFrequencyBands':numFrequencyBands,
              'dampingFactor':spectraDampingFactor,

              'minFrequency':userSettings.catalogViewSpectraMinimumBandFrequency,
              'maxFrequency':userSettings.catalogViewSpectraMaximumBandFrequency,

              'simpleNormalization':True,
              'showTimeScale':False,
              'showFrequencyScale':False,
              'borderWidth':spectraBorderWidth,
              'topBorderHeight':spectraTopBorderHeight,
              'timeScaleHeight':timeScaleHeight,
              'catalogSpectraNumFrequencyBands':numFrequencyBands}

            (success, imageFilePath, audioSegmentFilePath) = generateSpectraImage(**spectra_params)
            if not success:
              logging.error("CATALOG - Error While Generating Spectra for TagID: " + tag.id.__str__())


            # Add to list
            tagInfoList.append({
                'id': tag.id,
                'displayName': tag.tagClass.displayName,
                'userTagged': tag.userTagged,
                'machineTagged': tag.machineTagged,
                'imagePath': imageFilePath,
                'audioSegmentFilePath': audioSegmentFilePath
                })

          tagClassList.append((tagClass.id, tagClass.className, tagInfoList))


      #######################
      # Build response form

      # new Data to be passed back to form, a QueryDict object
      newData = request.POST.copy()

      # update data
      newData.__setitem__('tagPageNumber', tagPageNumber)

      # build new form
      newForm = CatalogForm(newData) # Rebuild the form with the modified data

      # add Select Form Fields

      with timed_execution("Build New Form Fields"):
        newForm.fields['tagSets'] = forms.ModelMultipleChoiceField(project.getTagSets(include_hidden=False), required=True, widget=forms.SelectMultiple(attrs={'size':11}))
        newForm.fields['tagClasses'] = forms.ModelMultipleChoiceField(
           TagClass.objects.filter(tag__tagSet__in=selectedTagSets).distinct(),
           required=True,
           widget=forms.SelectMultiple(attrs={'size':19}))

      if newForm.is_valid():                             ####
        logging.debug("Form still valid 1")           ####
      else:
        logging.debug("Form NOT still valid 1")           ####

      form = newForm


    else: # fall to here if form not valid
      logging.debug("POST Form Not Valid")

  else: # Not a POST request - Show empty form

    # select all fields by default
    form = CatalogForm(initial={
#      'tagSets': project.getTagSets(),
#      'tagClasses': project.getTagClasses(),
    })

    # Select Form Fields
    form.fields['tagSets'] = forms.ModelMultipleChoiceField(project.getTagSets(include_hidden=False), required=True, widget=forms.SelectMultiple(attrs={'size':11}))
#    form.fields['tagClasses'] = forms.ModelMultipleChoiceField(project.getTagClasses(), required=True)
    form.fields['tagClasses'] = forms.ModelMultipleChoiceField(TagClass.objects.none(), required=True, widget=forms.SelectMultiple(attrs={'size':19}))

    # Default Values
    form.fields['numFramesPerSecond'].initial = userSettings.catalogViewSpectraNumFramesPerSecond
    form.fields['numFrequencyBands'].initial = userSettings.catalogViewSpectraNumFrequencyBands
    form.fields['spectraDampingFactor'].initial = userSettings.catalogViewSpectraDampingFactor


  ###
  # All branches return to here

  # map TagClasses to TagSets for display of only applicable TagClasses based on TagSets
  tagSetsTagClasses = []
  with timed_execution("Get TagSets TagClasses"):
    for ts in project.getTagSets(include_hidden=False):
      ts_tc = getTagSetTagClasses(ts)
      _ = list(ts_tc)  # Force Evaluation of lazy QS for timing
      tagSetsTagClasses.append( (ts, ts_tc))

  renameForm = RenameTagClassForm()

  with timed_execution("Get Project TagClasses"):
    projectTagClasses = project.getTagClasses()
    _ = list(projectTagClasses)  # Force Evaluation of lazy QS for timing

  with timed_execution("Render and Return"):
    return render(request, 'tools/catalog.html', {
      'apacheRoot':settings.APACHE_ROOT,
      'user':user,
      'project': project,
      'form':form,
      'renameForm': renameForm,
      'tagClassList':tagClassList,
      'tagClassIDList': tagClassIDList,
      'tagSetsTagClasses': tagSetsTagClasses,
      'projectTagClasses': projectTagClasses,
      })


## Sort tags by the real-time of the tag. For each tag,
# calculate the real-time by the realStartTime of the file,
# and the file-startTime of the tag, and sort accordingly
# @param tags A Django queryset of the tags to sort
# @param sortOrder A string specifying the sort order - strings defined by the select box in models
# @return A list of tag objects (not a queryset)

def sortTags(tags, sortBy):

  if sortBy == 'realTime':
    # use a custom sort

    tagTimeList = []
    fileStartTimes = dict()

    for tag in tags:

      # Using some caching of the fileStartTime, since many
      # tags will probably come from each file. We compute
      # an array of audioFiles / startTime in seconds

      fileId = tag.mediaFile.id

      if fileId in fileStartTimes:  # have we seen this mediaFile before
        fileStartTime = fileStartTimes[fileId]

      else: # we haven't seen this mediaFile before...
        # example time format: 2010-09-29 06:00:00
        fileStartTime = tag.mediaFile.realStartTime

        if fileStartTime is None:
          fileStartTime = 0
        else:
          try:
            fileStartTime = time.mktime(fileStartTime.timetuple())
          except:
            logging.error("*** Failed Converting Date: " + fileStartTime)
            fileStartTime = 0
          fileStartTimes[fileId] = fileStartTime # cache entry

      tagTime = fileStartTime + tag.startTime

      tagTimeList.append((tagTime, tag))

    tagTimeList = sorted(tagTimeList, key=lambda tagTimeList: tagTimeList[0])

    tagList = []
    for (tagTime, tag) in tagTimeList:
      tagList.append(tag)

    return tagList

  # these use the QuerySet functions to sort, convert to a list afterward
  elif sortBy == 'cron':
    tags = tags.order_by("mediaFile__alias", "startTime")
  elif sortBy == 'strengthUp':
    tags = tags.order_by("strength", "id")
  elif sortBy == 'strengthDown':
    tags = tags.order_by("-strength", "id")

  # convert to List
  tagList = []
  for tag in tags:
    tagList.append(tag)
  return tagList


@contextlib.contextmanager
def timed_execution(name):
    start_time = timeit.default_timer()
    yield None
    logging.info("Catalog - '%s' in %f seconds", name, timeit.default_timer() - start_time)
