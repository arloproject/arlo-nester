from django.core.exceptions import ObjectDoesNotExist, MultipleObjectsReturned

from tools.models import MediaFileUserMetaDataField, MediaFileUserMetaData


## @package
# This is UserMetaData, user-defined MetaData that is stored along with
# MediaFiles, but not necessary directly related to ARLO functions (though
# we'll build out tools to interact with this, such as searching for files
# that match specific criteria).
#
# We'll make several functions for accessing MetaData - the underlying format
# may change in the future, so we want to abstract this as much as possible.
# (e.g., we may change from storing this in MySQL to using a document-based
# database such as Mongo.)
#
# Individual Access
# -----------------
#   getUserMetaDataEntry(mediaFile, fieldName):
#      return the value of the record
#      return None if not found, value is NULL, or if the field does not exist
#
#   setUserMetaDataEntry(mediaFile, fieldName, value, autoAddField=True):
#      Looks for an existing entry, and updates it. Will add if doesn't exist.
#      If autoAddField is False, and a corresponding field does not exist, return False.
#
# Multiple access, one MediaFile
# ---------------
#   getMediaFileUserMetaDataDictionary(mediaFile):
#      returns all metaData entries for a mediaFile as a dictionary
#      {fieldName: value}
#
#   getMediaFileUserMetaDataDictionaryList(mediaFile):
#      returns all metaData entries for a mediaFile as a dictionary (with additional data)
#      [{id: <entry id>, fieldName: <name>, value: <value>}]
#
#   addOrUpdateMediaFileUserMetaData(mediaFile, data, autoAddFields=True):
#      add, or update if records exist, a list of data for a file.
#      data is a Dictionary {fieldName: value}
#      NOTE: will update existing records or add new... will not delete existing records
#      e.g., if records 1 and 2 exist, and 2 and 3 are passed to this
#      function, 1, 2, and 3 will exist at the end
#      If autoAddFields is False, and a field is passed that does not exist, return False.
#
# Multiple Access, Library
# ---------------
#   getLibraryUserMetaDataFields(library):
#      return a list of UserMetaDataFields defined for a Library, as a Django QuerySet
#
# Helper Functions
# ---------------
#   getUserMetaDataField(fieldName, library)
#      Get the MediaFileUserMetaDataField from a Library and field name.
#


# ############################################### #
#                                                 #
#                 Helper Functions                #
#                                                 #
# ############################################### #


## Get the MediaFileUserMetaDataField from a Library and field name.
#
# @param fieldName String name of the UserMetaData Field.
# @param library Library object in which to search for the field.
# @param autoAddField If True, add the field to the Library if it does not exist.
# @return MediaFileUserMetaDataField Object, None if not found.

def getUserMetaDataField(fieldName, library, autoAddField=False):
  try:
    return MediaFileUserMetaDataField.objects.get(name=fieldName, library=library)
  except ObjectDoesNotExist:
    if autoAddField:
      field = MediaFileUserMetaDataField(name=fieldName, library=library)
      field.save()
      return field

  return None


# ############################################### #
#                                                 #
#                Individual Access                #
#                                                 #
# ############################################### #

## Get a UserMetaData Entry from a MediaFile.
#
# @param mediaFile MediaFile Object
# @param fieldName String Name of the UserMetaData Field.
# @return String value of the UserMetaData entry, None if not found.

def getUserMetaDataEntry(mediaFile, fieldName):
  field = getUserMetaDataField(fieldName, mediaFile.library)
  if field is None:
    return None

  try:
    userMetaData = MediaFileUserMetaData.objects.get(metaDataField=field, mediaFile=mediaFile)
    return userMetaData.value
  except MultipleObjectsReturned:
    return None
  except:
    return None


## Set a UserMetaData Entry for a MediaFile.
#
# @param mediaFile MediaFile Object
# @param fieldName String Name of the UserMetaData Field.
# @param value String value to set for the field.
# @param autoAddField If the UserMetaDataField does not exist, create it if True.
# @return True on Success, False on any failure.

def setUserMetaDataEntry(mediaFile, fieldName, value, autoAddField=True):
  field = getUserMetaDataField(fieldName, mediaFile.library, autoAddField)
  if field is None:
    return False

  try:
    userMetaData = MediaFileUserMetaData.objects.get(metaDataField=field, mediaFile=mediaFile)
    userMetaData.value = value
    userMetaData.save()
  except ObjectDoesNotExist:
    userMetaData = MediaFileUserMetaData(
                     metaDataField=field,
                     mediaFile=mediaFile,
                     value=value)
    userMetaData.save()
  except MultipleObjectsReturned:
    return False
  except:
    return False

  return True


# ############################################### #
#                                                 #
#         Multiple access, one MediaFile          #
#                                                 #
# ############################################### #


## Get all UserMetaData entries from a MediaFile.
#
# @param mediaFile MediaFile Object
# @return all metaData entries for a mediaFile as a dictionary
#         {fieldName: value}

def getMediaFileUserMetaDataDictionary(mediaFile):
  entries = MediaFileUserMetaData.objects.filter(mediaFile=mediaFile)

  results = {}
  for entry in entries:
    results[entry.metaDataField.name] = entry.value

  return results


## Get all UserMetaData entries from a MediaFile.
#
# @param mediaFile MediaFile Object
# @return List of dictionaries, one dictionary per entry:
#         [{id: <entry id>, fieldName: <name>, value: <value>}]

def getMediaFileUserMetaDataDictionaryList(mediaFile):
  entries = MediaFileUserMetaData.objects.filter(mediaFile=mediaFile)

  results = []
  for entry in entries:
    results.append({
        'id': entry.id,
        'fieldName': entry.metaDataField.name,
        'value': entry.value,
        })

  return results


## Get all UserMetaData entries from a MediaFile.
#
# @param mediaFile MediaFile Object
# @return all metaData entries for a mediaFile as a Django Queryset

def getMediaFileUserMetaData(mediaFile):
  return MediaFileUserMetaData.objects.filter(mediaFile=mediaFile)


## Set Multiple UserMetaData entries for a MediaFile.
#
# Add or update UserMetaData records for a MediaFile
# @note If they don't exist, new fields will automatically be created in the Library.
# @note This is not an atomic transaction, such that on error, none, some, or
#       all of the data may have been saved.
# @note This will update existing records or add new, but it will not delete
#       existing records that are not contained within this list (e.g., if
#       records 1 and 2 exist, and 2 and 3 are passed to this function, 1, 2,
#       and 3 will exist at the end).
# @param mediaFile MediaFile Object
# @param data Dictionary of entries, as {fieldName: value}
# @return True on success, False on any error

def addOrUpdateMediaFileUserMetaData(mediaFile, data, autoAddFields=True):
  for fieldName in data:
    if not setUserMetaDataEntry(mediaFile, fieldName, data[fieldName], autoAddFields):
      return False
  return True


# ############################################### #
#                                                 #
#           Multiple access, one Library          #
#                                                 #
# ############################################### #


## Get a list of UserMetaDataFields defined for a Library.
#
# @param library Library Object.
# @return A list of UserMetaDataFields, as a Django QuerySet.

def getLibraryUserMetaDataFields(library):
  return MediaFileUserMetaDataField.objects.filter(library=library)


# ############################################### #
#                                                 #
#                 Interface Views                 #
#                                                 #
# ############################################### #


## Edit a UserMetaDataEntry for a MediaFile.
#
# @param request The Django HTTP request object.
# @param mediaFileId The database Id of the MediaFile for which we're editing the entry.
# @return Django HTTP response.

#@login_required
#def editUserMetaDataEntry(request, mediaFileId, userMetaDataEntryId
