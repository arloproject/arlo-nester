import logging
import datetime

from django.contrib.auth.decorators import login_required
from django.http import HttpResponseRedirect, HttpResponseForbidden, HttpResponseServerError, JsonResponse
from django.shortcuts import render, redirect
from django import forms
from django.views.decorators.http import require_http_methods
from django.contrib.contenttypes.models import ContentType
from django.contrib.auth.models import User
from django.conf import settings
from django.core.paginator import Paginator, EmptyPage, PageNotAnInteger

from tools.models import Library, Project, Jobs, JobTypes, JobStatusTypes, JobParameters, SensorArray, ArloUserGroup, MediaFile
from . import userPermissions
from .viewMetaData import getMetaDataEntry
from .userPermissions import ARLO_PERMS


filesPerPage = 10

## Create a new Library owned by 'user'
#
# @param user User object that owns the Library.
# @param name The name of the new Library.
# @param notes Notes stored along with the Library.
# @return The new Library object if successful, None if a conflicting Library name exists.

def createLibrary(user, name, notes):
  logging.debug("Creating Library for " + user.__str__() + " called " + name.__str__())
  try:
    library = Library.objects.get(user=user,name=name)
    logging.warning("Library already exists")
    return None
  except:
    library = Library(user=user,name=name,notes=notes)
    library.save()
    return library


## Display all Libraries owned or accessible by a user (/tools/userLibraries).
#
# @param request The Django HTTP request object
# @return Django HTTP response object

@login_required
def userLibraries(request):
  user = request.user

  libraries = Library.objects.getUserLibraries(user)

  return render(request, 'tools/userLibraries.html',
                           {'user': user,
                            'libraries':libraries,
                            'apacheRoot':settings.APACHE_ROOT,
                            'section': 'libraries'
                            })


###
# Djangeo form for creating a new Library

class StartNewLibraryForm(forms.Form):
  name = forms.CharField(max_length=255)
  notes = forms.CharField(widget=forms.Textarea, required=False)



## Start a new Library.
# GET request returns the create Form, POST processes it.
# @param request The Django HTTP request object
# @return Django HTTP response object

@login_required
def startNewLibrary(request):
  user = request.user
  logging.info("Starting New Library: user = " + user.__str__())

  if request.method == 'POST': # If the form has been submitted...

    form = StartNewLibraryForm(request.POST) # A form bound to the POST data

    if form.is_valid(): # All validation rules pass
      name = form.cleaned_data['name']
      notes = form.cleaned_data['notes']

      newLib = createLibrary(user, name, notes)
      if newLib is None:
        return HttpResponseServerError("Failed - Unknown Error Creating Library")

      return HttpResponseRedirect('userLibraries')
    else:
      return render(request, 'tools/startNewLibrary.html', {
                            'form': form,
                            'apacheRoot':settings.APACHE_ROOT,
                            'user':user,
                            })

  else:
    form = StartNewLibraryForm()

    return render(request, 'tools/startNewLibrary.html', {
                            'form': form,
                            'apacheRoot':settings.APACHE_ROOT,
                            'user':user,
                            })


###
# Form to add new SensorArray to the Library

class AddSensorArrayForm(forms.Form):
  name = forms.CharField(max_length=255, required=True)


###
# Add a new SensorArray to a Library.
#
# @param request The Django HTTP request object
# @param libraryId The database ID of the Library to display.
# @return Django HTTP response object, redirect to Library on success.

@login_required
@require_http_methods(['POST'])
def libraryAddSensorArray(request, libraryId):
  user = request.user
  logging.info("libraryAddSensorArray: user = " + user.__str__())

  library = Library.objects.get(id=libraryId)

  # Check Library Write Permissions
  if not library.userHasPermissions(user, ARLO_PERMS.WRITE):
    return HttpResponseForbidden("Forbidden - You do not have access to this Library.")

  # add a new sensorArray
  form = AddSensorArrayForm(request.POST)
  if form.is_valid():
    data = form.cleaned_data
    newSensorArray = SensorArray(
        name=data['name'],
        library=library,
        user=user,
        layoutRows=0,
        layoutColumns=0)
    newSensorArray.save()

    # redirect back to Library View
    return redirect('libraryFiles', libraryId=libraryId)

  # Return Form, presumably with Error
  return render(request, 'tools/genericForm.html', {
                            'user': user,
                            'form':form,
                            'formTitle': "Add SensorArray to Library",
                            })


## Delete a sensor array.
#
# @param request The Django HTTP request object
# @param id The database ID of the sensor array to display.
# @return JSON object

@login_required
def deleteSensorArray(request):
  user = request.user
  # TODO check permissions
  sensorArrayId = request.GET.get('id', None)

  sensorArray = SensorArray.objects.get(id=sensorArrayId)
  sensorArray.delete()

  data = {
    'id': sensorArrayId,
  }

  return JsonResponse(data)


## Display all Files in a Library.
#
# @param request The Django HTTP request object
# @param libraryId The database ID of the Library to display.
# @return Django HTTP response object

@login_required
def libraryFiles(request, libraryId):
  user = request.user
  library = Library.objects.get(id=libraryId)

  # Check user permissions
  if not library.userHasPermissions(user, ARLO_PERMS.READ):
    return HttpResponseForbidden("Forbidden - You do not have access to this Library.")

  ## If deleting files before processing page
  if request.GET.get('deleteMediaFiles', None):
    mediaFilesToDelete = request.GET.get('deleteMediaFiles', None).split("//")
    for mediaFile in mediaFilesToDelete:
      MediaFile.objects.get(id=mediaFile).delete()

  mediaFilesAll = library.mediafile_set.all()

  ## Sort -- still working on how this should work across views.

  sort = request.GET.get('sort', None)
  if sort is None:
    sort = 'uploadDate'

  if sort == 'uploadDate':
    mediaFilesAll = mediaFilesAll.order_by(sort).reverse()
  elif sort == 'tagCount':
    mediaFilesAll = mediaFilesAll.order_by('-tagCount')
  else:
    mediaFilesAll = mediaFilesAll.order_by(sort)



  paginator = Paginator(mediaFilesAll, settings.ARLO_UI_PAGE_SIZE)

  page = request.GET.get('page')

  try:
    mediaFiles = paginator.page(page)
  except PageNotAnInteger:
    # If page is not an integer, deliver first page.
    mediaFiles = paginator.page(1)
  except EmptyPage:
    # If page is out of range (e.g. 9999), deliver last page of results.
    mediaFiles = paginator.page(paginator.num_pages)

  libraryFiles = []

  for mediaFile in mediaFiles:

    projectList = []
    for project in mediaFile.getContainingProjects():
      projectList.append( {
        'name': project.name,
        'id': project.id
      })

    libraryFiles.append( {
        'id': mediaFile.id,
        'library': mediaFile.library,
        'active': mediaFile.active,
        'startTime': getMetaDataEntry(mediaFile, 'startTime'),
        'alias': mediaFile.alias,
        'filePath': mediaFile.getPath(),
        'sensor': mediaFile.sensor,
        'projectList': projectList,
        'duration': round(float(getMetaDataEntry(mediaFile, 'durationInSeconds')))
        })

  mediaFilesAllIDs = mediaFilesAll.values_list('id', flat=True)

  libraryPermissions = userPermissions.getLibraryPermissions(library)
  user_type = ContentType.objects.get_for_model(User)
  libraryUserPermissions = libraryPermissions.filter(bearer_type=user_type)
  group_type = ContentType.objects.get_for_model(ArloUserGroup)
  libraryGroupPermissions = libraryPermissions.filter(bearer_type=group_type)

  ###
  # User's Own Projects

  userProjects = Project.objects.filter(user=user).order_by('name')
  userProjectInfos = []

  for project in userProjects:

    sharedUserInfos = []
    # sharedUsers= userPermissions.getAdditionalProjectUsers(project)
    sharedUsers = project.getProjectUsers()

    for sharedUser in sharedUsers:
      sharedUserInfos.append({
        'user': sharedUser,
        'canRead': project.userHasPermissions(sharedUser, userPermissions.ARLO_PERMS.READ),
        'canWrite': project.userHasPermissions(sharedUser, userPermissions.ARLO_PERMS.WRITE),
        })

    userProjectInfos.append( {
      'project': project,
      'sharedUserInfos': sharedUserInfos,
      })

  ## Display Options

  MediaFileListColumns = {
    'name': True,
    'duration': True,
    'project': True,
    'library':False,
  }

  listSortOptions = {
    'uploadDate': True,
    'alias': True,
    'duration': True,
    'userTagCount': False,
    'machineTagCount': False,
    }

  searchPrompt = 'Search files in library \'' + library.name + '\'...'
  allFilesLabel = 'library \'' + library.name + '\''


  return render(request, 'tools/libraryFiles.html',
                           {'user': user,
                            'userProjects': userProjectInfos,
                            'library':library,
                            'MediaFileList': libraryFiles,
                            'MediaFilesPaginator': mediaFiles,
                            'apacheRoot':settings.APACHE_ROOT,
                            'sensorArrays': library.sensorarray_set.all(),
                            'addSensorArrayForm': AddSensorArrayForm(),
                            'libraryUserPermissions': libraryUserPermissions,
                            'libraryGroupPermissions': libraryGroupPermissions,
                            'MediaFileListColumns': MediaFileListColumns,
                            'currentLibraryId': library.id,
                            'fileListHeader': 'Library Audio Files',
                            'section': 'libraries',
                            'searchPrompt': searchPrompt,
                            'totalFiles': len(mediaFilesAll),
                            'allFilesLabel': allFilesLabel,
                            'mediaFilesAllIDs': mediaFilesAllIDs,
                            'listSortOptions': listSortOptions,
                            'listSort': sort,
                            })


## View and edit library permissions
#
# @param request The Django HTTP request object
# @param libraryId The database ID of the Library to display.
# @return Django HTTP response object

@login_required
def libraryPermissions(request, libraryId):


  user = request.user
  library = Library.objects.get(id=libraryId)


  # Check user permissions
  if not library.userHasPermissions(user, ARLO_PERMS.READ):
    return HttpResponseForbidden("Forbidden - You do not have access to this Library.")


  libraryPermissions = userPermissions.getLibraryPermissions(library)
  user_type = ContentType.objects.get_for_model(User)
  libraryUserPermissions = libraryPermissions.filter(bearer_type=user_type)
  group_type = ContentType.objects.get_for_model(ArloUserGroup)
  libraryGroupPermissions = libraryPermissions.filter(bearer_type=group_type)




  return render(request, 'tools/libraryPermissions.html',
                           {'user': user,
                            'library': library,
                            'libraryUserPermissions': libraryUserPermissions,
                            'libraryGroupPermissions': libraryGroupPermissions,
                            'section': 'libraries',
                            })

class LaunchLibraryValidationForm(forms.Form):
  project = forms.ModelChoiceField(Project.objects.none(), empty_label=None)
  library = forms.ModelChoiceField(Library.objects.none(), empty_label=None)


## Launch a LibraryMediaFileValidation Task.
#
# @param request The Django HTTP request object
# @return Django HTTP response object, redirect to the Job that was launched.

@login_required
def launchLibraryValidation(request):
  user = request.user

  if request.method == 'POST': # If the form has been submitted...

    form = LaunchLibraryValidationForm(request.POST) # A form bound to the POST data
    form.fields['project'] = forms.ModelChoiceField(Project.objects.getUserProjects(user).order_by('name'), required=True)
    form.fields['library'] = forms.ModelChoiceField(Library.objects.getUserLibraries(user).order_by('name'), required=True)

    if form.is_valid(): # All validation rules pass
        data = form.cleaned_data

        project = data['project']
        library = data['library']

        # Check user permissions
        if not project.userHasPermissions(user, ARLO_PERMS.READ):
          return HttpResponseForbidden("Forbidden - You do not have access to this Project.")

        if not library.userHasPermissions(user, ARLO_PERMS.READ):
          return HttpResponseForbidden("Forbidden - You do not have access to this Library.")

        # start job in unknown status so that it doesn't kick off 'til we're done
        j = Jobs(
          user=request.user,
          project=project,
          creationDate=datetime.datetime.now(),
          name='Library Validation',
          type=JobTypes.objects.get(name="LibraryMediaFileValidation"),
          status=JobStatusTypes.objects.get(name="Unknown"),
          )
        j.save()

        # add job parameters
        JobParameters(job=j, name="libraryId", value=str(library.id)).save()

        # ready for QueueRunner to pick it up
        j.status=JobStatusTypes.objects.get(name="Queued")
        j.save()

        # Redirect to the Job page
        return redirect('viewJob', jobID=j.id)

    else:
      return render(request, 'tools/launchLibraryValidation.html', {
                              'form': form,
                              'apacheRoot':settings.APACHE_ROOT,
                              'user':user,
                              })

  else:
    form = LaunchLibraryValidationForm()
    form.fields['project'] = forms.ModelChoiceField(Project.objects.getUserProjects(user).order_by('name'), required=True)
    form.fields['library'] = forms.ModelChoiceField(Library.objects.getUserLibraries(user).order_by('name'), required=True)

    return render(request, 'tools/launchLibraryValidation.html', {
                            'form': form,
                            'apacheRoot':settings.APACHE_ROOT,
                            'user':user,
                            })
