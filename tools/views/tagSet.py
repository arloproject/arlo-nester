import datetime
import logging

from django.contrib.auth.decorators import login_required
from django.http import HttpResponseRedirect, HttpResponseServerError, JsonResponse, HttpResponseForbidden, HttpResponseNotAllowed
from django.shortcuts import render
from django import forms
from django.conf import settings

from tools.models import Tag, TagSet, Project
from .userPermissions import ARLO_PERMS

class TagSetForm(forms.Form):
  name = forms.CharField(max_length=255)
  notes = forms.CharField(widget=forms.Textarea, required=False)


### disabled with Cassandra Updates - was previously disabled in GUI
#
#@login_required
#def tagAnalysis(request, projectName):
#  user = request.user
#  project = Project.objects.get(user=user, name=projectName)
#  logging.info("Starting Tag Analysis: user = " + user.__str__() + " projectName = " + projectName.__str__())
#
#  if request.method == 'POST': # If the form has been submitted...
#    form = TagAnalysisForm(request.POST) # A form bound to the POST data
#    if form.is_valid(): # All validation rules pass
#
#      data = form.cleaned_data
#
#      # removes existing items... don't think we need, nor should we have, this
#      #origBiases = TagAnalysisBias.objects.filter(user=user, name=data['name'], wasDeleted=False)
#
#      #if origBiases.count() > 0:
#        #results = Meandre.call(MeandreAction.stopTagAnalysis, {'id':origBiases[0].id})
#        #origBiases.delete()
#      #else:
#        #pass
#      tagAnalysisJob = Jobs(
#        user=user,
#        project=project,
#        creationDate=datetime.datetime.now(),
#        name=data['name'],
#        type=JobTypes.objects.get(name="TagAnalysis")
#        )
#      tagAnalysisJob.save()
#
#
#      params = {
#        'numFrequencyBands': data['numFrequencyBands'],
#        'numTimeFramesPerSecond': data['numTimeFramesPerSecond'],
#        'dampingRatio': data['dampingRatio'],
#        'minFrequency': data['minFrequency'],
#        'maxFrequency': data['maxFrequency'],
#        'freqAveragingWindowWidth' : data['freqAveragingWindowWidth'],
#        'numFramesToAverageNoise' : data['numFramesToAverageNoise'],
#        'updateTagPositions' : data['updateTagPositions']}
#
# NOTE if we re-enable this code, AddJobParameters needs to be updated
# to provide a list of values for each parameter
#      if not AddJobParameters(tagAnalysisJob, params):
#        logging.info("FAILED adding job parameters")
#
#      logging.info("### 4 ###")
#
#      results = Meandre.call(MeandreAction.startTagAnalysis, {'id':tagAnalysisJob.id})
#
#      if results[0]:
#				# Succeeded
#        logging.info(results[1])
#        return HttpResponseRedirect(settings.URL_PREFIX + '/tools/projectFiles/' + project.name)
#      else:
#        logging.info(results[1])
#        return HttpResponseServerError()
#
#
#    else:
#      return render_to_response('tools/tagAnalysis.html',
#        {
#        'user':user,
#        'apacheRoot':settings.APACHE_ROOT,
#        'projects':Project.objects.filter(user=user),
#        'form':form})
#
#  else:        #request method
#    # TODO - removing the apparently 'last used settings' feature for simplicity for now
#    #bias = TagAnalysisBias.objects.filter(user=user, project=project).order_by('creationDate')
#    #if bias.count() != 0:
#      #bias = bias[0]
#      #form = TagAnalysisForm(
#        #initial={'name':bias.name,
#                #'freqAveragingWindowWidth':bias.freqAveragingWindowWidth,
#                #'numFramesToAverageNoise':bias.numFramesToAverageNoise,
#                #'updateTagPositions':bias.updateTagPositions})
#    #else:
#      #form = TagAnalysisForm()
#
#    form = TagAnalysisForm()
#
#    return render_to_response('tools/tagAnalysis.html',
#        {
#        'projects':Project.objects.filter(user=user),
#        'user':user,
#        'apacheRoot':settings.APACHE_ROOT,
#        'form':form})


## Delete the specified TagSet, and all contained Tags.
# @param request The Django HTTP request object
# @param tagSetId The Database ID of the tagset to remove
# @param project The Project Object to which the TagSet belongs
# @return Django HTTP response object ( tagSets() )

@login_required
def deleteTagSet(request, tagSetId):
  user = request.user
  logging.info("Deleting Tag Set: user = " + user.__str__() + " tagSetId = " + tagSetId.__str__())

  tagSet = TagSet.objects.get(id=tagSetId)

  if not tagSet.userHasPermissions(user, ARLO_PERMS.WRITE):  # check user permissions
    return HttpResponseForbidden("Forbidden")

  Tag.objects.filter(tagSet=tagSet).delete()
  tagSet.delete()

  return tagSets(request)

## Display a list of all of the user's own TagSets
# @param request The Django HTTP request object
# @return Django HTTP response object

@login_required
def tagSets(request):
  user = request.user
  logging.info("Retrieving Tag Sets: user = " + user.__str__())

  return render(request, 'tools/tagSets.html', {
                            'user': user,
                            'apacheRoot':settings.APACHE_ROOT,
                            'tagSets': TagSet.objects.filter(user=user).order_by('name'),
                            })


## Display a list of all of the user's own TagSets
# @param request The Django HTTP request object
# @return Django HTTP response object

@login_required
def projectTagSets(request, projectId):
  user = request.user
  logging.info("Retrieving Tag Sets: user = " + user.__str__())
  project = Project.objects.get(id=projectId)

  return render(request, 'tools/tagSets.html', {
                            'user': user,
                            'apacheRoot':settings.APACHE_ROOT,
                            'tagSets': project.getTagSets(include_hidden=True),
                            })


## Create a new TagSet within the specified Project.
# Uses the TagSetForm. Returns the form on GET, processes it on POST
# @param request The Django HTTP request object
# @param projectId The Database ID of the Project in which to create the TagSet.
# @return Django HTTP response object (Redirect to the TagSets Page on Success)

@login_required
def startNewTagSet(request,projectId):
  user = request.user
  logging.info("Starting New Tag Set: user = " + user.__str__() + " projectId = " + projectId.__str__())

  project = Project.objects.get(id=projectId)

  # check permissions
  if not project.userHasPermissions(user, ARLO_PERMS.WRITE):
    return HttpResponseForbidden('Forbidden')

  if request.method == 'POST': # If the form has been submitted...

    form = TagSetForm(request.POST) # A form bound to the POST data

    if form.is_valid(): # All validation rules pass

      name  = form.cleaned_data['name']
      notes = form.cleaned_data['notes']
      date  = datetime.datetime.now()

      try:
        tagSet = TagSet.objects.get(name=name, project=project)
        return HttpResponseServerError("TagSet With This Name Already Exists")
      except:
        tagSet = TagSet(user=user, name=name, project=project, creationDate=date, notes=notes)
        tagSet.save()

      return HttpResponseRedirect('/tools/projectFiles/' + projectId)

    else:
      return render(request, 'tools/startNewTagSet.html', {
                                'user':user,
                                'form': form,
                                'apacheRoot':settings.APACHE_ROOT,
                                })
  else:
    form = TagSetForm()

    return render(request, 'tools/startNewTagSet.html', {
                              'user':user,
                              'apacheRoot':settings.APACHE_ROOT,
                              'form': form,
                              })



## Get a list of TagSets in a Project, and return a count of the tags in each TagSet
# @note The user and machine tag counts may be skewed. Here, machine tags are those that
# are also NOT user tags, so an exclusive OR.
# @param request The Django HTTP request. Note that this expects a GET method.
# @param projectId The database Id of a Project to search.
# @return JSON encoded list of dictionary [ {id, name, numUserTags, numMachineTags, totalTags } ].

@login_required
def getProjectTagSetsInfo(request, projectId):
  logging.info("getProjectTagSetsInfo projectId = " + projectId.__str__())

  if request.method != 'GET':
    return HttpResponseNotAllowed(['GET'])

  user = request.user
  project = Project.objects.get(id=projectId)

  # check user permissions
  if not project.userHasPermissions(user, ARLO_PERMS.READ):  # check user permissions
    return HttpResponseForbidden("Forbidden")

  tagSetsList = []
  for tagSet in project.getTagSets(include_hidden=False):
    numUserTags = Tag.objects.filter(tagSet=tagSet,userTagged=True).count()
    numMachineTags = Tag.objects.filter(tagSet=tagSet,machineTagged=True,userTagged=False).count()
    totalTags = Tag.objects.filter(tagSet=tagSet).count()

    tagSetDict = {
      'id': tagSet.id,
      'name': tagSet.name,
      'numUserTags': numUserTags,
      'numMachineTags': numMachineTags,
      'totalTags': totalTags,
      }
    tagSetsList.append(tagSetDict)

  return JsonResponse(tagSetsList, safe=False)



## View a TagSet and edit it's Notes.
# @param request The Django HTTP request. Note that this expects a GET method.
# @param tagSetId The database Id of the TagSet to show.
# @return Django HTTP response object.

@login_required
def viewTagSet(request, tagSetId):
  logging.info("viewTagSet tagSetId = " + tagSetId.__str__())

  user = request.user
  try:
    tagSet = TagSet.objects.get(id=tagSetId)
  except:
    return HttpResponseServerError("TagSet Not Found")

  project = tagSet.project


  if request.method == 'POST': # If the form has been submitted...

    if not tagSet.userHasPermissions(user, ARLO_PERMS.WRITE):  # check user permissions
      return HttpResponseForbidden("Forbidden")

    form = TagSetForm(request.POST)

    if not form.is_valid():
      return render(request, 'tools/tagSet.html', {
        'form': form,
        })

    data = form.cleaned_data
    tagSet.name = data['name']
    tagSet.notes = data['notes']
    tagSet.save()

    return render(request, 'tools/tagSet.html', {
      'form': form,
      'tagSet': tagSet,
      'saveMessage': "Tag Set Updated",
      'project': project,
      })


  elif request.method == 'GET':
    if not tagSet.userHasPermissions(user, ARLO_PERMS.READ):  # check user permissions
      return HttpResponseForbidden("Forbidden")

    form = TagSetForm(initial={'name':tagSet.name, 'notes':tagSet.notes})

    return render(request, 'tools/tagSet.html', {
      'form': form,
      'tagSet': tagSet,
      'project': project,
      })
  else:
    return HttpResponseNotAllowed(['GET', 'POST'])
