import logging

from django.contrib.auth.decorators import login_required
from django.http import HttpResponse, HttpResponseNotAllowed, JsonResponse, HttpResponseForbidden

from tools.models import TagClass, Project, Tag, RenameTagClassForm
from .userPermissions import ARLO_PERMS

## Gets a list of TagClasses within the specified TagSet. 
# Namely used for the auto-complete in CreateTag forms. Must be a GET request
# @param request The Django HTTP request object
# @param projectId The Database ID of the Project from which to return a list of classes.
# @return Django HTTP response object

@login_required
def getTagClassAutoArray(request, projectId):
  user=request.user
  logging.info("Getting Tag Class Auto Array: user = " + user.__str__() + " projectId = " + projectId.__str__())

  project = Project.objects.get(id=projectId)

  # check permissions
  if not project.userHasPermissions(user, ARLO_PERMS.READ):
    return HttpResponseForbidden('Forbidden')

  results_array = []

  if request.method != 'GET':
    return HttpResponseNotAllowed(['POST'])

  if request.is_ajax():
    tagClasses = TagClass.objects.filter(project=project)
    for tagClass in tagClasses:
      results_array.append(tagClass.className)

  return JsonResponse(results_array, safe=False)

## Rename a TagClass. Update both the className and the displayName.
# Expects the request to be a POST
# @param request The Django HTTP request object
# @param tagClassId The database ID of the TagClass to update
# @return Django HTTP response object

@login_required
def renameTagClass(request, tagClassId):
  user=request.user
  logging.info("Renaming Tag Class: user = " + user.__str__() + " tagClassId = " + tagClassId)
  tagClassOrig = TagClass.objects.get(id=tagClassId)

  # check permissions
  if not tagClassOrig.userHasPermissions(user, ARLO_PERMS.WRITE):
    return HttpResponseForbidden('Forbidden')

  if request.method == "POST":
    form = RenameTagClassForm(request.POST)

    if form.is_valid():
      data = form.cleaned_data

      newClassName = data['newClassName']
      if 'newDisplayName' in data:
        newDisplayName = data['newDisplayName']
      else:
        newDisplayName = ''

      if newClassName == '':
        return HttpResponse()
      if newDisplayName == '':
        newDisplayName = newClassName

      tagClassOrig.className = newClassName
      tagClassOrig.displayName = newDisplayName
      tagClassOrig.save()

  else: # not POST request
    return HttpResponseNotAllowed(['POST'])

  return HttpResponse()

## Deletes all Tags from a class. 
# Expects the request to be a POST
# @param request The Django HTTP request object
# @param tagClassId The database ID of the TagClass to update
# @return Django HTTP response object

@login_required
def emptyTagClass(request, tagClassId):
  user = request.user
  logging.info("Emptying Tag Class: user = " + user.__str__() + " tagClassId = " + tagClassId.__str__())
  
  tagClass = TagClass.objects.get(id=tagClassId)

  # check perms
  if not tagClass.userHasPermissions(user, ARLO_PERMS.WRITE):
    return HttpResponseForbidden('Forbidden')

  if request.method == "POST":
    Tag.objects.filter(tagClass=tagClass).delete()

  else: # not POST request
    return HttpResponseNotAllowed(['POST'])

  return HttpResponse()

## Deletes a TagClass, and all contained tags. 
# Expects the request to be a POST
# @param request The Django HTTP request object
# @param tagClassId The database ID of the TagClass to update
# @return Django HTTP response object

@login_required
def deleteTagClass(request,tagClassId):
  user = request.user
  logging.info("Deleting Tag Class: user = " + user.__str__() + " tagClassId = " + tagClassId.__str__())
  
  if request.method != "POST":
    return HttpResponseNotAllowed(['POST'])

  tagClass = TagClass.objects.get(id=tagClassId)

  # check perms
  if not tagClass.userHasPermissions(user, ARLO_PERMS.WRITE):
    return HttpResponseForbidden('Forbidden')

  Tag.objects.filter(tagClass=tagClass).delete()
  tagClass.delete()

  return JsonResponse(tagClassId, safe=False)

