import logging

from django.contrib.auth.decorators import login_required
from django.http import HttpResponseForbidden
from django.shortcuts import render
from django.contrib.contenttypes.models import ContentType
from django.contrib.auth.models import User
from django.conf import settings

from tools.models import Project, ArloPermission, TagSet, ArloUserGroup
from .userPermissions import ARLO_PERMS  # can't do a 'from ... import ...' since userPemsissions imports this module


## Display or edit the permissions on a Project.
# @param request The Django HTTP request object
# @param projectId The database ID of the Project
# @return Django HTTP response object

@login_required
def projectPermissions(request, projectId):
    user = request.user
    logging.info("projectPermissions: user = " + user.__str__() + " projectId: " + projectId.__str__())

    project = Project.objects.get(id=projectId)

    # Check Permissions
    if not project.userHasPermissions(user, ARLO_PERMS.READ):
        return HttpResponseForbidden("Forbidden")

    user_type = ContentType.objects.get_for_model(User)
    group_type = ContentType.objects.get_for_model(ArloUserGroup)

    # Project Permissions
    projectPermissions = project.permissions.all()
    projectUserPermissions = projectPermissions.filter(bearer_type=user_type)
    projectGroupPermissions = projectPermissions.filter(bearer_type=group_type)


    # TagSet Permissions
    project_tagsets = TagSet.objects.filter(project=project)
    tagset_type = ContentType.objects.get_for_model(TagSet)
    projectTagSetPermissions = ArloPermission.objects.filter(target_type=tagset_type, target_id__in=project_tagsets.values_list('id'))

    projectTagSetUserPermissions = projectTagSetPermissions.filter(bearer_type=user_type)
    projectTagSetGroupPermissions = projectTagSetPermissions.filter(bearer_type=group_type)


    return render(request, 'tools/projectPermissions.html', {
                            'user':user,
                            'apacheRoot':settings.APACHE_ROOT,
                            'project':project,
                            'projectUserPermissions': projectUserPermissions,
                            'projectGroupPermissions': projectGroupPermissions,
                            'projectTagSetUserPermissions': projectTagSetUserPermissions,
                            'projectTagSetGroupPermissions': projectTagSetGroupPermissions,
                            'project_tagsets': project_tagsets,
                            })
