import os
import os.path
import logging
import urllib
import hashlib

from django.conf import settings

import nesterSettings
from tools.views.userSettings import getExtendedUserSetting
import tools.Meandre as Meandre


class SpectraImageSettings(object):
  """Settings for Generating a Spectra Image"""

  userSettings = None
  audioFile = None
  startTime = None
  endTime = None
  gain = 1
  minFrequency = None
  maxFrequency = None
  numFrequencyBands = None
  numFramesPerSecond = None
  dampingFactor = None
  simpleNormalization = True
  showTimeScale = True
  showFrequencyScale = True
  borderWidth = 50
  topBorderHeight = 30
  timeScaleHeight = 22
  catalogSpectraNumFrequencyBands = None


  def __init__(self, **kwargs):
    vars(self).update(kwargs)

  def getSettingsDict(self):
    return {
      'userSettings':self.userSettings,
      'audioFile':self.audioFile,
      'startTime':self.startTime,
      'endTime':self.endTime,
      'gain':self.gain,
      'minFrequency':self.minFrequency,
      'maxFrequency':self.maxFrequency,
      'numFrequencyBands':self.numFrequencyBands,
      'numFramesPerSecond':self.numFramesPerSecond,
      'dampingFactor':self.dampingFactor,
      'simpleNormalization':self.simpleNormalization,
      'showTimeScale':self.showTimeScale,
      'showFrequencyScale':self.showFrequencyScale,
      'borderWidth':self.borderWidth,
      'topBorderHeight':self.topBorderHeight,
      'timeScaleHeight':self.timeScaleHeight,
      'catalogSpectraNumFrequencyBands':self.catalogSpectraNumFrequencyBands,
      }


class SpectraImage(object):
  spectraImageSettings = None

  # Generated Images
  imageFilePath = None
  audioSegmentFilePath = None

  imageUrl = None
  audioSegmentUrl = None

  # automatically derived values
  imageWidth = None
  imageHeight = None



  ## Call the Java backend to generate a Spectra Image.
  #
  # @param spectraImageSettings An initialized SpectraImageSettings object
  # @return a SpectraImage on success, None on error

  @staticmethod
  def generateSpectraImage(spectraImageSettings):
    spectra_params = spectraImageSettings.getSettingsDict()

    ###
    # Create result Object, and update settings

    image = SpectraImage()

    image.spectraImageSettings = spectraImageSettings

    image.imageWidth = spectraImageSettings.numFramesPerSecond * (spectraImageSettings.endTime - spectraImageSettings.startTime)
    image.imageWidth += spectraImageSettings.borderWidth * 2

    image.imageHeight = spectraImageSettings.numFrequencyBands + spectraImageSettings.topBorderHeight
    if spectraImageSettings.showTimeScale:
      image.imageHeight += spectraImageSettings.timeScaleHeight


    ###
    # Get new Image from Java Backend

    (success, imageFilePath, audioSegmentFilePath) = generateSpectraImage(**spectra_params)
    if not success:
      return None

    image.imageFilePath = imageFilePath
    image.audioSegmentFilePath = audioSegmentFilePath

    # URLs
    mediaUrl = settings.MEDIA_URL
    if mediaUrl[-1] != '/':
      mediaUrl = mediaUrl + '/'

    image.imageUrl = mediaUrl + urllib.quote(image.imageFilePath)
    image.audioSegmentUrl = mediaUrl + urllib.quote(image.audioSegmentFilePath)

    return image


## Generate Spectra Images / Wav Segments
#
# @param
# @param
# @return

def generateSpectraImage(
        userSettings,
        audioFile,
        startTime,
        endTime,
        gain,
        minFrequency,
        maxFrequency,

        numFrequencyBands,
        numFramesPerSecond,
        dampingFactor,

        simpleNormalization,
        showTimeScale,
        showFrequencyScale,
        borderWidth,
        topBorderHeight,
        timeScaleHeight,
        catalogSpectraNumFrequencyBands,
        ):

#removed params
#        audioFilePath,
#        imageFilePath,
#        audioSegmentFilePath,

  logging.debug("===================================================================")
  logging.debug("==                                                               ==")
  logging.debug("==                  Start Of Spectra Generation                  ==")
  logging.debug("==                                                               ==")
  logging.debug("===================================================================")

  ##########################
  ##      FILE PATHS      ##
  ##########################

# TODO
# A potential conflict exists; for example, if we have 2 files named the same, but in different
# directories (e.g., wav/file1.wav and wav/set2/file1.wav) then the cache images and
# segments may conflict. See the commented code below


  ###
  # audio File source path

  audioFilePath = audioFile.getPath()  # relative to MEDIA_ROOT
  audioFilePathQuoted = urllib.quote(audioFilePath)

  audioFileName = audioFile.file.name.split('/')[-1]

  ###
  # Settings

  # get spectraPitchTraceType from ExtendedUserSettings
  spectraPitchTraceType = getExtendedUserSetting('spectraPitchTraceType', userSettings)
  spectraPitchTraceNumSamplePoints = getExtendedUserSetting('spectraPitchTraceNumSamplePoints', userSettings)
  spectraPitchTraceColor = getExtendedUserSetting('spectraPitchTraceColor', userSettings)
  spectraPitchTraceWidth = getExtendedUserSetting('spectraPitchTraceWidth', userSettings)
  spectraPitchTraceMinCorrelation = getExtendedUserSetting('spectraPitchTraceMinCorrelation', userSettings)
  spectraPitchTraceEntropyThreshold = getExtendedUserSetting('spectraPitchTraceEntropyThreshold', userSettings)
  spectraPitchTraceStartFreq = getExtendedUserSetting('spectraPitchTraceStartFreq', userSettings)
  spectraPitchTraceEndFreq = getExtendedUserSetting('spectraPitchTraceEndFreq', userSettings)

  spectraPitchTraceMinEnergyThreshold = getExtendedUserSetting('spectraPitchTraceMinEnergyThreshold', userSettings)
  spectraPitchTraceTolerance          = getExtendedUserSetting('spectraPitchTraceTolerance', userSettings)
  spectraPitchTraceInverseFreqWeight  = getExtendedUserSetting('spectraPitchTraceInverseFreqWeight', userSettings)

  spectraPitchTraceMaxPathLengthPerTransition = getExtendedUserSetting('spectraPitchTraceMaxPathLengthPerTransition', userSettings)
  spectraPitchTraceWindowSize = getExtendedUserSetting('spectraPitchTraceWindowSize', userSettings)
  spectraPitchTraceExtendRangeFactor = getExtendedUserSetting('spectraPitchTraceExtendRangeFactor', userSettings)


  ###
  # Settings Hash
  #
  # Generate an md5 hash of all of the settings, and use this in the filename
  # to uniquely label the generated files.

  h = hashlib.new('md5')
  h.update('u' + userSettings.id.__str__())
  h.update('s' + startTime.__str__() + 'e' + endTime.__str__())
  h.update('g' + gain.__str__())
  h.update('mi' + minFrequency.__str__() + 'ma' + maxFrequency.__str__())
  h.update('fb' + numFrequencyBands.__str__() + 'fps' + numFramesPerSecond.__str__())
  h.update('df' + dampingFactor.__str__())
  h.update('ss' + userSettings.showSpectra.__str__())
  h.update('spt' + spectraPitchTraceType.__str__())
  h.update('sptns' + spectraPitchTraceNumSamplePoints.__str__())
  h.update('sptc' + spectraPitchTraceColor.__str__())
  h.update('sptw' + spectraPitchTraceWidth.__str__())
  h.update('sptmc' + spectraPitchTraceMinCorrelation.__str__())
  h.update('sptet' + spectraPitchTraceEntropyThreshold.__str__())
  h.update('sptsf' + spectraPitchTraceStartFreq.__str__())
  h.update('sptef' + spectraPitchTraceEndFreq.__str__())
  h.update('sptmet' + spectraPitchTraceMinEnergyThreshold.__str__())
  h.update('sptt' + spectraPitchTraceTolerance.__str__())
  h.update('sptifw' + spectraPitchTraceInverseFreqWeight.__str__())
  h.update('sptmpl' + spectraPitchTraceMaxPathLengthPerTransition.__str__())
  h.update('sptws' + spectraPitchTraceWindowSize.__str__())
  h.update('spterf' + spectraPitchTraceExtendRangeFactor.__str__())
  h.update('sw' + userSettings.showWaveform.__str__())
  h.update('simpleNormalization' + simpleNormalization.__str__())
  h.update('showTimeScale' + showTimeScale.__str__())
  h.update('showFrequencyScale' + showFrequencyScale.__str__())
  h.update('borderWidth' + borderWidth.__str__())
  h.update('topBorderHeight' + topBorderHeight.__str__())
  h.update('timeScaleHeight' + timeScaleHeight.__str__())
  h.update('catalogSpectraNumFrequencyBands' + catalogSpectraNumFrequencyBands.__str__())
  settingsDigest = h.hexdigest()


  ###
  # target file path for Spectra Image

  imageFilePath = os.path.join(nesterSettings.getUsersJpegCacheDirectory(audioFile), audioFileName)
  imageFilePath += '-' + settingsDigest
  imageFilePath += '.jpg'
  imageFilePathQuoted = urllib.quote(imageFilePath)


  ###
  # audioFile Segment Path

  audioSegmentFilePath = os.path.join(nesterSettings.getUsersSegmentCacheDirectory(audioFile), audioFileName)
  audioSegmentFilePath += settingsDigest
  audioSegmentFilePath += '.segment.wav'
  audioSegmentFilePathQuoted = urllib.quote(audioSegmentFilePath)


  # check cache
  cached = False

  if settings.ENABLE_SPECTRA_CACHE:
    fullImageFilePath = os.path.join(settings.MEDIA_ROOT, imageFilePath)
    fullAudioSegmentFilePath = os.path.join(settings.MEDIA_ROOT, audioSegmentFilePath)

    try:
      if ( (os.stat(fullImageFilePath)[6] > 0) and (os.stat(fullAudioSegmentFilePath)[6] > 0) ):
        logging.info("+++++++++++++++++++++++++++++++++++++ Cached")
        cached = True
    except:
      pass

  ###
  # Meandre Params and Call to generate spectra

  if cached:
    success = True
  else:
    spectra_params = {
            'userSettingsID':userSettings.id,

            'audioFilePath': audioFilePathQuoted,
            'imageFilePath': imageFilePathQuoted,
            'audioSegmentFilePath': audioSegmentFilePathQuoted,
            'startTime': startTime,
            'endTime': endTime,
            'gain': gain,
            'numFramesPerSecond': numFramesPerSecond,
            'numFrequencyBands': numFrequencyBands,
            'dampingFactor': dampingFactor,

            'minFrequency': minFrequency,
            'maxFrequency': maxFrequency,

            'simpleNormalization': simpleNormalization,
            'showTimeScale': showTimeScale,
            'showFrequencyScale': showFrequencyScale,
            'borderWidth': borderWidth,
            'topBorderHeight': topBorderHeight,
            'timeScaleHeight': timeScaleHeight,
            'catalogSpectraNumFrequencyBands': catalogSpectraNumFrequencyBands,

            'spectraPitchTraceType': spectraPitchTraceType,
            'spectraPitchTraceNumSamplePoints': spectraPitchTraceNumSamplePoints,
            'spectraPitchTraceColor': spectraPitchTraceColor,
            'spectraPitchTraceWidth': spectraPitchTraceWidth,
            'spectraPitchTraceMinCorrelation': spectraPitchTraceMinCorrelation,
            'spectraPitchTraceEntropyThreshold': spectraPitchTraceEntropyThreshold,
            'spectraPitchTraceStartFreq': spectraPitchTraceStartFreq,
            'spectraPitchTraceEndFreq': spectraPitchTraceEndFreq,
            'spectraPitchTraceMinEnergyThreshold': spectraPitchTraceMinEnergyThreshold,
            'spectraPitchTraceTolerance': spectraPitchTraceTolerance,
            'spectraPitchTraceInverseFreqWeight': spectraPitchTraceInverseFreqWeight,

            'spectraPitchTraceMaxPathLengthPerTransition': spectraPitchTraceMaxPathLengthPerTransition,
            'spectraPitchTraceWindowSize': spectraPitchTraceWindowSize,
            'spectraPitchTraceExtendRangeFactor': spectraPitchTraceExtendRangeFactor,
            }

    try:
      results = Meandre.call(Meandre.MeandreAction.createSpectra, spectra_params)
      success = results[0]
      if not success:
        logging.info(repr(results))
        logging.error("Meandre Failure Executing Spectra Generation: " + results[1])
    except:
      logging.error("Unknown Meandre Call Exception During Spectra Generation")
      success = False


  return (success, imageFilePath, audioSegmentFilePath)
