import os
import logging
import re

from django.contrib.auth.decorators import login_required
from django.http import HttpResponse, HttpResponseRedirect, HttpResponseForbidden, HttpResponseNotAllowed, HttpResponseServerError, JsonResponse
from django.shortcuts import render
from django.db.models import Q
from django.conf import settings
from django import forms
from django.core.paginator import Paginator, EmptyPage, PageNotAnInteger

import nesterSettings as nesterSettings
from tools.models import Project, MediaFile, MediaFileMetaData, Tag, Library, MediaFileUserMetaData, MediaFileUserMetaDataField
from tools.models import UserSettings
import tools.Meandre as Meandre
from tools.Meandre import MeandreAction
from .viewMetaData import getMetaDataEntry
from .userPermissions import ARLO_PERMS

## Enable all audio files for a Project
# For all audio files in a Project, loop through and set
# them as 'active'
# Note: In future versions, we'll likely move the 'active' tag from
# tools_mediafile to something more project specific.
# @param request The Django HTTP request object
# @param projectId Project Id
# @return Django HTTPResponse object

@login_required
def enableAllAudioFiles(request, projectId):
  user = request.user
  logging.info("enableAllAudioFiles: user = " + user.__str__() +
    " projectId = " + projectId.__str__())

  project = Project.objects.get(id=projectId)

  if not project.userHasPermissions(user, ARLO_PERMS.WRITE):  # check user permissions
    return HttpResponseForbidden("Forbidden")

  mediaFiles = project.mediaFiles.all()

  mediaFiles.update(active=True)
#  for mediaFile in mediaFiles:
#    mediaFile.active = True;
#    mediaFile.save();

  return HttpResponseRedirect(settings.URL_PREFIX + '/tools/projectFiles/' + projectId.__str__())


## Disable all audio files for a Project
# For all audio files in a Project, loop through and set
# them as 'active' = false
# Note: In future versions, we'll likely move the 'active' tag from
# tools_mediafile to something more project specific.
# @param request The Django HTTP request object
# @param projectId Project Id
# @return Django HTTPResponse object

@login_required
def disableAllAudioFiles(request, projectId):
  user = request.user
  logging.info("enableAllAudioFiles: user = " + user.__str__() +
    " projectId = " + projectId.__str__())

  project = Project.objects.get(id=projectId)

  if not project.userHasPermissions(user, ARLO_PERMS.WRITE):  # check user permissions
    return HttpResponseForbidden("Forbidden")

  mediaFiles = project.mediaFiles.all()

  mediaFiles.update(active=False)

  return HttpResponseRedirect(settings.URL_PREFIX + '/tools/projectFiles/' + projectId.__str__())


## Disable an audio file from a project
# Set the audio file in a Project as 'active' = false
# Note: In future versions, we'll likely move the 'active' tag from
# tools_mediafile to something more project specific.
# @param request The Django HTTP request object
# @param audioID The ID of the media file to toggle
# @param projectId Project Id, only used for the response URL
# @return Django HTTPResponse object

@login_required
def disableAudioFile(request, audioID, projectId):
  user = request.user
  logging.info("Disable Audio File: user = " + user.__str__() + " audioID = " + audioID.__str__())

  mediaFile = MediaFile.objects.get(id=audioID)

  if not mediaFile.userHasPermissions(user, ARLO_PERMS.WRITE):
    return HttpResponseForbidden("Forbidden")

  mediaFile.active = False
  mediaFile.save()

  return HttpResponseRedirect(settings.URL_PREFIX + '/tools/projectFiles/' + projectId.__str__())

## Enable an audio file from a project
# Set the audio file in a Project as 'active'
# Note: In future versions, we'll likely move the 'active' tag from
# tools_mediafile to something more project specific.
# @param request The Django HTTP request object
# @param audioID The ID of the media file to toggle
# @param projectId Project Id, only used for the response URL
# @return Django HTTPResponse object

@login_required
def enableAudioFile(request, audioID, projectId):
  user = request.user
  logging.info("Enable Audio File: user = " + user.__str__() + " audioID = " + audioID.__str__())

  mediaFile = MediaFile.objects.get(id=audioID)

  if not mediaFile.userHasPermissions(user, ARLO_PERMS.WRITE):
    return HttpResponseForbidden("Forbidden")

  mediaFile.active = True
  mediaFile.save()

  return HttpResponseRedirect(settings.URL_PREFIX + '/tools/projectFiles/' + projectId.__str__())


## Deletes wav file, jpgs, segments, features, MetaData, Tags.
# @note Only the MediaFile's owner can delete.
# @param audioID The ID of the media file to delete
# @return True on success, False otherwise

def deleteAudioFile(audioID):
  mediaFile = MediaFile.objects.get(id=audioID)
  user = mediaFile.user
  logging.info("Deleting Audio: user = " + user.__str__() + " audioID = " + audioID.__str__())

  # Get File Path
  wavPath = os.path.join(settings.MEDIA_ROOT, mediaFile.getPath())

  # ...and delete from the filesystem
  try:
    os.remove(wavPath)
  except:
    logging.error('Could not delete ' + wavPath)

  Tag.objects.filter(mediaFile=mediaFile).delete()
  MediaFileMetaData.objects.filter(mediaFile=mediaFile).delete()

  mediaFile.delete()

  return True


## Deletes MediaFile (by calling deleteAudioFile())
# @note Only the MediaFile's owner can delete.
# @param audioID The ID of the media file to toggle
# @return True on success, False otherwise

@login_required
def deleteAudio(request, audioID):
  user = request.user
  logging.info("Deleting Audio File: user = " + user.__str__() + " audioID = " + audioID.__str__())

  mediaFile = MediaFile.objects.get(id=audioID)
  if (user.id != mediaFile.user.id):
    return HttpResponseForbidden("Forbidden")

  if (not deleteAudioFile(int(audioID))):
    return HttpResponse("Failed")

  return HttpResponseRedirect(settings.URL_PREFIX + "/tools/audioFiles")


## Display all media files owned by a user (/tools/audioFiles).
# @param request The Django HTTP request object
# @return Django HTTP response object

@login_required
def audioFiles(request):
  user = request.user

  logging.info("Retrieving Audio Files: user = " + user.__str__())

  return render(request, 'tools/audioFiles.html', {
                            'user': user,
                            'apacheRoot':settings.APACHE_ROOT,
                            })

## Get AudioFile info associated with a Tag.
# Namely used by JavaScript to get the AudioFileId to to open the audio
# file of a tag.
# @param request The Django HTTP request
# @param tagID The database Id of the Tag
# @return JSON encoded dictionary; {'projectName', 'projectId', mediaFile 'alias', 'audioID' mediaFile Id}

@login_required
def getAudioInfo(request, tagID):
  user=request.user
  logging.info("Getting Audio Info: user = " + user.__str__() + " tagID = " + tagID.__str__())

  tag = Tag.objects.get(id=tagID)

  if not tag.userHasPermissions(user, ARLO_PERMS.READ):  # check user permissions
    return HttpResponseForbidden("Forbidden")

  results_dict = {}

  if request.method != 'GET':
    return HttpResponseNotAllowed(['GET'])

  #if request.is_ajax():

  results_dict = {
    'projectName': tag.tagClass.project.name,
    'projectId': tag.tagClass.project.id,
    'alias': tag.mediaFile.alias,
    'audioID': tag.mediaFile.id
  }

  return JsonResponse(results_dict)


## Delete all Media Files for a user.
# @param request The Django HTTP request object
# @return Django HTTP response object

@login_required
def deleteUserAudioFiles(request):
  user = request.user
  logging.info("Deleting User Audio Files: user = " + user.__str__())

  mediaFiles = MediaFile.objects.filter(user=user)
  for mediaFile in mediaFiles:
    deleteAudioFile(mediaFile.id)

  return HttpResponseRedirect(settings.URL_PREFIX + "/tools/audioFiles")

## Pass-through call to Java backend for mass import function.
# @note This is currently a hidden function (i.e., you have to manually
# type and go to the URL)
# @param request The Django HTTP request object
# @return Django HTTPResponse object (redirect to audioFiles())

@login_required
def importAudioFiles(request):
  user = request.user
  logging.info("Importing Audio Files: user = " + user.__str__())

  results = Meandre.call(MeandreAction.importAudioFiles, {'username':user})
  if results[0]:
    logging.info(results[1])
  else:
    logging.info(results[1])

  return audioFiles(request)


## Search through for AudioFiles alias and file name.
# Search through EITHER all files the user owns OR, if projectId
# is supplied, through all of the files in that project. If searchString
# is not supplied, return all files.
# Match against 'searchString' in the GET data.
# @note It is called AudioFiles, but now searches all media files
# @param request The Django HTTP request. Note that this expects
# a GET method with a 'searchString' variable.
# @param projectId The database Id of a Project to search.
# @return JSON encoded dictionary {totalFiles, filesReturned, filesDict}.
# filesDict is another dictionary {mediaID, alias, mediaRelativeFilePath, startTime, durationInSeconds, active, sensorName}

@login_required
def searchAudioFiles(request, responseFormat = 'JSON'):
  user = request.user

  ###
  # User's Own Projects

  userProjects = Project.objects.filter(user=user).order_by('name')
  userProjectInfos = []

  for project in userProjects:

    sharedUserInfos = []
    sharedUsers = project.getProjectUsers()

    for sharedUser in sharedUsers:
      sharedUserInfos.append({
        'user': sharedUser,
        'canRead': project.userHasPermissions(sharedUser, ARLO_PERMS.READ),
        'canWrite': project.userHasPermissions(sharedUser, ARLO_PERMS.WRITE),
      })

    userProjectInfos.append( {
      'project': project,
      'sharedUserInfos': sharedUserInfos,
    })

  if request.method != 'GET':
    return HttpResponseNotAllowed(['GET'])

  searchString = request.GET.get('searchString', '')

  projectId = request.GET.get('projectId', '')
  libraryId = request.GET.get('libraryId', '')

  currentProject = projectId
  currentLibrary = libraryId

  logging.info("Searching Audio Files: user = " + user.__str__() +
    " search: " + searchString +
    " projectId: " + projectId +
    " libraryId: " + libraryId)

  userSettings = UserSettings.objects.get(user=user, name='default')

  # Get a list of Files to Consider for Matching
  if projectId:
    project = Project.objects.get(id=projectId)

    # check user permissions
    if not project.userHasPermissions(user, ARLO_PERMS.READ):  # check user permissions
      return HttpResponseForbidden("Forbidden")

    #logging.debug("Searching Project: " + str(project.name))

    mediaFilesToConsider = project.mediaFiles.all()

  elif libraryId:
    library = Library.objects.get(id=libraryId)

    # check user permissions
    if not library.userHasPermissions(user, ARLO_PERMS.READ):  # check user permissions
      return HttpResponseForbidden("Forbidden")

    #logging.debug("Searching Project: " + str(project.name))

    mediaFilesToConsider = library.mediaFiles.all()

  else:
    mediaFilesToConsider = MediaFile.objects.filter(user=user)

  #logging.info("\n\n=== MediaFilesToConsider: =====\n" + repr(mediaFilesToConsider) + "\n\n")

  # Check the MediaFileUserMetaData
  libraries = Library.objects.filter(id__in=mediaFilesToConsider.values('library').distinct())
  userMetaDataFields = MediaFileUserMetaDataField.objects.filter(library__in=libraries)
  userMetaDataMatches = MediaFileUserMetaData.objects.filter(value__icontains=searchString)
  # TODO Should we also filter by project here since we're no longer filtering by userMetaDataFields?
  userMetaDataAllMatchFiles = MediaFile.objects.filter(id__in=userMetaDataMatches.values('mediaFile').distinct())

  # filter the MetaDataFiles MediaFiles back down to the possible matches
  userMetaDataMatchFiles = mediaFilesToConsider.filter(id__in=userMetaDataAllMatchFiles.values('id'))
  #logging.info("\n\n=== UserMetaDataMatchFiles: =====\n" + repr(userMetaDataAllMatchFiles) + "\n\n")

  mediaFilesAll = mediaFilesToConsider.filter(

    Q(alias__icontains=searchString) |
    Q(file__icontains=searchString) |
    Q(id__in=userMetaDataMatchFiles)
    ).order_by('uploadDate').reverse()

  totalFiles = len(mediaFilesAll)

  mediaFilesAllIDs = mediaFilesAll.values_list('id', flat=True)

  paginator = Paginator(mediaFilesAll, settings.ARLO_UI_PAGE_SIZE)

  page = request.GET.get('page')
  try:
    mediaFiles = paginator.page(page)
  except PageNotAnInteger:
    # If page is not an integer, deliver first page.
    mediaFiles = paginator.page(1)
  except EmptyPage:
    # If page is out of range (e.g. 9999), deliver last page of results.
    mediaFiles = paginator.page(paginator.num_pages)

  # MediaFilesAll is now all the search results.
  # MediaFiles is the paginated results, so we only need details on those.

  #logging.info("\n\n=== mediaFiles: =====\n" + repr(mediaFiles) + "\n\n")

  totalFiles = len(mediaFilesAll)
  filesReturned = len(mediaFiles)
  #Don't need to limit it like this since we're paginating.
  #if totalFiles > userSettings.maxNumAudioFilesToList:
    #mediaFiles = mediaFiles[0:userSettings.maxNumAudioFilesToList]
    #logging.info("Limiting result to " + userSettings.maxNumAudioFilesToList.__str__() + " files")

  files_list = []
  for mediaFile in mediaFiles:
    if (mediaFile.sensor):
      sensorName = mediaFile.sensor.name
    else:
      sensorName = ""

    if mediaFile.realStartTime:
      startTime = mediaFile.realStartTime.strftime('%Y-%m-%d %H:%M:%S')
    else:
      startTime = ""

    projectList = []
    for project in mediaFile.getContainingProjects():
      projectList.append( {
        'name': project.name,
        'id': project.id
      })


    results_dict = {
      'id': mediaFile.id,
      'alias': mediaFile.alias,
      'library': mediaFile.library,
      'library_id': mediaFile.library_id,
      'mediaRelativeFilePath': mediaFile.file.name,
      'startTime': startTime,
      'duration': getMetaDataEntry(mediaFile, 'durationInSeconds'),
      'active': mediaFile.active,
      'sensorName': sensorName,
      'projectList': projectList,
      }
    files_list.append(results_dict)

  MediaFileListColumns = {
    'name': True,
    'duration': True,
    'project': True,
    'library':True,
  }

  listSortOptions = {
    'uploadDate': True,
    'alias': True,
    'duration': True,
    'userTagCount': False,
    'machineTagCount': False,
    }

  response_dict = {
    'searchString': searchString,
    'totalFiles': totalFiles,
    'filesReturned': filesReturned,
    'filesDict' : files_list }

  if projectId:
    fileListHeader = 'Search results for project \'' + project.name + '\':'
    searchPrompt = 'Search files in project \'' + project.name + '\'...'
  elif libraryId:
    fileListHeader = 'Search results for library \'' + library.name + '\':'
    searchPrompt = 'Search files in library \'' + library.name + '\'...'
  else:
    fileListHeader = 'Search results:'
    searchPrompt = 'Search all files...'

  if (responseFormat == 'JSON'):
    return JsonResponse(response_dict)
  else:
    return render(request, "tools/searchResults.html", {
      'user': user,
      'MediaFileList': files_list,
      'searchPrompt': searchPrompt,
      'MediaFilesPaginator': mediaFiles,
      'MediaFileListColumns': MediaFileListColumns,
      'apacheRoot':settings.APACHE_ROOT,
      'fileListHeader': fileListHeader,
      'currentProject': currentProject,
      'currentLibrary': currentLibrary,
      'totalFiles': totalFiles,
      'allFilesLabel': 'search results',
      'userProjects': userProjectInfos,
      'mediaFilesAllIDs': mediaFilesAllIDs,
      })
