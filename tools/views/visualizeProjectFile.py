from collections import namedtuple
import json
import logging

from django import forms
from django.contrib.auth.decorators import login_required
from django.http import HttpResponseForbidden
from django.shortcuts import render
from django.conf import settings

from tools.models import MediaFile, Project, UserSettings, Tag, TagClass, CreateTagForm, NavigateTagClassForm, TagSet
from .viewMetaData import getMetaDataEntry
from .userPermissions import ARLO_PERMS
from .userMetaData import getMediaFileUserMetaDataDictionary


Segment = namedtuple('Segment',['start','duration'])

def getTagDict(tagSetID,mediaFileID):
    '''
    Returns a dictionary
    {
       tagName : {
           machine:[[start,duration]...],
           user:[[start,duration]...]
       }
       ,
       ...
    }

    for a particular media file
    '''
    allTags = Tag.objects.filter(tagSet=tagSetID,mediaFile=mediaFileID)
    sortedTags = sorted(allTags,key=lambda tag:tag.startTime)

    tagDict = dict()

    for tag in sortedTags:
        tagName = tag.tagClass.className
        if tagName not in tagDict:
            tagDict[tagName] = {'user':list(),'machine':list()}
        if tag.machineTagged:
            tagDict[tagName]['machine'].append(Segment(tag.startTime, tag.endTime - tag.startTime))
        elif tag.userTagged:
            tagDict[tagName]['user'].append(Segment(tag.startTime, tag.endTime - tag.startTime))

    return tagDict

def makeFreqMap(numBuckets=10):
    freqMap = [[bucket,0] for bucket in xrange(numBuckets)]
    return freqMap

def getMidpoint(start,duration):
    return start + duration/2.0

def toBuckets(tupleList,duration,numBuckets):
    freqMap = makeFreqMap(numBuckets)
    bucketSize = duration/float(numBuckets)
    if(duration <= 0):
        return freqMap
    else:
        for tup in tupleList:
            bucket = int(getMidpoint(tup.start,tup.duration)/bucketSize)
            freqMap[bucket][1] += 1
        return freqMap

def sumBuckets(b1,b2):
    return [[index,pair[0][1] + pair[1][1]] for index,pair in enumerate(zip(b1,b2))]

def getMediaFrequencyDict(tagSetID,mediaFile,numBuckets=10):

    duration = float(getMetaDataEntry(mediaFile, 'durationInSeconds'))
    fileBuckets = getTagDict(tagSetID,mediaFile.pk)

    for tagName in fileBuckets:
        fileBuckets[tagName]['machine'] = toBuckets(fileBuckets[tagName]['machine'],duration,numBuckets)
        fileBuckets[tagName]['user'] = toBuckets(fileBuckets[tagName]['user'],duration,numBuckets)

    return fileBuckets

# def getTagSetFrequencyDict(tagSetID,numBuckets=10,allTags=None,tagLabels=None):
#     if(not allTags):
#         allTags = Tag.objects.filter(tagSet=tagsetID)

#     classDict = {label:makeFreqMap(numBuckets) for label in tagLabels}
#     for tag in allTags:
#         bucket = int(getMidpoint(tag.startTime,(tag.endTime - tag.startTime))/bucketSize)
#         freqMap[bucket][1] += 1
#         classDict[tag.tagClass.className][bucket][1] += 1
#     return classDict

def aggregateBuckets(tagType,tagIdLabels,tagsetID,mediaFiles):
    obj = {tup[1]:makeFreqMap() for tup in tagIdLabels}

    for mf in mediaFiles:
        mfdict = getMediaFrequencyDict(tagsetID,mf)
        for tagName in mfdict:
            obj[tagName] = sumBuckets(obj[tagName],mfdict[tagName][tagType])

    return obj


## Visualize tags in relation to a project.
# @param request The Django HTTP request object
# @param projectId The Database ID of the Project
# @param tagsetID The Database ID of the tagset to visualize
# @return Django HTTP response object
@login_required
def visualizeProjectTags(request, projectId, tagsetID):
    '''
    For each tag, returns the relative frequency of that tag occurring
    aggregated over each media file.
    {
       'raven_calls' : {user:[[0,100],[1,200],...], machine:{...}}
       'hawk_calls' : {user:[[0,10],[1,3572],...]},machine:{...}}
       ...
    }
    '''

    user = request.user

    project = Project.objects.get(id=projectId)

    if not project.userHasPermissions(user, ARLO_PERMS.READ):
        return HttpResponseForbidden("Forbidden")

    logging.info("Visualizing ProjectFileTags: user = " + user.__str__(
    ) + " projectID= " + projectId.__str__() + " tagSetID= " + tagsetID.__str__())


    mediaFiles = project.mediaFiles.all()

    allTags = Tag.objects.filter(tagSet=tagsetID)
    # Iterate through all tags, find the intersection of labels
    tagIdLabels = list(set(map(lambda x: (x.pk,x.tagClass.className), allTags)))
    tagCount = len(allTags)

    machineBuckets = aggregateBuckets('machine',tagIdLabels,tagsetID,mediaFiles)
    userBuckets = aggregateBuckets('user',tagIdLabels,tagsetID,mediaFiles)

    obj = dict()
    for idLabel in tagIdLabels:
        label = idLabel[1]
        obj[label] = {'machine':machineBuckets[label],'user':userBuckets[label]}

    print("DUMPED OBJ\n\n\n")
    print(json.dumps(obj))
    print('Labels: ', tagIdLabels)
    print('tagTotal:', tagCount)

    tagSets = TagSet.objects.filter(project=project)
    ## Do we want to show hidden here?
    #if not include_hidden:
    tagSets = tagSets.filter(hidden=False)
    tagSets = tagSets.order_by('name')

    tagSetsList = []

    for tagSet in tagSets:
        numTags = Tag.objects.filter(tagSet=tagSet).count()

        tagSetsList.append( {
        'tagSet': tagSet,
        'numTags': numTags,
    })

    return render(request, 'tools/visualizeProjectTags.html',
                    {'apacheRoot': settings.APACHE_ROOT,
                    'tagData': json.dumps(obj),
                    'tagLabels': tagIdLabels,
                    'tagTotal': tagCount,
                    'tagSets': tagSetsList,
                    'project': project,
                    'currentTagSetId': int(tagsetID),
                    },
                   )


## Visualize the tags attached to an audio file.
# @param request The Django HTTP request object
# @param projectId The Database ID of the Project
# @param audioID The Database ID of the audio file to visualize
# @param tagsetID The Database ID of the tagset to visualize
# @return Django HTTP response object

@login_required
def visualizeProjectFileTags(request, projectId, audioID, tagSetID):

    #Get all the tags associated with one audio file

    #begin COPIED
    user = request.user
    # TODO - remove this later.
    logging.info("Visualizing ProjectFileTags: user = " + user.__str__(
    ) + " audioID = " + audioID.__str__() + " tagSetID= " + tagSetID.__str__())

    mediaFile = MediaFile.objects.get(id=audioID)
    project = Project.objects.get(id=projectId)
    tagSets = project.getTagSets(include_hidden=False)

    tagSetsList = []
    for tagSet in tagSets:
        numTags = Tag.objects.filter(tagSet=tagSet,mediaFile=mediaFile).count()
        if(numTags > 0):
            tagSetsList.append({
                'tagSet': tagSet,
                'numTags': numTags,
            })

    tagSetsList = sorted(tagSetsList, key=lambda x: x['numTags'], reverse=True)

    #TODO - check the syntax
    allTags = Tag.objects.filter(tagSet=tagSetID, mediaFile=audioID)
    tagLabels = list(set(map(lambda x: (x.pk,x.tagClass.className), allTags)))
    #TODO - should we change the model to make this more efficient? Will this be happening all the time?

    if not project.userHasPermissions(user, ARLO_PERMS.READ):
        return HttpResponseForbidden("Forbidden")
    if not mediaFile.userHasPermissions(user, ARLO_PERMS.READ):
        return HttpResponseForbidden("Forbidden")
    #end COPIED

    duration = getMetaDataEntry(mediaFile, 'durationInSeconds')

    # #TODO - is this even necessary?
    # allTags = sorted(allTags,key=lambda tag:tag.startTime)

    obj = getTagDict(tagSetID,audioID)
    # import pdb; pdb.set_trace()

    return render(request, 'tools/visualizeProjectFileTags.html', {
        'apacheRoot': settings.APACHE_ROOT,
        'duration': duration,
        'tagData': json.dumps(obj),
        'tagLabels': json.dumps(tagLabels),
        'tagCount': len(allTags),
        'tagSets': tagSetsList,
        'project': project,
        'currentTagSetId': int(tagSetID),
        'audioId': audioID,
        'mediaFile': mediaFile,
    })


class VisualizeAudioForm(forms.Form):
  alias = forms.CharField(max_length=1024, widget=forms.TextInput(attrs={'readonly':'1'}))
  mediaFileId = forms.IntegerField(required=True, widget=forms.TextInput(attrs={'readonly':'1'}))
  startTime = forms.FloatField(label='startTime', initial=0.0)
  endTime = forms.FloatField(label='endTime', initial=16.0)
  gain = forms.FloatField(label='Gain', initial=1.0)
  spectraNumFramesPerSecond = forms.FloatField(label='Num Frames Per Second', initial=64)
  spectraNumFrequencyBands = forms.IntegerField(label='Num Frequency Bands', initial=256)
  spectraDampingFactor = forms.FloatField(label='Damping Factor', initial=0.02)
  spectraMinimumBandFrequency = forms.FloatField(label='Minimum Band Frequency', initial=20.0)
  spectraMaximumBandFrequency = forms.FloatField(label='Maximum Band Frequency', initial=20000.0)
  loadAudio = forms.BooleanField(label='loadAudio?', required=False, initial=True)
  tagSets = forms.ModelMultipleChoiceField(TagSet.objects.all(), widget=forms.CheckboxSelectMultiple, required=False)


## Visualize an audio file.
# This page allows the creation of and navigating through tags.
# @param request The Django HTTP request object
# @param projectId The Database ID of the Project
# @param audioID The Database ID of the audio file to visualize
# @return Django HTTP response object


@login_required
def visualizeProjectFile(request, projectId, audioID):
    user = request.user
    logging.info("Visualizing ProjectFile: user = " + user.__str__() +
                 " audioID = " + audioID.__str__() + " projectId = " +
                 projectId.__str__())

    audioFile = MediaFile.objects.get(id=audioID)
    project = Project.objects.get(id=projectId)

    if not project.userHasPermissions(user, ARLO_PERMS.READ):
        return HttpResponseForbidden("Forbidden")
    if not audioFile.userHasPermissions(user, ARLO_PERMS.READ):
        return HttpResponseForbidden("Forbidden")

    sampleRate = getMetaDataEntry(audioFile, 'sampleRate')
    userSettings = UserSettings.objects.get(user=user, name='default')

    ##
    # settings
    duration = userSettings.fileViewWindowSizeInSeconds

    startTime = 0
    endTime = userSettings.fileViewWindowSizeInSeconds

    spectraMinimumBandFrequency = userSettings.fileViewSpectraMinimumBandFrequency
    spectraMaximumBandFrequency = userSettings.fileViewSpectraMaximumBandFrequency

    ##
    # if a tagId is passed in, we'll zoom into that spot in the file
    # this will be from a GET request
    hasTagID = request.GET.has_key('tagID')
    if hasTagID:
        tagID = request.GET.get('tagID', -1)
        if tagID == -1:
            hasTagID = False

    if hasTagID:
        tag = Tag.objects.get(id=tagID)

    if hasTagID:
        timeLeft = duration - (tag.endTime - tag.startTime)

        if timeLeft <= 0:
            timeLeft = 2.0

        startTime = tag.startTime - timeLeft / 2
        endTime = tag.endTime + timeLeft / 2

        if startTime < 0:
            endTime += -startTime
            startTime = 0

        if spectraMinimumBandFrequency > tag.minFrequency:
            spectraMinimumBandFrequency = tag.minFrequency - 200
            if spectraMinimumBandFrequency <= 0:
                spectraMinimumBandFrequency = 1

        if spectraMaximumBandFrequency < tag.maxFrequency:
            spectraMaximumBandFrequency = tag.maxFrequency + 200

    ##
    # Build forms - spectra, navigate, createTag

    tagSetsSelectionList = []
    for proj in project.getTagSets(include_hidden=False):
        tagSetsSelectionList.append(proj.id)

    spectraForm = VisualizeAudioForm({
        'alias': audioFile.alias,
        'mediaFileId': audioFile.id,
        'startTime': startTime,
        'endTime': endTime,
        'gain': 1.0,
        'spectraNumFramesPerSecond': userSettings.fileViewSpectraNumFramesPerSecond,
        'spectraNumFrequencyBands': userSettings.fileViewSpectraNumFrequencyBands,
        'spectraDampingFactor': userSettings.fileViewSpectraDampingFactor,
        'spectraMinimumBandFrequency': spectraMinimumBandFrequency,
        'spectraMaximumBandFrequency': spectraMaximumBandFrequency,
        'tagSets': [l.pk for l in project.getTagSets(include_hidden=False)],
        'loadAudio': userSettings.loadAudio,
    })
    spectraForm.fields['tagSets'] = forms.ModelMultipleChoiceField(
        project.getTagSets(include_hidden=False),
        required=False)

    createTagForm = CreateTagForm()
    createTagForm.fields['tagSet'] = forms.ModelChoiceField(
        project.getTagSets(include_hidden=False))

    navigateForm = NavigateTagClassForm()

    if hasTagID:
        navigateForm.fields['navigateTagClass'] = forms.ModelChoiceField(
            TagClass.objects.filter(
                tag__mediaFile=audioFile).distinct().order_by('className'),
            required=False,
            initial=tag.tagClass)

        navigateForm.fields['currentTagID'] = forms.IntegerField(
            'currentTagID',
            widget=forms.HiddenInput,
            required=False,
            initial=tagID)

    else:
        navigateForm.fields['navigateTagClass'] = forms.ModelChoiceField(
            TagClass.objects.filter(
                tag__mediaFile=audioFile).distinct().order_by("className"),
            required=False)

        navigateForm.fields['currentTagID'] = forms.IntegerField(
            'currentTagID', widget=forms.HiddenInput, required=False)

    return render(request, 'tools/visualizeProjectFile.html', {
        'apacheRoot': settings.APACHE_ROOT,
        'project': project,
        'user': user,
        'loadAudio': userSettings.loadAudio,
        'audioID': audioFile.id,
        'spectraForm': spectraForm,
        'createTagForm': createTagForm,
        'navigateForm': navigateForm,
        'contextInfo': getMetaDataEntry(audioFile, 'contextInfo'),
        'sampleRate': sampleRate,
        'durationInSeconds': getMetaDataEntry(audioFile, 'durationInSeconds'),
        'mediaFileUserMetaData': getMediaFileUserMetaDataDictionary(audioFile),
        'fileAlias': audioFile.alias
    })
