from django.conf.urls import url


urlpatterns = []

from .views import index
urlpatterns += [
    url(r'^$', index.index),
    url(r'^login$', index.loginUser),
    url(r'^logout$', index.logoutUser),
    url(r'^logout/', index.logoutUser),
    url(r'^userHome$', index.userHome),
    url(r'^createUser$', index.createUser),
    url(r'^deleteUser$', index.deleteUser),
    url(r'^loginFailed$', index.loginFailed),
    url(r'^meandreTestCall$', index.meandreTestCall),
    url(r'^loginAsUser$', index.loginAsUser),
    url(r'^addTestTaskToDb$', index.addTestTaskToDb),
    url(r'^deleteMediaFile/$', index.deleteMediaFile),
]

from .views import project
urlpatterns += [
    url(r'^projects$', project.projects),
    url(r'^deleteProject/(?P<projectId>\d+)$', project.deleteProject),
    url(r'^startNewProject$', project.startNewProject),
    url(r'^addAudioToProject/(?P<audioFileID>\d+)$', project.addAudioToProject),
    url(r'^removeAudioFromProject/(?P<projectID>.*)/(?P<audioFileID>.*)$',project.removeAudioFromProject),
    url(r'^addUserToProject$', project.addUserToProject),
    url(r'^removeUserFromProject$', project.removeUserFromProject),
    url(r'^projectLibraryFiles/(?P<projectId>\d+)$', project.projectFiles), # keep temporarily for backward compatibility
    url(r'^projectFiles/(?P<projectId>\d+)$', project.projectFiles, name="projectFiles"),
    url(r'^projectNotes/(?P<projectId>\d+)$', project.projectNotes),
    url(r'^addMultipleAudioToProject/$', project.addMultipleAudioToProject),
    url(r'^AJAXRemoveAudioFromProject/$', project.AJAXRemoveAudioFromProject),
    url(r'^deleteMultipleProjects/$', project.deleteMultipleProjects),
]

from .views import audio
urlpatterns += [
    url(r'^enableAllAudioFiles/(?P<projectId>\d+)$',audio.enableAllAudioFiles),
    url(r'^disableAllAudioFiles/(?P<projectId>\d+)$',audio.disableAllAudioFiles),
    url(r'^enableAudioFile/(?P<projectId>\d+)/(?P<audioID>\d+)$',audio.enableAudioFile),
    url(r'^disableAudioFile/(?P<projectId>\d+)/(?P<audioID>\d+)$',audio.disableAudioFile),
    url(r'^deleteAudio/(?P<audioID>.*)$', audio.deleteAudio),
    url(r'^audioFiles$', audio.audioFiles),
    url(r'^getAudioInfo/(?P<tagID>.*)$',audio.getAudioInfo),
    url(r'^deleteUserAudioFiles$',audio.deleteUserAudioFiles),
    url(r'^importAudioFiles$', audio.importAudioFiles),
    url(r'^searchAudioFileNames[/]?$', audio.searchAudioFiles, {'responseFormat':'HTML'}),
]

from .views import biasManipulation
urlpatterns += [
    url(r'^setJobStatus/(?P<jobId>\d+)$', biasManipulation.setJobStatus),
    url(r'^setJobPriority/(?P<jobId>\d+)$', biasManipulation.setJobPriority),
    url(r'^stopQueuedChildTasks/(?P<parentJobId>\d+)$', biasManipulation.stopQueuedChildTasks),
    url(r'^queueStoppedChildTasks/(?P<parentJobId>\d+)$', biasManipulation.queueStoppedChildTasks),
    url(r'^stopQueuedTask/(?P<jobId>\d+)$', biasManipulation.stopQueuedTask),
    url(r'^queueStoppedTask/(?P<jobId>\d+)$',biasManipulation.queueStoppedTask),
    url(r'^searchQueueTasks/(?P<projectId>\d+)$', biasManipulation.searchQueueTasks),
    url(r'^manageJobs/(?P<projectId>\d+)$', biasManipulation.manageJobs),
    url(r'^randomWindowTagging/(?P<projectId>\d+)$', biasManipulation.randomWindowTagging),
    url(r'^getRandomWindowTasks/(?P<projectId>\d+)$', biasManipulation.getRandomWindowTasks),
]

from .views import catalog
urlpatterns += [
    url(r'^catalog/(?P<projectId>\d*)$', catalog.catalog),
]

from .views import expertAgreementClassification
urlpatterns += [
    url(r'^expertAgreementClassificationQueueRunner/(?P<projectId>\d+)$', expertAgreementClassification.expertAgreementClassificationQueueRunner),
]

from .views import exportLibraryMediaFileMetaData
urlpatterns += [
    url(r'^exportLibraryMediaFileMetaData/(?P<libraryId>[^/]+)$', exportLibraryMediaFileMetaData.exportLibraryMediaFileMetaData),
]

from .views import importLibraryUserMetaData
urlpatterns += [
    url(r'^importLibraryUserMetaData/(?P<libraryId>[^/]+)$', importLibraryUserMetaData.importLibraryUserMetaData),
]

from .views import importTagsFromCSV
urlpatterns += [
    url(r'^importTagsFromCSV/(?P<projectId>\d+)$',importTagsFromCSV.importTagsFromCSV),
]

from .views import library
urlpatterns += [
    url(r'^userLibraries$', library.userLibraries),
    url(r'^startNewLibrary$', library.startNewLibrary),
    url(r'^libraryFiles/(?P<libraryId>[^/]+)$', library.libraryFiles, name="libraryFiles"),
    url(r'^libraryAddSensorArray/(?P<libraryId>[^/]+)$', library.libraryAddSensorArray),
    url(r'^launchLibraryValidation/$', library.launchLibraryValidation),
    url(r'^libraryPermissions/(?P<libraryId>[^/]+)$', library.libraryPermissions),
    url(r'^deleteSensorArray/$', library.deleteSensorArray),
]

from .views import parameterInfo
urlpatterns += [
    url(r'^getInfo/(?P<infoType>.*)$',parameterInfo.getInfo),
]

from .views import projectPermissions
urlpatterns += [
    url(r'^projectPermissions/(?P<projectId>\d+)$', projectPermissions.projectPermissions),
]

from .views import reports
urlpatterns += [
    url(r'^createFileTagReport/(?P<projectId>\d+)$', reports.createFileTagReport),
]

from .views import sensor
urlpatterns += [
    url(r'^sensorArray/(?P<sensorArrayId>[^/]+)$', sensor.sensorArray),
    url(r'^updateSensorArrayLayout[/]?$', sensor.updateSensorArrayLayout),
]

from .views import supervisedTagDiscovery
urlpatterns += [
    url(r'^supervisedTagDiscoveryQueueRunner/(?P<projectId>\d+)$', supervisedTagDiscovery.supervisedTagDiscoveryQueueRunner),
    url(r'^supervisedTagDiscoveryQueueRunner/(?P<projectId>\d+)/fromJob/(?P<fromJob>\d+)$', supervisedTagDiscovery.supervisedTagDiscoveryQueueRunner),
    url(r'^supervisedTagDiscovery/(?P<projectId>\d+)$', supervisedTagDiscovery.supervisedTagDiscovery),
    url(r'^supervisedTagDiscovery/(?P<projectId>\d+)/fromJob/(?P<fromJob>\d+)$', supervisedTagDiscovery.supervisedTagDiscovery),
]

from .views import tagClass
urlpatterns += [
    url(r'^getTagClassAutoArray/(?P<projectId>\d+)$', tagClass.getTagClassAutoArray),
    url(r'^renameTagClass/(?P<tagClassId>\d+)$', tagClass.renameTagClass),
    url(r'^emptyTagClass/(?P<tagClassId>\d+)$', tagClass.emptyTagClass),
    url(r'^deleteTagClass/(?P<tagClassId>\d+)$', tagClass.deleteTagClass),
]

from .views import tag
urlpatterns += [
    url(r'^getTagInfo/(?P<tagID>.*)$', tag.getTagInfo),
    url(r'^viewTag/(?P<tagID>.*)$', tag.viewTag),
    url(r'^viewParentTag/(?P<tagID>.*)$', tag.viewParentTag),
    url(r'^deleteTag/(?P<tagID>.*)$', tag.deleteTag),
    url(r'^acceptTag/(?P<tagID>.*)$', tag.acceptTag),
    url(r'^getTagsInAudioSegment/(?P<audioID>.*)/(?P<startTime>.*)/(?P<endTime>.*)/(?P<minFrequency>.*)/(?P<maxFrequency>.*)$', tag.getTagsInAudioSegment),
    url(r'^createUserTag/(?P<projectID>.*)/(?P<mediaFileId>.*)$', tag.createUserTag),
    url(r'^navigateTags/(?P<projectID>\d+)/(?P<audioID>\d+)$', tag.navigateTags),
    url(r'^createUserTag/(?P<projectId>\d+)$', tag.newCreateUserTag),
    url(r'^adjustTag/(?P<tagId>\d+)$', tag.adjustTag),
    url(r'^deleteTags/(?P<projectId>\d+)$', tag.deleteTags),
    url(r'^getDeleteTagCount/(?P<projectId>\d+)$', tag.getDeleteTagCount),
]

from .views import tagSet
urlpatterns += [
    url(r'^deleteTagSet/(?P<tagSetId>\d+)$', tagSet.deleteTagSet),
    url(r'^tagSets$', tagSet.tagSets),
    url(r'^projectTagSets/(?P<projectId>\d+)$', tagSet.projectTagSets),
    url(r'^startNewTagSet/(?P<projectId>\d+)$', tagSet.startNewTagSet),
    url(r'^getProjectTagSetsInfo/(?P<projectId>\d+)$', tagSet.getProjectTagSetsInfo),
    url(r'^viewTagSet/(?P<tagSetId>.*)$', tagSet.viewTagSet),
]

from .views import tempFunctions
urlpatterns += [
    url(r'^compareExpertTagging/(?P<compareTagSetId>\d+)$', tempFunctions.compareExpertTagging),
]

from .views import transcriptCreation
urlpatterns += [
    url(r'^createTranscriptForTagSet/(?P<projectId>\d+)$', transcriptCreation.createTranscriptForTagSet),
]

from .views import pitchTraceCsv
urlpatterns += [
    url(r'^createPitchTraceCsv/(?P<projectId>\d+)/(?P<mediaFileId>\d+)$', pitchTraceCsv.createPitchTraceCsv),
]

from .views import unsupervisedTagDiscovery
urlpatterns += [
    url(r'^unsupervisedTagDiscoveryQueueRunner/(?P<projectId>\d+)$', unsupervisedTagDiscovery.unsupervisedTagDiscoveryQueueRunner),
    url(r'^unsupervisedTagDiscovery/(?P<projectId>\d+)$', unsupervisedTagDiscovery.unsupervisedTagDiscovery),
]

from .views import upload
urlpatterns += [
    url(r'^uploadAudioFile$', upload.uploadAudioFile),
    url(r'^uploadAudioFile2$', upload.uploadAudioFile2),
]

from .views import userPermissions
urlpatterns += [
    url(r'^userGroups/$', userPermissions.userGroupsView),
    url(r'^arloUserGroup/(?P<arloUserGroupId>\d+)$', userPermissions.arloUserGroup)
]

from .views import userSettings
urlpatterns += [
    url(r'^userSettings$', userSettings.userSettings),
    url(r'^viewSettings/(?P<settingsName>.*)$', userSettings.viewSettings),
    url(r'^deleteSettings/(?P<settingsName>.*)$', userSettings.deleteSettings),
]

from .views import viewJob
urlpatterns += [
    url(r'^viewJob/(?P<jobID>\d+)$', viewJob.viewJob, name='viewJob'),
]

from .views import viewMetaData
urlpatterns += [
    url(r'^guessTimeFromName/(?P<audioID>[^/]+)$', viewMetaData.guessTimeFromName),
    url(r'^viewMetadata/(?P<audioID>[^/]+)$', viewMetaData.viewMetadata),
]

from .views import visualizeMultiProjectFile
urlpatterns += [
    url(r'^visualizeMultiProjectFile/(?P<projectId>\d+)$', visualizeMultiProjectFile.visualizeMultiProjectFile),
    url(r'^createAudioSpectra/(?P<audioFileId>[^/]+)$', visualizeMultiProjectFile.newCreateAudioSpectra),
]

from .views import visualizeProjectFile
urlpatterns += [
    url(r'^visualizeProjectFile/(?P<projectId>\d+)/(?P<audioID>\d+)/$', visualizeProjectFile.visualizeProjectFile),
    url(r'^visualizeProjectFileTags/(?P<projectId>\d+)/(?P<audioID>\d+)/(?P<tagSetID>\d+)/$', visualizeProjectFile.visualizeProjectFileTags),
    url(r'^visualizeProjectTags/(?P<projectId>\d+)/(?P<tagsetID>\d+)/$', visualizeProjectFile.visualizeProjectTags),
]

from .views import visualize
urlpatterns += [
    url(r'^visualizeAudio/(?P<audioID>.*)$', visualize.visualizeAudio),
    url(r'^tagRandomWindow/(?P<randomWindowTaggingBiasID>.*)/(?P<trialIndex>.*)$', visualize.tagRandomWindow),
    url(r'^createAudioSpectra$', visualize.createAudioSpectra),
]

from .views import wekaJob
urlpatterns += [
    url(r'^launchWekaJob/(?P<projectId>.*)$', wekaJob.launchWekaJob),
]

from .views import jobs
urlpatterns += [
    url(r'^launchJob/(?P<projectId>\d+)/selectJobType$', jobs.selectJobType),
    url(r'^launchJob/(?P<projectId>\d+)/configureJob/(?P<jobTypeName>.*)$', jobs.configureJob, name='configureJob'),
]


###          ###
###  UNUSED  ###
###          ###

##    (r'^tagAnalysis/(?P<projectName>.*)$', 'tagAnalysis'),
##    (r'^unknownTagDiscovery/(?P<projectName>.*)$', 'unknownTagDiscovery'),
##    (r'^createSettings$', 'createSettings'),
##    (r'^publicTagSets', 'publicTagSets'),
##    (r'^importPublicTagSet/(?P<tagSetName>[^/]+)/(?P<username>[^/]+)$','importPublicTagSet'),
## disabled during Cassandra updates
##    (r'^startTranscript/(?P<projectName>[^/]+)/(?P<audioID>.*)$','startTranscript'),
##    (r'^createTranscript/(?P<projectName>[^/]+)/(?P<audioID>.*)$', 'createTranscript'),
##    (r'^createTranscriptForProject/(?P<projectName>[^/]+)$', 'createTranscriptForProject'),
##    (r'^exportTagSetForProject/(?P<projectName>[^/]+)$', 'exportTagSetForProject'),
##    (r'^deleteTranscript/(?P<transcriptID>.*)$', 'deleteTranscript'),
##    (r'^resumeTranscript/(?P<transcriptID>.*)$', 'resumeTranscript'),
##    (r'^pauseTranscript/(?P<transcriptID>.*)$', 'pauseTranscript'),
##    (r'^deleteSupervisedBias/(?P<biasID>.*)$', 'deleteSupervisedBias'),
##    (r'^resumeSupervisedBias/(?P<biasID>.*)$', 'resumeSupervisedBias'),
##    (r'^pauseSupervisedBias/(?P<biasID>.*)$', 'pauseSupervisedBias'),
##
##    (r'^deleteUnsupervisedBias/(?P<biasID>.*)$', 'deleteUnsupervisedBias'),
##    (r'^resumeUnsupervisedBias/(?P<biasID>.*)$', 'resumeUnsupervisedBias'),
##    (r'^pauseTagAnalysis/(?P<biasID>.*)$', 'pauseTagAnalysis'),
##
##    (r'^deleteTagAnalysis/(?P<biasID>.*)$', 'deleteTagAnalysis'),
##    (r'^resumeTagAnalysis/(?P<biasID>.*)$', 'resumeTagAnalysis'),
##    (r'^pauseTagAnalysis/(?P<biasID>.*)$', 'pauseTagAnalysis'),
##    (r'^dumpUserTags', 'dumpUserTags'),
##    (r'^importUserTags', 'importUserTags'),
#    ####################
#    ## Hidden Functions
## removed from Cassandra updates... hidden and unused
##    (r'^autoSetTimeAllFiles/(?P<projectName>.*)$', 'autoSetTimeAllFiles'),
