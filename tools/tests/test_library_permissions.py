from django.test import TestCase
from django.contrib.auth.models import User

from tools.models import Library, ArloPermission, ArloUserGroup, Project, ProjectTypes
from tools.views.userPermissions import ARLO_PERMS, ArloLibraryPermissions


class ArloLibraryPermissionTest(TestCase):
#     fixtures = ['api_test_data.yaml']


    def test_library_owner_permissions(self):

        # Add a User and Library - ensure user get's all privileges for admin
        user_owner = User(username="Test User Owner")
        user_owner.save()

        library1 = Library(user=user_owner, name='Test Library 1')
        library1.save()

        self.assertTrue(library1.userHasPermissions(user_owner, ARLO_PERMS.READ | ARLO_PERMS.WRITE | ARLO_PERMS.ADMIN | ARLO_PERMS.LAUNCH_JOB))
        self.assertTrue(library1.userHasPermissions(user_owner, ARLO_PERMS.READ | ARLO_PERMS.WRITE | ARLO_PERMS.ADMIN | ARLO_PERMS.LAUNCH_JOB, False))

        # another user should have no access
        user_deny = User(username="Test User Deny")
        user_deny.save()
        self.assertFalse(library1.userHasPermissions(user_deny, ARLO_PERMS.READ))
        self.assertFalse(library1.userHasPermissions(user_deny, ARLO_PERMS.WRITE))
        self.assertFalse(library1.userHasPermissions(user_deny, ARLO_PERMS.ADMIN))
        self.assertFalse(library1.userHasPermissions(user_deny, ARLO_PERMS.LAUNCH_JOB))


    def test_library_user_permissions(self):

        # Add a User and Library
        user_owner = User(username="Test User Owner")
        user_owner.save()

        library1 = Library(user=user_owner, name='Test Library 1')
        library1.save()

        USER_PERMS = (
            # (username, perms)
            ('user_r', ARLO_PERMS.READ),
            ('user_rw', ARLO_PERMS.READ | ARLO_PERMS.WRITE),
            ('user_rwx', ARLO_PERMS.READ | ARLO_PERMS.WRITE | ARLO_PERMS.LAUNCH_JOB),
            ('user_rwxa', ARLO_PERMS.READ | ARLO_PERMS.WRITE | ARLO_PERMS.LAUNCH_JOB | ARLO_PERMS.ADMIN),
            )

        # Add users and Permissions
        for username, perms in USER_PERMS:
            user = User(username=username)
            user.save()

            read = (perms & ARLO_PERMS.READ) > 0
            write = (perms & ARLO_PERMS.WRITE) > 0
            launch_job = (perms & ARLO_PERMS.LAUNCH_JOB) > 0
            admin = (perms & ARLO_PERMS.ADMIN) > 0

            ArloPermission(
                bearer_object=user,
                target_object=library1,
                createdBy=user_owner,
                read=read,
                write=write,
                launch_job=launch_job,
                admin=admin).save()

        # check permissions
        for username, perms in USER_PERMS:
            user = User.objects.get(username=username)

            read = (perms & ARLO_PERMS.READ) > 0
            write = (perms & ARLO_PERMS.WRITE) > 0
            launch_job = (perms & ARLO_PERMS.LAUNCH_JOB) > 0
            admin = (perms & ARLO_PERMS.ADMIN) > 0

            self.assertEqual(library1.userHasPermissions(user, ARLO_PERMS.READ), read)
            self.assertEqual(library1.userHasPermissions(user, ARLO_PERMS.WRITE), write)
            self.assertEqual(library1.userHasPermissions(user, ARLO_PERMS.LAUNCH_JOB), launch_job)
            self.assertEqual(library1.userHasPermissions(user, ARLO_PERMS.ADMIN), admin)


    def test_library_group_permissions(self):

        # Add a User and Library
        user_owner = User(username="Test User Owner")
        user_owner.save()

        library1 = Library(user=user_owner, name='Test Library 1')
        library1.save()

        GROUP_PERMS = (
            # (groupname, username, perms)
            ('group_r', 'user_r', ARLO_PERMS.READ),
            ('group_rw', 'user_rw', ARLO_PERMS.READ | ARLO_PERMS.WRITE),
            ('group_rwx', 'user_rwx', ARLO_PERMS.READ | ARLO_PERMS.WRITE | ARLO_PERMS.LAUNCH_JOB),
            ('group_rwxa', 'user_rwxa', ARLO_PERMS.READ | ARLO_PERMS.WRITE | ARLO_PERMS.LAUNCH_JOB | ARLO_PERMS.ADMIN),
            )

        # Add users and Permissions
        for groupname, username, perms in GROUP_PERMS:
            user = User(username=username)
            user.save()

            group = ArloUserGroup(name=groupname, createdBy=user_owner)
            group.save()
            group.users.add(user)

            read = (perms & ARLO_PERMS.READ) > 0
            write = (perms & ARLO_PERMS.WRITE) > 0
            launch_job = (perms & ARLO_PERMS.LAUNCH_JOB) > 0
            admin = (perms & ARLO_PERMS.ADMIN) > 0

            ArloPermission(
                bearer_object=group,
                target_object=library1,
                createdBy=user_owner,
                read=read,
                write=write,
                launch_job=launch_job,
                admin=admin).save()

        # check permissions
        for groupname, username, perms in GROUP_PERMS:
            user = User.objects.get(username=username)

            read = (perms & ARLO_PERMS.READ) > 0
            write = (perms & ARLO_PERMS.WRITE) > 0
            launch_job = (perms & ARLO_PERMS.LAUNCH_JOB) > 0
            admin = (perms & ARLO_PERMS.ADMIN) > 0

            self.assertEqual(library1.userHasPermissions(user, ARLO_PERMS.READ), read)
            self.assertEqual(library1.userHasPermissions(user, ARLO_PERMS.WRITE), write)
            self.assertEqual(library1.userHasPermissions(user, ARLO_PERMS.LAUNCH_JOB), launch_job)
            self.assertEqual(library1.userHasPermissions(user, ARLO_PERMS.ADMIN), admin)


        ###
        # Create separate groups for each permission, add one user to all
        # groups, and ensure the user has ALL permissions (union of all perms)

        SEP_USER_NAME = 'user_sep'
        SEP_GROUP_PERMS = (
            # (groupname, perms)
            ('sep_group_r', ARLO_PERMS.READ),
            ('sep_group_w', ARLO_PERMS.WRITE),
            ('sep_group_x', ARLO_PERMS.LAUNCH_JOB),
            )

        # Add user and Permissions
        user = User(username=SEP_USER_NAME)
        user.save()

        for groupname, perms in SEP_GROUP_PERMS:
            group = ArloUserGroup(name=groupname, createdBy=user_owner)
            group.save()
            group.users.add(user)

            read = (perms & ARLO_PERMS.READ) > 0
            write = (perms & ARLO_PERMS.WRITE) > 0
            launch_job = (perms & ARLO_PERMS.LAUNCH_JOB) > 0
            admin = (perms & ARLO_PERMS.ADMIN) > 0

            ArloPermission(
                bearer_object=group,
                target_object=library1,
                createdBy=user_owner,
                read=read,
                write=write,
                launch_job=launch_job,
                admin=admin).save()

        self.assertTrue(library1.userHasPermissions(user, ARLO_PERMS.READ))
        self.assertTrue(library1.userHasPermissions(user, ARLO_PERMS.WRITE))
        self.assertTrue(library1.userHasPermissions(user, ARLO_PERMS.LAUNCH_JOB))


    ## Test the special Default permissions group. All users with no other
    # permissions should inherit these permissions.

    def test_library_default_permissions(self):

        # Add a User and Library
        user_owner = User(username="Test User Owner")
        user_owner.save()

        user_default = User(username="Test Default User")
        user_default.save()

        library1 = Library(user=user_owner, name='Test Library 1')
        library1.save()

        self.assertFalse(library1.userHasPermissions(user_default, ARLO_PERMS.READ))
        self.assertFalse(library1.userHasPermissions(user_default, ARLO_PERMS.WRITE))
        self.assertFalse(library1.userHasPermissions(user_default, ARLO_PERMS.LAUNCH_JOB))
        self.assertFalse(library1.userHasPermissions(user_default, ARLO_PERMS.ADMIN))

        library1.default_permission_read = True
        library1.default_permission_write = True
        library1.default_permission_launch_job = True
        library1.default_permission_admin = True
        library1.save()

        self.assertTrue(library1.userHasPermissions(user_default, ARLO_PERMS.READ | ARLO_PERMS.WRITE | ARLO_PERMS.ADMIN | ARLO_PERMS.LAUNCH_JOB))


    ## Test the 'shortcutting' of permissions. Permissions should be
    # determined in the following order, with the first found having
    # priority over all others:
    # - Owner
    # - User Perms
    # - Group Perms
    # - Default Perms

    def test_library_shortcutting_permissions(self):

        # Add a User and Library
        user_owner = User(username="Test User Owner")
        user_owner.save()

        library1 = Library(user=user_owner, name='Test Library 1')
        library1.save()

        ###
        # Test Owner

        # add '0' perms for the owner in user/group perms - these should be ignored

        ArloPermission(
            bearer_object=user_owner,
            target_object=library1,
            createdBy=user_owner,
            read=False,
            write=False,
            launch_job=False,
            admin=False).save()

        group_deny = ArloUserGroup(name='group_deny', createdBy=user_owner)
        group_deny.save()
        group_deny.users.add(user_owner)

        ArloPermission(
            bearer_object=group_deny,
            target_object=library1,
            createdBy=user_owner,
            read=False,
            write=False,
            launch_job=False,
            admin=False).save()

        self.assertTrue(library1.userHasPermissions(user_owner, ARLO_PERMS.READ | ARLO_PERMS.WRITE | ARLO_PERMS.ADMIN | ARLO_PERMS.LAUNCH_JOB))
        self.assertTrue(library1.userHasPermissions(user_owner, ARLO_PERMS.READ | ARLO_PERMS.WRITE | ARLO_PERMS.ADMIN | ARLO_PERMS.LAUNCH_JOB, False))

        ###
        # Test User

        user_test = User(username="Test User")
        user_test.save()

        self.assertFalse(library1.userHasPermissions(user_test, ARLO_PERMS.READ))
        self.assertFalse(library1.userHasPermissions(user_test, ARLO_PERMS.WRITE))
        self.assertFalse(library1.userHasPermissions(user_test, ARLO_PERMS.LAUNCH_JOB))
        self.assertFalse(library1.userHasPermissions(user_test, ARLO_PERMS.ADMIN))

        # first start with default ALL perms, then add a user / group with '0' perms and ensure they have no access

        library1.default_permission_read = True
        library1.default_permission_write = True
        library1.default_permission_launch_job = True
        library1.default_permission_admin = True
        library1.save()

        self.assertTrue(library1.userHasPermissions(user_test, ARLO_PERMS.READ | ARLO_PERMS.WRITE | ARLO_PERMS.ADMIN | ARLO_PERMS.LAUNCH_JOB))

        # add user to deny group
        group_deny.users.add(user_test)
        self.assertFalse(library1.userHasPermissions(user_test, ARLO_PERMS.READ))
        self.assertFalse(library1.userHasPermissions(user_test, ARLO_PERMS.WRITE))
        self.assertFalse(library1.userHasPermissions(user_test, ARLO_PERMS.LAUNCH_JOB))
        self.assertFalse(library1.userHasPermissions(user_test, ARLO_PERMS.ADMIN))
        group_deny.users.remove(user_test)
        self.assertTrue(library1.userHasPermissions(user_test, ARLO_PERMS.READ | ARLO_PERMS.WRITE | ARLO_PERMS.ADMIN | ARLO_PERMS.LAUNCH_JOB))

        # add explicit perms for user to deny
        user_deny_perms = ArloPermission(
            bearer_object=user_test,
            target_object=library1,
            createdBy=user_owner,
            read=False,
            write=False,
            launch_job=False,
            admin=False)
        user_deny_perms.save()
        self.assertFalse(library1.userHasPermissions(user_test, ARLO_PERMS.READ))
        self.assertFalse(library1.userHasPermissions(user_test, ARLO_PERMS.WRITE))
        self.assertFalse(library1.userHasPermissions(user_test, ARLO_PERMS.LAUNCH_JOB))
        self.assertFalse(library1.userHasPermissions(user_test, ARLO_PERMS.ADMIN))
        user_deny_perms.delete()
        self.assertTrue(library1.userHasPermissions(user_test, ARLO_PERMS.READ | ARLO_PERMS.WRITE | ARLO_PERMS.ADMIN | ARLO_PERMS.LAUNCH_JOB))


    ##
    # Ensure system admin ('staff') users cat get permission overrides.

    def test_library_staff_override_permissions(self):

        # Add a User and Library
        user_owner = User(username="Test User Owner")
        user_owner.save()

        library1 = Library(user=user_owner, name='Test Library 1')
        library1.save()

        user_staff = User(username='Test User Staff', is_staff=True)
        user_staff.save()


        # add '0' perms for in user/group perms - these should be ignored

        ArloPermission(
            bearer_object=user_staff,
            target_object=library1,
            createdBy=user_owner,
            read=False,
            write=False,
            launch_job=False,
            admin=False).save()

        group_deny = ArloUserGroup(name='group_deny', createdBy=user_owner)
        group_deny.save()
        group_deny.users.add(user_staff)

        ArloPermission(
            bearer_object=group_deny,
            target_object=library1,
            createdBy=user_owner,
            read=False,
            write=False,
            launch_job=False,
            admin=False).save()

        self.assertTrue(library1.userHasPermissions(user_staff, ARLO_PERMS.READ | ARLO_PERMS.WRITE | ARLO_PERMS.ADMIN | ARLO_PERMS.LAUNCH_JOB))
        # ensure admin override will turn off
        self.assertFalse(library1.userHasPermissions(user_staff, ARLO_PERMS.READ, False))
        self.assertFalse(library1.userHasPermissions(user_staff, ARLO_PERMS.WRITE, False))
        self.assertFalse(library1.userHasPermissions(user_staff, ARLO_PERMS.LAUNCH_JOB, False))
        self.assertFalse(library1.userHasPermissions(user_staff, ARLO_PERMS.ADMIN, False))


    def test_throws_exception_on_invalid_type(self):
        # Should raise an Exception for another object type
        user_owner = User(username="Test User Owner")
        user_owner.save()
        projectType = ProjectTypes(name='test')
        projectType.save()
        project1 = Project(user=user_owner, name='Test Project 1', type=projectType)
        project1.save()
        self.assertRaises(TypeError, ArloLibraryPermissions, project1)


    def test_admin_perms(self):
        """
        Admin perms should implicitly grant ALL perms.
        """

        user_owner = User(username="Test User Owner")
        user_owner.save()

        library1 = Library(user=user_owner, name='Test Library 1')
        library1.save()

        new_admin_user = User(username='newadmin')
        new_admin_user.save()

        # Grant Admin to the user on the Project
        ArloPermission(
            bearer_object=new_admin_user,
            target_object=library1,
            createdBy=user_owner,
            read=False,
            write=False,
            launch_job=False,
            admin=True).save()

        self.assertTrue(library1.userHasPermissions(new_admin_user, ARLO_PERMS.READ | ARLO_PERMS.WRITE | ARLO_PERMS.LAUNCH_JOB | ARLO_PERMS.ADMIN))


    def test_getUserLibraries(self):
        """
        Add a user to Libraries via several methods, ensuring that all
        libraries show up in our query.
        """

        # Add a User and Project
        user_owner = User(username="Test User Owner")
        user_owner.save()
        user = User(username="Test User")
        user.save()
        user_dummy = User(username="Dummy User")
        user_dummy.save()

        # ensure the user doesn't yet have any projects returned
        libraries = Library.objects.getUserLibraries(user)
        self.assertEqual(libraries.count(), 0)

        library1 = Library(user=user_owner, name='Test Library 1')
        library1.save()

        # test as owner
        library_own = Library(user=user, name='Test Library Owner')
        library_own.save()
        libraries = Library.objects.getUserLibraries(user)
        self.assertEqual(libraries.count(), 1)

        # add user direct
        library_user = Library(user=user_owner, name='Test Library User')
        library_user.save()
        # add our dummy user, make sure it doesn't get counted in
        ArloPermission(
            bearer_object=user_dummy,
            target_object=library_user,
            createdBy=user_owner,
            read=True,
            write=True,
            launch_job=True,
            admin=False).save()
        libraries = Library.objects.getUserLibraries(user)
        self.assertEqual(libraries.count(), 1)
        ArloPermission(
            bearer_object=user,
            target_object=library_user,
            createdBy=user_owner,
            read=True,
            write=True,
            launch_job=True,
            admin=False).save()
        libraries = Library.objects.getUserLibraries(user)
        self.assertEqual(libraries.count(), 2)

        # add user via groups
        library_group = Library(user=user_owner, name='Test Library Group')
        library_group.save()
        group = ArloUserGroup(name='Test Group', createdBy=user_owner)
        group.save()
        ArloPermission(
            bearer_object=group,
            target_object=library_group,
            createdBy=user_owner,
            read=True,
            write=True,
            launch_job=True,
            admin=False).save()
        # add our dummy user, make sure it doesn't get counted in
        group.users.add(user_dummy)
        libraries = Library.objects.getUserLibraries(user)
        self.assertEqual(libraries.count(), 2)
        group.users.add(user)
        libraries = Library.objects.getUserLibraries(user)
        self.assertEqual(libraries.count(), 3)

        # Add user multiple times, ensuring we don't get duplicates
        ArloPermission(
            bearer_object=user,
            target_object=library_group,
            createdBy=user_owner,
            read=True,
            write=True,
            launch_job=True,
            admin=False).save()
        libraries = Library.objects.getUserLibraries(user)
        self.assertEqual(libraries.count(), 3)
