from django.test import TestCase
from django.contrib.auth.models import User

from tools.models import TagSet, ArloPermission, ArloUserGroup, Project, ProjectTypes
from tools.views.userPermissions import ARLO_PERMS, ArloTagSetPermissions

class ArloTagSetPermissionTest(TestCase):
    fixtures = ['tagset_test_data.yaml']

    def test_tagset_owner_permissions(self):

        # Get Objects (loaded by fixture)
        admin_user = User.objects.get(pk=1)
        regular_user = User.objects.get(pk=2)

        admin_tagset = TagSet.objects.get(pk=1)
        regular_tagset = TagSet.objects.get(pk=2)

        # Check owners
        self.assertTrue(admin_tagset.userHasPermissions(admin_user, ARLO_PERMS.READ | ARLO_PERMS.WRITE | ARLO_PERMS.ADMIN | ARLO_PERMS.LAUNCH_JOB), True)
        self.assertTrue(admin_tagset.userHasPermissions(admin_user, ARLO_PERMS.READ | ARLO_PERMS.WRITE | ARLO_PERMS.ADMIN | ARLO_PERMS.LAUNCH_JOB), False)
        self.assertTrue(regular_tagset.userHasPermissions(regular_user, ARLO_PERMS.READ | ARLO_PERMS.WRITE | ARLO_PERMS.ADMIN | ARLO_PERMS.LAUNCH_JOB), True)
        self.assertTrue(regular_tagset.userHasPermissions(regular_user, ARLO_PERMS.READ | ARLO_PERMS.WRITE | ARLO_PERMS.ADMIN | ARLO_PERMS.LAUNCH_JOB), False)

        # Check admin access
        self.assertTrue(regular_tagset.userHasPermissions(admin_user, ARLO_PERMS.READ | ARLO_PERMS.WRITE | ARLO_PERMS.ADMIN | ARLO_PERMS.LAUNCH_JOB), True)
        self.assertTrue(regular_tagset.userHasPermissions(admin_user, ARLO_PERMS.READ | ARLO_PERMS.WRITE | ARLO_PERMS.ADMIN | ARLO_PERMS.LAUNCH_JOB), False)

        # another user should have no access
        self.assertFalse(admin_tagset.userHasPermissions(regular_user, ARLO_PERMS.READ))
        self.assertFalse(admin_tagset.userHasPermissions(regular_user, ARLO_PERMS.WRITE))
        self.assertFalse(admin_tagset.userHasPermissions(regular_user, ARLO_PERMS.ADMIN))
        self.assertFalse(admin_tagset.userHasPermissions(regular_user, ARLO_PERMS.LAUNCH_JOB))


    def test_tagset_user_permissions(self):

        # Get Objects (loaded by fixture)
        admin_user = User.objects.get(pk=1)
        regular_tagset = TagSet.objects.get(pk=2)


        USER_PERMS = (
            # (username, perms)
            ('user_r', ARLO_PERMS.READ),
            ('user_rw', ARLO_PERMS.READ | ARLO_PERMS.WRITE),
            ('user_rwx', ARLO_PERMS.READ | ARLO_PERMS.WRITE | ARLO_PERMS.LAUNCH_JOB),
            ('user_rwxa', ARLO_PERMS.READ | ARLO_PERMS.WRITE | ARLO_PERMS.LAUNCH_JOB | ARLO_PERMS.ADMIN),
            )

        # Add users and Permissions
        for username, perms in USER_PERMS:
            user = User(username=username)
            user.save()

            read = (perms & ARLO_PERMS.READ) > 0
            write = (perms & ARLO_PERMS.WRITE) > 0
            launch_job = (perms & ARLO_PERMS.LAUNCH_JOB) > 0
            admin = (perms & ARLO_PERMS.ADMIN) > 0

            ArloPermission(
                bearer_object=user,
                target_object=regular_tagset,
                createdBy=admin_user,
                read=read,
                write=write,
                launch_job=launch_job,
                admin=admin).save()

        # check permissions
        for username, perms in USER_PERMS:
            user = User.objects.get(username=username)

            read = (perms & ARLO_PERMS.READ) > 0
            write = (perms & ARLO_PERMS.WRITE) > 0
            launch_job = (perms & ARLO_PERMS.LAUNCH_JOB) > 0
            admin = (perms & ARLO_PERMS.ADMIN) > 0

            self.assertEqual(regular_tagset.userHasPermissions(user, ARLO_PERMS.READ), read)
            self.assertEqual(regular_tagset.userHasPermissions(user, ARLO_PERMS.WRITE), write)
            self.assertEqual(regular_tagset.userHasPermissions(user, ARLO_PERMS.LAUNCH_JOB), launch_job)
            self.assertEqual(regular_tagset.userHasPermissions(user, ARLO_PERMS.ADMIN), admin)


    def test_tagset_group_permissions(self):

        # Get Objects (loaded by fixture)
        admin_user = User.objects.get(pk=1)
        regular_tagset = TagSet.objects.get(pk=2)

        GROUP_PERMS = (
            # (groupname, username, perms)
            ('group_r', 'user_r', ARLO_PERMS.READ),
            ('group_rw', 'user_rw', ARLO_PERMS.READ | ARLO_PERMS.WRITE),
            ('group_rwx', 'user_rwx', ARLO_PERMS.READ | ARLO_PERMS.WRITE | ARLO_PERMS.LAUNCH_JOB),
            ('group_rwxa', 'user_rwxa', ARLO_PERMS.READ | ARLO_PERMS.WRITE | ARLO_PERMS.LAUNCH_JOB | ARLO_PERMS.ADMIN),
            )

        # Add users and Permissions
        for groupname, username, perms in GROUP_PERMS:
            user = User(username=username)
            user.save()

            group = ArloUserGroup(name=groupname, createdBy=admin_user)
            group.save()
            group.users.add(user)

            read = (perms & ARLO_PERMS.READ) > 0
            write = (perms & ARLO_PERMS.WRITE) > 0
            launch_job = (perms & ARLO_PERMS.LAUNCH_JOB) > 0
            admin = (perms & ARLO_PERMS.ADMIN) > 0

            ArloPermission(
                bearer_object=group,
                target_object=regular_tagset,
                createdBy=admin_user,
                read=read,
                write=write,
                launch_job=launch_job,
                admin=admin).save()

        # check permissions
        for groupname, username, perms in GROUP_PERMS:
            user = User.objects.get(username=username)

            read = (perms & ARLO_PERMS.READ) > 0
            write = (perms & ARLO_PERMS.WRITE) > 0
            launch_job = (perms & ARLO_PERMS.LAUNCH_JOB) > 0
            admin = (perms & ARLO_PERMS.ADMIN) > 0

            self.assertEqual(regular_tagset.userHasPermissions(user, ARLO_PERMS.READ), read)
            self.assertEqual(regular_tagset.userHasPermissions(user, ARLO_PERMS.WRITE), write)
            self.assertEqual(regular_tagset.userHasPermissions(user, ARLO_PERMS.LAUNCH_JOB), launch_job)
            self.assertEqual(regular_tagset.userHasPermissions(user, ARLO_PERMS.ADMIN), admin)


        ###
        # Create separate groups for each permission, add one user to all
        # groups, and ensure the user has ALL permissions (union of all perms)

        SEP_USER_NAME = 'user_sep'
        SEP_GROUP_PERMS = (
            # (groupname, perms)
            ('sep_group_r', ARLO_PERMS.READ),
            ('sep_group_w', ARLO_PERMS.WRITE),
            ('sep_group_x', ARLO_PERMS.LAUNCH_JOB),
            )

        # Add user and Permissions
        user = User(username=SEP_USER_NAME)
        user.save()

        for groupname, perms in SEP_GROUP_PERMS:
            group = ArloUserGroup(name=groupname, createdBy=admin_user)
            group.save()
            group.users.add(user)

            read = (perms & ARLO_PERMS.READ) > 0
            write = (perms & ARLO_PERMS.WRITE) > 0
            launch_job = (perms & ARLO_PERMS.LAUNCH_JOB) > 0
            admin = (perms & ARLO_PERMS.ADMIN) > 0

            ArloPermission(
                bearer_object=group,
                target_object=regular_tagset,
                createdBy=admin_user,
                read=read,
                write=write,
                launch_job=launch_job,
                admin=admin).save()

        self.assertTrue(regular_tagset.userHasPermissions(user, ARLO_PERMS.READ))
        self.assertTrue(regular_tagset.userHasPermissions(user, ARLO_PERMS.WRITE))
        self.assertTrue(regular_tagset.userHasPermissions(user, ARLO_PERMS.LAUNCH_JOB))


    ## Test the 'shortcutting' of permissions. Permissions should be
    # determined in the following order, with the first found having
    # priority over all others:
    # - Owner
    # - User Perms
    # - Group Perms

    def test_tagset_shortcutting_permissions(self):

        # Get Objects (loaded by fixture)
        admin_user = User.objects.get(pk=1)
        regular_user = User.objects.get(pk=2)
        regular_tagset = TagSet.objects.get(pk=2)


        ###
        # Test Owner

        # add '0' perms for the owner in user/group perms - these should be ignored

        ArloPermission(
            bearer_object=regular_user,
            target_object=regular_tagset,
            createdBy=admin_user,
            read=False,
            write=False,
            launch_job=False,
            admin=False).save()

        group_deny = ArloUserGroup(name='group_deny', createdBy=admin_user)
        group_deny.save()
        group_deny.users.add(regular_user)

        ArloPermission(
            bearer_object=group_deny,
            target_object=regular_tagset,
            createdBy=admin_user,
            read=False,
            write=False,
            launch_job=False,
            admin=False).save()

        self.assertTrue(regular_tagset.userHasPermissions(regular_user, ARLO_PERMS.READ | ARLO_PERMS.WRITE | ARLO_PERMS.ADMIN | ARLO_PERMS.LAUNCH_JOB))
        self.assertTrue(regular_tagset.userHasPermissions(regular_user, ARLO_PERMS.READ | ARLO_PERMS.WRITE | ARLO_PERMS.ADMIN | ARLO_PERMS.LAUNCH_JOB, False))

        ###
        # Test User

        user_test = User(username="Test User")
        user_test.save()

        self.assertFalse(regular_tagset.userHasPermissions(user_test, ARLO_PERMS.READ))
        self.assertFalse(regular_tagset.userHasPermissions(user_test, ARLO_PERMS.WRITE))
        self.assertFalse(regular_tagset.userHasPermissions(user_test, ARLO_PERMS.LAUNCH_JOB))
        self.assertFalse(regular_tagset.userHasPermissions(user_test, ARLO_PERMS.ADMIN))


    ##
    # Ensure system admin ('staff') users can get permission overrides.

    def test_tagset_staff_override_permissions(self):

        # Get Objects (loaded by fixture)
        admin_user = User.objects.get(pk=1)
        regular_tagset = TagSet.objects.get(pk=2)


        # add '0' perms for in user/group perms - these should be ignored

        ArloPermission(
            bearer_object=admin_user,
            target_object=regular_tagset,
            createdBy=admin_user,
            read=False,
            write=False,
            launch_job=False,
            admin=False).save()

        group_deny = ArloUserGroup(name='group_deny', createdBy=admin_user)
        group_deny.save()
        group_deny.users.add(admin_user)

        ArloPermission(
            bearer_object=group_deny,
            target_object=regular_tagset,
            createdBy=admin_user,
            read=False,
            write=False,
            launch_job=False,
            admin=False).save()

        self.assertTrue(regular_tagset.userHasPermissions(admin_user, ARLO_PERMS.READ | ARLO_PERMS.WRITE | ARLO_PERMS.ADMIN | ARLO_PERMS.LAUNCH_JOB))
        # ensure admin override will turn off
        self.assertFalse(regular_tagset.userHasPermissions(admin_user, ARLO_PERMS.READ, False))
        self.assertFalse(regular_tagset.userHasPermissions(admin_user, ARLO_PERMS.WRITE, False))
        self.assertFalse(regular_tagset.userHasPermissions(admin_user, ARLO_PERMS.LAUNCH_JOB, False))
        self.assertFalse(regular_tagset.userHasPermissions(admin_user, ARLO_PERMS.ADMIN, False))


    ## Test that if there are no permissions for the TagSet, we fall back
    #  to the owning Project's permissions.

    def test_tagset_project_fallback(self):

        # Get Objects (loaded by fixture)
        admin_user = User.objects.get(pk=1)
        regular_tagset = TagSet.objects.get(pk=2)
        regular_project = Project.objects.get(pk=2)

        USER_PERMS = (
            # (username, perms)
            ('user_r', ARLO_PERMS.READ),
            ('user_rw', ARLO_PERMS.READ | ARLO_PERMS.WRITE),
            ('user_rwx', ARLO_PERMS.READ | ARLO_PERMS.WRITE | ARLO_PERMS.LAUNCH_JOB),
            ('user_rwxa', ARLO_PERMS.READ | ARLO_PERMS.WRITE | ARLO_PERMS.LAUNCH_JOB | ARLO_PERMS.ADMIN),
            )

        # Add users and Permissions
        for username, perms in USER_PERMS:
            user = User(username=username)
            user.save()

            read = (perms & ARLO_PERMS.READ) > 0
            write = (perms & ARLO_PERMS.WRITE) > 0
            launch_job = (perms & ARLO_PERMS.LAUNCH_JOB) > 0
            admin = (perms & ARLO_PERMS.ADMIN) > 0

            ArloPermission(
                bearer_object=user,
                target_object=regular_project,
                createdBy=admin_user,
                read=read,
                write=write,
                launch_job=launch_job,
                admin=admin).save()

        # check permissions
        for username, perms in USER_PERMS:
            user = User.objects.get(username=username)

            read = (perms & ARLO_PERMS.READ) > 0
            write = (perms & ARLO_PERMS.WRITE) > 0
            launch_job = (perms & ARLO_PERMS.LAUNCH_JOB) > 0
            admin = (perms & ARLO_PERMS.ADMIN) > 0

            self.assertEqual(regular_tagset.userHasPermissions(user, ARLO_PERMS.READ), read)
            self.assertEqual(regular_tagset.userHasPermissions(user, ARLO_PERMS.WRITE), write)
            self.assertEqual(regular_tagset.userHasPermissions(user, ARLO_PERMS.LAUNCH_JOB), launch_job)
            self.assertEqual(regular_tagset.userHasPermissions(user, ARLO_PERMS.ADMIN), admin)


    def test_throws_exception_on_invalid_type(self):
        # Should raise an Exception for another object type
        user_owner = User(username="Test User Owner")
        user_owner.save()
        projectType = ProjectTypes(name='test')
        projectType.save()
        project1 = Project(user=user_owner, name='Test Project 1', type=projectType)
        project1.save()
        self.assertRaises(TypeError, ArloTagSetPermissions, project1)


    def test_tagset_project_admin_override(self):
        """
        If a user has Admin permissions on a Project, then that user
        should receive Admin on ALL TagSets, regardless of permissions
        on the TagSet.
        """

        # Get Objects (loaded by fixture)
        admin_user = User.objects.get(pk=1)
        regular_tagset = TagSet.objects.get(pk=2)
        regular_project = Project.objects.get(pk=2)

        new_admin_user = User(username='newadmin')
        new_admin_user.save()

        # Grant Admin to the user on the Project
        ArloPermission(
            bearer_object=new_admin_user,
            target_object=regular_project,
            createdBy=admin_user,
            read=False,
            write=False,
            launch_job=False,
            admin=True).save()

        self.assertTrue(regular_tagset.userHasPermissions(new_admin_user, ARLO_PERMS.READ | ARLO_PERMS.WRITE | ARLO_PERMS.LAUNCH_JOB | ARLO_PERMS.ADMIN))

        # Grant '0' perms on the TagSet to the user, should retain ADMIN
        ArloPermission(
            bearer_object=new_admin_user,
            target_object=regular_tagset,
            createdBy=admin_user,
            read=False,
            write=False,
            launch_job=False,
            admin=False).save()

        self.assertTrue(regular_tagset.userHasPermissions(new_admin_user, ARLO_PERMS.READ | ARLO_PERMS.WRITE | ARLO_PERMS.LAUNCH_JOB | ARLO_PERMS.ADMIN))


    def test_tagset_admin_perms(self):
        """
        Admin perms should implicitly grant ALL perms.
        """

        # Get Objects (loaded by fixture)
        admin_user = User.objects.get(pk=1)
        regular_tagset = TagSet.objects.get(pk=2)

        new_admin_user = User(username='newadmin')
        new_admin_user.save()

        # Grant Admin to the user on the Project
        ArloPermission(
            bearer_object=new_admin_user,
            target_object=regular_tagset,
            createdBy=admin_user,
            read=False,
            write=False,
            launch_job=False,
            admin=True).save()

        self.assertTrue(regular_tagset.userHasPermissions(new_admin_user, ARLO_PERMS.READ | ARLO_PERMS.WRITE | ARLO_PERMS.LAUNCH_JOB | ARLO_PERMS.ADMIN))
