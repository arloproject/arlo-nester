from django.test import TestCase
from django.contrib.auth.models import User
from django.core.exceptions import ObjectDoesNotExist

from tools.models import Library, ArloPermission, ArloUserGroup, Project, ProjectTypes, TagSet
from tools.views.userPermissions import ARLO_PERMS


class ArloUserGroupTest(TestCase):

    def test_arlousergroup_admin_user_permissions(self):
        """
        Add user as Admin to an ArloUserGroup. Verify has Admin privs
        to the Group but not privs to the Group's targets.
        """

        # Add users
        user_owner = User(username="Test User Owner")
        user_owner.save()
        group_admin_user = User(username="Test Group Admin User")
        group_admin_user.save()

        # Create Group
        group = ArloUserGroup(name="TestGroup", createdBy=user_owner)
        group.save()

        # Add admin
        ArloPermission(
            bearer_object=group_admin_user,
            target_object=group,
            createdBy=user_owner,
            read=False,
            write=False,
            launch_job=False,
            admin=True).save()



        ###
        # Add this Group to other targets, and make sure the Admin user
        # doesn't receive any privileges to that target.

        # Library
        library1 = Library(user=user_owner, name='Test Library 1')
        library1.save()
        ArloPermission(
            bearer_object=group,
            target_object=library1,
            createdBy=user_owner,
            read=True,
            write=True,
            launch_job=True,
            admin=True).save()
        self.assertFalse(library1.userHasPermissions(group_admin_user, ARLO_PERMS.READ))
        self.assertFalse(library1.userHasPermissions(group_admin_user, ARLO_PERMS.WRITE))
        self.assertFalse(library1.userHasPermissions(group_admin_user, ARLO_PERMS.LAUNCH_JOB))
        self.assertFalse(library1.userHasPermissions(group_admin_user, ARLO_PERMS.ADMIN))

        # Project
        # using --keepdb doesn't seem to persist data from migrations
        try:
            projectType = ProjectTypes.objects.get(pk=1)
        except ObjectDoesNotExist:
            projectType = ProjectTypes(name='audio')
            projectType.save()
        project1 = Project(user=user_owner, name='Test Project 1', type=projectType)
        project1.save()
        ArloPermission(
            bearer_object=group,
            target_object=project1,
            createdBy=user_owner,
            read=True,
            write=True,
            launch_job=True,
            admin=True).save()
        self.assertFalse(project1.userHasPermissions(group_admin_user, ARLO_PERMS.READ))
        self.assertFalse(project1.userHasPermissions(group_admin_user, ARLO_PERMS.WRITE))
        self.assertFalse(project1.userHasPermissions(group_admin_user, ARLO_PERMS.LAUNCH_JOB))
        self.assertFalse(project1.userHasPermissions(group_admin_user, ARLO_PERMS.ADMIN))

        # TagSet
        tagSet1 = TagSet(user=user_owner, name='Test TagSet 1', project=project1)
        tagSet1.save()
        ArloPermission(
            bearer_object=group,
            target_object=tagSet1,
            createdBy=user_owner,
            read=True,
            write=True,
            launch_job=True,
            admin=True).save()
        self.assertFalse(tagSet1.userHasPermissions(group_admin_user, ARLO_PERMS.READ))
        self.assertFalse(tagSet1.userHasPermissions(group_admin_user, ARLO_PERMS.WRITE))
        self.assertFalse(tagSet1.userHasPermissions(group_admin_user, ARLO_PERMS.LAUNCH_JOB))
        self.assertFalse(tagSet1.userHasPermissions(group_admin_user, ARLO_PERMS.ADMIN))


    def test_arlousergroup_admin_group_permissions(self):
        """
        Add an ArloUserGroup as Admin to another ArloUserGroup. Verify
        the bearer Group has Admin privs to the target Group but not
        privs to the target Group's targets.
        """

        # Add users
        user_owner = User(username="Test User Owner")
        user_owner.save()
        admin_group_user = User(username="Test Admin Group User")
        admin_group_user.save()
        admin_group = ArloUserGroup(name="AdminGroup", createdBy=user_owner)
        admin_group.save()
        admin_group.users.add(admin_group_user)

        # Create Target Group
        group = ArloUserGroup(name="TestGroup", createdBy=user_owner)
        group.save()

        # Add admin
        ArloPermission(
            bearer_object=admin_group,
            target_object=group,
            createdBy=user_owner,
            read=False,
            write=False,
            launch_job=False,
            admin=True).save()


        self.assertTrue(group.userHasPermissions(admin_group_user, ARLO_PERMS.ADMIN))


        ###
        # Add this Group to other targets, and make sure the Admin user
        # doesn't receive any privileges to that target.

        # Library
        library1 = Library(user=user_owner, name='Test Library 1')
        library1.save()
        ArloPermission(
            bearer_object=group,
            target_object=library1,
            createdBy=user_owner,
            read=True,
            write=True,
            launch_job=True,
            admin=True).save()
        self.assertFalse(library1.userHasPermissions(admin_group_user, ARLO_PERMS.READ))
        self.assertFalse(library1.userHasPermissions(admin_group_user, ARLO_PERMS.WRITE))
        self.assertFalse(library1.userHasPermissions(admin_group_user, ARLO_PERMS.LAUNCH_JOB))
        self.assertFalse(library1.userHasPermissions(admin_group_user, ARLO_PERMS.ADMIN))

        # Project
        # using --keepdb doesn't seem to persist data from migrations
        try:
            projectType = ProjectTypes.objects.get(pk=1)
        except ObjectDoesNotExist:
            projectType = ProjectTypes(name='audio')
            projectType.save()
        project1 = Project(user=user_owner, name='Test Project 1', type=projectType)
        project1.save()
        ArloPermission(
            bearer_object=group,
            target_object=project1,
            createdBy=user_owner,
            read=True,
            write=True,
            launch_job=True,
            admin=True).save()
        self.assertFalse(project1.userHasPermissions(admin_group_user, ARLO_PERMS.READ))
        self.assertFalse(project1.userHasPermissions(admin_group_user, ARLO_PERMS.WRITE))
        self.assertFalse(project1.userHasPermissions(admin_group_user, ARLO_PERMS.LAUNCH_JOB))
        self.assertFalse(project1.userHasPermissions(admin_group_user, ARLO_PERMS.ADMIN))

        # TagSet
        tagSet1 = TagSet(user=user_owner, name='Test TagSet 1', project=project1)
        tagSet1.save()
        ArloPermission(
            bearer_object=group,
            target_object=tagSet1,
            createdBy=user_owner,
            read=True,
            write=True,
            launch_job=True,
            admin=True).save()
        self.assertFalse(tagSet1.userHasPermissions(admin_group_user, ARLO_PERMS.READ))
        self.assertFalse(tagSet1.userHasPermissions(admin_group_user, ARLO_PERMS.WRITE))
        self.assertFalse(tagSet1.userHasPermissions(admin_group_user, ARLO_PERMS.LAUNCH_JOB))
        self.assertFalse(tagSet1.userHasPermissions(admin_group_user, ARLO_PERMS.ADMIN))


    def test_arlousergroup_getUserArloUserGroups(self):
        """
        Test that getUserArloUserGroups() returns the proper set of
        Groups that a user is in.
        """

        # Add users
        user_owner = User(username="Test User Owner")
        user_owner.save()
        user_group = User(username="Test User In Group")
        user_group.save()
        user_admin = User(username="Test User Admin")
        user_admin.save()
        user_group_admin = User(username="Test User Group Admin")
        user_group_admin.save()
        user_read = User(username="Test User Read")
        user_read.save()
        user_group_read = User(username="Test User Group Read")
        user_group_read.save()

        # Create the Group we're testing against
        group = ArloUserGroup(name="Group", createdBy=user_owner)
        group.save()

        # Create another Group that we won't add users to
        other_group = ArloUserGroup(name="OtherGroup", createdBy=user_owner)
        other_group.save()

        # getUserArloUserGroups returns a sorted list, so we can use subscripts

        # owner should have all
        self.assertEqual(ArloUserGroup.objects.getUserArloUserGroups(user_owner).count(), 2)
        self.assertEqual(ArloUserGroup.objects.getUserArloUserGroups(user_owner)[0], group)
        self.assertEqual(ArloUserGroup.objects.getUserArloUserGroups(user_owner)[1], other_group)

        # add a user to the group
        self.assertEqual(ArloUserGroup.objects.getUserArloUserGroups(user_group).count(), 0)
        group.users.add(user_group)
        group.save()
        self.assertEqual(ArloUserGroup.objects.getUserArloUserGroups(user_group).count(), 1)
        self.assertEqual(ArloUserGroup.objects.getUserArloUserGroups(user_group)[0], group)

        # Add a user as an Admin
        self.assertEqual(ArloUserGroup.objects.getUserArloUserGroups(user_admin).count(), 0)
        ArloPermission(
            bearer_object=user_admin,
            target_object=group,
            createdBy=user_owner,
            read=False,
            write=False,
            launch_job=False,
            admin=True).save()
        self.assertEqual(ArloUserGroup.objects.getUserArloUserGroups(user_admin).count(), 1)
        self.assertEqual(ArloUserGroup.objects.getUserArloUserGroups(user_admin)[0], group)

        # Add a user to a Group that has Admin
        self.assertEqual(ArloUserGroup.objects.getUserArloUserGroups(user_group_admin).count(), 0)
        admin_group = ArloUserGroup(name="zAdminGroup", createdBy=user_owner)
        admin_group.save()
        admin_group.users.add(user_group_admin)
        admin_group.save()
        self.assertEqual(ArloUserGroup.objects.getUserArloUserGroups(user_group_admin).count(), 1)
        self.assertEqual(ArloUserGroup.objects.getUserArloUserGroups(user_group_admin)[0], admin_group)
        ArloPermission(
            bearer_object=admin_group,
            target_object=group,
            createdBy=user_owner,
            read=False,
            write=False,
            launch_job=False,
            admin=True).save()
        self.assertEqual(ArloUserGroup.objects.getUserArloUserGroups(user_group_admin).count(), 2)
        self.assertEqual(ArloUserGroup.objects.getUserArloUserGroups(user_group_admin)[0], group)
        self.assertEqual(ArloUserGroup.objects.getUserArloUserGroups(user_group_admin)[1], admin_group)

        # Add a user with Read access
        self.assertEqual(ArloUserGroup.objects.getUserArloUserGroups(user_read).count(), 0)
        ArloPermission(
            bearer_object=user_read,
            target_object=group,
            createdBy=user_owner,
            read=True,
            write=False,
            launch_job=False,
            admin=False).save()
        self.assertEqual(ArloUserGroup.objects.getUserArloUserGroups(user_read).count(), 1)
        self.assertEqual(ArloUserGroup.objects.getUserArloUserGroups(user_read)[0], group)

        # Add a user to a Group that has Read
        self.assertEqual(ArloUserGroup.objects.getUserArloUserGroups(user_group_read).count(), 0)
        read_group = ArloUserGroup(name="zReadGroup", createdBy=user_owner)
        read_group.save()
        read_group.users.add(user_group_read)
        read_group.save()
        self.assertEqual(ArloUserGroup.objects.getUserArloUserGroups(user_group_read).count(), 1)
        self.assertEqual(ArloUserGroup.objects.getUserArloUserGroups(user_group_read)[0], read_group)
        ArloPermission(
            bearer_object=read_group,
            target_object=group,
            createdBy=user_owner,
            read=False,
            write=False,
            launch_job=False,
            admin=True).save()
        self.assertEqual(ArloUserGroup.objects.getUserArloUserGroups(user_group_read).count(), 2)
        self.assertEqual(ArloUserGroup.objects.getUserArloUserGroups(user_group_read)[0], group)
        self.assertEqual(ArloUserGroup.objects.getUserArloUserGroups(user_group_read)[1], read_group)


    def test_arlousergroup_read_permissions(self):
        """
        Users should have read access to Groups in a variety of situations.
        """

        # Add users
        user_owner = User(username="TestUserOwner")
        user_owner.save()
        user = User(username="TestUser")
        user.save()


        # Test that we have READ to a group that the user is in.
        group = ArloUserGroup(name="TestGroup", createdBy=user_owner)
        group.save()
        self.assertFalse(group.userHasPermissions(user, ARLO_PERMS.READ))
        group.users.add(user)
        group.save()
        self.assertTrue(group.userHasPermissions(user, ARLO_PERMS.READ))

        # Test that we have READ to a group that the user has been
        # granted explicit Perms on.
        group2 = ArloUserGroup(name="Group2", createdBy=user_owner)
        group2.save()
        self.assertFalse(group2.userHasPermissions(user, ARLO_PERMS.READ))
        ArloPermission(
            bearer_object=user,
            target_object=group2,
            createdBy=user_owner,
            read=True,
            write=False,
            launch_job=False,
            admin=False).save()
        self.assertTrue(group2.userHasPermissions(user, ARLO_PERMS.READ))

        # Test that we have READ to a group that the user has been
        # granted Perms via another group.
        group3 = ArloUserGroup(name="Group3", createdBy=user_owner)
        group3.save()
        perm_group = ArloUserGroup(name="PermGroup", createdBy=user_owner)
        perm_group.save()
        perm_group.users.add(user)
        perm_group.save()
        self.assertFalse(group3.userHasPermissions(user, ARLO_PERMS.READ))
        ArloPermission(
            bearer_object=perm_group,
            target_object=group3,
            createdBy=user_owner,
            read=True,
            write=False,
            launch_job=False,
            admin=False).save()
        self.assertTrue(group3.userHasPermissions(user, ARLO_PERMS.READ))

