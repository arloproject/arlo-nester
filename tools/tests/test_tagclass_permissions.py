from django.test import TestCase
from django.contrib.auth.models import User

from tools.models import TagClass, ArloPermission, ArloUserGroup, Project
from tools.views.userPermissions import ARLO_PERMS

class ArloTagSetPermissionTest(TestCase):
    fixtures = ['tagset_test_data.yaml']

    def test_tagclass_owner_permissions(self):

        # Get Objects (loaded by fixture)
        admin_user = User.objects.get(pk=1)
        regular_user = User.objects.get(pk=2)
        admin_tagclass = TagClass.objects.get(pk=1)
        regular_tagclass = TagClass.objects.get(pk=2)

        # Check owners
        self.assertTrue(admin_tagclass.userHasPermissions(admin_user, ARLO_PERMS.READ | ARLO_PERMS.WRITE | ARLO_PERMS.ADMIN | ARLO_PERMS.LAUNCH_JOB), True)
        self.assertTrue(admin_tagclass.userHasPermissions(admin_user, ARLO_PERMS.READ | ARLO_PERMS.WRITE | ARLO_PERMS.ADMIN | ARLO_PERMS.LAUNCH_JOB), False)
        self.assertTrue(regular_tagclass.userHasPermissions(regular_user, ARLO_PERMS.READ | ARLO_PERMS.WRITE | ARLO_PERMS.ADMIN | ARLO_PERMS.LAUNCH_JOB), True)
        self.assertTrue(regular_tagclass.userHasPermissions(regular_user, ARLO_PERMS.READ | ARLO_PERMS.WRITE | ARLO_PERMS.ADMIN | ARLO_PERMS.LAUNCH_JOB), False)

        # Check admin access
        self.assertTrue(regular_tagclass.userHasPermissions(admin_user, ARLO_PERMS.READ | ARLO_PERMS.WRITE | ARLO_PERMS.ADMIN | ARLO_PERMS.LAUNCH_JOB), True)
        self.assertTrue(regular_tagclass.userHasPermissions(admin_user, ARLO_PERMS.READ | ARLO_PERMS.WRITE | ARLO_PERMS.ADMIN | ARLO_PERMS.LAUNCH_JOB), False)

        # another user should have no access
        self.assertFalse(admin_tagclass.userHasPermissions(regular_user, ARLO_PERMS.READ))
        self.assertFalse(admin_tagclass.userHasPermissions(regular_user, ARLO_PERMS.WRITE))
        self.assertFalse(admin_tagclass.userHasPermissions(regular_user, ARLO_PERMS.ADMIN))
        self.assertFalse(admin_tagclass.userHasPermissions(regular_user, ARLO_PERMS.LAUNCH_JOB))


    def test_tagclass_user_permissions(self):
        # Get Objects (loaded by fixture)
        admin_user = User.objects.get(pk=1)
        regular_project = Project.objects.get(pk=2)
        regular_tagclass = TagClass.objects.get(pk=2)

        USER_PERMS = (
            # (username, perms)
            ('user_r', ARLO_PERMS.READ),
            ('user_rw', ARLO_PERMS.READ | ARLO_PERMS.WRITE),
            ('user_rwx', ARLO_PERMS.READ | ARLO_PERMS.WRITE | ARLO_PERMS.LAUNCH_JOB),
            ('user_rwxa', ARLO_PERMS.READ | ARLO_PERMS.WRITE | ARLO_PERMS.LAUNCH_JOB | ARLO_PERMS.ADMIN),
            )

        # Add users and Permissions
        for username, perms in USER_PERMS:
            user = User(username=username)
            user.save()

            read = (perms & ARLO_PERMS.READ) > 0
            write = (perms & ARLO_PERMS.WRITE) > 0
            launch_job = (perms & ARLO_PERMS.LAUNCH_JOB) > 0
            admin = (perms & ARLO_PERMS.ADMIN) > 0

            ArloPermission(
                bearer_object=user,
                target_object=regular_project,
                createdBy=admin_user,
                read=read,
                write=write,
                launch_job=launch_job,
                admin=admin).save()

        # check permissions
        for username, perms in USER_PERMS:
            user = User.objects.get(username=username)

            read = (perms & ARLO_PERMS.READ) > 0
            write = (perms & ARLO_PERMS.WRITE) > 0
            launch_job = (perms & ARLO_PERMS.LAUNCH_JOB) > 0
            admin = (perms & ARLO_PERMS.ADMIN) > 0

            self.assertEqual(regular_tagclass.userHasPermissions(user, ARLO_PERMS.READ), read)
            self.assertEqual(regular_tagclass.userHasPermissions(user, ARLO_PERMS.WRITE), write)
            self.assertEqual(regular_tagclass.userHasPermissions(user, ARLO_PERMS.LAUNCH_JOB), launch_job)
            self.assertEqual(regular_tagclass.userHasPermissions(user, ARLO_PERMS.ADMIN), admin)


    def test_tagclass_group_permissions(self):
        # Get Objects (loaded by fixture)
        admin_user = User.objects.get(pk=1)
        regular_project = Project.objects.get(pk=2)
        regular_tagclass = TagClass.objects.get(pk=2)

        GROUP_PERMS = (
            # (groupname, username, perms)
            ('group_r', 'user_r', ARLO_PERMS.READ),
            ('group_rw', 'user_rw', ARLO_PERMS.READ | ARLO_PERMS.WRITE),
            ('group_rwx', 'user_rwx', ARLO_PERMS.READ | ARLO_PERMS.WRITE | ARLO_PERMS.LAUNCH_JOB),
            ('group_rwxa', 'user_rwxa', ARLO_PERMS.READ | ARLO_PERMS.WRITE | ARLO_PERMS.LAUNCH_JOB | ARLO_PERMS.ADMIN),
            )

        # Add users and Permissions
        for groupname, username, perms in GROUP_PERMS:
            user = User(username=username)
            user.save()

            group = ArloUserGroup(name=groupname, createdBy=admin_user)
            group.save()
            group.users.add(user)

            read = (perms & ARLO_PERMS.READ) > 0
            write = (perms & ARLO_PERMS.WRITE) > 0
            launch_job = (perms & ARLO_PERMS.LAUNCH_JOB) > 0
            admin = (perms & ARLO_PERMS.ADMIN) > 0

            ArloPermission(
                bearer_object=group,
                target_object=regular_project,
                createdBy=admin_user,
                read=read,
                write=write,
                launch_job=launch_job,
                admin=admin).save()

        # check permissions
        for groupname, username, perms in GROUP_PERMS:
            user = User.objects.get(username=username)

            read = (perms & ARLO_PERMS.READ) > 0
            write = (perms & ARLO_PERMS.WRITE) > 0
            launch_job = (perms & ARLO_PERMS.LAUNCH_JOB) > 0
            admin = (perms & ARLO_PERMS.ADMIN) > 0

            self.assertEqual(regular_tagclass.userHasPermissions(user, ARLO_PERMS.READ), read)
            self.assertEqual(regular_tagclass.userHasPermissions(user, ARLO_PERMS.WRITE), write)
            self.assertEqual(regular_tagclass.userHasPermissions(user, ARLO_PERMS.LAUNCH_JOB), launch_job)
            self.assertEqual(regular_tagclass.userHasPermissions(user, ARLO_PERMS.ADMIN), admin)


        ###
        # Create separate groups for each permission, add one user to all
        # groups, and ensure the user has ALL permissions (union of all perms)

        SEP_USER_NAME = 'user_sep'
        SEP_GROUP_PERMS = (
            # (groupname, perms)
            ('sep_group_r', ARLO_PERMS.READ),
            ('sep_group_w', ARLO_PERMS.WRITE),
            ('sep_group_x', ARLO_PERMS.LAUNCH_JOB),
            )

        # Add user and Permissions
        user = User(username=SEP_USER_NAME)
        user.save()

        for groupname, perms in SEP_GROUP_PERMS:
            group = ArloUserGroup(name=groupname, createdBy=admin_user)
            group.save()
            group.users.add(user)

            read = (perms & ARLO_PERMS.READ) > 0
            write = (perms & ARLO_PERMS.WRITE) > 0
            launch_job = (perms & ARLO_PERMS.LAUNCH_JOB) > 0
            admin = (perms & ARLO_PERMS.ADMIN) > 0

            ArloPermission(
                bearer_object=group,
                target_object=regular_project,
                createdBy=admin_user,
                read=read,
                write=write,
                launch_job=launch_job,
                admin=admin).save()

        self.assertTrue(regular_tagclass.userHasPermissions(user, ARLO_PERMS.READ))
        self.assertTrue(regular_tagclass.userHasPermissions(user, ARLO_PERMS.WRITE))
        self.assertTrue(regular_tagclass.userHasPermissions(user, ARLO_PERMS.LAUNCH_JOB))


    ## Test the special Default permissions group. All users with no other
    # permissions should inherit these permissions.

    def test_tagclass_default_permissions_on_tagset(self):
        """
        Check Default Permissions when applied on TagSet
        """

         # Get Objects (loaded by fixture)
        admin_user = User.objects.get(pk=1)
        regular_project = Project.objects.get(pk=2)
        regular_tagclass = TagClass.objects.get(pk=2)

        regular_project.default_permission_read = True
        regular_project.default_permission_write = True
        regular_project.default_permission_launch_job = True
        regular_project.default_permission_admin = True
        regular_project.save()

        user_default = User(username="Test User Defaults")
        user_default.save()

        self.assertTrue(regular_tagclass.userHasPermissions(user_default, ARLO_PERMS.READ | ARLO_PERMS.WRITE | ARLO_PERMS.ADMIN | ARLO_PERMS.LAUNCH_JOB))


    def test_tagclass_staff_override_permissions(self):
        """
        Test system admin ('staff') user overrides.
        """

        # Get Objects (loaded by fixture)
        admin_user = User.objects.get(pk=1)
        regular_project = Project.objects.get(pk=2)
        regular_tagclass = TagClass.objects.get(pk=2)

        # add '0' perms for in user/group perms - these should be ignored

        ArloPermission(
            bearer_object=admin_user,
            target_object=regular_project,
            createdBy=admin_user,
            read=False,
            write=False,
            launch_job=False,
            admin=False).save()

        self.assertTrue(regular_tagclass.userHasPermissions(admin_user, ARLO_PERMS.READ | ARLO_PERMS.WRITE | ARLO_PERMS.ADMIN | ARLO_PERMS.LAUNCH_JOB))
        # ensure admin override will turn off
        self.assertFalse(regular_tagclass.userHasPermissions(admin_user, ARLO_PERMS.READ, False))
        self.assertFalse(regular_tagclass.userHasPermissions(admin_user, ARLO_PERMS.WRITE, False))
        self.assertFalse(regular_tagclass.userHasPermissions(admin_user, ARLO_PERMS.LAUNCH_JOB, False))
        self.assertFalse(regular_tagclass.userHasPermissions(admin_user, ARLO_PERMS.ADMIN, False))

