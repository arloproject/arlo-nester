from django.test import TestCase

from tools.models import Library
from tools.views.userMetaData import getUserMetaDataField


class ArloUserMetaDataTest(TestCase):
    fixtures = ['mediafileusermetadata_test_data.yaml']

    def test_usermetadata_getUserMetaDataField(self):

        # Get Objects (loaded by fixture)
        library_one = Library.objects.get(pk=1)
        library_two = Library.objects.get(pk=2)

        # Get existing field
        field = getUserMetaDataField('testFieldLib1', library_one, False)
        self.assertEqual(field.id, 1)

        # should NOT create new field
        field = getUserMetaDataField('testFieldLib1', library_two, False)
        self.assertEqual(field, None)

        # should create new field
        field = getUserMetaDataField('testFieldLib1', library_two, True)
        self.assertTrue(field.id > 1)
