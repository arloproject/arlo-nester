from django.test import TestCase
from django.contrib.auth.models import User

from tools.models import Library, ArloPermission, ArloUserGroup, Library, Project, TagSet
from tools.views.userPermissions import ARLO_PERMS, ArloPermissionPermissions


class ArloPermissionTest(TestCase):
    """
    Runs tests against the ArloPermission model permissions.
    """
    fixtures = ['permissions_test_data.yaml']

    def test_admin_permissions(self):
        """
        Verify that a System Admin has access ADMIN to various permissions.
        """

        # Add users
        user_owner = User(username="Test User Owner")
        user_owner.save()
        user_bearer = User(username="Test Bearer")
        user_bearer.save()
        user_admin = User(username="Test Admin User", is_staff=True)
        user_admin.save()
        user_other = User(username="Other User")
        user_other.save()

        # Create Group
        group = ArloUserGroup(name="TestGroup", createdBy=user_owner)
        group.save()

        # Add a Permission to the Group for user_owner
        perm = ArloPermission(
            bearer_object=user_bearer,
            target_object=group,
            createdBy=user_owner,
            read=True,
            write=False,
            launch_job=False,
            admin=False)
        perm.save()

        # user_bearer should have READ only, since they are the bearer
        self.assertTrue(perm.userHasPermissions(user_bearer, ARLO_PERMS.READ))
        self.assertFalse(perm.userHasPermissions(user_bearer, ARLO_PERMS.WRITE))
        self.assertFalse(perm.userHasPermissions(user_bearer, ARLO_PERMS.LAUNCH_JOB))
        self.assertFalse(perm.userHasPermissions(user_bearer, ARLO_PERMS.ADMIN))

        # user_owner should have ADMIN, since they own the Group
        self.assertTrue(perm.userHasPermissions(user_owner, ARLO_PERMS.READ))
        self.assertTrue(perm.userHasPermissions(user_owner, ARLO_PERMS.WRITE))
        self.assertTrue(perm.userHasPermissions(user_owner, ARLO_PERMS.LAUNCH_JOB))
        self.assertTrue(perm.userHasPermissions(user_owner, ARLO_PERMS.ADMIN))

        # user_admin should also have ADMIN, even though they aren't associated
        self.assertTrue(perm.userHasPermissions(user_admin, ARLO_PERMS.READ))
        self.assertTrue(perm.userHasPermissions(user_admin, ARLO_PERMS.WRITE))
        self.assertTrue(perm.userHasPermissions(user_admin, ARLO_PERMS.LAUNCH_JOB))
        self.assertTrue(perm.userHasPermissions(user_admin, ARLO_PERMS.ADMIN))

        # user_other should have nothing
        self.assertFalse(perm.userHasPermissions(user_other, ARLO_PERMS.READ))
        self.assertFalse(perm.userHasPermissions(user_other, ARLO_PERMS.WRITE))
        self.assertFalse(perm.userHasPermissions(user_other, ARLO_PERMS.LAUNCH_JOB))
        self.assertFalse(perm.userHasPermissions(user_other, ARLO_PERMS.ADMIN))


    def test_throws_exception_on_invalid_type(self):
        # Should raise an Exception for another object type
        user_owner = User(username="Test User Owner")
        user_owner.save()
        library1 = Library(user=user_owner, name='Test Library 1')
        library1.save()

        self.assertRaises(TypeError, ArloPermissionPermissions, library1)


    def test_user_query(self):
        """
        Check that the getUserArloPermissions() returns the appropriate
        list of queries.
        Test this by adding perm_user to all regular objects,
        admin_grant_user to all admin projects.
        """

        # Get Objects (loaded by fixture)
        admin_user = User.objects.get(pk=1)
        regular_user = User.objects.get(pk=2)
        perm_user = User.objects.get(pk=3)
        admin_grant_user = User.objects.get(pk=4)
        group_user = User.objects.get(pk=5)
        admin_library = Library.objects.get(pk=1)
        user_library = Library.objects.get(pk=2)
        admin_project = Project.objects.get(pk=1)
        regular_project = Project.objects.get(pk=2)
        admin_tagset = TagSet.objects.get(pk=1)
        regular_tagset = TagSet.objects.get(pk=2)


        ###
        # Check perms

        # admin_user created these perms, so should see them
        arloPerms = ArloPermission.objects.getUserArloPermissions(admin_user)
        self.assertEqual(arloPerms.count(), 3)

        # perms are on regular_user's objects, so should see these
        arloPerms = ArloPermission.objects.getUserArloPermissions(regular_user)
        self.assertEqual(arloPerms.count(), 3)

        # check perm_user
        arloPerms = ArloPermission.objects.getUserArloPermissions(perm_user)
        self.assertEqual(arloPerms.count(), 3)

        # Shouldn't have any explicit perms
        arloPerms = ArloPermission.objects.getUserArloPermissions(admin_grant_user)
        self.assertEqual(arloPerms.count(), 0)

        # Shouldn't have any explicit perms
        arloPerms = ArloPermission.objects.getUserArloPermissions(group_user)
        self.assertEqual(arloPerms.count(), 0)

# TODO finish out perms for Grouop user and admin

