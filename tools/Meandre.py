import urllib
import logging
import re
import httplib
import json

from django.conf import settings


def filterHttpEscapes(urlString):
    logging.info("FilterIn: " + urlString)
    urlString = re.sub("\%20", " ", urlString)
    urlString = re.sub("\%21", "!", urlString)
    urlString = re.sub("\%22", "\"", urlString)
    urlString = re.sub("\%23", "#", urlString)
    urlString = re.sub("\%24", "$", urlString)
    # %25 last, see below
    urlString = re.sub("\%26", "&", urlString)
    urlString = re.sub("\%27", "'", urlString)
    urlString = re.sub("\%28", "(", urlString)
    urlString = re.sub("\%29", ")", urlString)
    urlString = re.sub("\%2[aA]", "*", urlString)
    urlString = re.sub("\%2[bB]", "+", urlString)
    urlString = re.sub("\%2[cC]", ",", urlString)
    urlString = re.sub("\%2[dD]", "-", urlString)
    urlString = re.sub("\%2[eE]", ".", urlString)
    urlString = re.sub("\%2[fF]", "/", urlString)

    urlString = re.sub("\%3[aA]", ":", urlString)
    urlString = re.sub("\%3[bB]", ";", urlString)
    urlString = re.sub("\%3[cC]", "<", urlString)
    urlString = re.sub("\%3[dD]", "=", urlString)
    urlString = re.sub("\%3[eE]", ">", urlString)
    urlString = re.sub("\%3[fF]", "?", urlString)

    urlString = re.sub("\%40", "@", urlString)

    urlString = re.sub("\%5[bB]", "[", urlString)
    # Note the special string handling here for the backslash
    # http://stackoverflow.com/questions/4427174/python-re-bogus-escape-error
    urlString = re.sub("\%5[cC]", r'\\', urlString)
    urlString = re.sub("\%5[dD]", "]", urlString)
    urlString = re.sub("\%5[eE]", "^", urlString)
    urlString = re.sub("\%5[fF]", "_", urlString)

    urlString = re.sub("\%60", "`", urlString)

    urlString = re.sub("\%7[bB]", "{", urlString)
    urlString = re.sub("\%7[cC]", "|", urlString)
    urlString = re.sub("\%7[dD]", "}", urlString)
    urlString = re.sub("\%7[eE]", "~", urlString)

    # do % last so that any '%xx' string exists in the unencoded string it
    # isn't mistakenly converted
    urlString = re.sub("\%25", "%", urlString)
    logging.info("FilterOut: " + urlString)
    return urlString

class MeandreAction:
  """
  This is an enumerated class defining the possible meandre calls
  """

  analyzeAudioFile = 'analyzeAudioFile'
  createSpectra = 'createSpectra'
  createWavSegment = 'createWavSegment'
  analyzeTags = 'analyzeTags'
  dumpTags = 'dumpTags'
  importAudioFiles = 'importAudioFiles'

  startTranscript = 'startTranscript'
  stopTranscript = 'stopTranscript'
  pauseTranscript = 'pauseTranscript'
  resumeTranscript = 'resumeTranscript'

  startADAPTAnalysis = 'startADAPTAnalysis'
  stopADAPTAnalysis = 'stopADAPTAnalysis'
  pauseADAPTAnalysis = 'pauseADAPTAnalysis'
  resumeADAPTAnalysis = 'resumeADAPTAnalysis'

  startTagAnalysis = 'startTagAnalysis'
  stopTagAnalysis = 'stopTagAnalysis'
  pauseTagAnalysis = 'pauseTagAnalysis'
  resumeTagAnalysis = 'resumeTagAnalysis'

  startSupervisedTagDiscovery = 'startSupervisedTagDiscovery'
  stopSupervisedTagDiscovery = 'stopSupervisedTagDiscovery'
  pauseSupervisedTagDiscovery = 'pauseSupervisedTagDiscovery'
  resumeSupervisedTagDiscovery = 'resumeSupervisedTagDiscovery'

  startUnsupervisedTagDiscovery = 'startUnsupervisedTagDiscovery'
  stopUnsupervisedTagDiscovery = 'stopUnsupervisedTagDiscovery'
  pauseUnsupervisedTagDiscovery = 'pauseUnsupervisedTagDiscovery'
  resumeUnsupervisedTagDiscovery = 'resumeUnsupervisedTagDiscovery'

  meandreTestCall = 'meandreTestCall'

def call(action, param_dict):
  """
  This function makes meandre calls
  """

#  logging.info("Meandre Call: action = " + action.__str__())

#  logging.info("Meandre Call: param_dict = " + param_dict.__str__())

  if action == MeandreAction.analyzeAudioFile:
    params = {
              'action' : action,
              'audioFileID' : str(param_dict['id']),
              }
    actionString = "Analyzing Audio File: " + str(param_dict['id'])

  elif action == MeandreAction.createWavSegment:
    params = {
              'action' : action,
              'audioFilePath' : param_dict['audioFilePath'].__str__(),
              'audioSegmentFilePath' : param_dict['audioSegmentFilePath'].__str__(),
              'startTime' : param_dict['startTime'].__str__(),
              'endTime' : param_dict['endTime'].__str__(),
              }
    actionString = 'Creating Wav Segment.'

  elif action == MeandreAction.createSpectra:
    params = {
              'action' : action,
              'userSettingsID' : param_dict['userSettingsID'].__str__(),
              'audioFilePath' : filterHttpEscapes(param_dict['audioFilePath'].__str__()),
              'imageFilePath' : filterHttpEscapes(param_dict['imageFilePath'].__str__()),
              'audioSegmentFilePath' : filterHttpEscapes(param_dict['audioSegmentFilePath'].__str__()),
              'startTime' : param_dict['startTime'].__str__(),
              'endTime' : param_dict['endTime'].__str__(),
              'gain' : param_dict['gain'].__str__(),
              'spectraNumFramesPerSecond' : param_dict['numFramesPerSecond'].__str__(),
              'spectraNumFrequencyBands' : param_dict['numFrequencyBands'].__str__(),
              'spectraDampingFactor' : param_dict['dampingFactor'].__str__(),
              'spectraMinimumBandFrequency' : param_dict['minFrequency'].__str__(),
              'spectraMaximumBandFrequency' : param_dict['maxFrequency'].__str__(),
              'spectraBorderWidth' : param_dict['borderWidth'].__str__(),
              'spectraTopBorderHeight' : param_dict['topBorderHeight'].__str__(),
              'timeScaleHeight' : param_dict['timeScaleHeight'].__str__(),
              'catalogSpectraNumFrequencyBands' : param_dict['numFrequencyBands'].__str__(),
              'spectraPitchTraceType': param_dict['spectraPitchTraceType'].__str__(),
              'spectraPitchTraceNumSamplePoints': param_dict['spectraPitchTraceNumSamplePoints'].__str__(),
              'spectraPitchTraceColor': param_dict['spectraPitchTraceColor'].__str__(),
              'spectraPitchTraceWidth': param_dict['spectraPitchTraceWidth'].__str__(),
              'spectraPitchTraceMinCorrelation': param_dict['spectraPitchTraceMinCorrelation'].__str__(),
              'spectraPitchTraceEntropyThreshold': param_dict['spectraPitchTraceEntropyThreshold'].__str__(),
              'spectraPitchTraceStartFreq': param_dict['spectraPitchTraceStartFreq'].__str__(),
              'spectraPitchTraceEndFreq': param_dict['spectraPitchTraceEndFreq'].__str__(),
              'spectraPitchTraceMinEnergyThreshold': param_dict['spectraPitchTraceMinEnergyThreshold'].__str__(),
              'spectraPitchTraceTolerance': param_dict['spectraPitchTraceTolerance'].__str__(),
              'spectraPitchTraceInverseFreqWeight': param_dict['spectraPitchTraceInverseFreqWeight'].__str__(),
              'spectraPitchTraceMaxPathLengthPerTransition': param_dict['spectraPitchTraceMaxPathLengthPerTransition'].__str__(),
              'spectraPitchTraceWindowSize': param_dict['spectraPitchTraceWindowSize'].__str__(),
              'spectraPitchTraceExtendRangeFactor': param_dict['spectraPitchTraceExtendRangeFactor'].__str__(),
              }
    if 'spectraSimpleNormalization' in param_dict:
      params['spectraSimpleNormalization'] = param_dict['simpleNormalization'].__str__()
    else:
      params['spectraSimpleNormalization'] = 'True'
    if 'showTimeScale' in param_dict:
      params['showTimeScale'] = param_dict['showTimeScale'].__str__()
    if 'showFrequencyScale' in param_dict:
      params['showFrequencyScale'] = param_dict['showFrequencyScale'].__str__()

    actionString = 'Creating Spectra.'

  elif action == MeandreAction.analyzeTags:
    params = {
              'action' : action,
              'username' : param_dict['username'].__str__(),
              }
    actionString = 'AnalyzeTags.'

  elif action == MeandreAction.dumpTags:
    params = {
              'action' : action,
              'username' : param_dict['username'].__str__(),
              }
    actionString = 'DumpingTags.'

  elif action == MeandreAction.importAudioFiles:
    params = {
              'action' : action,
              'username' : param_dict['username'].__str__(),
              }
    actionString = 'Importing Audio Files.'

  elif action == MeandreAction.startTagAnalysis:
    params = {
              'action' : action,
              'id' : str(param_dict['id']),
              }
    actionString = 'Starting Tag Analysis.'
  elif action == MeandreAction.stopTagAnalysis:
    params = {
              'action' : action,
              'id' : param_dict['id'].__str__(),
              }
    actionString = 'Stopping Tag Analysis.'
  elif action == MeandreAction.pauseTagAnalysis:
    params = {
              'action' : action,
              'id' : param_dict['id'].__str__(),
              }
    actionString = 'Pausing Tag Analysis.'
  elif action == MeandreAction.resumeTagAnalysis:
    params = {
              'action' : action,
              'id' : param_dict['id'].__str__(),
              }
    actionString = 'Resuming Tag Analysis.'


  elif action == MeandreAction.startADAPTAnalysis:
    params = {
              'action' : action,
              'id' : param_dict['id'].__str__(),
              }
    actionString = 'Starting ADAPT Analysis.'
  elif action == MeandreAction.stopADAPTAnalysis:
    params = {
              'action' : action,
              'id' : param_dict['id'].__str__(),
              }
    actionString = 'Stopping ADAPT Analysis.'
  elif action == MeandreAction.pauseADAPTAnalysis:
    params = {
              'action' : action,
              'id' : param_dict['id'].__str__(),
              }
    actionString = 'Pausing ADAPT Analysis.'
  elif action == MeandreAction.resumeADAPTAnalysis:
    params = {
              'action' : action,
              'id' : param_dict['id'].__str__(),
              }
    actionString = 'Resuming ADAPT Analysis.'

  elif action == MeandreAction.startSupervisedTagDiscovery:
    params = {
              'action' : action,
              'id' : param_dict['id'].__str__(),
              }
    actionString = 'Starting Supervised Tag Discovery.'
  elif action == MeandreAction.stopSupervisedTagDiscovery:
    params = {
              'action' : action,
              'id' : param_dict['id'].__str__(),
              }
    actionString = 'Stopping Supervised Tag Discovery.'
  elif action == MeandreAction.pauseSupervisedTagDiscovery:
    params = {
              'action' : action,
              'id' : param_dict['id'].__str__(),
              }
    actionString = 'Pausing Supervised Tag Discovery.'
  elif action == MeandreAction.resumeSupervisedTagDiscovery:
    params = {
              'action' : action,
              'id' : param_dict['id'].__str__(),
              }
    actionString = 'Resuming Supervised Tag Discovery.'

  elif action == MeandreAction.startUnsupervisedTagDiscovery:
    params = {
              'action' : action,
              'id' : param_dict['id'].__str__(),
              }
    actionString = 'Starting Unsupervised Tag Discovery.'
  elif action == MeandreAction.stopUnsupervisedTagDiscovery:
    params = {
              'action' : action,
              'id' : param_dict['id'].__str__(),
              }
    actionString = 'Stopping Unsupervised Tag Discovery.'
  elif action == MeandreAction.pauseUnsupervisedTagDiscovery:
    params = {
              'action' : action,
              'id' : param_dict['id'].__str__(),
              }
    actionString = 'Pausing Unsupervised Tag Discovery.'
  elif action == MeandreAction.resumeUnsupervisedTagDiscovery:
    params = {
              'action' : action,
              'id' : param_dict['id'].__str__(),
              }
    actionString = 'Resuming Unsupervised Tag Discovery.'

  elif action == MeandreAction.startTranscript:
    params = {
              'action' : action,
              'id' : param_dict['id'].__str__(),
              }
    actionString = 'Starting Transcript.'
  elif action == MeandreAction.stopTranscript:
    params = {
              'action' : action,
              'id' : param_dict['id'].__str__(),
              }
    actionString = 'Stopping Transcript.'
  elif action == MeandreAction.pauseTranscript:
    params = {
              'action' : action,
              'id' : param_dict['id'].__str__(),
              }
    actionString = 'Pausing Transcript.'
  elif action == MeandreAction.resumeTranscript:
    params = {
              'action' : action,
              'id' : param_dict['id'].__str__(),
              }
    actionString = 'Resuming Transcript.'
  elif action == MeandreAction.meandreTestCall:
    params = param_dict
    params['action'] = action

    actionString = 'meandreTestCall.'

  else:
    return (False, 'Incorrect Action!')

  logging.info("Meandre Call params: " + params.__str__())

  try:

    ###
    # call adapt via the HTTP API

    ###                      ###
    #      JYTHON NOTICE       #
    ###                      ###

    # Jython's httplib absolutely sucks (at least from my testing).
    # requests (at conn.request()) are taking several seconds before
    # a connection is even established to the server. It seems that this
    # works fine in Python at least.

    encodedParams = urllib.urlencode(params)

    headers = {"Content-type": "application/x-www-form-urlencoded",
               "Accept": "application/json"}

#    f = urllib.urlopen("http://0.0.0.0:8080/callMe/", sReqParams)
    conn = httplib.HTTPConnection(settings.ADAPT_HOST + ":" + settings.ADAPT_PORT)
    conn.request("POST", settings.ADAPT_PATH, encodedParams, headers)
    httpResp = conn.getresponse()

    if httpResp.status != 200:
      logging.error("NOT 200 !!!")
      return (False, 'Error in Meandre Call - Not 200 Status Code')

    sResp = httpResp.read()
    conn.close()

    response = json.loads( sResp )

    # log the results
    s = "Meandre Call Response: \n"
    for k in response:
      s += "  - " + k + " : " + repr(response[k]) + "\n"
    logging.debug(s)

    return (True, actionString)

  except:
    logging.exception("Finished Meandre Call - Error - Exception Encountered")
    return (False, 'Unknown Error in Meandre Call')



class AdaptInterface:

  ### Get Raw Audio Spectra Data for a MediaFile from the ADAPT API
  #
  # @param mediaFile MediaFile object from which to compute the Spectra.
  # @param startTime
  # @param endTime
  # @param numTimeFramesPerSecond
  # @param numFrequencyBands
  # @param dampingRatio
  # @param minFrequency
  # @param maxFrequency
  # @return The 'spectraData' object returned by Adapt

  def GetAudioSpectraData(self, mediaFile, startTime, endTime, numTimeFramesPerSecond, numFrequencyBands, dampingRatio, minFrequency, maxFrequency):
    params = {
              'mediaFileId': mediaFile.id.__str__(),
              'startTime': startTime.__str__(),
              'endTime': endTime.__str__(),
              'numTimeFramesPerSecond': numTimeFramesPerSecond.__str__(),
              'numFrequencyBands': numFrequencyBands.__str__(),
              'dampingRatio': dampingRatio.__str__(),
              'minFrequency': minFrequency.__str__(),
              'maxFrequency': maxFrequency.__str__(),
             }

    response = self.AdaptCall('getAudioSpectraData', params)
    if not response:
      return None

    return {'spectraData': response['spectraData']}

  ### Get Audio PitchTrace Data for a MediaFile from the ADAPT API
  #
  # @param mediaFile MediaFile object from which to compute the Spectra.
  # @param pitchTraceType One of "MAX_ENERGY" or "FUNDAMENTAL"
  # @param startTime
  # @param endTime
  # @param numTimeFramesPerSecond
  # @param numFrequencyBands
  # @param dampingRatio
  # @param spectraMinFrequency
  # @param spectraMaxFrequency
  # FUNDAMENTAL SETTINGS
  # @param numSamplePoints
  # @param minCorrelation
  # @param entropyThreshold
  # @param pitchTraceStartFreq
  # @param pitchTraceEndFreq
  # @param minEnergyThreshold
  # @param tolerance
  # @param inverseFreqWeight
  # @param maxPathLengthPerTransition
  # @param windowSize
  # @param extendRangeFactor

  # @return The 'spectraData' object returned by Adapt


  def GetAudioPitchTraceData(self, mediaFile, pitchTraceType, startTime, endTime, numTimeFramesPerSecond, numFrequencyBands, dampingRatio, spectraMinFrequency, spectraMaxFrequency, numSamplePoints, minCorrelation, entropyThreshold, pitchTraceStartFreq, pitchTraceEndFreq, minEnergyThreshold, tolerance, inverseFreqWeight, maxPathLengthPerTransition, windowSize, extendRangeFactor):

    params = {
              'mediaFileId': mediaFile.id.__str__(),
              'pitchTraceType': pitchTraceType,
              'startTime': startTime.__str__(),
              'endTime': endTime.__str__(),
              'numTimeFramesPerSecond': numTimeFramesPerSecond.__str__(),
              'numFrequencyBands': numFrequencyBands.__str__(),
              'dampingRatio': dampingRatio.__str__(),
              'spectraMinFrequency': spectraMinFrequency.__str__(),
              'spectraMaxFrequency': spectraMaxFrequency.__str__(),
              'pitchTraceStartFreq': pitchTraceStartFreq.__str__(),
              'pitchTraceEndFreq': pitchTraceEndFreq.__str__(),
             }

    # Only for FUNDAMENTAL, so may not always exist

    if numSamplePoints is not None:
      params['numSamplePoints'] = numSamplePoints
    if minCorrelation is not None:
      params['minCorrelation'] = minCorrelation
    if entropyThreshold is not None:
      params['entropyThreshold'] = entropyThreshold
    if pitchTraceStartFreq is not None:
      params['pitchTraceStartFreq'] = pitchTraceStartFreq
    if pitchTraceEndFreq is not None:
      params['pitchTraceEndFreq'] = pitchTraceEndFreq
    if minEnergyThreshold is not None:
      params['minEnergyThreshold'] = minEnergyThreshold
    if tolerance is not None:
      params['tolerance'] = tolerance
    if inverseFreqWeight is not None:
      params['inverseFreqWeight'] = inverseFreqWeight
    if maxPathLengthPerTransition is not None:
      params['maxPathLengthPerTransition'] = maxPathLengthPerTransition
    if windowSize is not None:
      params['windowSize'] = windowSize
    if extendRangeFactor is not None:
      params['extendRangeFactor'] = extendRangeFactor

    response = self.AdaptCall('getAudioPitchTraceData', params)
    if not response:
      return None

    return {'pitchTraceData': response['pitchTraceData']}


  ### Make a call to the Java Adapt Service (callMe).
  #
  # @param action The 'action' name of the API we are calling
  # @param params Dictionary of the parameters for the API call.
  # @return None on error, else JSON decoded Object from API call.

  def AdaptCall(self, action, params):

    logging.debug("AdaptCall():\n      action: {}\n      params:{}".format(action, repr(params)))

    try:
      params['action'] = action
      encodedParams = urllib.urlencode(params)

      headers = {"Content-type": "application/x-www-form-urlencoded",
                 "Accept": "application/json"}

      conn = httplib.HTTPConnection(settings.ADAPT_HOST + ":" + settings.ADAPT_PORT)
      conn.request("POST", settings.ADAPT_PATH, encodedParams, headers)
      httpResp = conn.getresponse()

      if httpResp.status != 200:
        logging.error("AdaptCall() - Non 200 Response (" + str(httpResp.status) + ") - " + str(httpResp.read()))
        return None

      sResp = httpResp.read()
      conn.close()

      response = json.loads(sResp)

      return response

    except Exception as e:
      logging.exception("Finished Adapt Call - Error - Exception Encountered")
      return None
