from django.contrib import admin
from tools.models import (ArloPermission, ArloUserGroup, ProjectTypes, Library,
    SensorArray, Sensor, MediaFile, MediaFileMetaData, MediaFileUserMetaDataField,
    MediaFileUserMetaData, Project, TagClass, TagSet, Tag, JobTypes, JobStatusTypes,
    Jobs, JobParameters, JobLog, JobResultFile, RandomWindow, UserSettings,
    ExtendedUserSettings, ExtendedUserSettingsValues, ProjectPermissions
)


@admin.register(ArloPermission)
class ArloPermissionAdmin(admin.ModelAdmin):
  pass

@admin.register(ArloUserGroup)
class ArloUserGroupAdmin(admin.ModelAdmin):
  pass

@admin.register(ProjectTypes)
class ProjectTypesAdmin(admin.ModelAdmin):
  pass

@admin.register(Library)
class LibraryAdmin(admin.ModelAdmin):
  pass

@admin.register(SensorArray)
class SensorArrayAdmin(admin.ModelAdmin):
  pass

@admin.register(Sensor)
class SensorAdmin(admin.ModelAdmin):
  pass

@admin.register(MediaFile)
class MediaFileAdmin(admin.ModelAdmin):
  pass

@admin.register(MediaFileMetaData)
class MediaFileMetaDataAdmin(admin.ModelAdmin):
  pass

@admin.register(MediaFileUserMetaDataField)
class MediaFileUserMetaDataFieldAdmin(admin.ModelAdmin):
  pass

@admin.register(MediaFileUserMetaData)
class MediaFileUserMetaDataAdmin(admin.ModelAdmin):
  pass

@admin.register(Project)
class ProjectAdmin(admin.ModelAdmin):
  pass

@admin.register(TagClass)
class TagClassAdmin(admin.ModelAdmin):
  pass

@admin.register(TagSet)
class TagSetAdmin(admin.ModelAdmin):
  pass

@admin.register(Tag)
class TagAdmin(admin.ModelAdmin):
  pass

@admin.register(JobTypes)
class JobTypesAdmin(admin.ModelAdmin):
  pass

@admin.register(JobStatusTypes)
class JobStatusTypesAdmin(admin.ModelAdmin):
  pass

@admin.register(Jobs)
class JobsAdmin(admin.ModelAdmin):
  pass

@admin.register(JobParameters)
class JobParametersAdmin(admin.ModelAdmin):
  pass

@admin.register(JobLog)
class JobLogAdmin(admin.ModelAdmin):
  pass

@admin.register(JobResultFile)
class JobResultFileAdmin(admin.ModelAdmin):
  pass

@admin.register(RandomWindow)
class RandomWindowAdmin(admin.ModelAdmin):
  pass

@admin.register(UserSettings)
class UserSettingsAdmin(admin.ModelAdmin):
  pass

@admin.register(ExtendedUserSettings)
class ExtendedUserSettingsAdmin(admin.ModelAdmin):
  pass

@admin.register(ExtendedUserSettingsValues)
class ExtendedUserSettingsValuesAdmin(admin.ModelAdmin):
  pass

@admin.register(ProjectPermissions)
class ProjectPermissionsAdmin(admin.ModelAdmin):
  pass
