# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
from django.conf import settings


class Migration(migrations.Migration):

    dependencies = [
        ('tools', '0016_assignedJobStatus'),
    ]

    operations = [
        migrations.AlterModelOptions(
            name='arlopermission',
            options={'verbose_name': 'ARLO Permission'},
        ),
        migrations.AlterModelOptions(
            name='arlousergroup',
            options={'verbose_name': 'ARLO UserGroup'},
        ),
        migrations.AlterModelOptions(
            name='extendedusersettings',
            options={'verbose_name': 'Extended UserSetting', 'verbose_name_plural': 'Extended UserSettings'},
        ),
        migrations.AlterModelOptions(
            name='extendedusersettingsvalues',
            options={'verbose_name': 'Extended UserSetting Value', 'verbose_name_plural': 'Extended UserSetting Values'},
        ),
        migrations.AlterModelOptions(
            name='joblog',
            options={'verbose_name': 'Job Log'},
        ),
        migrations.AlterModelOptions(
            name='jobparameters',
            options={'verbose_name': 'Job Parameter'},
        ),
        migrations.AlterModelOptions(
            name='jobresultfile',
            options={'verbose_name': 'Job Result File'},
        ),
        migrations.AlterModelOptions(
            name='jobs',
            options={'verbose_name': 'Job'},
        ),
        migrations.AlterModelOptions(
            name='jobstatustypes',
            options={'verbose_name': 'Job Status Type'},
        ),
        migrations.AlterModelOptions(
            name='jobtypes',
            options={'verbose_name': 'Job Type'},
        ),
        migrations.AlterModelOptions(
            name='library',
            options={'verbose_name_plural': 'Libraries'},
        ),
        migrations.AlterModelOptions(
            name='mediafile',
            options={'verbose_name': 'MediaFile'},
        ),
        migrations.AlterModelOptions(
            name='mediafilemetadata',
            options={'verbose_name': 'MediaFile MetaData'},
        ),
        migrations.AlterModelOptions(
            name='mediafileusermetadata',
            options={'verbose_name': 'MediaFile UserMetaData'},
        ),
        migrations.AlterModelOptions(
            name='mediafileusermetadatafield',
            options={'verbose_name': 'MediaFile UserMetaData Field'},
        ),
        migrations.AlterModelOptions(
            name='projecttypes',
            options={'verbose_name': 'Project Type'},
        ),
        migrations.AlterModelOptions(
            name='randomwindow',
            options={'verbose_name': 'RandomWindow'},
        ),
        migrations.AlterModelOptions(
            name='sensorarray',
            options={'verbose_name': 'Sensor Array', 'verbose_name_plural': 'Sensor Arrays'},
        ),
        migrations.AlterModelOptions(
            name='tagclass',
            options={'verbose_name': 'TagClass', 'verbose_name_plural': 'TagClasses'},
        ),
        migrations.AlterModelOptions(
            name='tagset',
            options={'verbose_name': 'TagSet'},
        ),
        migrations.AlterModelOptions(
            name='usersettings',
            options={'verbose_name': 'UserSettings', 'verbose_name_plural': 'UserSettings'},
        ),
        migrations.RemoveField(
            model_name='jobresultfile',
            name='uploadAuthorizationNonce',
        ),
        migrations.AlterField(
            model_name='arlousergroup',
            name='users',
            field=models.ManyToManyField(default=list, to=settings.AUTH_USER_MODEL, blank=True),
        ),
    ]
