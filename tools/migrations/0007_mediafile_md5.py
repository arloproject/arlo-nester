# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('tools', '0006_remove_mediafile_hastranscript'),
    ]

    operations = [
        migrations.AddField(
            model_name='mediafile',
            name='md5',
            field=models.CharField(default=None, max_length=32, null=True, verbose_name=b'MD5 File Hash', blank=True),
        ),
    ]
