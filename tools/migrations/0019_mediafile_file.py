# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
import nesterSettings

def migrateMediaFileField(apps, schema_editor):
    MediaFile = apps.get_model("tools", "MediaFile")
    for m in MediaFile.objects.all():
        m.file.name = nesterSettings.getMediaFilePath(m.library, m.relativeFilePath)
        m.save()

class Migration(migrations.Migration):

    dependencies = [
        ('tools', '0018_tag_index'),
    ]

    operations = [
        migrations.AddField(
            model_name='mediafile',
            name='file',
            field=models.FileField(max_length=1024, upload_to=nesterSettings.getMediaFilePath),
        ),

        migrations.RunPython(migrateMediaFileField),

        migrations.RemoveField(
            model_name='mediafile',
            name='relativeFilePath',
        ),
    ]
