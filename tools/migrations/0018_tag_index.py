# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('tools', '0017_remove_nonce'),
    ]

    operations = [
        migrations.AlterIndexTogether(
            name='tag',
            index_together=set([('tagSet', 'tagClass')]),
        ),
    ]
