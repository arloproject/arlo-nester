# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class SensorArrayMigrationException(Exception):
    def __init__(self, message):
        super(SensorArrayMigrationException, self).__init__(message)
    def __str__(self):
        return repr(self.message)


def moveSensorArrays(apps, schema_editor):
    SensorArray = apps.get_model("tools", "SensorArray")
    Library = apps.get_model("tools", "Library")

    # Loop over each existing SensorArray, and see if it's use is wholly
    # contained within one Library. Move to that library.
    # If not, we'll need to raise an Exception
    for sa in SensorArray.objects.all():
        libs = Library.objects.filter(mediafile__sensor__in=sa.sensor_set.values_list('id', flat=True)).distinct()
        if len(libs) > 1:
            raise SensorArrayMigrationException("SensorArray {} ({}) is used in multiple Libraries. You will need to clone this and create a unique instance per Library.".format(sa.name, sa.id))
        if len(libs) < 1:
            raise SensorArrayMigrationException("SensorArray {} ({}) is not used in any Libraries. You will need to manually assign or delete this SensorArray.".format(sa.name, sa.id))
        sa.library = libs[0]
        sa.save()


class Migration(migrations.Migration):

    dependencies = [
        ('tools', '0011_merge_tags'),
    ]

    operations = [
        migrations.AddField(
            model_name='sensorarray',
            name='library',
            field=models.ForeignKey(default=None, null=True, to='tools.Library'),
            preserve_default=False,
        ),

        migrations.RunPython(moveSensorArrays),

        migrations.AlterField(
            model_name='sensorarray',
            name='library',
            field=models.ForeignKey(to='tools.Library'),
        ),

    ]
