# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
import datetime
import tools.models
from django.conf import settings
from django.core.exceptions import ObjectDoesNotExist

import nesterSettings


def addInitialJobTypes(apps, schema_editor):
    JobTypes = apps.get_model("tools", "JobTypes")

    # id 13 (WekaJobWrapper) provided in 0002_weka_jobtype.py
    jobs = [
            (1, 'SupervisedTagDiscovery'),
            (2, 'RandomWindowTagging'),
            (3, 'TagAnalysis'),
            (4, 'AdaptAnalysis'),
            (5, 'Transcription'),
            (6, 'UnknownTagDiscovery'),
            (7, 'UnsupervisedTagDiscovery'),
            (8, 'Test'),
            (9, 'ImportAudioFile'),
            (10, 'SupervisedTagDiscoveryParent'),
            (11, 'SupervisedTagDiscoveryChild'),
            (12, 'ExpertAgreementClassification')
           ]

    for (id, name) in jobs:
        try:
            x = JobTypes.objects.get(pk=id)
        except ObjectDoesNotExist:
            JobTypes(pk=id, name=name).save()


def addInitialProjectTypes(apps, schema_editor):
    ProjectTypes = apps.get_model("tools", "ProjectTypes")

    projectTypes = [
                  (1, 'audio'),
                  (2, 'image'),
                  (3, 'movement'),
                  (4, 'text'),
                  (5, 'video'),
                 ]

    for (id, name) in projectTypes:
        try:
            x = ProjectTypes.objects.get(pk=id)
        except ObjectDoesNotExist:
            ProjectTypes(pk=id, name=name).save()


def addInitialMediaTypes(apps, schema_editor):
    MediaTypes = apps.get_model("tools", "MediaTypes")

    mediaTypes = [
                  (1, 'audio'),
                  (2, 'image'),
                  (3, 'movement'),
                  (4, 'text'),
                  (5, 'video'),
                 ]

    for (id, name) in mediaTypes:
        try:
            x = MediaTypes.objects.get(pk=id)
        except ObjectDoesNotExist:
            MediaTypes(pk=id, name=name).save()


def addInitialJobStatusTypes(apps, schema_editor):
    JobStatusTypes = apps.get_model("tools", "JobStatusTypes")

    jobStatusTypes = [
                      (1, 'Unknown'),
                      (2, 'Queued'),
                      (3, 'Running'),
                      (4, 'Error'),
                      (5, 'Stopped'),
                      (6, 'Complete'),
                     ]

    for (id, name) in jobStatusTypes:
        try:
            x = JobStatusTypes.objects.get(pk=id)
        except ObjectDoesNotExist:
            JobStatusTypes(pk=id, name=name).save()


def addInitialExtendedUserSettings(apps, schema_editor):
    ExtendedUserSettings = apps.get_model("tools", "ExtendedUserSettings")

    extendedUserSettings = [
                            (1, 'spectraPitchTraceType', 'None'),
                            (2, 'spectraPitchTraceNumSamplePoints', '1000'),
                            (3, 'spectraPitchTraceColor', 'Black'),
                            (4, 'spectraPitchTraceWidth', '1'),
                            (5, 'spectraPitchTraceMinCorrelation', '0'),
                            (6, 'spectraPitchTraceEntropyThreshold', '999999'),
                            (7, 'spectraPitchTraceStartFreq', '80'),
                            (8, 'spectraPitchTraceEndFreq', '2000'),
                            (9, 'spectraPitchTraceMinEnergyThreshold', '0'),
                            (10, 'spectraPitchTraceTolerance', '0.15'),
                            (11, 'spectraPitchTraceInverseFreqWeight', '0'),
                           ]

    for (id, name, default) in extendedUserSettings:
        try:
            x = ExtendedUserSettings.objects.get(pk=id)
        except ObjectDoesNotExist:
            ExtendedUserSettings(pk=id, name=name, default=default).save()


class Migration(migrations.Migration):

    dependencies = [
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
    ]

    operations = [
        migrations.CreateModel(
            name='ExtendedUserSettings',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('name', models.CharField(max_length=255)),
                ('default', models.CharField(max_length=255, null=True)),
            ],
        ),
        migrations.CreateModel(
            name='ExtendedUserSettingsValues',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('value', models.CharField(max_length=255, null=True)),
                ('setting', models.ForeignKey(to='tools.ExtendedUserSettings')),
            ],
        ),
        migrations.CreateModel(
            name='JobLog',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('messageDate', models.DateTimeField(default=datetime.datetime.now, verbose_name=b'Message Date')),
                ('message', models.TextField(default=None, null=True, verbose_name=b'Log Message')),
            ],
        ),
        migrations.CreateModel(
            name='JobParameters',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('name', models.CharField(max_length=255)),
                ('value', models.CharField(max_length=255)),
            ],
        ),
        migrations.CreateModel(
            name='JobResultFile',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('creationDate', models.DateTimeField(default=datetime.datetime.now, verbose_name=b'File Creation Date')),
                ('resultFile', models.FileField(max_length=255, null=True, upload_to=nesterSettings.getJobResultFileUploadFilePath)),
                ('notes', models.TextField(null=True, verbose_name=b'Notes')),
                ('uploadAuthorizationNonce', models.CharField(max_length=255, null=True)),
            ],
        ),
        migrations.CreateModel(
            name='Jobs',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('name', models.CharField(max_length=255)),
                ('creationDate', models.DateTimeField(default=datetime.datetime.now, verbose_name=b'Creation Date')),
                ('numToComplete', models.IntegerField(default=0, verbose_name=b'numToComplete')),
                ('numCompleted', models.IntegerField(default=0, verbose_name=b'numCompleted')),
                ('fractionCompleted', models.FloatField(default=0, verbose_name=b'fractionCompleted')),
                ('elapsedRealTime', models.FloatField(default=0.0, verbose_name=b'elapsedRealTime')),
                ('timeToCompletion', models.FloatField(default=-1, verbose_name=b'timeToCompletion')),
                ('isRunning', models.BooleanField(default=False, verbose_name=b'isRunning')),
                ('wasStopped', models.BooleanField(default=False, verbose_name=b'wasStopped')),
                ('isComplete', models.BooleanField(default=False, verbose_name=b'isComplete')),
                ('wasDeleted', models.BooleanField(default=False, verbose_name=b'wasDeleted')),
                ('startedDate', models.DateTimeField(default=None, null=True, verbose_name=b'Started Date', blank=True)),
                ('lastStatusDate', models.DateTimeField(default=None, null=True, verbose_name=b'Last Status Date', blank=True)),
                ('priority', models.IntegerField(default=0, blank=True)),
                ('parentJob', models.ForeignKey(default=None, blank=True, to='tools.Jobs', null=True)),
            ],
        ),
        migrations.CreateModel(
            name='JobStatusTypes',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('name', models.CharField(max_length=255, verbose_name=b'Name')),
            ],
        ),
        migrations.CreateModel(
            name='JobTypes',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('name', models.CharField(max_length=255, verbose_name=b'Name')),
            ],
        ),
        migrations.CreateModel(
            name='Library',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('name', models.CharField(max_length=255, verbose_name=b'Name')),
                ('notes', models.TextField(default=None, null=True, verbose_name=b'Notes')),
                ('user', models.ForeignKey(to=settings.AUTH_USER_MODEL)),
            ],
        ),
        migrations.CreateModel(
            name='MediaFile',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('active', models.BooleanField(default=True, verbose_name=b'active')),
                ('alias', models.CharField(max_length=255, verbose_name=b'Alias')),
                ('uploadDate', models.DateTimeField(default=datetime.datetime.now, null=True, verbose_name=b'Date Uploaded')),
                ('relativeFilePath', models.CharField(max_length=1024, null=True, verbose_name=b'relativeFilePath')),
                ('absoluteFilePath', models.CharField(max_length=1024, null=True, verbose_name=b'absoluteFilePath')),
                ('hasTranscript', models.BooleanField(default=False, verbose_name=b'hasTranscript')),
                ('realStartTime', models.DateTimeField(default=None, null=True, verbose_name=b'File Start Date/Time', blank=True)),
                ('library', models.ForeignKey(to='tools.Library')),
            ],
        ),
        migrations.CreateModel(
            name='MediaFileMetaData',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('name', models.CharField(max_length=255)),
                ('value', models.CharField(max_length=255, null=True)),
                ('userEditable', models.BooleanField(default=False, verbose_name=b'userEditable')),
                ('mediaFile', models.ForeignKey(to='tools.MediaFile')),
            ],
        ),
        migrations.CreateModel(
            name='MediaFileUserMetaData',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('value', models.TextField(default=None, null=True, verbose_name=b'Value')),
                ('mediaFile', models.ForeignKey(to='tools.MediaFile')),
            ],
        ),
        migrations.CreateModel(
            name='MediaFileUserMetaDataField',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('name', models.CharField(max_length=255)),
                ('description', models.CharField(max_length=255, blank=True)),
                ('library', models.ForeignKey(to='tools.Library')),
            ],
        ),
        migrations.CreateModel(
            name='MediaTypes',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('name', models.CharField(max_length=255, verbose_name=b'Name')),
            ],
        ),
        migrations.CreateModel(
            name='Project',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('name', models.CharField(max_length=255, verbose_name=b'Name')),
                ('creationDate', models.DateTimeField(default=datetime.datetime.now, null=True, verbose_name=b'Creation Date')),
                ('mediaFiles', models.ManyToManyField(to='tools.MediaFile')),
            ],
        ),
        migrations.CreateModel(
            name='ProjectPermissions',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('canRead', models.BooleanField(default=False)),
                ('canWrite', models.BooleanField(default=False)),
                ('project', models.ForeignKey(to='tools.Project')),
                ('user', models.ForeignKey(to=settings.AUTH_USER_MODEL)),
            ],
        ),
        migrations.CreateModel(
            name='ProjectTypes',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('name', models.CharField(max_length=255, verbose_name=b'Name')),
            ],
        ),
        migrations.CreateModel(
            name='RandomWindow',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('imagePath', models.CharField(max_length=1024, null=True, verbose_name=b'imagePath')),
                ('startTime', models.FloatField(verbose_name=b'startTime')),
                ('endTime', models.FloatField(verbose_name=b'endTime')),
                ('mediaFile', models.ForeignKey(to='tools.MediaFile')),
                ('randomWindowTaggingJob', models.ForeignKey(to='tools.Jobs')),
            ],
        ),
        migrations.CreateModel(
            name='Sensor',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('name', models.CharField(max_length=255, verbose_name=b'Name')),
                ('x', models.FloatField()),
                ('y', models.FloatField()),
                ('z', models.FloatField()),
                ('layoutRow', models.IntegerField()),
                ('layoutColumn', models.IntegerField()),
            ],
        ),
        migrations.CreateModel(
            name='SensorArray',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('name', models.CharField(max_length=255, verbose_name=b'Name')),
                ('layoutRows', models.IntegerField()),
                ('layoutColumns', models.IntegerField()),
                ('user', models.ForeignKey(to=settings.AUTH_USER_MODEL)),
            ],
        ),
        migrations.CreateModel(
            name='Tag',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('creationDate', models.DateTimeField(default=datetime.datetime.now, null=True, verbose_name=b'Creation Date')),
                ('randomlyChosen', models.BooleanField(default=False, verbose_name=b'randomlyChosen')),
                ('machineTagged', models.BooleanField(default=None, verbose_name=b'machineTagged')),
                ('userTagged', models.BooleanField(default=None, verbose_name=b'userTagged')),
                ('needsReview', models.BooleanField(default=False, verbose_name=b'needsReview')),
                ('strength', models.FloatField(verbose_name=b'strength')),
                ('parentTag', models.ForeignKey(blank=True, to='tools.Tag', null=True)),
            ],
        ),
        migrations.CreateModel(
            name='TagClass',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('className', models.CharField(max_length=255)),
                ('displayName', models.CharField(max_length=255, null=True)),
                ('project', models.ForeignKey(to='tools.Project')),
                ('user', models.ForeignKey(to=settings.AUTH_USER_MODEL)),
            ],
        ),
        migrations.CreateModel(
            name='TagExample',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('creationDate', models.DateTimeField(default=datetime.datetime.now, null=True, verbose_name=b'Creation Date')),
                ('imagePath', models.CharField(max_length=1024, null=True, verbose_name=b'imagePath')),
                ('startTime', models.FloatField(verbose_name=b'startTime')),
                ('endTime', models.FloatField(verbose_name=b'endTime')),
                ('minFrequency', models.FloatField(verbose_name=b'minFrequency')),
                ('maxFrequency', models.FloatField(verbose_name=b'maxFrequency')),
                ('mediaFile', models.ForeignKey(to='tools.MediaFile')),
            ],
        ),
        migrations.CreateModel(
            name='TagSet',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('name', models.CharField(max_length=255, verbose_name=b'Name')),
                ('creationDate', models.DateTimeField(default=datetime.datetime.now, null=True, verbose_name=b'Creation Date')),
                ('notes', models.TextField(default=None, null=True, verbose_name=b'Notes')),
                ('hidden', models.BooleanField(default=False, verbose_name=b'Hidden')),
                ('project', models.ForeignKey(to='tools.Project')),
                ('user', models.ForeignKey(to=settings.AUTH_USER_MODEL)),
            ],
        ),
        migrations.CreateModel(
            name='UserSettings',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('name', models.CharField(default=b'default', max_length=255, verbose_name=b'name')),
                ('windowSizeInSeconds', models.FloatField(default=4.0, verbose_name=b'windowSizeInSeconds')),
                ('spectraMinimumBandFrequency', models.FloatField(default=20.0, verbose_name=b'spectraMinimumBandFrequency')),
                ('spectraMaximumBandFrequency', models.FloatField(default=20000.0, verbose_name=b'spectraMaximumBandFrequency')),
                ('spectraDampingFactor', models.FloatField(default=0.02, verbose_name=b'spectraDampingFactor')),
                ('spectraNumFrequencyBands', models.IntegerField(default=256, verbose_name=b'spectraNumFrequencyBands')),
                ('spectraNumFramesPerSecond', models.FloatField(default=128, verbose_name=b'spectraNumFramesPerSecond')),
                ('showSpectra', models.NullBooleanField(default=True, verbose_name=b'showSpectra')),
                ('showWaveform', models.NullBooleanField(default=True, verbose_name=b'showWaveform')),
                ('loadAudio', models.NullBooleanField(default=True, verbose_name=b'loadAudio')),
                ('maxNumAudioFilesToList', models.IntegerField(default=100, verbose_name=b'maxNumAudioFilesToList')),
                ('fileViewWindowSizeInSeconds', models.FloatField(default=4.0, verbose_name=b'fileViewWindowSizeInSeconds')),
                ('fileViewSpectraMinimumBandFrequency', models.FloatField(default=20.0, verbose_name=b'fileViewSpectraMinimumBandFrequency')),
                ('fileViewSpectraMaximumBandFrequency', models.FloatField(default=20000.0, verbose_name=b'fileViewSpectraMaximumBandFrequency')),
                ('fileViewSpectraDampingFactor', models.FloatField(default=0.02, verbose_name=b'fileViewSpectraDampingFactor')),
                ('fileViewSpectraNumFrequencyBands', models.IntegerField(default=256, verbose_name=b'fileViewSpectraNumFrequencyBands')),
                ('fileViewSpectraNumFramesPerSecond', models.FloatField(default=128, verbose_name=b'fileViewSpectraNumFramesPerSecond')),
                ('catalogViewWindowSizeInSeconds', models.FloatField(default=4.0, verbose_name=b'catalogViewWindowSizeInSeconds')),
                ('catalogViewSpectraMinimumBandFrequency', models.FloatField(default=20.0, verbose_name=b'catalogViewSpectraMinimumBandFrequency')),
                ('catalogViewSpectraMaximumBandFrequency', models.FloatField(default=20000.0, verbose_name=b'catalogViewSpectraMaximumBandFrequency')),
                ('catalogViewSpectraDampingFactor', models.FloatField(default=0.02, verbose_name=b'catalogViewSpectraDampingFactor')),
                ('catalogViewSpectraNumFrequencyBands', models.IntegerField(default=64, verbose_name=b'catalogViewSpectraNumFrequencyBands')),
                ('catalogViewSpectraNumFramesPerSecond', models.FloatField(default=100, verbose_name=b'catalogViewSpectraNumFramesPerSecond')),
                ('tagViewWindowSizeInSeconds', models.FloatField(default=4.0, verbose_name=b'tagViewWindowSizeInSeconds')),
                ('tagViewSpectraMinimumBandFrequency', models.FloatField(default=20.0, verbose_name=b'tagViewSpectraMinimumBandFrequency')),
                ('tagViewSpectraMaximumBandFrequency', models.FloatField(default=20000.0, verbose_name=b'tagViewSpectraMaximumBandFrequency')),
                ('tagViewSpectraDampingFactor', models.FloatField(default=0.02, verbose_name=b'tagViewSpectraDampingFactor')),
                ('tagViewSpectraNumFrequencyBands', models.IntegerField(default=256, verbose_name=b'tagViewSpectraNumFrequencyBands')),
                ('tagViewSpectraNumFramesPerSecond', models.FloatField(default=128, verbose_name=b'tagViewSpectraNumFramesPerSecond')),
                ('user', models.ForeignKey(to=settings.AUTH_USER_MODEL)),
            ],
        ),
        migrations.AddField(
            model_name='tagexample',
            name='tagSet',
            field=models.ForeignKey(to='tools.TagSet'),
        ),
        migrations.AddField(
            model_name='tagexample',
            name='user',
            field=models.ForeignKey(to=settings.AUTH_USER_MODEL),
        ),
        migrations.AddField(
            model_name='tag',
            name='tagClass',
            field=models.ForeignKey(to='tools.TagClass'),
        ),
        migrations.AddField(
            model_name='tag',
            name='tagExample',
            field=models.ForeignKey(to='tools.TagExample'),
        ),
        migrations.AddField(
            model_name='tag',
            name='user',
            field=models.ForeignKey(to=settings.AUTH_USER_MODEL),
        ),
        migrations.AddField(
            model_name='sensor',
            name='sensorArray',
            field=models.ForeignKey(to='tools.SensorArray'),
        ),
        migrations.AddField(
            model_name='project',
            name='type',
            field=models.ForeignKey(to='tools.ProjectTypes'),
        ),
        migrations.AddField(
            model_name='project',
            name='user',
            field=models.ForeignKey(to=settings.AUTH_USER_MODEL),
        ),
        migrations.AddField(
            model_name='mediafileusermetadata',
            name='metaDataField',
            field=models.ForeignKey(to='tools.MediaFileUserMetaDataField'),
        ),
        migrations.AddField(
            model_name='mediafile',
            name='sensor',
            field=models.ForeignKey(default=None, blank=True, to='tools.Sensor', null=True),
        ),
        migrations.AddField(
            model_name='mediafile',
            name='type',
            field=models.ForeignKey(to='tools.MediaTypes'),
        ),
        migrations.AddField(
            model_name='mediafile',
            name='user',
            field=models.ForeignKey(to=settings.AUTH_USER_MODEL),
        ),
        migrations.AddField(
            model_name='jobs',
            name='project',
            field=models.ForeignKey(to='tools.Project', null=True),
        ),
        migrations.AddField(
            model_name='jobs',
            name='requestedStatus',
            field=models.ForeignKey(related_name='jobs_requestStatuses', default=None, blank=True, to='tools.JobStatusTypes', null=True),
        ),
        migrations.AddField(
            model_name='jobs',
            name='status',
            field=models.ForeignKey(related_name='jobs_statuses', default=None, blank=True, to='tools.JobStatusTypes', null=True),
        ),
        migrations.AddField(
            model_name='jobs',
            name='type',
            field=models.ForeignKey(to='tools.JobTypes'),
        ),
        migrations.AddField(
            model_name='jobs',
            name='user',
            field=models.ForeignKey(to=settings.AUTH_USER_MODEL),
        ),
        migrations.AddField(
            model_name='jobresultfile',
            name='job',
            field=models.ForeignKey(to='tools.Jobs'),
        ),
        migrations.AddField(
            model_name='jobparameters',
            name='job',
            field=models.ForeignKey(to='tools.Jobs'),
        ),
        migrations.AddField(
            model_name='joblog',
            name='job',
            field=models.ForeignKey(to='tools.Jobs'),
        ),
        migrations.AddField(
            model_name='extendedusersettingsvalues',
            name='userSettings',
            field=models.ForeignKey(to='tools.UserSettings'),
        ),
        migrations.AlterUniqueTogether(
            name='mediafileusermetadatafield',
            unique_together=set([('name', 'library')]),
        ),
        # Add Initial Data
        migrations.RunPython(addInitialJobTypes),
        migrations.RunPython(addInitialProjectTypes),
        migrations.RunPython(addInitialMediaTypes),
        migrations.RunPython(addInitialJobStatusTypes),
        migrations.RunPython(addInitialExtendedUserSettings),
    ]
