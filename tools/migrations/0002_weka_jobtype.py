from __future__ import unicode_literals

from django.db import models, migrations

# INSERT INTO tools_jobtypes (id, name) VALUES (13, 'WekaJobWrapper');

def addWekaJobType(apps, schema_editor):
    JobTypes = apps.get_model("tools", "JobTypes")
    JobTypes(pk=13, name='WekaJobWrapper').save()

class Migration(migrations.Migration):

    dependencies = [
        ('tools', '0001_initial'),
    ]

    operations = [
        migrations.RunPython(addWekaJobType),
    ]
