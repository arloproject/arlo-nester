# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations

def addPitchTraceSettings(apps, schema_editor):
    ExtendedUserSettings = apps.get_model("tools", "ExtendedUserSettings")
    ExtendedUserSettings(pk=12, name='spectraPitchTraceMaxPathLengthPerTransition', default='4').save()
    ExtendedUserSettings(pk=13, name='spectraPitchTraceWindowSize', default='13').save()
    ExtendedUserSettings(pk=14, name='spectraPitchTraceExtendRangeFactor', default='2').save()

class Migration(migrations.Migration):

    dependencies = [
        ('tools', '0003_project_notes'),
    ]

    operations = [
        migrations.RunPython(addPitchTraceSettings),
    ]

