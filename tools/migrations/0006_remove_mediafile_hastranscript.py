# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('tools', '0005_pitchtrace_settings'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='mediafile',
            name='hasTranscript',
        ),
    ]
