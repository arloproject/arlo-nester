# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('tools', '0013_arlopermission_arlousergroup'),
    ]

    operations = [
        migrations.AddField(
            model_name='library',
            name='default_permission_admin',
            field=models.BooleanField(default=False, verbose_name=b'Default Admin Permission?'),
        ),
        migrations.AddField(
            model_name='library',
            name='default_permission_launch_job',
            field=models.BooleanField(default=False, verbose_name=b'Default Launch Job Permission?'),
        ),
        migrations.AddField(
            model_name='library',
            name='default_permission_read',
            field=models.BooleanField(default=False, verbose_name=b'Default Read Permission?'),
        ),
        migrations.AddField(
            model_name='library',
            name='default_permission_write',
            field=models.BooleanField(default=False, verbose_name=b'Default Write Permission?'),
        ),
        migrations.AddField(
            model_name='project',
            name='default_permission_admin',
            field=models.BooleanField(default=False, verbose_name=b'Default Admin Permission?'),
        ),
        migrations.AddField(
            model_name='project',
            name='default_permission_launch_job',
            field=models.BooleanField(default=False, verbose_name=b'Default Launch Job Permission?'),
        ),
        migrations.AddField(
            model_name='project',
            name='default_permission_read',
            field=models.BooleanField(default=False, verbose_name=b'Default Read Permission?'),
        ),
        migrations.AddField(
            model_name='project',
            name='default_permission_write',
            field=models.BooleanField(default=False, verbose_name=b'Default Write Permission?'),
        ),
    ]
