# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
import datetime
from django.conf import settings


class Migration(migrations.Migration):

    dependencies = [
        ('contenttypes', '0002_remove_content_type_name'),
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
        ('tools', '0012_sensorarray_library'),
    ]

    operations = [
        migrations.CreateModel(
            name='ArloPermission',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('bearer_id', models.PositiveIntegerField()),
                ('target_id', models.PositiveIntegerField()),
                ('creationDate', models.DateTimeField(default=datetime.datetime.now, verbose_name=b'Creation Date')),
                ('read', models.BooleanField(default=False, verbose_name=b'Read?')),
                ('write', models.BooleanField(default=False, verbose_name=b'Write?')),
                ('launch_job', models.BooleanField(default=False, verbose_name=b'Launch Job?')),
                ('admin', models.BooleanField(default=False, verbose_name=b'Admin?')),
                ('bearer_type', models.ForeignKey(related_name='+', to='contenttypes.ContentType')),
                ('createdBy', models.ForeignKey(to=settings.AUTH_USER_MODEL)),
                ('target_type', models.ForeignKey(related_name='+', to='contenttypes.ContentType')),
            ],
        ),
        migrations.CreateModel(
            name='ArloUserGroup',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('name', models.CharField(unique=True, max_length=255, verbose_name=b'Group Name')),
                ('creationDate', models.DateTimeField(default=datetime.datetime.now, verbose_name=b'Creation Date')),
                ('createdBy', models.ForeignKey(related_name='+', to=settings.AUTH_USER_MODEL)),
                ('users', models.ManyToManyField(to=settings.AUTH_USER_MODEL)),
            ],
        ),
    ]
