from __future__ import unicode_literals

from django.db import models, migrations

def addWekaJobType(apps, schema_editor):
    JobTypes = apps.get_model("tools", "JobTypes")
    JobTypes(pk=14, name='LibraryMediaFileValidation').save()

class Migration(migrations.Migration):

    dependencies = [
        ('tools', '0007_mediafile_md5'),
    ]

    operations = [
        migrations.RunPython(addWekaJobType),
    ]
