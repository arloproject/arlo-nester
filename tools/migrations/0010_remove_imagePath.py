# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('tools', '0009_remove_mediafile_absolutepath'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='randomwindow',
            name='imagePath',
        ),
        migrations.RemoveField(
            model_name='tagexample',
            name='imagePath',
        ),
    ]
