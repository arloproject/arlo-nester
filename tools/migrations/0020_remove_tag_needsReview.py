# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
import tools.models


class Migration(migrations.Migration):

    dependencies = [
        ('tools', '0019_mediafile_file'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='tag',
            name='needsReview',
        ),
        migrations.AlterField(
            model_name='mediafile',
            name='file',
            field=models.FileField(max_length=1024, upload_to=tools.models._getMediaFileUploadTo),
        ),
    ]
