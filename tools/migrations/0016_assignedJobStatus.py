from __future__ import unicode_literals

from django.db import migrations


def addAssignedJobStatusType(apps, schema_editor):
    JobTypes = apps.get_model("tools", "JobStatusTypes")
    JobTypes(pk=7, name='Assigned').save()

class Migration(migrations.Migration):

    dependencies = [
        ('tools', '0015_jobPlugins'),
    ]

    operations = [
        migrations.RunPython(addAssignedJobStatusType),
    ]
