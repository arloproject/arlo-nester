# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations

def addPitchTraceSettings(apps, schema_editor):
    ExtendedUserSettings = apps.get_model("tools", "ExtendedUserSettings")
    ExtendedUserSettings(pk=15, name='spectraPitchTraceNumFramesPerSecond', default='200').save()
    ExtendedUserSettings(pk=16, name='spectraPitchTraceNumFrequencyBands', default='256').save()
    ExtendedUserSettings(pk=17, name='spectraPitchTraceDampingFactor', default='0.01').save()
    ExtendedUserSettings(pk=18, name='spectraPitchTraceMinimumBandFrequency', default='75').save()
    ExtendedUserSettings(pk=19, name='spectraPitchTraceMaximumBandFrequency', default='10000').save()

class Migration(migrations.Migration):

    dependencies = [
        ('tools', '0004_pitchtrace_settings'),
    ]

    operations = [
        migrations.RunPython(addPitchTraceSettings),
    ]

