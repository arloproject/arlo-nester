# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('tools', '0008_validateLibrary_jobtype'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='mediafile',
            name='absoluteFilePath',
        ),
        migrations.AlterField(
            model_name='mediafile',
            name='relativeFilePath',
            field=models.CharField(default='', max_length=1024, verbose_name=b'relativeFilePath'),
            preserve_default=False,
        ),
    ]
