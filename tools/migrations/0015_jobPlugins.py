from __future__ import unicode_literals

from django.db import migrations


def addPluginJobType(apps, schema_editor):
    JobTypes = apps.get_model("tools", "JobTypes")
    JobTypes(pk=15, name='JobPlugin').save()

class Migration(migrations.Migration):

    dependencies = [
        ('tools', '0014_default_permissions'),
    ]

    operations = [
        migrations.RunPython(addPluginJobType),
    ]
