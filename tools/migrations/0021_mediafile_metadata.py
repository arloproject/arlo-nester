# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.core.exceptions import ObjectDoesNotExist
from django.db import migrations


def migrateMetadata(apps, schema_editor):
    '''
    This migration moves some MediaFileMetaData to MediaFileUserMetaData to
    deprecate the original fields.
    '''

    # Get Models
    MediaFileMetaData = apps.get_model("tools", "MediaFileMetaData")
    MediaFileUserMetaDataField = apps.get_model("tools", "MediaFileUserMetaDataField")
    MediaFileUserMetaData = apps.get_model("tools", "MediaFileUserMetaData")

    # Migrate the 'sex' Metadata field into the User Metadata
    for fieldname in ['sex']:
        entries = MediaFileMetaData.objects.filter(name=fieldname)
        for entry in entries:
            if entry.value:
                # Get Field
                try:
                    field = MediaFileUserMetaDataField.objects.get(name=fieldname, library=entry.mediaFile.library)
                except ObjectDoesNotExist:
                    # create the field
                    field = MediaFileUserMetaDataField(name=fieldname, library=entry.mediaFile.library)
                    field.save()

                # Set Value
                try:
                    metadata = MediaFileUserMetaData.objects.get(metaDataField=field, mediaFile=entry.mediaFile)
                    metadata.value = entry.value
                    metadata.save()
                except ObjectDoesNotExist:
                    metadata = MediaFileUserMetaData(
                                     metaDataField=field,
                                     mediaFile=entry.mediaFile,
                                     value=entry.value)
                    metadata.save()
            entry.delete()


class Migration(migrations.Migration):

    dependencies = [
        ('tools', '0020_remove_tag_needsReview'),
    ]

    operations = [
        migrations.RunPython(migrateMetadata),
    ]
