# -*- coding: utf-8 -*-
from __future__ import unicode_literals
import logging

from django.db import models, migrations


###
# Merge the TagExample into the Tag Model


# def update_tag_from_tagexample(apps, schema_editor):
#     Tag = apps.get_model("tools", "Tag")
#     TagExample = apps.get_model("tools", "TagExample")
# 
#     # Update Tags from TagExample data
#     for tag in Tag.objects.all():
#         tagExample = tag.tagExample
# 
#         tag.minFrequency = tagExample.minFrequency
#         tag.maxFrequency = tagExample.maxFrequency
#         tag.startTime = tagExample.startTime
#         tag.endTime = tagExample.endTime
#         tag.mediaFile = tagExample.mediaFile
#         tag.tagSet = tagExample.tagSet
#         tag.tagExample = None
#         tag.save()


# def delete_all_tagExamples(apps, schema_editor):
#     TagExample = apps.get_model("tools", "TagExample")
#     x = TagExample.objects.all().delete()


class Migration(migrations.Migration):

    dependencies = [
        ('tools', '0010_remove_imagePath'),
        ('poetrytagging', '0002_merge_tags'),
        ('metatagging', '0002_merge_tags'),
    ]

    operations = [

        ###
        # Add new Tag Fields

        migrations.AddField(
            model_name='tag',
            name='endTime',
            field=models.FloatField(default=None, null=True, verbose_name=b'endTime'),
        ),
        migrations.AddField(
            model_name='tag',
            name='maxFrequency',
            field=models.FloatField(default=None, null=True, verbose_name=b'maxFrequency'),
        ),
        migrations.AddField(
            model_name='tag',
            name='mediaFile',
            field=models.ForeignKey(default=None, to='tools.MediaFile', null=True),
        ),
        migrations.AddField(
            model_name='tag',
            name='minFrequency',
            field=models.FloatField(default=None, null=True, verbose_name=b'minFrequency'),
        ),
        migrations.AddField(
            model_name='tag',
            name='startTime',
            field=models.FloatField(default=None, null=True, verbose_name=b'startTime'),
        ),
        migrations.AddField(
            model_name='tag',
            name='tagSet',
            field=models.ForeignKey(default=None, to='tools.TagSet', null=True),
        ),
        migrations.AlterField(
            model_name='tag',
            name='tagExample',
            field=models.ForeignKey(default=None, to='tools.TagExample', null=True),
        ),

        ###
        # Update Tags

        # Python version is far too slow - have to drop back to raw SQL here
#         migrations.RunPython(update_tag_from_tagexample),
        migrations.RunSQL(
            'UPDATE tools_tag, tools_tagexample '
            'SET '
                'tools_tag.minFrequency = tools_tagexample.minFrequency, '
                'tools_tag.maxFrequency = tools_tagexample.maxFrequency, '
                'tools_tag.startTime = tools_tagexample.startTime, '
                'tools_tag.endTime = tools_tagexample.endTime, '
                'tools_tag.mediaFile_id = tools_tagexample.mediaFile_id, '
                'tools_tag.tagSet_id = tools_tagexample.tagSet_id, '
                'tools_tag.tagExample_id = NULL '
            'WHERE tools_tag.tagExample_id = tools_tagexample.id'
            ),

        ###
        # Delete all TagExample objects

        # Python version is far too slow - have to drop back to raw SQL here
#         migrations.RunPython(delete_all_tagExamples),
        migrations.RunSQL('DELETE FROM tools_tagexample'),

        ###
        # Remove temporary defaults from Fields

        migrations.AlterField(
            model_name='tag',
            name='endTime',
            field=models.FloatField(verbose_name=b'endTime'),
        ),
        migrations.AlterField(
            model_name='tag',
            name='maxFrequency',
            field=models.FloatField(verbose_name=b'maxFrequency'),
        ),
        migrations.AlterField(
            model_name='tag',
            name='mediaFile',
            field=models.ForeignKey(to='tools.MediaFile'),
        ),
        migrations.AlterField(
            model_name='tag',
            name='minFrequency',
            field=models.FloatField(verbose_name=b'minFrequency'),
        ),
        migrations.AlterField(
            model_name='tag',
            name='startTime',
            field=models.FloatField(verbose_name=b'startTime'),
        ),
        migrations.AlterField(
            model_name='tag',
            name='tagSet',
            field=models.ForeignKey(to='tools.TagSet'),
        ),

    ]
