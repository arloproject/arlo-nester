import datetime

from django import forms
from django.db import models
from django.contrib.contenttypes.fields import GenericForeignKey, GenericRelation
from django.contrib.contenttypes.models import ContentType
from django.db.models import Q
from django.utils.encoding import python_2_unicode_compatible

import nesterSettings as nesterSettings


###############################################################
#                                                             #
#                       DB Models                             #
#                                                             #
###############################################################


##
# Permissions / Sharing Models

class ArloPermissionManager(models.Manager):

  ## Return a list of all ArloPermissions to which a user has access.
  # Call as ArloPermission.objects.getUserArloUserGroups(user)
  # @param user A User Object
  # @return A ArloPermission QuerySet

  def getUserArloPermissions(self, user):

    # get a list of Permissions that:

    #   - the User created
    #   - the user is the Bearer of directly
    #   - Groups the User is in
    #   - All Permissions to Targets that the User has Admin
    #     - ArloUserGroup
    #     - Library
    #     - Project
    #     - TagSet


    from tools.views.userPermissions import ARLO_PERMS

    group_type = ContentType.objects.get_for_model(ArloUserGroup)
    user_type = ContentType.objects.get_for_model(user)

    # For each Target type, loop over all and add to list if user has Admin
    admin_targets = []  # will be a list of (target_class, [target_ids])
    for type_class in [ArloUserGroup, Library, Project, TagSet]:
      admin_ids = []
      for target in type_class.objects.all():
        if target.userHasPermissions(user, ARLO_PERMS.ADMIN, enable_staff_override=False):
          admin_ids.append(target.id)
      if admin_ids:
        admin_targets.append( (type_class, admin_ids) )


    user_groups = ArloUserGroup.objects.filter(users=user).values_list('id', flat=True)

    ###
    # Queries

    query_list = []
    # the User created
    query_list.append(Q(createdBy=user))
    # the user is the Bearer of
    query_list.append(Q(bearer_type=user_type, bearer_id=user.id))
    # Groups the User is in
    query_list.append(Q(bearer_type=group_type, bearer_id__in=user_groups))
    #   - All Permissions to Targets that the User has Admin
    query_list.extend(map(lambda x: Q(target_type=ContentType.objects.get_for_model(x[0]), target_id__in=x[1]), admin_targets))

    query = reduce(lambda x,y: x | y, query_list)

    return super(ArloPermissionManager, self).get_queryset().filter(query).distinct().order_by('id')



# Use ContentTypes framework to enable generic bearers / targets for our permissions
@python_2_unicode_compatible
class ArloPermission(models.Model):
  class Meta:
    verbose_name = "ARLO Permission"

  objects = ArloPermissionManager()  # override to user our custom manager

  # I can't imagine a case where we'll need to use the reverse relation
  # on these - disable with "related_name = '+'"
  bearer_type = models.ForeignKey(ContentType, on_delete=models.CASCADE, related_name='+')
  bearer_id = models.PositiveIntegerField()
  bearer_object = GenericForeignKey('bearer_type', 'bearer_id')
  target_type = models.ForeignKey(ContentType, on_delete=models.CASCADE, related_name='+')
  target_id = models.PositiveIntegerField()
  target_object = GenericForeignKey('target_type', 'target_id')
  creationDate = models.DateTimeField(verbose_name='Creation Date', default=datetime.datetime.now)
  createdBy = models.ForeignKey('auth.User')
  # Permissions
  read = models.BooleanField(verbose_name='Read?', default=False)
  write = models.BooleanField(verbose_name='Write?', default=False)
  launch_job = models.BooleanField(verbose_name='Launch Job?', default=False)
  admin = models.BooleanField(verbose_name='Admin?', default=False)

  ## Checks that the user has the indicated permissions.
  # Convenience wrapper around ArloPermissionPermissions.
  def userHasPermissions(self, user, target_permissions, enable_staff_override=True):
      # Runtime import to avoid circular-import issues with models
      from tools.views.userPermissions import ArloPermissionPermissions
      perms = ArloPermissionPermissions(self)
      return perms.userHasPermissions(user, target_permissions, enable_staff_override)

  def __str__(self):
    return '%s:%s -> %s:%s %s%s%s%s' % (
      self.bearer_type,
      self.bearer_id,
      self.target_type,
      self.target_id,
      1 if self.read else 0,
      1 if self.write else 0,
      1 if self.launch_job else 0,
      1 if self.admin else 0)

class ArloUserGroupManager(models.Manager):

  ## Return a list of all ArloUserGroups to which a user has access.
  # Call as ArloUserGroup.objects.getUserArloUserGroups(user)
  # @param user A User Object
  # @return A ArloUserGroup QuerySet

  def getUserArloUserGroups(self, user):

    # get a list of Groups that:
    #   - the user is in
    #   - the user created
    #   - the user is directly an admin of the group
    #   - the user is in groups that are an admin of the group

    # This seems very messy - I wonder if it isn't safer to just loop
    # over all Groups, checking perms on each. This is a function
    # that would definitely benefit from some caching.

    group_type = ContentType.objects.get_for_model(ArloUserGroup)
    user_type = ContentType.objects.get_for_model(user)

    user_groups = ArloUserGroup.objects.filter(users=user).values_list('id', flat=True)

    return super(ArloUserGroupManager, self).get_queryset().filter(
        # the user is in
        Q(users=user) |
        # the user created
        Q(createdBy=user) |
        # the user is directly an admin of the group
        Q(
            permissions__bearer_id=user.id,
            permissions__bearer_type=user_type,
            permissions__admin=True
        ) |
        # the user is in groups that are an admin of the group
        Q(
            permissions__bearer_type=group_type,
            permissions__bearer_id__in=user_groups,
            permissions__admin=True
        ) |
        # the user has directly read access to the group
        Q(
            permissions__bearer_id=user.id,
            permissions__bearer_type=user_type,
            permissions__read=True
        ) |
        # the user is in groups that have read access to the group
        Q(
            permissions__bearer_type=group_type,
            permissions__bearer_id__in=user_groups,
            permissions__read=True
        )
        ).distinct().order_by('name')


@python_2_unicode_compatible
class ArloUserGroup(models.Model):
  class Meta:
    verbose_name = "ARLO UserGroup"

  objects = ArloUserGroupManager()  # override to user our custom manager

  name = models.CharField(verbose_name='Group Name', max_length=255, unique=True)
  users = models.ManyToManyField('auth.User', blank=True, default=list)
  creationDate = models.DateTimeField(verbose_name='Creation Date', default=datetime.datetime.now)
  createdBy = models.ForeignKey('auth.User', related_name='+')

  # Setup a GenericRelation so we can directly query perms for the Group
  permissions = GenericRelation(
                                ArloPermission,
                                content_type_field='target_type',
                                object_id_field='target_id')


  def __str__(self):
    return '%s' % (self.name)


  ## Checks that the user has the indicated permissions.
  # Convenience wrapper around ArloUserGroupPermissions.
  def userHasPermissions(self, user, target_permissions, enable_staff_override=True):
      # Runtime import to avoid circular-import issues with models
      from tools.views.userPermissions import ArloUserGroupPermissions
      perms = ArloUserGroupPermissions(self)
      return perms.userHasPermissions(user, target_permissions, enable_staff_override)



@python_2_unicode_compatible
class ProjectTypes(models.Model):
  class Meta:
    verbose_name = "Project Type"

  name = models.CharField(verbose_name='Name', max_length=255)
  def __str__(self):
    return '%s' % (self.name)


class LibraryManager(models.Manager):

  ## Return a list of all Libraries to which a user has access.
  # Call as Library.objects.getUserLibraries(user)
  # @param user A User Object
  # @return A Library QuerySet

  def getUserLibraries(self, user):
    from django.contrib.auth.models import User
    user_type = ContentType.objects.get_for_model(User)
    groupContentType = ContentType.objects.get_for_model(ArloUserGroup)
    libraryContentType = ContentType.objects.get_for_model(Library)
    userGroupIds = ArloUserGroup.objects.filter(users=user).values_list('id', flat=True)
    userGroupLibIds = ArloPermission.objects.filter(bearer_type=groupContentType, bearer_id__in=userGroupIds, target_type=libraryContentType).values_list('target_id', flat=True)
    libraries = super(LibraryManager, self).get_queryset().filter(
      models.Q(user=user) |
      models.Q(permissions__bearer_id=user.id, permissions__bearer_type=user_type) |
      models.Q(id__in=userGroupLibIds)
    ).distinct()
    return libraries


@python_2_unicode_compatible
class Library(models.Model):
  class Meta:
    verbose_name_plural = "Libraries"

  objects = LibraryManager()  # override to user our custom manager

  user = models.ForeignKey('auth.User')
  name = models.CharField(verbose_name='Name', max_length=255)
  notes = models.TextField(verbose_name='Notes', default=None, null=True)
  #mediaFiles = models.ManyToManyField('MediaFile')
  default_permission_read = models.BooleanField(verbose_name='Default Read Permission?', default=False)
  default_permission_write = models.BooleanField(verbose_name='Default Write Permission?', default=False)
  default_permission_launch_job = models.BooleanField(verbose_name='Default Launch Job Permission?', default=False)
  default_permission_admin = models.BooleanField(verbose_name='Default Admin Permission?', default=False)

  permissions = GenericRelation(
                                ArloPermission,
                                content_type_field='target_type',
                                object_id_field='target_id')

  def __str__(self):
    return '%s' % (self.name)

  ## Checks that the user has the indicated permissions.
  # Convenience wrapper around ArloLibraryPermissions.
  def userHasPermissions(self, user, target_permissions, enable_staff_override=True):
      # Runtime import to avoid circular-import issues with models
      from tools.views.userPermissions import ArloLibraryPermissions
      alp = ArloLibraryPermissions(self)
      return alp.userHasPermissions(user, target_permissions, enable_staff_override)

  def getSensors(self):
      return Sensor.objects.filter(sensorArray__in=SensorArray.objects.filter(library=self))


class SensorArrayManager(models.Manager):

  ## Return a list of all SensorArrays to which a user has access.
  # Call as SensorArray.objects.getUserSensorArrays(user)
  # @param user A User Object
  # @return A SensorArray QuerySet

  def getUserSensorArrays(self, user):
    return super(SensorArrayManager, self).get_queryset().filter(library__in=Library.objects.getUserLibraries(user)).distinct()


@python_2_unicode_compatible
class SensorArray(models.Model):
  class Meta:
    verbose_name = "Sensor Array"
    verbose_name_plural = "Sensor Arrays"

  objects = SensorArrayManager()  # override to user our custom manager

  user = models.ForeignKey('auth.User')
  name = models.CharField(verbose_name='Name', max_length=255)
  library = models.ForeignKey(Library)
  layoutRows = models.IntegerField(null=False)
  layoutColumns = models.IntegerField(null=False)

  def __str__(self):
    return '%s' % (self.name)

  ## Checks that the user has the indicated permissions.
  # Convenience wrapper around ArloLibraryPermissions.
  def userHasPermissions(self, user, target_permissions, enable_staff_override=True):
      # Runtime import to avoid circular-import issues with models
      from tools.views.userPermissions import ArloLibraryPermissions
      alp = ArloLibraryPermissions(self.library)
      return alp.userHasPermissions(user, target_permissions, enable_staff_override)


class SensorManager(models.Manager):

  ## Return a list of all Sensors to which a user has access.
  # Call as Sensor.objects.getUserSensors(user)
  # @param user A User Object
  # @return A Sensor QuerySet

  def getUserSensors(self, user):
    user_arrays = SensorArray.objects.getUserSensorArrays(user)
    return super(SensorManager, self).get_queryset().filter(sensorArray__in=user_arrays).distinct()


@python_2_unicode_compatible
class Sensor(models.Model):
  objects = SensorManager()  # override to user our custom manager

  sensorArray = models.ForeignKey(SensorArray)
  name = models.CharField(verbose_name='Name', max_length=255)
  x = models.FloatField(null=False)
  y = models.FloatField(null=False)
  z = models.FloatField(null=False)
  layoutRow = models.IntegerField(null=False)
  layoutColumn = models.IntegerField(null=False)

  def __str__(self):
    return '%s' % (self.name)

  def userHasPermissions(self, user, target_permissions, enable_staff_override=True):
      return self.sensorArray.userHasPermissions(user, target_permissions, enable_staff_override)


@python_2_unicode_compatible
class MediaTypes(models.Model):
  name = models.CharField(verbose_name='Name', max_length=255)
  def __str__(self):
    return '%s' % (self.name)


def _getMediaFileUploadTo(mediaFile, filename):
  return nesterSettings.getMediaFilePath(mediaFile.library, filename)

@python_2_unicode_compatible
class MediaFile(models.Model):
  class Meta:
    verbose_name = "MediaFile"

  file = models.FileField(upload_to=_getMediaFileUploadTo, max_length=1024)
  user = models.ForeignKey('auth.User')
  type = models.ForeignKey(MediaTypes)
  active = models.BooleanField(verbose_name='active', default=True)
  alias = models.CharField(verbose_name='Alias', max_length=255)
  uploadDate = models.DateTimeField(verbose_name='Date Uploaded', null=True, default=datetime.datetime.now)
  sensor = models.ForeignKey(Sensor, null=True, blank=True, default=None)
  library = models.ForeignKey(Library)
  realStartTime = models.DateTimeField(verbose_name='File Start Date/Time', null=True, blank=True, default=None)
  md5 = models.CharField(verbose_name='MD5 File Hash', max_length=32, null=True, blank=True, default=None)

  def __str__(self):
    return '%s [%s]' % (self.alias, self.file.name)

  def getPath(self):
    return self.file.name

  def get_absolute_url(self):
    url = self.file.url
    return url

  ## Checks that the user has the indicated permissions.
  # Convenience wrapper around ARLO_PERMS.
  # Permissions are given through the Project. Check if the MediaFile is in any
  # of the Projects shared with the user, and if the user has
  # read access to that Project.
  # @note Always returns True if user owns the MediaFile.
  # @param self MediaFile object
  # @param user A User Object
  # @param target_permissions The permissions required to be successful, as defined by ARLO_PERMS
  # @return True on success, False otherwise

  def userHasPermissions(self, user, target_permissions, enable_staff_override=True):
      # User 'owns' the file
      if self.user == user:
        return True

      # User has access to the Library which contains the file
      if self.library.userHasPermissions(user, target_permissions, enable_staff_override):
        return True

      # Legacy, user has access to the Projects which contain the file
      for project in self.project_set.all():
        if project.userHasPermissions(user, target_permissions, enable_staff_override):
          return True

      return False

  ## Returns a list of Project objects in which this MediaFile is present.
  # @param self MediaFile object
  # @return True on success, False otherwise

  def getContainingProjects(self):
      return Project.objects.all().filter(mediaFiles__exact=self.id).distinct()


@python_2_unicode_compatible
class MediaFileMetaData(models.Model):
  class Meta:
    verbose_name = "MediaFile MetaData"

  mediaFile = models.ForeignKey(MediaFile)
  name = models.CharField(max_length=255)
  value = models.CharField(max_length=255, null=True)
  userEditable = models.BooleanField(verbose_name='userEditable', default=False)

  ## Checks that the user has the indicated permissions.
  def userHasPermissions(self, user, target_permissions, enable_staff_override=True):
    return self.mediaFile.userHasPermissions(user, target_permissions, enable_staff_override)

  def __str__(self):
      return '%s - %s' % (self.mediaFile.alias, self.name)

@python_2_unicode_compatible
class MediaFileUserMetaDataField(models.Model):
  name = models.CharField(max_length=255)
  library = models.ForeignKey(Library)
  description = models.CharField(max_length=255, blank=True)

  def __str__(self):
    return '%s' % (self.name)

  class Meta:
    unique_together = ('name', 'library')
    verbose_name = "MediaFile UserMetaData Field"


  ## Checks that the user has the indicated permissions.
  # Convenience wrapper around ARLO_PERMS.
  def userHasPermissions(self, user, target_permissions, enable_staff_override=True):

    if self.library.userHasPermissions(user, target_permissions, enable_staff_override):
      return True

    # get a list of all Projects which contain files from this Library
    libraryMediaFiles = MediaFile.objects.filter(library=self.library)
    projects = Project.objects.filter(mediaFiles__in=libraryMediaFiles.values('id')).distinct()

    # check each Project, and if the user has read on any, grant access
    for project in projects:
      if project.userHasPermissions(user, target_permissions):
        return True

    return False


@python_2_unicode_compatible
class MediaFileUserMetaData(models.Model):
  class Meta:
    verbose_name = "MediaFile UserMetaData"

  mediaFile = models.ForeignKey(MediaFile)
  metaDataField = models.ForeignKey(MediaFileUserMetaDataField)
  value = models.TextField(verbose_name='Value', default=None, null=True)

  def __str__(self):
    return '%s:%s' % (self.metaDataField.name, self.value)

  ## Checks that the user has the indicated permissions.
  # Convenience wrapper around ARLO_PERMS.
  def userHasPermissions(self, user, target_permissions, enable_staff_override=True):
    return self.mediaFile.userHasPermissions(user, target_permissions, enable_staff_override)


class ProjectManager(models.Manager):

  ## Return a list of all Projects to which a user has access.
  # Call as Project.objects.getUserProjects(user)
  # @param user A User Object
  # @return A Project QuerySet

  def getUserProjects(self, user):
    from django.contrib.auth.models import User
    user_type = ContentType.objects.get_for_model(User)
    groupContentType = ContentType.objects.get_for_model(ArloUserGroup)
    projectContentType = ContentType.objects.get_for_model(Project)
    userGroupIds = ArloUserGroup.objects.filter(users=user).values_list('id', flat=True)
    userGroupProjIds = ArloPermission.objects.filter(bearer_type=groupContentType, bearer_id__in=userGroupIds, target_type=projectContentType).values_list('target_id', flat=True)
    projects = super(ProjectManager, self).get_queryset().filter(
      models.Q(user=user) |
      models.Q(id__in=(ProjectPermissions.objects.filter(user=user, canRead=True).values_list('project__id', flat=True))) |
      models.Q(permissions__bearer_id=user.id, permissions__bearer_type=user_type) |
      models.Q(id__in=userGroupProjIds)
    ).distinct()

    return projects


@python_2_unicode_compatible
class Project(models.Model):
  objects = ProjectManager()  # override to user our custom manager

  user = models.ForeignKey('auth.User')
  name = models.CharField(verbose_name='Name', max_length=255)
  type = models.ForeignKey(ProjectTypes, null=False)
  mediaFiles = models.ManyToManyField('MediaFile')

  # don't add () to the end of now, else it is called once at model definition.
  # pass without () passes the function and is called each time data is inserted
  creationDate = models.DateTimeField(verbose_name='Creation Date', default=datetime.datetime.now, null=True)

  notes = models.TextField(verbose_name='Notes', default=None, null=True)
  default_permission_read = models.BooleanField(verbose_name='Default Read Permission?', default=False)
  default_permission_write = models.BooleanField(verbose_name='Default Write Permission?', default=False)
  default_permission_launch_job = models.BooleanField(verbose_name='Default Launch Job Permission?', default=False)
  default_permission_admin = models.BooleanField(verbose_name='Default Admin Permission?', default=False)
  permissions = GenericRelation(
                                ArloPermission,
                                content_type_field='target_type',
                                object_id_field='target_id')

  def __str__(self):
    return '%s' % (self.name)


  ## Checks that the user has the indicated permissions.
  # Convenience wrapper around ArloProjectPermissions.
  def userHasPermissions(self, user, target_permissions, enable_staff_override=True):
      # Runtime import to avoid circular-import issues with models
      from tools.views.userPermissions import ArloProjectPermissions
      perms = ArloProjectPermissions(self)
      return perms.userHasPermissions(user, target_permissions, enable_staff_override)

  ## Return a list of users and their permissions to the Project.
  # Note: Staff (admin) users are NOT enumerated here based on their staff
  #       status
  # Note: Default permissions on the Project are ignored
  # @return A dict of dicts, as {User: {'read': , 'write': 'launch_job':, 'admin':}, }
  #         User - auth.User Object
  def getProjectUsers(self):
      # Runtime import to avoid circular-import issues with models
      from tools.views.userPermissions import ArloProjectPermissions
      perms = ArloProjectPermissions(self)
      return perms.getProjectUsers()


  ## Retrieve a list of TagSets in a project, optionally filtering out the 'hidden' TagSets.
  # @param project Project object from which to retrieve the TagSets.
  # @param include_hidden (Default True) Whether to include 'hidden' TagSets in the result.
  # @return Django QuerySet of TagSet objects.

  def getTagSets(self, include_hidden=True):
    tagSets = TagSet.objects.filter(project=self)
    if not include_hidden:
      tagSets = tagSets.filter(hidden=False)
    tagSets = tagSets.order_by('name')
    return tagSets

  ## Get a list of TagClasses from Project
  # @param project The Project Object from which to get TagClasses
  # @return A list of TagClass Objects (as a Django QuerySet)

  def getTagClasses(self):
    return TagClass.objects.filter(project=self).order_by('className')

  ## Get a list of media files assigned to a Project.
  # @param project A Project object
  # @return A list of MediaFile objects

  def getMediaFiles(self):
    return self.mediaFiles.all()


@python_2_unicode_compatible
class ProjectPermissions(models.Model):
  user = models.ForeignKey('auth.User')
  project = models.ForeignKey(Project)
  canRead = models.BooleanField(default=False)
  canWrite = models.BooleanField(default=False)

  def __str__(self):
    return '[{}] {}'.format(self.project, self.user)

@python_2_unicode_compatible
class TagClass(models.Model):
  class Meta:
    verbose_name = "TagClass"
    verbose_name_plural = "TagClasses"

  user = models.ForeignKey('auth.User')
  project = models.ForeignKey(Project) # the Project to which this TagClass belongs

  className = models.CharField(max_length=255)
  displayName = models.CharField(max_length=255, null=True)

  def __str__(self):
    return '%s' % (self.className)

  ## Checks that the user has the indicated permissions.
  def userHasPermissions(self, user, target_permissions, enable_staff_override=True):
      return self.project.userHasPermissions(user, target_permissions, enable_staff_override)


@python_2_unicode_compatible
class TagSet(models.Model):
  class Meta:
    verbose_name = "TagSet"

  user = models.ForeignKey('auth.User')
  name = models.CharField(verbose_name='Name', max_length=255)

  project = models.ForeignKey(Project) # the Project to which this TagSet belongs

  creationDate = models.DateTimeField(verbose_name='Creation Date', default=datetime.datetime.now, null=True)
  notes = models.TextField(verbose_name='Notes', default=None, null=True)
  hidden = models.BooleanField(verbose_name="Hidden", default=False)

  permissions = GenericRelation(
                                ArloPermission,
                                content_type_field='target_type',
                                object_id_field='target_id')

  def __str__(self):
    return '%s' % (self.name)

  ## Checks that the user has the indicated permissions.
  # Convenience wrapper around ArloTagSetPermissions.
  def userHasPermissions(self, user, target_permissions, enable_staff_override=True):
      # Runtime import to avoid circular-import issues with models
      from tools.views.userPermissions import ArloTagSetPermissions
      perms = ArloTagSetPermissions(self)
      return perms.userHasPermissions(user, target_permissions, enable_staff_override)


###########################################################
#                                                         #
#          IMPORTANT NOTICE!  AVISO IMPORTANTE!           #
#                                                         #
#  TagExample is being removed - we're keeping the model  #
#  around for now until we can be sure the migrations     #
#  have ran on all clients, then we'll remove it. Note    #
#  all data will be getting removed from this table in    #
#  current migrations.                                    #
#                                                         #
#                          TODO                           #
#                                                         #
###########################################################

@python_2_unicode_compatible
class TagExample(models.Model):
  user = models.ForeignKey('auth.User')
  tagSet = models.ForeignKey(TagSet)
  mediaFile = models.ForeignKey(MediaFile)
  creationDate = models.DateTimeField(verbose_name='Creation Date', default=datetime.datetime.now, null=True)
  startTime = models.FloatField(verbose_name='startTime')
  endTime = models.FloatField(verbose_name='endTime')
  minFrequency = models.FloatField(verbose_name='minFrequency')
  maxFrequency = models.FloatField(verbose_name='maxFrequency')

  def __str__(self):
    return '{}'.format(self.pk)

@python_2_unicode_compatible
class Tag(models.Model):
  class Meta:
    index_together = [
      ['tagSet', 'tagClass'],
    ]

  user = models.ForeignKey('auth.User')

  # don't add () to the end of now, else it is called once at model definition.
  # pass without () passes the function and is called each time data is inserted
  creationDate = models.DateTimeField(verbose_name='Creation Date', default=datetime.datetime.now, null=True)

  tagExample = models.ForeignKey(TagExample, null=True, default=None)

  # Old TagExample Fields
  tagSet = models.ForeignKey(TagSet)
  mediaFile = models.ForeignKey(MediaFile)
  startTime = models.FloatField(verbose_name='startTime')
  endTime = models.FloatField(verbose_name='endTime')
  minFrequency = models.FloatField(verbose_name='minFrequency')
  maxFrequency = models.FloatField(verbose_name='maxFrequency')

  tagClass = models.ForeignKey(TagClass)
  parentTag = models.ForeignKey("Tag", null=True, blank=True)
  randomlyChosen = models.BooleanField(verbose_name='randomlyChosen', default=False)
  machineTagged = models.BooleanField(verbose_name='machineTagged', default=None)
  userTagged = models.BooleanField(verbose_name='userTagged', default=None)

  strength = models.FloatField(verbose_name='strength')

  def __str__(self):
    return 'Tag ' + '%s' % (self.tagClass.className)

  ## Checks that the user has the indicated permissions.
  # Convenience wrapper around ARLO_PERMS.
  def userHasPermissions(self, user, target_permissions, enable_staff_override=True):
      return self.tagSet.userHasPermissions(user, target_permissions, enable_staff_override)


@python_2_unicode_compatible
class JobTypes(models.Model):
  class Meta:
    verbose_name = "Job Type"

  name = models.CharField(verbose_name='Name', max_length=255)
  def __str__(self):
    return '%s' % (self.name)

# Default data is automatically loaded via migrations
#+----+-------------------------------+
#| id | name                          |
#+----+-------------------------------+
#|  1 | SupervisedTagDiscovery        |
#|  2 | RandomWindowTagging           |
#|  3 | TagAnalysis                   |
#|  4 | AdaptAnalysis                 |
#|  5 | Transcription                 |
#|  6 | UnknownTagDiscovery           |
#|  7 | UnsupervisedTagDiscovery      |
#|  8 | Test                          |
#|  9 | ImportAudioFile               |
#| 10 | SupervisedTagDiscoveryParent  |
#| 11 | SupervisedTagDiscoveryChild   |
#| 12 | ExpertAgreementClassification |
#| 13 | WekaJobWrapper                |
#| 14 | LibraryMediaFileValidation    |
#| 15 | JobPlugin                     |
#+----+-------------------------------+


@python_2_unicode_compatible
class JobStatusTypes(models.Model):
  class Meta:
    verbose_name = "Job Status Type"

  name = models.CharField(verbose_name='Name', max_length=255)
  def __str__(self):
    return '%s' % (self.name)

# Default data is automatically loaded via migrations
#+----+--------------------------+
#| id | name                     |
#+----+--------------------------+
#|  1 | Unknown                  |
#|  2 | Queued                   |
#|  3 | Running                  |
#|  4 | Error                    |
#|  5 | Stopped                  |
#|  6 | Complete                 |
#|  7 | Assigned                 |
#+----+--------------------------+


@python_2_unicode_compatible
class Jobs(models.Model):
  class Meta:
    verbose_name = "Job"

  name = models.CharField(max_length=255)

  user = models.ForeignKey('auth.User')
  project = models.ForeignKey('Project',null=True)
  type = models.ForeignKey(JobTypes, null=False)

  creationDate = models.DateTimeField(verbose_name='Creation Date', default=datetime.datetime.now)

  numToComplete = models.IntegerField(verbose_name='numToComplete', default=0)
  numCompleted = models.IntegerField(verbose_name='numCompleted', default=0)
  fractionCompleted = models.FloatField(verbose_name='fractionCompleted', default=0)

  elapsedRealTime = models.FloatField(verbose_name='elapsedRealTime', default=0.0)

  timeToCompletion = models.FloatField(verbose_name='timeToCompletion', default= -1)

  isRunning = models.BooleanField(verbose_name="isRunning", default=False)
  wasStopped = models.BooleanField(verbose_name="wasStopped", default=False)
  isComplete = models.BooleanField(verbose_name="isComplete", default=False)
  wasDeleted = models.BooleanField(verbose_name="wasDeleted", default=False)

  # override related_name since we have two FKs to the same model
  # see  http://stackoverflow.com/questions/1142378/django-why-some-fields-clashes-with-other
  status          = models.ForeignKey('JobStatusTypes', null=True, blank=True, default=None, related_name='jobs_statuses')
  requestedStatus = models.ForeignKey('JobStatusTypes', null=True, blank=True, default=None, related_name='jobs_requestStatuses')

  startedDate    = models.DateTimeField(verbose_name='Started Date',     null=True, blank=True, default=None)     # when the Job switched to Running
  lastStatusDate = models.DateTimeField(verbose_name='Last Status Date', null=True, blank=True, default=None) # the last change to the status, or when Java last checked in
  parentJob = models.ForeignKey("Jobs", null=True, blank=True, default=None)
  priority = models.IntegerField(default=0, null=False, blank=True)

  def __str__(self):
      return 'Job %s' % self.id

  ## Checks that the user has the indicated permissions.
  def userHasPermissions(self, user, target_permissions, enable_staff_override=True):
    if self.project is None:
      return False
    return self.project.userHasPermissions(user, target_permissions, enable_staff_override)


@python_2_unicode_compatible
class JobParameters(models.Model):
  class Meta:
    verbose_name = "Job Parameter"

  job = models.ForeignKey('Jobs')
  name = models.CharField(max_length=255)
  value = models.CharField(max_length=255)

  ## Checks that the user has the indicated permissions.
  # Convenience wrapper around ARLO_PERMS.
  def userHasPermissions(self, user, target_permissions, enable_staff_override=True):
    return self.job.project.userHasPermissions(user, target_permissions, enable_staff_override)

  def __str__(self):
      return '[{}] {}'.format(self.job, self.name)


@python_2_unicode_compatible
class JobLog(models.Model):
  """
  Stores Logs from Job processing for user review.
  """
  class Meta:
    verbose_name = "Job Log"

  job = models.ForeignKey('Jobs')
  messageDate = models.DateTimeField(verbose_name='Message Date', null=False, default=datetime.datetime.now)
  message = models.TextField(verbose_name='Log Message', default=None, null=True)

  def __str__(self):
    return '%s' % self.job

  ## Checks that the user has the indicated permissions.
  def userHasPermissions(self, user, target_permissions, enable_staff_override=True):
    return self.job.userHasPermissions(user, target_permissions, enable_staff_override)


@python_2_unicode_compatible
class JobResultFile(models.Model):
  class Meta:
    verbose_name = "Job Result File"

  job = models.ForeignKey('Jobs')
  creationDate = models.DateTimeField(verbose_name='File Creation Date', null=False, default=datetime.datetime.now)
  resultFile = models.FileField(upload_to=nesterSettings.getJobResultFileUploadFilePath, max_length=255,null=True)
  notes = models.TextField(verbose_name='Notes', null=True)

  def __str__(self):
    return '%s - %s' % (self.job, self.resultFile)

  ## Checks that the user has the indicated permissions.
  def userHasPermissions(self, user, target_permissions, enable_staff_override=True):
    return self.job.userHasPermissions(user, target_permissions, enable_staff_override)


@python_2_unicode_compatible
class RandomWindow(models.Model):
  class Meta:
    verbose_name = "RandomWindow"

  randomWindowTaggingJob = models.ForeignKey(Jobs)
  mediaFile = models.ForeignKey(MediaFile)
  startTime = models.FloatField(verbose_name='startTime')
  endTime = models.FloatField(verbose_name='endTime')

  ## Checks that the user has the indicated permissions.
  # Convenience wrapper around ARLO_PERMS.
  def userHasPermissions(self, user, target_permissions, enable_staff_override=True):
    return self.randomWindowTaggingJob.userHasPermissions(user, target_permissions, enable_staff_override)

  def __str__(self):
    return '[{}] {}'.format(self.randomWindowTaggingJob, self.pk)


@python_2_unicode_compatible
class UserSettings(models.Model):
  class Meta:
    verbose_name = "UserSettings"
    verbose_name_plural = "UserSettings"

  user = models.ForeignKey('auth.User')
  name = models.CharField(verbose_name='name', default="default", max_length=255)

  windowSizeInSeconds = models.FloatField(verbose_name='windowSizeInSeconds', default=4.0)
  spectraMinimumBandFrequency = models.FloatField(verbose_name='spectraMinimumBandFrequency', default=20.0)
  spectraMaximumBandFrequency = models.FloatField(verbose_name='spectraMaximumBandFrequency', default=20000.0)
  spectraDampingFactor = models.FloatField(verbose_name='spectraDampingFactor', default=0.02)
  spectraNumFrequencyBands = models.IntegerField(verbose_name='spectraNumFrequencyBands', default=256)
  spectraNumFramesPerSecond = models.FloatField(verbose_name='spectraNumFramesPerSecond', default=128)

  showSpectra = models.NullBooleanField(verbose_name='showSpectra', null=True, default=True)
  showWaveform = models.NullBooleanField(verbose_name='showWaveform', null=True, default=True)

  loadAudio = models.NullBooleanField(verbose_name='loadAudio', null=True, default=True)

  maxNumAudioFilesToList = models.IntegerField(verbose_name='maxNumAudioFilesToList', default=100)

#  for file view
  fileViewWindowSizeInSeconds = models.FloatField(verbose_name='fileViewWindowSizeInSeconds', default=4.0)
  fileViewSpectraMinimumBandFrequency = models.FloatField(verbose_name='fileViewSpectraMinimumBandFrequency', default=20.0)
  fileViewSpectraMaximumBandFrequency = models.FloatField(verbose_name='fileViewSpectraMaximumBandFrequency', default=20000.0)
  fileViewSpectraDampingFactor = models.FloatField(verbose_name='fileViewSpectraDampingFactor', default=0.02)
  fileViewSpectraNumFrequencyBands = models.IntegerField(verbose_name='fileViewSpectraNumFrequencyBands', default=256)
  fileViewSpectraNumFramesPerSecond = models.FloatField(verbose_name='fileViewSpectraNumFramesPerSecond', default=128)

#  for catalog view
  catalogViewWindowSizeInSeconds = models.FloatField(verbose_name='catalogViewWindowSizeInSeconds', default=4.0)
  catalogViewSpectraMinimumBandFrequency = models.FloatField(verbose_name='catalogViewSpectraMinimumBandFrequency', default=20.0)
  catalogViewSpectraMaximumBandFrequency = models.FloatField(verbose_name='catalogViewSpectraMaximumBandFrequency', default=20000.0)
  catalogViewSpectraDampingFactor = models.FloatField(verbose_name='catalogViewSpectraDampingFactor', default=0.02)
  catalogViewSpectraNumFrequencyBands = models.IntegerField(verbose_name='catalogViewSpectraNumFrequencyBands', default=64)
  catalogViewSpectraNumFramesPerSecond = models.FloatField(verbose_name='catalogViewSpectraNumFramesPerSecond', default=100)

#  for tag view
  tagViewWindowSizeInSeconds = models.FloatField(verbose_name='tagViewWindowSizeInSeconds', default=4.0)
  tagViewSpectraMinimumBandFrequency = models.FloatField(verbose_name='tagViewSpectraMinimumBandFrequency', default=20.0)
  tagViewSpectraMaximumBandFrequency = models.FloatField(verbose_name='tagViewSpectraMaximumBandFrequency', default=20000.0)
  tagViewSpectraDampingFactor = models.FloatField(verbose_name='tagViewSpectraDampingFactor', default=0.02)
  tagViewSpectraNumFrequencyBands = models.IntegerField(verbose_name='tagViewSpectraNumFrequencyBands', default=256)
  tagViewSpectraNumFramesPerSecond = models.FloatField(verbose_name='tagViewSpectraNumFramesPerSecond', default=128)

  def __str__(self):
    return '%s - %s' % (self.user, self.name)


@python_2_unicode_compatible
class ExtendedUserSettings(models.Model):
  class Meta:
    verbose_name = "Extended UserSetting"
    verbose_name_plural = "Extended UserSettings"

  name  = models.CharField(max_length=255)
  default = models.CharField(max_length=255, null=True)

  def __str__(self):
    return '%s' % self.name


@python_2_unicode_compatible
class ExtendedUserSettingsValues(models.Model):
  class Meta:
    verbose_name = "Extended UserSetting Value"
    verbose_name_plural = "Extended UserSetting Values"

  userSettings = models.ForeignKey('UserSettings')
  setting = models.ForeignKey('ExtendedUserSettings')
  value = models.CharField(max_length=255, null=True)

  def __str__(self):
    return '%s - %s' % (self.userSettings.user, self.setting.name)


###########################################################
#                                                         #
#                       Forms                             #
#                                                         #
###########################################################


class DeleteUserForm(forms.Form):
  username = forms.CharField(max_length=64)

class LoginForm(forms.Form):
  username = forms.CharField(max_length=64)
  password = forms.CharField(max_length=64)


class VisualizeAudioForm(forms.Form):
  alias = forms.CharField(max_length=1024, widget=forms.TextInput(attrs={'readonly':'1'}))
  mediaFileId = forms.IntegerField(required=True, widget=forms.TextInput(attrs={'readonly':'1'}))
  startTime = forms.FloatField(label='startTime', initial=0.0)
  endTime = forms.FloatField(label='endTime', initial=16.0)
  gain = forms.FloatField(label='Gain', initial=1.0)
  spectraNumFramesPerSecond = forms.FloatField(label='Num Frames Per Second', initial=64)
  spectraNumFrequencyBands = forms.IntegerField(label='Num Frequency Bands', initial=256)
  spectraDampingFactor = forms.FloatField(label='Damping Factor', initial=0.02)
  spectraMinimumBandFrequency = forms.FloatField(label='Minimum Band Frequency', initial=20.0)
  spectraMaximumBandFrequency = forms.FloatField(label='Maximum Band Frequency', initial=20000.0)
  loadAudio = forms.BooleanField(label='loadAudio?', required=False, initial=True)
  tagSets = forms.ModelMultipleChoiceField(TagSet.objects.all(), widget=forms.CheckboxSelectMultiple, required=False)
  contextWindow = forms.FloatField(label='Context Window (seconds)', initial=0, required=False)


class VisualizeTagSetForm(forms.Form):
  gain = forms.FloatField('gain', initial=1.0)
  tagSetName = forms.CharField(max_length=255)
  tagNumber = forms.IntegerField('tagNumber', initial=1)
  spectraNumFramesPerSecond = forms.FloatField('spectraNumFramesPerSecond', initial=64)
  spectraNumFrequencyBands = forms.IntegerField('spectraNumFrequencyBands', initial=256)
  spectraDampingFactor = forms.FloatField('spectraDampingFactor', initial=0.02)
  spectraMinimumBandFrequency = forms.FloatField('spectraMinimumBandFrequency', initial=20.0)
  spectraMaximumBandFrequency = forms.FloatField('spectraMaximumBandFrequency', initial=20000.0)


class UploadTarFileForm(forms.Form):
  file = forms.FileField(label='File Name')


class UploadLocalDirectory(forms.Form):
  directoryPath = forms.CharField(max_length=200, required=True)
  projectName = forms.CharField(max_length=200, required=True)


class CreateTagForm(forms.Form):
  tagSet = forms.ModelChoiceField(TagSet.objects.all(), required=True)
  className = forms.CharField(max_length=200, required=True)
  displayName = forms.CharField(max_length=200, required=False)
  strength = forms.FloatField('strength', initial=1.0)
  tagStartTime = forms.FloatField('tagStartTime', initial=0.0)
  tagEndTime = forms.FloatField('tagEndTime', initial=1.0)
  tagMinFrequency = forms.FloatField('tagMinFrequency', initial=60.0)
  tagMaxFrequency = forms.FloatField('tagMaxFrequency', initial=100.0)

# NEW CreateTagForm for use with MultiView
# difference is that audioFileId is passed with POST in form rather than URL
# probably eventually migrate all uses from existing CreateTagForm to this new usage
# also removed tagMediaFilePath

class NewCreateTagForm(forms.Form):
  tagSet = forms.ModelChoiceField(TagSet.objects.all(), required=True)
  className = forms.CharField(max_length=200, required=True)
  displayName = forms.CharField(max_length=200, required=False)
  strength = forms.FloatField('strength', initial=1.0)
  tagStartTime = forms.FloatField('tagStartTime', initial=0.0)
  tagEndTime = forms.FloatField('tagEndTime', initial=1.0)
  tagMinFrequency = forms.FloatField('tagMinFrequency', initial=60.0)
  tagMaxFrequency = forms.FloatField('tagMaxFrequency', initial=100.0)
  audioFileId = forms.IntegerField(required=True, widget=forms.TextInput(attrs={'readonly':'1'}))

class NavigateTagClassForm(forms.Form):
  navigateTagClass = forms.ModelChoiceField(TagClass.objects.all(), required=False)
  currentTagID = forms.IntegerField('currentTagID', widget=forms.HiddenInput, required=False)
  navStartTime = forms.FloatField('navStartTime', widget=forms.HiddenInput, required=False)
  navEndTime = forms.FloatField('navEndTime', widget=forms.HiddenInput, required=False)


class UnknownTagDiscoveryForm(forms.Form):
  name = forms.CharField(label='Name', max_length=255, initial='default')
  numberOfTagsToDiscover = forms.IntegerField(label='Number Of Tags To Discover', initial=100)
  searchWindowSizeInSeconds = forms.FloatField(label='Window Search Size In Seconds', initial=60.0 * 10.0)
#  REPRESENTATION BIAS
  numFrequencyBands = forms.IntegerField(label='Num Frequency Bands', initial=32)
  numTimeFramesPerSecond = forms.FloatField(label='Num Time Frames Per Second', initial=32.0)
  dampingRatio = forms.FloatField(label='Damping Ratio', initial=0.02)
  minFrequency = forms.FloatField(label='Min Frequency', initial=400.0)
  maxFrequency = forms.FloatField(label='Max Frequency', initial=8000.0)
# SELECTION BIAS
  entropyThreshold = forms.FloatField(label='entropyThreshold', initial=0.5)


class TagAnalysisForm(forms.Form):
  name = forms.CharField(label='Name', max_length=255, initial='default')
  freqAveragingWindowWidth = forms.IntegerField(label='Freq Averaging Window Width', initial=4)
  numFramesToAverageNoise = forms.IntegerField(label='Num Frames To Average Noise', initial=20)
  updateTagPositions = forms.BooleanField(label='updateTagPositions', initial=False, required=False)

  numFrequencyBands = forms.IntegerField(label='Num Frequency Bands', initial=256)
  numTimeFramesPerSecond = forms.FloatField(label='Num Time Frames Per Second', initial=250.0)
  dampingRatio = forms.FloatField(label='Damping Ratio', initial=0.002)
  minFrequency = forms.FloatField(label='Min Frequency', initial=2000.0)
  maxFrequency = forms.FloatField(label='Max Frequency', initial=3500.0)


class TranscriptForm(forms.Form):
#  REPRESENTATION BIAS
  numFrequencyBands = forms.IntegerField(label='Num Frequency Bands', initial=64)
  numTimeFramesPerSecond = forms.FloatField(label='Num Time Frames Per Second', initial=200.0)
  dampingRatio = forms.FloatField(label='Damping Ratio', initial=0.02)
  minFrequency = forms.FloatField(label='Min Frequency', initial=400.0)
  maxFrequency = forms.FloatField(label='Max Frequency', initial=8000.0)
  spectraWeight = forms.FloatField(label='Spectra Weight', initial=0.0)
  pitchWeight = forms.FloatField(label='Pitch Weight', initial=1.0)
  pitchEnergyWeight = forms.FloatField(label='Pitch Energy Weight', initial=0.0)
  averageEnergyWeight = forms.FloatField(label='Average Energy Weight', initial=0.0)
# SELECTION BIAS
  numRandomProbes = forms.FloatField(label='numRandomProbes', initial=1000000)
  minPerformance = forms.FloatField(label='minPerformance', initial=0.95)


#class NewCatalogForm(forms.Form):
#  numFramesPerSecond = forms.FloatField('numFramesPerSecond', initial=128.0)
#  numFrequencyBands = forms.IntegerField('numFrequencyBands', initial=64)
#  spectraDampingFactor = forms.FloatField('spectraDampingFactor', initial=0.02)
#
#  userTagsPerPage = forms.IntegerField(label='userTagsPerPage', initial=5)
#  machineTagsPerPage = forms.IntegerField(label='machineTagsPerPage', initial=5)
#
#  sortBy = forms.ChoiceField(label='sortBy', choices=[('cron', 'Chronological Order'), ('strength', 'Strength')])
#
#  showUserTags = forms.BooleanField(label='Show User Tags', required=False, initial=True)
#  showMachineTags = forms.BooleanField(label='Show Machine Tags', required=False, initial=True)
#
#  tagClasses = forms.ModelMultipleChoiceField(TagClass.objects.none(), widget=forms.CheckboxSelectMultiple, required=False)


class RenameTagClassForm(forms.Form):
  newClassName = forms.CharField(label='New Class Name', max_length=255)
  newDisplayName = forms.CharField(label='New Display Name', max_length=255, required=False)


class MergeTagClassesForm(forms.Form):
  tagClassesToMerge = forms.ModelMultipleChoiceField(TagClass.objects.none(), widget=forms.CheckboxSelectMultiple)
  newClassName = forms.CharField(label='New Class Name', max_length=255)
  newDisplayName = forms.CharField(label='New Display Name', max_length=255, required=False)


class UploadSoftTagSetForm(forms.Form):
    file = forms.FileField(label='File Name')
