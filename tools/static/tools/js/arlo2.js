$( document ).ready(function() {

  ///////////////////
  // Random Window Tagging
  ///////////////////

  $('.quickTagButtons button').click(function() {
    $('#id_className').val($(this).attr('data-tagClassName'));
    tagCreatedWithQuickTag = true;
    $('#createTagForm').submit();
  })

  ///////////////////
  // Media Files List
  ///////////////////

  ////// GENERAL FUNCTIONS /////

  var showWithSelected = function() {
    $('#withSelected').removeClass('hidden');
  }

  var hideWithSelected = function() {
    $('#withSelected').addClass('hidden');
    $('#selectAllPageFiles span').text("Select");
  }

  var selectNone = function() {
    $('.panel.audio-files input[type="checkbox"]').prop( "checked", false );
    $('#addSelectedFilesToProject, #removeSelectedFilesFromProject, #deleteSelectedFiles').removeClass('allPageSelected').removeClass('allSelected');;
    $('#selectAllFiles, #clearSelectAllFiles').addClass("hidden");
    hideWithSelected();
  }

  var selectAll = function() {
    $('#selectAllPageFiles span').text("Deselect");
    $('.panel.audio-files input[type="checkbox"]').prop( "checked", true );
    $('#addSelectedFilesToProject, #removeSelectedFilesFromProject, #deleteSelectedFiles').addClass('allPageSelected');
    $('#selectAllFiles .numFilesOnPage').text($('.panel.audio-files input[type="checkbox"]').length);
    if (Number($('#selectAllFiles .numFilesOnPage').text()) < Number($('#selectAllFiles .numFilesTotal').text())) {
      $('#selectAllFiles').removeClass("hidden");
    }
    showWithSelected();
  }

  var selectNotAll = function() {
    $('#selectAllPageFiles span').text("Select");
    $('#addSelectedFilesToProject, #removeSelectedFilesFromProject, #deleteSelectedFiles').removeClass('allPageSelected').removeClass('allSelected');
    $('#selectAllFiles, #clearSelectAllFiles').addClass("hidden");
  }



  ///// PREVIEW AUDIO //////
  $('.panel.audio-files .audioPreview a').hover(function() {
    var audioObject = $(this).find('audio')[0];
    audioObject.currentTime = 2;
    audioObject.play();
  }, function() {
    var audioObject = $(this).find('audio')[0];
    audioObject.currentTime = 0;
    audioObject.pause();
  })

  ///// REMOVE AUDIO FROM A PROJECT //////
  $('.panel.audio-files tr .removeProject').on('click', function() {

    var mediaFileID = $(this).parent().data('mediafile');
    var projectID = $(this).parent().data('project');

    $.ajax({
      url: '/tools/AJAXRemoveAudioFromProject/',
      data: {
        'mediaFileID': mediaFileID,
        'projectID': projectID,
      },
      dataType: 'json',
      success: function (data) {
        console.log(data)
        $('.panel.audio-files tr .project[data-project=' + data.projectID + '][data-mediafile=' + data.mediaFileID + ']').remove();
      }
    });

    return false;

  })

  ///// LISTENER FOR SELECT BOXES ///////
  $('.panel.audio-files input[type="checkbox"]').bind('change', function() {

    if ($('.panel.audio-files input[type="checkbox"]:checked').length == 0) {
      hideWithSelected();
    } else if ($('.panel.audio-files input[type="checkbox"]:checked').length == $('.panel.audio-files input[type="checkbox"]').length) {
      selectAll();
    } else {
      selectNotAll();
      showWithSelected();
    }

  })

  ///// ADD MULTIPLE FILES TO A PROJECT //////

  // Button to be ModelChoiceField
  var projectButton = "<div class='btn-group project' data-project='' data-mediafile=''><a href='#' class='btn btn-success btn-xs'></a><a href='#' class='removeProject btn btn-danger btn-xs'>x</a></div>";

  // Button to select all files on page
  $('#selectAllPageFiles').click(function() {

    if ($('#addSelectedFilesToProject').hasClass('allPageSelected') || $('#removeSelectedFilesFromProject').hasClass('allPageSelected') || $('#addSelectedFilesToProject').hasClass('allSelected') || $('#removeSelectedFilesFromProject').hasClass('allSelected') || $('#deleteSelectedFiles#deleteSelectedFiles').hasClass('allSelected') || $('#deleteSelectedFiles').hasClass('allSelected')) {
      //If all already selected
      selectNone();
      hideWithSelected();
      return false;
    } else {
      //Else select all files
      selectAll();
      showWithSelected();
      return false;
    }

  }) //selectAllFiles

  // Button to select all files including on unseen pages
  $('#selectAllFiles a').bind('click', function() {

      // Hide the 'select all files...' text
      $(this).parent().addClass('hidden');

      //Show the 'clear selection' text
      $('#clearSelectAllFiles').removeClass('hidden');

      //Select all the checkboxes, setup state
      $('.panel.audio-files input[type="checkbox"]').prop( "checked", true );
      $('#addSelectedFilesToProject, #removeSelectedFilesFromProject, #deleteSelectedFiles').addClass('allSelected');

      return false;

  })

  // Button to clear selection
  $('#clearSelectAllFiles a').bind('click', function() {

      // Hide this text
      $(this).parent().addClass('hidden');
      selectNone()
      return false;

  })

  // Modal for deleting a sensor array
  $('#modalDeleteSensorArray').on('show.bs.modal', function (event) {

    var button = $(event.relatedTarget) // Button that triggered the modal
    var modal = $(this)

    modal.find('.modal-title span').text(button.attr('data-name'))
    modal.find('a.submit').attr('data-id',button.attr('data-id'))

  })

  // Modal for adding selected files to a project
  $('#modalAddSelectedFilesToProject').on('show.bs.modal', function (event) {

    var numFiles = 0;

    if ($('#addSelectedFilesToProject').hasClass('allSelected')) {
      numFiles = $('.panel.audio-files').data('total-files')
    } else {
      numFiles = $('.panel.audio-files input[type="checkbox"]:checked').length;
    }

    var fileNames = $('.panel.audio-files tbody tr:has(input[type="checkbox"]:checked) td.name').text()

    var button = $(event.relatedTarget) // Button that triggered the modal
    var modal = $(this)
    modal.find('.modal-title span').text(numFiles)
    modal.find('ul li').remove();
    $('.panel.audio-files tbody tr:has(input[type="checkbox"]:checked) td.name').each(function(value) {
      modal.find('ul').append('<li>' + $(this).text() + '</li>');
    })
    var total = modal.find('ul li').length;
    if (total > 5) {
      modal.find('ul li:gt(5)').remove();
      var more = Number(total) - Number(5)
      modal.find('ul').append('<li class="text-success">and ' + more + ' more...</li>');
    }

  })

  // Modal for removing selected files from a project
  $('#modalRemoveSelectedFilesFromProject').on('show.bs.modal', function (event) {

    var numFiles = 0;

    if ($('#removeSelectedFilesFromProject').hasClass('allSelected')) {
      numFiles = $('.panel.audio-files').data('total-files')
    } else {
      numFiles = $('.panel.audio-files input[type="checkbox"]:checked').length;
    }

    var button = $(event.relatedTarget) // Button that triggered the modal
    var modal = $(this)
    modal.find('.modal-title span').text(numFiles)

    modal.find('ul li').remove();
    $('.panel.audio-files tbody tr:has(input[type="checkbox"]:checked) td.name').each(function(value) {
      modal.find('ul').append('<li>' + $(this).text() + '</li>');
    })

    var total = modal.find('ul li').length;
    if (total > 5) {
      modal.find('ul li:gt(5)').remove();
      var more = numFiles - Number(5)
      modal.find('ul').append('<li class="text-success">and ' + more + ' more...</li>');
    }

    //if removing all files on all pages
    if ($('#removeSelectedFilesFromProject').hasClass('allSelected')) {
      var mediaFiles = $('#removeSelectedFilesFromProject').data('allids')
    } else {
      var mediaFiles = [];
      $('.panel.audio-files input[type="checkbox"]:checked').each(function(){
        mediaFiles.push($(this).attr('value'));
      });
      mediaFiles = mediaFiles.join("//");
    }

    var original_href = $('#modalRemoveSelectedFilesFromProject a').attr('href');
    $('#modalRemoveSelectedFilesFromProject a').attr('href', original_href + '&removeMediaFiles=' + mediaFiles);

  })

  // Modal for deleting selected files
  $('#modalDeleteSelectedFiles').on('show.bs.modal', function (event) {

    var numFiles = 0;

    if ($('#deleteSelectedFiles').hasClass('allSelected')) {
      numFiles = $('.panel.audio-files').data('total-files')
    } else {
      numFiles = $('.panel.audio-files input[type="checkbox"]:checked').length;
    }

    var button = $(event.relatedTarget) // Button that triggered the modal
    var modal = $(this)
    modal.find('.modal-title span').text(numFiles)

    modal.find('ul li').remove();
    $('.panel.audio-files tbody tr:has(input[type="checkbox"]:checked) td.name').each(function(value) {
      modal.find('ul').append('<li>' + $(this).text() + '</li>');
    })

    var total = modal.find('ul li').length;
    if (total > 5) {
      modal.find('ul li:gt(5)').remove();
      var more = numFiles - Number(5)
      modal.find('ul').append('<li class="text-success">and ' + more + ' more...</li>');
    }

    //if removing all files on all pages
    if ($('#removeSelectedFilesFromProject').hasClass('allSelected')) {
      var mediaFiles = $('#removeSelectedFilesFromProject').data('allids')
    } else {
      var mediaFiles = [];
      $('.panel.audio-files input[type="checkbox"]:checked').each(function(){
        mediaFiles.push($(this).attr('value'));
      });
      mediaFiles = mediaFiles.join("//");
    }

    $('#modalDeleteSelectedFiles a').attr('data-files', mediaFiles);

  })

  // Modal for deleting selected projects
  $('#modalDeleteSelectedProjects').on('show.bs.modal', function (event) {

    var numProjects = $('.panel.projects input[type="checkbox"]:checked').length;

    var button = $(event.relatedTarget) // Button that triggered the modal
    var modal = $(this)
    modal.find('.modal-title span').text(numProjects)

    var projects = [];
    $('.panel.projects input[type="checkbox"]:checked').each(function(){
      projects.push($(this).attr('value'));
    });
    projects = projects.join("//");

    modal.find('a.submit').attr('data-projectIDs',projects)

  })

  // Modal for editing user project permissions
  $('#modalEditUserProjectPermissions').on('show.bs.modal', function (event) {

    var button = $(event.relatedTarget) // Button that triggered the modal
    var modal = $(this)
    modal.find('.modal-title span.user').text(button.attr('data-username'));

    if (button.attr('data-read')) {
      modal.find('#read').prop('checked', true)
    }
    if (button.attr('data-write')) {
      modal.find('#write').prop('checked', true)
    }
    if (button.attr('data-launch')) {
      modal.find('#launch_job').prop('checked', true)
    }
    if (button.attr('data-admin')) {
      modal.find('#admin').prop('checked', true)
    }

  })


  ///////////////////////////////
  ///////////////////////////////
  // Submits for modals
  ///////////////////////////////
  ///////////////////////////////


  //Edit user project permissions
  $('#modalEditUserProjectPermissions a.submit').bind('click', function() {

    editDefaultPermissions(project_id, has_read, has_write, has_launch_job, has_admin);

  })

  // Delete sensor array submit
  $('#modalDeleteSensorArray a.submit').bind('click', function() {

    $.ajax({
      url: '/tools/deleteSensorArray/',
      data: {
        'id': $(this).attr('data-id')
      },
      dataType: 'json',
      success: function (data) {

        $('.sensor-arrays li[data-id = "' + data.id + '"]').remove();

        $('#modalDeleteSensorArray').modal('hide');
      }
    });

  })

  // Delete selected projects submit
  $('#modalDeleteSelectedProjects a.submit').bind('click', function() {

    projects = $(this).attr('data-projectIDs');

    $.ajax({
      url: '/tools/deleteMultipleProjects/',
      data: {
        'projects': projects
      },
      dataType: 'json',
      success: function (data) {
        for (i = 0; i < data.projects.length; i++) {
          $('tr[data-project = "' + data.projects[i] + '"]').remove();
        }
        selectNone();
        $('#modalDeleteSelectedProjects').modal('hide');
      }
    });

  })

  // Add selected files to project submit
  $('#modalAddSelectedFilesToProject button.submit').bind('click', function() {

    //if adding all files on all pages
    if ($('#addSelectedFilesToProject').hasClass('allSelected')) {
      var mediaFiles = $('#addSelectedFilesToProject').data('allids')
    } else {
      var mediaFiles = [];
      $('.panel.audio-files input[type="checkbox"]:checked').each(function(){
        mediaFiles.push($(this).attr('value'))
      });
      mediaFiles = mediaFiles.join("//")
    }

    $.ajax({
      url: '/tools/addMultipleAudioToProject/',
      data: {
        'mediaFiles': mediaFiles,
        'projectID': $('#selectProject option:selected').attr('value'),
      },
      dataType: 'json',
      success: function (data) {
        var projectID = data.projectID
        var projectName = data.projectName
        for (i = 0; i < data.mediaFiles.length; i++) {
          var newButton = $(projectButton)
          if ($('tr[data-mediafile = "' + data.mediaFiles[i] + '"] div.project[data-project = "' + projectID + '"]').length < 1) {
            $('tr[data-mediafile="' + data.mediaFiles[i] + '"] td.projects').prepend(newButton)
            newButton.attr('data-project',projectID).attr('data-mediafile',data.mediaFiles[i]).children('.btn-success').attr('href', '/tools/projectFiles/' + projectID).text(projectName)
          }
        }
        $('#modalAddSelectedFilesToProject').modal('hide')
        selectNone();
      }
    });

  })

  // Delete selected files
	$('#modalDeleteSelectedFiles a').bind('click', function() {

		files = $(this).attr('data-files');

		$.ajax({
			url: '/tools/deleteMediaFile/',
			data: {
				'files': files
			},
			dataType: 'json',
			method: 'POST',
			success: function (data) {
				for (i = 0; i < data.files.length; i++) {
					$('tr[data-mediafile = "' + data.files[i] + '"]').remove();
				}
				selectNone();
				$('#modalDeleteSelectedFiles').modal('hide');
			}
		});

	})



  /////// Tag Functions ///////
  $('.loadingSubmit').on('click', function() {
    $(this).button('loading');
  })



}) // onReady
