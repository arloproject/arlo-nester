//
// catalog.js
// 
// JavaScript functions specific to the catalog page



////////////////////////////////////////////////////////////////////////////
//                                                                        //
//   Functions for 'Class Actions' - Focus, Rename, Empty, Delete, etc.   //
//                                                                        //
////////////////////////////////////////////////////////////////////////////

// Global Variables used in this block
// #popupR
// #windowR
// #popupM
// #windowM
// #renameForm
// #exitR
// #id_newClassName
// #id_tagClasses
// 
// .popupwindow
// .main_submit
// 
// 
// urlPrefix
// 


// popupR for Rename Form
// popupM for Merge Form
// popupA for Adjust Form

function show_popupR() {
	$('#popupR').fadeIn('fast');
	$('#windowR').fadeIn('fast');
};
function close_popupR() {
	$('#popupR').fadeOut('fast');
	$('#windowR').fadeOut('fast');
};
function show_popupM() {
	$('#popupM').fadeIn('fast');
	$('#windowM').fadeIn('fast');
};
function close_popupM() {
	$('#popupM').fadeOut('fast');
	$('#windowM').fadeOut('fast');
};



$(document).ready(function(){

	$(".popupwindow").click(function(){
		action = $(this).attr('id');
		tagClassId = $(this).parent().attr('id');
		console.log("tagClassId: " + tagClassId);

		// rename
		if(action == 'rename'){
			url = urlPrefix + "/tools/renameTagClass/" + tagClassId;
			$("#renameForm").ajaxForm({url:url,type:"POST"});

			show_popupR();
			$("#exitR").click(function(){close_popupR();});
			$("#id_newClassName").autocomplete({source: tagClasses});

			$("#renameForm").submit(function(){
				close_popupR();

				$("#id_tagClasses").children().each(function(){
					$(this).prop("selected", false);
				});
				//$(".main_submit").click();
				//1. change className

				//2. change displayNames
				//3. change option name in form
				return false;
			});
		}

		if(action == 'empty'){
			if (confirm("Are you sure you wish to empty this class?")) { 
				url = urlPrefix + "/tools/emptyTagClass/" + tagClassId;
				$.post(url, function(){
					// reload the catalog page
					$(".main_submit").click();
				});
			}
		}

		if(action == 'delete'){
			if (confirm("Are you sure you wish to delete this Tag Class?")) {
				url = urlPrefix + "/tools/deleteTagClass/" + tagClassId;
				$.post(url, function(data){
					//hide tag class in form
					tagClassOption = $("option[value=" + data + "]");
					tagClassOption.prop('selected', false);
					tagClassOption.hide();
					//remove tag class from page
					tagClass_div = $("#" + tagClassId);
					tagClass_div.parent().parent().parent().remove();
				},"json");
			}
		}

	});  // $(".popupwindow").click(function(){


});  // $(document).ready(function(){







////////////////////////////////////////////////////////////////////
//                                                                //
//  Functions for the TagClass / TagSet selection form  //
//                                                                //
////////////////////////////////////////////////////////////////////


$(document).ready(function(){
	// Used to test if the selection settings have changed.
	$("#id_haveClassesChanged").val(0);

	/* ************************* */
	/*  'Change' Event Handlers  */
	/* ************************* */

	// Update TagClasses from Chosen TagSets
	$("#id_tagSets").change(function(event){
		$("#id_haveClassesChanged").val(1);
		updateTagClassesShown()
    });

	$("#id_tagClasses").change(function(){
		$("#id_haveClassesChanged").val(1);
	});

	/* **************************** */
	/*  Select All Button Handlers  */
	/* **************************** */

	// Select All TagSets Button
	$("#selectAllTagSets").click(function(event){
		event.preventDefault();
		selectAllTagSets();
	}).submit(function(event){
		event.preventDefault();
		selectAllTagSets();
	});

	// Select All TagClasses Button
	$("#selectAllTagClasses").click(function(event){
		event.preventDefault();
		selectAllTagClasses();
	}).submit(function(event){
		event.preventDefault();
		selectAllTagClasses();
	});

});

function tagSetsSelectionChange() {
	updateTagClassesShown();
};

function selectAllTagSets() {
	$("#id_haveClassesChanged").val(1);
	$("#id_tagSets").children().each(function(){
		if ($(this).prop('disabled') != true) {
			$(this).prop("selected", true);
		} else {
			$(this).prop("selected", false);
		}   
	}); 

	tagSetsSelectionChange();
}

function selectAllTagClasses() {
	$("#id_haveClassesChanged").val(1);
	$("#id_tagClasses").children().each(function(){
		if ($(this).prop('disabled') != true) {
			$(this).prop("selected", true);
		} else {
			$(this).prop("selected", false);
		}
	});
};

//function disableAllTagClasses() {
//	$("#id_tagClasses > option").each(function() {
//		var x = $("#id_tagClasses option[value='"+this.value+"']");
//		x.prop('disabled', true);
//	});
//};

//function deselectDisabledTagClasses() {
//    $("#id_tagClasses > option").each(function() {
//        var x = $("#id_tagClasses option[value='"+this.value+"']");
//        if (x.prop('disabled')) {
//			x.prop('selected', false);
//		}
//    });
//};

var myARLOStatus = null;
var preSelectedTagClasses = [];    // Array of Integers
var preUnselectedTagClasses = [];  // Array of Integers
var selectedTagClasses = [];       // Array of Integers
var shownTagClasses = [];          // Array of Integers

/* Update the TagClasses in the Select Box Based on the TagSets Chosen */
function updateTagClassesShown() {

	preSelectedTagClasses = [];
	preUnselectedTagClasses = [];
	selectedTagClasses = [];
	shownTagClasses = [];

	/* Generate List of Existing Select TagClasses */
	$("#id_tagClasses > option").each(function() {
		var x = $("#id_tagClasses option[value='" + this.value + "']");
		if ( x.prop('selected')) {
			preSelectedTagClasses.push(parseInt(this.value));
		} else {
			preUnselectedTagClasses.push(parseInt(this.value));
		}
	});

	/* Find out what TagClasses need to be added */
	$("#id_tagSets > option").each(function() {
		/* for each TagSet */
		var tagSetId = this.value;

		/* get the option object in the selector */
		var x = $("#id_tagSets option[value='" + tagSetId + "']");
		/* is TagSet selected? */
		if ( x.prop('selected')) {
			/* add all TagClasses in this TagSet to the shown list */
			for (var i = 0; i < tagSetsTagClasses[tagSetId].length; i++) {
				var tagClassId = tagSetsTagClasses[tagSetId][i];
				/* NOTE: inArray returns index of object, -1 if not in array */
				if ($.inArray(tagClassId, shownTagClasses) < 0) {  
					if (tagClassId in tagClassNames) {
						shownTagClasses.push(tagClassId);
						selectedTagClasses.push(tagClassId);
					} /* TODO If false,  This means we have some bad data somewhere */
				}
			}
		}
	});

	/* delete all TagClasses in the select box */
	$("#id_tagClasses").empty();

	/* Should we display the Status Popup? */
	/* Display for lists > 100 TagClasses */

	if (myARLOStatus == null) {
		myARLOStatus = new ARLOModalStatus();
	}
	if (shownTagClasses.length > 100) {
		myARLOStatus.setStatus("<img src='" + LOADING_GIF_URL + "' width='35px' height='35px'/><br /><strong>Loading TagClasses...</strong>", true);

		/* We have to do a little hack here to get the Status Image to Display. 
		   Fire an event to finish running this in 20ms so that the rest of this 
		   doesn't block the UI from displaying the status */

		setTimeout( updateTagClassesShownStep2, 50);
	} else {
		updateTagClassesShownStep2();
	}

};

function updateTagClassesShownStep2() {

	/* sort the array, based on the Class Names */
	shownTagClasses.sort( function(a,b) { 
		if ((a in tagClassNames) && (b in tagClassNames)) {
			return tagClassNames[a].toUpperCase() > tagClassNames[b].toUpperCase() ? 1 : -1; 
		} else {
			return 0;  /* TODO This means there is bad data in the database */
		}
	});

	/* add back the applicable TagClasses */
	var myOptions = [];
	for (var i = 0; i < shownTagClasses.length; i++) {
		var tagClassId = shownTagClasses[i];
		
		/* Whether or not to select the TagClass - basically, select all, except 
		   for those that were previously unselected */
		var selected = $.inArray(tagClassId, preUnselectedTagClasses) == -1;
		myOptions.push(
			'<option value="' + tagClassId + '"' + ( selected ? ' selected=selected"' : '' ) + '>' + 
			tagClassNames[tagClassId] + 
			'</option>');
	}
	var sOptions = myOptions.join('');

	/* pure JS loads faster for the append */
	document.getElementById('id_tagClasses').innerHTML = sOptions;
	/* $("#id_tagClasses").html(sOptions); */

	if ( myARLOStatus != null) {
		myARLOStatus.close();
	}
};

