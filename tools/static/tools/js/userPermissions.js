// userPermissions.js
//
// A set of tools for editing ArloUserPermissions on objects (namely
// Project, Library, and TagSet).
//
// Implementation Notes:
// - arlo.js needs to be loaded and initialized.
//

/**
 * Initialize a new UserPermissionsEditor object. 
 *
 * This must be initialized with:
 * @param loadingGifURL - URL To the Loading GIF displayed on the ARLOModalStatus
 * @param myAPIArloPermission - Initialized APIArloPermission object
 * @param myAPIUser - Initialized APIUser object
 * @param myAPIArloUserGroup - Initialized APIArloUserGroup object
 *
 * @return Boolean - true if success, false on error
 */

function UserPermissionsEditor(loadingGifURL, myAPIArloPermission, myAPIUser, myAPIArloUserGroup)
{
	this.myARLOModalStatus = null;
	this.loadingGifURL = loadingGifURL;
	this.myAPIArloPermission = myAPIArloPermission;
	this.myAPIUser = myAPIUser;
	this.myAPIArloUserGroup = myAPIArloUserGroup;


	/**
	 * Remove an existing ArloUserPermission assigned to an object.
	 * @param arloPermissionId The ID of the Permission to delete.
	 * @param permName Name of the perm to delete (used for logging / UI confirmation)
	 */

	this.removePermission = function(arloPermissionId, permName){
		if (confirm("Remove " + permName + "?")) {
			if (this.myARLOModalStatus == null) {
				this.myARLOModalStatus = new ARLOModalStatus();
			}
			this.myARLOModalStatus.setStatus("<img src='" + this.loadingGifURL + "' width='35px' height='35px'/><br /><strong>Removing Permission...</strong>", true);
// 			var myAPIArloPermission = new APIArloPermission("{{ArloAPIv0Url}}");
			this.myAPIArloPermission.removePermission(
				arloPermissionId,
				function(json) {
					ARLOLogNotify("Successfully Removed Permission: " + permName);
					location.reload();
				},
				function(jqXHR, textStatus, errorThrown) {
					ARLOLogError("Failed Removing Permission: Status: " + textStatus + " Error: " + errorThrown);
					this.myARLOModalStatus.close();
					this.myARLOModalStatus = null;
				}
			);
		}
	}

	/*                               */
	/*        Add Permissions        */
	/*                               */

	/**
	 * Add a new ArloPermission. Show a form to collect info, then
	 * make an API call to create.
	 *
	 * @param target_type - one of 'library', 'project', 'tagset'
	 * @param target_ids - a dictionary of target_id to target_names
	 */

	this.showAddPermissionForm = function(target_type, target_ids){

		// see if target type is in one of our accepted types
		if (['library', 'project', 'tagset'].indexOf(target_type) == -1) {
			ARLOLogError("Failed initializing UserPermissionsEditor - unknown target type '" + target_type + "'");
			return false;
		}

		/* Create an ARLOModalForm to set the new permissions */
		var form = document.createElement("form");
		form.setAttribute('action', "#");
		
		var sHtml = "" +
			"<table>" +
			"  <tr>" +
			"    <td colspan='2'>" +
			"        Target: <select name='target_id'>";

		for (var id in target_ids) {
			sHtml += "<option value='" + id + "'>" + target_ids[id] + "</option>";
		}

		sHtml += "" +
			"        </select>" +
			"    </td>" +
			"  </tr><tr>" +
			"    <td>" +
			"      <input type='hidden' name='target_type' value='" + target_type + "' />" +
			"      <select name='bearer_type'><option>User</option><option>Group</option></select>" +
			"    </td><td>" +
			"      <input type='text' name='bearer_name'></input><br />" +
			"    </td>" +
			"  </tr><tr>" +
			"    <td><p style='text-align:right'>Read?</p></td>" +
			"    <td><input type='checkbox' name='read' value='read'></td>" +
			"  </tr><tr>" +
			"    <td><p style='text-align:right'>Write?</p></td>" +
			"    <td><input type='checkbox' name='write' value='write'></td>" +
			"  </tr><tr>" +
			"    <td><p style='text-align:right'>LaunchJob?</p></td>" +
			"    <td><input type='checkbox' name='launch_job' value='launch_job'></td>" +
			"  </tr><tr>" +
			"    <td><p style='text-align:right'>Admin?</p></td>" +
			"    <td><input type='checkbox' name='admin' value='admin'></td>" +
			"  </tr><tr>" +
			"</table>" +
			"<input type='submit' name='submit' value='Save' />&nbsp;" +
			"<input type='submit' name='cancel' value='Cancel' />";

		form.innerHTML = sHtml;

		var modalForm = new ARLOModalForm();
		modalForm.popupForm(
			form,
			function(form, context){
				/* callback from the form  */
				/* Save the new Value Here */

				var bearer_type = $(form).find('[name=bearer_type]').val();
				if (bearer_type == "User") {
					bearer_type = 'user';
				} else if (bearer_type == 'Group') {
					bearer_type = "arlousergroup";
				}

				var target_id = $(form).find('[name=target_id]').val();
				var target_type = $(form).find('[name=target_type]').val();
				var bearer_name = $(form).find('[name=bearer_name]').val();
				var has_read = $(form).find('[name=read]').is(':checked');
				var has_write = $(form).find('[name=write]').is(':checked');
				var has_launch_job = $(form).find('[name=launch_job]').is(':checked');
				var has_admin = $(form).find('[name=admin]').is(':checked');

				if (context.myARLOModalStatus == null) {
					context.myARLOModalStatus = new ARLOModalStatus();
				}
				context.myARLOModalStatus.setStatus("<img src='" + context.loadingGifURL + "' width='35px' height='35px'/><br /><strong>Looking Up Bearer Id...</strong>", true);

				if (bearer_type == 'user') {
					context.myAPIUser.lookup_username(
						bearer_name,
						function(json) {

							/* Check if we found exactly 1 user */
							if (json.count != 1) {
								ARLOLogError("User Not Found");
								context.myARLOModalStatus.close();
								context.myARLOModalStatus = null;
								return;
							}

							/* We have 1 user, proceed to add permissions */
							var bearer_id = json.results[0].id;
							ARLOLogNotify("Successfully Found User: " + bearer_id + " bearer_type: " + bearer_type);

							context.addPermission(bearer_type, bearer_id, target_type, target_id, has_read, has_write, has_launch_job, has_admin);
						},
						function(jqXHR, textStatus, errorThrown) {
							ARLOLogError("Failed Searching for User: Status: " + textStatus + " Error: " + errorThrown);
							context.myARLOModalStatus.close();
							context.myARLOModalStatus = null;
						}
					);
				} else if (bearer_type == 'arlousergroup') {
					context.myAPIArloUserGroup.lookup_name(
						bearer_name,
						function(json) {
							/* Check if we found exactly 1 group */
							if (json.count != 1) {
								ARLOLogError("Group Not Found");
								context.myARLOModalStatus.close();
								context.myARLOModalStatus = null;
								return;
							}
							/* We have 1 group, proceed to add permissions */
							var bearer_id = json.results[0].id;
							ARLOLogNotify("Successfully Found Group: " + bearer_id + " bearer_type: " + bearer_type);
							context.addPermission(bearer_type, bearer_id, target_type, target_id, has_read, has_write, has_launch_job, has_admin);
						},
						function(jqXHR, textStatus, errorThrown) {
							ARLOLogError("Failed Searching for Group: Status: " + textStatus + " Error: " + errorThrown);
							context.myARLOModalStatus.close();
							context.myARLOModalStatus = null;
						}
					);
				} else {
					ARLOLogError("Unexpected Bearer Type Found");
					context.myARLOModalStatus.close();
					context.myARLOModalStatus = null;

				}

				return true;  /* close the ModalForm */
			},
			null,  /* on cancel, do nothing */
			this  /* context passed to callbacks */
		);
	}


	this.addPermission = function(bearer_type, bearer_id, target_type, target_id, has_read, has_write, has_launch_job, has_admin){
		if (this.myARLOModalStatus == null) {
			this.myARLOModalStatus = new ARLOModalStatus();
		}
		this.myARLOModalStatus.setStatus("<img src='" + this.loadingGifURL + "' width='35px' height='35px'/><br /><strong>Saving Permission...</strong>", true);

		this.myAPIArloPermission.addPermission(
			bearer_type,
			bearer_id,
			target_type,
			target_id,
			has_read,
			has_write,
			has_launch_job,
			has_admin,
			function(json) {
				ARLOLogNotify("Successfully Added Permission");
				location.reload();
			},
			function(jqXHR, textStatus, errorThrown) {
				ARLOLogError("Failed Adding Permission: Status: " + textStatus + " Error: " + errorThrown);
				this.myARLOModalStatus.close();
				this.myARLOModalStatus = null;
			}
		);
	}


	/*                                */
	/*        Edit Permissions        */
	/*                                */


	/**
	 * Edit an existing ArloPermission. Pass in the existing values,
	 * Show a form to collect new info, then make an API call to create.
	 */

	this.showEditPermissionForm = function(perm_id, bearer_name, target_name, has_read, has_write, has_launch_job, has_admin){

		/* Create an ARLOModalForm to edit the permissions */
		var form = document.createElement("form");
		form.setAttribute('action', "#");
		var formHtml = "" +
			"<input type='hidden' name='perm_id' value='" + perm_id + "'/>" +
			"<label>Modifying Permissions for '" + bearer_name + "' on '" + target_name + "'</label>" +
			"<table>" +
			"  <tr>" +
			"  </tr><tr>" +
			"    <td><p style='text-align:right'>Read?</p></td>" +
			"    <td><input type='checkbox' name='read' value='read'";
		if (has_read) { formHtml += " checked='checked'"; }
		formHtml += "></td>" +
			"  </tr><tr>" +
			"    <td><p style='text-align:right'>Write?</p></td>" +
			"    <td><input type='checkbox' name='write' value='write'";
		if (has_write) { formHtml += " checked='checked'"; }
		formHtml += "></td>" +
			"  </tr><tr>" +
			"    <td><p style='text-align:right'>LaunchJob?</p></td>" +
			"    <td><input type='checkbox' name='launch_job' value='launch_job'";
		if (has_launch_job) { formHtml += " checked='checked'"; }
		formHtml += "></td>" +
			"  </tr><tr>" +
			"    <td><p style='text-align:right'>Admin?</p></td>" +
			"    <td><input type='checkbox' name='admin' value='admin'";
		if (has_admin) { formHtml += " checked='checked'"; }
		formHtml += "></td>" +
			"  </tr><tr>" +
			"</table>" +
			"<input type='submit' name='submit' value='Save' />&nbsp;" +
			"<input type='submit' name='cancel' value='Cancel' />";

		form.innerHTML = formHtml;

		var modalForm = new ARLOModalForm();
		modalForm.popupForm(
			form,
			function(form, context){
				/* callback from the form  */
				/* Save the Values Here */

				var perm_id = $(form).find('[name=perm_id]').val();
				var has_read = $(form).find('[name=read]').is(':checked');
				var has_write = $(form).find('[name=write]').is(':checked');
				var has_launch_job = $(form).find('[name=launch_job]').is(':checked');
				var has_admin = $(form).find('[name=admin]').is(':checked');

				if (context.myARLOModalStatus == null) {
					context.myARLOModalStatus = new ARLOModalStatus();
				}

				context.editPermission(perm_id, has_read, has_write, has_launch_job, has_admin);

				return true;  /* close the ModalForm */
			},
			null,  /* on cancel, do nothing */
			this /* callback context */
		);
	}


	this.editPermission = function(perm_id, has_read, has_write, has_launch_job, has_admin){
		if (this.myARLOModalStatus == null) {
			this.myARLOModalStatus = new ARLOModalStatus();
		}
		this.myARLOModalStatus.setStatus("<img src='" + this.loadingGifURL + "' width='35px' height='35px'/><br /><strong>Saving...</strong>", true);

		this.myAPIArloPermission.editPermission(
			perm_id,
			has_read,
			has_write,
			has_launch_job,
			has_admin,
			function(json) {
				ARLOLogNotify("Successfully Edited Permission");
				location.reload();
			},
			function(jqXHR, textStatus, errorThrown) {
				ARLOLogError("Failed Adding Permission: Status: " + textStatus + " Error: " + errorThrown);
				this.myARLOModalStatus.close();
				this.myARLOModalStatus = null;
			},
			this /* callback context */
		);
	}


	return true;
};
