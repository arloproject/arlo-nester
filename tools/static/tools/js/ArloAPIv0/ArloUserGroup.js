
// Remember to include CSRF Token in AJAX call if making PUT/POST/DELETE etc. requests. 
// See https://docs.djangoproject.com/en/1.7/ref/contrib/csrf/


function APIArloUserGroup(API_BASE_URL) {
	this.myAPI_BASE_URL = API_BASE_URL;


	/* Get a List of ArloUserGroups that this User can View */

	this.getArloUserGroups = function(successCallback, errorCallback) {
		var url = this.myAPI_BASE_URL + 'arloUserGroup/';
		this.myGetAllPages = new GetAllPages();

		this.myGetAllPages.getAllPages(url, successCallback, errorCallback);
	};


	/* Create a new ArloUserGroup object.                         */
	/* @param arloUserGroupName Name of the new ArloUserGroup     */

	this.addUserGroup = function(arloUserGroupName, successCallback, errorCallback) {
		$.ajax({  
			type: 'POST',
			url: this.myAPI_BASE_URL + 'arloUserGroup/',
			data: {'name': arloUserGroupName, 'users': []},
			dataType: 'json',
			success: successCallback,
			error: errorCallback,
			timeout:10000
		});
	};

	/* Remove a user from ArloUserGroup                   */
	/* @param arloUserGroupId Id of the ArloUserGroup     */
	/* @param userId Id of the User to remove             */

	this.removeUserFromGroup = function(arloUserGroupId, userId, successCallback, errorCallback) {
		$.ajax({
			type: 'DELETE',
			url: this.myAPI_BASE_URL + 'arloUserGroup/' + arloUserGroupId + '/remove_user/',
			data: {'id': userId},
			dataType: 'json',
			success: successCallback,
			error: errorCallback,
			timeout:10000
		});
	};

	/* Add a user to an ArloUserGroup                  */
	/* @param arloUserGroupId Id of the ArloUserGroup  */
	/* @param username Username of the User to add     */

	this.addUserToGroup = function(arloUserGroupId, username, successCallback, errorCallback) {
		$.ajax({
			type: 'POST',
			url: this.myAPI_BASE_URL + 'arloUserGroup/' + arloUserGroupId + '/add_user/',
			data: {'name': username},
			dataType: 'json',
			success: successCallback,
			error: errorCallback,
			timeout:10000
		});
	};

	/* Lookup an ArloUserGroup object by querying it's name. */
	/* @param name Name of the ArloUserGroup                 */
	/* NOTE: Return will be the json object - the            */
	/*       receiver will need to validate the results      */
	/*       as there may be multiple or none.               */

	this.lookup_name = function(name, successCallback, errorCallback) {
		$.ajax({  
			type: 'GET',
			url: this.myAPI_BASE_URL + 'arloUserGroup/?name=' + name,
			dataType: 'json',
			success: successCallback,
			error: errorCallback,
			timeout:10000
		});
	};

	/* Create a new ArloUserGroup object.                         */
	/* @param arloUserGroupId ID of the ArloUserGroup to Delete   */

	this.deleteUserGroup = function(arloUserGroupId, successCallback, errorCallback) {
		$.ajax({  
			type: 'DELETE',
			url: this.myAPI_BASE_URL + 'arloUserGroup/' + arloUserGroupId,
			dataType: 'json',
			success: successCallback,
			error: errorCallback,
			timeout:10000
		});
	};


	/* Add a user as an Admin to an ArloUserGroup - adds an   */
	/* ArloPermission for the user to the Group with Admin    */
	/* privileges.                                            */
	/* @param arloUserGroupId Id of the ArloUserGroup         */
	/* @param user_id User ID of the User to add              */

	this.addAdminToGroup = function(arloUserGroupId, user_id, successCallback, errorCallback) {
		$.ajax({
			type: 'POST',
			url: this.myAPI_BASE_URL + 'arloPermission/',
			data: {
				"bearer_type": "user",
				"bearer_id": user_id,
				"target_type": "arlousergroup",
				"target_id": arloUserGroupId,
				"read": false,
				"write": false,
				"launch_job": false,
				"admin": true
			},
			dataType: 'json',
			success: successCallback,
			error: errorCallback,
			timeout:10000
		});
	};


}

