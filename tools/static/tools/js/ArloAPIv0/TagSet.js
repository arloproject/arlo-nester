
// Remember to include CSRF Token in AJAX call if making PUT/POST/DELETE etc. requests. 
// See https://docs.djangoproject.com/en/1.7/ref/contrib/csrf/


function APITagSet(API_BASE_URL) {  
	this.myAPI_BASE_URL = API_BASE_URL;  
  
	this.hideTagSet = function(tagSetId, hidden, successCallback, errorCallback) {  
		$.ajax({  
			type: 'PATCH',  
			url: this.myAPI_BASE_URL + 'tagSet/' + tagSetId + '/',  
            data: {'hidden': hidden},
			dataType: 'json',
			success: successCallback,  
			error: errorCallback,
            timeout:10000
		});  
	};  

}  

