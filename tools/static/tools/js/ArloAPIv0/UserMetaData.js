
// Remember to include CSRF Token in AJAX call if making PUT/POST/DELETE etc. requests. 
// See https://docs.djangoproject.com/en/1.7/ref/contrib/csrf/


function APIUserMetaData(API_BASE_URL) {  
	this.myAPI_BASE_URL = API_BASE_URL;
  
	this.getMediaFile_mediaFileUserMetaData = function(mediaFileId, successCallback, errorCallback) {  
		var url = this.myAPI_BASE_URL + 'mediaFile/' + mediaFileId + '/userMetaData/';
		this.myGetAllPages = new GetAllPages();

		this.myGetAllPages.getAllPages(url, successCallback, errorCallback);
	};  

	this.updateMediaFileUserMetaData_value = function(mediaFileUserMetaDataId, newValue, successCallback, errorCallback) { 
		$.ajax({  
			type: 'PUT',  
			url: this.myAPI_BASE_URL + 'mediaFileUserMetaData/' + mediaFileUserMetaDataId + '/',  
			data: {'value': newValue},
			dataType: 'json',
			success: successCallback,  
			error: errorCallback,
			timeout:10000  
		});  
	};  

	this.deleteMediaFileUserMetaData = function(mediaFileUserMetaDataId, successCallback, errorCallback) { 
		$.ajax({  
			type: 'DELETE',  
			url: this.myAPI_BASE_URL + 'mediaFileUserMetaData/' + mediaFileUserMetaDataId + '/',  
			// if we use 'json' for the dataType, this will throw an error since no JSON is returned 
			// (204 No Content)
			dataType: 'text',
			success: successCallback,  
			error: errorCallback,
			timeout:10000  
		});  
	};  

	this.getLibrary_mediaFileUserMetaDataFields = function(libraryId, successCallback, errorCallback) {  
		var url = this.myAPI_BASE_URL + 'library/' + libraryId + '/userMetaDataFields/';
		this.myGetAllPages = new GetAllPages();

		this.myGetAllPages.getAllPages(url, successCallback, errorCallback);
	};  

	this.addMediaFileUserMetaData = function(mediaFileId, userMetaDataFieldId, newValue, successCallback, errorCallback) {
		$.ajax({
			type: 'POST',
			url: this.myAPI_BASE_URL + 'mediaFileUserMetaData/',
			data: {'mediaFile': mediaFileId, 'metaDataField':userMetaDataFieldId, 'value': newValue},
			dataType: 'json',
			success: successCallback,
			error: errorCallback,
			timeout:10000
		});
	};

	this.addMediaFileUserMetaDataField = function(name, description, libraryId, successCallback, errorCallback) {
		$.ajax({
			type: 'POST',
			url: this.myAPI_BASE_URL + 'mediaFileUserMetaDataField/',
			data: {'name': name, 'description':description, 'library': libraryId},
			dataType: 'json',
			success: successCallback,
			error: errorCallback,
			timeout:10000
		});
	};

}  

