
/*  GetAllPages Functions                                               */
/*                                                                      */
/*  Iterate through all pages for an API endpoint to collect all data.  */

function GetAllPages() {

	this.getAllPages_successHandler = function(data) {

		/* Iterate over result pagination */
		for (var x = 0; x < data.results.length; x++) {
			this.dataArray.push(data.results[x]);
		}
		if (data.next) {
			this.getFunc(data.next, this.dataArray, this.getFunc, this.success, this.finalSuccess, this.error);
		} else {
			this.finalSuccess(this.dataArray);
		}
	};

	this.getAllPages_get = function(url, dataArray, getFunc, successHandler, successCallback, errorCallback) {
		$.ajax({  
			type: 'GET',  
			url: url,
			contentType: 'application/json', 
			dataType: 'json',
			/* store a reference to all of our functions */
			dataArray: dataArray,
			getFunc: getFunc,
			finalSuccess: successCallback,
			success: successHandler,
			error: errorCallback,
			timeout:30000  /* 30 seconds */
		});  
	};

	this.getAllPages = function( url, successCallback, errorCallback) {
		var dataArray = [];

		this.getAllPages_get(url, dataArray, this.getAllPages_get, this.getAllPages_successHandler, successCallback, errorCallback);
    };
}  

