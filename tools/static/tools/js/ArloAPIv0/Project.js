
// Remember to include CSRF Token in AJAX call if making PUT/POST/DELETE etc. requests. 
// See https://docs.djangoproject.com/en/1.7/ref/contrib/csrf/


function APIProject(API_BASE_URL) {
	this.myAPI_BASE_URL = API_BASE_URL;

	this.getProject_mediaFiles = function(projectId, successCallback, errorCallback) {
		var url = this.myAPI_BASE_URL + 'project/' + projectId + '/mediaFiles/';
		this.myGetAllPages = new GetAllPages();

		this.myGetAllPages.getAllPages(url, successCallback, errorCallback);
	};

	this.updateDefaultPermissions = function(project_id, read, write, launch_job, admin, successCallback, errorCallback) {
		$.ajax({
			type: 'PATCH',
			url: this.myAPI_BASE_URL + 'project/' + project_id,
			data: {
				'default_permission_read': read,
				'default_permission_write': write,
				'default_permission_launch_job': launch_job,
				'default_permission_admin': admin,
			},
			dataType: 'json',
			success: successCallback,
			error: errorCallback,
			timeout:10000
		});
	};
}

