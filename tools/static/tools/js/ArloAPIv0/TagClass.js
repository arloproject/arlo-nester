
// Remember to include CSRF Token in AJAX call if making PUT/POST/DELETE etc. requests. 
// See https://docs.djangoproject.com/en/1.7/ref/contrib/csrf/


function APITagClass(API_BASE_URL) {  
	this.myAPI_BASE_URL = API_BASE_URL;  
  
	this.addTagClass = function(projectId, className, displayName, successCallback, errorCallback) {  
		$.ajax({  
			type: 'POST',
			url: this.myAPI_BASE_URL + 'tagClass/',
			data: {'project': projectId, 'className': className, 'displayName': displayName},
			dataType: 'json',
			success: successCallback,
			error: errorCallback,
			timeout:10000
		});  
	};  

}  

