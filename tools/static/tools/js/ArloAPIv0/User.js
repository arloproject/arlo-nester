
// Remember to include CSRF Token in AJAX call if making PUT/POST/DELETE etc. requests. 
// See https://docs.djangoproject.com/en/1.7/ref/contrib/csrf/


function APIUser(API_BASE_URL) {  
	this.myAPI_BASE_URL = API_BASE_URL;  

	/* Lookup a User object by querying it's username    */
	/* @param username Username of the User              */
	/* NOTE: Return will be the json object - the        */
	/*       receiver will need to validate the results  */
	/*       as there may be multiple or none.           */

	this.lookup_username = function(username, successCallback, errorCallback, context) {
		$.ajax({  
			type: 'GET',
			url: this.myAPI_BASE_URL + 'user/?username=' + username,
			dataType: 'json',
			success: successCallback,
			error: errorCallback,
			context: context,
			timeout:10000
		});  
	};  


}

