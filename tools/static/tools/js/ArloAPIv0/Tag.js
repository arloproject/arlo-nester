
// Remember to include CSRF Token in AJAX call if making PUT/POST/DELETE etc. requests. 
// See https://docs.djangoproject.com/en/1.7/ref/contrib/csrf/


function APITag(API_BASE_URL) {  
	this.myAPI_BASE_URL = API_BASE_URL;  

	this.addTag = function(mediaFileId, tagSetId, startTime, endTime, minFrequency, maxFrequency, tagClassId, randomlyChosen, machineTagged, userTagged, strength, successCallback, errorCallback) {
		$.ajax({
			type: 'POST',
			url: this.myAPI_BASE_URL + 'tag/',
			data: {
				'mediaFile': mediaFileId, 
				'tagSet': tagSetId,
				'startTime': startTime,
				'endTime': endTime,
				'minFrequency': minFrequency,
				'maxFrequency': maxFrequency,
				'tagClass': tagClassId,
				'randomlyChosen': randomlyChosen,
				'machineTagged': machineTagged,
				'userTagged': userTagged,
				'strength': strength
			},
			dataType: 'json',
			success: successCallback,
			error: errorCallback,
			timeout:10000
		});
	};

	this.deleteTag = function(tagId, successCallback, errorCallback) {
		$.ajax({
			type: 'DELETE',
			url: this.myAPI_BASE_URL + 'tag/' + tagId + '/',
			dataType: 'json',
			success: successCallback,
			error: errorCallback,
			timeout:10000
		});
	};

}

