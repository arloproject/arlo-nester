
// Remember to include CSRF Token in AJAX call if making PUT/POST/DELETE etc. requests. 
// See https://docs.djangoproject.com/en/1.7/ref/contrib/csrf/


function APIArloPermission(API_BASE_URL) {  
	this.myAPI_BASE_URL = API_BASE_URL;  

	/* Create a new ArloPermission object.                        */

	this.addPermission = function(
							bearer_type,
							bearer_id,
							target_type,
							target_id,
							has_read,
							has_write,
							has_launch_job,
							has_admin,
							successCallback, errorCallback, context) {
		$.ajax({  
			type: 'POST',
			url: this.myAPI_BASE_URL + 'arloPermission/',
			data: {
				'bearer_type': bearer_type,
				'bearer_id': bearer_id,
				'target_type': target_type,
				'target_id': target_id,
				'read': has_read,
				'write': has_write,
				'launch_job': has_launch_job,
				'admin': has_admin
			},
			dataType: 'json',
			success: successCallback,
			error: errorCallback,
			context: context,
			timeout:10000
		});
	};

	/* Remove an ArloPermission.                        */
	/* @param arloPermissionId Id of the ArloPermission */
	
	this.removePermission = function(arloPermissionId, successCallback, errorCallback, context) {
		$.ajax({
			type: 'DELETE',
			url: this.myAPI_BASE_URL + 'arloPermission/' + arloPermissionId,
			data: {'id': arloPermissionId},
			dataType: 'json',
			success: successCallback,
			error: errorCallback,
			context: context,
			timeout:10000
		});
	};


	/* Edit an existing ArloPermission object.  */

	this.editPermission = function(
							arloPermissionId,
							has_read,
							has_write,
							has_launch_job,
							has_admin,
							successCallback, errorCallback, context) {
		$.ajax({
			type: 'PATCH',
			url: this.myAPI_BASE_URL + 'arloPermission/' + arloPermissionId,
			data: {
				'read': has_read,
				'write': has_write,
				'launch_job': has_launch_job,
				'admin': has_admin
			},
			dataType: 'json',
			success: successCallback,
			error: errorCallback,
			context: context,
			timeout:10000
		});
	};

}
