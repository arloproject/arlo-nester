function TagClass(id, tagSet, className, displayName, parentDiv) {
	//type properties
	this.id = id;
	this.tagSet = tagSet;
	//position properties
	this.parentDiv = parentDiv;
	this.className = className;
	this.displayName = displayName;
	this.userStartIndex = 0;
	this.machineStartIndex = 0;
	this.userEndIndex = $("#id_userTagsPerPage").val();
	this.machineEndIndex = $("#id_machineTagsPerPage").val();

	this.parentDiv.append("<div id=" + this.id + "></div>");
	this.div = $("#" + this.id);
	this.div.addClass("tagClass");
	this.div.append("<table border='3'><tr><td><div id = '"+this.id+"i'></div></td><td><div id = '"+this.id+"m'></div></td><td><div id = '"+this.id+"u'></div></td></tr></table>");
	this.infoDiv = $("#" + this.id + "i");
	this.machineDiv = $("#" + this.id + "m");
	this.userDiv = $("#" + this.id + "u");
	//alert("machineDiv: " + this.machineDiv.attr("id"));
	//alert("userDiv: " + this.userDiv.attr("id"));
	this.userTags = new Array();
	this.machineTags = new Array();
  
	this.infoDiv.append("<H3>" + this.className + "</H3><br/>");
	this.infoDiv.append("<a class='popupwindow' id='rename'>Rename</a><br/>");
	this.infoDiv.append("<a class='popupwindow' id='adjust'>Adjust</a><br/>");
	this.infoDiv.append("<a class='popupwindow' id='empty'>Empty</a><br/>");
	this.infoDiv.append("<a class='popupwindow' id='delete'>Delete</a><br/>");
	this.infoDiv.append("<a class='popupwindow' id='focus'>Focus</a><br/>");

	this.userLoad = function(startIndex, endIndex, sort, framesPerSecond, frequencyBands, dampingFactor) {
		url = '/nester/tools/createCatalogSpectra/' + this.id + '/' + startIndex + '/' + endIndex + '/user/' + sort;
		url += '/' + framesPerSecond + '/' + frequencyBands + '/' + dampingFactor;
		alert(url);
		tagClass = this;
		$.getJSON(url, function(json){
			alert("got url!");
			tagClass.userDiv.empty();
			tagClass.userTags = new Array();
			alert("id: " + tagClass.userDiv.attr("id") + " count: " + json.length);
			for(i=0;i<json.length;i++) {
				alert('hit loop!');
				tagClass.userTags[i] = new Tag(json[i].id, json[i].userTagged, json[i].machineTagged, this.userDiv, tagClass.displayName);
				tagClass.userTags[i].display(json[i].imagePath);
			}
		});
	};

	this.machineLoad = function(startIndex, endIndex, sort, framesPerSecond, frequencyBands, dampingFactor){
		url = '/nester/tools/createCatalogSpectra/' + this.id + '/' + startIndex + '/' + endIndex + '/machine/' + sort;
		url += '/' + framesPerSecond + '/' + frequencyBands + '/' + dampingFactor;
		alert(url);
		tagClass = this;
		$.getJSON(url, function(json){
			tagClass.machineDiv.empty();
			tagClass.machineTags = new Array();
			for(i=0;i<json.length;i++){
				tagClass.machineTags[i] = new Tag(json[i].id, json[i].userTagged, json[i].machineTagged, tagClass.machineDiv, tagClass.displayName);
				tagClass.machineTags[i].display(json[i].imagePath);
			}
		});
	};

	this.userLoad(this.userStartIndex, this.userEndIndex, $("#id_sortBy").val(),
		$("#id_numFramesPerSecond").val(), $("#id_numFrequencyBands").val(),
		$("#id_spectraDampingFactor").val());

	this.machineLoad(this.machineStartIndex, this.machineEndIndex, $("#id_sortBy").val(),
		$("#id_numFramesPerSecond").val(), $("#id_numFrequencyBands").val(),
		$("#id_spectraDampingFactor").val());
	return true;
};


