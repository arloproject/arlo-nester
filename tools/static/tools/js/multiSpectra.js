// multiSpectra.js
// TonyB April 2012
// for multiple file view spectraPlotting

// Dependencies
// ------------
// - arlo.js
//   - ARLO Logging Needs to be setup with ARLOInit();

// variables passed in through django (for multiSpectra.js)
//
//  var urlPrefix = "{{url_prefix}}";

//  var audioFileIds = jQuery.parseJSON("{{audioFileIdsJSON|safe}}");
//  var audioFileAliases = jQuery.parseJSON("{{audioFileAliasesJSON|safe}}");

// re-initialized when images are returned
//
//  var imageWidth = 0;
//  var imageHeight = 0;


//  +----#audioFilesMasterDiv----------------------------+
//  |                                                    |
//  |  +----#audioFile_(audioFileId)------------------+  |
//  |  |                                              |  |
//  |  | +----#audioFileCollapse_(audioFileId)-----+  |  |
//  |  | |                                         |  |  |
//  |  | |  <audio> audioPlayer_(audioFileId)      |  |  |
//  |  | |  <a> audioDownload_(audioFileId)        |  |  |
//  |  | |                                         |  |  |
//  |  | |  +----#audioFileImg_(audioFileId)----+  |  |  |
//  |  | |  |                                   |  |  |  |
//  |  | |  |  img = background of div          |  |  |  |
//  |  | |  |                                   |  |  |  |
//  |  | |  |                                   |  |  |  |
//  |  | |  +-----------------------------------+  |  |  |
//  |  | +-----------------------------------------+  |  |
//  |  +----------------------------------------------+  |
//  |                                                    | 
//  |  +----#audioFile_(audioFileId)------------------+  |
//  |  |                                              |  |
//  |  | +----#audioFileCollapse_(audioFileId)-----+  |  |
//  |  | |                                         |  |  |
//  |  | |  +----#audioFileImg_(audioFileId)----+  |  |  |
//  |  | |  |                                   |  |  |  |
//  |  | |  |  img = background of div          |  |  |  |
//  |  | |  |                                   |  |  |  |
//  |  | |  |                                   |  |  |  |
//  |  | |  +-----------------------------------+  |  |  |
//  |  | +-----------------------------------------+  |  |
//  |  +----------------------------------------------+  |
//  |                                                    | 
//  +----------------------------------------------------+



//////
// Callbacks
// 
// Guess we need a more function API - include some Callbacks from these 
// functions so the calling pages can extend upon them. 
//
// I guess you could call this, a Callback "Callback" </dramatic look>
// 
// createTagFormCallback_Success(success, saveMessage, rawJson)
//    - Called at the very end of a successful createTagFormCallback
// 





////
// StatusLogging 

function MultiViewStatusUpdate(s) {
	console.log(s);
}


///////////////////////////////
//                           //
//        Static Data        //
//                           //
///////////////////////////////

var myARLOModalStatus = new ARLOModalStatus();


////////////////////////////////
//                            //
//        Data Classes        //
//                            //
////////////////////////////////

///
// AudioFile Data

function AudioFileProperties(audioFileId, audioFileDiv, imageDiv, audioPlayer, audioDownload, sensorId, sensorArrayId) {
	this.audioFileId = audioFileId;
	this.audioFileDiv = audioFileDiv;
	this.imageDiv = imageDiv;  // jQuery selector

	this.audioPlayer = audioPlayer;  // was an audio element, now our AudioPlayer custom class
	this.audioDownload = audioDownload;  // 'a' element

	this.spectraStartSeconds = 0;
	this.spectraEndSeconds = 0;
    
	this.fileEndSeconds = 0;

	this.imageWidth = 0;
	this.imageHeight = 0;
	this.minFrequency = 0;
	this.maxFrequency = 0;

	// test if isNumeric, in case sensor is 'null'
	if (!isNaN(parseFloat(sensorId)) && isFinite(sensorId)) {
		this.sensorId = sensorId;
	} else {
		this.sensorId = null;
	}
	if (!isNaN(parseFloat(sensorArrayId)) && isFinite(sensorArrayId)) {
		this.sensorArrayId = sensorArrayId;
	} else {
		this.sensorArrayId = null;
	}

	return true;
};

audioFileProperties = [];


///
// state machine to keep track of clicks

function ClickStateMachine() {
	this.audioFileProperties = undefined;
	this.haveFirstPoint = false;
	this.haveSecondPoint = false;

	this.firstX = this.firstY = 0;
	this.secondX = this.secondY = 0;

	return true;
}

var clickSM;


///
// SettingsForm Data For Loading Files

function SettingsFormData(
			startTime, 
			duration, 
			endTime, 
			loadAudio, 
			spectraNumFramesPerSecond, 
			spectraNumFrequencyBands, 
			spectraDampingFactor,
			gain, 
			spectraMinimumBandFrequency,
			spectraMaximumBandFrequency
/*			sensorArrayLayout,
			tagSets,
*/
			) 
{
	this.startTime = startTime;  // Date Object
	this.duration = duration;    // Float (seconds)
	this.endTime = endTime;      // Date Object

	this.loadAudio = loadAudio;

	this.spectraNumFramesPerSecond = spectraNumFramesPerSecond;
	this.spectraNumFrequencyBands = spectraNumFrequencyBands;
	this.spectraDampingFactor = spectraDampingFactor;
	this.gain = gain;
	this.spectraMinimumBandFrequency = spectraMinimumBandFrequency;
	this.spectraMaximumBandFrequency = spectraMaximumBandFrequency;

///
// Debug Data
console.log("Created SettingsFormData with:" + 
    "\nStartTime: " + startTime + 
    "\nEndTime: "   + endTime + 
    "\nDuration: "  + duration + 
    "\nLoadAudio: " + loadAudio + 
	"\nspectraNumFramesPerSecond: " + spectraNumFramesPerSecond + 
	"\nspectraNumFrequencyBands: " + spectraNumFrequencyBands + 
	"\nspectraDampingFactor: " + spectraDampingFactor
    );

	return true;
};

var plotSettingsFormData;

//////////////////////////////////////////
//                                      //
//        Load and Display Files        //
//                                      //
//////////////////////////////////////////


///
// Called with a Valid SettingsFormData object to get the list of applicable files
// @return True on Success, False otherwise

function getFilesList(settingsFormData) {

	// ensure we have a valid SettingsFormData instance
	if ( ! (settingsFormData instanceof SettingsFormData)) {
		ARLOLogError("Invalid settingsFormData received by getFileList()");
		return false;
	}


	MultiViewStatusUpdate("getFilesList has valid form, continuing...");

	///
	// Set the current plotSettingsFormData

	plotSettingsFormData = settingsFormData;

	///
	// Get a list of Files that corresponds to these Dates
	
	var startDateString = formatDate(settingsFormData.startTime, "yyyy-MM-dd HH:mm:ss");
	var endDateString = formatDate(settingsFormData.endTime, "yyyy-MM-dd HH:mm:ss"); 

	var url = urlPrefix + "/api/v0/searchMediaFilesByRealTime/" + 
	          projectId + 
	          "/?startDate=" + startDateString + 
	          "&endDate=" + endDateString;

	myARLOModalStatus.setStatus("<img src='" + LOADING_GIF_URL + "' width='35px' height='35px'/><br /><strong>Retrieving File List...</strong>", true);

	$.ajax({
		url: url,
		dataType: 'json',
		error: function(request, textStatus, errorThrown) { 
			var sMsg = "Failed AJAX GET in getFilesList()";
			if ( ! (textStatus == undefined)) { sMsg += " (" + textStatus + ")"; }
			ARLOLogError(sMsg);
			},
		success: getFilesListCallback
	});

	return true;
};

/// 
// We have received a list of files back that we are going to display
//
// @param json JSON response from API with our data, see /api/v0/searchMediaFilesByRealTime/

function getFilesListCallback(json) {

MultiViewStatusUpdate("getFilesListCallback got json");
console.log("getFilesListCallback JSON: " + json);

	myARLOModalStatus.close();

	var audioFileIds = [];
	var audioFileAliases = [];
	var audioFileSensors = [];
	var audioFileSensorArrays = [];

	for (var i=0; i<json.length; i++) {

		audioFileIds.push(json[i].id);
		audioFileAliases.push(json[i].alias);
		audioFileSensors.push(json[i].sensorId);
		audioFileSensorArrays.push(json[i].sensorArrayId);
	}

	loadFiles(audioFileIds, audioFileAliases, audioFileSensors, audioFileSensorArrays);
};

// 
// @param audioFileIds List of MediaFileIds to load
// @param audioFileAliases List of Aliases corresponding to the audioFileIds
// @param audioFileSensors List of SensorIds corresponding to the audioFileIds
// @param audioFileSensorArrays List of SensorArrayIds (which the Sensor is in) corresponding to the audioFileIds

function loadFiles(audioFileIds, audioFileAliases, audioFileSensors, audioFileSensorArrays) {

MultiViewStatusUpdate("Start loadFiles");
MultiViewStatusUpdate("audioFileIds:" + audioFileIds);
MultiViewStatusUpdate("audioFileAliases:" + audioFileAliases);
MultiViewStatusUpdate("audioFileSensors:" + audioFileSensors);
MultiViewStatusUpdate("audioFileSensorArrays:" + audioFileSensorArrays);
	
	clickSM = new ClickStateMachine();  // reset state machine

	////////////////////
	// empty images div

	var masterNode = $("#audioFilesMasterDiv");
	masterNode.empty();

	// empty audioFileProperties
	audioFileProperties = [];
	
	////////////////
	// load and display files

	for (var i = 0; i < audioFileIds.length; i++) {
MultiViewStatusUpdate("loadFiles loop (" + i + ")");
		var currentFileId = audioFileIds[i];
		var currentFileAlias = audioFileAliases[i];
		var currentSensorId = audioFileSensors[i];
		var currentSensorArrayId = audioFileSensorArrays[i];

		// add new <div> to master
		var newNode = document.createElement('div');
		newNode.setAttribute('class','collapsible_header');
		newNode.setAttribute('id', "audioFile_" + currentFileId);
		newNode.setAttribute('style', "position: relative");
		newNode.innerHTML="<h3>" + currentFileAlias + " - " + currentFileId + "</h3>";
		masterNode.append(newNode);

		var collapseDiv = document.createElement('div');
		collapseDiv.setAttribute('id', "audioFileCollapse_" + currentFileId);
		collapseDiv.setAttribute('class','collapsible_body');
		collapseDiv.setAttribute('style', "position: relative");
		newNode.appendChild(collapseDiv);

		////
		// Setup Audio Player

		var audioPlayer;
		var audioDownload;

		if (plotSettingsFormData.loadAudio) {
			audioPlayer = new AudioPlayer(
				"audioPlayer_" + currentFileId, 
				EMPTY_WAV_URL,
				collapseDiv
			);

			audioDownload = document.createElement('a');
			audioDownload.setAttribute('id', "audioDownload_" + currentFileId);
			audioDownload.innerHTML = "Right-Click to Download This WAV Segment";
			collapseDiv.appendChild(audioDownload);
		}
		
		var imgDiv = document.createElement('div');
		imgDiv.setAttribute('id', "audioFileImg_" + currentFileId);
		imgDiv.setAttribute('class', 'spectraImage');
		collapseDiv.appendChild(imgDiv);

		// use selector to get the JQuery selector return - using the node fails with existing tag.js code
		imgDivNode = $("#audioFileImg_" + currentFileId);

		audioFileProperties[i] = new AudioFileProperties(currentFileId, newNode, imgDivNode, audioPlayer, audioDownload, currentSensorId, currentSensorArrayId);

		/////////////
		// get image

		// use the SettingsForm to issues POSTs to generate the spectra images

		// Serialize the SettingsForm into a dictionary to use as the $.ajax parameters

		// var form = $('#settingsForm');
		var form = document.forms.settingsForm;
		var first = 1;
		var data = {};
		for(var n = 0; n < form.elements.length; n++)
		{
			if (form.elements[n].type != "submit") {
			//  if (first) {
			//    first = 0;
			//  } else {
			//    data = data + ",";
			//  }
			//data = data + form.elements[n].name + ": '" + form.elements[n].value + "'";
			data[form.elements[n].name] = form.elements[n].value;
			}
		}
		data = $('#settingsForm').serialize(); 

		$.ajax({
			url: urlPrefix + "/tools/createAudioSpectra/" + currentFileId,
			type: 'post',
			context: audioFileProperties[i],
			data: data,
			dataType: 'json',
			error: function(request, errordata, errorObject) { 
					ARLOLogError("Failed createAudioSpectra: " + errorObject.toString()); 
				},
			success: function(json) {
				// image width
				if (imageWidth == 0) {
					imageWidth = json.imageWidth;
				} else {
					if (imageWidth != json.imageWidth) {
						console.log("ACHTUNG! Image Widths Don't Match");
					}
				}
				if (imageHeight == 0) {
					imageHeight = json.imageHeight;
				} else {
					if (imageHeight != json.imageHeight) {
						console.log("Avertissement! Image Heights Don't Match");
					}
				}

				myAudioFileProperties = this;
				myAudioFileProperties.imageWidth = json.imageWidth;
				myAudioFileProperties.imageHeight = json.imageHeight;
				myAudioFileProperties.minFrequency = json.minFrequency;
				myAudioFileProperties.maxFrequency = json.maxFrequency;
				myAudioFileProperties.spectraStartSeconds = json.spectraStartSeconds;
				myAudioFileProperties.spectraEndSeconds = json.spectraEndSeconds;
				myAudioFileProperties.fileEndSeconds = json.fileEndSeconds;

				if (plotSettingsFormData.loadAudio) {
					myAudioFileProperties.audioPlayer.setFile(urlPrefix + '/site_media/' + json.audioSegmentFilePath, false);
					myAudioFileProperties.audioDownload.setAttribute('href', '/site_media/' + json.audioSegmentFilePath);
				}

				var imgDivNode = myAudioFileProperties.imageDiv[0];
				imgDivNode.setAttribute('style', "position: relative; background-image: url(" + urlPrefix + "/site_media/" + json.imageFilePath + "); width: " + json.imageWidth + "px; height: " + json.imageHeight + "px; "); 

				// draw tags for this file
				drawTags(myAudioFileProperties);
			}
		});  // load image ajax

		// couldn't figure out how to get a context out of an ajaxSubmit - so screw it for now
//		$('#settingsForm').ajaxSubmit({
//          url: urlPrefix + "/tools/createAudioSpectra/" + audioFileIds[i],
//          type: 'POST',
//          context: newNode,
//          beforeSubmit: function() { alert('b4'); },
//          success: function(json) { 
//            var newImg = document.createElement('img');
//            newImg.setAttribute('src', urlPrefix + "/site_media/" + json.imageFilePath;
//            $(this).append(newImg);
//            alert('Run on Success'); 
//          },
//          error: function(request, errordata, errorObject) { alert(errorObject.toString()); },
//          dataType: 'json'
//          });
		
	}

	////////////////////////////////////////
	// setup collapsible headers for images

	$(".collapsible_header").click(function() {
		//var toSlide = $(this).next("collapsible_body");
		$(".collapsible_body", $(this)).slideToggle(100); //find the collapsible_body class div in this parent div
	});

	// disable clicks from body from propagating through
	$(".collapsible_body").click(function(e) {
		// return false;  /* using false breaks the submit buttons on the pop-up forms */
		e.stopPropagation();
	});

	// for drawing tags
	$(".spectraImage").click( function(event) {point_it(event);});


	/////////////	
	// Draw Tags

	// drawTags(); // now called for individual images in above ajax

	////////////////////////////
	// Setup SensorArray Layout
	LayoutAsSensorArraySetup();


}


function drawTags(myAudioFileProperties){
	// now called for individual images as they load

	// build URL
	url = urlPrefix + "/tools/getTagsInAudioSegment/";
	url += myAudioFileProperties.audioFileId + "/";
	url += myAudioFileProperties.spectraStartSeconds + "/";
	url += myAudioFileProperties.spectraEndSeconds + "/";
	url += myAudioFileProperties.minFrequency + "/";
	url += myAudioFileProperties.maxFrequency;
	
	// get and draw
	$.ajax({
		url: url, 
		context: myAudioFileProperties,
		dataType: 'json',
		error: function(request, errordata, errorObject) { 
				ARLOLogError("Error in getTagsInAudioSegment: " + errorObject.toString()); 
			},
		success: function(json){
			myProps = this;
			for(i=0;i < json.length;i++){

				// is this tag selected for display
				var selected = 0;
				$("#id_tagSets option:selected").each(function () {
					if ($(this)[0].value == json[i].tagSetId) {
						selected = 1;
					}
				});

				if (selected) {
					startTimePixels = timeToX(json[i].startTime, myProps.spectraStartSeconds, myProps.spectraEndSeconds, myProps.imageWidth);
					endTimePixels = timeToX(json[i].endTime, myProps.spectraStartSeconds, myProps.spectraEndSeconds, myProps.imageWidth);
					width = endTimePixels - startTimePixels;
					minFrequencyPixels = freqToY(json[i].minFrequency, myProps.minFrequency, myProps.maxFrequency, myProps.imageHeight);
					maxFrequencyPixels = freqToY(json[i].maxFrequency, myProps.minFrequency, myProps.maxFrequency, myProps.imageHeight);
					height = minFrequencyPixels - maxFrequencyPixels;

					// Tag(id, userTagged, machineTagged, parentDiv, displayName)
					t= new Tag(json[i].id, json[i].userTagged, json[i].machineTagged, myProps.imageDiv, json[i].displayName);
					if(t.id == currentTagID){
						border = 3
					} else {
						border = 1
					}
					t.draw(maxFrequencyPixels, startTimePixels, width, height, border);
				}  // selected
			}  // for

		}  // success
	});

};

function point_it(event){
	// should be 'basic' or 'adv'
	taggingMethod = $("#taggingMethod").val();
	
	targetImage = event.target;
	
	// Maybe we clicked on a Tag within an image...
	// Set to the parent
	if ( $(targetImage).hasClass("tags")) {
		targetImage = $(targetImage).parent()[0];
	}

	// Did we click on an image?
	if (! $(targetImage).hasClass('spectraImage')) {
		return;
	}

	// figure out which file was clicked on
	targetAudioId = (targetImage.id).split('_', 2)[1];
	var targetAudioFileProperties; 
	for (i = 0; i < audioFileProperties.length; i++) {
		if (audioFileProperties[i].audioFileId == targetAudioId) {
			targetAudioFileProperties = audioFileProperties[i];
		}
	}
	
	if (targetAudioFileProperties == undefined) {
		ARLOLogError("Audio File Not Found For Image");
		return;
	}

// at this point we have 
// - targetImage
// - targetAudioFileProperties

	/////////////////////////////////
	// get position clicked on image
	// if event object has offsetX, use that, otherwise compute from page clicked position and image offsets
	// can't seem to find a browser that supports offsetX
	// looks to be IE8 and 9 but 8 is failing for a variety of other reasons when I test
	// disable for now

//	if (event.offsetX || event.offsetY) {
//		pos_x = event.offsetX;
//		pos_y = event.offsetY;
//	} else {
		// compute position of the image

		imgLeft = 0;
		imgTop = 0;
		curObj = targetAudioFileProperties.imageDiv[0];

		do {
			imgLeft += curObj.offsetLeft;
			imgTop += curObj.offsetTop;
		} while (curObj = curObj.offsetParent);

		pos_x = event.pageX - imgLeft;
		pos_y = event.pageY - imgTop;
//	}

	////////////////////////
	// update state machine

	if ((! clickSM.audioFileProperties) || (clickSM.audioFileProperties.audioFileId != targetAudioFileProperties.audioFileId)) {
		// reset state machine
		clickSM.audioFileProperties = targetAudioFileProperties;
		clickSM.haveFirstPoint = false;
		clickSM.haveSecondPoint = false;
		fx();
	}

	if (! isValid(pos_x, pos_y, clickSM.audioFileProperties)) {
		// click outside of spectra - bail out
		console.log("Click in Invalid Location\n");
		return false;
	}

	// first click
	if (!(clickSM.haveFirstPoint) && !(clickSM.haveSecondPoint)) {
		clickSM.firstX = pos_x;
		if(taggingMethod == 'adv'){
			clickSM.firstY = pos_y;
		}
		else if(taggingMethod == 'basic'){
			clickSM.firstY = freqToY(targetAudioFileProperties.maxFrequency,targetAudioFileProperties.minFrequency,targetAudioFileProperties.maxFrequency, targetAudioFileProperties.imageHeight);
		}

		clickSM.haveFirstPoint = true;
		fx();
	// second click
	} else if ( (clickSM.haveFirstPoint) && !(clickSM.haveSecondPoint) ) {
		clickSM.secondX = pos_x;
		
		if(taggingMethod == 'adv'){
			clickSM.secondY = pos_y;
		}
		else if(taggingMethod == 'basic') {
			clickSM.secondY = freqToY(targetAudioFileProperties.minFrequency,targetAudioFileProperties.minFrequency,targetAudioFileProperties.maxFrequency, targetAudioFileProperties.imageHeight);
		}

		var myStartTime, myEndTime;
		if (clickSM.firstX < clickSM.secondX) {
			myStartTime = xToTime(clickSM.firstX,clickSM.audioFileProperties.spectraStartSeconds,clickSM.audioFileProperties.spectraEndSeconds, clickSM.audioFileProperties.imageWidth);
			myEndTime   = xToTime(clickSM.secondX,clickSM.audioFileProperties.spectraStartSeconds,clickSM.audioFileProperties.spectraEndSeconds, clickSM.audioFileProperties.imageWidth);
		} else {
			myStartTime = xToTime(clickSM.secondX,clickSM.audioFileProperties.spectraStartSeconds,clickSM.audioFileProperties.spectraEndSeconds, clickSM.audioFileProperties.imageWidth);
			myEndTime   = xToTime(clickSM.firstX,clickSM.audioFileProperties.spectraStartSeconds,clickSM.audioFileProperties.spectraEndSeconds, clickSM.audioFileProperties.imageWidth);
		}

		var myMinFreq, myMaxFreq;
		if (clickSM.firstY < clickSM.secondY) {
			myMinFreq = yToFreq(clickSM.secondY,clickSM.audioFileProperties.minFrequency,clickSM.audioFileProperties.maxFrequency, clickSM.audioFileProperties.imageHeight);
			myMaxFreq =  yToFreq(clickSM.firstY,clickSM.audioFileProperties.minFrequency,clickSM.audioFileProperties.maxFrequency, clickSM.audioFileProperties.imageHeight);
		} else { 
			myMinFreq = yToFreq(clickSM.firstY,clickSM.audioFileProperties.minFrequency,clickSM.audioFileProperties.maxFrequency, clickSM.audioFileProperties.imageHeight);
			myMaxFreq =  yToFreq(clickSM.secondY,clickSM.audioFileProperties.minFrequency,clickSM.audioFileProperties.maxFrequency, clickSM.audioFileProperties.imageHeight);
		}

		if (clickSM.firstY == clickSM.secondY || clickSM.firstX == clickSM.secondX) {
			// alert("This tag has no area. Please try again.");
			if(window.console&&window.console.log){
				window.console.log('This tag has no area. Please try again.');
			}
		} else {
			$("#id_audioFileId").val(clickSM.audioFileProperties.audioFileId);
			$("#id_tagStartTime").val(myStartTime);
			$("#id_tagEndTime").val(myEndTime);
			$("#id_tagMinFrequency").val(myMinFreq);
			$("#id_tagMaxFrequency").val(myMaxFreq);
			if ($("#id_createTagDuration") != null) {
				$("#id_createTagDuration").val(myEndTime - myStartTime);
			} else {
				alert("Doesn't Exist");
			}
			clickSM.haveSecondPoint = true;
			fx();
		} 
	}  // second Click
	else { // first and second point picked, clear points
		$("#id_tagStartTime").val(0);
		$("#id_tagMaxFrequency").val(0);
		$("#id_tagEndTime").val(0);
		$("#id_tagMinFrequency").val(0);
		if ($("#id_createTagDuration") != null) {
			$("#id_createTagDuration").val('');
		} else {
			alert("Doesn't Exist");
		}
		clickSM.haveFirstPoint = false;
		clickSM.haveSecondPoint = false;
		fx();
	}  // first and second point picked, clear points
} // point_it (event)



////////////////////
// Helper Functions 

//transforms the mouse location x to the time location on the image
function xToTime(x, startTime, endTime, imageWidth)
{
	var x = x - borderWidth;
	var width = imageWidth - (2 * borderWidth);
	var duration = endTime - startTime;
	var time = Number(startTime) + (duration *(x/width));
	return time;
}

function timeToX(time, startTime, endTime, imageWidth)
{
	var width = imageWidth - (2*borderWidth);
	var duration = endTime - startTime;
	var x = (time-startTime)*(width/duration);
	x = x + borderWidth;
	return x;
}

function freqToY(frequency, minFrequency, maxFrequency, imageHeight)
{
	var range = Math.log(maxFrequency) - Math.log(minFrequency);
	var spectraHeight = imageHeight - (timeScaleHeight + topBorderHeight);
	var y = (spectraHeight/range) * Math.log(frequency/minFrequency);
	y = spectraHeight - y;
	y = y + topBorderHeight;
	return y;
}

//transforms the mouse location y into the frequency location on the image
function yToFreq(y, minFrequency, maxFrequency, imageHeight)
{
	var y = y - topBorderHeight;
	var spectraHeight = imageHeight - (timeScaleHeight + topBorderHeight);
	//invert y because pixels start from top and spectra starts from bottom
	y = (spectraHeight - y);
	var range = Math.log(maxFrequency) - Math.log(minFrequency);
	freq = minFrequency * Math.exp(range*(y/spectraHeight));
	return freq;
}

// returns true if mouse click is within spectra borders, false otherwise
// also check if point is before or after start of audio file
function isValid(x, y, myAudioFileProperties)
{
	xValid = (x > borderWidth) && (x < (imageWidth - borderWidth));
	yValid = (y > (timeScaleHeight+10))  && (y < (imageHeight - topBorderHeight+10));

	// TODO this will allow the selection of a point AFTER the file data - need to pass back the true file length with the JSON file data
	timeValid = 
		(xToTime(x, myAudioFileProperties.spectraStartSeconds, myAudioFileProperties.spectraEndSeconds, myAudioFileProperties.imageWidth) > 0)  // point after start
		&& 
		(xToTime(x, myAudioFileProperties.spectraStartSeconds, myAudioFileProperties.spectraEndSeconds, myAudioFileProperties.imageWidth) < myAudioFileProperties.fileEndSeconds);
	return xValid && yValid && timeValid;
}


//  var imageWidth;
//  var imageHeight;
//  var startTime;
//  var endTime;
//  var minFrequency;
//  var maxFrequency;
//  var borderWidth;
//  var topBorderHeight;
//  var timeScaleHeight;
//  var currentTagID;

//  var firstX = 0;
//  var firstY = 0;
//  var firstPoint = false;
//  var secondX = 0;
//  var secondY = 0;
//  var secondPoint = false;

//  var tagSetName = "{{tagSet.name}}";


//must add restrictions so it doesn't work if the user points off the spectra


// functions:
//   function point_it(event) - mouse_click on image

//   function xToTime(x) - 
//   function timeToX(time)
//   function yToFreq(y)
//   function freqToY(frequency)

//   function isValid(x,y) - returns true if mouse click is within spectra borders, false otherwise

//   function fx() 
//     - looks like this draws the selection box - called by point_it after start and end point selected

//   function drawTags() - gets tags in view, and draws them on the spectra
//   function spectraFormCallback(json)
//     - process new navigation request - done instead of posting the Form

// function createTagFormCallback(json){
// function getAutoComplete(){
// function getSpectra(){
// function navigateFormCallback(json){



 

function fx()
{
	if (clickSM.haveFirstPoint && !(clickSM.haveSecondPoint)) {
		// only have first point
		// create start crosshairs
		imgDiv = clickSM.audioFileProperties.imageDiv;
		imgDiv.append("<div id=startLineV></div>");
		$("#startLineV").css("border","1px solid #0f0");
		$("#startLineV").css("position","absolute");
		$("#startLineV").css("top",(clickSM.firstY - 4) + "px");
		$("#startLineV").css("left",(clickSM.firstX - 1) + "px");
		$("#startLineV").css("width","0px");
		$("#startLineV").css("height","6px");
		imgDiv.append("<div id=startLineH></div>");
		$("#startLineH").css("border","1px solid #0f0");
		$("#startLineH").css("position","absolute");
		$("#startLineH").css("top",(clickSM.firstY - 1) + "px");
		$("#startLineH").css("left",(clickSM.firstX - 4) + "px");
		$("#startLineH").css("width","6px");
		$("#startLineH").css("height","0px");
	} else if(clickSM.haveFirstPoint && clickSM.haveSecondPoint) {
		// have second point too
		// create end crosshairs
		imgDiv = clickSM.audioFileProperties.imageDiv;
		imgDiv.append("<div id=endLineV></div>");
		$("#endLineV").css("border","1px solid #0f0");
		$("#endLineV").css("position","absolute");
		$("#endLineV").css("top",(clickSM.secondY - 4) + "px");
		$("#endLineV").css("left",(clickSM.secondX - 1) + "px");
		$("#endLineV").css("width","0px");
		$("#endLineV").css("height","6px");
		imgDiv.append("<div id=endLineH></div>");
		$("#endLineH").css("border","1px solid #0f0");
		$("#endLineH").css("position","absolute");
		$("#endLineH").css("top",(clickSM.secondY - 1) + "px");
		$("#endLineH").css("left",(clickSM.secondX - 4) + "px");
		$("#endLineH").css("width","6px");
		$("#endLineH").css("height","0px");

		//creates box to show tag
		imgDiv.append("<div id='pointer' style='background-color:#fff;-moz-opacity=.50'></div>")
		$("#pointer").addClass('opacity50');
		$("#pointer").css("background-color","#fff");
//		$("#pointer").css("opacity","0,50");
		$("#pointer").css("position","absolute");
		
		topPoint = clickSM.firstY < clickSM.secondY ? clickSM.firstY - 1 : clickSM.secondY - 1;
		leftPoint = clickSM.firstX < clickSM.secondX ? clickSM.firstX - 1 : clickSM.secondX - 1;
		width = clickSM.firstX < clickSM.secondX ? clickSM.secondX - clickSM.firstX + 2 : clickSM.firstX - clickSM.secondX + 2; 
		height = clickSM.firstY < clickSM.secondY ? clickSM.secondY - clickSM.firstY + 2 : clickSM.firstY - clickSM.secondY + 2;

		$("#pointer").css("top",(topPoint) + "px");
		$("#pointer").css("left",(leftPoint) + "px");
		$("#pointer").css("width",(width) + "px");
		$("#pointer").css("height",(height) + "px");
	} else {
		//removes tag
		$("#pointer").remove();
		$("#startLineV").remove();
		$("#startLineH").remove();
		$("#endLineV").remove();
		$("#endLineH").remove();
	}
};


function spectraFormCallback(json){
	imageWidth = json.imageWidth;
	imageHeight = json.imageHeight;
	startTime = json.startTime;
	endTime = json.endTime;
	minFrequency = json.minFrequency;
	maxFrequency = json.maxFrequency;
	borderWidth = json.borderWidth;
	topBorderHeight = json.topBorderHeight;
	timeScaleHeight = json.timeScaleHeight;

	if(json.action == 'time') {
		$("#id_startTime").val(json.firstParam);
		$("#id_endTime").val(json.secondParam);
		$("#id_spectraNumFramesPerSecond").val(json.thirdParam);
		$("#id_navStartTime").val(json.firstParam);
		$("#id_navEndTime").val(json.secondParam);
	} else if(json.action == 'frequencyBands') {
		$("#id_spectraNumFrequencyBands").val(json.firstParam);
	} else if(json.action == 'dampingFactor') {
		$("#id_spectraDampingFactor").val(json.firstParam);
	}

	$("#image").css('background-image','url(/site_media/' + json.imageFilePath + ')');
	$("#image").css('width',imageWidth+'px');
	$("#image").css('height',imageHeight+'px');
	$("#audioSegment").attr('src', '/site_media/' + json.audioSegmentFilePath);
	$("#audioSegment").attr('width', json.playerWidth); 
	if (document.getElementById("audioSegmentDownload")) {
		$("#audioSegmentDownload").attr('href', '/site_media/' + json.audioSegmentFilePath);
	}

	drawTags();
};

function createTagFormCallback(json){
	if (json.Success) {
		ARLOLogNotify(json.saveMessage);

		$("#pointer").remove();
		$("#startLineV").remove();
		$("#startLineH").remove();
		$("#endLineV").remove();
		$("#endLineH").remove();

		clickSM.haveFirstPoint = false;
		clickSM.haveSecondPoint = false;

		// in case new class added, need to add it to auto-complete
		getAutoComplete();

		drawTags(clickSM.audioFileProperties);
	} else {
		ARLOLogWarning(json.saveMessage);
	}

	if (typeof createTagFormCallback_Success == 'function') {
		createTagFormCallback_Success(json.Success, json.saveMessage, json);
	}
};


function getAutoComplete() {
	$.getJSON(urlPrefix + "/tools/getTagClassAutoArray/" + projectId, function(json){
		tagClasses = json;
		$("#id_className").autocomplete({source: tagClasses});
	});
};











// auto-submits the form on load - not desired here I don't think

// function getSpectra(){
//   $("#spectraForm").ajaxSubmit({
//     url:urlPrefix + '/tools/createAudioSpectra',
//     dataType:'json',
//     success:spectraFormCallback
//   });
// };

function navigateFormCallback(json){
	currentTagID = json.tagID;
	$("#id_currentTagID").val(json.tagID);
	$("#id_startTime").val(json.startTime);
	$("#id_endTime").val(json.endTime);
	$("#id_navStartTime").val(json.startTime);
	$("#id_navEndTime").val(json.endTime);
	getSpectra();
};

////////////////////////////////
//                            //
//     SensorArray Layout     //
//                            //
////////////////////////////////

function SensorArrayLayoutProperties(arrayId, numRows, numCols, sensors) {
	this.arrayId = arrayId;
	this.numRows = numRows;
	this.numCols = numCols;
	this.sensors = sensors;
};

function SensorProperties(id, name, x, y, z, layoutRow, layoutCol) {
	this.id = id;
	this.name = name;
	this.x = x;
	this.y = y;
	this.z = z;
	this.layoutRow = layoutRow;
	this.layoutCol = layoutCol;
};

var sensorArrayLayoutProperties = null;

function GetSensorX(sensorId) {
	if (sensorArrayLayoutProperties) {
		for (var i=0; i < sensorArrayLayoutProperties.sensors.length; i++) {
			if (sensorArrayLayoutProperties.sensors[i].id == sensorId) {
				return sensorArrayLayoutProperties.sensors[i].layoutCol;
			}
		}
	}
	return null;
};

function GetSensorY(sensorId) {
	if (sensorArrayLayoutProperties) {
		for (var i=0; i < sensorArrayLayoutProperties.sensors.length; i++) {
			if (sensorArrayLayoutProperties.sensors[i].id == sensorId) {
				return sensorArrayLayoutProperties.sensors[i].layoutRow;
			}
		}
	}
	return null;
};

function ToggleLayoutAsSensorArray() {
	// event handler for id_sensorArrayLayout
	LayoutAsSensorArraySetup();
};

function LayoutAsSensorArraySetup() {
	var asSensorArrayLayout = $("#id_sensorArrayLayout")[0].checked;

	if (asSensorArrayLayout) {
		// setup SensorLayout Array

		sensorArrayLayoutProperties = new SensorArrayLayoutProperties(-1, 0, 0, []);

		//////////
		// figure out which sensor arrays are in use

		for (var i=0; i < audioFileProperties.length; i++) {
			if (audioFileProperties[i].sensorArrayId == null) {
				// numNullSensors += 1;
			} else {
				if (sensorArrayLayoutProperties.arrayId == -1) {
					sensorArrayLayoutProperties.arrayId = audioFileProperties[i].sensorArrayId;
				} else {
					if (sensorArrayLayoutProperties.arrayId != audioFileProperties[i].sensorArrayId) {
						// multiple sensor arrays in use... bail out
						ArloLogWarning("Multiple Sensor Arrays Found... Bailing out!");
						sensorArrayLayoutProperties = null;
						LayoutAsSensorArray();
						return;
					}
				}
			}
		}

		if (sensorArrayLayoutProperties.arrayId == -1) {
			ARLOLogWarning("No SensorArrays Found");
			sensorArrayLayoutProperties = null;
			LayoutAsSensorArray();
			return;
		} 

		// if here, then only one Array is in use, so safe to proceed

		///////////
		// get SensorArray info
		$.ajax({
			url: urlPrefix + "/api/v0/sensorArray/" + sensorArrayLayoutProperties.arrayId + "/",
			dataType: 'json',
			error: function(request, errordata, errorObject) {
				ARLOLogError("Error getting SensorArray data!");
				sensorArrayLayoutProperties = null;
				LayoutAsSensorArray();
			},
			success: function(json){
				if (sensorArrayLayoutProperties.arrayId != json.id) {
					ARLOLogError("sensorArrayId doesn't match JSON... bailing out!");
					sensorArrayLayoutProperties = null;
				} else {
					var sensors = [];
					for (var i=0; i<json.sensor_set.length; i++) {
						sensors.push(new SensorProperties(
							json.sensor_set[i].id,
							json.sensor_set[i].name,
							json.sensor_set[i].x,
							json.sensor_set[i].y,
							json.sensor_set[i].z,
							json.sensor_set[i].layoutRow,
							json.sensor_set[i].layoutColumn));
					}
					sensorArrayLayoutProperties = new SensorArrayLayoutProperties(
						json.id,
						json.layoutRows,
						json.layoutColumns,
						sensors);
				}
				LayoutAsSensorArray();
			}  // success
		});  // ajax


	} else {  // not SensorArray Layout
		sensorArrayLayoutProperties = null;
		LayoutAsSensorArray();
	}  // else SensorArray Layout
};

function LayoutAsSensorArray() {
	// this assumes LayoutAsSensorArraySetup() has been ran

	var audioFilesMasterDiv = document.getElementById('audioFilesMasterDiv');

	////////////////////////////
	// start out by placing all files in the master div

	// loop through files and place in master div
	for (var i=0; i < audioFileProperties.length; i++) {
		audioFilesMasterDiv.appendChild(audioFileProperties[i].audioFileDiv);
	}

	// if the table exists, delete it
	var layoutTable = document.getElementById('sensorArrayLayoutTable');
	if (layoutTable) {
		layoutTable.parentNode.removeChild(layoutTable);
	}

	if (sensorArrayLayoutProperties) {
		// have layout data, layout as SensorArray

		var rows = sensorArrayLayoutProperties.numRows;
		var cols = sensorArrayLayoutProperties.numCols;

		if (sensorArrayLayoutProperties.sensors.length < 1) {
			ARLOLogWarning("No Sensors Found for Array... bailing out!");
			return;
		}

		////////////
		// Build Table

		var innerHtml = "";
		for (var row = 0; row < rows; row++) {
			innerHtml += "\n<tr>";
			for (var col = 0; col < cols; col++) {
				innerHtml += "<td style='border:none;'></td>"
				}
			innerHtml += "</tr>";
		}
	
		var newTable = document.createElement('table');
		newTable.setAttribute('id', "sensorArrayLayoutTable");
		newTable.setAttribute('border', "9px");
		newTable.innerHTML=innerHtml;

		audioFilesMasterDiv.appendChild(newTable);

		///////////////////
		// loop through files and place in grid
		for (var i=0; i < audioFileProperties.length; i++) {
			if (audioFileProperties[i].sensorId) {
				var col = GetSensorX(audioFileProperties[i].sensorId);
				var row = GetSensorY(audioFileProperties[i].sensorId);
				
				if ((col != null) && (row != null) && (col >= 0) && (row >= 0)) {
					var myRow = newTable.rows[row];
					var myCell = myRow.cells[col];
					myCell.appendChild(audioFileProperties[i].audioFileDiv);
				}
			}
		}  // i < audioFileProperties.length
	}  // if (sensorArrayLayoutProperties)

};


//////////////////////////////////////////////////////
//                                                  //
//            Date/Time Helper Functions            //
//                                                  //
//////////////////////////////////////////////////////


// ===================================================================
// Author: Matt Kruse <matt@mattkruse.com>
// WWW: http://www.mattkruse.com/
//
// NOTICE: You may use this code for any purpose, commercial or
// private, without any further permission from the author. You may
// remove this notice from your final code if you wish, however it is
// appreciated by the author if at least my web site address is kept.
//
// You may *NOT* re-distribute this code in any way except through its
// use. That means, you can include it in your product, or your web
// site, or any other form where the code is actually being used. You
// may not put the plain javascript up on your site for download or
// include it in your javascript libraries for download. 
// If you wish to share this code with others, please just point them
// to the URL instead.
// Please DO NOT link directly to my .js files from your site. Copy
// the files to your server and use them there. Thank you.
// ===================================================================

// HISTORY
// ------------------------------------------------------------------
// May 17, 2003: Fixed bug in parseDate() for dates <1970
// March 11, 2003: Added parseDate() function
// March 11, 2003: Added "NNN" formatting option. Doesn't match up
//                 perfectly with SimpleDateFormat formats, but 
//                 backwards-compatability was required.

// ------------------------------------------------------------------
// These functions use the same 'format' strings as the 
// java.text.SimpleDateFormat class, with minor exceptions.
// The format string consists of the following abbreviations:
// 
// Field        | Full Form          | Short Form
// -------------+--------------------+-----------------------
// Year         | yyyy (4 digits)    | yy (2 digits), y (2 or 4 digits)
// Month        | MMM (name or abbr.)| MM (2 digits), M (1 or 2 digits)
//              | NNN (abbr.)        |
// Day of Month | dd (2 digits)      | d (1 or 2 digits)
// Day of Week  | EE (name)          | E (abbr)
// Hour (1-12)  | hh (2 digits)      | h (1 or 2 digits)
// Hour (0-23)  | HH (2 digits)      | H (1 or 2 digits)
// Hour (0-11)  | KK (2 digits)      | K (1 or 2 digits)
// Hour (1-24)  | kk (2 digits)      | k (1 or 2 digits)
// Minute       | mm (2 digits)      | m (1 or 2 digits)
// Second       | ss (2 digits)      | s (1 or 2 digits)
// AM/PM        | a                  |
//
// NOTE THE DIFFERENCE BETWEEN MM and mm! Month=MM, not mm!
// Examples:
//  "MMM d, y" matches: January 01, 2000
//                      Dec 1, 1900
//                      Nov 20, 00
//  "M/d/yy"   matches: 01/20/00
//                      9/2/00
//  "MMM dd, yyyy hh:mm:ssa" matches: "January 01, 2000 12:30:45AM"
// ------------------------------------------------------------------

var MONTH_NAMES=new Array('January','February','March','April','May','June','July','August','September','October','November','December','Jan','Feb','Mar','Apr','May','Jun','Jul','Aug','Sep','Oct','Nov','Dec');
var DAY_NAMES=new Array('Sunday','Monday','Tuesday','Wednesday','Thursday','Friday','Saturday','Sun','Mon','Tue','Wed','Thu','Fri','Sat');
function LZ(x) {return(x<0||x>9?"":"0")+x}

// ------------------------------------------------------------------
// isDate ( date_string, format_string )
// Returns true if date string matches format of format string and
// is a valid date. Else returns false.
// It is recommended that you trim whitespace around the value before
// passing it to this function, as whitespace is NOT ignored!
// ------------------------------------------------------------------
function isDate(val,format) {
	var date=getDateFromFormat(val,format);
	if (date==0) { return false; }
	return true;
	}

// -------------------------------------------------------------------
// compareDates(date1,date1format,date2,date2format)
//   Compare two date strings to see which is greater.
//   Returns:
//   1 if date1 is greater than date2
//   0 if date2 is greater than date1 of if they are the same
//  -1 if either of the dates is in an invalid format
// -------------------------------------------------------------------
function compareDates(date1,dateformat1,date2,dateformat2) {
	var d1=getDateFromFormat(date1,dateformat1);
	var d2=getDateFromFormat(date2,dateformat2);
	if (d1==0 || d2==0) {
		return -1;
		}
	else if (d1 > d2) {
		return 1;
		}
	return 0;
	}

// ------------------------------------------------------------------
// formatDate (date_object, format)
// Returns a date in the output format specified.
// The format string uses the same abbreviations as in getDateFromFormat()
// ------------------------------------------------------------------
function formatDate(date,format) {
	format=format+"";
	var result="";
	var i_format=0;
	var c="";
	var token="";
	var y=date.getYear()+"";
	var M=date.getMonth()+1;
	var d=date.getDate();
	var E=date.getDay();
	var H=date.getHours();
	var m=date.getMinutes();
	var s=date.getSeconds();
	var yyyy,yy,MMM,MM,dd,hh,h,mm,ss,ampm,HH,H,KK,K,kk,k;
	// Convert real date parts into formatted versions
	var value=new Object();
	if (y.length < 4) {y=""+(y-0+1900);}
	value["y"]=""+y;
	value["yyyy"]=y;
	value["yy"]=y.substring(2,4);
	value["M"]=M;
	value["MM"]=LZ(M);
	value["MMM"]=MONTH_NAMES[M-1];
	value["NNN"]=MONTH_NAMES[M+11];
	value["d"]=d;
	value["dd"]=LZ(d);
	value["E"]=DAY_NAMES[E+7];
	value["EE"]=DAY_NAMES[E];
	value["H"]=H;
	value["HH"]=LZ(H);
	if (H==0){value["h"]=12;}
	else if (H>12){value["h"]=H-12;}
	else {value["h"]=H;}
	value["hh"]=LZ(value["h"]);
	if (H>11){value["K"]=H-12;} else {value["K"]=H;}
	value["k"]=H+1;
	value["KK"]=LZ(value["K"]);
	value["kk"]=LZ(value["k"]);
	if (H > 11) { value["a"]="PM"; }
	else { value["a"]="AM"; }
	value["m"]=m;
	value["mm"]=LZ(m);
	value["s"]=s;
	value["ss"]=LZ(s);
	while (i_format < format.length) {
		c=format.charAt(i_format);
		token="";
		while ((format.charAt(i_format)==c) && (i_format < format.length)) {
			token += format.charAt(i_format++);
			}
		if (value[token] != null) { result=result + value[token]; }
		else { result=result + token; }
		}
	return result;
	}
	
// ------------------------------------------------------------------
// Utility functions for parsing in getDateFromFormat()
// ------------------------------------------------------------------
function _isInteger(val) {
	var digits="1234567890";
	for (var i=0; i < val.length; i++) {
		if (digits.indexOf(val.charAt(i))==-1) { return false; }
		}
	return true;
	}
function _getInt(str,i,minlength,maxlength) {
	for (var x=maxlength; x>=minlength; x--) {
		var token=str.substring(i,i+x);
		if (token.length < minlength) { return null; }
		if (_isInteger(token)) { return token; }
		}
	return null;
	}
	
// ------------------------------------------------------------------
// getDateFromFormat( date_string , format_string )
//
// This function takes a date string and a format string. It matches
// If the date string matches the format string, it returns the 
// getTime() of the date. If it does not match, it returns 0.
// ------------------------------------------------------------------
function getDateFromFormat(val,format) {
	val=val+"";
	format=format+"";
	var i_val=0;
	var i_format=0;
	var c="";
	var token="";
	var token2="";
	var x,y;
	var now=new Date();
	var year=now.getYear();
	var month=now.getMonth()+1;
	var date=1;
	var hh=now.getHours();
	var mm=now.getMinutes();
	var ss=now.getSeconds();
	var ampm="";
	
	while (i_format < format.length) {
		// Get next token from format string
		c=format.charAt(i_format);
		token="";
		while ((format.charAt(i_format)==c) && (i_format < format.length)) {
			token += format.charAt(i_format++);
			}
		// Extract contents of value based on format token
		if (token=="yyyy" || token=="yy" || token=="y") {
			if (token=="yyyy") { x=4;y=4; }
			if (token=="yy")   { x=2;y=2; }
			if (token=="y")    { x=2;y=4; }
			year=_getInt(val,i_val,x,y);
			if (year==null) { return 0; }
			i_val += year.length;
			if (year.length==2) {
				if (year > 70) { year=1900+(year-0); }
				else { year=2000+(year-0); }
				}
			}
		else if (token=="MMM"||token=="NNN"){
			month=0;
			for (var i=0; i<MONTH_NAMES.length; i++) {
				var month_name=MONTH_NAMES[i];
				if (val.substring(i_val,i_val+month_name.length).toLowerCase()==month_name.toLowerCase()) {
					if (token=="MMM"||(token=="NNN"&&i>11)) {
						month=i+1;
						if (month>12) { month -= 12; }
						i_val += month_name.length;
						break;
						}
					}
				}
			if ((month < 1)||(month>12)){return 0;}
			}
		else if (token=="EE"||token=="E"){
			for (var i=0; i<DAY_NAMES.length; i++) {
				var day_name=DAY_NAMES[i];
				if (val.substring(i_val,i_val+day_name.length).toLowerCase()==day_name.toLowerCase()) {
					i_val += day_name.length;
					break;
					}
				}
			}
		else if (token=="MM"||token=="M") {
			month=_getInt(val,i_val,token.length,2);
			if(month==null||(month<1)||(month>12)){return 0;}
			i_val+=month.length;}
		else if (token=="dd"||token=="d") {
			date=_getInt(val,i_val,token.length,2);
			if(date==null||(date<1)||(date>31)){return 0;}
			i_val+=date.length;}
		else if (token=="hh"||token=="h") {
			hh=_getInt(val,i_val,token.length,2);
			if(hh==null||(hh<1)||(hh>12)){return 0;}
			i_val+=hh.length;}
		else if (token=="HH"||token=="H") {
			hh=_getInt(val,i_val,token.length,2);
			if(hh==null||(hh<0)||(hh>23)){return 0;}
			i_val+=hh.length;}
		else if (token=="KK"||token=="K") {
			hh=_getInt(val,i_val,token.length,2);
			if(hh==null||(hh<0)||(hh>11)){return 0;}
			i_val+=hh.length;}
		else if (token=="kk"||token=="k") {
			hh=_getInt(val,i_val,token.length,2);
			if(hh==null||(hh<1)||(hh>24)){return 0;}
			i_val+=hh.length;hh--;}
		else if (token=="mm"||token=="m") {
			mm=_getInt(val,i_val,token.length,2);
			if(mm==null||(mm<0)||(mm>59)){return 0;}
			i_val+=mm.length;}
		else if (token=="ss"||token=="s") {
			ss=_getInt(val,i_val,token.length,2);
			if(ss==null||(ss<0)||(ss>59)){return 0;}
			i_val+=ss.length;}
		else if (token=="a") {
			if (val.substring(i_val,i_val+2).toLowerCase()=="am") {ampm="AM";}
			else if (val.substring(i_val,i_val+2).toLowerCase()=="pm") {ampm="PM";}
			else {return 0;}
			i_val+=2;}
		else {
			if (val.substring(i_val,i_val+token.length)!=token) {return 0;}
			else {i_val+=token.length;}
			}
		}
	// If there are any trailing characters left in the value, it doesn't match
	if (i_val != val.length) { return 0; }
	// Is date valid for month?
	if (month==2) {
		// Check for leap year
		if ( ( (year%4==0)&&(year%100 != 0) ) || (year%400==0) ) { // leap year
			if (date > 29){ return 0; }
			}
		else { if (date > 28) { return 0; } }
		}
	if ((month==4)||(month==6)||(month==9)||(month==11)) {
		if (date > 30) { return 0; }
		}
	// Correct hours value
	if (hh<12 && ampm=="PM") { hh=hh-0+12; }
	else if (hh>11 && ampm=="AM") { hh-=12; }
	var newdate=new Date(year,month-1,date,hh,mm,ss);
	return newdate.getTime();
	}

// ------------------------------------------------------------------
// parseDate( date_string [, prefer_euro_format] )
//
// This function takes a date string and tries to match it to a
// number of possible date formats to get the value. It will try to
// match against the following international formats, in this order:
// y-M-d   MMM d, y   MMM d,y   y-MMM-d   d-MMM-y  MMM d
// M/d/y   M-d-y      M.d.y     MMM-d     M/d      M-d
// d/M/y   d-M-y      d.M.y     d-MMM     d/M      d-M
// A second argument may be passed to instruct the method to search
// for formats like d/M/y (european format) before M/d/y (American).
// Returns a Date object or null if no patterns match.
// ------------------------------------------------------------------
function parseDate(val) {
	var preferEuro=(arguments.length==2)?arguments[1]:false;
	generalFormats=new Array('y-M-d','MMM d, y','MMM d,y','y-MMM-d','d-MMM-y','MMM d');
	monthFirst=new Array('M/d/y','M-d-y','M.d.y','MMM-d','M/d','M-d');
	dateFirst =new Array('d/M/y','d-M-y','d.M.y','d-MMM','d/M','d-M');
	var checkList=new Array('generalFormats',preferEuro?'dateFirst':'monthFirst',preferEuro?'monthFirst':'dateFirst');
	var d=null;
	for (var i=0; i<checkList.length; i++) {
		var l=window[checkList[i]];
		for (var j=0; j<l.length; j++) {
			d=getDateFromFormat(val,l[j]);
			if (d!=0) { return new Date(d); }
			}
		}
	return null;
	}

