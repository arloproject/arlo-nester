
function Tag(id, userTagged, machineTagged, parentDiv, displayName) {
	//type properties
	this.id = id;
	this.userTagged = userTagged;
	this.machineTagged = machineTagged;
	//position properties
	this.parentDiv = parentDiv;
	this.displayName = displayName;

	this.draw = function(top, left, width, height, border){
		this.parentDiv.append("<div class='tags' id=" + this.id + "></div>");
		this.div = $("#" + this.id);
		this.div.addClass('tags');
		this.div.addClass('tagDivContextMenu');
		this.div.css('position', 'absolute');
		this.div.css('top', top);
		this.div.css('left', left);
		this.div.width(width);
		this.div.height(height);
		this.div.append("<div id=displayName" + this.id + "></div");

		this.displayNameDiv = $("#displayName" + this.id);
		this.displayNameDiv.css("float", "left");
		if (this.userTagged && this.machineTagged){
			this.div.css('border', '#FF00FF '+border+'px solid');
			this.displayNameDiv.append("<font size='0' color='#FF00FF'><b>" + this.displayName + "</b></font>");
		} else if(this.userTagged && !this.machineTagged) {
			this.div.css('border', '#00FF00 '+border+'px solid');
			this.displayNameDiv.append("<font size='0' color='#00FF00'><b>" + this.displayName + "</b></font>");
		} else if(!this.userTagged && this.machineTagged) {
			this.div.css('border', '#FF0000 '+border+'px solid');
			this.displayNameDiv.append("<font size='0' color='#FF0000'><b>" + this.displayName + "</b></font>");
		}


		this.div.dblclick(function(){
			$("body").append("<a id='pop'></a>");
			$('#pop').popupWindow({
				windowURL:"/tools/viewTag/" + this.id,
				height:500,
				width:600,
				top:50,
				left:50
			});
			$('#pop').click();
			$('#pop').remove();
		});
	};

	this.display = function(imagePath) {
		this.parentDiv.append("<div class='tags' id=" + this.id + "></div>");
		this.div = $("#" + this.id);
		this.div.append("<img src='" + imagePath + "'></img>");
		this.div.addClass('tags');

		this.div.append("<div id='displayName'" + this.id + "></div");
		this.displayNameDiv = $("#displayName" + this.id);
		this.displayNameDiv.css("float", "left");
		if (this.userTagged && this.machineTagged) {
			this.div.css('border', '#FF00FF 1px solid');
			this.displayNameDiv.append("<font size='0' color='#FF00FF'><b>" + this.displayName + "</b></font>");
		} else if (this.userTagged && !this.machineTagged) {
			this.div.css('border', '#00FF00 1px solid');
			this.displayNameDiv.append("<font size='0' color='#00FF00'><b>" + this.displayName + "</b></font>");
		} else if (!this.userTagged && this.machineTagged) {
			this.div.css('border', '#FF0000 1px solid');
			this.displayNameDiv.append("<font size='0' color='#FF0000'><b>" + this.displayName + "</b></font>");
		}

	};

	return true;
};


/////////////////////////////////////////////
// 
// Context Window
// 
// JavaScript Class to basically gray out the Context Window, portions of the 
// view not actually part of the desired spectra.
// 
// @param id A unique identifier for the new DOM object
// @param parentDiv Where this object will live.

function ContextWindow(id, parentDiv){
	//type properties
	this.id = id;

	//position properties
	this.parentDiv = parentDiv;

	this.draw = function(left, top, width, height, border){
		this.parentDiv.append("<div class='contextWindow' id=" + this.id + "></div>");
		this.div = $("#" + this.id);
		this.div.addClass('contextWindow');
		this.div.css('position', 'absolute');
		this.div.css('top', top);
		this.div.css('left', left);
		this.div.width(width);
		this.div.height(height);
		//this.div.css('border', '#0000FF '+border+'px solid');
		this.div.css('border', '#FFFFFF '+border+'px solid');

		// for the grayed-out version
//		this.div.css('background-color', '#FFFFFF');
		this.div.css('opacity', '0.4');
	};

	return true;
};


