//variables passed in through django
//  var audioID = '{{audioID}}';
//  var currentTagID;

//  var urlPrefix = "{{url_prefix}}";
//  var mediaUrl = "{{media_url}}";
//  // var tagSetName = "{{tagSet.name}}";

// functions:
//   function point_it(event) - mouse_click on image

//   function xToTime(x) -
//   function timeToX(time)
//   function yToFreq(y)
//   function freqToY(frequency)

//   function isValid(x,y) - returns true if mouse click is within spectra borders, false otherwise

//   function fx()
//     - looks like this draws the selection box - called by point_it after start and end point selected

//   function drawTags() - gets tags in view, and draws them on the spectra
//   function spectraFormCallback(json)
//     - process new navigation request - done instead of posting the Form

// function createTagFormCallback(json){
// function getAutoComplete(){
// function getSpectra(){
// function navigateFormCallback(json){


//////
// Callbacks
//
// Guess we need a more function API - include some Callbacks from these
// functions so the calling pages can extend upon them.
//
// I guess you could call this, a Callback "Callback" </dramatic look>
//
// createTagFormCallback_Success(success, saveMessage, rawJson)
//    - Called at the very end of a successful createTagFormCallback
//


////
// Script Variables

var spectraStartTime;
var spectraEndTime;
var formStartTime;
var formEndTime;
var contextWindowTime;

var minFrequency;
var maxFrequency;

var imageWidth;
var imageHeight;
var borderWidth;
var topBorderHeight;
var timeScaleHeight;

// whether to allow tagging in the Context Window portions of the spectra image
var allowContextWindowTagging = true;

// once the spectra image is loaded, tag (highlight) the entire window area
var tagWholeWindowOnLoad = false;

/////
// State machine for Tag Highlighting

var clickMode = "";
var adjustTagId = 0;
var firstX = 0;
var firstY = 0;
var firstPoint = false;
var secondX = 0;
var secondY = 0;
var secondPoint = false;

var savedTagStartTime = "0";
var savedTagEndTime = "0";
var savedTagMinFreq = "0";
var savedTagMaxFreq = "0";

/* Override Callbacks */

/* If set, don't use the built-in logic to set the Spectra parameters in the form */
/* Rather, pass the parameters to this function and let it do the work */
var setSpectraCallback = null; /* function (startTime, endTime, startFreq, endFreq) */


function point_it(event){
	taggingMethod = $("#taggingMethod").val();

	// this position code seems to be a continually evolving piece.
	// the goal is to get pos_x and pos_y which are relative to the image

  console.log(event)
  console.log(event.offsetX)

	if (event.type == "pseudoEvent") {
		// Manual event for programatically 'clicking' on the image
		pos_x = event.x;
		pos_y = event.y;
	} else {
    pos_x = event.offsetX;
		pos_y = event.offsetY;
//		if (event.offsetX) {
//			pos_x = event.offsetX;
//			pos_y = event.offsetY;
//		} else {
//			pos_x = event.pageX-document.getElementById("image").offsetLeft;
//			pos_y = event.pageY-document.getElementById("image").offsetTop;
//		}
	}


	if (!isValid(pos_x, pos_y))
		return;

	// check for first point
	if(!firstPoint) {
		firstX = pos_x;
		if(taggingMethod == 'adv'){
			firstY = pos_y;
		}
		else if(taggingMethod == 'basic'){
			firstY = freqToY(maxFrequency);
		}

		firstPoint = true;
		secondPoint = false;
		fx();
		return;
	}

	// check for second point
	if(firstPoint && !secondPoint)
	{
		secondX = pos_x;
		if(taggingMethod == 'adv') {
			secondY = pos_y;
		} else if(taggingMethod == 'basic') {
			secondY = freqToY(minFrequency);
		}

		var myStartTime, myEndTime;
		if (firstX < secondX) {
			myStartTime = xToTime(firstX);
			myEndTime   = xToTime(secondX);
		} else {
			myStartTime = xToTime(secondX);
			myEndTime   = xToTime(firstX);
		}

		var myMinFreq, myMaxFreq;
		if (firstY < secondY) {
			myMinFreq = yToFreq(secondY);
			myMaxFreq =  yToFreq(firstY);
		} else {
			myMinFreq = yToFreq(firstY);
			myMaxFreq =  yToFreq(secondY);
		}

		if (firstY == secondY || firstX == secondX) {
			ARLOLogWarning("This tag has no area. Please try again.");
		} else {

			if (setSpectraCallback) {
				/* override the default behavior */
				setSpectraCallback(myStartTime, myEndTime, myMinFreq, myMaxFreq);
			} else {
				// Save Prior Settings
				savedTagStartTime = $("#id_tagStartTime").val();
				savedTagEndTime = $("#id_tagEndTime").val();
				savedTagMinFreq = $("#id_tagMinFrequency").val();
				savedTagMaxFreq = $("#id_tagMaxFrequency").val();

				$("#id_tagStartTime").val(myStartTime);
				$("#id_tagEndTime").val(myEndTime);
				$("#id_tagMinFrequency").val(myMinFreq);
				$("#id_tagMaxFrequency").val(myMaxFreq);
				if ($("#id_createTagDuration") != null) {
					$("#id_createTagDuration").val(myEndTime - myStartTime);
				} else {
					alert("Doesn't Exist");  // left over debugging code - can be deleted someday
				}
			}
			secondPoint = true;
			fx();
		} // (firstY == secondY || firstX == secondX)

		if (clickMode == "adjustTag") {
			clickMode = "";
			// right-click -> Adjust Tag Handler
			if (confirm("Save Tag?")){
				url = urlPrefix + "/tools/adjustTag/" + adjustTagId;
				dataString = $("#createTagForm").serialize();
				$.ajax({
					url: url,
					type: 'post',
					data: dataString,
					dataType: "json",
					error: function(){ARLOLogError("Error Adjusting Tag");},
					success: function(json){
					ARLOLogNotify(json.saveMessage);
					firstPoint = false;
					fx();
					drawTags();
				}});
			} else {
				// cancel save
				firstPoint = false;
				fx();
				drawTags();
			}
		}

		return;
	}

	// if here, first and second point picked, clear points
	clearTagHighlight();
} // point_it (event)

function clearTagHighlight()
{
	if (savedTagEndTime > 0) {
		$("#id_tagStartTime").val(savedTagStartTime);
		$("#id_tagMaxFrequency").val(savedTagMaxFreq);
		$("#id_tagEndTime").val(savedTagEndTime);
		$("#id_tagMinFrequency").val(savedTagMinFreq);
	}
	if ($("#id_createTagDuration") != null) {
		$("#id_createTagDuration").val('');
	} else {
		alert("Doesn't Exist");  // left over debugging code - can be deleted someday
	}
	firstPoint = false;
	secondPoint = false;
	clickMode = "";
	fx();
}

//transforms the mouse location x to the time location on the image
function xToTime(x)
{
	var x = x - borderWidth;
	var width = imageWidth - (2 * borderWidth);
	var duration = spectraEndTime - spectraStartTime;
	var time = Number(spectraStartTime) + (duration *(x/width));
	return time;
}

function timeToX(time)
{
	var width = imageWidth - (2*borderWidth);
	var duration = spectraEndTime - spectraStartTime;
	var x = (time-spectraStartTime)*(width/duration);
	x = x + borderWidth;
	return x;
}

//transforms the mouse location y into the frequency location on the image
function yToFreq(y)
{
	var y = y - topBorderHeight;
	var spectraHeight = imageHeight - (timeScaleHeight + topBorderHeight);
	//invert y because pixels start from top and spectra starts from bottom
	y = (spectraHeight - y);
	var range = Math.log(maxFrequency) - Math.log(minFrequency);
	freq = minFrequency * Math.exp(range*(y/spectraHeight));
	return freq;
}

function freqToY(frequency)
{
	var range = Math.log(maxFrequency) - Math.log(minFrequency);
	var spectraHeight = imageHeight - (timeScaleHeight + topBorderHeight);
	var y = (spectraHeight/range) * Math.log(frequency/minFrequency);
	y = spectraHeight - y;
	y = y + topBorderHeight;
	return y;
}

//returns true if mouse click is within spectra borders, false otherwise
function isValid(x,y)
{
	var xValid = false;
	var yValid = (y > (timeScaleHeight+10))  && (y < (imageHeight - topBorderHeight+10));

	if (allowContextWindowTagging) {
		xValid = (x > borderWidth) && (x < (imageWidth - borderWidth));
	} else {
		var xStartOfWindow = timeToX(Number(formStartTime));
		var xEndOfWindow = timeToX(Number(formEndTime));
		xValid = (x <= xEndOfWindow) && (x >= xStartOfWindow);
	}

	return xValid && yValid;
}

function fx()
{
	if(firstPoint && !secondPoint) {
		//create start crosshairs
		$("#image").append("<div id=startLineV></div>");
		$("#startLineV").css("border","1px solid #0f0");
		$("#startLineV").css("position","absolute");
		$("#startLineV").css("top",(firstY-4) + "px");
		$("#startLineV").css("left",(firstX-1) + "px");
		$("#startLineV").css("width","0px");
		$("#startLineV").css("height","6px");
		$("#image").append("<div id=startLineH></div>");
		$("#startLineH").css("border","1px solid #0f0");
		$("#startLineH").css("position","absolute");
		$("#startLineH").css("top",(firstY-1) + "px");
		$("#startLineH").css("left",(firstX-4) + "px");
		$("#startLineH").css("width","6px");
		$("#startLineH").css("height","0px");
	} else if(firstPoint && secondPoint) {
		//create end crosshairs
		$("#image").append("<div id=endLineV></div>");
		$("#endLineV").css("border","1px solid #0f0");
		$("#endLineV").css("position","absolute");
		$("#endLineV").css("top",(secondY-4) + "px");
		$("#endLineV").css("left",(secondX-1) + "px");
		$("#endLineV").css("width","0px");
		$("#endLineV").css("height","6px");
		$("#image").append("<div id=endLineH></div>");
		$("#endLineH").css("border","1px solid #0f0");
		$("#endLineH").css("position","absolute");
		$("#endLineH").css("top",(secondY-1) + "px");
		$("#endLineH").css("left",(secondX-4) + "px");
		$("#endLineH").css("width","6px");
		$("#endLineH").css("height","0px");

		//creates box to show tag
		$("#image").append("<div id='pointer' style='background-color:#fff;-moz-opacity=.50'></div>")
		$("#pointer").addClass('opacity50');
		$("#pointer").css("background-color","#fff");
		//$("#pointer").css("opacity","0,50");
		$("#pointer").css("position","absolute");
		//second click in second quadrant relative to first click
		if(firstX < secondX && firstY < secondY) {
			$("#pointer").css("top",(firstY-1) + "px");
			$("#pointer").css("left",(firstX-1) + "px");
			$("#pointer").css("width",(secondX-firstX+2) + "px");
			$("#pointer").css("height",(secondY-firstY+2) + "px");
		}
		//second click in second quadrant relative to first click
		else if(firstX > secondX && firstY < secondY) {
			$("#pointer").css("top",(firstY-1) + "px");
			$("#pointer").css("left",(secondX-1) + "px");
			$("#pointer").css("width",(firstX-secondX+2) + "px");
			$("#pointer").css("height",(secondY-firstY+2) + "px");
		}
		//second click in third quadrant relative to first click
		else if(firstX > secondX && firstY > secondY) {
			$("#pointer").css("top",(secondY-1) + "px");
			$("#pointer").css("left",(secondX-1) + "px");
			$("#pointer").css("width",(firstX-secondX+2) + "px");
			$("#pointer").css("height",(firstY-secondY+2) + "px");
		}
		//second click in fourth quadrant relative to first click
		else if(firstX < secondX && firstY > secondY){
			$("#pointer").css("top",(secondY-1) + "px");
			$("#pointer").css("left",(firstX-1) + "px");
			$("#pointer").css("width",(secondX-firstX+2) + "px");
			$("#pointer").css("height",(firstY-secondY+2) + "px");
		}
	} else {
		//removes tag
		$("#pointer").remove();
		$("#startLineV").remove();
		$("#startLineH").remove();
		$("#endLineV").remove();
		$("#endLineH").remove();
	}
};


//  //  //  //  //  //  //  //  //  //  //  //  //  //  //
  //  //  //  //  //  //  //  //  //  //  //  //  //  //
//  //  //  //  //  //  //  //  //  //  //  //  //  //  //


function drawTags(){
	$(".tags").remove();
	url = urlPrefix + "/tools/getTagsInAudioSegment/";
	url += audioID + "/";
	url += spectraStartTime + "/";
	url += spectraEndTime + "/";
	url += minFrequency + "/";
	url += maxFrequency;
	$.getJSON(url, function(json){
		for(i=0;i < json.length;i++){

			// is this tag selected for display
			var selected = 0;
			$("#id_tagSets option:selected").each(function () {
				if ($(this)[0].value == json[i].tagSetId) {
					selected = 1;
				}
			});

			if (selected) {
				startTimePixels = timeToX(json[i].startTime);
				endTimePixels = timeToX(json[i].endTime);
				width = endTimePixels - startTimePixels;
				minFrequencyPixels = freqToY(json[i].minFrequency);
				maxFrequencyPixels = freqToY(json[i].maxFrequency);
				height = minFrequencyPixels - maxFrequencyPixels;

				t= new Tag(json[i].id, json[i].userTagged, json[i].machineTagged, $("#image"), json[i].displayName);
				if(t.id == currentTagID) {
					border = 3
				} else {
					border = 1
				}
				t.draw(maxFrequencyPixels, startTimePixels, width, height, border);

				// set focus to image so that keyboard shortcuts will work
				// doesn't seem to work on Firefox, but modified keyPress handler to compensate
				$("#image").focus();
			}  // selected
		}

		// Setup Context Menu
		setupTagContextMenus();

		drawContextWindow();

	});
};



///
// If 'contextWindowTime' is defined and > 0, then we draw a window around the
// part of the spectra that we are actually looking at.

function drawContextWindow(){

	$("#ctxWindow").remove();

	if (typeof contextWindowTime !== 'undefined') {
		if (contextWindowTime > 0) {
			var borderWidth = 6;

			var y = freqToY(maxFrequency);
			var height = freqToY(minFrequency) - y - (borderWidth * 2);

			var xStartOfContext = timeToX(Number(spectraStartTime));
			var xStartOfWindow = timeToX(Number(formStartTime));
			var xEndOfWindow = timeToX(Number(formEndTime));
			var xEndOfContext = timeToX(Number(spectraEndTime));

			////
			// Grayed-Out Context Version

			if (false) {
			// Left Side
				var ctxWindow = new ContextWindow('ctxWindowLeft', $("#image"));
				var x = xStartOfContext;
				var width = xStartOfWindow - xStartOfContext;
				ctxWindow.draw(x, y, width, height, 0);

				// Right Side
				var ctxWindow = new ContextWindow('ctxWindowRight', $("#image"));
				var x = xEndOfWindow;
				var width = xEndOfContext - xEndOfWindow;
				ctxWindow.draw(x, y, width, height, 0);
			}

			////
			// Box around the Window version

			if (true) {
				var ctxWindow = new ContextWindow('ctxWindow', $("#image"));
				var x = xStartOfWindow - borderWidth;
				var width = xEndOfWindow - xStartOfWindow;
				ctxWindow.draw(x, y, width, height, borderWidth);
			}
		}
	}


};

function spectraFormCallback(json){

	// Clear Loading Status
	$("#spectraStatusDiv").prop("innerHTML", '');

	// Context Window shows contextWindowTime on each side of the formStart/EndTime
	// spectraStart/EndTime is the time of the entire spectra image
	// formStart/EndTime is the time of the user-selected view.

	spectraStartTime = json.spectraStartTime;
	spectraEndTime = json.spectraEndTime;
	formStartTime = json.formStartTime;
	formEndTime =json.formEndTime;

	contextWindowTime = json.contextWindowTime;

	imageWidth = json.imageWidth;
	imageHeight = json.imageHeight;
	minFrequency = json.minFrequency;
	maxFrequency = json.maxFrequency;
	borderWidth = json.borderWidth;
	topBorderHeight = json.topBorderHeight;
	timeScaleHeight = json.timeScaleHeight;

	// update forms
	$("#id_navStartTime").val(formStartTime);
	$("#id_navEndTime").val(formEndTime);
	$("#id_startTime").val(formStartTime);
	$("#id_endTime").val(formEndTime);
	$("#id_spectraNumFramesPerSecond").val(json.spectraNumFramesPerSecond);
	$("#id_spectraNumFrequencyBands").val(json.spectraNumFrequencyBands);
	$("#id_spectraDampingFactor").val(json.spectraDampingFactor);

	// load spectra image
	$("#image").css('background-image','url(' + mediaUrl + json.imageFilePath + ')');
	$("#image").css('width',imageWidth+'px');
	$("#image").css('height',imageHeight+'px');

	// Setup Audio Player
	// $("#audioSegment").prop('src', mediaUrl + json.audioSegmentFilePath);
	audioPlayer.setFile(mediaUrl + json.audioSegmentFilePath, document.getElementById('id_loadAudio').checked );
	audioPlayer.initializeSpectraProgress(
		document.getElementById('image'), borderWidth, topBorderHeight, imageWidth-(2*borderWidth), imageHeight-(2*topBorderHeight));
	// $("#audioSegment").prop('width', json.playerWidth)

	// Setup Audio Download Link
	if (document.getElementById("audioSegmentDownload")) {
		$("#audioSegmentDownload").prop('href', mediaUrl + json.audioSegmentFilePath);
	}

	clearTagHighlight();
	drawTags();


	if (tagWholeWindowOnLoad) {
		var xStartOfWindow = timeToX(Number(formStartTime)) + 1;
		var xEndOfWindow = timeToX(Number(formEndTime)) - 1;

		var pseudoEvent = new Object();
		pseudoEvent["type"] = "pseudoEvent";
		pseudoEvent["x"] = xStartOfWindow;
		pseudoEvent["y"] = timeScaleHeight + 10 + 1;
		point_it(pseudoEvent);

		pseudoEvent["x"] = xEndOfWindow;
		pseudoEvent["y"] = imageHeight - topBorderHeight + 10 - 1;
		point_it(pseudoEvent);
	}
};

function createTagFormCallback(json){
	if (json.Success) {
		ARLOLogNotify(json.saveMessage);

		$("#pointer").remove();
		$("#startLineV").remove();
		$("#startLineH").remove();
		$("#endLineV").remove();
		$("#endLineH").remove();
		firstPoint = false;
		secondPoint = false;
		drawTags();
	} else {
		ARLOLogWarning(json.saveMessage);
	}

	if (typeof createTagFormCallback_Success == 'function') {
		createTagFormCallback_Success(json.Success, json.saveMessage, json);
	}
};

function getAutoComplete(){
	$.getJSON(urlPrefix + "/tools/getTagClassAutoArray/" + projectId, function(json){
		tagClasses = json;
		$("#id_className").autocomplete({source: tagClasses});
	});
};

function getSpectra(){
	$("#spectraStatusDiv").prop("innerHTML", 'Loading Spectra... <img src="' + LOADING_GIF_URL + '" width="20px" height="20px"/>');
	$("#spectraForm").ajaxSubmit({
		url:urlPrefix + '/tools/createAudioSpectra',
		dataType:'json',
		success:spectraFormCallback
	});
};

function navigateFormCallback(json){
	currentTagID = json.tagID;
	$("#id_currentTagID").val(json.tagID);
	$("#id_startTime").val(json.startTime);
	$("#id_endTime").val(json.endTime);
	$("#id_navStartTime").val(json.startTime);
	$("#id_navEndTime").val(json.endTime);
	getSpectra();
};


/*                     */
/*    Context Menus    */
/*                     */

function setupTagContextMenus() {
	$.contextMenu({
		selector: '.tagDivContextMenu',
		callback: contextMenuCallback,
		items: {
			"viewTag": {name: "View Tag"},
			"viewMultiAudioFile": {name: "View in MultiView"},
			"adjustTag": {name: "Adjust Tag"},
			"cancelAdjust": {name: "Cancel Adjust"},
			"deleteTag": {name: "Delete Tag"}
		}
	});
};


function contextMenuCallback(action, options){
	// highlight selected Tag
	var el = options.$trigger;
	var myId = el[0].id;
	var origBorder=$('#'+myId).css('border');
	var keepHighlighted = false;

	// Firefox doesn't seem to support the border-color and -width properties
	//  var origColor=$('#'+myId).css('border-color');
	//  var origWidth=$('#'+myId).css('border-width');
	$('#'+myId).css('border-color', '#FFFF00');
	$('#'+myId).css('border-width', '3px');

	//  alert("origColor: " + origColor + "  origWidth: " + origWidth + "  origBorder: " + origBorder);

	if (action == 'viewTag'){
		el.after("<a id='pop'></a>");
		$('#pop').popupWindow({
			windowURL:urlPrefix + "/tools/viewTag/" + myId,
			height:500,
			width:600,
			top:50,
			left:50
		});
		$('#pop').click();
		$('#pop').remove();
	}

	if (action == 'viewMultiAudioFile'){
		audiourl = urlPrefix + "/tools/visualizeMultiProjectFile/" + projectId + "?tagId=" + myId;
		window.open(audiourl);
	}

	if(action=='deleteTag'){
		if (confirm("Delete Tag?")){
			url = urlPrefix + "/tools/deleteTag/" + myId;
			$.ajax({
				url: url,
				type: 'get',
				error: function(){ARLOLogError("Error Deleting Tag");},
				success: function(){
					el.remove();
					drawTags();
				}
			});
		}
	}

	if (action=='adjustTag'){
		clickMode = "adjustTag";
		firstPoint = false;
		fx();
		adjustTagId = myId;
		keepHighlighted = true;
	}

	if (action=='cancelAdjust'){
		clickMode = "";
		firstPoint = false;
		fx();
		drawTags();
	}

	// unhighlight tag
	//  $('#'+myId).css('border-color', origColor);
	//  $('#'+myId).css('border-width', origWidth);
	if (! keepHighlighted) {
		$('#'+myId).css('border', origBorder);
	}
};

function keyPressHandler(evt){
	// handle a keyPress from the page
	switch (evt.which) {
		case 97: // 'a'
			// jump audio to beginning
			audioPlayer.jumpToBeginning();
			break;
		case 115: // 's'
			// jump audio 1 second back
			audioPlayer.jumpTimeRelative(-1);
			break;
		case 100: // 'd'
			// jump audio 1 second forward
			audioPlayer.jumpTimeRelative(1);
			break;
		case 102: // 'f'
			// pause / play
			audioPlayer.pausePlay();
			break;
	}  // switch
};
