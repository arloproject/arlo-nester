
////////////////////////////////////////////////////////////////
// A collection of utilities used throughout the ARLO project.

// Also expects the document to have imported arlo.css for some of these functions


// init these functions - expected to be called on document load
// return true on success, false if any errors occur.
function ARLOInit() {
	var success = true;
	success = ARLOLogSetup() && success;

	if (! success) {
		console.log("Failed ARLOInit()");
	}

	return success;
};


// SubmitFormOnEnter(mEvt)
//   Captures <Enter> on form text elements and force it to submit
//   using the element with id = "submit"
//   This bypasses the non-specified browser determined behavior of form
//   submission with the Enter key. Most browsers seem to use the first
//   button on the form. Not necessarily what we want.

// NOTE: This assumes there will be a button with id "submit" in the form,
// otherwise the form's submit function will be used.

// implement by adding to form onkeypress, e.g.,
//   <form action="" onkeypress="return SubmitFormOnEnter(event)">

function SubmitFormOnEnter(mEvt) {
	mEvt = (mEvt) ? mEvt : event;
	var mTarget = (mEvt.target) ? mEvt.target : mEvt.srcElement;  /* srcElement deprecated in jQuery 1.7 */
	var mForm = mTarget.form;
	var charCode = (mEvt.charCode) ? mEvt.charCode : ((mEvt.which) ? mEvt.which : mEvt.keyCode);
	if (charCode == 13) {
		if (typeof mForm.submit == "object") {
			// submit is likely a button on the form
			mForm.submit.click();
		} else { // submit probably is function
			mForm.submit();
		}
		return false;
	}  // if (charCode == 13)
	return true;
};



// each() is a JQuery function... need to make sure that's included

// example setup...
//       SetupHelpButtons('{{url_prefix}}');
// in $(document).ready(function(){
//
// button
//   <img src="{{url_prefix}}/site_media/images/info.gif" class="help" height="12px" width="12px" id="numRandomProbes"/>

function SetupHelpButtons(urlPrefix) {
	$(".help").each(function(){
		$(this).popupWindow({
			windowURL:String(urlPrefix) + "/tools/getInfo/" + $(this).attr('id'),
			height:250,
			width:300,
			top:50,
			left:50
		});
	});
};


//////////////////////////////////////////
// ARLO Global Message Notifications

// These functions build a framework for logging messages on the client side of
// the application. Actions are defined based upon the Logging Level, for which
// there are a number of wrapper functions pre-defined. Based on the level, the
// message may be displayed in multiple places:
//   - Browser JavaScript Console
//   - A static list of log messages in the UI
//   - A popup message, which may either timeout and disappear on its own, or be
//     closed by the user.
//
// In the UI, all of the logging is contained within one <div>, #ARLOLogMessageDiv
// Popup messages are placed at the top of this <div>, a title header (<h3>) in the
// middle, and a separate <div>, #ARLOLogMessageCollapseDiv, for the the static list
// at the bottom. Clicking on the <h3> header displays or collapses the static list,
// the default state.
//
// Logging is accomplished with a variety of functions -
//
// Main Function
//   - ARLOLog(sMesg, logLevel)
// Wrappers
//   - ARLOLogDebug(sMesg)
//   - ARLOLogInfo(sMesg)
//   - ARLOLogNotify(sMesg)
//   - ARLOLogWarning(sMesg)
//   - ARLOLogError(sMesg)
//
// The logLevel determines whether the alert is popped up, whether it automatically
// fades out, and the background color, based on the defined settings below.
//
//  ---------------------------------------
// | <div> ARLOLogMessageDiv               |
// |                                       |
// | popup messages go here                |
// |                                       |
// |  ---------------------------------    |
// | | <div> ARLOLogMessageCollapseDiv |   |
// | |  --------------------------     |   |
// | | | <ul> ARLOLogMessageList  |    |   |
// | | |                          |    |   |
// | | | Static messages go here  |    |   |
// | | |                          |    |   |
// | |  --------------------------     |   |
// |  ---------------------------------    |
//  ---------------------------------------
//
// Log Levels
// 0 - Debug
// 1 - Info
// 2 - Notify
// 3 - Warning
// 4 - Error

var ARLOLogInitialized = null;

var ARLOLogLevelText =           ["DEBUG"  , "INFO"   , "NOTIFY" , "WARNING", "ERROR"  , "UNKNOWN LEVEL"];
var ARLOLogLevelPopupColor =     ["#fffafa", "#c0c0c0", "#32cd32", "#ffd700", "#c00000", "#ff0000"      ];
var ARLOLogLevelPopupTextColor = ["#000000", "#000000", "#000000", "#000000", "#ffffff", "#ffffff"      ];
var ARLOLogLevelPopupTimeouts  = [5        , 5        , 5        , 30       , 0        , 0              ];
var ARLOLogRemoveStatusMessage = [false    , false    , false    , true     , true     , true           ];
var ARLOLogMinLevelConsole = 0;  // log all to console
var ARLOLogMinLevelUI      = 0;
var ARLOLogMinLevelPopup   = 1;

function ARLOLogPopup (popupDiv, timeoutSeconds) {
	this.popupDiv = popupDiv;
	this.timeoutSeconds = timeoutSeconds;
}

var ARLOLogPopups = [];
var ARLOLogTimerId = null;

function ARLOLogSetup() {
	// add a container to the bottom-right of the screen to hold the log messages

	var container = document.createElement('div');
	container.setAttribute('id','ARLOLogMessageDiv');
	container.setAttribute('class','ARLOLogMessageDiv');
	document.body.appendChild(container);

	var header = document.createElement('h3');
	header.setAttribute('id', 'ARLOLogMessageHeader');
	header.innerHTML = "ARLO Log";
	container.appendChild(header);

	var colContainer = document.createElement('div');
	colContainer.setAttribute('id','ARLOLogMessageCollapseDiv');
	colContainer.setAttribute('class','ARLOLogMessageCollapseDiv');
	container.appendChild(colContainer);
	colContainer.style.visibility = 'hidden';
	colContainer.style.display = 'none';

	var mesgList = document.createElement('ul');
	mesgList.setAttribute('id','ARLOLogMessageList');
	mesgList.setAttribute('class','ARLOLogMessageList');
	colContainer.appendChild(mesgList);

	// setup collapsible header
	$("#ARLOLogMessageHeader").click(function()
	{
		//$(".collapsible_body", $(this)).slideToggle(100); //find the collapsible_body class div in this parent div
		var colContainer = document.getElementById('ARLOLogMessageCollapseDiv');
		if (! colContainer) {
			alert("Failure in ARLO Log");
			return false;
		}

		if (colContainer.style.visibility == 'hidden') {
			colContainer.style.visibility = 'visible';
			colContainer.style.display = 'block';
		} else {
			colContainer.style.visibility = 'hidden';
			colContainer.style.display = 'none';
		}

	});

	ARLOLogInitialized = 1;
	return true;
};

// Wrapper functions
function ARLOLogDebug(sMesg) {ARLOLog(sMesg, 0);};
function ARLOLogInfo(sMesg) {ARLOLog(sMesg, 1);};
function ARLOLogNotify(sMesg) {ARLOLog(sMesg, 2);};
function ARLOLogWarning(sMesg) {ARLOLog(sMesg, 3);};
function ARLOLogError(sMesg) {ARLOLog(sMesg, 4);};

function ARLOLog(sMesg, logLevel) {
	logLevel = typeof logLevel !== 'undefined' ? logLevel : 4; // default value
	if (logLevel >= ARLOLogLevelText.length) {
		logLevel = ARLOLogLevelText.length - 1;
	}

	if (logLevel >= ARLOLogMinLevelConsole) {
		console.log("ARLOLog " + ARLOLogLevelText[logLevel] + ": " + sMesg);
	}

	if (logLevel >= ARLOLogMinLevelUI) {
		// add to list
		var newLogItem = document.createElement('li');
		newLogItem.innerHTML = ARLOLogLevelText[logLevel] + ": " + sMesg;
		var logList = document.getElementById('ARLOLogMessageList');
		if (! logList) {
			alert("Error In ARLOLog");
			return;
		}
		logList.appendChild(newLogItem);
	}

	if (logLevel >= ARLOLogMinLevelPopup) {
		// add popup
		var popup = document.createElement('div');
		popup.setAttribute('class','ARLOLogMessagePopup');
		popup.style.background = ARLOLogLevelPopupColor[logLevel];
		popup.style.color = ARLOLogLevelPopupTextColor[logLevel];

		popup.innerHTML = "<p><b><span onclick='ARLOLogClickHandler(this);'>x</span></b>  " + sMesg + "</p>";

		var logDiv = document.getElementById('ARLOLogMessageDiv');
		if (! logDiv) {
			alert("Error In ARLOLog");
			return;
		}
		logDiv.insertBefore(popup, logDiv.firstChild);  // essentially a 'prepend'

		var myPop = new ARLOLogPopup(popup, ARLOLogLevelPopupTimeouts[logLevel]);
		ARLOLogPopups.push( myPop );

		if (! ARLOLogTimerId) {
			// if timer isn't running, then start it
			ARLOLogTimerId = window.setInterval(ARLOLogTimerHandler, 1000);
		} else {

		}
	}

	if (ARLOLogRemoveStatusMessage[logLevel]) {
		// remove any ARLOModalStatus display
		ARLOModalStatusCloseAll();
	}

	return;
};

function ARLOLogTimerHandler() {
	// Event Handler to cleanup popups
	var anyLeft = false;

	for (var i=0; i<ARLOLogPopups.length; i++) {
		if (ARLOLogPopups[i].timeoutSeconds) {  // only mess with popups with a timeout set
			ARLOLogPopups[i].timeoutSeconds--;
			if (ARLOLogPopups[i].timeoutSeconds) {
				// still time left
				anyLeft = true;
			} else {
				// time to remove this guy
				ARLOLogPopups[i].popupDiv.parentNode.removeChild(ARLOLogPopups[i].popupDiv);
				ARLOLogPopups.splice(i,1);
				i--;  // rerun the index since this one is gone
			}
		}
	}
	if (! anyLeft) {
		window.clearInterval(ARLOLogTimerId);
		ARLOLogTimerId = null;
	}
};

function ARLOLogClickHandler(clickedSpan) {
	// close out the message that was clicked on

	//                         <b>        <p>        <div>
	var popupDiv = clickedSpan.parentNode.parentNode.parentNode;

	// find the message in the array
	for (var i=0; i<ARLOLogPopups.length; i++) {
		if (ARLOLogPopups[i].popupDiv == popupDiv) {
			// kill the message
			ARLOLogPopups[i].popupDiv.parentNode.removeChild(ARLOLogPopups[i].popupDiv);
			ARLOLogPopups.splice(i,1);
			return;  // found and removed, so we can go home
		}
	}
}


/////////////////////////
// ARLO Browser Settings
//
// This is a simple API to get/set browser settings for controlling the web
// interface. Currently, and possibly forever, this will be a simple wrapper
// around the HTML 5 Local Storage API. These are settings that are
// persistently stored in the local browser, and may be reset at any time.
//
// NOTE: There could be an issue of cross-domain restrictions, since the JS
// files may be served from a subdomain / CDN. Hopefully since this script
// should always be served from one domain, this will store data in it's own
// domain.


function ARLOSettingGet(name) {
	if (typeof(localStorage) !== "undefined") {
		return localStorage.getItem(name);
	} else {
		return null;
	}
}

function ARLOSettingSet(name, value) {
	if (typeof(localStorage) !== "undefined") {
		return localStorage.setItem(name, value);
	} else {
		return null;
	}
}

function ARLOSettingDelete(name) {
	if (typeof(localStorage) !== "undefined") {
		return localStorage.removeItem(name);
	} else {
		return null;
	}
}

// returns a list of key names
function ARLOSettingAllKeys() {
	var keys = [];
	if (typeof(localStorage) !== "undefined") {
		for (key in localStorage) {
			keys.push(key);
		}
	}
	return keys;
}

function ARLOSettingClearAll() {
	localStorage.clear();
}

var KNOWN_ARLO_SETTINGS = [
	"EnableQuickTag",
	"DisableCatalogAudioPreview"
];




///////////////////
// ARLOModalStatus
//
// Provides an object to place a modal popup with Status Messages over the
// screen while background operations are happening.
//
// Requires arlo.css to be loaded.
//
// this.setStatus(statusHtml, showClose)
// this.close()

function ARLOModalStatus() {
	this.statusMessageDiv = null;
	this.overDiv = null;

	// @param statusHtml  The Message to display on the Status Popup, inserted
	//                    into a div as HTML.
	// @param showClose   (Boolean) Whether to display a 'close' button.
	this.setStatus = function(statusHtml, showClose) {

		// first remove any prior status
		this.close();

		// create the fade overlay
		this.overDiv = document.createElement("div");
		this.overDiv.className = "ARLOModalStatusOverDiv";

		// Create the status display box
		this.statusMessageDiv = document.createElement("div");
		this.statusMessageDiv.className = "ARLOModalStatusMessage";
		this.statusMessageDiv.align = "center";

		// store a copy of this instance in the Div
		this.statusMessageDiv.Code = this;

		// create the status message
		var msg = document.createElement("div");
		//msg.style = "margin-left:auto; margin-right:auto;position:fixed";
		msg.className = "ARLOModalStatus";
		msg.style.cssText = "margin:20px 20px 20px 20px;";

		msg.innerHTML = statusHtml;

		// place the status message in the display box
		this.statusMessageDiv.appendChild(msg);

		if (showClose) {
			// add a close button to the status screen
			var closebtn = document.createElement("button");
			closebtn.onclick = function() {
					this.parentNode.Code.close();
				};
			closebtn.innerHTML = "Close";
			this.statusMessageDiv.appendChild(closebtn);

		}

		document.body.appendChild(this.overDiv);
		document.body.appendChild(this.statusMessageDiv);
	}

	// close and remove this Status Message
	this.close = function() {
		if (this.statusMessageDiv != null) {
			var parentNode = this.statusMessageDiv.parentNode;
			if (parentNode != null) {
				if (parentNode == document.body) {
					document.body.removeChild(this.statusMessageDiv);
				} else {
					console.log("ARLOModalStatus close() statusMessageDiv - parentNode != document.body");
				}
			} else {
				console.log("ARLOModalStatus close() statusMessageDiv - parentNode == null");
			}
			this.statusMessageDiv = null;
		}

		if (this.overDiv != null) {
			var parentNode = this.overDiv.parentNode;
			if (parentNode != null) {
				if (parentNode == document.body) {
					document.body.removeChild(this.overDiv);
				} else {
					console.log("ARLOModalStatus close() overDiv - parentNode != document.body");
				}
			} else {
				console.log("ARLOModalStatus close() overDiv - parentNode == null");
			}
			this.overDiv = null;
		}

	}
}

// remove all ARLOModalStatus instances from document
// namely called on ARLOLog functions to clear status on error

function ARLOModalStatusCloseAll() {
	var nodeList = document.querySelectorAll(".ARLOModalStatusMessage");
	for (var i = 0, length = nodeList.length; i < length; i++) {
		document.body.removeChild(nodeList[i]);
	}

	var nodeList = document.querySelectorAll(".ARLOModalStatusOverDiv");
	for (var i = 0, length = nodeList.length; i < length; i++) {
		document.body.removeChild(nodeList[i]);
	}
}


///////////////////
// ARLOModalForm
//
// Provides an object to place a modal popup with a custom form over the
// screen while background operations are happening.
//
// Pass in a Form object to be rendered in the popup.
// - A submit with "name=submit" calls the submitCallback. If submitCallback
//   returns True, close the Form. The default event for the submit will not fire.
// - A submit with "name=cancel" calls the cancelCallback. If cancelCallback
//   returns True, close the Form. The default event for the submit will not fire.
//
// Requires arlo.css to be loaded.
//
// MODAL FORM TEST EXAMPLE
//  var f = document.createElement("form");
//  f.setAttribute('action', "#");
//
//  f.innerHTML = "<label>TestLabel</label>";
//  f.innerHTML += "<input type='text' />";
//  f.innerHTML += "<input type='submit' name='submit' value='TestSubmit' />";
//  f.innerHTML += "<input type='submit' name='cancel' value='TestCancel' />";

//  var popupForm = new ARLOModalForm();
//  popupForm.popupForm(
//    f,
//    function(form) {
//      ARLOLogInfo("Submit Callback");
//      return false;  // form will stay open
//    },
//    function(form) {
//      ARLOLogInfo("Cancel Callback");
//      return true;
//    }
//  );

// WARNING
//
// This may be re-entrant as the prior forms are Destroyed when a
// new is created. UNTESTED - proceed with caution. If it is, it is
// barely so...

// There should just be one Global ARLOModalForm Active
// We should probably re-factor this once we upgrade JQuery > 1.4.3 where
// we can pass custom data in through the events. Then we can allow multiple
// instances of this.
var GLOBAL_ACTIVE_ARLO_MODAL_FORM = null;

function ARLOModalForm() {
	this.statusMessageDiv = null;
	this.overDiv = null;

	this.myForm = null;
	this.myOnSubmitCallback = null;
	this.myOnCancelCallback = null;
	this.cb_context = null;

	// @param form The Form to render on screen.
	// @param onSubmitCallback A function(form) called when a Submit button
	//                         named "submit" is clicked.
	// @param onCancelCallback A function(form) called when a Submit button
	//                         named "cancel" is clicked.
	// @param cb_context Context to pass to the callbacks.

	this.popupForm = function(form, onSubmitCallback, onCancelCallback, cb_context = null) {

		// remove any prior form if we are resetting the same one
		this.close();

		// now remove ANY prior forms
		ARLOModalFormCloseAll();

		// set the variables
		GLOBAL_ACTIVE_ARLO_MODAL_FORM = this;
		this.myForm = form;
		this.myOnSubmitCallback = onSubmitCallback;
		this.myOnCancelCallback = onCancelCallback;
		this.cb_context = cb_context;

		if (this.myForm == null) {
			ARLOLogError("ARLOModalForm setup will Null Form");
			return false;
		}

		// //
		// Event Handlers for the Submits

		// can't pass custom data til JQuery > 1.4.3
		// $(form)
		//	.find('input[name=submit]')
		//	.click({'myARLOModalForm': this}, this.onSubmitButton);  // pass 'this' to the event

		$(form)
			.find('input[name=submit]')
			.click(this.onSubmitButton);  // pass 'this' to the event

		$(form)
			.find('input[name=cancel]')
			.click(this.onCancelButton);  // pass 'this' to the event


		// //
		// Display

		// create the fade overlay
		this.overDiv = document.createElement("div");
		this.overDiv.className = "ARLOModalFormOverDiv";

		// Create the form display box
		this.statusMessageDiv = document.createElement("div");
		this.statusMessageDiv.className = "ARLOModalFormMessage";
		this.statusMessageDiv.align = "center";

		// store a copy of this instance in the Div
		this.statusMessageDiv.Code = this;

		// create the form div
		var msg = document.createElement("div");
		//msg.style = "margin-left:auto; margin-right:auto;position:fixed";
		msg.className = "ARLOModalForm";
		msg.style.cssText = "margin:20px 20px 20px 20px;";

		msg.appendChild(this.myForm);

		// place the form div in the display box
		this.statusMessageDiv.appendChild(msg);

		document.body.appendChild(this.overDiv);
		document.body.appendChild(this.statusMessageDiv);
	}


	this.onSubmitButton = function(event) {
		// get the global active form
		var myARLOModalForm = GLOBAL_ACTIVE_ARLO_MODAL_FORM;
		if (myARLOModalForm == null) {
			ARLOLogError("myARLOModalForm == null in onSubmitButton");
			return false;
		}

		if (myARLOModalForm.myOnSubmitCallback == null) {
			myARLOModalForm.close();
		} else {
			var ret = (myARLOModalForm.cb_context == null ?
						myARLOModalForm.myOnSubmitCallback(myARLOModalForm.myForm) :
						myARLOModalForm.myOnSubmitCallback(myARLOModalForm.myForm, myARLOModalForm.cb_context));

			if (ret) {
				// close out form
				myARLOModalForm.close();
			} // else do nothing
		}

		return false; // do not let the default event for the submit fire.
	};

	this.onCancelButton = function(event) {
		// get the global active form
		var myARLOModalForm = GLOBAL_ACTIVE_ARLO_MODAL_FORM;
		if (myARLOModalForm == null) {
			ARLOLogError("myARLOModalForm == null in onCancelButton");
			return false;
		}

		if (myARLOModalForm.myOnCancelCallback == null) {
			myARLOModalForm.close();
		} else {
			var ret = (myARLOModalForm.cb_context == null ?
						myARLOModalForm.myOnCancelCallback(myARLOModalForm.myForm) :
						myARLOModalForm.myOnCancelCallback(myARLOModalForm.myForm, myARLOModalForm.cb_context));

			if (ret) {
				// close out form
				myARLOModalForm.close();
			} // else do nothing
		}

		return false; // do not let the default event for the submit fire.
	};



	// close and remove this Popup Form
	this.close = function() {
		if (this.statusMessageDiv != null) {
			var parentNode = this.statusMessageDiv.parentNode;
			if (parentNode != null) {
				if (parentNode == document.body) {
					document.body.removeChild(this.statusMessageDiv);
				} else {
					console.log("ARLOModalForm close() statusMessageDiv - parentNode != document.body");
				}
			} else {
				console.log("ARLOModalForm close() statusMessageDiv - parentNode == null");
			}
			this.statusMessageDiv = null;
		}

		if (this.overDiv != null) {
			var parentNode = this.overDiv.parentNode;
			if (parentNode != null) {
				if (parentNode == document.body) {
					document.body.removeChild(this.overDiv);
				} else {
					console.log("ARLOModalForm close() overDiv - parentNode != document.body");
				}
			} else {
				console.log("ARLOModalFormus close() overDiv - parentNode == null");
			}
			this.overDiv = null;
		}

		// don't force a reset of this here, as it seems to cause re-entry
		// problems since it destroys the new object.
		// GLOBAL_ACTIVE_ARLO_MODAL_FORM = null;
	}
}

// remove all ARLOModalStatus instances from document
// namely called on ARLOLog functions to clear status on error

function ARLOModalFormCloseAll() {
	var nodeList = document.querySelectorAll(".ARLOModalFormMessage");
	for (var i = 0, length = nodeList.length; i < length; i++) {
		document.body.removeChild(nodeList[i]);
	}

	var nodeList = document.querySelectorAll(".ARLOModalFormOverDiv");
	for (var i = 0, length = nodeList.length; i < length; i++) {
		document.body.removeChild(nodeList[i]);
	}
	GLOBAL_ACTIVE_ARLO_MODAL_FORM = null;
}
