// audioPlayer.js
// TonyB Jun 2012
//
// Implements an HTML5 Audio object to build up a custom audio player
// with features and extensions specific to the ARLO Project. This is
// only for WAV files at this time.
//
// One extension is the spectraProgress feature, which displays a vertical line
// over spectra images to indicate player progress.
//
// This player implements its own controls.
// [Play] [--Progres/Seek --] [x]Half-Speed
//
// Implementation Notes:
// When using the spectraProgress, the parent div (this.spectraImageDiv) should
// probably have style position:relative


// DOM ids
// id = passed id string
// AudioPlayer_id - Audio element
// AudioPlayerontrolsDiv_id -
// AudioPlayerControl_Play_id -
// AudioPlayerControl_Progress_id -
// AudioPlayerControl_HalfSpeed_id -


// handy event handler discussion
// http://www.digital-web.com/articles/scope_in_javascript/

// External Resources - referenced to ArloStaticUrl
const PlayButtonImgURL = '/third-party/ts_picker/next.gif';


/**
* Initialize a new AudioPlayer object.
*
* This must be initialized with a file. If the specific file is
* not known at the time of initialization, you can use an empty
* WAV file (see Bugzilla Bug 53 for more info)
* @param id - the DOM id for the new object
* @param fileUrl - URL of the file to load
* @param parentDiv - the DIV that will contain the controls
* (DOM object, not jQuery selector or id)
* @return Boolean - true if success, false on error
*/


function AudioPlayer(id, fileUrl, parentDiv)
{
	this.id = id;
	this.fileUrl = fileUrl;
	this.parentDiv = parentDiv;

	// initialize the Audio object
	this.audio = new Audio(fileUrl);
	this.audio.setAttribute('id', "AudioPlayer_" + this.id);
	this.parentDiv.appendChild(this.audio);
	this.audio.controls = false;
	//this.audio.play();

	// initialize the controls
	this.controlsDiv = document.createElement('div');
	this.parentDiv.appendChild(this.controlsDiv);
	this.controlsDiv.setAttribute('id', "AudioPlayerontrolsDiv_" + this.id);
	// play button
	this.controlPlay = document.createElement('img');
	// this.controlPlay = document.createElement('button');
	this.controlsDiv.appendChild(this.controlPlay);
	this.controlPlay.setAttribute('id', "AudioPlayerControl_Play_" + this.id);
	//this.controlPlay.setAttribute('value', "Play");
	//this.controlPlay.style.backgroundImage = 'url(' + mediaUrl + PlayButtonImgURL + ")";
	this.controlPlay.src = ArloStaticUrl + PlayButtonImgURL;
	this.controlPlay.style.verticalAlign = 'middle';
	// progress bar
	this.controlProgress = document.createElement('progress');
	this.controlsDiv.appendChild(this.controlProgress);
	this.controlProgress.setAttribute('id', "AudioPlayerControl_Progress_" + this.id);
	this.controlProgress.setAttribute('max', '100');
	this.controlProgress.setAttribute('value', '0');
	// half-speed div
//	this.controlHalfSpeedDiv = document.createElement('div');
//	this.controlsDiv.appendChild(this.controlHalfSpeedDiv);
	// half-speed checkbox
	this.controlHalfSpeed = document.createElement('input');
	this.controlsDiv.appendChild(this.controlHalfSpeed);
//	this.controlHalfSpeedDiv.appendChild(this.controlHalfSpeed);
	this.controlHalfSpeed.setAttribute('id', "AudioPlayerControl_HalfSpeed_" + this.id);
	this.controlHalfSpeed.setAttribute('type', 'checkbox');
	this.controlHalfSpeed.setAttribute('value', 'Half-Speed');
	this.controlsDiv.appendChild(document.createTextNode("Half-Speed (Chrome Only)"));
//	this.controlHalfSpeedDiv.appendChild(document.createTextNode("Half-Speed"));



	// initialize spectraProgress variables
	this.spectraImageDiv = null;
	this.spectraMarkerDiv = null;
	this.spectraLeftOffset = 0;
	this.spectraTopOffset = 0;
	this.spectraWidth = 0;
	this.spectraHeight = 0;

	/**
	* Pass through to the Audio Element play()
	*/

	this.play = function() {
		this.audio.play();
	}


	/**
	 * Initialize the player with a new Audio file. Basically reset
	 * all parameters.
	 * @param fileUrl The URL for the new audio file
	 * @param autoPlay Whether or not to start playing the file
	 */


	this.setFile = function(fileUrl, autoPlay) {
		this.fileUrl = fileUrl;
		this.audio.pause();
		this.audio.src = fileUrl;
		//this.audio.currentTime = 0;
		if (autoPlay)
			this.audio.play();
		this.controlProgress.setAttribute('value', '0');

		// reset spectra
		if (this.spectraMarkerDiv != null)
			this.spectraMarkerDiv.parentNode.removeChild(this.spectraMarkerDiv);
		this.spectraImageDiv = null;
		this.spectraMarkerDiv = null;
		this.spectraLeftOffset = 0;
		this.spectraTopOffset = 0;
		this.spectraWidth = 0;
		this.spectraHeight = 0;
		this.updateSpectraProgress();

		if (this.controlHalfSpeed.checked) {
			this.audio.playbackRate = "0.5";
		} else {
			this.audio.playbackRate = "1";
		}
	}

	/**
	 * Initialize a progress display overlaid on a spectra
	 * image. This will show a moving vertical line across the
	 * spectra similar to common desktop audio players.
	 * @param spectraImageDiv
	 * @param leftOffset
	 * @param topOffset
	 * @param width
	 * @param height
	 */

	this.initializeSpectraProgress = function(
		spectraImageDiv, leftOffset, topOffset, width, height ) {

		this.spectraImageDiv = spectraImageDiv;
		this.spectraLeftOffset = leftOffset;
		this.spectraTopOffset = topOffset;
		this.spectraWidth = width;
		this.spectraHeight = height;

		this.spectraMarkerDiv = document.createElement('div');
		this.spectraMarkerDiv.setAttribute('id', "AudioPlayer_SpectraMarkerDiv_" + this.id);
		this.spectraImageDiv.appendChild(this.spectraMarkerDiv);
		this.spectraMarkerDiv.style.border = "1px solid #fc0";
		this.spectraMarkerDiv.style.position = "absolute";
		this.spectraMarkerDiv.style.left = leftOffset + "px";
		this.spectraMarkerDiv.style.top = topOffset + "px";
		this.spectraMarkerDiv.style.width = "0px";
		this.spectraMarkerDiv.style.height = height + "px";

		this.updateSpectraProgress();

		return;
	};

	this.updateSpectraProgress = function(progressFraction) {
		if (this.spectraMarkerDiv == null)
			return;

		this.spectraMarkerDiv.style.left =
			(this.spectraLeftOffset + (progressFraction * this.spectraWidth)) + "px";
	};

	this.pausePlay = function() {
		this.OnPlay();
	};

	this.jumpToBeginning = function() {
		this.audio.currentTime = 0;
	};

	this.jumpTimeRelative = function(seconds) {
		var newTime = this.audio.currentTime + seconds;
		if (newTime > this.audio.duration) {
			newTime = this.audio.duration;
		}
		if (newTime < 0) {
			newTime = 0;
		}
		this.audio.currentTime = newTime;
	};

	// /////////////////////////////////// //
	//                                     //
	//           Event Handlers            //
	//                                     //
	// /////////////////////////////////// //

	this.OnTimeupdate = function() {
		var progress = this.audio.currentTime / this.audio.duration;
		if (isNaN(progress))
			progress = 0;
		this.controlProgress.setAttribute('value', (100 * progress));

		this.updateSpectraProgress(progress);
	};

	this.OnPlay = function() {
		// toggle play/pause
		if (this.audio.paused == false) {
			this.audio.pause();
		} else {
			if (this.audio.currentTime >= this.audio.duration)
        //this.audio.currentTime = this.audio.initialTime;
        //initialTime isn't standard and isn't working for Daniel. Replacing with 0 for now.
				this.audio.currentTime = 0;
			this.audio.play();
		}
	};

	this.OnClickProgress = function(event) {
		var myLeft = 0;
		//var myTop = 0;
		var curObj = this.controlProgress;
		do {
			myLeft += curObj.offsetLeft;
			//myTop += curObj.offsetTop;
		} while (curObj = curObj.offsetParent);

		var pos_x = event.pageX - myLeft;
		// var pos_y = event.pageY - myTop;

		var newTime = (pos_x / this.controlProgress.offsetWidth) * this.audio.duration;
		this.audio.currentTime = newTime;
		this.audio.play();

		// debug logging
		if (typeof ARLOLogInitialized != 'undefined' && ARLOLogInitialized) {
			ARLOLogDebug("OnClickProgress: " +
				pos_x + " " +
				this.controlProgress.offsetWidth + " " +
				this.audio.duration + " " + newTime);
		}
	};

	this.OnChangeSpeed = function() {
		if (this.controlHalfSpeed.checked) {
			this.audio.playbackRate = "0.5";
		} else {
			this.audio.playbackRate = "1";
		}
	};

	// /////////////////////////////////// //
	//                                     //
	//        Setup Event Handlers         //
	//                                     //
	// /////////////////////////////////// //

	this.audio.addEventListener("timeupdate", this.OnTimeupdate.bind(this));
	this.controlPlay.onclick = this.OnPlay.bind(this);
	this.controlProgress.addEventListener("click", this.OnClickProgress.bind(this));
	this.controlHalfSpeed.onchange = this.OnChangeSpeed.bind(this);

	return true;
};
