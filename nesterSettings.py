"""
Nester Settings for Managing properties of the installation.

This is in parallel to the standard Django settings.py. This class is
intended to keep Nester (ARLO) settings, keeping settings.py for Django
specific settings.

Notably this will be defining file paths. Use these functions to dynamically
generate file paths. This abstracts the file storage structure.

These paths are relative to MEDIA_ROOT (for the file system) and
MEDIA_URL
"""

import os

from django.conf import settings


#################################
#                               #
#        MediaFile Paths        #
#                               #
#################################


## Given a username and mediafile name, generate the file path.
# This abstracts the file storage structure.
# @note Relative to MEDIA_ROOT and MEDIA_URL
# @param library Library object that owns the file
# @param relativeFilePath Relative path of the file (usually the filename)
# @return The file's relative path location, as a string

def getMediaFilePath(library, relativeFilePath):
  path = os.path.join(
    'files',
    'user-files',
    'mediaFiles',
    'library-{}'.format(library.id))
  # Ensure Path Exists
  abs_path = os.path.join(settings.MEDIA_ROOT, path)
  if not os.path.exists(abs_path):
    os.makedirs(abs_path, 0775)

  return os.path.join(path,relativeFilePath)


################################
#                              #
#        JobResultFiles        #
#                              #
################################


## Generate Path for a JobResultFile upload.
#
# @note This is intended to only be used by the JobResultFile.resultFile
# models.FileField.
# @param instance JobResultFile instance.
# @param filename Name of the uploaded file.
# @return Path of the destination file, relative to MEDIA_ROOT

def getJobResultFileUploadFilePath(instance, filename):
  path = os.path.join(
                      'files',
                      'user-files',
                      'job-result-files',
                      'project-{}'.format(instance.job.project.id)
                      )
  # Ensure Path Exists
  abs_path = os.path.join(settings.MEDIA_ROOT, path)
  if not os.path.exists(abs_path):
    os.makedirs(abs_path, 0775)

  return os.path.join(path,filename)


##############################
#                            #
#        Export Files        #
#                            #
##############################


## Generate Path for an Export file, given it's base filename.
#
# @param user User object.
# @param filename Name of the export file.
# @return Path of the destination file, relative to MEDIA_ROOT

def getExportFilePath(user, filename):
  path = os.path.join(
                      'files',
                      'user-files',
                      'exports',
                      'user-{}'.format(user.id)
                      )
  # Ensure Path Exists
  abs_path = os.path.join(settings.MEDIA_ROOT, path)
  if not os.path.exists(abs_path):
    os.makedirs(abs_path, 0775)

  return os.path.join(path,filename)


#############################
#                           #
#        Cache Files        #
#                           #
#############################


## Get the path to the jpeg (spectra image) cache storage.
#
# @note Relative to MEDIA_ROOT and MEDIA_URL
# @param mediaFile MediaFile object that corresponds to the image.
# @return Path of the destination file, relative to MEDIA_ROOT

def getUsersJpegCacheDirectory(mediaFile):
  path = os.path.join(
    'files',
    'user-files',
    'cache',
    'library-{}'.format(mediaFile.library.id),
    'jpgs')
  # Ensure Path Exists
  abs_path = os.path.join(settings.MEDIA_ROOT, path)
  if not os.path.exists(abs_path):
    os.makedirs(abs_path, 0775)

  return path


## Get the path to the segment (wav) cache storage.
#
# @note Relative to MEDIA_ROOT and MEDIA_URL
# @param mediaFile MediaFile object that corresponds to the segment.
# @return Path of the destination file, relative to MEDIA_ROOT

def getUsersSegmentCacheDirectory(mediaFile):
  path = os.path.join(
    'files',
    'user-files',
    'cache',
    'library-{}'.format(mediaFile.library.id),
    'segments')
  # Ensure Path Exists
  abs_path = os.path.join(settings.MEDIA_ROOT, path)
  if not os.path.exists(abs_path):
    os.makedirs(abs_path, 0775)

  return path
