from django import forms

# don't import the plugin base directly...
# https://yapsy.readthedocs.io/en/latest/Advices.html#plugin-class-detection-caveat
import plugins.ArloJobPluginBase as ArloJobPluginBase


class TestJobPlugin(ArloJobPluginBase.ArloJobPluginBase):

    class SettingsForm(forms.Form):
        name = forms.CharField(label='Job Name', max_length=255)

    # Overrides

    def getSettingsForm(self, project=None, data=None):
        """
        return a tuple of a Django Form to render, and the name of the
        template to use for rendering it.
        """
        return (self.SettingsForm(data), 'TestJobPlugin_configure.html')

    def parseSettingsForm(self, project, form):
        """
        Parse a form and return a dictionary of settings for the Job if
        data is valid, else return None
        {
            'name': Name of the Job,
            'params': list of tuples of (key, value) settings for the plugin.
        }
        'params would then be saved as JobParameters, so all 'value' types
        need to be string < 255 bytes.
        'params' MUST have a key for 'PluginName' to save with the Job
        """
        if not isinstance(form, self.SettingsForm):
            return None
        if not form.is_valid():
            return None
        data = form.cleaned_data
        return {
                'name': data.get('name'),
                'params': [('PluginName', 'TestJobPlugin')]
                }

    def showInLaunchUI(self):
        """
        Disable this test plugin by default. Change below to enable for
        testing/development.
        """
        return False

    def run(self, job_id, api_client, logger):
        """
        job_id - The Database ID of the Job to run
        api_client - Initialized ArloApiClient object
        logger - log instance
        """
        # Load the Job Settings
        logger.debug("Starting TestJobPlugin run()")

        api_client.jobs.updateJobStatus(job_id, "Running")

        # format the parameters
        job_details = api_client.jobs.getJobDetails(job_id)
        params = {}
        for entry in job_details['jobparameters_set']:
            if entry['name'] not in params:
                params[entry['name']] = []
            params[entry['name']].append(entry['value'])

        api_client.jobs.addJobLog(job_id, "TestJobPlugin Received the following parameters: {}".format(repr(params)))
        api_client.jobs.addJobLog(job_id, "TestJobPlugin Uploading JobResult File 'test.txt' with content 'Test'")
        api_client.jobs.addJobResultFile(job_id, 'test.txt', 'Test', 'Test JobResultFile')
        api_client.jobs.updateJobStatus(job_id, "Complete")
