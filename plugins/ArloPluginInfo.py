from ConfigParser import NoOptionError
from yapsy.PluginInfo import PluginInfo

class ArloPluginInfo(PluginInfo):
    """
    ARLO's custom PluginInfo class, adds additional details to the base
    Yapsy PluginInfo
    """

    def __getFriendlyName(self):
        try:
            return self.details.get("Core","friendlyname")
        except NoOptionError:
            return self.name

    def __setFriendlyName(self, friendlyName):
        if not self.details.has_section("Core"):
            self.details.add_section("Core")
        self.details.set("Core","friendlyname", friendlyName)

    friendlyName = property(fget=__getFriendlyName,fset=__setFriendlyName)
