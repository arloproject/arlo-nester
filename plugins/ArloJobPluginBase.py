import os
import sys

from yapsy.IPlugin import IPlugin


class ArloJobPluginBase(IPlugin):
    """
    Base class for ARLO Job Plugins

    Plugin Classes will have two primary functions:
        - Define the User interface for Job settings (ran within the Nester
          interface processes)
        - Run the Jobs. (via a stand-alone process)

    Django Views
    ------------

    Plugins define the user interface via a set of Python functions and a
    Django HTML template.

    Interface code in tools/jobs.py will load the configuration form and
    template and manage launching new jobs.

    - getSettingsForm
        Builds the Django Form instance to use during configuration and returns
        the name of the template (provided in the plugin) to render it.

    - parseSettingsForm
        Parse a settings form and return a dictionary of settings to save for
        the new Job

    Run
    ---

    The run() function will perform the actual Job tasks, typically being
    called by QueueRunner.

    It is important to note that this function WILL NOT have access to the
    Django Database, so all data access MUST be provided via the API.

    Rules / Design Goals
    --------------------

    - Plugins MAY import and use Django classes, for example to define
      the interface forms.
    - Plugin functions MUST NOT do anything that requires database access,
      as the stand-alone QueueRunner process will not be connected to the
      DB. All data must be accessed/saved via the Nester REST API.
    """

    def getPluginTemplateDir(self):
        """
        Return the absolute path to the directory where this plugin's
        templates are located. This generally should not need to be overridden.
        """
        return os.path.join(
                  os.path.dirname(sys.modules[self.__module__].__file__),
                  'templates')

    ###
    # Override these in the Plugins

    def getSettingsForm(self, project=None, data=None):
        """
        return a tuple of a Django Form to render, and the name of the
        template to use for rendering it.
        """
        raise NotImplementedError

    def parseSettingsForm(self, project, form):
        """
        Parse a form and return a dictionary of settings for the Job if
        data is valid, else return None
        {
            'name': Name of the Job,
            'params': list of tuples of (key, value) settings for the plugin.
        }
        'params would then be saved as JobParameters, so all 'value' types
        need to be string < 255 bytes
        """
        raise NotImplementedError

    def showInLaunchUI(self):
        """
        Whether to display this plugin as an option on the Launch Page.
        """
        return True

    def run(self, job_id, api_client, logger):
        """
        job_id - The Database ID of the Job to run
        api_client - Initialized ArloApiClient object
        logger - log instance

        Called to actually run the Job, usually by QueueRunner or similar
        process.

        NOTE: This function will likely be ran in an environment without access
        to the Django database.
        """
        raise NotImplementedError
