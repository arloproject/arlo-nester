import logging
from yapsy.PluginManager import PluginManager

from django.conf import settings

from .ArloPluginInfo import ArloPluginInfo

pluginManager = None

if pluginManager is None:
    logging.debug("Loading Plugin Manager")
    pluginManager = PluginManager()
    # manually set the Plugin Info class
    # can't set this in init due to https://github.com/tibonihoo/yapsy/pull/8
    # looks like pip hasn't picked up this change
    pluginManager.getPluginLocator().setPluginInfoClass(ArloPluginInfo)
    pluginManager.setPluginPlaces(settings.NESTER_PLUGIN_DIRS)
    pluginManager.collectPlugins()
    # Activate all loaded plugins
    for pluginInfo in pluginManager.getAllPlugins():
        logging.debug("Loading Plugin: %s", pluginInfo.name)
        pluginManager.activatePluginByName(pluginInfo.name)


class ArloPluginManager(object):
    """
    Interface through which we'll access activated plugins.
    """

    @staticmethod
    def getJobPlugins():
        """
        return a list of the activated ARLO Job Plugins, as PluginInfo objects
        """
        return pluginManager.getAllPlugins()

    @staticmethod
    def getJobPluginByName(name):
        """
        Return the plugin_object for the Plugin.
        Returns None if not found.
        """
        plugin = pluginManager.getPluginByName(name)
        if plugin is None:
            return None
        return plugin.plugin_object
