from collections import defaultdict
import contextlib
import timeit

from django import forms

# don't import the plugin base directly...
# https://yapsy.readthedocs.io/en/latest/Advices.html#plugin-class-detection-caveat
import plugins.ArloJobPluginBase as ArloJobPluginBase


WEIGHTS_CHOICES = (
    ('uniform', 'Uniform'),
    ('distance', 'Distance'),
)
ALGORITHM_CHOICES = (
    ('auto', 'Auto'),
    ('ball_true', 'Ball Tree'),
    ('kd_tree', 'KD Tree'),
    ('brute', 'Brute Force'),
)

MAX_TAGS_PER_REQUEST = 1000


class KNNClassifierJobPlugin(ArloJobPluginBase.ArloJobPluginBase):

    class SettingsForm(forms.Form):
        from tools.models import TagSet, TagClass

        name = forms.CharField(label='Job Name', max_length=255)

        n_neighbors = forms.IntegerField(label='n Neighbors', initial=5, required=True)
        weights = forms.ChoiceField(choices=WEIGHTS_CHOICES, required=True)
        algorithm = forms.ChoiceField(choices=ALGORITHM_CHOICES, required=True)

        exampleTagSet = forms.ModelChoiceField(TagSet.objects.none(), label="Class Examples TagSet", empty_label=None, required=False)
        sourceTagSet = forms.ModelChoiceField(TagSet.objects.none(), label="Source TagSet", empty_label=None, required=False)
        destinationTagSet = forms.ModelChoiceField(TagSet.objects.none(), label="Destination TagSet", empty_label=None, required=False)

        # Spectra Details
        numFrequencyBands = forms.IntegerField(label='Num Frequency Bands', initial=128)
        numTimeFramesPerSecond = forms.IntegerField(label='Num Time Frames Per Second', initial=100)
        dampingRatio = forms.FloatField(label='Damping Ratio', initial=0.02)
        minFrequency = forms.FloatField(label='Min Frequency', initial=60.0)
        maxFrequency = forms.FloatField(label='Max Frequency', initial=12000.0)


    def getSettingsForm(self, project=None, data=None):
        """
        return a tuple of a Django Form to render, and the name of the
        template to use for rendering it.
        """

        form = self.SettingsForm(data)
        tagSets = project.getTagSets(include_hidden=False)
        form.fields['exampleTagSet'] = forms.ModelChoiceField(tagSets, label="Class Examples TagSet", required=False)
        form.fields['sourceTagSet'] = forms.ModelChoiceField(tagSets, label="Source TagSet", required=False)
        form.fields['destinationTagSet'] = forms.ModelChoiceField(tagSets, label="Destination TagSet", required=False)
        return (form, 'KNNClassifierJobPlugin_configure.html')

    def parseSettingsForm(self, project, form):
        """
        Parse a form and return a dictionary of settings for the Job if
        data is valid, else return None
        {
            'name': Name of the Job,
            'params': list of tuples of (key, value) settings for the plugin.
        }
        'params would then be saved as JobParameters, so all 'value' types
        need to be string < 255 bytes.
        'params' MUST have a key for 'PluginName' to save with the Job
        """

        if not isinstance(form, self.SettingsForm):
            return None
        if not form.is_valid():
            return None
        data = form.cleaned_data

        params = [
            ('PluginName', 'KNNClassifierJobPlugin'),
            ('n_neighbors', data['n_neighbors']),
            ('weights', data['weights']),
            ('algorithm', data['algorithm']),
            ('exampleTagSet', data['exampleTagSet'].id),
            ('sourceTagSet', data['sourceTagSet'].id),
            ('destinationTagSet', data['destinationTagSet'].id),
            ('numFrequencyBands', data['numFrequencyBands']),
            ('numTimeFramesPerSecond', data['numTimeFramesPerSecond']),
            ('dampingRatio', data['dampingRatio']),
            ('minFrequency', data['minFrequency']),
            ('maxFrequency', data['maxFrequency']),
            ]

        return {
            'name': data.get('name'),
            'params': params
            }


    def run(self, job_id, api_client, logger):
        """
        job_id - The Database ID of the Job to run
        api_client - Initialized ArloApiClient object
        logger - log instance
        """
        logger.debug("Starting KNNClassifierPlugin run() - id: %d", job_id)
        api_client.jobs.updateJobStatus(job_id, "Running")


        # Run Imports
        import numpy
        from sklearn import neighbors

        # Load the Job Settings

        ###
        # Get parameters

        job_details = api_client.jobs.getJobDetails(job_id)
        params = defaultdict(list)
        for entry in job_details['jobparameters_set']:
            params[entry['name']].append(entry['value'])

        logger.debug("Received the following parameters: {}".format(repr(params)))

        def get_first(l, default=None):
            "Get the first item in a list, or None"
            if l:
                for x in l:
                    return x
            return default

        n_neighbors = int(get_first(params.get('n_neighbors')))
        weights = get_first(params.get('weights'))
        algorithm = get_first(params.get('algorithm'))

        exampleTagSet_id = int(get_first(params.get('exampleTagSet')))
        sourceTagSet_id = int(get_first(params.get('sourceTagSet')))
        destinationTagSet_id = int(get_first(params.get('destinationTagSet')))

        numFrequencyBands = int(get_first(params.get('numFrequencyBands')))
        numTimeFramesPerSecond = int(get_first(params.get('numTimeFramesPerSecond')))
        dampingRatio = float(get_first(params.get('dampingRatio')))
        minFrequency = float(get_first(params.get('minFrequency')))
        maxFrequency = float(get_first(params.get('maxFrequency')))

        ###
        # Load Tags

        exampleTags = api_client.tagSet.getTagsInTagSet(exampleTagSet_id)
        logger.debug("Loaded %d Example Tags", len(exampleTags))
        with timed_execution('Load Example Vectors', job_id, api_client, logger):
            exampleTags_X, exampleTags_y, _ = self._buildTagSpectraVectors(
                exampleTags,
                api_client,
                numFrequencyBands,
                numTimeFramesPerSecond,
                dampingRatio,
                minFrequency,
                maxFrequency
            )

        sourceTags = api_client.tagSet.getTagsInTagSet(sourceTagSet_id)
        logger.debug("Loaded %d Source Tags", len(sourceTags))
        with timed_execution('Load Source Vectors', job_id, api_client, logger):
            sourceTags_X, _, sourceTags_ids = self._buildTagSpectraVectors(
                sourceTags,
                api_client,
                numFrequencyBands,
                numTimeFramesPerSecond,
                dampingRatio,
                minFrequency,
                maxFrequency
                )

        ###
        # Pad Feature Arrays

        self._padArraysToMax([exampleTags_X, sourceTags_X])

        ###
        # Convert to Numpy Arrays

        exampleTags_X = numpy.array(exampleTags_X, dtype=numpy.int32)
        exampleTags_y = numpy.array(exampleTags_y)
        sourceTags_X = numpy.array(sourceTags_X, dtype=numpy.int32)

        ###
        # Initialize Model

        with timed_execution('Build Model', job_id, api_client, logger):
            clf = neighbors.KNeighborsClassifier(
                n_neighbors=n_neighbors,
                weights=weights,
                algorithm=algorithm)
            clf.fit(exampleTags_X, exampleTags_y)

        ###
        # Run Classification

        with timed_execution('Classify', job_id, api_client, logger):
            Z = clf.predict(sourceTags_X)
            classified_tags = zip(Z, sourceTags_ids)

        ###
        # Create new Tags

        new_tags = []

        for tagClass_id, sourceTag_id in classified_tags:
            # find the source tag
            sourceTag = None
            for tag in sourceTags:
                if tag['id'] == sourceTag_id:
                    sourceTag = tag
                    break

            if sourceTag is None:
                logger.warn("Didn't find Source Tag %d", sourceTag_id)
            else:
                new_tags.append({
                    "tagSet": destinationTagSet_id,
                    "mediaFile": sourceTag['mediaFile'],
                    "startTime": sourceTag['startTime'],
                    "endTime": sourceTag['endTime'],
                    "minFrequency": sourceTag['minFrequency'],
                    "maxFrequency": sourceTag['maxFrequency'],
                    "tagClass": tagClass_id,
                    "randomlyChosen": False,
                    "machineTagged": True,
                    "userTagged": False,
                    "strength": 1
                })

        api_client.jobs.addJobLog(job_id, "Saving {} Classified Tags".format(len(new_tags)))
        logger.debug("Saving %d New Tags", len(new_tags))
        with timed_execution('Saving {} New Tags'.format(len(new_tags)), job_id, api_client, logger):
            api_client.tag.bulkCreate(job_details['project'], new_tags)

        api_client.jobs.updateJobStatus(job_id, "Complete")

    @staticmethod
    def _buildTagSpectraVectors(tags, api_client, numFrequencyBands, numTimeFramesPerSecond, dampingRatio, minFrequency, maxFrequency):
        """
        Generate the Spectra for a list of Tags and build into a list of
        Vectors for sklearn

        return (X, y, tag_ids)
            X - numpy.array(float64) of Tag spectra vectors
            y - numpy.array of Tag Classes
            tag_ids = list of Tag.id
        """

        X = []
        y = []
        tagIds = []
        for tag in tags:
            spectra = api_client.spectra.getAudioSpectraData(
                tag['mediaFile'],
                tag['startTime'],
                tag['endTime'],
                numFrequencyBands,
                numTimeFramesPerSecond,
                dampingRatio,
                minFrequency,
                maxFrequency
            )

            spectra_vector = []
            for k in sorted(spectra['spectraData']['frames'].keys(), key=lambda x: float(x)):
                spectra_vector.extend(spectra['spectraData']['frames'][k])

            X.append(spectra_vector)
            y.append(tag['tagClass'])
            tagIds.append(tag['id'])

        return (X, y, tagIds)


    @staticmethod
    def _padArraysToMax(arrays):
        """
        Pad the feature vectors so that all are the same length
        """
        max_v_len = 0
        for a in arrays:
            for v in a:
                if len(v) > max_v_len:
                    max_v_len = len(v)
        for a in arrays:
            for v in a:
                v.extend([0 for _ in xrange(max_v_len - len(v))])


@contextlib.contextmanager
def timed_execution(name, job_id, api_client, logger):
    start_time = timeit.default_timer()
    yield None
    msg = "'%s' in %f seconds" % (name, timeit.default_timer() - start_time)
    if logger is not None:
        logger.info(msg)
    if api_client is not None and job_id is not None:
        api_client.jobs.addJobLog(job_id, msg)
