import io

from django.core.exceptions import SuspiciousFileOperation
from django.template import TemplateDoesNotExist
from django.template.loaders.base import Loader as BaseLoader
from django.utils._os import safe_join

cached_plugin_template_dirs = None

class Loader(BaseLoader):
    """
    Custom Template Loader that will load Templates included in ARLO Plugins.

    Templates for plugins are stored in their /templates/ directory.

    Currently unenforced, but the naming convention is that templates should
    be prepended with '$pluginName_', e.g., 'TestJobPlugin_config.html'
    """
    is_usable = True

    def _cache_plugin_template_dirs(self):
        # late import to prevent possible circular references since this class
        # may initially be loaded at startup
        global cached_plugin_template_dirs
        from .ArloPluginManager import ArloPluginManager
        manager = ArloPluginManager()
        cached_plugin_template_dirs = []
        for plugin in manager.getJobPlugins():
            d = plugin.plugin_object.getPluginTemplateDir()
            cached_plugin_template_dirs.append(d)

    def get_template_sources(self, template_name, template_dirs=None):
        """
        Returns the absolute paths to "template_name", when appended to each
        plugin's template directory. Any paths that don't lie inside one of the
        template dirs are excluded from the result set, for security reasons.
        """

        if cached_plugin_template_dirs is None:
            self._cache_plugin_template_dirs()

        for template_dir in cached_plugin_template_dirs:
            try:
                yield safe_join(template_dir, template_name)
            except SuspiciousFileOperation:
                # The joined path was located outside of this template_dir
                # (it might be inside another one, so this isn't fatal).
                pass

    def load_template_source(self, template_name, template_dirs=None):
        tried = []
        for filepath in self.get_template_sources(template_name, template_dirs):
            try:
                with io.open(filepath, encoding=self.engine.file_charset) as fp:
                    return fp.read(), filepath
            except IOError:
                tried.append(filepath)
        if tried:
            error_msg = "Tried %s" % tried
        else:
            error_msg = ("Your template directories configuration is empty. "
                         "Change it to point to at least one template directory.")
        raise TemplateDoesNotExist(error_msg)
    load_template_source.is_usable = True
