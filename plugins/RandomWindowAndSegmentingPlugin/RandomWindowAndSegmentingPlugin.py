from collections import defaultdict
import random

from django import forms

# don't import the plugin base directly...
# https://yapsy.readthedocs.io/en/latest/Advices.html#plugin-class-detection-caveat
import plugins.ArloJobPluginBase as ArloJobPluginBase


MODE_CHOICES = (
    ('RandomWindow', 'Random Windows'),
    ('RandomTag', 'Random Tags'),
    ('Segment', 'Segment Files')
    )

MAX_TAGS_PER_REQUEST = 1000

class RandomWindowAndSegmentingPlugin(ArloJobPluginBase.ArloJobPluginBase):

    class SettingsForm(forms.Form):
        from tools.models import TagSet, TagClass

        name = forms.CharField(label='Job Name', max_length=255)

        mode = forms.ChoiceField(choices=MODE_CHOICES, required=True)
        sizeInSeconds = forms.FloatField(label='Window/Tag Size in Seconds', initial=4,required=True)
        # Random Window / Tag Options
        count = forms.IntegerField(label='Number of Windows/Tags to Generate', initial=100, required=False)
        weight = forms.ChoiceField(choices=(("Time", "Time"), ("File", "File")), required=False)
        maxOverlap = forms.FloatField(label='Allowable Overlap', initial=0.1, required=False, min_value=0.0, max_value=1.0)
        # Random Tags / Segmenting
        tagSet = forms.ModelChoiceField(TagSet.objects.none(), label="TagSet", empty_label=None, required=False)
        tagClass = forms.ModelChoiceField(TagClass.objects.none(), label="TagClass", empty_label=None, required=False)
        # Segmenting
        stepInSeconds = forms.FloatField(label='Step Size in Seconds', initial=4,required=False)

        def clean(self):
            """
            Add our custom validators to adjust the necessary fields based on
            which mode is selected.
            """
            cleaned_data = super(RandomWindowAndSegmentingPlugin.SettingsForm, self).clean()
            mode = cleaned_data.get("mode")

            required_fields = []
            if mode == 'RandomWindow':
                required_fields = ['count', 'weight', 'maxOverlap']
            elif mode == 'RandomTag':
                required_fields = ['count', 'weight', 'maxOverlap', 'tagSet', 'tagClass']
            elif mode == 'Segment':
                required_fields = ['tagSet', 'tagClass', 'stepInSeconds']
            else:
                self.add_error(None, "Invalid Mode Selected")

            missing_fields = []
            for field in required_fields:
                if cleaned_data.get(field) is None:
                    missing_fields.append(field)
                    self.add_error(field, "Required for {} mode.".format(mode))
            if missing_fields:
                missing_fields_str = ','.join(["'{}'".format(field) for field in missing_fields])
                self.add_error(None, "{} required for {} mode.".format(missing_fields_str, mode))

    def getSettingsForm(self, project=None, data=None):
        """
        return a tuple of a Django Form to render, and the name of the
        template to use for rendering it.
        """

        form = self.SettingsForm(data)
        form.fields['tagSet'] = forms.ModelChoiceField(project.getTagSets(include_hidden=False), label="TagSet", required=False)
        form.fields['tagClass'] = forms.ModelChoiceField(project.getTagClasses(), label="TagClass", required=False)
        return (form, 'RandomWindowAndSegmentingPlugin_configure.html')

    def parseSettingsForm(self, project, form):
        """
        Parse a form and return a dictionary of settings for the Job if
        data is valid, else return None
        {
            'name': Name of the Job,
            'params': list of tuples of (key, value) settings for the plugin.
        }
        'params would then be saved as JobParameters, so all 'value' types
        need to be string < 255 bytes.
        'params' MUST have a key for 'PluginName' to save with the Job
        """
        if not isinstance(form, self.SettingsForm):
            return None
        if not form.is_valid():
            return None
        data = form.cleaned_data

        params = [
            ('PluginName', 'RandomWindowAndSegmentingPlugin'),
            ('mode', data['mode']),
            ('sizeInSeconds', str(data['sizeInSeconds'])),
            ]

        mode = data['mode']
        if mode == 'RandomWindow':
            params += [
                ('count', str(data['count'])),
                ('weight', data['weight']),
                ('maxOverlap', data['maxOverlap']),
                ]
        elif mode == 'RandomTag':
            params += [
                ('count', str(data['count'])),
                ('weight', data['weight']),
                ('maxOverlap', data['maxOverlap']),
                ('tagSet', str(data['tagSet'].id)),
                ('tagClass', str(data['tagClass'].id)),
                ]
        elif mode == 'Segment':
            params += [
                ('stepInSeconds', str(data['stepInSeconds'])),
                ('tagSet', str(data['tagSet'].id)),
                ('tagClass', str(data['tagClass'].id)),
                ]
        else:
            return None

        return {
            'name': data.get('name'),
            'params': params
            }

    def run(self, job_id, api_client, logger):
        """
        job_id - The Database ID of the Job to run
        api_client - Initialized ArloApiClient object
        logger - log instance
        """
        # Load the Job Settings
        logger.debug("Starting RandomWindowAndSegmentingPlugin run()")

        api_client.jobs.updateJobStatus(job_id, "Running")

        ###
        # Get parameters

        job_details = api_client.jobs.getJobDetails(job_id)
        params = defaultdict(list)
        for entry in job_details['jobparameters_set']:
            params[entry['name']].append(entry['value'])

        logger.debug("RandomWindowAndSegmentingPlugin Received the following parameters: {}".format(repr(params)))

        def get_first(l, default=None):
            "Get the first item in a list, or None"
            if l:
                for x in l:
                    return x
            return default

        mode = get_first(params.get('mode'))
        sizeInSeconds = float(get_first(params.get('sizeInSeconds')))

        ###
        # Load Project MediaFiles

        filesDurations = [] # a list of tuples, (mediaFileId, duration)

        mediaFiles = api_client.project.getProjectMediaFiles(job_details['project'])
        for mediaFile in mediaFiles:
            # find the file duration in the mediaFile metadata
            duration = None
            for md in mediaFile.get('mediafilemetadata_set', []):
                if md.get('name') == 'durationInSeconds':
                    duration = float(md.get('value'))
                    break
            if duration > sizeInSeconds:
                filesDurations.append( (mediaFile['id'], duration) )
            else:
                logger.debug("Skipping MediaFile '{}' with duration '{}'".format(mediaFile['id'], duration))

        if not filesDurations:
            api_client.jobs.addJobLog(job_id, "Did not find any valid MediaFiles")
            api_client.jobs.updateJobStatus(job_id, "Error")
            return

        logger.debug("Found %s files for Random Tagging", len(filesDurations))


        ###
        # Generate based on mode

        if mode in ['RandomWindow', 'RandomTag']:

            weight = get_first(params.get('weight'))
            count = int(get_first(params.get('count')))
            maxOverlapSeconds = params.get('maxOverlap')
            if maxOverlapSeconds is not None:
                maxOverlapSeconds = float(get_first(maxOverlapSeconds)) * sizeInSeconds

            ##
            # Depending on weight, choose how to generate the windows

            randomWindows = []
            if weight == 'File':
                randomWindows = self._generateFileWeightedWindows(filesDurations, count, maxOverlapSeconds, sizeInSeconds, logger)
                if len(randomWindows) != count:
                    raise Exception("RandomWindowAndSegmentingPlugin Didn't Generate Expected Number of Windows")

            elif weight == 'Time':
                randomWindows = self._generateTimeWeightedWindows(filesDurations, count, maxOverlapSeconds, sizeInSeconds, logger)
                if len(randomWindows) != count:
                    raise Exception("RandomWindowAndSegmentingPlugin Didn't Generate Expected Number of Windows")

            else:
                api_client.jobs.addJobLog(job_id, "Invalid Weight '{}'".format(weight))
                api_client.jobs.updateJobStatus(job_id, "Error")
                return

            # Save as Tags or RandomWindows
            if mode == 'RandomWindow':
                logger.debug("Saving %s RandomWindows", len(randomWindows))
                for randomWindow in randomWindows:
                    api_client.randomWindow.createRandomWindow(job_id, **randomWindow)
                logger.debug("Finished saving RandomWindows")
            else:
                tagSetId = get_first(params.get('tagSet'))
                tagClassId = get_first(params.get('tagClass'))
                tags = []
                for randomWindow in randomWindows:
                    tags.append({
                        "tagSet": tagSetId,
                        "mediaFile": randomWindow['mediaFile_id'],
                        "startTime": randomWindow['startTime'],
                        "endTime": randomWindow['endTime'],
                        "minFrequency": 20,
                        "maxFrequency": 20000,
                        "tagClass": tagClassId,
                        "randomlyChosen": True,
                        "machineTagged": True,
                        "userTagged": False,
                        "strength": 1
                    })
                api_client.jobs.addJobLog(job_id, "Saving {} Tags".format(len(tags)))
                api_client.tag.bulkCreate(job_details['project'], tags)
                logger.debug("Finished saving Tags")

        elif mode == 'Segment':
            tagSetId = get_first(params.get('tagSet'))
            tagClassId = get_first(params.get('tagClass'))
            stepInSeconds = float(get_first(params.get('stepInSeconds')))
            if stepInSeconds <= 0:
                api_client.jobs.addJobLog(job_id, "Invalid stepInSeconds '{}'".format(stepInSeconds))
                api_client.jobs.updateJobStatus(job_id, "Error")
            tags = []
            for (mediaFile, fileDuration) in filesDurations:
                startTime = 0
                while startTime <= (fileDuration - sizeInSeconds):
                    tags.append({
                        "tagSet": tagSetId,
                        "mediaFile": mediaFile,
                        "startTime": startTime,
                        "endTime": startTime + sizeInSeconds,
                        "minFrequency": 20,
                        "maxFrequency": 20000,
                        "tagClass": tagClassId,
                        "randomlyChosen": False,
                        "machineTagged": True,
                        "userTagged": False,
                        "strength": 1
                    })
                    startTime += stepInSeconds
            api_client.jobs.addJobLog(job_id, "Saving {} Tags".format(len(tags)))
            api_client.tag.bulkCreate(job_details['project'], tags)
            logger.debug("Done Segmenting")

        else:  # mode
            api_client.jobs.addJobLog(job_id, "Unknown Mode '{}'".format(mode))
            api_client.jobs.updateJobStatus(job_id, "Error")
            return


        api_client.jobs.updateJobStatus(job_id, "Complete")


    @classmethod
    def _findFileGaps(cls, fileDuration, maxOverlapSeconds, sizeInSeconds, priorWindows):
        """
        Find the gaps in a file where new random Tags/Windows are possible.

        maxOverlapSeconds - If None, disable overlap checking and use entire file
        priorWindows - List of previously generated windows to exlcude.

        Returns a list of tuples of gaps, (gap_start_time, gap_duration)
        """
        sortedPriorWindows = sorted(priorWindows, key=lambda w: w['startTime'])
        gaps = []  # list of tuples, (gap_start_time, gap_duration)

        # if allowing full overlap, entire file is game
        if maxOverlapSeconds is None or maxOverlapSeconds == sizeInSeconds:
            return [(0, fileDuration - sizeInSeconds)]

        lastWindowEnd = 0
        for w in sortedPriorWindows:
            gap_duration = w['startTime'] - lastWindowEnd + maxOverlapSeconds - sizeInSeconds
            if lastWindowEnd > 0:
                gap_duration += maxOverlapSeconds
                gap_start_time = lastWindowEnd - maxOverlapSeconds
            else:
                gap_start_time = lastWindowEnd
            if gap_duration > 0:
                gaps.append( (gap_start_time, gap_duration) )
            lastWindowEnd = w['endTime']

        if lastWindowEnd == 0:
            gap_duration = fileDuration - sizeInSeconds
            if gap_duration > 0:
                gaps.append( (0, gap_duration) )
        else:
            gap_start_time = lastWindowEnd - maxOverlapSeconds
            if gap_start_time < 0:
                gap_start_time = 0
            gap_duration = fileDuration - sizeInSeconds - gap_start_time
            if gap_duration > 0:
                gaps.append( (gap_start_time, gap_duration) )

        return gaps


    @classmethod
    def _generateFileWeightedWindows(cls, filesDurations, numWindowsToGenerate, maxOverlapSeconds, sizeInSeconds, logger):
        """
        Generate a set of Random Tags/Windows by first randomly selecting an
        available file, then randomly selecting an available time within that
        file.

        filesDurations - List of tuples describing the available files
                         (mediaFile, fileDuration)
        numWindowsToGenerate - How many Windows we'll return
        maxOverlapSeconds - time, in seconds, to allow generated Tags to
                            overlap. If None, disable overlap checking and use
                            entire file.
        sizeInSeconds - length of the generated Windows

        Return a list of dictionaries:
        [{
            'startTime',
            'endTime',
            'mediaFile_id'
        },]

        If the requested number of Windows can not be generated, raise an Exception.
        """

        randomWindows = []
        _filesDurations = filesDurations[:]  # copy since we'll be deleting entries

        # Randomly choose a file, then choose a point within that file
        while numWindowsToGenerate > 0:
            if len(_filesDurations) <= 0:
                raise Exception("Ran out of MediaFiles with {} Windows Left".format(numWindowsToGenerate))
            randomFileIndex = random.randint(0, (len(_filesDurations) - 1))
            (mediaFile, fileDuration) = _filesDurations[randomFileIndex]

            # compute the time available taking into account other generated Tags
            priorWindows = [w for w in randomWindows if w['mediaFile_id'] == mediaFile]

            gaps = cls._findFileGaps(fileDuration, maxOverlapSeconds, sizeInSeconds, priorWindows)
            if len(gaps) == 0:
                # remove file from list, restart
                if logger:
                    logger.debug("Skipping MediaFile '%d' for further Windows, fully consumed.", mediaFile)
                del _filesDurations[randomFileIndex]
                continue

            avail_duration = 0
            for (gap_start_time, gap_duration) in gaps:
                avail_duration += gap_duration

            start_time = random.random() * (avail_duration)
            windowStartTime = None

            # step through the gaps, finding the startTime
            for (gap_start_time, gap_duration) in gaps:
                if start_time <= gap_duration:
                    windowStartTime = gap_start_time + start_time
                    break
                else:
                    start_time -= gap_duration

            if windowStartTime is None:
                if logger:
                    logger.error("RandomWindowAndSegmentingPlugin: Unknown Error - Failed assigning Window")
                raise Exception("RandomWindowAndSegmentingPlugin: Unknown Error - Failed assigning Window")

            windowEndTime = windowStartTime + sizeInSeconds

            randomWindows.append({
                'startTime': windowStartTime,
                'endTime': windowEndTime,
                'mediaFile_id': mediaFile
                })
            numWindowsToGenerate -= 1

        return randomWindows

    @classmethod
    def _generateTimeWeightedWindows(cls, filesDurations, numWindowsToGenerate, maxOverlapSeconds, sizeInSeconds, logger):
        """
        Generate a set of Random Tags/Windows by concatenating all available
        gaps from all files together and selecting randomly within.

        filesDurations - List of tuples describing the available files
                         (mediaFile, fileDuration)
        numWindowsToGenerate - How many Windows we'll return
        maxOverlapSeconds - time, in seconds, to allow generated Tags to
                            overlap. If None, disable overlap checking and use
                            entire file.
        sizeInSeconds - length of the generated Windows

        Return a list of dictionaries:
        [{
            'startTime',
            'endTime',
            'mediaFile_id'
        },]

        If the requested number of Windows can not be generated, raise an Exception.
        """

        randomWindows = []
        while numWindowsToGenerate > 0:

            # concatenate all files into one time series, and pick points along that series
            fileGaps = []
            for (mediaFile, fileDuration) in filesDurations:
                priorWindows = [w for w in randomWindows if w['mediaFile_id'] == mediaFile]
                gaps = cls._findFileGaps(fileDuration, maxOverlapSeconds, sizeInSeconds, priorWindows)
                fileGaps.append( (mediaFile, gaps) )

            totalDuration = 0.0
            for (mediaFile, gaps) in fileGaps:
                for (gap_start_time, gap_duration) in gaps:
                    totalDuration += gap_duration

            if totalDuration == 0.0:
                raise Exception('Error Generating Window - No Available Gaps, {} Windows Left'.format(numWindowsToGenerate))

            # choose a random time in the series, find the file it is in
            randomWindowStartTime = random.random() * totalDuration
            windowStartTime = None
            randomTime = randomWindowStartTime
            for (mediaFile, gaps) in fileGaps:
                for (gap_start_time, gap_duration) in gaps:
                    if randomTime < gap_duration:
                        windowStartTime = gap_start_time + randomTime
                        break
                    randomTime -= gap_duration
                if windowStartTime is not None:
                    # break out to preserve mediaFile
                    break

            if windowStartTime is None:
                raise Exception('Error finding Window - randomWindowStartTime: {}'.format(randomWindowStartTime))

            randomWindows.append({
                'startTime': windowStartTime,
                'endTime': windowStartTime + sizeInSeconds,
                'mediaFile_id': mediaFile
                })
            numWindowsToGenerate -= 1

        return randomWindows
