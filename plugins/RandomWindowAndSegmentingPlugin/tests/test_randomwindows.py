# pylint: disable=protected-access

from django.test import TestCase

from ..RandomWindowAndSegmentingPlugin import RandomWindowAndSegmentingPlugin


class RandomWindowsTest(TestCase):
    """
    Test RandomWindowPlugin methods.
    """


    def test_filegaps(self):

        gaps = RandomWindowAndSegmentingPlugin._findFileGaps(
            fileDuration=10,
            maxOverlapSeconds=2,
            sizeInSeconds=2,
            priorWindows=[]
        )
        self.assertEqual(gaps, [(0, 8)] )

        # Add in a priorTag to exclude
        priorWindows = [{
            'startTime': 3,
            'endTime': 5,
        },]
        gaps = RandomWindowAndSegmentingPlugin._findFileGaps(
            fileDuration=10,
            maxOverlapSeconds=0,
            sizeInSeconds=2,
            priorWindows=priorWindows
        )
        self.assertEqual(gaps, [
            (0, 1),
            (5, 3)
        ])

        # And allowing overlap...
        gaps = RandomWindowAndSegmentingPlugin._findFileGaps(
            fileDuration=10,
            maxOverlapSeconds=0.5,
            sizeInSeconds=2,
            priorWindows=priorWindows
        )
        self.assertEqual(gaps, [
            (0, 1.5),
            (4.5, 3.5)
        ])

        # Lot's of overlaps that should leave us with no gaps
        priorWindows = [
            {'startTime': 1, 'endTime': 3},
            {'startTime': 4, 'endTime': 6},
            {'startTime': 7, 'endTime': 9},
        ]
        gaps = RandomWindowAndSegmentingPlugin._findFileGaps(
            fileDuration=10,
            maxOverlapSeconds=0,
            sizeInSeconds=2,
            priorWindows=priorWindows
        )
        self.assertEqual(gaps, [])

        # If we allow full overlap, we want to be left with one gap for the
        # whole file
        gaps = RandomWindowAndSegmentingPlugin._findFileGaps(
            fileDuration=10,
            maxOverlapSeconds=2,
            sizeInSeconds=2,
            priorWindows=priorWindows
        )
        self.assertEqual(gaps, [
            (0, 8),
        ])

        # If we allow full overlap, we want to be left with one gap for the
        # whole file - use None
        gaps = RandomWindowAndSegmentingPlugin._findFileGaps(
            fileDuration=10,
            maxOverlapSeconds=None,
            sizeInSeconds=2,
            priorWindows=priorWindows
        )
        self.assertEqual(gaps, [
            (0, 8),
        ])


    def test_generateFileWeightedWindows(self):

        filesDurations = [
            (1, 10),
            (2, 20),
        ]

        windows = RandomWindowAndSegmentingPlugin._generateFileWeightedWindows(
            filesDurations=filesDurations,
            numWindowsToGenerate=1,
            maxOverlapSeconds=0,
            sizeInSeconds=2,
            logger=None)
        self.assertTrue(len(windows) > 0)

        # Should fail if we ask for too many windows without overlap
        with self.assertRaises(Exception):
            windows = RandomWindowAndSegmentingPlugin._generateFileWeightedWindows(
                filesDurations=filesDurations,
                numWindowsToGenerate=100,
                maxOverlapSeconds=0,
                sizeInSeconds=2,
                logger=None)

        # If we allow full overlap, should allow any number of windows
        windows = RandomWindowAndSegmentingPlugin._generateFileWeightedWindows(
            filesDurations=filesDurations,
            numWindowsToGenerate=1000,
            maxOverlapSeconds=2,
            sizeInSeconds=2,
            logger=None)
        self.assertTrue(len(windows) > 0)

    def test_generateTimeWeightedWindows(self):

        filesDurations = [
            (1, 10),
            (2, 20),
        ]

        windows = RandomWindowAndSegmentingPlugin._generateTimeWeightedWindows(
            filesDurations=filesDurations,
            numWindowsToGenerate=10,
            maxOverlapSeconds=None,
            sizeInSeconds=2,
            logger=None
        )
        self.assertTrue(len(windows) > 0)

        # Should fail if we ask for too many windows without overlap
        with self.assertRaises(Exception):
            windows = RandomWindowAndSegmentingPlugin._generateTimeWeightedWindows(
                filesDurations=filesDurations,
                numWindowsToGenerate=100,
                maxOverlapSeconds=0,
                sizeInSeconds=2,
                logger=None)
            print windows

        # If we allow full overlap, should allow any number of windows
        windows = RandomWindowAndSegmentingPlugin._generateTimeWeightedWindows(
            filesDurations=filesDurations,
            numWindowsToGenerate=1000,
            maxOverlapSeconds=2,
            sizeInSeconds=2,
            logger=None)
        self.assertTrue(len(windows) > 0)
