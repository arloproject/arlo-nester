from django.conf.urls import url
from django.http import HttpResponseRedirect

from views import chooseProject, chooseTagSet, chooseMetaTaggingForm, metaTaggingOverview, classifyTag, exportClassifications, visualizeClassifications

urlpatterns = [

  url(r'^$', lambda x: HttpResponseRedirect('chooseProject')),

  url(r'^chooseProject$', 
        chooseProject.chooseProject),

  url(r'^chooseTagSet/(?P<projectId>\d+)$', 
        chooseTagSet.chooseTagSet, 
        name='chooseTagSet'),

  url(r'^chooseMetaTaggingForm/(?P<projectId>\d+)/(?P<tagSetId>\d+)$', 
        chooseMetaTaggingForm.chooseMetaTaggingForm, 
        name='chooseMetaTaggingForm'),

  url(r'^metaTaggingOverview/(?P<projectId>\d+)/(?P<tagSetId>\d+)/(?P<tagMetaFormId>\d+)$', 
        metaTaggingOverview.metaTaggingOverview, 
        name='metaTaggingOverview'),

  url(r'^classifyTag/(?P<projectId>\d+)/(?P<tagSetId>\d+)/(?P<tagMetaFormId>\d+)/(?P<tagIndex>\d+)$', 
        classifyTag.classifyTag, 
        name='classifyTag'),

  url(r'^exportUserClassifications/(?P<projectId>\d+)/(?P<tagSetId>\d+)/(?P<tagMetaFormId>\d+)$', 
        exportClassifications.exportClassifications, 
        {'requestUserOnly':True}, 
        name='exportUserClassifications'),

  url(r'^exportAllClassifications/(?P<projectId>\d+)/(?P<tagSetId>\d+)/(?P<tagMetaFormId>\d+)$', 
        exportClassifications.exportClassifications, 
        {'requestUserOnly':False}, 
        name='exportAllClassifications'),

  url(r'^visualizeAllClassifications/(?P<projectId>\d+)/(?P<tagSetId>\d+)/(?P<tagMetaFormId>\d+)$', 
        visualizeClassifications.visualizeAllClassifications, 
        name='visualizeAllClassifications'),
    
]
