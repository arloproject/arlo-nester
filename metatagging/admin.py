from django.contrib import admin
from metatagging.models import TagMetaForm, TagMetaField, TagMetaFieldOption, TagMeta


class TagMetaFormAdmin(admin.ModelAdmin):
  fields = ['user',
            'name',
            'templateData']

class TagMetaFieldAdmin(admin.ModelAdmin):
  fields = ['form',
            'required',
            'name',
            'label',
            'initial',
            'widget']

class TagMetaFieldOptionAdmin(admin.ModelAdmin):
  fields = ['field',
            'label']

class TagMetaAdmin(admin.ModelAdmin):
  fields = ['user',
            'dateTagged',
            'tag',
            'option']


admin.site.register(TagMetaForm, TagMetaFormAdmin)
admin.site.register(TagMetaField, TagMetaFieldAdmin)
admin.site.register(TagMetaFieldOption, TagMetaFieldOptionAdmin)
admin.site.register(TagMeta, TagMetaAdmin)
