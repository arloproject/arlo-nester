import datetime

from django.db import models


### Meta Tagging
#
# This is an experimental interface evolved from the PennSound Poetry Tagging
# application. There's been incredible response from participants requesting
# their own versions, and unfortunately, that application wasn't really
# extensible, and creating new apps for each user isn't practical.

# Instead, we make the entire app dynamic, generating the tagging forms based
# on values stored in the database. I'm not yet sure how scalable this will
# be, as we're adding a lot of DB overhead into the mix, but I think this
# will work out.

# NOTE
# As of now, this model only allows for multiple-choice style data. That is,
# there is no free entry possibilities. So far, all requested use cases have
# been select-from-choices so I think this will work out fine.

class TagMetaForm(models.Model):
  user         = models.ForeignKey('auth.User')
  name         = models.CharField(max_length=255)
  templateData = models.TextField(max_length=16777215)

  def __str__(self):
    return '%s' % (self.name)


class TagMetaField(models.Model):
  RADIO              = 1
  SELECT             = 2
  CHECKBOX           = 3
  # Special, overrides all other required fields and only this one will be
  # recorded as 'True'
  EXCLUSIVE_SUBMIT   = 4
  # didn't think we'd ever need this one, but here we are
  # this is basically a Submit Button that DOES store all of the other fields
  # This is never required.
  # recorded as 'True'
  SUBMIT             = 5
  WIDGET_CHOICES = (
    (RADIO,              "Radio"),
    (SELECT,             "Select"),
    (CHECKBOX,           "Checkbox"),
    (EXCLUSIVE_SUBMIT,   "Exclusive Submit"),
    (SUBMIT,             "Submit"),
  )

  # Not all data applicable to all widgets
  # CHECKBOX MUST Have Two Options, "True" and "False"
  # SUBMIT must be manually built into the HTML Template
  # EXCLUSIVE must have exactly one TagMetaFieldOption, named anything

  form      = models.ForeignKey(TagMetaForm)
  required  = models.BooleanField(default=None)
  name      = models.CharField(max_length=255)  # no spaces, used in the HTML form name
  label     = models.CharField(max_length=255)
  widget    = models.IntegerField(null=False, choices=WIDGET_CHOICES)
  initial   = models.ForeignKey('TagMetaFieldOption', null=True, default=None)
#  exclusive = models.BooleanField()  # If exclusive, this and only this field can be saved

  def __str__(self):
    return '%s' % (self.name)

  def getCustomName(self):
    return '%s' % self.name

  def getHtmlFormId(self):
    return 'id_%s' % self.getCustomName()


class TagMetaFieldOption(models.Model):
  field = models.ForeignKey(TagMetaField)
  label = models.CharField(max_length=255)

  def __str__(self):
    return '%s' % (self.label)


class TagMeta(models.Model):
  user       = models.ForeignKey('auth.User')
  dateTagged = models.DateTimeField(default=datetime.datetime.now, null=False)
  
  # NOTE - We're removing TagExample - see tools/models.py
  # TODO
  tagExample     = models.ForeignKey('tools.TagExample', null=True, default=None)
  tag        = models.ForeignKey('tools.Tag')
  option     = models.ForeignKey(TagMetaFieldOption)



