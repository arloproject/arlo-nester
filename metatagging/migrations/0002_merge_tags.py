# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class UniqueTagNotFound(Exception):
    def __init__(self, value):
        super(UniqueTagNotFound, self).__init__()
        self.value = value
    def __str__(self):
        return repr(self.value)


def update_tag_from_tagexample(apps, schema_editor):
    TagMeta = apps.get_model("metatagging", "TagMeta")
    Tag = apps.get_model("tools", "Tag")

    # Update Tags from TagExample
    for tagMeta in TagMeta.objects.all():
        tagExample = tagMeta.tagExample

        # Find Tag that corresponds to this TagExample
        tags = Tag.objects.filter(tagExample=tagExample)
        if tags.count() != 1:
            raise UniqueTagNotFound('{} Tags Found for TagExample {}'.format(tags.count(), tagExample.id))

        tagMeta.tag = tags[0]
        tagMeta.tagExample = None
        tagMeta.save()


class Migration(migrations.Migration):

    dependencies = [
        ('tools', '0010_remove_imagePath'),
        ('metatagging', '0001_initial'),
    ]

    operations = [
        migrations.AlterField(
            model_name='tagmeta',
            name='tagExample',
            field=models.ForeignKey(default=None, to='tools.TagExample', null=True),
        ),
        migrations.AddField(
            model_name='tagmeta',
            name='tag',
            field=models.ForeignKey(default=None, to='tools.Tag', null=True),
        ),

        ###
        # Update the TagExample reference to Tag

        migrations.RunPython(update_tag_from_tagexample),


        ###
        # Remove the Default / null options

        migrations.AlterField(
            model_name='tagmeta',
            name='tag',
            field=models.ForeignKey(to='tools.Tag', null=False),
        ),

    ]
