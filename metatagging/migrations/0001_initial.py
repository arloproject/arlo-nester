# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
import datetime
from django.conf import settings


class Migration(migrations.Migration):

    dependencies = [
        ('tools', '0001_initial'),
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
    ]

    operations = [
        migrations.CreateModel(
            name='TagMeta',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('dateTagged', models.DateTimeField(default=datetime.datetime.now)),
            ],
        ),
        migrations.CreateModel(
            name='TagMetaField',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('required', models.BooleanField(default=None)),
                ('name', models.CharField(max_length=255)),
                ('label', models.CharField(max_length=255)),
                ('widget', models.IntegerField(choices=[(1, b'Radio'), (2, b'Select'), (3, b'Checkbox'), (4, b'Exclusive Submit'), (5, b'Submit')])),
            ],
        ),
        migrations.CreateModel(
            name='TagMetaFieldOption',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('label', models.CharField(max_length=255)),
                ('field', models.ForeignKey(to='metatagging.TagMetaField')),
            ],
        ),
        migrations.CreateModel(
            name='TagMetaForm',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('name', models.CharField(max_length=255)),
                ('templateData', models.TextField(max_length=16777215)),
                ('user', models.ForeignKey(to=settings.AUTH_USER_MODEL)),
            ],
        ),
        migrations.AddField(
            model_name='tagmetafield',
            name='form',
            field=models.ForeignKey(to='metatagging.TagMetaForm'),
        ),
        migrations.AddField(
            model_name='tagmetafield',
            name='initial',
            field=models.ForeignKey(default=None, to='metatagging.TagMetaFieldOption', null=True),
        ),
        migrations.AddField(
            model_name='tagmeta',
            name='option',
            field=models.ForeignKey(to='metatagging.TagMetaFieldOption'),
        ),
        migrations.AddField(
            model_name='tagmeta',
            name='tagExample',
            field=models.ForeignKey(to='tools.TagExample'),
        ),
        migrations.AddField(
            model_name='tagmeta',
            name='user',
            field=models.ForeignKey(to=settings.AUTH_USER_MODEL),
        ),
    ]
