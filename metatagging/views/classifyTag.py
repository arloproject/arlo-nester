import logging

from django.contrib.auth.decorators import login_required
from django.http import HttpResponse, HttpResponseRedirect, HttpResponseNotAllowed, HttpResponseServerError
from django.shortcuts import render, redirect
from django import forms
from django.core.urlresolvers import reverse
from django.template import Template, RequestContext
from django.conf import settings

from tools.models import TagSet, Tag, UserSettings, VisualizeAudioForm
from metatagging.models import TagMeta, TagMetaField, TagMetaForm, TagMetaFieldOption


## Dynamically build a Django Form from the database data
#
# Finally got this working compliments of
# http://jacobian.org/writing/dynamic-form-generation/
# @param tagMetaForm TagMetaForm Object
# @return forms.Form Object

class TagMetaCustomForm(forms.Form):
  tagId = forms.IntegerField(
    required=True,
    label="Tag Id",
    widget=forms.TextInput(attrs={'readonly':'1'}))

  def __init__(self, *args, **kwargs):
    tagMetaForm = kwargs.pop('tagMetaForm')

    super(TagMetaCustomForm, self).__init__(*args, **kwargs)

    custom_fields = TagMetaField.objects.filter(form=tagMetaForm)
    for field in custom_fields:
      logging.info("Adding Field: " + str(field.name))
      choices = TagMetaFieldOption.objects.filter(field=field)
      choices_dict = []
      for choice in choices:
        choices_dict.append( (choice.id, choice.label) )

      field_name = field.getCustomName()
      initial = None
      if field.widget == TagMetaField.RADIO:
        if field.initial:
          initial=field.initial.id

        self.fields[field_name] = forms.ChoiceField(
            widget=forms.RadioSelect,
            label=field.label,
            required=field.required,
            choices=choices_dict,
            initial=initial)
      elif field.widget == TagMetaField.SELECT:
        if field.initial:
          initial=field.initial.id

        self.fields[field_name] = forms.ChoiceField(
            widget=forms.Select,
            label=field.label,
            required=field.required,
            choices=choices_dict,
            initial=initial)
      elif field.widget == TagMetaField.CHECKBOX:
        initial = False
        if (field.initial is not None) and (field.initial.label == 'True'):
          initial = True
        self.fields[field_name] = forms.BooleanField(
            label=field.label,
            required=False,
            initial=initial)
      elif field.widget == TagMetaField.EXCLUSIVE_SUBMIT:
        pass  # manually added in Template
      elif field.widget == TagMetaField.SUBMIT:
        pass  # manually added in Template
      else:
        logging.error("ERROR - UNHANDLED WIDGET IN TagMetaCustomForm")


## Classify a Tag from the specified TagSet
# @param request The Django HTTP request object.
# @param projectId The Database Id of the selected Project.
# @param tagSetId The Database Id of the selected TagSet.
# @param tagIndex The index, starting from 0, of the Tag(Example) to classify.
# Tags from the TagSet are ordered by id.
# @return Django HTTP response object.

@login_required
def classifyTag(request, projectId, tagSetId, tagMetaFormId, tagIndex):
  user = request.user

  tagSet  =  TagSet.objects.get(id=tagSetId)
  userSettings = UserSettings.objects.get(user=user, name='default')
  tagMetaForm = TagMetaForm.objects.get(id=tagMetaFormId)

  # Get the Tags we're tagging
  tags = Tag.objects.filter(tagSet=tagSet).order_by("id")
  tagIndex = int(tagIndex)

  overviewUrl = reverse('metaTaggingOverview', args=(projectId, tagSetId, tagMetaFormId))

  if (tagIndex > 0):
    prevUrl = reverse('classifyTag', args=(projectId, tagSetId, tagMetaFormId, tagIndex-1))
  else:
    prevUrl = None

  if tagIndex < (tags.count()-1):
    nextUrl = reverse('classifyTag', args=(projectId, tagSetId, tagMetaFormId, tagIndex+1))
  else:
    nextUrl = None


  # Where are we in the set?
  if (tagIndex >= tags.count()):
    # end of Tags
    return HttpResponseRedirect(overviewUrl)

  tag = tags[tagIndex]

  # Get Existing TagMeta entries
  try:
    existingTagMetaEntries = TagMeta.objects.filter(user=user, tag=tag, option__field__form=tagMetaForm)
  except:
    existingTagMetaEntries = None


  if request.method == 'GET':
    initial={'tagId': tag.id}
    if existingTagMetaEntries:
      for existingTME in existingTagMetaEntries:
        if existingTME.option.field.widget == TagMetaField.RADIO:
          initial[existingTME.option.field.getCustomName()] = existingTME.option.id
        elif existingTME.option.field.widget == TagMetaField.CHECKBOX:
          initial[existingTME.option.field.getCustomName()] = (existingTME.option.label == "True")
        elif existingTME.option.field.widget == TagMetaField.SELECT:
          initial[existingTME.option.field.getCustomName()] = (existingTME.option.id)
#      logging.info("######")
#      for k in initial:
#        logging.info(str(k) + " - " + str(initial[k]))

    form = TagMetaCustomForm(
      tagMetaForm=tagMetaForm,
      initial=initial)


  elif request.method == 'POST':

    form = TagMetaCustomForm(
      request.POST,
      tagMetaForm=tagMetaForm)

    ###
    # verify this is still the right tag

    if not 'tagId' in request.POST:
      return HttpResponseServerError("Missing Tag Id")

    if (int(request.POST['tagId']) != tag.id):
      return HttpResponseServerError("Mismatched Tag Id")


    ###
    # Check for EXCLUSIVE options (i.e., skip the rest of this tagging)

    for field in TagMetaField.objects.filter(form=tagMetaForm):
      if field.widget == TagMetaField.EXCLUSIVE_SUBMIT:
        if field.getHtmlFormId() in request.POST:
          # Save the field exclusively

          logging.info("************************************ Exclusive Field " + str(field.name))
          # delete existing
          if existingTagMetaEntries is not None:
            existingTagMetaEntries.delete()

          option = TagMetaFieldOption.objects.get(field=field)
          TagMeta(
              user=user,
              tag=tag,
              option=option).save()

          return redirect('classifyTag',
              projectId=projectId,
              tagSetId=tagSetId,
              tagMetaFormId = tagMetaFormId,
              tagIndex=tagIndex+1)



    ###
    # Not Skipping, try to save data

    if form.is_valid():

      ###
      # Finally, we get to save all the things!

      # delete existing
      if existingTagMetaEntries is not None:
        existingTagMetaEntries.delete()

      # Save all fields
      cleaned_data = form.cleaned_data
      for field in TagMetaField.objects.filter(form=tagMetaForm):
        logging.info("************************************ Checking Field " + str(field.name))

        if field.widget == TagMetaField.SUBMIT:
          if field.getHtmlFormId() in request.POST:
            option = TagMetaFieldOption.objects.get(field=field)
            # Save
            TagMeta(
                user=user,
                tag=tag,
                option=option).save()

        elif field.required or ( (field.getCustomName() in cleaned_data) and (cleaned_data[field.getCustomName()] != "")):
          logging.info("************************************ Found Field " + str(field.name))

          if field.widget == TagMetaField.RADIO:
            try:
              option = TagMetaFieldOption.objects.get(field=field, id=cleaned_data[field.getCustomName()])
            except:
              return HttpResponse("Invalid Choice - " + field.getCustomName() + " Data: '" + cleaned_data[field.getCustomName()] + "' Required: " + str(field.required))

          if field.widget == TagMetaField.SELECT:
            try:
              option = TagMetaFieldOption.objects.get(field=field, id=cleaned_data[field.getCustomName()])
            except:
              return HttpResponse("Invalid Choice - " + field.getCustomName() + " Data: '" + cleaned_data[field.getCustomName()] + "' Required: " + str(field.required))

          elif field.widget == TagMetaField.CHECKBOX:
            selection = cleaned_data[field.getCustomName()]
            try:
              if selection:
                option = TagMetaFieldOption.objects.get(field=field, label="True")
              else:
                option = TagMetaFieldOption.objects.get(field=field, label="False")
            except:
              return HttpResponse("Invalid Choice - " + field.getCustomName())

          # Save
          TagMeta(
              user=user,
              tag=tag,
              option=option).save()

      return redirect('classifyTag',
          projectId=projectId,
          tagSetId=tagSetId,
          tagMetaFormId = tagMetaFormId,
          tagIndex=tagIndex+1)

  else:
    return HttpResponseNotAllowed(['GET', 'POST'])



  ###
  # Fall here to return the form

  spectraForm = VisualizeAudioForm({
    'alias':tag.mediaFile.alias,
    'mediaFileId':tag.mediaFile.id,
    'startTime' : tag.startTime,
    'endTime' : tag.endTime,
    'gain' : 1.0,
    'spectraNumFramesPerSecond': userSettings.fileViewSpectraNumFramesPerSecond,
    'spectraNumFrequencyBands': userSettings.fileViewSpectraNumFrequencyBands,
    'spectraDampingFactor': userSettings.fileViewSpectraDampingFactor,
    'spectraMinimumBandFrequency': tag.minFrequency,
    'spectraMaximumBandFrequency': tag.maxFrequency,
    'tagSets': None,
    'loadAudio': userSettings.loadAudio,
    'contextWindow': 0,
    })

  spectraForm.fields['tagSets'] = forms.ModelMultipleChoiceField(tagSet.project.tagset_set.all().order_by('name'), required=False)



  response_dict = {
      'apacheRoot':settings.APACHE_ROOT,
      'user':user,
      'form':form,
      'spectraForm': spectraForm,
      'tag': tag,
      'existingTagMetaEntries': existingTagMetaEntries,
      'overviewUrl': overviewUrl,
      'prevUrl': prevUrl,
      'nextUrl': nextUrl,
      'audioID':tag.mediaFile.id,
      }

  # Generate the custom user form
  myTemplate = Template(tagMetaForm.templateData)
  context = RequestContext(request, response_dict)
  tagMetaFormTemplateData = myTemplate.render(context)

  response_dict['tagMetaFormTemplateData'] = tagMetaFormTemplateData

  return render(request, 'classifyTag.html', response_dict)
