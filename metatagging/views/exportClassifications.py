import csv

from django.contrib.auth.decorators import login_required
from django.http import HttpResponse

from tools.models import Project, TagSet
from metatagging.models import TagMetaForm, TagMeta, TagMetaField


## Export Tag Classifications as a CSV.
# @param request The Django HTTP request object.
# @param projectId The Database Id of the selected Project.
# @param tagSetId The Database Id of the selected TagSet.
# @param tagMetaFormId The Database Id of the select TagMetaForm
# @param requestUserOnly Export only the request user's classifications if True, all classifications otherwise.
# @return Django HTTP response object.

@login_required
def exportClassifications(request, projectId, tagSetId, tagMetaFormId, requestUserOnly):
  user = request.user

  project = Project.objects.get(id=projectId)
  tagSet  =  TagSet.objects.get(id=tagSetId)
  tagMetaForm = TagMetaForm.objects.get(id=tagMetaFormId)

  # Get 'column' names - names of every field in the form
  fields = TagMetaField.objects.filter(form=tagMetaForm)
  field_data = {}  # map field id to a tuple of (name, label, column)
  column = 3
  field_names_list = ['userName', 'tag_id', 'dateTagged']

  for field in fields:
    field_names_list.append(field.label)
    field_data[field.id] = (field.name, field.label, column)
    column += 1

  num_columns = column


  ###
  # Setup CSV Export

  response = HttpResponse(content_type='text/csv')
  sFilename = "MetaTagging-%d-%d-%d.csv" % (project.id, tagSet.id, tagMetaForm.id)
  response['Content-Disposition'] = 'attachment; filename="' + sFilename + '"'

  writer = csv.writer(response)
  writer.writerow(field_names_list)

  # Get TagMeta entries in this form
  tagMetas = TagMeta.objects.filter(option__field__form=tagMetaForm, tag__tagSet=tagSet).order_by("tag__id")
  if requestUserOnly:
    tagMetas = tagMetas.filter(user=user)

  # find which Tags we have
  tagIds = list(set(tagMetas.values_list('tag_id', flat=True)))

  # for each Tag, find and export the data
  for teid in tagIds:

    #separate row per user
    for uid in list(set(tagMetas.filter(tag__id=teid).values_list('user_id', flat=True))):

      # create a blank row
      row = [''] * num_columns
      row[1] = teid
      for tm in tagMetas.filter(tag__id=teid, user=uid):
        # we're repeating ourselves here, but deal with it
        row[0] = tm.user.username
        row[2] = tm.dateTagged

        (fieldName, fieldLabel, col) = field_data[tm.option.field.id]
        row[col] = tm.option.label

      writer.writerow(row)

  return response
