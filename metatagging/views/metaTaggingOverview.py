from django.contrib.auth.decorators import login_required
from django.http import HttpResponseNotAllowed
from django.shortcuts import render, redirect
from django.core.urlresolvers import reverse
from django.conf import settings

from tools.models import TagSet, Tag
from metatagging.models import TagMeta, TagMetaForm

##
# @param request The Django HTTP request object.
# @param projectId The Database Id of the selected Project.
# @param tagSetId The Database Id of the selected TagSet.
# @param tagMetaFormId The Database Id of the selected MetaTaggingForm
# (i.e., what data are we going to be tagging).
# @return Django HTTP response object.

@login_required
def metaTaggingOverview(request, projectId, tagSetId, tagMetaFormId):
  user = request.user

  tagSet      = TagSet.objects.get(id=tagSetId)
  tagMetaForm = TagMetaForm.objects.get(id=tagMetaFormId)

  exportUserClassificationsUrl = reverse('exportUserClassifications', args=(projectId, tagSetId, tagMetaFormId))
  exportAllClassificationsUrl = reverse('exportAllClassifications', args=(projectId, tagSetId, tagMetaFormId))
  visualizeAllClassificationsUrl = reverse('visualizeAllClassifications', args=(projectId, tagSetId, tagMetaFormId))


  if request.method == 'GET':

    # Show the overview page

    # NOTE - optimized querysets - change with caution - all of these optimized on a 100,000 set list

    # Get Tags in TagSet
    tags = Tag.objects.filter(tagSet=tagSet).order_by("id")

    # count the number of classified Tags
    # find the first unclassified Tag
    # get all classifications at once to avoid pounding on the database

    existingMetaTags = TagMeta.objects.select_related('tag').filter(
      user=user,
      option__field__form=tagMetaForm,
      tag__tagSet=tagSet,
    )

    # build a list of all Tag IDs
    tagIds = list(tags.values_list('id', flat=True))

    # Build a list of 'classified' Tag IDs
    classifiedTagIds = list(existingMetaTags.values_list('tag__id', flat=True))

    # search for the first 'Untagged' (unclassified) Tag
    firstUntaggedIndex = None
    numClassifiedTags = 0
    for i in range(len(tagIds)):
      if tagIds[i] in classifiedTagIds:
        numClassifiedTags += 1
      else:
        if firstUntaggedIndex is None:
          firstUntaggedIndex = i

    return render(request, 'metaTaggingOverview.html', {
        'apacheRoot':settings.APACHE_ROOT,
        'user':user,
        'numTags': tags.count(),
        'numClassifiedTags': numClassifiedTags,
        'firstUntaggedIndex': firstUntaggedIndex,
        'exportUserClassificationsUrl': exportUserClassificationsUrl,
        'exportAllClassificationsUrl': exportAllClassificationsUrl,
        'visualizeAllClassificationsUrl': visualizeAllClassificationsUrl,
        })

  elif request.method == 'POST':

    if 'gotoIndex' in request.POST:
      tagIndex = int(request.POST['gotoIndex'])
      return redirect('classifyTag', projectId=projectId, tagSetId=tagSetId, tagMetaFormId=tagMetaForm.id, tagIndex=tagIndex)

    # Jump to the first tag
    return redirect('classifyTag', projectId=projectId, tagSetId=tagSetId, tagMetaFormId=tagMetaForm.id, tagIndex=0)

  else:
    return HttpResponseNotAllowed(['GET', 'POST'])
