from django.contrib.auth.decorators import login_required
from django.http import HttpResponseNotAllowed
from django.shortcuts import render, redirect
from django import forms
from django.conf import settings

from metatagging.models import TagMetaForm


class ChooseMetaTaggingFormForm(forms.Form):
  metaTaggingForm = forms.ModelChoiceField(TagMetaForm.objects.all().order_by("name"), required=True)

## Select a TagSet to use for the Poetry Tagging
# @param request The Django HTTP request object.
# @param projectId The Database Id of the selected Project.
# @param tagSetId The Database Id of the selected TagSet.
# @return Django HTTP response object.

@login_required
def chooseMetaTaggingForm(request, projectId, tagSetId):
  user = request.user

  if request.method == 'GET':
    form = ChooseMetaTaggingFormForm()

    return render(request, 'chooseMetaTaggingForm.html', {
      'apacheRoot':settings.APACHE_ROOT,
      'user':user,
      'form':form,
      })

  elif request.method == 'POST':
    form = ChooseMetaTaggingFormForm(request.POST)

    if not form.is_valid():
      return render(request, 'chooseMetaTaggingForm.html', {
        'apacheRoot':settings.APACHE_ROOT,
        'user':user,
        'form':form,
        })

    data = form.cleaned_data
    metaTaggingForm = data['metaTaggingForm']

    return redirect('metaTaggingOverview', projectId=projectId, tagSetId=tagSetId, tagMetaFormId=metaTaggingForm.id)


  else:
    return HttpResponseNotAllowed(['GET', 'POST'])
