from django.contrib.auth.decorators import login_required
from django.shortcuts import render
from django.core.urlresolvers import reverse
from django.conf import settings

from metatagging.models import TagMetaForm
from tools.models import Project, TagSet


## Dump data into a JavaScript visualization page.
# @param request The Django HTTP request object.
# @param projectId The Database Id of the selected Project.
# @param tagSetId The Database Id of the selected TagSet.
# @return Django HTTP response object.

@login_required
def visualizeAllClassifications(request, projectId, tagSetId, tagMetaFormId):
  user = request.user

  project = Project.objects.get(id=projectId)
  tagSet  =  TagSet.objects.get(id=tagSetId)
  tagMetaForm = TagMetaForm.objects.get(id=tagMetaFormId)

  overviewUrl = reverse('metaTaggingOverview', args=(projectId, tagSetId, tagMetaFormId))

  return render(request, 'visualizeTagMeta.html', {
    'apacheRoot':settings.APACHE_ROOT,
    'user':user,
    'project': project,
    'tagSet': tagSet,
    'tagMetaForm': tagMetaForm,
    'overviewUrl': overviewUrl,
    })
