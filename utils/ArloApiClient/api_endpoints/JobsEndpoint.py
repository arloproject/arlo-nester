import logging
import json
import socket

from ..Client import ArloApiClientException
from .BaseEndpoint import BaseEndpoint


logger = logging.getLogger(__name__)
logger.setLevel(logging.DEBUG)

class JobsEndpoint(BaseEndpoint):

    _jobStatusIdToName = None # dynamic cache, loaded as needed

    def _loadJobStatusNames(self):
        logger.debug("_getJobStatusNames() - Retrieving JobStatusNames")

        url = self.API_V0_URL + 'jobStatusTypes/'
        self._jobStatusIdToName = {}

        while url:
            resp = self._session.get(url, verify=self.VERIFY_SSL)
            if resp.status_code != 200:
                raise ArloApiClientException("Error in _getJobStatusNames - Status: %s Content: %s" % (resp.status_code, resp.content))
            data = json.loads(resp.content)
            for item in data['results']:
                self._jobStatusIdToName[item['id']] = item['name']
            url = data.get('next')

    def getJobStatusId(self, status_name):
        if self._jobStatusIdToName is None:
            self._loadJobStatusNames()
        for k,v in self._jobStatusIdToName.iteritems():
            if v == status_name:
                return k
        return None

    def getJobStatusName(self, status_id):
        if self._jobStatusIdToName is None:
            self._loadJobStatusNames()
        return self._jobStatusIdToName.get(status_id)

    def getQueuedPluginJob(self, jobPluginNames):
        """
        Use the ARLO API to search for Queued Jobs for the passed plugin names.
        """
        if not isinstance(jobPluginNames, list):
            plugins = [jobPluginNames]

        resp = self._session.post(self.API_V0_URL + 'getQueuedJob/', data={
            'jobTypes': [15],
            'jobPluginNames': jobPluginNames,
            }, verify=self.VERIFY_SSL )

        if resp.status_code == 204:
            logger.debug("_getQueuedJob() - No Queued Jobs Available")
            return None
        if resp.status_code == 200:
            data = json.loads(resp.content)
            job_id = data.get('id')
            logger.debug("_getQueuedJob() - Assigned Job: %s", job_id)
            return job_id

        raise ArloApiClientException("Error in getQueuedJob - Status: %s Content: %s" % (resp.status_code, resp.content))

    def getJobDetails(self, job_id):
        resp = self._session.get(self.API_V0_URL + 'jobs/{}/'.format(job_id), verify=self.VERIFY_SSL )

        if resp.status_code == 200:
            data = json.loads(resp.content)
            logger.debug("getJobDetails() - Job Details: %s", repr(data))
            return data

        raise ArloApiClientException("Error in getJobDetails - Status: %s Content: %s" % (resp.status_code, resp.content))

    def addJobLog(self, job_id, message):
        """
        Add a JobLog message to a Job. We prefix the message with the hostname
        and, if set, the UserAgent used for the API session.
        """
        logger.debug("addJobLog() - Adding JobLog - Job: %s  Message: %s", job_id, message)
        message_prefix = ""
        if 'User-Agent' in self._session.headers:
            message_prefix = "({} on {}) ".format(self._session.headers.get('User-Agent'), socket.gethostname())
        else:
            message_prefix = "({}) ".format(socket.gethostname())
        resp = self._session.post(self.API_V0_URL + 'jobLog/', data={
                'job': job_id,
                'message': message_prefix + message,
                }, verify=self.VERIFY_SSL )

        if resp.status_code != 201:
            raise ArloApiClientException("Error in addJobLog - Status: %s Content: %s" % (resp.status_code, resp.content))

    def updateJobStatus(self, job_id, status_name):
        logger.debug("updateJobStatus() - Updating Job: %s Status: %s", job_id, status_name)
        status_id = self.getJobStatusId(status_name)
        if status_id is None:
            raise ArloApiClientException("Error in updateJobStatus - Status: %s Not Found" % status_name)

        # Update Status
        resp = self._session.patch(self.API_V0_URL + 'jobs/{}/'.format(job_id), data={
                'status': status_id,
                }, verify=self.VERIFY_SSL )
        if resp.status_code != 200:
            raise ArloApiClientException("Error in updateJobStatus - Status: %s Content: %s" % (resp.status_code, resp.content))

        # Log it
        self.addJobLog(job_id, "Updated Job Status to '{}'".format(status_name))

    def addJobResultFile(self, job_id, filename, content, notes=None):
        """
        Upload a file to a JobResultFile for a Job.
        job_id -
        filename - Filename for the uploaded content.
        content - Content to save into the file, as a string.
        notes
        """
        logger.debug("addJobResultFile() - Uploading JobResultFile - Job: %s  filename: %s", job_id, filename)
        resp = self._session.post(self.API_V0_URL + 'jobResultFile/',
                                  data={
                                       'job': job_id,
                                       'notes': notes,
                                       },
                                  files={
                                        'resultFile': (filename, str(content))
                                        },
                                  verify=self.VERIFY_SSL )

        if resp.status_code != 201:
            raise ArloApiClientException("Error in addJobResultFile - Status: %s Content: %s" % (resp.status_code, resp.content))
