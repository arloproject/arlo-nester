import logging
import json

from ..Client import ArloApiClientException
from .BaseEndpoint import BaseEndpoint


logger = logging.getLogger(__name__)

class TagEndpoint(BaseEndpoint):

    def bulkCreate(self, project_id, tags):
        """
        'tags' is a list of dictionaries describing the Tags to create, of the
        same format as specified in the bulkTagCreate API function.

        Example:
        data = [
            {
                "tagSet": 769,
                "mediaFile": 179070,
                "startTime": 0.5,
                "endTime": 1.5,
                "minFrequency": 20,
                "maxFrequency": 20000,
                "tagClass": 6849,
                "parentTag": 1976960,
                "randomlyChosen": false,
                "machineTagged": true,
                "userTagged": false,
                "strength": 1
            },
            {
                "tagSet": 769,
                "mediaFile": 179070,
                "startTime": 2,
                "endTime": 3,
                "minFrequency": 20,
                "maxFrequency": 20000,
                "tagClass": 6849,
                "userTagged": true,
                "strength": 0.01
            }
        ]
        """
        url = self.API_V0_URL + 'bulkTagCreate/{}/'.format(project_id)
        TAGS_PER_REQUEST = 10
        start_index = 0

        while start_index < len(tags):
            logger.debug("POSTing bulkCreate - index %s", start_index)
            resp = self._session.post(
                url,
                verify=self.VERIFY_SSL,
                data={'tags': json.dumps(tags[start_index:start_index+TAGS_PER_REQUEST])}
                )
            if resp.status_code < 200 or resp.status_code >= 300: # Any 2xx is good
                raise ArloApiClientException("Error in bulkCreate - Start Index: %s  Status: %s Content: %s" % (start_index, resp.status_code, resp.content))
            start_index += TAGS_PER_REQUEST
