import logging
import json

from ..Client import ArloApiClientException
from .BaseEndpoint import BaseEndpoint


logger = logging.getLogger(__name__)
logger.setLevel(logging.DEBUG)

class ProjectEndpoint(BaseEndpoint):

    def getProjectMediaFiles(self, project_id):
        url = self.API_V0_URL + 'project/{}/mediaFiles'.format(project_id)
        mediaFiles = []

        while url:
            logger.debug("getProjectMediaFiles() - GET %s", url)
            resp = self._session.get(url, verify=self.VERIFY_SSL)
            if resp.status_code != 200:
                raise ArloApiClientException("Error in getProjectMediaFiles - Status: %s Content: %s" % (resp.status_code, resp.content))
            data = json.loads(resp.content)
            for item in data['results']:
                mediaFiles.append(item)
            url = data.get('next')

        return mediaFiles
