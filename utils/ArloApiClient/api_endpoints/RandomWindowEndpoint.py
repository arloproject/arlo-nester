import logging

from ..Client import ArloApiClientException
from .BaseEndpoint import BaseEndpoint


logger = logging.getLogger(__name__)
logger.setLevel(logging.DEBUG)

class RandomWindowEndpoint(BaseEndpoint):

    def createRandomWindow(self, job_id, mediaFile_id, startTime, endTime):
        url = self.API_V0_URL + 'randomWindow/'
        resp = self._session.post(
            url,
            verify=self.VERIFY_SSL,
            data={
                'randomWindowTaggingJob': job_id,
                'mediaFile': mediaFile_id,
                'startTime': startTime,
                'endTime': endTime
                }
            )
        if resp.status_code < 200 or resp.status_code >= 300: # Any 2xx is good
            raise ArloApiClientException("Error in createRandomWindow - Status: %s Content: %s" % (resp.status_code, resp.content))

        return
