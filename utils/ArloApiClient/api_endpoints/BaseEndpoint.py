import logging


logger = logging.getLogger(__name__)
logger.setLevel(logging.DEBUG)

class BaseEndpoint(object):

    API_V0_URL = None  # trailing /
    log = None
    _session = None
    VERIFY_SSL = False

    def __init__(self,
                 API_V0_URL,
                 session, # initialize requests session object
                 log=logger,
                 VERIFY_SSL=False):
        self.API_V0_URL = API_V0_URL
        self.log = log
        self._session = session
        self.VERIFY_SSL = VERIFY_SSL
