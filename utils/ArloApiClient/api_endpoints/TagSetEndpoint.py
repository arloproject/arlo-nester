import json

from ..Client import ArloApiClientException
from .BaseEndpoint import BaseEndpoint


class TagSetEndpoint(BaseEndpoint):

    def getTagsInTagSet(self, tagSet_id):
        url = self.API_V0_URL + 'tagSet/{}/tags/'.format(tagSet_id)
        tags = []

        while url:
            if self.log:
                self.log.debug("getTagsInTagSet() - GET %s", url)
            resp = self._session.get(url, verify=self.VERIFY_SSL)
            if resp.status_code != 200:
                raise ArloApiClientException("Error in getTagsInTagSet - Status: %s Content: %s" % (resp.status_code, resp.content))
            data = json.loads(resp.content)
            for item in data['results']:
                tags.append(item)
            url = data.get('next')

        return tags
