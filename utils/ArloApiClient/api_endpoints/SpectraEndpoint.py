import json

from ..Client import ArloApiClientException
from .BaseEndpoint import BaseEndpoint


class SpectraEndpoint(BaseEndpoint):

    def getAudioSpectraData(self, mediaFileId, startTime, endTime, numTimeFramesPerSecond, numFrequencyBands, dampingFactor, minFrequency, maxFrequency):

        url = self.API_V0_URL + 'getAudioSpectraData/{}/'.format(mediaFileId)

        if self.log:
            self.log.debug("getTagsInTagSet() - GET %s", url)

        params = {
            'startTime': startTime,
            'endTime': endTime,
            'numTimeFramesPerSecond': numTimeFramesPerSecond,
            'numFrequencyBands': numFrequencyBands,
            'dampingFactor': dampingFactor,
            'minFrequency': minFrequency,
            'maxFrequency': maxFrequency
        }
        resp = self._session.get(url, params=params, verify=self.VERIFY_SSL)

        if resp.status_code != 200:
            raise ArloApiClientException("Error in getAudioSpectraData - Status: %s Content: %s" % (resp.status_code, resp.content))
        data = json.loads(resp.content)
        return data
