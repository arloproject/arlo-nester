import logging
import requests


logger = logging.getLogger(__name__)

class ArloApiClientException(Exception):
    pass

class ArloApiClient(object):

    API_V0_URL = None  # trailing /
    API_V0_TOKEN = None

    log = None
    USER_AGENT = None
    VERIFY_SSL = False

    _session = None

    jobs = None

    def __init__(self,
                 API_V0_URL,
                 API_V0_TOKEN,
                 log=logger,
                 USER_AGENT='ArloApiClient/0.1',
                 VERIFY_SSL=False):
        self.API_V0_URL = API_V0_URL
        self.API_V0_TOKEN = API_V0_TOKEN
        self.log = log
        self.USER_AGENT = USER_AGENT
        self.VERIFY_SSL = VERIFY_SSL


        self._session = requests.Session()
        self._session.headers['User-Agent'] = self.USER_AGENT
        self._session.headers['Authorization'] = "Token " + self.API_V0_TOKEN
#         self._session.headers['Content-Type'] = "application/json"

        # late imports to initialize above items (namely exception)
        from .api_endpoints.JobsEndpoint import JobsEndpoint
        from .api_endpoints.ProjectEndpoint import ProjectEndpoint
        from .api_endpoints.TagEndpoint import TagEndpoint
        from .api_endpoints.RandomWindowEndpoint import RandomWindowEndpoint
        from .api_endpoints.TagSetEndpoint import TagSetEndpoint
        from .api_endpoints.SpectraEndpoint import SpectraEndpoint

        self.jobs = JobsEndpoint(self.API_V0_URL, self._session, self.log, self.VERIFY_SSL)
        self.project = ProjectEndpoint(self.API_V0_URL, self._session, self.log, self.VERIFY_SSL)
        self.tag = TagEndpoint(self.API_V0_URL, self._session, self.log, self.VERIFY_SSL)
        self.randomWindow = RandomWindowEndpoint(self.API_V0_URL, self._session, self.log, self.VERIFY_SSL)
        self.tagSet = TagSetEndpoint(self.API_V0_URL, self._session, self.log, self.VERIFY_SSL)
        self.spectra = SpectraEndpoint(self.API_V0_URL, self._session, self.log, self.VERIFY_SSL)
