import logging
import argparse
import time

from django.conf import settings

from plugins.ArloPluginManager import ArloPluginManager
from utils.ArloApiClient.Client import ArloApiClient

logger = logging.getLogger(__name__)
# handler = logging.StreamHandler()
# logger.addHandler(handler)
logger.setLevel(logging.DEBUG)

class QueueRunnerException(Exception):
    pass

class QueueRunner(object):

    ARLO_API_V0_URL = None  # trailing /
    API_TOKEN = None
    CYCLE_TIME = None

    VERIFY_SSL = False

    apiClient = None

    def __init__(self):

        args = vars(self._parseArgs())

        self.ARLO_API_V0_URL = args.get('ARLO_API_V0_URL')
        if self.ARLO_API_V0_URL[-1] != '/':
            self.ARLO_API_V0_URL = self.ARLO_API_V0_URL + '/'
        self.API_TOKEN = args.get('API_TOKEN')
        self.CYCLE_TIME = args.get('CYCLE_TIME')

        self.apiClient = ArloApiClient(
                 self.ARLO_API_V0_URL,
                 self.API_TOKEN,
                 log=logger,
                 USER_AGENT='QueueRunner/0.1',
                 VERIFY_SSL=self.VERIFY_SSL)

    @classmethod
    def _parseArgs(cls):
        """
        Parse the arguments from the CLI, return them, or exit if they're
        invalid.
        """

        # get settings defaults
        ARLO_API_V0_URL = getattr(settings, 'ARLO_API_V0_URL', None)

        qr_settings = getattr(settings, 'QUEUERUNNER', {})
        API_TOKEN = qr_settings.get('API_TOKEN')
        CYCLE_TIME = qr_settings.get('CYCLE_TIME')

        parser = argparse.ArgumentParser(
            description='Stand-alone daemon to process Jobs in the ARLO '
                        'QueueRunner system. Defaults for most parameters '
                        'are taken from the settings.py file.'
        )

        parser.add_argument('--ARLO_API_V0_URL',
                            required=ARLO_API_V0_URL is None,
                            default=ARLO_API_V0_URL,
                            help="Base URL for the v0 API.")
        parser.add_argument('--API_TOKEN',
                            required=API_TOKEN is None,
                            default=API_TOKEN,
                            help="Authentication Token for API Access.")
        parser.add_argument('--CYCLE_TIME',
                            type=int,
                            required=CYCLE_TIME is None,
                            default=CYCLE_TIME,
                            help="The time, in seconds, to wait before "
                                 "checking for new queued Jobs. If 0, do not "
                                 "cycle, only run through any queued Jobs "
                                 "then exit (similar to OneShot in the Java "
                                 "daemon).")
        return parser.parse_args()

    def run(self):
        job_plugins = ArloPluginManager.getJobPlugins()
        while True:
            for plugin in job_plugins:
                logger.info("Running Plugin: %s", plugin.name)
                while True:
                    job_id = self.apiClient.jobs.getQueuedPluginJob(plugin.name)
                    if job_id is None:
                        break
                    try:
                        plugin.plugin_object.run(job_id, self.apiClient, logger)
                    except Exception as e:
                        logging.exception("Unknown Exception Encountered Running Plugin for Job: %s", job_id)
                        self.apiClient.jobs.addJobLog(job_id, '"Unknown Exception Encountered Running Plugin for Job: {}'.format(e))
                        self.apiClient.jobs.updateJobStatus(job_id, "Error")
            if self.CYCLE_TIME > 0:
                logger.debug("Sleeping %s seconds until next cycle", self.CYCLE_TIME)
                time.sleep(self.CYCLE_TIME)
            else:
                return

    def printSettings(self):
        print "Settings:"
        print "    ARLO_API_V0_URL: " + repr(self.ARLO_API_V0_URL)
        print "    API_TOKEN: " + repr(self.API_TOKEN)
        print "    CYCLE_TIME: " + repr(self.CYCLE_TIME)


if __name__ == "__main__":
    qr = QueueRunner()
    qr.printSettings()
    qr.run()
    print "Done"
