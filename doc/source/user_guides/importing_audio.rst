####################
Importing MediaFiles
####################

Importing Media
===============

See also :ref:`arlo-upload-and-convert`.

ARLO officially supports only mono (single channel) 8 or 16 bit WAV
files. A facility is in place to attempt automatic conversion from mp3 files.

Known Problematic Formats
-------------------------

Several common WAV formats will break ARLO, usually appearing as a file with
'0' duration after import. These are mostly WAV files using an EXTENSIBLE
format, compression, or alternative encodings. Specifically, the wFormat
should be 0x0001 (WAV PCM). 

I generally use sox for forcing re-encoding of files, such as with: 

.. code-block:: bash

    i="filename.wav"; sox $i -e signed-integer -t wavpcm -2 re-converted/$i

Importing Files Individually
----------------------------

User Interface Process
^^^^^^^^^^^^^^^^^^^^^^

To access the upload feature, browse to Home -> Media Files and choose
Upload Media File from the top of the page. 

Required Settings:

- Alias
    The Alias stored for the Audio File and subsequently displayed
    in most ARLO file lists.
- File Name
    Select the file (from your local computer) to upload to ARLO.

Then click Submit at the bottom of the page. 

Low Level Process
^^^^^^^^^^^^^^^^^

When the file upload form is submitted, here are the steps that take place on the server:

- Form Submission goes to `uploadAudioFile()` (upload.py)

  - *MediaFile* Object stored to database along with default (i.e., 0)
    *MediaFileMetaData*, any StaticUserMetaData supplied with the
    upload form, and assigned to the user's ID.

  - Meandre call to MeandreAction.analyzeAudioFile - analyzeAudioFile() (ServiceHead.java)

    - File analyzed for MetaData, which is then updated in the database. 

  - *MediaFile* is added to the specified *Project*


Upload and Convert
------------------

This is a versatile import method, offering conversion with SoX prior to
import. The SoX conversion can be used to convert mp3 (and possibly
others) to WAV format, as well as change the sample rate and bit size.

See :ref:`arlo-upload-and-convert` for more details.


Mass Import
===========

Install the Script in a Python Virtualenv
-----------------------------------------

This will create and install the required modules in a Python Virtualenv.
While not stricly necessary, using the Virtualenv will help prevent conflicts
between library versions already on your system.

- Either clone the repo, or copy the script (mass_import_mediafiles.py) and
  requirements.txt file from
  https://bitbucket.org/arloproject/arlo-scripts/src/master/mass_file_upload/

- Create and install the required modules in a Python Virtualenv (using the
  virtualenv is optional but recommended to prevent conflicts between library
  versions).

  .. code-block:: bash

      virtualenv venv  # Create a new virtualenv, if it doesn't exist
      source venv/bin/activate  # Load it into the terminal session
      pip install -r requirements.txt  # Install the required modules

- Run the script

  .. code-block:: bash

      python mass_import_mediafiles.py file1.wav file2.wav

- When completed, unload the virtualenv, or logout of the terminal session.

  .. code-block:: bash

      deactivate  # Unload the virtualenv


Basic Usage
-----------

There are two main methods for using the script, interactive and scripted.
Interactive will prompt the user for the necessary information, while
scripted use requires all details ahead of time.

Interactive Use

.. code-block:: bash

    python mass_import_mediafiles.py file1.wav file2.wav

Scripted Use

.. code-block:: bash

    python mass_import_mediafiles.py -u tonyb -p <PASSWORD> --project 93 --library 69 -f file1.wav file2.wav


Uploading MP3 Files
-------------------

MP3 files can be uploaded which will use the Upload and Convert feature
(see :ref:`arlo-upload-and-convert`).
The mass_import script does not (currently) expose options for the
conversion, so only the automatic conversion settings can be used.

.. code-block:: bash

    python mass_import_mediafiles.py file1.mp3


Importing Files Directly from the Server
----------------------------------------

.. note::
    This section applies only to administrators with SSH access to the
    ARLO server.

Due to the large size of some collections, it may be easier to first copy
the files to the server using a tool better suited for large file copies
(rsync, scp, etc.) and then import them locally.

Note that this process will need additional space as both the temporary and
imported copy of the file will exist at the same time. Use caution when
importing large collections at once.

1. Copy the files to the server into new directory in the user's home directory
   using your preferred tool (WinSCP, Cyberduck, rsync).

2. Login via SSH and setup the virtualenv and script as described above.

3. Run the script as normal on the uploaded files.

4. Once complete, be sure to remove the temporary uploads in your home
   directory.


