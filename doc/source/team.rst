#############
The ARLO Team
#############

Team
====

ARLO has been developed by the collaborative efforts of teams of researchers at
the University of Illinois at Urbana Champaign and the University of Texas at
Austin.

Team members have included:

- Loretta Auvil
- Tony Borries
- Daniel Carter
- Tanya Clement
- Bill Cochran
- Oliver Croomes
- David Enstrom
- Ben Kamen
- Steve McLaughlin
- Surangi Punyasena
- David Tcheng


Publications
============

- Chris Mustazza - The noise is the content: Toward computationally determining the provenance of poetry recordings https://jacket2.org/commentary/noise-content-toward-computationally-determining-provenance-poetry-recordings


Funding
=======

Initial seed funding for ARLO came from UIUC, and subsequently from the railroad
industry to study impacts of increased rail traffic on bird singing behavior
and from UIUC to automate the classification of pollen grains.

Current funding is provided by National Endowment for the Humanities and
Institute of Museum and Library Services.
