.. contents::

###################
ADAPT Configuration
###################

The Adapt (Java) portion of ARLO uses a custom class, ArloSettings, to
manage configuration options within the project. Upon initialization,
this class loads parameters (as described below), and validates that it
has all required parameters. If required parameters are missing, the
initialization returns `false` and the program **SHOULD** terminate. 

All parameters are defined as public class members; adding a new parameter
involves adding a new public member, as well as adding code to load the
parameter and any validation in `Initialize()`.

Properties
==========

+--------------------------------------------+---------+---------------+----------------------------------------------------------+
| Name                                       | Type    | Required      | Description                                              |
+============================================+=========+===============+==========================================================+
| **General Settings**                                                                                                            |
+--------------------------------------------+---------+---------------+----------------------------------------------------------+
| hostname                                   | String  | n/a           | Hostname of the machine - (Automatically set at Startup) |
+--------------------------------------------+---------+---------------+----------------------------------------------------------+
| processDescription                         | String  | Optional      | A Description of this Process                            |
|                                            |         |               | (e.g., 'Stampede QueueRunner')                           |
+--------------------------------------------+---------+---------------+----------------------------------------------------------+
| numThreads                                 | Integer | Default '1'   | How many parallel threads to use in processing           |
+--------------------------------------------+---------+---------------+----------------------------------------------------------+
| cacheEnabled                               | Boolean | Default False | Enable the Spectra Generation cache - When enabled, if   |
|                                            |         |               | existing spectra file found, return it immediately (much |
|                                            |         |               | of the caching has been moved into Nester front-end, so  |
|                                            |         |               | this will likely be deprecated).                         |
+--------------------------------------------+---------+---------------+----------------------------------------------------------+
| advancedTagging                            | Boolean |               | **currently unused**                                     |
+--------------------------------------------+---------+---------------+----------------------------------------------------------+
| **File Paths**                                                                                                                  |
+--------------------------------------------+---------+---------------+----------------------------------------------------------+
| bigDataCacheDirectoryPath                  | String  | Required      | Directory in which to store cached data for computation  |
|                                            |         |               | (large, local storage).                                  |
+--------------------------------------------+---------+---------------+----------------------------------------------------------+
| userFilesDirectoryPath                     | String  | Required      | Points to /user-files/ Directory                         |
|                                            |         |               | (currently MediaRoot + /files/user-files/)               |
+--------------------------------------------+---------+---------------+----------------------------------------------------------+
| mediaRootDirectoryPath                     | String  | Required      | Points to MediaRoot Directory                            |
+--------------------------------------------+---------+---------------+----------------------------------------------------------+
| **Database Settings**                                                                                                           |
+--------------------------------------------+---------+---------------+----------------------------------------------------------+
| dbUserName                                 | String  | Required      | Database Username                                        |
+--------------------------------------------+---------+---------------+----------------------------------------------------------+
| dbPassword                                 | String  | Required      | Database Password                                        |
+--------------------------------------------+---------+---------------+----------------------------------------------------------+
| dbUrl                                      | String  | Required      | JDBC Connection String                                   |
|                                            |         |               | (e.g., jdbc:mysql://localhost/amanda_db)                 |
+--------------------------------------------+---------+---------------+----------------------------------------------------------+
| dbDriveClassString                         | String  |               | JDBC Driver - Default 'com.mysql.jdbc.Driver'            |
+--------------------------------------------+---------+---------------+----------------------------------------------------------+
| **HttpCallMeApiServer**                                                                                                         |
+--------------------------------------------+---------+---------------+----------------------------------------------------------+
| startHttpCallMeAPIServer                   | Boolean | Default False | Whether to start an instance of the HttpCallMeApiServer  |
+--------------------------------------------+---------+---------------+----------------------------------------------------------+
| HttpCallMeAPIServerPort                    | Integer | Required if   | Port on which the HttpCallMeApiServer listens.           |
|                                            |         | Enabled       |                                                          |
+--------------------------------------------+---------+---------------+----------------------------------------------------------+
| **QueueRunner**                                                                                                                 |
+--------------------------------------------+---------+---------------+----------------------------------------------------------+
| RunQueueDaemonInterval_Seconds             | Integer | Default '0'   | How often QueueRunner checks the Queue for Tasks         |
|                                            |         | (Disabled)    | ('0' Disables)                                           |
+--------------------------------------------+---------+---------------+----------------------------------------------------------+
| QueueRunnerOneShot                         | Boolean | Default False | If True, OneShot mode causes the program to exit after   |
|                                            |         |               | no further Queued Tasks are found. (e.g., on Stampede,   |
|                                            |         |               | kill the process and release the node)                   |
+--------------------------------------------+---------+---------------+----------------------------------------------------------+
| QueueRunnerAPIUserName                     | String  |               | System username for authenticating to the API            |
+--------------------------------------------+---------+---------------+----------------------------------------------------------+
| QueueRunnerAPIPassword                     | String  |               | System password for authenticating to the API            |
+--------------------------------------------+---------+---------------+----------------------------------------------------------+
| QueueRunnerAPIHost                         | String  |               | Arlo API Hostname                                        |
+--------------------------------------------+---------+---------------+----------------------------------------------------------+
| QueueRunnerAPIPort                         | String  |               | Arlo API Port number                                     |
+--------------------------------------------+---------+---------------+----------------------------------------------------------+
| QueueRunnerOneShotMaxTasks                 | Integer | Default -1    | If > 0, kill this instance after this number of Tasks    |
|                                            |         | (Disabled)    | has completed. Useful for stabilizing the system when    |
|                                            |         |               | long-running instances lead to memory leaks.             |
+--------------------------------------------+---------+---------------+----------------------------------------------------------+
| RunQueueEnableImportAudioFile              | Boolean | Default False | Enable ImportAudioFile Task - this should only be        |
|                                            |         |               | enabled on instances that have write access to the       |
|                                            |         |               | user-files                                               |
+--------------------------------------------+---------+---------------+----------------------------------------------------------+
| RunQueueEnableTest                         | Boolean | Default False | Enable the Test Task (enable as needed, not meant for    |
|                                            |         |               | production)                                              |
+--------------------------------------------+---------+---------------+----------------------------------------------------------+
| RunQueueEnableSupervisedTagDiscoveryParent | Boolean | Default False | Enable the SupervisedTagDiscoveryParent (heavy database  |
|                                            |         |               | access, low CPU - best on a local instance)              |
+--------------------------------------------+---------+---------------+----------------------------------------------------------+
| RunQueueEnableSupervisedTagDiscoveryChild  | Boolean | Default False | Enable the SupervisedTagDiscoveryChild (heavy compute -  |
|                                            |         |               | often on a single instance per node)                     |
+--------------------------------------------+---------+---------------+----------------------------------------------------------+
| RunQueueEnableUnsupervisedTagDiscovery     | Boolean | Default False | Enable the UnsupervisedTagDiscovery (single threaded,    |
|                                            |         |               | long run times)                                          |
+--------------------------------------------+---------+---------------+----------------------------------------------------------+



Where To Set Properties
=======================

There are multiple ways to set properties. In order of precedence:

- System Properties
- Properties File
- Defaults

System Properties
-----------------

Parameters are specified on the Java command line with -D, and will override
all other settings. Property names are prefixed with "ADAPT.", so the full
argument would be `-DADAPT.propertyName=value`

Properties File
---------------

The properties file **MUST** be specified to Adapt on the java command line
as the System Property 'ARLO_CONFIG_FILE'. An example using the property
file `ArloSettings.properties` is shown below. This resource name must be
available on the CLASSPATH.

Properties are defined as a key/value per line, separated by an `=`. This
file is parsed with `java.util.Properties`.

A sample file is provided in `adapt/ArloSettings.properties-SAMPLE`.
This shows the format, as well as all properties that need to be defined.
A number of defaults are in place. 

Defaults
--------

Defaults are hard-coded into the program; if not provided from either other
location, then the defaults will be used; however, there are no valid
defaults for some options, so a fallback to the defaults may result in
a failed initialization. 

Examples
========

An example specifying the `ARLO_CONFIG_FILE` and using the System Properties
to override the HttpCallMeAPIServerPort value (this example is from the
current production launch scripts):

.. code-block:: bash

    nohup ${JAVA_BIN} -server -Xms22G -Xmx22G -DARLO_CONFIG_FILE=ArloSettings.properties -DADAPT.HttpCallMeAPIServerPort=${ADAPT_PORT} ${GC_SETTINGS} ${APARAPI_SETTINGS} -classpath ${CLASSPATH} arlo.ServiceHead > $ARLO_JAVA_LOG 2>&1 &

The SAMPLE file:

.. code-block:: bash

    $ cat adapt/ArloSettings.properties-SAMPLE 
    # escape : and =
    # file paths do NOT end with a slash
    startHttpCallMeAPIServer=true
    HttpCallMeAPIServerPort=10001
    dbUserName=DATABASE_USER
    dbPassword=DATABASE_PASSWORD
    dbUrl=jdbc\:mysql\://DATABASE_HOST/DATABASE_NAME
    dbDriveClassString=com.mysql.jdbc.Driver
    numThreads=2
    bigDataCacheDirectoryPath=BIGDATA_CACHE_PATH
    cacheEnabled=false
    advancedTagging=true
    userFilesDirectoryPath=USERFILES_PATH
    mediaRootDirectoryPath=MEDIAROOT_PATH
    RunQueueDaemonInterval_Seconds=10
    QueueRunnerOneShot=false
    RunQueueEnableImportAudioFile=true
    RunQueueEnableTest=true
    RunQueueEnableSupervisedTagDiscoveryParent=true
    RunQueueEnableSupervisedTagDiscoveryChild=true
    RunQueueEnableUnsupervisedTagDiscovery=true

