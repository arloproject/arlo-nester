=====================
ADAPT callMe HTTP API
=====================

.. note::

    The ADAPT Java Code is in the process of being deprecated. Future
    development should not use this API. This documentation was copied
    from the Wiki for reference only.

API History
===========

The early versions of ARLO used Meandre as an interface between the
Java (ADAPT) and Python (Nester) components. Each was ran in a separate
process, and Meandre facilitated making calls between them via a network
connection (in a very similar method to this new API). 

Next, the components were pulled together into a single process, by
running the Python code in Jython; this brought some short term benefits
(which have been forgotten), but limited scalability.

This newest version switches to an HTTP interface for making the calls
from Python to Java. 

Brief Overview of the general API model
=======================================

Each of these methods works in a similar method; the Python code issues
a call to the Java code via a single function (callMe) with a dictionary
of data, expecting a simple response (currently mostly just a Boolean).
This dictionary either performs an immediate task (e.g., build a spectra
for display) or a long running background job, with an immediate return
after it is started. 

In each version of the interface, the only main change was the
communication interface between the components, though the workflow has
remained the same. In the case of this latest HTTP version, we have both
the ability to scale (unlike Jython) and easy debugging by using a well
established protocol, whereas Meandre was more proprietary.

Java "callMe" HTTP API Implementation
=====================================

I have embedded the Jetty HTTP server into the Java code to perform this.
The nice thing about using a standard protocol is that we can replace this
later, with minimal or no interruption to the rest of the code. There
are three main pieces of code for this:

- `ServiceHead::initialize()`
    Start the server to listen on a defined port
- `HttpCallMeAPIServer` class
    The HTTP server class

    -  `Initialize()`
        Start the server, and setup the handler for the /callMe/ URL

- `HttpCallMeAPIHandler` class
    The HTTP Request Handler

    - `handle()`
        Parse the POST data from the request (the data dictionary), make
        a call to `ServiceHead::callMe()`, and return the result

The call returns a JSON value with another data dictionary, as specified
by the action requested (in the POST data). 

Other notes / limitations:

- Every call is to the same URL (/callMe/) - actions are defined in the POST
- Only POST calls are currently implemented
- There is no authentication or security - this can be implemented via a
  proxy (Apache / nginx) or firewalls. Only the Python code and
  developers should be accessing this, and the Python interface has it's
  own user authentication.

Example Calls
=============

This is an example of a call using curl to the "meandreTestCall" (a generic
function for debugging), passing some test data (testParam) which is
actually ignored; note that we pass all data as POST values. This assumes
the server is running on the localhost on port 8080.

.. code-block:: bash

    curl -i 0.0.0.0:8080/callMe/ --data "testParam=testValue" --data "action=meandreTestCall"
    HTTP/1.1 200 OK
    Content-Type: application/json;charset=ISO-8859-1
    Content-Length: 144
    Server: Jetty(9.0.0.v20130308)

    {"text":"<br>Service Head Started:\n<br>&nbsp;&nbsp;taskName    = meandreTestCall\n<br>&nbsp;&nbsp;taskID      = TEST_CALL\nMeandre Test Call"}

Defined Actions
===============

analyzeAudioFile
----------------

Analyze an (audio) *MediaFile*, and store various WAV MetaData to the
database (size, bits, sample rate, etc).

This is called when importing a new *MediaFile* to parse the file and
save its metadata to the database. 

Parameters
^^^^^^^^^^

+----------------+--------------+---------------------------------+
| Parameter Name | Type / Range | Description                     |
+================+==============+=================================+
| audioFileID    | Integer      | Id of the MediaFile to analyze. |
+----------------+--------------+---------------------------------+

createSpectra
-------------

Parameters
^^^^^^^^^^

+---------------------------------+----------------+---------------------------------------------------------+
| Parameter Name                  | Type / Range   | Description                                             |
+=================================+================+=========================================================+
| numberOfTagsToDiscover          | Integer        | How many *Tags* of each *TagClass* and from each        |
|                                 |                | *MediaFile* for which to search. The number of returned |
|                                 |                | *Tags* would thus be (numberOfTagsToDiscover * Number   |
|                                 |                | of Files * Number of TagClasses.                        |
+---------------------------------+----------------+---------------------------------------------------------+
| userSettingsID                  | Integer        | Which userSettings object to use for parameters         |
+---------------------------------+----------------+---------------------------------------------------------+
| audioFilePath                   |                | relative to MEDIA_ROOT (TODO - Change to be relative to |
|                                 |                | UserFilePath - see Bug 92)                              |
+---------------------------------+----------------+---------------------------------------------------------+
| imageFilePath                   |                | relative to MEDIA_ROOT (TODO - Change to be relative to |
|                                 |                | UserFilePath - see Bug 92)                              |
+---------------------------------+----------------+---------------------------------------------------------+
| audioSegmentFilePath            |                | relative to MEDIA_ROOT (TODO - Change to be relative to |
|                                 |                | UserFilePath - see Bug 92)                              |
+---------------------------------+----------------+---------------------------------------------------------+
| startTime                       |                |                                                         |
+---------------------------------+----------------+---------------------------------------------------------+
| endTime                         |                |                                                         |
+---------------------------------+----------------+---------------------------------------------------------+
| gain                            |                |                                                         |
+---------------------------------+----------------+---------------------------------------------------------+
| numFramesPerSecond              |                |                                                         |
+---------------------------------+----------------+---------------------------------------------------------+
| numFrequencyBands               |                |                                                         |
+---------------------------------+----------------+---------------------------------------------------------+
| dampingFactor                   |                |                                                         |
+---------------------------------+----------------+---------------------------------------------------------+
| minFrequency                    |                |                                                         |
+---------------------------------+----------------+---------------------------------------------------------+
| maxFrequency                    |                |                                                         |
+---------------------------------+----------------+---------------------------------------------------------+
| simpleNormalization             |                |                                                         |
+---------------------------------+----------------+---------------------------------------------------------+
| borderWidth                     |                |                                                         |
+---------------------------------+----------------+---------------------------------------------------------+
| topBorderHeight                 |                |                                                         |
+---------------------------------+----------------+---------------------------------------------------------+
| timeScaleHeight                 |                |                                                         |
+---------------------------------+----------------+---------------------------------------------------------+
| catalogSpectraNumFrequencyBands |                |                                                         |
+---------------------------------+----------------+---------------------------------------------------------+
| spectraPitchTraceType           | One of "None", | Specifies whether to display the Pitch Trace on the     |
|                                 | "Fundamental", | Spectra, and what type to use.                          |
|                                 | or "MaxEnergy" |                                                         |
+---------------------------------+----------------+---------------------------------------------------------+

createWavSegment
----------------

importAudioFiles
----------------

REMOVED from Adapt (12-27-2015)

startADAPTAnalysis
------------------

pauseADAPTAnalysis
------------------

resumeADAPTAnalysis
-------------------

stopADAPTAnalysis
-----------------

startUnsupervisedTagDiscovery
-----------------------------

pauseUnsupervisedTagDiscovery
-----------------------------

resumeUnsupervisedTagDiscovery
------------------------------

stopUnsupervisedTagDiscovery
----------------------------

startSupervisedTagDiscovery
---------------------------

pauseSupervisedTagDiscovery
---------------------------

resumeSupervisedTagDiscovery
----------------------------

stopSupervisedTagDiscovery
--------------------------

startTranscript
---------------

pauseTranscript
---------------

resumeTranscript
----------------

stopTranscript
--------------

startTagAnalysis
----------------

pauseTagAnalysis
----------------

resumeTagAnalysis
-----------------

stopTagAnalysis
---------------

meandreTestCall
---------------

Test call in place for debugging purposes. Frequently changes...

getAudioSpectraData
-------------------

Computes and returns the raw Spectra Data of a portion of audio. 

Returns a JSON object "spectraData":

.. code-block:: python

    'spectraData': { 
        'params': {
          'mediaFileId': mediaFileId,
          'startTime': startTime,
          'endTime': endTime,
          'numTimeFramesPerSecond': numTimeFramesPerSecond,
          'numFrequencyBands': numFrequencyBands,
          'dampingRatio': dampingRatio,
          'minFrequency': minFrequency,
          'maxFrequency': maxFrequency
        },
        'freqBands': [
                       freq0,
                       freq1,
                       ...
                     ],
        'frames': {
                    time0: [
                             sample0,
                             sample1,
                             ...
                           ],
                    time1: [
                             sample0,
                             sample1,
                             ...
                           ],
                    ...
                  }
    }

Where

* each 'time' entry contains 'numFrequencyBands' samples corresponding to each ascending Frequency Band
* the number of 'time' entries = (endTime - startTime) / numTimeFramesPerSecond


Parameters
^^^^^^^^^^
+------------------------+--------------+-------------------------------------------------------------+
| Parameter Name         | Type / Range | Description                                                 |
+========================+==============+=============================================================+
| mediaFileId            | Integer      | Database ID of the MediaFile to use                         |
+------------------------+--------------+-------------------------------------------------------------+
| startTime              | Double       | Start Time, seconds, relative to the beginning of the file. |
+------------------------+--------------+-------------------------------------------------------------+
| endTime                | Double       | End Time, seconds, relative to the beginning of the file.   |
+------------------------+--------------+-------------------------------------------------------------+
| numTimeFramesPerSecond | Integer      |                                                             |
+------------------------+--------------+-------------------------------------------------------------+
| numFrequencyBands      | Integer      |                                                             |
+------------------------+--------------+-------------------------------------------------------------+
| dampingFactor          | Double       |                                                             |
+------------------------+--------------+-------------------------------------------------------------+
| minFrequency           | Double       |                                                             |
+------------------------+--------------+-------------------------------------------------------------+
| maxFrequency           | Double       |                                                             |
+------------------------+--------------+-------------------------------------------------------------+


getAudioPitchTraceData
----------------------

Computes and returns the PitchTrace Data of a portion of an Audio File. 

Returns a JSON object "spectraData":

.. code-block:: python

    'pitchTraceData': { 
        'params': {
          'mediaFileId': mediaFileId,
          'startTime': startTime,
          'endTime': endTime,
          'numTimeFramesPerSecond': numTimeFramesPerSecond,
          'numFrequencyBands': numFrequencyBands,
          'dampingRatio': dampingRatio,
          'minFrequency': minFrequency,
          'maxFrequency': maxFrequency
        },
        'freqBands': [
                       freq0,
                       freq1,
                       ...
                     ],
        'frames': {
                    time0: {
                             peakFreqBand: ,
                             energy: ,
                             ...
                           },
                    time1: {
                             peakFreqBand: ,
                             energy: ,
                             ...
                           },
                    ...
                  }
    }


Where

* each 'time' entry contains 'numFrequencyBands' samples corresponding to each ascending Frequency Band
* the number of 'time' entries = (endTime - startTime) / numTimeFramesPerSecond


Parameters
^^^^^^^^^^

+------------------------+--------------+-------------------------------------------------------------+
| Parameter Name         | Type / Range | Description                                                 |
+========================+==============+=============================================================+
| mediaFileId            | Integer      | Database ID of the MediaFile to use                         |
+------------------------+--------------+-------------------------------------------------------------+
| startTime              | Double       | Start Time, seconds, relative to the beginning of the file. |
+------------------------+--------------+-------------------------------------------------------------+
| endTime                | Double       | End Time, seconds, relative to the beginning of the file.   |
+------------------------+--------------+-------------------------------------------------------------+
| numTimeFramesPerSecond | Integer      |                                                             |
+------------------------+--------------+-------------------------------------------------------------+
| numFrequencyBands      | Integer      |                                                             |
+------------------------+--------------+-------------------------------------------------------------+
| dampingFactor          | Double       |                                                             |
+------------------------+--------------+-------------------------------------------------------------+
| minFrequency           | Double       |                                                             |
+------------------------+--------------+-------------------------------------------------------------+
| maxFrequency           | Double       |                                                             |
+------------------------+--------------+-------------------------------------------------------------+

