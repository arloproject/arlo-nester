.. contents::

##########
File Paths
##########

Getting a consistent framework in place for working with the various
File Paths has proven to be fairly difficult. I outline here the new
pattern I am implementing to help resolve this.

It should be noted that any use of absolute file paths should be
prevented as file locations may be different on various systems.

Common Paths
============

- MEDIA_ROOT / MEDIA_URL
    This is the File Path and URL for the user files (MediaFiles, logs,
    JobResultFiles, etc.).
    Files here are generally served directly by Apache.
- STATIC_ROOT / STATIC_URL
    This is the file path and URL for static content (CSS, JS, etc.).
    Note the static content is automatically collected by Django from
    sources and copied here.
    Files here are generally served directly by Apache.
- user-files
    A location, currently under MEDIA_ROOT/, for storing all user content.
    (e.g., MEDIA_ROOT/user-files/)

Database Paths
--------------

Details of paths stored in the database.

+--------------------------------------------------+---------------------------------------------------+
| Field / Parameter                                | Relative to...                                    |
+==================================================+===================================================+
| MediaFile.file                                   | Django MEDIA_ROOT (e.g.,                          |
|                                                  | 'files/user-files/mediaFiles/library-1/test.wav') |
+--------------------------------------------------+---------------------------------------------------+


Examples
--------

+---------------------------------+-----------------------------------------------------------------------+-------------------------------------------+-------------------------------------+
| Path                            | Current BigD Settings                                                 | Python                                    | Java                                |
+=================================+=======================================================================+===========================================+=====================================+
| MEDIA_ROOT                      | /data/workspace/nester/media/                                         | settings.MEDIA_ROOT                       | Shared.mediaRootDirectoryPath       |
+---------------------------------+-----------------------------------------------------------------------+-------------------------------------------+-------------------------------------+
| user-files                      | /data/workspace/nester/media/files/user-files                         |                                           |                                     |
+---------------------------------+-----------------------------------------------------------------------+-------------------------------------------+-------------------------------------+
| user's file directory           | /data/workspace/nester/media/files/user-files/username                | settings.getUsersFileDirectory()          |                                     |
+---------------------------------+-----------------------------------------------------------------------+-------------------------------------------+-------------------------------------+
| user's media directory          | /data/workspace/nester/media/files/user-files/username                | settings.getUsersMediaFileDirectory()     | Shared.getUsersMediaFileDirectory() |
+---------------------------------+-----------------------------------------------------------------------+-------------------------------------------+-------------------------------------+
| user's media cache directory    | /data/workspace/nester/media/files/user-files/username/cache          | settings.getUsersMediaCacheDirectory()    | n/a                                 |
+---------------------------------+-----------------------------------------------------------------------+-------------------------------------------+-------------------------------------+
| user's media features directory | /data/workspace/nester/media/files/user-files/username/features       | settings.getUsersMediaFeaturesDirectory() | n/a                                 |
+---------------------------------+-----------------------------------------------------------------------+-------------------------------------------+-------------------------------------+
| user's JPEG Cache directory     | /data/workspace/nester/media/files/user-files/username/cache/jpgs     | settings.getUsersJpegCacheDirectory()     | n/a                                 |
+---------------------------------+-----------------------------------------------------------------------+-------------------------------------------+-------------------------------------+
| user's Segment Cache directory  | /data/workspace/nester/media/files/user-files/username/cache/segments | settings.getUsersSegmentCacheDirectory()  | n/a                                 |
+---------------------------------+-----------------------------------------------------------------------+-------------------------------------------+-------------------------------------+

Implementation
==============

Python (Nester)
---------------

In Python, I am now basing all file paths off of MEDIA_ROOT (for the
local file system) and MEDIA_URL (for web requests); these both point
to the same location so this forms a solid basis on which to build all paths. 


Java (Adapt)
------------

For Java, it makes more sense that all file paths are relative to either
user-files/ or the user's folder.


