.. contents::

##########
Deployment
##########

This document details various items around ARLO's deployment and configuration.


settings.py
===========

The `settings.py` file is Django's typical configuration point, and we extend
that with some of our own settings for ARLO.

.. note::
    There are several restrictions around using Django's settings framework.
    Be sure to review `their documentation
    <https://docs.djangoproject.com/en/1.8/topics/settings/#creating-your-own-settings>`_
    before attempting to add in further customizations.

Template
--------

To facilitate deployments, especially around automation, we keep a
`settings.template` file in the repo, which is a Jinja-compatible template
to generate settings.py. The ARLO SaltStack states in ARLO Vagrant use
this and may be a reference of the implementation.


Settings
--------

Misc ARLO Settings
^^^^^^^^^^^^^^^^^^

+----------------------+------------------------------------------------------------------------+
| Setting              | Description                                                            |
+======================+========================================================================+
| ENABLE_SPECTRA_CACHE | Enables caching when generating spectra images for the web interface   |
+----------------------+------------------------------------------------------------------------+

Adapt
^^^^^

These settings define how to call the ADAPT HTTP API. Note that in the case of
using a load balancer, such as HAProxy in the default ARLO Vagrant
configuration, these will point to the load balancer instead.

+-------------+---------------------------------------------------------------+
| Setting     | Description                                                   |
+=============+===============================================================+
| ADAPT_HOST  | Hostname or IP Address where Java ADAPT is running.           |
+-------------+---------------------------------------------------------------+
| ADAPT_PORT  | TCP Port where ADAPT is reachable.                            |
+-------------+---------------------------------------------------------------+
| ADAPT_PATH  | URL Path where ADAPT is reachable (typically '/callMe/'       |
+-------------+---------------------------------------------------------------+

File Paths
^^^^^^^^^^

The following file paths point Django and the webserver to various storage
areas.

.. note::
    Unless otherwise noted, all file paths are assumed to be absolute
    locations and end with a trailing '/'

+-----------------------------+----------------------------------------------------------------------+
| Setting                     | Description                                                          |
+=============================+======================================================================+
| NESTER_ROOT                 | The directory where Nester is installed (e.g., '/opt/arlo/nester/')  |
+-----------------------------+----------------------------------------------------------------------+
| MEDIA_ROOT                  | The base directory for 'media' files - note media files here refers  |
|                             | not to ARLO's MediaFiles, but to some generic files namely used by   |
|                             | the web interface (CSS, etc.). With Static files and the separate    |
|                             | user-file locations now available, there may not be much use for     |
|                             | this. Historically was widely used for accessing user-files which    |
|                             | should be considered Deprecated, but we still install symlinks on    |
|                             | filesystem to maintain this use (see examples in ARLO Vagrant config.|
|                             | (Typically this points to {NESTER_ROOT}/media/                       |
+-----------------------------+----------------------------------------------------------------------+
| USER_FILE_ROOT              | The base directory for storing user-uploaded and generated content   |
|                             | (e.g., MediaFiles, JobResultFiles)                                   |
+-----------------------------+----------------------------------------------------------------------+
| STATIC_ROOT                 | The directory where Django's Static file framework will compile      |
|                             | 'static' files. These are namely files for the web-interface         |
|                             | (CSS, JS, etc.). These files are compiled from sources in various    |
|                             | projects and third-party libraries, and served by the webserver from |
|                             | this directory.                                                      |
+-----------------------------+----------------------------------------------------------------------+
| FILE_UPLOAD_TEMP_DIR        | Temporary directory where file uploads are stored - recommended to   |
|                             | use a physical file store rather than a memory-backed temp location  |
|                             | as file I/O is rarely a limit here, and we are typically memory      |
|                             | constrained elsewhere in ARLO.                                       |
+-----------------------------+----------------------------------------------------------------------+
| FILE_UPLOAD_MAX_MEMORY_SIZE | The maximum size of files uploaded via the web interface. The        |
|                             | default is often 2147483648 (2 GB) but in production installs this   |
|                             | will often be MUCH larger (>= 10 GB).                                |
+-----------------------------+----------------------------------------------------------------------+
| Template Dirs               | See :ref:`deployment_settings_templates`                             |
+-----------------------------+----------------------------------------------------------------------+


URL Paths and Webserver Config
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

These settings are used for building URL paths. These are dependent
on the webserver configuration and are highly customizable. See ARLO Vagrant
for an example implementation.

+--------------------+---------------------------------------------------------------+
| Setting            | Description                                                   |
+====================+===============================================================+
| APACHE_ROOT        | The base URL for the ARLO installation. (e.g.,                |
|                    | 'http://live.arloproject.com:81/')                            |
+--------------------+---------------------------------------------------------------+
| URL_PREFIX         | Path appended to APACHE_ROOT to get to the ARLO location      |
|                    | (typically blank, '')                                         |
+--------------------+---------------------------------------------------------------+
| LOGIN_URL          | Where to redirect users to force a login (typically set to    |
|                    | '{URL_PREFIX}/accounts/login/' )                              |
+--------------------+---------------------------------------------------------------+
| MEDIA_URL          | URL which corresponds to the MEDIA_ROOT location (often a     |
|                    | subdomain or accessed via a webserver alias)                  |
+--------------------+---------------------------------------------------------------+
| STATIC_URL         | URL which corresponds to the STATIC_ROOT location (often a    |
|                    | subdomain or accessed via a webserver alias)                  |
+--------------------+---------------------------------------------------------------+
| ADMIN_MEDIA_PREFIX | (This may be unused.) URL prefix for 'admin' site media (CSS, |
|                    | JS and images). Make sure to use a trailing slash. Examples:  |
|                    | 'http://foo.com/media/', '/media/'.                           |
+--------------------+---------------------------------------------------------------+
| ARLO_API_V0_URL    | See :ref:`deployment_settings_drf`                            |
+--------------------+---------------------------------------------------------------+


Allowed Hosts
^^^^^^^^^^^^^

+---------------+------------------------------------------------------------------------------+
| Setting       | Description                                                                  |
+===============+==============================================================================+
| ALLOWED_HOSTS | A list of strings representing the host/domain names that this Django site   |
|               | can serve. Typically this would include any domains / subdomains used to     |
|               | access the site. This is a security measure to prevent CSRF attacks.         |
|               |                                                                              |
|               | It is recommended to review `Django's AllowedHosts Documentation             |
|               | <https://docs.djangoproject.com/en/1.8/ref/settings/#allowed-hosts>`_        |
|               | before configuring this value.                                               |
|               |                                                                              |
|               | To disable this protection and allow ALL hosts, use the following::          |
|               |                                                                              |
|               |     ALLOWED_HOSTS = ['*']                                                    |
+---------------+------------------------------------------------------------------------------+

Database
^^^^^^^^

See `Django's Database Configuration Documentation
<https://docs.djangoproject.com/en/1.8/ref/settings/#databases>`_ for further
details.

The Database configuration is stored as a dictionary, as in this example::

    DATABASES = {
      'default': {
        'ENGINE': 'django.db.backends.mysql',
        'NAME': 'arlodb',
        'USER': 'username',
        'PASSWORD': 'password',
        'HOST': 'hostname',
        'PORT': '',
      }
    }

The DATABASES dictionary contains one key, 'default', which designates that
only one database is used for storing all data.

+-----------+-------------------------------------------------------------------------+
| Setting   | Description                                                             |
+===========+=========================================================================+
| DATABASES | Dictionary which stores all of the database configurations              |
+-----------+-------------------------------------------------------------------------+
| ENGINE    | Class used to access the database - ARLO is developed specifically for  |
|           | MySQL, so this should always be 'django.db.backends.mysql'              |
+-----------+-------------------------------------------------------------------------+
| NAME      | Name of the database                                                    |
+-----------+-------------------------------------------------------------------------+
| USER      | Username used to authenticate the the database engine.                  |
+-----------+-------------------------------------------------------------------------+
| PASSWORD  | Password used to authenticate to the database engine                    |
+-----------+-------------------------------------------------------------------------+
| HOST      | Hostname or IP address to connect to the database engine                |
+-----------+-------------------------------------------------------------------------+
| PORT      | Port number used to connect to the database. Set to blank ('') to use   |
|           | the default (3306)                                                      |
+-----------+-------------------------------------------------------------------------+


Debug
^^^^^

Debug mode enables debug level logging and will provide a 'debug' page in the
web interface when encountering an error processing a request. This debug page
includes tracebacks and dumps variable contents, which is a valuable aid for
debugging.

.. note::

    DEBUG mode should NOT be used on production systems, as there are security
    and performance implications. Namely, debug pages, rendered on hitting an
    error, dump internal settings which may include sensitive information.

+-----------+------------------------------------------------+
| Setting   | Description                                    |
+===========+================================================+
| DEBUG     | Whether to enable Debug settings within Django |
+-----------+------------------------------------------------+


.. _deployment_settings_templates:

Template Settings
^^^^^^^^^^^^^^^^^

These settings instruct Django on where to load templates for rendering
web pages, both our own templates and those provided by third-party libraries.

The only setting here most users should need to consider is `DIRS` which
must point to the templates/ directory in the Nester installation. These
templates may eventually be migrated into app-specific directories and may
not be used in the future.

Example config::

    TEMPLATES = [
      {
        'BACKEND': 'django.template.backends.django.DjangoTemplates',
        'DIRS': [
          '{{ NESTER_ROOT }}/templates',
        ],
        'APP_DIRS': True,
        'OPTIONS': {
          'context_processors': [
            # These are the defaults
            'django.contrib.auth.context_processors.auth',
            'django.template.context_processors.debug',
            'django.template.context_processors.i18n',
            'django.template.context_processors.media',
            'django.template.context_processors.static',
            'django.template.context_processors.tz',
            'django.contrib.messages.context_processors.messages',
            # custom
            'tools.arloDjangoMiddleWare.ArloTemplateContextProcessor',
          ],
          'debug': DEBUG,
        },
      },
    ]


.. _deployment_settings_drf:

Django Rest Framework Settings
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

These are settings specific to the Django Rest Framework interface. See `DRF's
Documentation <http://www.django-rest-framework.org/api-guide/settings/>`_
for more information.

Example configuration::

    REST_FRAMEWORK = {
        'DEFAULT_AUTHENTICATION_CLASSES': (
            'rest_framework.authentication.SessionAuthentication',
            'rest_framework.authentication.BasicAuthentication',
            'rest_framework.authentication.TokenAuthentication',
        ),
        'DEFAULT_METADATA_CLASS': 'api.v0.ChoicelessMetadata.ChoicelessMetadata',
        'DEFAULT_PERMISSION_CLASSES': ('api.v0.IsAuthenticatedOrOptionsOnly.IsAuthenticatedOrOptionsOnly',),
        'PAGE_SIZE': 10
    }

    ARLO_API_V0_URL = 'http://vagrantdev.arloproject.com:9000/api/v0/'

+--------------------------------+------------------------------------------------------------------------+
| Setting                        | Description                                                            |
+================================+========================================================================+
| REST_FRAMEWORK                 | Dictionary which stores most of the DRF configurations                 |
+--------------------------------+------------------------------------------------------------------------+
| DEFAULT_AUTHENTICATION_CLASSES | DRF Classes that will be used for request authentication               |
+--------------------------------+------------------------------------------------------------------------+
| DEFAULT_METADATA_CLASS         | DRF Class used for generating schema for OPTIONS requests.             |
+--------------------------------+------------------------------------------------------------------------+
| DEFAULT_PERMISSION_CLASSES     | Classes used for request authorizations                                |
+--------------------------------+------------------------------------------------------------------------+
| PAGE_SIZE                      | Default page size for pagination of responses.                         |
+--------------------------------+------------------------------------------------------------------------+
| ARLO_API_V0_URL                | Absolute URL where the API v0 (DRF backed API) is available from ARLO. |
+--------------------------------+------------------------------------------------------------------------+


Misc Django Settings
^^^^^^^^^^^^^^^^^^^^

These settings are Django specific and will not be detailed here. See
`Django's Documentation <https://docs.djangoproject.com/en/1.8/ref/settings/>`_
for further details.

* USE_STATIC_SERVE
* ADMINS
* MANAGERS
* TIME_ZONE
* LANGUAGE_CODE
* SITE_ID
* USE_I18N
* SECRET_KEY
* MIDDLEWARE_CLASSES
* ROOT_URLCONF
* TEST_RUNNER
* INSTALLED_APPS
