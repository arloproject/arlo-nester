=============
ARLO Glossary
=============

A quick reference of terms used throughout ARLO.


- Tag
    A portion of an audio *MediaFile* marked with start and end times
    (and optionally minimum and maximum frequency).

- TagClass
    Each Tag is assigned to exactly one *TagClass*, a short name describing
    the *Tag*. For example, there might be a *TagClass* for utterances of
    each of the vowels; A, E, I, O, and U. *TagClasses* are shared across
    a *Project*, and *Tags* across multiple *TagSets* may have the same
    *TagClass*.

- TagSet
    Each *Tag* belongs to exactly one *TagSet*. A *TagSet* is meant to be
    a container of related *Tags*. There are many reasons to use *TagSets*.

    - There may be a *TagSet* 'vowels' that contains *Tags* with the
      *TagClasses* A, E, I, O, and U.
    - There may be a *TagSet* for each user of a *Project*, containing
      the *Tags* that they have manually tagged.
    - Results from an automated search may be stored to a separate
      *TagSet* from the source *Tags*.

