.. contents::

#######
Plugins
#######

Overview
========

A plugin framework is being added to ARLO to support future extensibility
through a layer of abstraction.

The ARLO Plugin framework is based on the Yapsy
(`Yet Another Plugin SYstem <http://yapsy.sourceforge.net/>`_)
Python library.

Currently, we define only plugins for adding new Job types through the
QueueRunner system.


Framework Components
====================


ArloPluginManager
-----------------

The ArloPluginManager handles loading plugins from the filesystem, and
provides convenience functions for accessing the installed plugins.


ArloPluginTemplateLoader
------------------------

A custom Django Template Loader that supports loading templates
from Plugins.

ArloPluginInfo
--------------

ARLO's custom PluginInfo class, adds additional details to the base Yapsy
PluginInfo.


Plugin Types
============

ArloJobPlugin
-------------

Job Plugins extend **ArloJobPluginBase** to add new Job Types to ARLO. Plugin
Jobs in the database use a JobType of "JobPlugin" (15), and add a
JobParameter "PluginName" set to the name of the corresponding Job plugin.

The **TestJobPlugin** serves as a template and minimal example of this plugin
type.


Plugin Methods
^^^^^^^^^^^^^^

An ArloJobPlugin consists of a class with the following methods:


  - getSettingsForm()
    Used by the web interface, returns a Django Form and Template to render
    the configuration page.

  - parseSettingsForm()
    Used by the web interface, parses a form from the user to generate the
    settings for the Job

  - run()
    Called by the QueueRunner daemon, actually runs the job. Note that the
    QueueRunner daemon doesn't have database access through Django, so
    all data access must be done through the API.
