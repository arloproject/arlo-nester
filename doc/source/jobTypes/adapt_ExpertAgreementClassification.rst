###############################
Expert Agreement Classification
###############################

+-----------------------------------------+------------------------------------+
| QueueTask Name                          | Database Id / Name                 |
+=========================================+====================================+
| QueueTask_ExpertAgreementClassification | 12 - ExpertAgreementClassification |
+-----------------------------------------+------------------------------------+


This process takes an existing set of Tags and classifies MediaFiles by
looking for Tag examples that best match the source tags.

.. note::

    This will eventually be split up to use separate child-tasks, similar to
    Supervised Tag Discovery. However, while under development this is being
    done under a single task. 

.. note::

    Also note that this function call is overloaded with two separate
    processes, the actual "Classify" process and an Optimization process,
    which only report optimal search settings.


Job Parameters
==============

+------------------------------------+------------------+-------------------------------------------------------------------+
| Parameter Name                     | Type / Range     | Description                                                       |
+====================================+==================+===================================================================+
| **Common Parameters**                                                                                                     |
+------------------------------------+------------------+-------------------------------------------------------------------+
| randomWindowJobId                  | Integer          | Database ID of the Job associated with a set of Random Windows    |
+------------------------------------+------------------+-------------------------------------------------------------------+
| sourceTagSets                      | List of Integers | Comma-separated list of database IDs                              |
+------------------------------------+------------------+-------------------------------------------------------------------+
| distanceWeightingPower             | Float            |                                                                   |
+------------------------------------+------------------+-------------------------------------------------------------------+
| classificationProbabilityThreshold | Float            |                                                                   |
+------------------------------------+------------------+-------------------------------------------------------------------+
| method                             | String           | Either "classify" or "optimize"                                   |
+------------------------------------+------------------+-------------------------------------------------------------------+
| **Classify Settings**                                                                                                     |
| Settings applicable only to the Classification process.                                                                   |
+------------------------------------+------------------+-------------------------------------------------------------------+
| *Spectra Details*                                                                                                         |
+------------------------------------+------------------+-------------------------------------------------------------------+
| numFrequencyBands                  | Integer          |                                                                   |
+------------------------------------+------------------+-------------------------------------------------------------------+
| numTimeFramesPerSecond             | Float            |                                                                   |
+------------------------------------+------------------+-------------------------------------------------------------------+
| dampingRatio                       | Float            |                                                                   |
+------------------------------------+------------------+-------------------------------------------------------------------+
| minFrequency                       | Float            |                                                                   |
+------------------------------------+------------------+-------------------------------------------------------------------+
| maxFrequency                       | Float            |                                                                   |
+------------------------------------+------------------+-------------------------------------------------------------------+
| **Optimize Settings**                                                                                                     |
| Settings applicable only to the Optimization process.                                                                     |
+------------------------------------+------------------+-------------------------------------------------------------------+
| *Spectra Details*                                                                                                         |
+------------------------------------+------------------+-------------------------------------------------------------------+
| numFrequencyBandsLower             | Integer          | Lower Bound of the numFrequencyBands                              |
+------------------------------------+------------------+-------------------------------------------------------------------+
| numFrequencyBandsUpper             | Integer          | Upper Bound of the numFrequencyBands                              |
+------------------------------------+------------------+-------------------------------------------------------------------+
| numTimeFramesPerSecondLower        | Float            | Lower Bound of the numTimeFramesPerSecond parameter               |
+------------------------------------+------------------+-------------------------------------------------------------------+
| numTimeFramesPerSecondUpper        | Float            | Upper Bound of the numTimeFramesPerSecond parameter               |
+------------------------------------+------------------+-------------------------------------------------------------------+
| dampingRatioLower                  | Float            | Lower Bound of the dampingRatio parameter                         |
+------------------------------------+------------------+-------------------------------------------------------------------+
| dampingRatioUpper                  | Float            | Upper Bound of the dampingRatio parameter                         |
+------------------------------------+------------------+-------------------------------------------------------------------+
| minFrequencyLower                  | Float            | Lower Bound of the minFrequency parameter                         |
+------------------------------------+------------------+-------------------------------------------------------------------+
| minFrequencyUpper                  | Float            | Upper Bound of the minFrequency parameter                         |
+------------------------------------+------------------+-------------------------------------------------------------------+
| maxFrequencyLower                  | Float            | Lower Bound of the maxFrequency parameter                         |
+------------------------------------+------------------+-------------------------------------------------------------------+
| maxFrequencyUpper                  | Float            | Upper Bound of the maxFrequency parameter                         |
+------------------------------------+------------------+-------------------------------------------------------------------+
| **currently UNUNSED**                                                                                                     |
+------------------------------------+------------------+-------------------------------------------------------------------+
| spectraWeight                      | Float            | When computing correlations, how much to weight (multiply) the    |
|                                    |                  | spectra computation.                                              |
+------------------------------------+------------------+-------------------------------------------------------------------+
| pitchWeight                        | Float            | When computing correlations, how much to weight (multiply) the    |
|                                    |                  | spectra computation.                                              |
+------------------------------------+------------------+-------------------------------------------------------------------+
| averageEnergyWeight                | Float            | (aka, 'volumeWeight') When computing correlations, how much to    |
|                                    |                  | weight (multiply) the spectra computation.                        |
+------------------------------------+------------------+-------------------------------------------------------------------+
| destinationTagSet                  | Integer          | database ID of the TagSet                                         |
+------------------------------------+------------------+-------------------------------------------------------------------+
| **Advanced / Optional**                                                                                                   |
+------------------------------------+------------------+-------------------------------------------------------------------+
| searchWithinTagClasses             | List of Integers | (See above) Comma-separated list of database IDs of the           |
|                                    |                  | TagClasses in which to restrict the search.                       |
+------------------------------------+------------------+-------------------------------------------------------------------+


Priority
========

This will set the Job priority to '0', the default priority.
