#############
TestJobPlugin
#############

**Plugin Name**: TestJobPlugin

This plugin is included as a minimal example of a Job Plugin implementation.
It's only actions when ran are to add a Job Log message and upload a
*JobResultFile*, 'test.txt' which contains the text "Test".

Job Settings
============

None required, accepts parameters which will be logged in a Job Log.

Priority
========

This will set the Job priority to '0', the default priority.
