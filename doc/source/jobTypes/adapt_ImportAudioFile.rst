.. _arlo-upload-and-convert: 

###############
ImportAudioFile
###############

+-----------------------------+-----------------------+
| QueueTask Name              | Database Id / Name    |
+=============================+=======================+
| QueueTask_ImportAudioFile   | 9 - ImportAudioFile   |
+-----------------------------+-----------------------+

This is a new import method (March 2013) using the QueueRunner system to
import Audio Files into ARLO. Each QueueTask entry will import a single file.
These Tasks are added through the Import MediaFile UI, which will upload
the source file and optionally run a conversion with SoX prior to import.

The SoX conversion can be used to convert mp3 (and possibly others) to wav
format, as well as change the sample rate and bit size.

.. note::

    The system running the SoX conversion must have the necessary libraries
    and support compiled into SoX for the source formats used. (e.g. the mp3
    decoder libraries).

.. note::

    As part of the migration to the MediaFile 'file' field, we need to
    manually pass the **mediaRelativeFilePath** field value to ADAPT.

Launching a Job
===============

From the **Audio Files** page, select the **Upload and Convert** link.


Job Parameters
==============

+-----------------------------+--------------+-------------------------------------------------------------------+
| Parameter Name              | Type / Range | Description                                                       |
+=============================+==============+===================================================================+
| projectId **(required)**    | Integer      | Add the media file to this Project once imported.                 |
+-----------------------------+--------------+-------------------------------------------------------------------+
| libraryId **(required)**    | Integer      | Add the media file to this Library once imported.                 |
+-----------------------------+--------------+-------------------------------------------------------------------+
| mediaRelativeFilePath       | String       | Used for the 'file' field, path relative to the Django MEDIA_ROOT |
| **(required)**              |              | (e.g., 'files/user-files/mediaFiles/library-1/test.wav')          |
+-----------------------------+--------------+-------------------------------------------------------------------+
| sourceRelativeFilePath      | String       | If provided, this source file is converted to a new file and      |
| (optional)                  |              | saved to **mediaRelativeFilePath**.                               |
+-----------------------------+--------------+-------------------------------------------------------------------+
| alias (optional)            | String       | Set as the mediaFile.alias if provided (otherwise the filename is |
|                             |              | used by default).                                                 |
+-----------------------------+--------------+-------------------------------------------------------------------+
| outputBits (optional)       | Integer      | If provided along with sourceRelativeFilePath, this forces the    |
|                             |              | conversion to the specified bit size.                             |
+-----------------------------+--------------+-------------------------------------------------------------------+
| outputSampleRate (optional) | Integer      | If provided along with sourceRelativeFilePath, this forces the    |
|                             |              | conversion to the specified bit rate.                             |
+-----------------------------+--------------+-------------------------------------------------------------------+


Priority
========

This will set the Job priority to '0', the default priority.
