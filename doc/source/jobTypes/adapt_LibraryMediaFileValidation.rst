##########################
LibraryMediaFileValidation
##########################

+-----------------------------------------+-----------------------------------+
| QueueTask Name                          | Database Id / Name                |
+=========================================+===================================+
| QueueTask_LibraryMediaFileValidation    | 14 - LibraryMediaFileValidation   |
+-----------------------------------------+-----------------------------------+

.. note::

    This Job is currently limited to Admin users.

This function audits all of the MediaFiles in a Library, ensuring that all
files are present and in a state that ARLO expects. This will generate a
JobResultFile CSV with details of all of the MediaFiles.

.. note::

    Since Jobs are specific to a Project, we will have the user select an
    arbitrary project during launch in which to place the Job.


Launching a Job
===============

From the User Home page, select 'Libraries', then select the "Launch Library
Validation Task" from the Admin Functions.


Job Parameters
==============

+---------------------------+--------------+-------------------------------------------------------------------+
| Parameter Name            | Type / Range | Description                                                       |
+===========================+==============+===================================================================+
| libraryId                 | Integer      | Database ID of the Library to validate. If this parameter is      |
|                           |              | absent, validate ALL MediaFiles in ARLO.                          |
+---------------------------+--------------+-------------------------------------------------------------------+
| updateNullChecksum        | ANY          | If this parameter is present (with any value), compute and save   |
|                           |              | the md5 Checksum if the existing md5 is null                      |
+---------------------------+--------------+-------------------------------------------------------------------+
| skipMd5                   | ANY          | If this parameter is present (with any value), skip all MD5       |
|                           |              | related checks.                                                   |
+---------------------------+--------------+-------------------------------------------------------------------+
| updateMetadata            | ANY          | If this parameter is present with any value, update missing /     |
|                           |              | incorrect file MetaData stored in the database.                   |
+---------------------------+--------------+-------------------------------------------------------------------+


Priority
========

This will set the Job priority to '0', the default priority.
