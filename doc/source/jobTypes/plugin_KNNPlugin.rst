#########################
KNN Classifier Job Plugin
#########################

**Plugin Name**: KNNClassifierJobPlugin

This plugin runs a K-Nearest Neighbors Classification using the SciKit Learn
library.

More information on this Classifier is available at
http://scikit-learn.org/stable/modules/generated/sklearn.neighbors.KNeighborsClassifier.html

.. note::
    Tags in the Example and Source TagSets SHOULD be the EXACT
    same length. Currently, features will be padded to the length of the
    longest Tag.


Launching a Job
===============

Jobs are launched via the Plugin Job interface, by selecting "Launch Plugin
Jobs" on the Project Files page.

Job Settings
============

+------------------------+---------------------------------------------------------------+
| Setting Name           | Description                                                   |
+========================+===============================================================+
| name                   | Not used internally. This is meant as a book-keeping item for |
|                        | later reference by the user.                                  |
+------------------------+---------------------------------------------------------------+
| n_neighbors            | Number of neighbors to use                                    |
+------------------------+---------------------------------------------------------------+
| weights                | weight function used in prediction.                           |
|                        |                                                               |
|                        | - uniform                                                     |
|                        |     uniform weights. All points in each neighborhood are      |
|                        |     weighted equally.                                         |
|                        | - distance                                                    |
|                        |     weight points by the inverse of their distance. Closer    |
|                        |     neighbors of a query point will have a greater influence  |
|                        |     than neighbors which are further away.                    |
+------------------------+---------------------------------------------------------------+
| algorithm              | Algorithm used to compute the nearest neighbors.              |
|                        |                                                               |
|                        | - Auto                                                        |
|                        |     Attempt to decide the most appropriate algorithm based on |
|                        |     the data                                                  |
|                        | - Ball Tree                                                   |
|                        | - KD Tree                                                     |
|                        | - Brute Force                                                 |
+------------------------+---------------------------------------------------------------+
| exampleTagSet          | The TagSet from which the Class Examples are loaded.          |
+------------------------+---------------------------------------------------------------+
| sourceTagSet           | Tags which will be classified.                                |
+------------------------+---------------------------------------------------------------+
| destinationTagSet      | TagSet into which new Tags are created - these are copied     |
|                        | from the sourceTagSet, with a new tagClass corresponding to   |
|                        | the exampleTagSet examples.                                   |
+------------------------+---------------------------------------------------------------+
| **Spectra Details**                                                                    |
+------------------------+---------------------------------------------------------------+
| numFrequencyBands      | The number of bands (sections) in the spectra creation        |
|                        | between minFrequency and maxFrequency.                        |
+------------------------+---------------------------------------------------------------+
| numTimeFramesPerSecond | The number of times per second in an audio file that a        |
|                        | line of spectra is computed.                                  |
+------------------------+---------------------------------------------------------------+
| dampingRatio           |                                                               |
+------------------------+---------------------------------------------------------------+
| minFrequency           | The minimum frequency used for the spectra creation.          |
+------------------------+---------------------------------------------------------------+
| maxFrequency           | The maximum frequency used for the spectra creation.          |
+------------------------+---------------------------------------------------------------+




Priority
========

This will set the Job priority to '0', the default priority.

