###############################
RandomWindowAndSegmentingPlugin
###############################

**Plugin Name**: RandomWindowAndSegmentingPlugin

.. note::
    This Plugin replaces the original *RandomWindowTagging* JobType (id 2).

This plugin has multiple modes of operation, as selected in the settings when
launching a job:

+--------------+---------------------------------------------------------------+
| Mode         | Description                                                   |
+==============+===============================================================+
| RandomWindow | Generate a number of RandomWindows across files in a Project. |
|              | These can then be navigated via the RandomTagging interface   |
|              | for manual discovery / classification of these windows.       |
+--------------+---------------------------------------------------------------+
| RandomTag    | Generate Tags randomly across a collection of files in a      |
|              | Project.                                                      |
+--------------+---------------------------------------------------------------+
| Segmenting   | Generate Tags across all files in a periodic (not random)     |
|              | manner. Creates a Tag of *sizeInSeconds* length every         |
|              | *stepInSeconds* from the beginning of each file.              |
+--------------+---------------------------------------------------------------+

Launching a Job
===============

Jobs are launched via the Plugin Job interface, by selecting "Launch Plugin
Jobs" on the Project Files page.

Job Settings
============

+---------------+---------------------------------------------------------------+
| Setting Name  | Description                                                   |
+===============+===============================================================+
| name          | Not used internally. This is meant as a book-keeping item for |
|               | later reference by the user.                                  |
+---------------+---------------------------------------------------------------+
| mode          | Determines the Mode this Job will run with:                   |
|               |                                                               |
|               | - 'RandomWindow'                                              |
|               |    Generate Random Windows for use in the Random Tagging      |
|               |    interface.                                                 |
|               | - 'RandomTag'                                                 |
|               |    Generate Random Tags, stored in the indicated 'tagSet'     |
|               | - 'Segment'                                                   |
|               |    Segment all MediaFiles in the Project, creating Tags using |
|               |    the specified *sizeInSeconds* and *stepInSeconds*          |
+---------------+---------------------------------------------------------------+
| sizeInSeconds | The size of the generated Windows / Tags                      |
+---------------+---------------------------------------------------------------+
| **RandomWindow Settings**                                                     |
+---------------+---------------------------------------------------------------+
| count         | The number of Windows to generate                             |
+---------------+---------------------------------------------------------------+
| weight        | Whether to weight the random selection based on 'Time' or     |
|               | 'File'.                                                       |
+---------------+---------------------------------------------------------------+
| maxOverlap    | [0,1] How much new Tags are allowed to overlap (in Time) with |
|               | other generated Tags, as a fraction of the length of the tag  |
|               | (i.e., 0 = no overlap, 1 = complete overlap).                 |
+---------------+---------------------------------------------------------------+
| **RandomTag Settings**                                                        |
+---------------+---------------------------------------------------------------+
| count         | The number of Windows to generate                             |
+---------------+---------------------------------------------------------------+
| weight        | Whether to weight the random selection based on 'Time' or     |
|               | 'File'.                                                       |
+---------------+---------------------------------------------------------------+
| maxOverlap    | [0,1] How much new Tags are allowed to overlap (in Time) with |
|               | other generated Tags, as a fraction of the length of the tag  |
|               | (i.e., 0 = no overlap, 1 = complete overlap).                 |
+---------------+---------------------------------------------------------------+
| tagSet        | The TagSet ID into which the new Random Tags will be stored.  |
+---------------+---------------------------------------------------------------+
| tagClass      | The TagClass assigned to new Random Tags.                     |
+---------------+---------------------------------------------------------------+
| **Segmenting Settings**                                                       |
+---------------+---------------------------------------------------------------+
| stepInSeconds | The step size to use when generating Tags.                    |
+---------------+---------------------------------------------------------------+
| tagSet        | The TagSet ID into which the new Random Tags will be stored.  |
+---------------+---------------------------------------------------------------+
| tagClass      | The TagClass assigned to new Random Tags.                     |
+---------------+---------------------------------------------------------------+


Priority
========

This will set the Job priority to '0', the default priority.


RandomWindow Tagging
====================

The existing RandomWindow Tagging interface is retained, available under the
Project Files page.
