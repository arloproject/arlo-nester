##############
ADAPT Test Job
##############


+-----------------+--------------------+
| QueueTask Name  | Database Id / Name |
+=================+====================+
| QueueTask_Test  | 8 - Test           |
+-----------------+--------------------+

This Task is for debug / test purposes. This logs the parameters configured
for the Task and completes.

Job Settings
============

None required, accepts parameters which will be logged in a Job Log.

Priority
========

This will set the Job priority to '0', the default priority.
