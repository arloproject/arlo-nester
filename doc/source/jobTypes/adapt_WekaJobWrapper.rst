##############
WekaJobWrapper
##############

+-----------------------------------------+-----------------------------------+
| QueueTask Name                          | Database Id / Name                |
+=========================================+===================================+
| QueueTask_WekaJobWrapper                | 13 - WekaJobWrapper               |
+-----------------------------------------+-----------------------------------+


This Job integrates the Weka SVM classification on a set of Tags.


Job Parameters
==============

+---------------------------+--------------+-------------------------------------------------------------------+
| Parameter Name            | Type / Range | Description                                                       |
+===========================+==============+===================================================================+
| wekaFunction              | String       | Name of the Weka function to run (currently only 'svm' supported).|
+---------------------------+--------------+-------------------------------------------------------------------+
| sourceTagSet              | Integer      | Database ID of the source TagSet.                                 |
+---------------------------+--------------+-------------------------------------------------------------------+
| destinationTagSet         | Integer      | Database ID of the destination TagSet into which to save Tags.    |
+---------------------------+--------------+-------------------------------------------------------------------+
| **SVM Parameters**                                                                                           |
+---------------------------+--------------+-------------------------------------------------------------------+
| numFramesPerExample       | Integer      | The number of frames that are used to create an example for the   |
|                           |              | machine learning. For instance, using 1 frame would have no       |
|                           |              | context. Also this needs to be set corresponding to the           |
|                           |              | numTimeFramesPerSecond. For instance, if you want an example to   |
|                           |              | represent one second, then this has to correspond with the        |
|                           |              | setting for the numTimeFramesPerSecond.                           |
+---------------------------+--------------+-------------------------------------------------------------------+
| complexityConstant        | Float        |                                                                   |
+---------------------------+--------------+-------------------------------------------------------------------+
| **Spectra Details**                                                                                          |
+---------------------------+--------------+-------------------------------------------------------------------+
| numFrequencyBands         | Integer      | The number of bands (sections) in the spectra creation between    |
|                           |              | minFrequency and maxFrequency.                                    |
+---------------------------+--------------+-------------------------------------------------------------------+
| numTimeFramesPerSecond    | Integer      | The number of times per second in an audio file that a line of    |
|                           |              | spectra is computed.                                              |
+---------------------------+--------------+-------------------------------------------------------------------+
| dampingRatio              | Float        |                                                                   |
+---------------------------+--------------+-------------------------------------------------------------------+
| minFrequency              | Float        | The minimum frequency used for the spectra creation.              |
+---------------------------+--------------+-------------------------------------------------------------------+
| maxFrequency              | Float        | The maximum frequency used for the spectra creation.              |
+---------------------------+--------------+-------------------------------------------------------------------+


Priority
========

This will set the Job priority to '0', the default priority.
