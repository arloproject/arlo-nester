===========
MetaTagging
===========


.. note::

    For User Documentation also checkout the PennSound specific
    documentation:
    https://sites.google.com/site/nehhipstas/tests/pennsound-poetry-tagging-january-2014

MetaTagging is "Tagging Tags". Whereas a *Tag* assigns a category
(*TagClass*) to an event in time, MetaTags assign additional
information to these *Tags*.

The first version of this was the PoetryTagging App for the PennSound
library. However, this used a fixed format of data fields and lacked
any provision for later modifications or extensibility. This new
MetaTagging framework allows customization of the interface and data fields.

.. image:: images/Poetry_tagging_interface.png
    :scale: 50%

Field Types
===========

- Radio Buttons

  - Select a single choice from a set of options
  - May or may not be required

- Checkbox

  - A True/False choice
  - Never "required"

- Exclusive Button

  - An 'exclusive' option exists too, which overrides all other fields in
    the form. For example, in the original PoetryTagging interface, Silence
    or Chatter (among others) can be selected, ignoring all data in the
    other fields.


Data Storage
============

All configuration and user data is stored in the database:

TagMetaForm
-----------

This specifies a "Form" for a tagging interface.

- user
    User who 'owns' the Form - not really used for anything
- name
    Name of the form
- templateData
    Raw HTML template to use when generating the MetaTagging page

TagMetaField
------------

This specifies the fields that are in a 'form'

- form
    To what 'form' does this field belong
- required
    Is this field required
- name
    internal name (used in the template, so no spaces)
- label
    What is displayed on the form.
- widget
    Which widget to use
- initial
    An initial value is selected (points to an Option below)

Widget Types
^^^^^^^^^^^^

- RADIO(1)
    Radio options
- SELECT(2)
    NOT IMPLEMENTED
- CHECKBOX(3)
    A True/False checkbox
- EXCLUSIVE_SUBMIT(4)
    See Above

Notes:

- Not all data applicable to all widgets. 
- CHECKBOX MUST Have Two Options, "True" and "False"
- EXCLUSIVE must be manually built into the HTML Template
- EXCLUSIVE must have exactly one TagMetaFieldOption, named anything


TagMetaFieldOption
------------------

Enumerates each possible option for a field:

- field
    Points to the TagMetaField
- label
    Name of the option

TagMeta
-------

These entries are the actual user MetaTags.

- user
    Who tagged this
- dateTagged
    When it was tagged
- tagExample
    What did we tag?
- option
    Points to a TagMetaFieldOption

