.. _arlo-sensors:

#######
Sensors
#######


The Sensors model provides a way to group MediaFiles by which microphone
(Sensor) they were collected with, and store some basic information about
the sensor, namely location. In addition, this supports the 'SensorArray
Layout' display in the visualizeMultiProjectFile page.

Sensors and SensorArrays are contained within Libraries.


Interface
=========

Adding / Editing Sensors and Arrays
-----------------------------------

Sensors are accessed from within the Library page. From the User Home Page,
click Libraries and select a Library. From there, either View an existing
Library or add a new one.

The top of the screen displays the Sensors in the array, and the bottom
allows adding new Sensors. The main feature is the layout grid in the middle.

On the visualizeMultiProjectFile, the SensorArray Layout can be selected,
which displays files based on location. The layout grid here is used to
define these locations.

First, select the number of rows and columns to
use in the grid, and click 'Update Grid'. Drag and drop the various
sensors into the layout grid; once done, click the 'Save to Database'
button to save the layout. 

Models
------

Sensors
^^^^^^^

A Sensor is comprised of:

id
    Unique integer id (internally generated)

name
    Text name assigned to the Sensor by the user.

    Common naming schemes will take into account the location of the
    Sensor. For example, in a diamond grid, perhaps "North", "South",
    "East", and "West".

SensorArray
    Which SensorArray is this Sensor part of.

Coordinate
    An **x**, **y**, and **z** value assigned to the Sensor. This is
    (currently) not used by ARLO.

    **Note:** The x,y,z coordinates are stored in the database as unit-less
    floats. However, assumptions will likely be made about these coordinates
    in processing. At a minimum, these should likely be a Cartesian
    coordinate system. I propose that these always be stored in meters,
    preferably UTM for geographic datum -- TonyB

Layout Coordinate
    The **layoutRow** and **layoutColumn** - the position to use in the
    Layout grid when displaying files by location (in
    visualizeMultiProjectFile).


Perhaps in the future, this will expand. Items such as microphone /
recorder type, and non-audio specific sensors (for images / videos).


SensorArray
^^^^^^^^^^^

A SensorArray defines a group of Sensors. Sensors within this array
are assignable to any MediaFiles within the same Library.

id
    Unique integer id (internally generated)

name
    Text name assigned by the user.

layoutRows
    The number of Rows to use in the Layout grid when displaying files by location.

layoutColumns
    The number of Columns to use in the Layout grid when displaying files by location.


Layout Details
--------------

The layout works by creating a grid of rows and columns, as specified by
*layoutRows* and *layoutColumns* respectively in the SensorArray.

Each Sensor has a *layoutRow* and *layoutColumn* value; these are
zero-based indexes in the grid, so a 5x5 grid can have coordinates
from (0,0) to (4,4).

Sensors that are to be excluded from the layout should have a Row and Column of (-1).

