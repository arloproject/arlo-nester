##################
MediaFile MetaData
##################


There are two models for storing MediaFile related MetaData. 

- *MediaFile MetaData*
    A model loosely fixed to internal coding for ARLO's use.

- *MediaFile UserMetaData*
    A user-defined set of MetaData, less for ARLO's use and more for
    sorting or searching *MediaFile* collections.


MediaFile MetaData
==================

This data mostly corresponds to technical details of a file, largely
what you would find in the file headers.

For audio files, such data includes:

- numChannels
- sampleRate
- durationInSeconds


MediaFile UserMetaData
======================

The *MediaFile UserMetaData* allows users to assign custom, user-defined
MetaData to a *MediaFile*. These are Key/Value pairs, for example
"Author=Douglas Adams" or "Recorded Date=21 July 1969".

Storage Method
--------------

*UserMetaData* is stored and queried on the Library level. 

To facilitate queries and prevent/detect typos, entries are not simply
stored as Key/Value pairs. Rather, each Library has a list of UserMetaData
Fields associated with it (for example, "Author", "Recorded Date", etc.).
*UserMetaData* is then stored by associating the value with the UserMetaData
Field and *MediaFile*.

These fields are unique to individual *Libraries*. While "Author" for
example could exist in multiple *Libraries*, these fields would not be
directly associated.


Storage Example
^^^^^^^^^^^^^^^

Let's take these example entries:

+-------------+-----------------+---------------+
| MediaFileID | Author          | Recorded Date |
+=============+=================+===============+
| 1           | Douglas Adams   | 8 March 1978  |
+-------------+-----------------+---------------+
| 2           | John F. Kennedy | 26 June 1963  |
+-------------+-----------------+---------------+

Fields
""""""

+-----------------+---------------+-----------------------------+
| MetaDataFieldID | Name          | Description                 |
+=================+===============+=============================+
| 101             | Author        | Author of the recording     |
+-----------------+---------------+-----------------------------+
| 102             | Recorded Date | When was this file recorded |
+-----------------+---------------+-----------------------------+

Entries
"""""""

+-------------+---------+-----------------+
| MediaFileID | FieldID | Value           |
+=============+=========+=================+
| 1           | 101     | Douglas Adams   |
+-------------+---------+-----------------+
| 1           | 102     | 8 March 1978    |
+-------------+---------+-----------------+
| 2           | 101     | John F. Kennedy |
+-------------+---------+-----------------+
| 2           | 102     | 26 June 1963    |
+-------------+---------+-----------------+



Internal Models
---------------

MediaFileUserMetaDataField
^^^^^^^^^^^^^^^^^^^^^^^^^^

+-------------+--------------+-----------------------------------------+
| Field Name  | Type         | Description                             |
+=============+==============+=========================================+
| name        | varchar(255) | Name of the UserMetaDataField           |
+-------------+--------------+-----------------------------------------+
| library     |              | The Library to which this Field belongs |
+-------------+--------------+-----------------------------------------+
| description | varchar(255) | Description of the Field                |
+-------------+--------------+-----------------------------------------+

MediaFileUserMetaData
^^^^^^^^^^^^^^^^^^^^^
+---------------+-----------------------------+---------------------------------------------------+
| Field Name    | Type                        | Description                                       |
+===============+=============================+===================================================+
| mediaFile     |                             | MediaFile to which this entry belongs             |
+---------------+-----------------------------+---------------------------------------------------+
| metaDataField |                             | The MetaDataField to which this entry corresponds |
+---------------+-----------------------------+---------------------------------------------------+
| description   | String (MySQL 'longtext'    | Value for this entry.                             |
|               | Field / 4 GB of characters) |                                                   |
+---------------+-----------------------------+---------------------------------------------------+


Interface
---------

Since *UserMetaData* is stored in the *Library*, many of the interface
functions are located on the *Library* page.
(Home -> Libraries -> Select a Library)

Import Library MetaData
^^^^^^^^^^^^^^^^^^^^^^^

This allows a user to import a CSV file of MetaData for *MediaFiles*
in a *Library*. We'll walk through an example to explain this process.
We'll use this test file as an example.

.. code-block:: none

    mediaFileId,name,testField,newField
    179070,Tom's_Diner,testEntry1,new1
    168682,5xt_1kinc,testEntry2,new2



Step 1 - Select File
""""""""""""""""""""

Choose the input file - once selecting the file, the browser will
read and parse this file and prepare the next steps.

.. image:: images/Metadata-file.png


Step 2 - Setup Columns and Fields
"""""""""""""""""""""""""""""""""

Here we have a lot of options to setup.

- Skip First Row on Import
    If the first row is a header, we want to skip this and not import.

- Import Mapping
    Here we determine what to do with each column in the file.

    - Skip
        In our example, we want to skip importing the 'mediaFileId' and
        'name' fields, since these are only there to help us map to the
        MediaFiles. There is no need to import a new field with this data.
    - Existing Fields
        Any existing MetaData Fields in the Library will show up in the
        Dropdown and can be selected as the target for the field import.
    - Add as New Field
        Create a new MetaData Field in the Library, and import data into this.

- MediaFile ID / Name
    Somehow, this data needs to be keyed to the files already in the
    *Library*. We can do this by using ARLO's File ID (if the CSV
    includes this information) or by matching on the Name of the file.
    The Name does not have to be an exact match, some fuzziness is
    allowed, but it's not a very intelligent search so if your names
    vary from ARLO's Alias / Filename data, it may not figure it out.

.. image:: images/Metadata-mapping.png

Step 3 - Import Prepared
""""""""""""""""""""""""

After setting up the data mapping, this parses the CSV data, creates
new Fields in ARLO, does some validations, etc. Verbose results are
shown in the top box, along with any errors in the bottom.

At this point, no data has actually been imported, only some validations
to help prevent partial imports.

.. image:: images/Metadata-prepared.png

Step 4 - Import
"""""""""""""""

This part can take a while, depending on the size of the data set.
Don't close your browser during this process.

.. image:: images/Metadata-import.png

Export Library MetaData
-----------------------

Export a CSV File of MetaData for all MediaFiles in a Library.
User-selectable for which fields to include, and includes both the
internal MediaFile MetaData and custom 'User' MetaData.

.. image:: images/Metadata-export.png

