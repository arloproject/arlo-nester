#########
MediaFile
#########

Originally just Audio Files, we have migrated to a more generic model,
*MediaFile*. This is extensible to any type of media; currently only
Audio is in common use, but anticipated future use includes Images and Videos.

Database Models
===============

MediaFile
---------

The MediaFile model itself is fairly generic, having only a few built-in columns.

+------------------+------------------------------------------------------+
| Field            | Description                                          |
+==================+======================================================+
| file             | The file. In Django, a FileField object holding the  |
|                  | file. In the database, the path to the file relative |
|                  | to MEDIA_ROOT.                                       |
+------------------+------------------------------------------------------+
| user             | Who owns the file                                    |
+------------------+------------------------------------------------------+
| type             | Reference to MediaTypes - indicates Audio, Image,    |
|                  | Video, etc.                                          |
+------------------+------------------------------------------------------+
| active           | Only used in very specific situations, but basically |
|                  | 'should this file be used in processing'             |
+------------------+------------------------------------------------------+
| alias            | Display name of the file                             |
+------------------+------------------------------------------------------+
| uploadDate       | For diagnostics / logging / tracking - when was the  |
|                  | file uploaded.                                       |
+------------------+------------------------------------------------------+
| sensor           | Null by default, or a reference to which             |
|                  | :ref:`Sensor<arlo-sensors>` collected this file      |
+------------------+------------------------------------------------------+
| library          | Which *Library* contains this file                   |
+------------------+------------------------------------------------------+
| realStartTime    | The datetime at which the recording begain, if known |
+------------------+------------------------------------------------------+
| md5              | MD5 Checksum of the file (may be explicitly set and  |
|                  | used for validation of proper import, or             |
|                  | automatically computed during import.                |
+------------------+------------------------------------------------------+


MediaFileMetaData
-----------------

The *MediaFileMetaData* is a key/value system for having extensible
metadata for *MediaFiles*. This allows flexibility for specific *Projects*,
though a core set of MetaData will likely be necessary for each
MediaFileType (though software should check and gracefully fail if the
necessary data isn't available).

+--------------+-------------------------------------------------------------+
| Field        | Description                                                 |
+==============+=============================================================+
| mediaFile    | Reference to the MediaFile                                  |
+--------------+-------------------------------------------------------------+
| name         | Name of the Datum                                           |
+--------------+-------------------------------------------------------------+
| value        | Value of the Datum                                          |
+--------------+-------------------------------------------------------------+
| userEditable | Whether the user is free to edit this data (for example,    |
|              | users should be able to freely edit Audio File length data) |
+--------------+-------------------------------------------------------------+

MetaData for Audio Files
^^^^^^^^^^^^^^^^^^^^^^^^

Here's a typical list of Metadata that is expected for AudioFiles

- Not User Editable (generally required by ARLO)

  - fileSize
  - validRIFFsize
  - validDATAsize
  - dataStartIndex
  - dataSize
  - wFormatTag
  - numChannels
  - sampleRate
  - nAvgBytesPerSec
  - nBlockAlign
  - bitsPerSample
  - numBytesToTailPad
  - numFrames
  - durationInSeconds

- User Editable (generally NOT required, i.e., not used directly
  by ARLO. Most of these are being removed now that we have the
  UserMetaData)

  - subjectName
  - startTime (necessary for some ARLO functions)
  - age
  - id_number
  - sunrise
  - sunset
  - contextInfo
  - microphoneType
  - microphonePlacement
  - numSpecies
  - weatherConditions
  - birdSpecies
  - latitude
  - longitude
