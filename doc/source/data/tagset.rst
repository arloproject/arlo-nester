.. _arlo-data-tagset:

######
TagSet
######

A *TagSet* is a collection of *Tags* within a *Project*. Every *Tag* is
stored in exactly one *TagSet*.

See also :ref:`arlo-data-hierarchy`

*TagSets* can be used to group *Tags* in many ways. For example:

- A set for each user that hand tags
- A set for an automatic tag discovery process (Machine Tags)
- A set for each category of tag, e.g., 

  - Spoken Words
  - Individual Syllables
  - Background Noise
  - Bird Species


TagSet Descriptions
===================

Descriptions can be added to *TagSets* to assist with organization.
To view or edit a description, click on the TagSet id in a listing: 

.. image:: images/Tagset-id.png

.. image:: images/Tagset-desc.png

Hiding TagSets
==============

The number of *TagSets* in a *Project* can quickly become overwhelming.
To simplify the interface, it is possible to 'Hide' *TagSets*.

Hiding a *TagSet* removes it from most of the interface, while keeping
all data intact.

To Hide / Unhide a *TagSet*, select the View All option for the *Project*.

.. image:: images/Tagset-all.png

This will display a list of all of the *Project*'s *TagSets*, with
hidden ones displayed with strikethrough.

.. image:: images/Tagset-hide.png

This results in far fewer *TagSets* being displayed in the interface: 

.. image:: images/Tagset-clean.png


