.. _arlo-data-hierarchy: 

#########################################
Hierarchy of Project, Libraries, and Tags
#########################################

.. image:: images/Project_Layout.png

Library
=======

A *Library* is the container in which *MediaFiles* and associated
data are stored. 

MediaFiles
==========

Every *MediaFile* is in exactly one *Library*.

.. note::

  In earlier versions we grouped *MediaFiles* by user. The *Library* aims
  to be an abstraction away from user-centric file ownership to facilitate
  sharing.

UserMetaData
------------

The custom *MediaFileUserMetaData* is stored within the *Library*. While the
MetaData entries are unique to a file, the Fields used to define these values
(e.g., "Author", "Date") are common to a *Library*. See [[MediaFileUserMetaData]]

Project
=======

*Projects* are the primary organizer that users work with. A *Project*
contains links a set of *MediaFiles* that are tagged or otherwise
processed.

Tag Classes
===========

Each *Project* contains a collection of *TagClasses*. These classes
are common among all of the *TagSets* in a *Project*, but are not
shared outside of the *Project*.

TagSet
======

See Also :ref:`arlo-data-tagset`.

A collection of *Tags*. *TagSets* can be used to group *Tags* in many ways. For example:

- A set for each user that hand tags
- A set for an automatic tag discovery process (Machine Tags)
- A set for each category of tag, e.g., 

  - Spoken Words
  - Individual Syllables
  - Background Noise
  - Bird Species

.. note::

  Each *Tag* is in exactly one *TagSet*.


