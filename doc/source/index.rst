.. ARLO documentation master file, created by
   sphinx-quickstart on Thu Jun 30 22:28:03 2016.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to ARLO's documentation!
================================

This is the official source of ARLO Documentation.

.. note::

  Documentation from the MediaWiki (http://wiki.arloproject.com/) will
  be migrated here and the Wiki shutdown. Some of this documentation
  may be out of date, please open a bug report / email TonyB if you find
  anything that is incorrect.


Contents:

.. toctree::
  :maxdepth: 2
  :glob:

  team
  data/index
  development/index
  glossary
  jobs
  pitch_trace
  plugins
  related_tools
  user_guides
  user_sharing

  api/v0/*

  arlo_doc_style


Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
