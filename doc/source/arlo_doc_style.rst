=====================
ARLO Docs Style Guide
=====================

Section Headings
----------------

RST doesn't specify a hierarchy for headings - lacking a better reference, in
general we'll prefer the conventions used by the Python documentation:

* ``#`` with overline, for parts
* ``*`` with overline, for chapters
* ``=``, for sections
* ``-``, for subsections
* ``^``, for subsubsections
* ``"``, for paragraphs


Graph Colors
------------

.. graphviz::

    digraph {
        antiquewhite [shape=box,style="rounded,filled",color=antiquewhite,label="antiquewhite"];
        coral [shape=box,style="rounded,filled",color=coral,label="coral"];
        darkgoldenrod [shape=box,style="rounded,filled",color=darkgoldenrod,label="darkgoldenrod"];
        azure [shape=box,style="rounded,filled",color=azure,label="azure"];
        crimson [shape=box,style="rounded,filled",color=crimson,label="crimson"];
	}
.. graphviz::

    digraph {
        gold [shape=box,style="rounded,filled",color=gold,label="gold"];
        bisque [shape=box,style="rounded,filled",color=bisque,label="bisque"];
        darksalmon [shape=box,style="rounded,filled",color=darksalmon,label="darksalmon"];
        goldenrod [shape=box,style="rounded,filled",color=goldenrod,label="goldenrod"];
        aliceblue [shape=box,style="rounded,filled",color=aliceblue,label="aliceblue"];
	}
.. graphviz::

    digraph {
        blanchedalmond [shape=box,style="rounded,filled",color=blanchedalmond,label="blanchedalmond"];
        deeppink [shape=box,style="rounded,filled",color=deeppink,label="deeppink"];
        greenyellow [shape=box,style="rounded,filled",color=greenyellow,label="greenyellow"];
        blue [shape=box,style="rounded,filled",color=blue,label="blue"];
        cornsilk [shape=box,style="rounded,filled",color=cornsilk,label="cornsilk"];
	}
.. graphviz::

    digraph {
        firebrick [shape=box,style="rounded,filled",color=firebrick,label="firebrick"];
        lightgoldenrod [shape=box,style="rounded,filled",color=lightgoldenrod,label="lightgoldenrod"];
        blueviolet [shape=box,style="rounded,filled",color=blueviolet,label="blueviolet"];
        floralwhite [shape=box,style="rounded,filled",color=floralwhite,label="floralwhite"];
        hotpink [shape=box,style="rounded,filled",color=hotpink,label="hotpink"];
	}
.. graphviz::

    digraph {
        lightgoldenrodyellow [shape=box,style="rounded,filled",color=lightgoldenrodyellow,label="lightgoldenrodyellow"];
        cadetblue [shape=box,style="rounded,filled",color=cadetblue,label="cadetblue"];
        gainsboro [shape=box,style="rounded,filled",color=gainsboro,label="gainsboro"];
        indianred [shape=box,style="rounded,filled",color=indianred,label="indianred"];
        lightyellow [shape=box,style="rounded,filled",color=lightyellow,label="lightyellow"];
	}
.. graphviz::

    digraph {
        cornflowerblue [shape=box,style="rounded,filled",color=cornflowerblue,label="cornflowerblue"];
        ghostwhite [shape=box,style="rounded,filled",color=ghostwhite,label="ghostwhite"];
        lightpink [shape=box,style="rounded,filled",color=lightpink,label="lightpink"];
        palegoldenrod [shape=box,style="rounded,filled",color=palegoldenrod,label="palegoldenrod"];
        darkslateblue [shape=box,style="rounded,filled",color=darkslateblue,label="darkslateblue"];
	}
.. graphviz::

    digraph {
        honeydew [shape=box,style="rounded,filled",color=honeydew,label="honeydew"];
        lightsalmon [shape=box,style="rounded,filled",color=lightsalmon,label="lightsalmon"];
        yellow [shape=box,style="rounded,filled",color=yellow,label="yellow"];
        deepskyblue [shape=box,style="rounded,filled",color=deepskyblue,label="deepskyblue"];
        ivory [shape=box,style="rounded,filled",color=ivory,label="ivory"];
	}
.. graphviz::

    digraph {
        maroon [shape=box,style="rounded,filled",color=maroon,label="maroon"];
        yellowgreen [shape=box,style="rounded,filled",color=yellowgreen,label="yellowgreen"];
        dodgerblue [shape=box,style="rounded,filled",color=dodgerblue,label="dodgerblue"];
        lavender [shape=box,style="rounded,filled",color=lavender,label="lavender"];
        mediumvioletred [shape=box,style="rounded,filled",color=mediumvioletred,label="mediumvioletred"];
	}
.. graphviz::

    digraph {
        indigo [shape=box,style="rounded,filled",color=indigo,label="indigo"];
        lavenderblush [shape=box,style="rounded,filled",color=lavenderblush,label="lavenderblush"];
        orangered [shape=box,style="rounded,filled",color=orangered,label="orangered"];
        lightblue [shape=box,style="rounded,filled",color=lightblue,label="lightblue"];
        lemonchiffon [shape=box,style="rounded,filled",color=lemonchiffon,label="lemonchiffon"];
	}
.. graphviz::

    digraph {
        palevioletred [shape=box,style="rounded,filled",color=palevioletred,label="palevioletred"];
        chartreuse [shape=box,style="rounded,filled",color=chartreuse,label="chartreuse"];
        lightskyblue [shape=box,style="rounded,filled",color=lightskyblue,label="lightskyblue"];
        linen [shape=box,style="rounded,filled",color=linen,label="linen"];
        pink [shape=box,style="rounded,filled",color=pink,label="pink"];
	}
.. graphviz::

    digraph {
        darkgreen [shape=box,style="rounded,filled",color=darkgreen,label="darkgreen"];
        lightslateblue [shape=box,style="rounded,filled",color=lightslateblue,label="lightslateblue"];
        mintcream [shape=box,style="rounded,filled",color=mintcream,label="mintcream"];
        red [shape=box,style="rounded,filled",color=red,label="red"];
        darkolivegreen [shape=box,style="rounded,filled",color=darkolivegreen,label="darkolivegreen"];
        mediumblue [shape=box,style="rounded,filled",color=mediumblue,label="mediumblue"];
	}
.. graphviz::

    digraph {
        mistyrose [shape=box,style="rounded,filled",color=mistyrose,label="mistyrose"];
        salmon [shape=box,style="rounded,filled",color=salmon,label="salmon"];
        darkseagreen [shape=box,style="rounded,filled",color=darkseagreen,label="darkseagreen"];
        mediumslateblue [shape=box,style="rounded,filled",color=mediumslateblue,label="mediumslateblue"];
        moccasin [shape=box,style="rounded,filled",color=moccasin,label="moccasin"];
	}
.. graphviz::

    digraph {
        tomato [shape=box,style="rounded,filled",color=tomato,label="tomato"];
        forestgreen [shape=box,style="rounded,filled",color=forestgreen,label="forestgreen"];
        midnightblue [shape=box,style="rounded,filled",color=midnightblue,label="midnightblue"];
        navajowhite [shape=box,style="rounded,filled",color=navajowhite,label="navajowhite"];
        violetred [shape=box,style="rounded,filled",color=violetred,label="violetred"];
	}
.. graphviz::

    digraph {
        green [shape=box,style="rounded,filled",color=green,label="green"];
        navy [shape=box,style="rounded,filled",color=navy,label="navy"];
        oldlace [shape=box,style="rounded,filled",color=oldlace,label="oldlace"];
        greenyellow [shape=box,style="rounded,filled",color=greenyellow,label="greenyellow"];
        navyblue [shape=box,style="rounded,filled",color=navyblue,label="navyblue"];
	}
.. graphviz::

    digraph {
        papayawhip [shape=box,style="rounded,filled",color=papayawhip,label="papayawhip"];
        lawngreen [shape=box,style="rounded,filled",color=lawngreen,label="lawngreen"];
        powderblue [shape=box,style="rounded,filled",color=powderblue,label="powderblue"];
        peachpuff [shape=box,style="rounded,filled",color=peachpuff,label="peachpuff"];
        beige [shape=box,style="rounded,filled",color=beige,label="beige"];
	}
.. graphviz::

    digraph {
        lightseagreen [shape=box,style="rounded,filled",color=lightseagreen,label="lightseagreen"];
        royalblue [shape=box,style="rounded,filled",color=royalblue,label="royalblue"];
        seashell [shape=box,style="rounded,filled",color=seashell,label="seashell"];
        brown [shape=box,style="rounded,filled",color=brown,label="brown"];
        limegreen [shape=box,style="rounded,filled",color=limegreen,label="limegreen"];
	}
.. graphviz::

    digraph {
        skyblue [shape=box,style="rounded,filled",color=skyblue,label="skyblue"];
        snow [shape=box,style="rounded,filled",color=snow,label="snow"];
        burlywood [shape=box,style="rounded,filled",color=burlywood,label="burlywood"];
        mediumseagreen [shape=box,style="rounded,filled",color=mediumseagreen,label="mediumseagreen"];
        slateblue [shape=box,style="rounded,filled",color=slateblue,label="slateblue"];
	}
.. graphviz::

    digraph {
        thistle [shape=box,style="rounded,filled",color=thistle,label="thistle"];
        chocolate [shape=box,style="rounded,filled",color=chocolate,label="chocolate"];
        mediumspringgreen [shape=box,style="rounded,filled",color=mediumspringgreen,label="mediumspringgreen"];
        steelblue [shape=box,style="rounded,filled",color=steelblue,label="steelblue"];
        wheat [shape=box,style="rounded,filled",color=wheat,label="wheat"];
	}
.. graphviz::

    digraph {
        darkkhaki [shape=box,style="rounded,filled",color=darkkhaki,label="darkkhaki"];
        mintcream [shape=box,style="rounded,filled",color=mintcream,label="mintcream"];
        white [shape=box,style="rounded,filled",color=white,label="white"];
        khaki [shape=box,style="rounded,filled",color=khaki,label="khaki"];
        olivedrab [shape=box,style="rounded,filled",color=olivedrab,label="olivedrab"];
	}
.. graphviz::

    digraph {
        whitesmoke [shape=box,style="rounded,filled",color=whitesmoke,label="whitesmoke"];
        peru [shape=box,style="rounded,filled",color=peru,label="peru"];
        palegreen [shape=box,style="rounded,filled",color=palegreen,label="palegreen"];
        blueviolet [shape=box,style="rounded,filled",color=blueviolet,label="blueviolet"];
        rosybrown [shape=box,style="rounded,filled",color=rosybrown,label="rosybrown"];
	}
.. graphviz::

    digraph {
        seagreen [shape=box,style="rounded,filled",color=seagreen,label="seagreen"];
        darkorchid [shape=box,style="rounded,filled",color=darkorchid,label="darkorchid"];
        saddlebrown [shape=box,style="rounded,filled",color=saddlebrown,label="saddlebrown"];
        springgreen [shape=box,style="rounded,filled",color=springgreen,label="springgreen"];
        darkviolet [shape=box,style="rounded,filled",color=darkviolet,label="darkviolet"];
	}
.. graphviz::

    digraph {
        darkslategray [shape=box,style="rounded,filled",color=darkslategray,label="darkslategray"];
        sandybrown [shape=box,style="rounded,filled",color=sandybrown,label="sandybrown"];
        yellowgreen [shape=box,style="rounded,filled",color=yellowgreen,label="yellowgreen"];
        magenta [shape=box,style="rounded,filled",color=magenta,label="magenta"];
        dimgray [shape=box,style="rounded,filled",color=dimgray,label="dimgray"];
	}
.. graphviz::

    digraph {
        sienna [shape=box,style="rounded,filled",color=sienna,label="sienna"];
        mediumorchid [shape=box,style="rounded,filled",color=mediumorchid,label="mediumorchid"];
        gray [shape=box,style="rounded,filled",color=gray,label="gray"];
        tan [shape=box,style="rounded,filled",color=tan,label="tan"];
        mediumpurple [shape=box,style="rounded,filled",color=mediumpurple,label="mediumpurple"];
	}
.. graphviz::

    digraph {
        gray0 [shape=box,style="rounded,filled",color=gray0,label="gray0"];
        gray10 [shape=box,style="rounded,filled",color=gray10,label="gray10"];
        gray20 [shape=box,style="rounded,filled",color=gray20,label="gray20"];
        gray30 [shape=box,style="rounded,filled",color=gray30,label="gray30"];
        gray40 [shape=box,style="rounded,filled",color=gray40,label="gray40"];
	}
.. graphviz::

    digraph {
        gray50 [shape=box,style="rounded,filled",color=gray50,label="gray50"];
        gray60 [shape=box,style="rounded,filled",color=gray60,label="gray60"];
        gray70 [shape=box,style="rounded,filled",color=gray70,label="gray70"];
        gray80 [shape=box,style="rounded,filled",color=gray80,label="gray80"];
        gray90 [shape=box,style="rounded,filled",color=gray90,label="gray90"];
	}
.. graphviz::

    digraph {
        gray100 [shape=box,style="rounded,filled",color=gray100,label="gray100"];
        aquamarine [shape=box,style="rounded,filled",color=aquamarine,label="aquamarine"];
        mediumvioletred [shape=box,style="rounded,filled",color=mediumvioletred,label="mediumvioletred"];
        lightgray [shape=box,style="rounded,filled",color=lightgray,label="lightgray"];
        cyan [shape=box,style="rounded,filled",color=cyan,label="cyan"];
	}
.. graphviz::

    digraph {
        orchid [shape=box,style="rounded,filled",color=orchid,label="orchid"];
        lightslategray [shape=box,style="rounded,filled",color=lightslategray,label="lightslategray"];
        darkorange [shape=box,style="rounded,filled",color=darkorange,label="darkorange"];
        darkturquoise [shape=box,style="rounded,filled",color=darkturquoise,label="darkturquoise"];
        palevioletred [shape=box,style="rounded,filled",color=palevioletred,label="palevioletred"];
	}
.. graphviz::

    digraph {
        slategray [shape=box,style="rounded,filled",color=slategray,label="slategray"];
        orange [shape=box,style="rounded,filled",color=orange,label="orange"];
        lightcyan [shape=box,style="rounded,filled",color=lightcyan,label="lightcyan"];
        plum [shape=box,style="rounded,filled",color=plum,label="plum"];
        orangered [shape=box,style="rounded,filled",color=orangered,label="orangered"];
	}
.. graphviz::

    digraph {
        mediumaquamarine [shape=box,style="rounded,filled",color=mediumaquamarine,label="mediumaquamarine"];
        purple [shape=box,style="rounded,filled",color=purple,label="purple"];
        mediumturquoise [shape=box,style="rounded,filled",color=mediumturquoise,label="mediumturquoise"];
        violet [shape=box,style="rounded,filled",color=violet,label="violet"];
        black [shape=box,style="rounded,filled",color=black,label="black"];
	}
.. graphviz::

    digraph {
        paleturquoise [shape=box,style="rounded,filled",color=paleturquoise,label="paleturquoise"];
        violetred [shape=box,style="rounded,filled",color=violetred,label="violetred"];

    }



