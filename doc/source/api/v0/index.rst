.. contents::

##################
Nester Rest API v0
##################

**Note:** This is the Development version of the Nester API, started in Dec
2013. Items will change very quickly, and I do not recommend using this
without keeping track of all changes to the Git repo.

Eventually, once we have a stable feature-set, I'll lock it to a v1
of the API. While I expect the API to continue evolving quickly, v1
will hopefully provide a stable platform for limited operation.

Overview
========

This is built on the Django-REST-framework, though will likely not be
strictly RESTful. I debated heavily between this framework and TastyPie,
and chose this one solely because it provides a powerful web-browsable
interface to access the API.

While multiple encoding options exist within Django-REST-framework, current
work has been solely around JSON.

While the Django-REST-framework provides for full
Create/Retrieve/Update/Delete (CRUD) capabilities for the data models,
operations may be limited due to the complex interaction among the
models and security concerns.

This API aims to be a complete replacement for interacting with ARLO.
Many of these options are available via the web interface already, but
the goal is to separate the core of ARLO from the interface, such that
application specific interfaces can be used.


Example Applications
--------------------

* Importing Audio Files (replace the mass_import_audiofiles.pl script)
* Full CRUD capabilities for Tags, so we can script Tag creation, or
  integrate with external tools.
* Visualize audiofiles (this capability already exists elsewhere in ARLO).

Prohibited Actions
------------------

Until the full security model can be determined, numerous operations will
be completely prohibited, such as listing user information.

Additionally, NO unauthenticated requests will be allowed. All requests
must provide valid user authentication (unless specifically noted for
some special functions, such as OPTIONS requests).


Admin Users
-----------

Admin users have special status within many of the functions. When
developing against this API, keep this in mind if using an Admin user
for authentication.


Basic Usage
-----------

Some API Examples can be found within the
`ARLO Scripts <https://bitbucket.org/arloproject/arlo-scripts/>`_ Repository.


Authentication
==============

In future versions, I would like to provide Oauth2 authentication to
securely allow the integration of 3rd party tools, allowing
Tokens to be generated with specific access rights (e.g., import files,
view/create tags).

For simplicity, now I only provide BasicAuthentication (username /
password), SessionAuthentication (e.g., Cookies), or Token based
Authentication from the Django-REST-framework. These provide unrestricted
access as the user.


Basic Authentication
--------------------

This uses HTTP Basic Authentication Headers, for example a user 'arlo' with
password 'arlo':

.. code-block:: bash

    $ curl http://live.arloproject.com:81/api/v0/tagClass/ -u arlo:arlo
    {"count": 0, "next": null, "previous": null, "results": []}


Session Authentication
----------------------

This uses the Session Cookie that you would receive for example when
logging in through your browser. Note that you will need to include the
*csrftoken* for any update (POST/DELETE/PUT/PATCH) actions.

.. code-block:: bash

    $ curl 'http://live.arloproject.com:81/api/v0/tagClass/' -H 'Cookie: sessionid=4203d3e73c1b337c506ae18c4b07319b; csrftoken=8c8b1180f2edc4009b10c4521611637c' 
    {"count": 0, "next": null, "previous": null, "results": []}


Token Authentication
--------------------

This is more ideal for API usage. This requires a single header to be
set and passes a token for authentication. 

To generate / retrieve a token for a user, you will need to perform a
POST request to /api-token-auth/

.. code-block:: bash

    $ curl -i -d "username=USERNAME&password=PASSWORD" http://live.arloproject.com:81/api-token-auth/
    {"token":"9944b09199c62bcf9418ad846dd0e4bbdfc6ee4b"}


Then calls will include this as a Header: 

.. code-block:: bash

    $ curl 'http://live.arloproject.com:81/api/v0/tagClass/' -H 'Authorization: Token 9944b09199c62bcf9418ad846dd0e4bbdfc6ee4b' 
    {"count": 0, "next": null, "previous": null, "results": []}

Currently, Tokens do not expire on any set schedule, and may be valid
practically forever. However, this is likely to change at some point,
so apps should make it a habit to check any stored Tokens and
re-authenticate as necessary. The above authentication request may be
issued repeatedly, and will always return the current valid Token - it will
remain the same until Expired or removed.


Schema Endpoint
===============

The Schema Endpoint provides a CoreAPI formatted representation of the API.
This can be retrieved dynamically to build CoreAPI clients for interaction.
Since this is generated dynamically, this view should always be up-to-date.

.. note::
    This function is outside of the v0 API and will include all API versions.

**/api/**
  | GET - Return the API Schema



.. code-block:: bash

    GET /api/

    HTTP 200 OK
    Allow: GET, HEAD, OPTIONS
    Content-Type: application/coreapi+json
    Vary: Accept

    {
        "_type": "document",
        "_meta": {
            "title": "ARLO API"
        },
        "api": {
            "v0": {
                "arloPermission": {
                    "list": {
                        "_type": "link",
                        "url": "/api/v0/arloPermission/",
                        "action": "get",
                        "description": "Manage ArloPermissions",
                        "fields": [
                            {
                                "name": "page",
                                "location": "query"
                            }
                        ]
                    },
                    "create": {

    <!-- snip -->

        },
        "api-token-auth": {
            "create": {
                "_type": "link",
                "url": "/api-token-auth/",
                "action": "post"
            }
        }
    }


API Model Endpoints
===================

API Endpoints for Django Models that store all ARLO data.

.. toctree::
  :maxdepth: 1
  :glob:

  models/*


API Function Endpoints
======================

API Endpoints for various ARLO functions.

.. toctree::
  :maxdepth: 1
  :glob:

  functions/*
