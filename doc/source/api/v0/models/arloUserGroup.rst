ArloUserGroup
=============

This is the *tools_arloUserGroup* model from Nester.

**arloUserGroup/**
  | GET - List ArloPermissions to which the User has access.
  | POST - Create new ArloPermission

**arloUserGroup/{Id}/**
  | GET - Retrieve Record
  | PUT - Update Record
  | DELETE - Delete Record


Example List
------------

.. code-block:: bash

    GET /api/v0/arloUserGroup/

    HTTP 200 OK
    Content-Type: application/json
    Vary: Accept
    Allow: GET, POST, HEAD, OPTIONS

    {
        "count": 2,
        "next": null,
        "previous": null,
        "results": [
            {
                "id": 2,
                "name": "test",
                "users": [],
                "creationDate": "2016-06-23T20:41:44",
                "createdBy": 18
            },
            {
                "id": 1,
                "name": "TestGroup",
                "users": [
                    1,
                    18
                ],
                "creationDate": "2016-06-23T19:58:51",
                "createdBy": 18
            }
        ]
    }

Example Detail
--------------

.. code-block:: bash

    GET /api/v0/arloUserGroup/1/

    HTTP 200 OK
    Content-Type: application/json
    Vary: Accept
    Allow: GET, PUT, PATCH, DELETE, HEAD, OPTIONS

    {
        "id": 1,
        "name": "TestGroup",
        "users": [
            1,
            18
        ],
        "creationDate": "2016-06-23T19:58:51",
        "createdBy": 18
    }
