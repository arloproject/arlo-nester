JobResultFile
=============

This is the *tools_jobResultFile* model from Nester. Note that this
comprises of both the metadata stored in the database, as well as a file
stored in the user-files/

**jobResultFile/**
  | GET - List JobResultFiles to which the User has access.
  | POST - Create new JobResultFile

**jobResultFile/{Id}/**
  | GET - Retrieve Record
  | PUT - Update Record
  | DELETE - Delete Record


Example List
------------

.. code-block:: bash

    GET /api/v0/jobResultFile/

    HTTP 200 OK
    Content-Type: application/json
    Vary: Accept
    Allow: GET, POST, HEAD, OPTIONS

    {
        "count": 1,
        "next": null,
        "previous": null,
        "results": [
            {
                "id": 1,
                "job": 140152,
                "creationDate": "2015-11-21T17:54:35",
                "resultFile": "https://media.amanda.arloproject.com/files/user-files/tonyb/job-result-files/test-file_Xd85nEz.txt",
                "notes": "Test API Upload"
            }
        ]
    }

Example Detail
--------------

.. code-block:: bash

    GET /api/v0/jobResultFile/5/

    HTTP 200 OK
    Content-Type: application/json
    Vary: Accept
    Allow: GET, PUT, PATCH, DELETE, HEAD, OPTIONS

    {
        "id": 5,
        "job": 139994,
        "creationDate": "2015-07-06T21:11:21",
        "resultFile": "https://media.amanda.arloproject.com/files/user-files/tonyb/job-result-files/test-file_yRStGcY.txt",
        "notes": "Test API Upload"
    }

Example Create
--------------

.. note:: Since this involves uploading a file, the regular JSON interface won't be sufficient. Here we'll need to use a mutlipart/form-data POST.

.. code-block:: bash

    $ curl -H "Authorization: Token $ARLOTOKEN" -i $ARLO_URL_ROOT/api/v0/jobResultFile/ -F resultFile=@test-file.txt -F job=139994 -F notes="Test API Upload"

    HTTP/1.0 201 CREATED
    Date: Tue, 07 Jul 2015 02:11:21 GMT
    Server: WSGIServer/0.1 Python/2.7.3
    Vary: Accept, Cookie
    Content-Type: application/json
    Allow: GET, POST, HEAD, OPTIONS

    {
        "creationDate": "2015-07-06T21:11:21.801131",
        "id": 5,
        "job": 139994,
        "notes": "Test API Upload",
        "resultFile": "https://media.amanda.arloproject.com/files/user-files/tonyb/job-result-files/test-file_yRStGcY.txt"
    }
