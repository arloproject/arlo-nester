Library
=======

This is the tools_library model from Nester.

**library/**
  | GET - List Libraries to which the User has access.
  | POST - Create new Library for the auth'd user.

**library/{Id}/**
  | GET - Retrieve Record
  | PUT - Update Record
  | DELETE - Delete Record

Example List
------------

.. code-block:: bash

    GET /api/v0/library/

    HTTP 200 OK
    Vary: Accept
    Content-Type: text/html; charset=utf-8
    Allow: GET, POST, HEAD, OPTIONS

    {
        "count": 1,
        "next": null,
        "previous": null,
        "results": [
            {
                "id": 1,
                "user": 1,
                "name": "myLib",
                "mediaFiles": [],
                "creationDate": "2014-01-19T01:18:50"
            }
        ]
    }

Example Detail
--------------

.. code-block:: bash

    GET /api/v0/library/1/

    HTTP 200 OK
    Vary: Accept
    Content-Type: text/html; charset=utf-8
    Allow: GET, PUT, DELETE, HEAD, OPTIONS, PATCH

    {
        "id": 1,
        "user": 1,
        "name": "myLib",
        "mediaFiles": [],
        "creationDate": "2014-01-19T01:18:50"
    }


Library MediaFiles
==================

Returns a list of MediaFiles in a Library. The output format is a list of
MediaFile objects as described elsewhere.

**library/{Id}/mediaFiles/**
  | GET - Retrieve MediaFile list

Example List
------------

.. code-block:: bash

    HTTP 200 OK
    Content-Type: application/json
    Vary: Accept
    Allow: OPTIONS, GET

    {
        "count": 7,
        "next": null,
        "previous": null,
        "results": [
            {
                "id": 168675,
                "file": "1k_every10.wav",
                "user": 18,
                "type": 1,
                "active": true,
                "alias": "tonyb_test",
                "uploadDate": "2012-08-30T17:44:37",
                "hasTranscript": false,
                "sensor": null,
                "library": 69,
                "realStartTime": "2012-08-30T17:44:37",
                "mediafilemetadata_set": [
                    {
                        "id": 76,
                        "mediaFile": 6,
                        "name": "fileSize",
                        "value": "260396",
                        "userEditable": false
                    },
    <SNIP>
                ],
                "url": "https://media.amanda.arloproject.com/files/user-files/tonyb/1k_every10.wav"
            },
    <SNIP>
         ]
    }


Library UserMetaDataFields
==========================

Returns a list of UserMetaDataFields in a Library. The output format is a
list of MediaFileUserMetaDataFields objects as described elsewhere.

**library/{Id}/userMetaDataFields/**
  | GET - Retrieve UserMetaDataFields list


Example List
------------

.. code-block:: bash

    GET /api/v0/library/69/userMetaDataFields/

    HTTP 200 OK
    Content-Type: application/json
    Vary: Accept
    Allow: OPTIONS, GET

    {
        "count": 3,
        "next": null,
        "previous": null,
        "results": [
            {
                "id": 1,
                "name": "testField",
                "library": 69,
                "description": "Test Field"
            },
            {
                "id": 2,
                "name": "testField2",
                "library": 69,
                "description": "Test Field 2"
            },
            {
                "id": 9,
                "name": "newField",
                "library": 69,
                "description": "Auto Added By Import"
            }
        ]
    }
