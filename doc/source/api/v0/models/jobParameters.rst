JobParameters
=============

This is the *tools_jobParameters* model from Nester.

**jobParameters/**
  | GET - List JobParameters from all Jobs to which the user has access. 
  | POST - Create new JobParameter

**jobParameters/{Id}/**
  | GET - Retrieve Record
  | PUT - Update Record
  | DELETE - Delete Record

Example List
------------

.. code-block:: bash

    GET /api/v0/jobParameters/

    HTTP 200 OK
    Vary: Accept
    Content-Type: text/html; charset=utf-8
    Allow: GET, POST, HEAD, OPTIONS

    {
        "count": 3,
        "next": null,
        "previous": null,
        "results": [
            {
                "id": 1,
                "job": 6,
                "name": "windowSizeInSeconds",
                "value": "4.0"
            },
            {
                "id": 2,
                "job": 6,
                "name": "numWindowsToTag",
                "value": "2"
            },
            {
                "id": 3,
                "job": 6,
                "name": "weight",
                "value": "Time"
            }
        ]
    }

Example Detail
--------------

.. code-block:: bash

    GET /api/v0/jobParameters/3/

    HTTP 200 OK
    Vary: Accept
    Content-Type: text/html; charset=utf-8
    Allow: GET, PUT, DELETE, HEAD, OPTIONS, PATCH

    {
        "id": 3,
        "job": 6,
        "name": "weight",
        "value": "Time"
    }
