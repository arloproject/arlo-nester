Group
=====

This is the *auth_group* model from the Django Authentication Framework,
currently unused in Nester.

**group/**
  GET - List/Create Groups

**group/{Id}/**
  Group Detail

This is read-only for Admin users, Forbidden for all other users.

Example List
------------

.. code-block:: bash

    GET /api/v0/group/

    HTTP 200 OK
    Vary: Accept
    Content-Type: text/html; charset=utf-8
    Allow: GET, HEAD, OPTIONS

    {
        "count": 0,
        "next": null,
        "previous": null,
        "results": []
    }
