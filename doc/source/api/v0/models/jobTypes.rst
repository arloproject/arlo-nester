JobTypes
========

This is the tools_jobtypes model from Nester. This is read-only for
*any* authenticated user.

**jobTypes/**
  | GET - List JobTypes

**jobTypes/{Id}/**
  | GET - JobTypes Detail


Example List
------------

.. code-block:: bash

    GET /api/v0/jobTypes/

    HTTP 200 OK
    Vary: Accept
    Content-Type: text/html; charset=utf-8
    Allow: GET, HEAD, OPTIONS

    {
        "count": 11,
        "next": "http://arlo.local/api/v0/jobTypes/?page=2",
        "previous": null,
        "results": [
            {
                "id": 1,
                "name": "SupervisedTagDiscovery"
            },
            {
                "id": 2,
                "name": "RandomWindowTagging"
            },
            {
                "id": 3,
                "name": "TagAnalysis"
            },
            {
                "id": 4,
                "name": "AdaptAnalysis"
            },
            {
                "id": 5,
                "name": "Transcription"
            },
            {
                "id": 6,
                "name": "UnknownTagDiscovery"
            },
            {
                "id": 7,
                "name": "UnsupervisedTagDiscovery"
            },
            {
                "id": 8,
                "name": "Test"
            },
            {
                "id": 9,
                "name": "ImportAudioFile"
            },
            {
                "id": 10,
                "name": "SupervisedTagDiscoveryParent"
            }
    }

Example Detail
--------------

.. code-block:: bash

    GET /api/v0/jobTypes/1/

    HTTP 200 OK
    Vary: Accept
    Content-Type: text/html; charset=utf-8
    Allow: GET, HEAD, OPTIONS

    {
        "id": 1,
        "name": "SupervisedTagDiscovery"
    }
