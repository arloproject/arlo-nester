ProjectTypes
============

This is the tools_projecttypes model from Nester. This is read-only
accessible to any authenticated user.

**projectTypes/**
  | GET - List ProjectTypes

**projectTypes/{Id}/**
  | GET - Retrieve ProjectType Detail

Example List
------------

.. code-block:: bash

    GET /api/v0/projectTypes/

    HTTP 200 OK
    Vary: Accept
    Content-Type: text/html; charset=utf-8
    Allow: GET, HEAD, OPTIONS

    {
        "count": 5,
        "next": null,
        "previous": null,
        "results": [
            {
                "id": 1,
                "name": "audio"
            },
            {
                "id": 2,
                "name": "image"
            },
            {
                "id": 3,
                "name": "movement"
            },
            {
                "id": 4,
                "name": "text"
            },
            {
                "id": 5,
                "name": "video"
            }
        ]
    }

Example Detail
--------------

.. code-block:: bash

    GET /api/v0/projectTypes/1/

    HTTP 200 OK
    Vary: Accept
    Content-Type: text/html; charset=utf-8
    Allow: GET, HEAD, OPTIONS

    {
        "id": 1,
        "name": "audio"
    }
