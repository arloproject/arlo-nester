Tag
===

This is the tools_tag model from Nester.

**tag/**
  | GET - List Tags to which the User has access.
  | POST - Create new Tag

**tag/{Id}/**
  | GET - Retrieve Record
  | PUT - Update Record
  | DELETE - Delete Record


Example List
------------

.. code-block:: bash

    GET /api/v0/tag/

    HTTP 200 OK
    Vary: Accept
    Content-Type: text/html; charset=utf-8
    Allow: GET, POST, HEAD, OPTIONS

    {
        "count": 1,
        "next": null,
        "previous": null,
        "results": [
            {
                "id": 2,
                "user": 1,
                "creationDate": "2014-02-28T03:48:04",
                "tagClass": 1,
                "tagExample": 2,
                "parentTag": null,
                "randomlyChosen": false,
                "machineTagged": false,
                "userTagged": true,
                "strength": 1.0
            }
        ]
    }

Example Detail
--------------

.. code-block:: bash

    GET /api/v0/tag/2/

    HTTP 200 OK
    Vary: Accept
    Content-Type: text/html; charset=utf-8
    Allow: GET, PUT, DELETE, HEAD, OPTIONS, PATCH

    {
        "id": 2,
        "user": 1,
        "creationDate": "2014-02-28T03:48:04",
        "tagClass": 1,
        "tagExample": 2,
        "parentTag": null,
        "randomlyChosen": false,
        "machineTagged": false,
        "userTagged": true,
        "strength": 1.0
    }




















TagClass
========

This is the tools_tagclass model from Nester.

**tagClass/**
  | GET - List all TagClasses to which a user has access.
  | POST - Create new TagClass for the auth'd user.

**tagClass/{Id}/**
  | GET - Retrieve Record
  | PUT - Update Record
  | DELETE - Delete Record


Example List
------------

.. code-block:: bash

    GET /api/v0/tagClass/

    HTTP 200 OK
    Vary: Accept
    Content-Type: text/html; charset=utf-8
    Allow: GET, HEAD, OPTIONS

    {
        "count": 1,
        "next": null,
        "previous": null,
        "results": [
            {
                "id": 1,
                "user": 1,
                "library": 1,
                "className": "testTagClass",
                "displayName": "testTagClass"
            }
        ]
    }

Example Detail
--------------

.. code-block:: bash

    GET /api/v0/tagClass/1/

    HTTP 200 OK
    Vary: Accept
    Content-Type: text/html; charset=utf-8
    Allow: GET, HEAD, OPTIONS

    {
        "id": 1,
        "user": 1,
        "library": 1,
        "className": "testTagClass",
        "displayName": "testTagClass"
    }
















TagExample
==========

This is the tools_tagexample model from Nester.

.. warning::
    This Model is deprecated and has been merged into Tag.


**tagExample/**
  | GET - List all TagExamples to which the User has access.
  | POST - Create new TagExample

**tagExample/{Id}/**
  | GET - Retrieve Record
  | PUT - Update Record
  | DELETE - Delete Record


Example List
------------

.. code-block:: bash

    GET /api/v0/tagExample/

    HTTP 200 OK
    Vary: Accept
    Content-Type: text/html; charset=utf-8
    Allow: GET, POST, HEAD, OPTIONS

    {
        "count": 1,
        "next": null,
        "previous": null,
        "results": [
            {
                "id": 2,
                "user": 1,
                "tagSet": 3,
                "mediaFile": 4,
                "creationDate": "2014-02-28T03:48:04",
                "startTime": 1.171875,
                "endTime": 1.65625,
                "minFrequency": 20.0,
                "maxFrequency": 20000.0
            }
        ]
    }

Example Detail
--------------

.. code-block:: bash

    GET /api/v0/tagExample/2/

    HTTP 200 OK
    Vary: Accept
    Content-Type: text/html; charset=utf-8
    Allow: GET, PUT, DELETE, HEAD, OPTIONS, PATCH

    {
        "id": 2,
        "user": 1,
        "tagSet": 3,
        "mediaFile": 4,
        "creationDate": "2014-02-28T03:48:04",
        "startTime": 1.171875,
        "endTime": 1.65625,
        "minFrequency": 20.0,
        "maxFrequency": 20000.0
    }


















TagSet
======

This is the tools_tagset model from Nester.

**tagSet/**
  | GET - List all TagSets to which the user has access.
  | POST - Create new TagSet

**tagSet/{Id}/**
  | GET - Retrieve Record
  | PUT - Update Record
  | DELETE - Delete Record


Example List
------------

.. code-block:: bash

    GET /api/v0/tagSet/

    HTTP 200 OK
    Vary: Accept
    Content-Type: text/html; charset=utf-8
    Allow: GET, POST, HEAD, OPTIONS

    {
        "count": 1,
        "next": null,
        "previous": null,
        "results": [
            {
                "id": 3,
                "user": 1,
                "name": "test",
                "library": 1,
                "creationDate": "2014-02-27T05:35:48"
            }
        ]
    }

Example Detail
--------------

.. code-block:: bash

    GET /api/v0/tagSet/3/

    HTTP 200 OK
    Vary: Accept
    Content-Type: text/html; charset=utf-8
    Allow: GET, PUT, DELETE, HEAD, OPTIONS, PATCH

    {
        "id": 3,
        "user": 1,
        "name": "test",
        "library": 1,
        "creationDate": "2014-02-27T05:35:48"
    }


TagSet Tags
===========

List
----

Returns a list of Tags in a TagSet. The output format is a list of
Tag objects as described elsewhere.

**tagSet/{Id}/tags/**
  | GET - List Tags in the TagSet

Example
^^^^^^^

.. code-block:: bash

    GET /api/v0/tagSet/1/tags/

    {
        "count": 9,
        "next": null,
        "previous": null,
        "results": [
            {
                "id": 296,
                "user": 1,
                "creationDate": "2017-07-29T22:21:49",
                "tagClass": 1,
                "tagSet": 1,
                "mediaFile": 6,
                "startTime": 6.38768537786542,
                "endTime": 7.38768537786542,
                "minFrequency": 20.0,
                "maxFrequency": 20000.0,
                "parentTag": null,
                "randomlyChosen": true,
                "machineTagged": true,
                "userTagged": true,
                "strength": 1.0
            },
    <SNIP>
            {
                "id": 308,
                "user": 2,
                "creationDate": "2017-08-26T14:19:13",
                "tagClass": 1,
                "tagSet": 1,
                "mediaFile": 8,
                "startTime": 6.42499109604,
                "endTime": 6.47186609604,
                "minFrequency": 20.0,
                "maxFrequency": 20000.0,
                "parentTag": null,
                "randomlyChosen": false,
                "machineTagged": false,
                "userTagged": true,
                "strength": 1.0
            }
        ]
    }
