RandomWindow
============

This is the tools_randomwindow model from Nester.

**randomWindow/**
  | GET - List RandomWindows to which the User has access.
  | POST - Create new RandomWindow

**randomWindow/{Id}/**
  | GET - Retrieve Record
  | PUT - Update Record
  | DELETE - Delete Record

Example List
------------

.. code-block:: bash

    GET /api/v0/randomWindow/

    HTTP 200 OK
    Vary: Accept
    Content-Type: text/html; charset=utf-8
    Allow: GET, POST, HEAD, OPTIONS

    {
        "count": 2,
        "next": null,
        "previous": null,
        "results": [
            {
                "id": 1,
                "randomWindowTaggingJob": 6,
                "mediaFile": 4,
                "startTime": 208.929947695481,
                "endTime": 212.929947695481
            },
            {
                "id": 2,
                "randomWindowTaggingJob": 6,
                "mediaFile": 4,
                "startTime": 136.607616242576,
                "endTime": 140.607616242576
            }
        ]
    }

Example Detail
--------------

.. code-block:: bash

    GET /api/v0/randomWindow/1/

    HTTP 200 OK
    Vary: Accept
    Content-Type: text/html; charset=utf-8
    Allow: GET, HEAD, OPTIONS

    {
        "id": 1,
        "randomWindowTaggingJob": 6,
        "mediaFile": 4,
        "startTime": 208.929947695481,
        "endTime": 212.929947695481
    }
