Jobs
====

This is the *tools_jobs* model from Nester.

**jobs/**
  | GET - List Jobs.
  | POST - Create new Job for the auth'd user.

**jobs/{Id}/**
  | GET - Retrieve Record
  | PUT - Update Record
  | DELETE - Delete Record

Example List
------------

.. code-block:: bash

    GET /api/v0/jobs/?page=20

    HTTP 200 OK
    Content-Type: application/json
    Vary: Accept
    Allow: GET, POST, HEAD, OPTIONS

    {
        "count": 37379,
        "next": "http://amanda.arloproject.com:9000/api/v0/jobs/?page=21",
        "previous": "http://amanda.arloproject.com:9000/api/v0/jobs/?page=19",
        "results": [
            {
                "id": 6474,
                "name": "Supervised Tag Discovery Child - mediaFileId: 179351",
                "user": 78,
                "project": 239,
                "type": 11,
                "creationDate": "2013-05-31T00:00:00",
                "numToComplete": 0,
                "numCompleted": 0,
                "fractionCompleted": 0.0,
                "elapsedRealTime": 0.0,
                "timeToCompletion": -1.0,
                "isRunning": true,
                "wasStopped": false,
                "isComplete": false,
                "wasDeleted": false,
                "status": 4,
                "requestedStatus": null,
                "startedDate": null,
                "lastStatusDate": null,
                "parentJob": 6446,
                "priority": 0,
                "jobparameters_set": [
                    {
                        "id": 19589,
                        "job": 6474,
                        "name": "mediaFileId",
                        "value": "179351"
                    }
                ]
            },
     <!-- snip -->
        ]
    }

Example Detail
--------------

.. code-block:: bash

    GET /api/v0/jobs/6446/

    HTTP 200 OK
    Content-Type: application/json
    Vary: Accept
    Allow: GET, PUT, PATCH, DELETE, HEAD, OPTIONS

    {
        "id": 6446,
        "name": "Supervised Tag Discovery Parent",
        "user": 78,
        "project": 239,
        "type": 10,
        "creationDate": "2013-05-31T10:50:46",
        "numToComplete": 0,
        "numCompleted": 0,
        "fractionCompleted": 0.0,
        "elapsedRealTime": 0.0,
        "timeToCompletion": -1.0,
        "isRunning": true,
        "wasStopped": false,
        "isComplete": false,
        "wasDeleted": false,
        "status": 4,
        "requestedStatus": null,
        "startedDate": null,
        "lastStatusDate": null,
        "parentJob": null,
        "priority": 0,
        "jobparameters_set": [
            {
                "id": 19544,
                "job": 6446,
                "name": "numberOfTagsToDiscover",
                "value": "10"
            },
     <!-- snip -->
            {
                "id": 19561,
                "job": 6446,
                "name": "destinationTagSet",
                "value": "209"
            }
        ]
    }
