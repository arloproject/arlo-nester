ProjectPermissions
==================

This is the tools_projectpermissions model from Nester. Users can access
ProjectPermissions of Projects which they own.

.. warning::
    This method for sharing data is deprecated. See the Library Sharing
    docs for more information.

**projectPermissions/**
  | GET - List ProjectPermissions
  | POST - Create new ProjectPermission

**projectPermissions/{Id}/**
  | GET - Retrieve Record
  | PUT - Update Record
  | DELETE - Delete Record

Example List
------------

.. code-block:: bash

    GET /api/v0/projectPermissions/

    HTTP 200 OK
    Vary: Accept
    Content-Type: text/html; charset=utf-8
    Allow: GET, POST, HEAD, OPTIONS

    {
        "count": 1,
        "next": null,
        "previous": null,
        "results": [
            {
                "id": 1,
                "user": 2,
                "project": 1,
                "canRead": true,
                "canWrite": false
            }
        ]
    }

Example Detail
--------------

.. code-block:: bash

    GET /api/v0/projectPermissions/1/

    HTTP 200 OK
    Vary: Accept
    Content-Type: text/html; charset=utf-8
    Allow: GET, PUT, DELETE, HEAD, OPTIONS, PATCH

    {
        "id": 1,
        "user": 2,
        "project": 1,
        "canRead": true,
        "canWrite": false
    }
