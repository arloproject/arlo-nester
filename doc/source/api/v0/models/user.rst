User
====

This is the auth_user model from the Django Authentication Framework. This
is read-only for Admin users, Forbidden for all other users.

**user/**
  | GET - List Users

**user/{Id}/**
  | GET - List User Detail

Example List
------------

.. code-block:: bash

    GET /api/v0/user/

    HTTP 200 OK
    Vary: Accept
    Content-Type: text/html; charset=utf-8
    Allow: GET, HEAD, OPTIONS

    {
        "count": 2,
        "next": null,
        "previous": null,
        "results": [
            {
                "id": 1,
                "username": "arlo",
                "groups": []
            },
            {
                "id": 2,
                "username": "user",
                "groups": []
            }
        ]
    }

UserSettings
============

This is the tools_usersettings model from Nester. Note that typically only
the 'default' named settings for each user are used, so creating a
separate UserSettings object is generally not useful.

**userSettings/**
  | GET - List all UserSettings for the User.
  | POST - Create new UserSettings for the auth'd user.

**userSettings/{Id}/**
  | GET - Retrieve Record
  | PUT - Update Record
  | DELETE - Delete Record

Example List
------------

.. code-block:: bash

    GET /api/v0/userSettings/

    HTTP 200 OK
    Vary: Accept
    Content-Type: text/html; charset=utf-8
    Allow: GET, POST, HEAD, OPTIONS

    {
        "count": 1,
        "next": null,
        "previous": null,
        "results": [
            {
                "id": 1,
                "user": 2,
                "name": "default",
                "windowSizeInSeconds": 4.0,
                "spectraMinimumBandFrequency": 20.0,
                "spectraMaximumBandFrequency": 20000.0,
                "spectraDampingFactor": 0.02,
                "spectraNumFrequencyBands": 256,
                "spectraNumFramesPerSecond": 128.0,
                "showSpectra": true,
                "showWaveform": true,
                "showPitchTrace": false,
                "loadAudio": true,
                "maxNumAudioFilesToList": 100,
                "transcriptFormat": "Enstrom",
                "fileViewWindowSizeInSeconds": 4.0,
                "fileViewSpectraMinimumBandFrequency": 20.0,
                "fileViewSpectraMaximumBandFrequency": 20000.0,
                "fileViewSpectraDampingFactor": 0.02,
                "fileViewSpectraNumFrequencyBands": 256,
                "fileViewSpectraNumFramesPerSecond": 128.0,
                "catalogViewWindowSizeInSeconds": 4.0,
                "catalogViewSpectraMinimumBandFrequency": 20.0,
                "catalogViewSpectraMaximumBandFrequency": 20000.0,
                "catalogViewSpectraDampingFactor": 0.02,
                "catalogViewSpectraNumFrequencyBands": 64,
                "catalogViewSpectraNumFramesPerSecond": 100.0,
                "tagViewWindowSizeInSeconds": 4.0,
                "tagViewSpectraMinimumBandFrequency": 20.0,
                "tagViewSpectraMaximumBandFrequency": 20000.0,
                "tagViewSpectraDampingFactor": 0.02,
                "tagViewSpectraNumFrequencyBands": 256,
                "tagViewSpectraNumFramesPerSecond": 128.0
            }
        ]
    }

Example Detail
--------------

.. code-block:: bash

    GET /api/v0/userSettings/1/

    HTTP 200 OK
    Vary: Accept
    Content-Type: text/html; charset=utf-8
    Allow: GET, PUT, DELETE, HEAD, OPTIONS, PATCH

    {
        "id": 1,
        "user": 2,
        "name": "default",
        "windowSizeInSeconds": 4.0,
        "spectraMinimumBandFrequency": 20.0,
        "spectraMaximumBandFrequency": 20000.0,
        "spectraDampingFactor": 0.02,
        "spectraNumFrequencyBands": 256,
        "spectraNumFramesPerSecond": 128.0,
        "showSpectra": true,
        "showWaveform": true,
        "showPitchTrace": false,
        "loadAudio": true,
        "maxNumAudioFilesToList": 100,
        "transcriptFormat": "Enstrom",
        "fileViewWindowSizeInSeconds": 4.0,
        "fileViewSpectraMinimumBandFrequency": 20.0,
        "fileViewSpectraMaximumBandFrequency": 20000.0,
        "fileViewSpectraDampingFactor": 0.02,
        "fileViewSpectraNumFrequencyBands": 256,
        "fileViewSpectraNumFramesPerSecond": 128.0,
        "catalogViewWindowSizeInSeconds": 4.0,
        "catalogViewSpectraMinimumBandFrequency": 20.0,
        "catalogViewSpectraMaximumBandFrequency": 20000.0,
        "catalogViewSpectraDampingFactor": 0.02,
        "catalogViewSpectraNumFrequencyBands": 64,
        "catalogViewSpectraNumFramesPerSecond": 100.0,
        "tagViewWindowSizeInSeconds": 4.0,
        "tagViewSpectraMinimumBandFrequency": 20.0,
        "tagViewSpectraMaximumBandFrequency": 20000.0,
        "tagViewSpectraDampingFactor": 0.02,
        "tagViewSpectraNumFrequencyBands": 256,
        "tagViewSpectraNumFramesPerSecond": 128.0
    }
