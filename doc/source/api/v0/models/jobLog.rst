JobLog
======

This is the tools_joblog model from Nester.

**jobLog/**
  | GET - List JobLogs to which the User has access.
  | POST - Add new JobLog

**jobLog/{Id}/**
  | GET - Retrieve Record
  | PUT - Update Record
  | DELETE - Delete Record

Example Detail
--------------

.. code-block:: bash

    GET /api/v0/jobLog/587/

    HTTP 200 OK
    Content-Type: application/json
    Vary: Accept
    Allow: GET, PUT, PATCH, DELETE, HEAD, OPTIONS

    {
        "id": 587,
        "job": 111867,
        "messageDate": "2014-11-20T00:56:02",
        "message": "Status Changed to 'Running' by 'bigd'"
    }
