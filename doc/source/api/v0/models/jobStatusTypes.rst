JobStatusTypes
==============

This is the tools_jobstatustypes model from Nester. This is read-only for
*any* authenticated user.

**jobStatusTypes/**
  | GET - List JobStatusTypes

**jobStatusTypes/{Id}/**
  | GET - JobStatusTypes Detail


Example List
------------

.. code-block:: bash

    GET /api/v0/jobStatusTypes/

    HTTP 200 OK
    Vary: Accept
    Content-Type: text/html; charset=utf-8
    Allow: GET, HEAD, OPTIONS

    {
        "count": 6,
        "next": null,
        "previous": null,
        "results": [
            {
                "id": 1,
                "name": "Unknown"
            },
            {
                "id": 2,
                "name": "Queued"
            },
            {
                "id": 3,
                "name": "Running"
            },
            {
                "id": 4,
                "name": "Error"
            },
            {
                "id": 5,
                "name": "Stopped"
            },
            {
                "id": 6,
                "name": "Complete"
            }
        ]
    }

Example Detail
--------------

.. code-block:: bash

    GET /api/v0/jobStatusTypes/1/

    HTTP 200 OK
    Vary: Accept
    Content-Type: text/html; charset=utf-8
    Allow: GET, HEAD, OPTIONS

    {
        "id": 1,
        "name": "Unknown"
    }
