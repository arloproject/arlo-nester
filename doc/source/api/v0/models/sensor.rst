Sensor
======

This is the tools_sensor model from Nester. 

**sensor/**
  | GET - List all Sensors to which the User has access.
  | POST - Create new Sensor for the auth'd user.

**sensor/{Id}/**
  | GET - Retrieve Record
  | PUT - Update Record
  | DELETE - Delete Record


Example List
------------

.. code-block:: bash

    GET /api/v0/sensor/

    HTTP 200 OK
    Vary: Accept
    Content-Type: text/html; charset=utf-8
    Allow: GET, POST, HEAD, OPTIONS

    {
        "count": 2,
        "next": null,
        "previous": null,
        "results": [
            {
                "id": 1,
                "sensorArray": 1,
                "name": "SensorA",
                "x": 1.0,
                "y": 2.0,
                "z": 3.0,
                "layoutRow": 0,
                "layoutColumn": 0
            },
            {
                "id": 2,
                "sensorArray": 1,
                "name": "SensorB",
                "x": 4.0,
                "y": 5.0,
                "z": 6.0,
                "layoutRow": 1,
                "layoutColumn": 1
            }
        ]
    }

Example Detail
--------------

.. code-block:: bash

    GET /api/v0/sensor/1/

    HTTP 200 OK
    Vary: Accept
    Content-Type: text/html; charset=utf-8
    Allow: GET, PUT, DELETE, HEAD, OPTIONS, PATCH

    {
        "id": 1, 
        "sensorArray": 1,
        "name": "SensorA",
        "x": 1.0,
        "y": 2.0,
        "z": 3.0,
        "layoutRow": 0,
        "layoutColumn": 0
    }


SensorArray
===========

This is the tools_sensorarray model from Nester.

**sensorArray/**
  | GET - List all SensorArrays to which the User has access.
  | POST - Create new SensorArray for the auth'd user.

**sensorArray/{Id}/**
  | GET - Retrieve Record
  | PUT - Update Record
  | DELETE - Delete Record


Example List
------------

.. code-block:: bash

    GET /api/v0/sensorArray/

    HTTP 200 OK
    Content-Type: application/json
    Vary: Accept
    Allow: GET, POST, HEAD, OPTIONS

    {
        "count": 1,
        "next": null,
        "previous": null,
        "results": [
            {
                "id": 6,
                "user": 18,
                "name": "tonyb-test-array",
                "layoutRows": 3,
                "layoutColumns": 3,
                "sensor_set": [
                    {
                        "id": 46,
                        "sensorArray": 6,
                        "name": "1",
                        "x": 0.0,
                        "y": 0.0,
                        "z": 0.0,
                        "layoutRow": 1,
                        "layoutColumn": 0
                    },
                    {
                        "id": 47,
                        "sensorArray": 6,
                        "name": "2",
                        "x": 0.0,
                        "y": 0.0,
                        "z": 0.0,
                        "layoutRow": 0,
                        "layoutColumn": 1
                    },
                    {
                        "id": 48,
                        "sensorArray": 6,
                        "name": "3",
                        "x": 0.0,
                        "y": 0.0,
                        "z": 0.0,
                        "layoutRow": 2,
                        "layoutColumn": 2
                    }
                ]
            }
        ]
    }

Example Detail
--------------

.. code-block:: bash

    GET /api/v0/sensorArray/6/

    HTTP 200 OK
    Content-Type: application/json
    Vary: Accept
    Allow: GET, PUT, PATCH, DELETE, HEAD, OPTIONS

    {
        "id": 6,
        "user": 18,
        "name": "tonyb-test-array",
        "layoutRows": 3,
        "layoutColumns": 3,
        "sensor_set": [
            {
                "id": 46,
                "sensorArray": 6,
                "name": "1",
                "x": 0.0,
                "y": 0.0,
                "z": 0.0,
                "layoutRow": 1,
                "layoutColumn": 0
            },
            {
                "id": 47,
                "sensorArray": 6,
                "name": "2",
                "x": 0.0,
                "y": 0.0,
                "z": 0.0,
                "layoutRow": 0,
                "layoutColumn": 1
            },
            {
                "id": 48,
                "sensorArray": 6,
                "name": "3",
                "x": 0.0,
                "y": 0.0,
                "z": 0.0,
                "layoutRow": 2,
                "layoutColumn": 2
            }
        ]
    }
