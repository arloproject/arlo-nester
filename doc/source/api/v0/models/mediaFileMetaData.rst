MediaFileMetaData
=================

This is the tools_mediafilemetadata model from Nester.

**mediaFileMetaData/**
  | GET - List all MediaFileMetaData to which the user has access.
  | POST - Create new MediaFileMetaData entry

**mediaFileMetaData/{Id}/**
  | GET - Retrieve Record
  | PUT - Update Record
  | DELETE - Delete Record

Example List
------------

.. code-block:: bash

    GET /api/v0/mediaFileMetaData/

    HTTP 200 OK
    Vary: Accept
    Content-Type: text/html; charset=utf-8
    Allow: GET, POST, HEAD, OPTIONS

    {
        "count": 26,
        "next": "http://arlo.local/api/v0/mediaFileMetaData/?page=2",
        "previous": null,
        "results": [
            {
                "id": 79,
                "mediaFile": 4,
                "name": "numSpecies",
                "value": "single",
                "userEditable": true
            },
            {
                "id": 80,
                "mediaFile": 4,
                "name": "microphonePlacement",
                "value": "",
                "userEditable": true
            },
     <!-- snip -->
        ]
    }

Example Detail
--------------

.. code-block:: bash

    GET /api/v0/mediaFileMetaData/79/

    HTTP 200 OK
    Vary: Accept
    Content-Type: text/html; charset=utf-8
    Allow: GET, PUT, DELETE, HEAD, OPTIONS, PATCH

    {
        "id": 79,
        "mediaFile": 4,
        "name": "numSpecies",
        "value": "single",
        "userEditable": true
    }
