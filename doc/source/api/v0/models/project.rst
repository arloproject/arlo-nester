Project
=======

This is the tools_project model from Nester. This will list Projects
to which the user has read or write access. Additionally, Admin users can
perform 'detail' actions against any individual project.

**project/**
  | GET - List Projects (Admin users only).
  | POST - Create new Project for the auth'd user.

**project/{Id}/**
  | GET - Retrieve Record
  | PUT - Update Record
  | DELETE - Delete Record

Example List
------------

.. code-block:: bash

    GET /api/v0/project/

    HTTP 200 OK
    Vary: Accept
    Content-Type: text/html; charset=utf-8
    Allow: GET, POST, HEAD, OPTIONS

    {
        "count": 1,
        "next": null,
        "previous": null,
        "results": [
            {
                "id": 1,
                "type": 2,
                "name": "testProject",
                "creationDate": "2014-01-10T15:59:13",
                "user": 1,
                "userSettings": 1
            }
        ]
    }

Example Detail
--------------

.. code-block:: bash

    GET /api/v0/project/1/

    HTTP 200 OK
    Vary: Accept
    Content-Type: text/html; charset=utf-8
    Allow: GET, PUT, DELETE, HEAD, OPTIONS, PATCH

    {
        "id": 1,
        "type": 2,
        "name": "testProject",
        "creationDate": "2014-01-10T15:59:13",
        "user": 1,
        "userSettings": 1
    }


Project MediaFiles
==================

List
----

Returns a list of MediaFiles in a Project. The output format is a list of
MediaFile objects as described elsewhere.

**project/{Id}/mediaFiles/**
  | GET - List MediaFiles in the Project

Example
^^^^^^^

.. code-block:: bash

    GET /api/v0/project/93/mediaFiles/

    {
        "count": 7,
        "next": null,
        "previous": null,
        "results": [
            {
                "id": 168675,
                "file": "1k_every10.wav",
                "user": 18,
                "type": 1,
                "active": true,
                "alias": "tonyb_test",
                "uploadDate": "2012-08-30T17:44:37",
                "hasTranscript": false,
                "sensor": null,
                "library": 69,
                "realStartTime": "2012-08-30T17:44:37",
                "mediafilemetadata_set": [
                    {
                        "id": 76,
                        "mediaFile": 6,
                        "name": "fileSize",
                        "value": "260396",
                        "userEditable": false
                    },
    <SNIP>
                ],
                "url": "https://media.amanda.arloproject.com/files/user-files/tonyb/1k_every10.wav"
            },
    <SNIP>
         ]
    }



Add
---

Add an existing MediaFile to a Project. This takes a dictionary with one key,
'id', that specifies the MediaFile Id to add.

If the MediaFile is already in the Project, a 200 status will be returned.

**project/{Id}/mediaFiles/add/**
  | POST - Add MediaFile to the Project

Example
^^^^^^^

.. code-block:: bash

    POST /api/v0/project/2/mediaFiles/add/

    {"id": 2}

    HTTP 201 Created
    Allow: POST, OPTIONS
    Content-Type: application/json
    Vary: Accept

    {
        "id": 2
    }


Remove
------

Remove a MediaFile from a Project. This takes a dictionary with one key,
'id', that specifies the MediaFile Id to remove.

If the MediaFile is not in the Project, a 200 status will be returned.

**project/{Id}/mediaFiles/remove/**
  | POST - Remove MediaFile from the Project

Example
^^^^^^^

.. code-block:: bash

    POST /api/v0/project/2/mediaFiles/remove/

    {"id": 2}

    HTTP 204 No Content
    Allow: POST, DELETE, OPTIONS
    Content-Type: application/json
    Vary: Accept
