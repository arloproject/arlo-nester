ArloPermission
==============

This is the *tools_arloPermission* model from Nester.

**arloPermission/**
  | GET - List ArloPermissions to which the User has access.
  | POST - Create new ArloPermission

**arloPermission/{Id}/**
  | GET - Retrieve Record
  | PUT - Update Record
  | DELETE - Delete Record


Example List
------------

.. code-block:: bash

    GET /api/v0/arloPermission/?page=2

    HTTP 200 OK
    Content-Type: application/json
    Vary: Accept
    Allow: GET, POST, HEAD, OPTIONS

    {
        "count": 7,
        "next": "https://amanda.arloproject.com/api/v0/arloPermission/?page=3",
        "previous": "https://amanda.arloproject.com/api/v0/arloPermission/",
        "results": [
            {
                "id": 22,
                "bearer_type": "user",
                "bearer_id": 1,
                "target_type": "tagset",
                "target_id": 34,
                "creationDate": "2016-08-14T13:17:36",
                "createdBy": 18,
                "read": true,
                "write": false,
                "launch_job": false,
                "admin": false
            },
            {
                "id": 31,
                "bearer_type": "user",
                "bearer_id": 90,
                "target_type": "arlousergroup",
                "target_id": 1,
                "creationDate": "2016-08-15T17:54:58",
                "createdBy": 18,
                "read": true,
                "write": true,
                "launch_job": true,
                "admin": true
            },
            {
                "id": 34,
                "bearer_type": "arlousergroup",
                "bearer_id": 1,
                "target_type": "library",
                "target_id": 69,
                "creationDate": "2016-08-27T21:38:37",
                "createdBy": 18,
                "read": true,
                "write": false,
                "launch_job": false,
                "admin": false
            }
        ]
    }

Example Detail
--------------

.. code-block:: bash

    GET /api/v0/arloPermission/3/

    HTTP 200 OK
    Content-Type: application/json
    Vary: Accept
    Allow: GET, PUT, PATCH, DELETE, HEAD, OPTIONS

    {
        "id": 3,
        "bearer_type": "user",
        "bearer_id": 18,
        "target_type": "library",
        "target_id": 69,
        "creationDate": "2016-08-07T14:16:14",
        "createdBy": 18,
        "read": true,
        "write": true,
        "launch_job": false,
        "admin": false
    }
