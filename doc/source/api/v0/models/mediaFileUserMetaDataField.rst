MediaFileUserMetaDataField
==========================

This is the tools_mediafileusermetadatafield model from Nester.

**mediaFileUserMetaDataField/**
  | GET - List all MediaFileUserMetaDataFields in all Libraries to which the user has access.
  | POST - Create new MediaFileUserMetaDataField entry

**mediaFileUserMetaDataField/{Id}/**
  | GET - Retrieve Record
  | PUT - Update Record
  | DELETE - Delete Record

Example List
------------

.. code-block:: bash

    GET /api/v0/mediaFileUserMetaDataField/

    HTTP 200 OK
    Content-Type: application/json
    Vary: Accept
    Allow: GET, POST, HEAD, OPTIONS

    {
        "count": 59,
        "next": "http://amanda.arloproject.com:9000/api/v0/mediaFileUserMetaDataField/?page=2",
        "previous": null,
        "results": [
            {
                "id": 1,
                "name": "testField",
                "library": 69,
                "description": "Test Field"
            },
            {
                "id": 2,
                "name": "testField2",
                "library": 69,
                "description": "Test Field 2"
            },
     <!-- snip -->
        ]
    }

Example Detail
--------------

.. code-block:: bash

    GET /api/v0/mediaFileUserMetaDataField/1/

    HTTP 200 OK
    Content-Type: application/json
    Vary: Accept
    Allow: GET, PUT, PATCH, DELETE, HEAD, OPTIONS

    {
        "id": 1,
        "name": "testField",
        "library": 69,
        "description": "Test Field"
    }
