MediaFile
=========

This is the tools_mediafile model from Nester. This allows read-only and
limited update operations, but not create. This will show all MediaFiles
to which a user has access, and Admin users can access any detail item.

.. note::

    The 'url' field will possibly be deprecated, the new 'file' field should
    be used instead for accessing the file.


**mediaFile/**
  | GET - List MediaFiles to which the User has access.

**mediaFile/{Id}/**
  | GET - Retrieve Record
  | DELETE - Delete a MediaFile

Example List
------------

.. code-block:: bash

    GET /api/v0/mediaFile/

    HTTP 200 OK
    Content-Type: application/json
    Vary: Accept
    Allow: GET, POST, HEAD, OPTIONS

    {
        "count": 42126,
        "next": "http://amanda.arloproject.com:9000/api/v0/mediaFile/?page=2",
        "previous": null,
        "results": [
            {
                "id": 168675,
                "file": "https://<!--- SNIP -->/arlo.wav",
                "user": 18,
                "type": 1,
                "active": true,
                "alias": "tonyb_test",
                "uploadDate": "2012-08-30T17:44:37",
                "hasTranscript": false,
                "sensor": null,
                "library": 69,
                "realStartTime": "2012-08-30T17:44:37",
                "mediafilemetadata_set": [
                    {
                        "id": 76,
                        "mediaFile": 6,
                        "name": "fileSize",
                        "value": "260396",
                        "userEditable": false
                    },
     <!-- snip -->
                    {
                        "id": 2422981,
                        "mediaFile": 168675,
                        "name": "columns",
                        "value": "",
                        "userEditable": true
                    }
                ],
                "url": "https://<!--- SNIP -->/arlo.wav"
            },
            {
     <!-- snip -->
            }
        ]
    }

Example Detail
--------------

.. code-block:: bash

    GET /api/v0/mediaFile/179070/

    HTTP 200 OK
    Content-Type: application/json
    Vary: Accept
    Allow: GET, PUT, DELETE, HEAD, OPTIONS, PATCH

    {
        "id": 179070,
        "file": "https://<!--- SNIP -->/arlo.wav",
        "user": 18,
        "type": 1,
        "active": true,
        "alias": "tom's_diner",
        "uploadDate": "2013-04-19T00:00:00",
        "hasTranscript": false,
        "sensor": null,
        "library": 69,
        "mediafilemetadata_set": [
            {
                "id": 2422982,
                "mediaFile": 179070,
                "name": "fileSize",
                "value": "45693432",
                "userEditable": false
            },
     <!-- snip -->
            {
                "id": 2422995,
                "mediaFile": 179070,
                "name": "durationInSeconds",
                "value": "228.46694",
                "userEditable": false
            }
        ],
        "url": "https://<!--- SNIP -->/arlo.wav"
    }

Example Delete
--------------

.. code-block:: bash

    DELETE /api/v0/mediaFile/223395/

    HTTP 204 No Content
    Allow: GET, PUT, PATCH, DELETE, OPTIONS
    Content-Type: application/json
    Vary: Accept


MediaFile UserMetaData
======================

Returns a list of UserMetaData for a MediaFile. The output format is a list
of MediaFileUserMetaData objects as described elsewhere.

**mediaFile/{Id}/userMetaData/**
  | GET - List UserMetaData for the MediaFile

Example List
------------

.. code-block:: bash

    GET /api/v0/mediaFile/218046/userMetaData/

    HTTP 200 OK
    Content-Type: application/json
    Vary: Accept
    Allow: OPTIONS, GET

    {
        "count": 1,
        "next": null,
        "previous": null,
        "results": [
            {
                "id": 8,
                "mediaFile": 218046,
                "metaDataField": 5,
                "value": "Frank Bidart",
                "metaDataFieldName": "subjectName"
            }
        ]
    }


MediaFile Projects
==================

List
----

Returns a list of Projects which contain a MediaFile. The output format is a
list of Project objects as described elsewhere.

**mediaFile/{Id}/projects/**
  | GET - List Projects which contain the MediaFile

.. note::
    MediaFiles are not added or removed from Projects with this endpoint.
    Instead, use the Project MediaFile endpoints
    (e.g., /projects/{Id}/mediaFiles/add)

Example
^^^^^^^

.. code-block:: bash

    GET /api/v0/mediaFile/168675/projects/

    HTTP 200 OK
    Allow: GET, OPTIONS
    Content-Type: application/json
    Vary: Accept

    {
        "count": 1,
        "next": null,
        "previous": null,
        "results": [
            {
                "id": 93,
                "user": 18,
                "type": 1,
                "name": "tonyb_test_lib",
                "mediaFiles": [
                    168675,
                    168681,
                    168682,
                    179070,
                    223579,
                    223580
                ],
                "creationDate": "2011-10-23T11:17:30",
                "notes": "TonyB's Test / Development Project. ",
                "default_permission_read": false,
                "default_permission_write": false,
                "default_permission_launch_job": false,
                "default_permission_admin": false
            }
        ]
    }
