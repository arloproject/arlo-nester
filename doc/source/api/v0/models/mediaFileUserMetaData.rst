MediaFileUserMetaData
=====================

This is the tools_mediafileusermetadata model from Nester.

**mediaFileUserMetaData/**
  | GET - List all MediaFileUserMetaData in all Libraries to which the user has access.
  | POST - Create new MediaFileUserMetaData entry

**mediaFileUserMetaData/{Id}/**
  | GET - Retrieve Record
  | PUT - Update Record
  | DELETE - Delete Record

Example List
------------

.. code-block:: bash

    GET /api/v0/mediaFileUserMetaData/

    HTTP 200 OK
    Content-Type: application/json
    Vary: Accept
    Allow: GET, POST, HEAD, OPTIONS

    {
        "count": 139032,
        "next": "http://amanda.arloproject.com:9000/api/v0/mediaFileUserMetaData/?page=2",
        "previous": null,
        "results": [
            {
                "id": 31,
                "mediaFile": 179070,
                "metaDataField": 1,
                "value": "testEntry1",
                "metaDataFieldName": "testField"
            },
            {
                "id": 32,
                "mediaFile": 179070,
                "metaDataField": 9,
                "value": "new1",
                "metaDataFieldName": "newField"
            },
     <!-- snip -->
        ]
    }

Example Detail
--------------

.. code-block:: bash

    GET /api/v0/mediaFileUserMetaData/3/

    HTTP 200 OK
    Content-Type: application/json
    Vary: Accept
    Allow: GET, PUT, PATCH, DELETE, HEAD, OPTIONS

    {
        "id": 31,
        "mediaFile": 179070,
        "metaDataField": 1,
        "value": "testEntry1",
        "metaDataFieldName": "testField"
    }
