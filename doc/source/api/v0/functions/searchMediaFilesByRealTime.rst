searchMediaFilesByRealTime
==========================

Search for MediaFiles that are within a specified Time Period (based on the
real "startTime" metadata for the file). This is namely used for the
MultiView interface.

**searchMediaFilesByRealTime/{libraryId}/**
  | GET

    - Parameters

      - startDate
          **(required)**: Formatted as "yyyy-mm-dd hh:mm:ss"
      - endDate
          **(required)**: Formatted as "yyyy-mm-dd hh:mm:ss"

    - Response

      - A JSON list of Dictionaries describing the found MediaFiles

        - id
        - alias
        - realStartTime
        - sensorId
        - sensorArrayId

Example
-------

.. code-block:: bash

    GET /api/v0/searchMediaFilesByRealTime/12/?startDate=2010-09-28%2019:48:53&endDate=2010-09-28%2019:48:58

    [
        {
            "alias": "Inner_SE_20100928_1900_00.wav",
            "id": 25052,
            "realStartTime": "2010-09-28T19:00:00",
            "sensorArrayId": "1",
            "sensorId": "3"
        },
        {
            "alias": "Inner_NW_20100928_1900_00.wav",
            "id": 25057,
            "realStartTime": "2010-09-28T19:00:00",
            "sensorArrayId": "1",
            "sensorId": "4"
        }
    ]
