generateSpectraImage
====================

Generate Spectra Images and Audio File Segments from a MediaFile.

.. note::
    If the queried endTime is greater than the length of the file, the
    endTime will be updated to the length of the file and returned in the
    parameters.

**generateSpectraImage/{mediaFileId}/**
  | POST

    - Parameters

      - startTime
          in seconds, relative to the beginning of the file.
      - endTime
          in seconds, relative to the beginning of the file. If this is
          greater than the length of the file, then data will be returned up
          until the end of the file.
      - numTimeFramesPerSecond
      - numFrequencyBands
      - dampingFactor
      - minFrequency
          (in Hz)
      - maxFrequency
          (in Hz)
      - gain
          (optional, default 1.0)

    - Response

      - A JSON dictionary of

        - imageUrl
        - audioSegmentUrl
        - params
        - imageHeight
        - imageWidth

Example
-------

Example Parameters
~~~~~~~~~~~~~~~~~~

.. code-block:: bash

    {
        "startTime": 0.0,
        "endTime": 5.0,
        "minFrequency": 20.0,
        "maxFrequency": 20000.0,
        "numFrequencyBands": 32,
        "numTimeFramesPerSecond": 20,
        "dampingFactor": 0.02,
        "gain": 1
    }

Example Response
~~~~~~~~~~~~~~~~

.. code-block:: bash

    POST /api/v0/generateSpectraImage/179070/

    HTTP 200 OK
    Content-Type: application/json
    Vary: Accept
    Allow: POST, OPTIONS

    {
        "imageUrl": "https://media.amanda.arloproject.com/files/<!-- SNIP -->.jpg",
        "audioSegmentUrl": "https://media.amanda.arloproject.com/files/<!-- SNIP -->.segment.wav",
        "params": {
            "minFrequency": 20.0,
            "numTimeFramesPerSecond": 20,
            "dampingFactor": 0.02,
            "numFrequencyBands": 32,
            "showFrequencyScale": true,
            "showTimeScale": true,
            "maxFrequency": 20000.0,
            "catalogSpectraNumFrequencyBands": null,
            "mediaFile": 179070,
            "topBorderHeight": 30,
            "simpleNormalization": true,
            "gain": 1.0,
            "startTime": 0.0,
            "timeScaleHeight": 22,
            "endTime": 5.0,
            "borderWidth": 50
        },
        "imageHeight": 84,
        "imageWidth": 200.0
    }
