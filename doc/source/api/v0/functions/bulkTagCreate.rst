bulkTagCreate
=============

This function allows the creation of multiple Tag/TagExample objects in one
request.

.. note::
    This function breaks the pattern of most other functions in this API so deserves special attention.

Success of this function is "all-or-nothing". If any errors are encountered
during the request, none of the tags have been saved to the database.
A success (HTTP 2xx) indicates all Tags have been saved.

An example implementation is available at
https://bitbucket.org/arloproject/arlo-scripts/src/master/segment_mediafile/

.. note::
    There is currently no limit on the number of Tags that can be generated
    at once, pending investigation of usage patterns and performance. For now,
    I would recommend limiting to 100 per request, as any future limit will
    likely be well above this.

.. note::
    System Users (Admins) may specify an additional 'user' parameter on the
    Tag dictionaries to override the user for the created Tags.

.. note::
    Some clients may not allow passing a List of Dictionaries as POST data
    (such as the built-in Django API Test Client) as they instead require
    a Dictionary. In such cases, an optional Dictionary of {'tags': values}
    may be specified, where 'values' is a JSON rendered string of the List
    of Dictionaries.

.. note::
    While optional, at least one of "machineTagged" or "userTagged" *SHOULD* be
    set in normal use.

**bulkTagCreate/{projectId}/**
  | POST

    - Parameters

      - A list of dictionaries containing

        - tagSet
            TagSet ID into which to save these Tags
        - mediaFile
            MediaFile ID 
        - startTime
            in seconds from start of file
        - endTime
            in seconds from start of file
        - minFrequency
        - maxFrequency
        - tagClass
            tagClass ID for this Tag
        - parentTag
            (optional) Tag ID of the source 'parent' Tag
        - randomlyChosen
            (optional) Set the 'randomlyChosen' flag on the Tag
        - machineTagged
            (optional) Set the 'machineTagged' flag on the Tag
        - userTagged
            (optional) Set the 'userTagged' flag on the Tag
        - strength
            Strength index for the Tag. Application / source specific, but
            typically between 0.0 and 1.0

    - Response

      - This function currently returns an HTTP 200 response code on success.
        A 201 or 204 response may be more appropriate and may be used in the
        future, so checking of response status should check for any 2xx class
        Successful status.

.. note::
    Be sure to set Content-Type to 'application/json' when providing raw JSON
    as below.

Example
-------

Example Data
~~~~~~~~~~~~

.. code-block:: bash

    [
        {
            "tagSet": 769,
            "mediaFile": 179070,
            "startTime": 0.5,
            "endTime": 1.5,
            "minFrequency": 20,
            "maxFrequency": 20000,
            "tagClass": 6849,
            "parentTag": 1976960,
            "randomlyChosen": false,
            "machineTagged": true,
            "userTagged": false,
            "strength": 1
        },
        {
            "tagSet": 769,
            "mediaFile": 179070,
            "startTime": 2,
            "endTime": 3,
            "minFrequency": 20,
            "maxFrequency": 20000,
            "tagClass": 6849,
            "userTagged": true,
            "strength": 0.01
        }
    ]

Example Response
~~~~~~~~~~~~~~~~

.. code-block:: bash

    POST /api/v0/bulkTagCreate/93/
    HTTP 200 OK
    Content-Type: application/json
    Vary: Accept
    Allow: POST, OPTIONS

    "OK"
