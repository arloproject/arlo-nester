getTagsInAudioSegment
==========================

Search for Tags within a specified time and MediaFile (This is commonly used
for the visualization interfaces to retrieve Tags).

**getTagsInAudioSegment/{projectId}/**
  | GET

    - Parameters

      - mediaFile
          MediaFile ID to search.
      - startTime
          (optional) in seconds from start of file - filter Tags based on
          position within the file
      - endTime
          (optional) in seconds from start of file - filter Tags based on
          position within the file
      - restrictTagSets
          (optional) a list of TagSet IDs in which to search for Tags. Note
          that since this is an HTTP GET parameter, multiple options are
          provided by passing the parameters multiple times
          (e.g., "?...&restrictTagSets=845&restrictTagSets=510")

    - Response

      - A JSON list of Dictionaries describing the found Tags

        - id
        - userId
        - username
        - className
        - displayName
        - projectId
        - tagSetId
        - mediaFileId
        - mediaFileAlias
        - startTime
        - endTime
        - realTime
        - minFrequency
        - maxFrequency
        - userTagged
        - machineTagged
        - strength

Example
-------

.. code-block:: bash

    GET /api/v0/getTagsInAudioSegment/93/?mediaFile=179070&startTime=0&endTime=.9

    [
        {
            "className": "test",
            "displayName": "test",
            "endTime": 1.3984375,
            "id": 1976960,
            "machineTagged": false,
            "maxFrequency": 5189.1054428080297,
            "mediaFileAlias": "tom's_diner",
            "mediaFileId": 179070,
            "minFrequency": 763.94350985673304,
            "projectId": 93,
            "realTime": "",
            "startTime": 0.78125,
            "strength": 1.0,
            "tagSetId": 86,
            "userId": 18,
            "userTagged": true,
            "username": "tonyb"
        }
    ]
