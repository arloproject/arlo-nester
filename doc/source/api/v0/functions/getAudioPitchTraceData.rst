getAudioPitchTraceData
======================

Compute and return the Audio PitchTrace Data from a MediaFile


.. note::
    Since the returned frame data is a dictionary, sort order is not retained,
    as in the example below.

.. note::
    If the queried endTime is greater than the length of the file, the
    endTime will be updated to the length of the file and returned in the
    parameters as in the example below.

**getAudioPitchTraceData/{mediaFileId}/**
  | GET

    - Parameters

      - startTime
          in seconds, relative to the beginning of the file.
      - endTime
          in seconds, relative to the beginning of the file. If this is
          greater than the length of the file, then data will be returned
          up until the end of the file.
      - numTimeFramesPerSecond
      - numFrequencyBands
      - dampingFactor
      - minFrequency
          (in Hz)
      - maxFrequency
          (in Hz)

    - Response

      - The JSON 'spectraData' object returned from the APAPT callMe() API.

Example
-------

.. code-block:: bash

    GET /api/v0/getAudioPitchTraceData/179070/?startTime=227&endTime=300&numTimeFramesPerSecond=2&numFrequencyBands=4&dampingFactor=0.02&minFrequency=20&maxFrequency=2000

    HTTP 200 OK
    Content-Type: application/json
    Vary: Accept
    Allow: OPTIONS, GET

    {
        "spectraData": {
            "frames": {
                "227.0": [
                    2582,
                    42785,
                    9456,
                    9167
                ],
                "228.0": [
                    131,
                    282,
                    622,
                    891
                ],
                "227.5": [
                    973,
                    3303,
                    1985,
                    2048
                ]
            },
            "freqBands": [
                20.0,
                92.83177667225559,
                430.8869380063768,
                2000.0000000000005
            ],
            "params": {
                "minFrequency": 20.0,
                "numTimeFramesPerSecond": 2,
                "numFrequencyBands": 4,
                "maxFrequency": 2000.0,
                "dampingRatio": 0.02,
                "startTime": 227.0,
                "endTime": 228.46694,
                "mediaFileId": 179070
            }
        }
    }
