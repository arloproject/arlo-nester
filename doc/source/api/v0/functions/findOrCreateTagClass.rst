findOrCreateTagClass
====================

Search for an existing TagClass, or creates a new class in one request.

**findOrCreateTagClass/{projectId}/**
  | POST

    - Parameters

      - tagClassName
          TagClass Name to retrieve or create.

    - Response

      - Fields

        - id
        - user
        - project
        - className
        - displayName

      - Status

        - HTTP 200 If an existing TagClass is returned.
        - HTTP 201 If a new TagClass is created.

Example
-------

.. code-block:: bash

    POST /api/v0/findOrCreateTagClass/93/

    HTTP 201 CREATED
    Content-Type: application/json
    Vary: Accept
    Allow: POST, OPTIONS

    {
        "id": 59856,
        "user": 18,
        "project": 93,
        "className": "test-new-class",
        "displayName": "test-new-class"
    }
