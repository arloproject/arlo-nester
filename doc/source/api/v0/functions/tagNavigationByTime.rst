tagNavigationByTime
===================

Search for Tags within a Project based on 'Real'-Time relations.

**tagNavigationByTime/{projectId}/**
  | GET

    - Parameters

      - relation:

          - 'first' or 'last'
              search for the respective Tag, based on it's 'Real' Time.
          - 'next' or 'previous'
              search for the respective Tag from a given 'date'

      - tagSets
          List of Database IDs of the TagSets in which to search
      - tagClass
          Database ID of the TagClass in which to search
      - date
          (Optional) If searching for 'next' or 'previous', the date from
          which to search, formatted as "yyyy-mm-dd hh:mm:ss". Not required
          for 'first' or 'last' searches.

    - Response

      - A JSON Dictionary describing the found Tag

        - tagId
        - tagExampleId
        - realStartTime
        - duration
        - mediaFileId

Example
-------

.. code-block:: bash

    GET /api/v0/tagNavigationByTime/93/?relation=next&tagClass=6849&tagSets=86&date=2010-01-01%2000:00:00

    HTTP 200 OK
    Content-Type: application/json
    Vary: Accept
    Allow: OPTIONS, GET

    {
        "tagId": 1977200,
        "tagExampleId": 1999569,
        "realStartTime": "2012-08-30 17:44:46",
        "duration": 1.209999999999999,
        "mediaFileId": 168675
    }
