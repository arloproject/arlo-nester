getQueuedJob
============

Get a Queued Job from the Database and assign it to the requesting client. If
a *Job* matching the supplied parameters is found, it's Status is set to
"Assigned" and the Job details returned to the Client.

**getQueuedJob/**
  | POST

    - Parameters

      - jobTypes
          **(required)**: A list of *JobStatusType* ids to consider for
          assignment.
      - jobPluginNames
          **(optional)**: List of JobPluginNames which the client can process,
          required if the PluginType is provided.

    - Response

      - If a Job is found and Assigned, an HTTP 200 Response of a *Job*
        Serializer, a dictionary describing the assigned Job.
      - If no matching Job is found, an HTTP 204 NO CONTENT is returned.

.. note::
    *jobTypes* and *jobPluginNames* MUST be provided as a List, even if only
    a single value is provided.


Example
-------

.. code-block:: bash

    POST /api/v0/getQueuedJob/

    {
        "id": 3,
        "name": "Test1",
        "user": 2,
        "project": 1,
        "type": 15,
        "creationDate": "2017-01-14T22:23:41",
        "numToComplete": 0,
        "numCompleted": 0,
        "fractionCompleted": 0.0,
        "elapsedRealTime": 0.0,
        "timeToCompletion": -1.0,
        "isRunning": false,
        "wasStopped": false,
        "isComplete": false,
        "wasDeleted": false,
        "status": 3,
        "requestedStatus": null,
        "startedDate": null,
        "lastStatusDate": null,
        "parentJob": null,
        "priority": 0,
        "jobparameters_set": [
            {
                "id": 1,
                "job": 3,
                "name": "PluginName",
                "value": "TestJobPlugin"
            }
        ]
    }
