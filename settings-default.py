# Django settings for nester project.
import os
import logging

logging.basicConfig(
  level = logging.DEBUG,
  format = '%(asctime)s %(levelname)s %(message)s',
)

logging.info("executing settings.py")

# The absolute directory where nester is installed
# ex. NESTER_ROOT = '/home/user/nester/'
NESTER_ROOT = '/home/sjohns/workspace/nester/'

# The host IP address
# ex. APACHE_ROOT = 'http://nester.lis.uiuc.edu:80/'
APACHE_ROOT = 'http://nester.lis.uiuc.edu/'

# The prefix appended to the APACHE_ROOT to get to files served by Django
# ex. URL_PREFIX = '/tools/'
URL_PREFIX = ''
LOGIN_URL = URL_PREFIX + '/accounts/login/'

# The IP address of the Meandre Server.
MEANDRE_SERVICE_HEAD_IP = 'http://nester.lis.uiuc.edu'
# The port Meandre Server is running on as string.
MEANDRE_SERVICE_HEAD_PORT = '1725'

DEBUG = True
TEMPLATE_DEBUG = DEBUG
USE_STATIC_SERVE = True

ADMINS = (
    # ('Your Admin', 'admin@yoursite.com'),
)

MANAGERS = ADMINS

DATABASE_ENGINE = 'mysql'           # 'postgresql_psycopg2', 'postgresql', 'mysql', 'sqlite3' or 'oracle'.
DATABASE_NAME = 'nester_sam'             # Or path to database file if using sqlite3.
DATABASE_USER = 'sjohns'             # Not used with sqlite3.
DATABASE_PASSWORD = 'lock16'         # Not used with sqlite3.
DATABASE_HOST = ''             # Set to empty string for localhost. Not used with sqlite3.
DATABASE_PORT = ''             # Set to empty string for default. Not used with sqlite3.

# Local time zone for this installation. Choices can be found here:
# http://en.wikipedia.org/wiki/List_of_tz_zones_by_name
# although not all choices may be available on all operating systems.
# If running in a Windows environment this must be set to the same as your
# system time zone.
TIME_ZONE = 'America/Chicago'

# Language code for this installation. All choices can be found here:
# http://www.i18nguy.com/unicode/language-identifiers.html
LANGUAGE_CODE = 'en-us'

SITE_ID = 1

# If you set this to False, Django will make some optimizations so as not
# to load the internationalization machinery.
USE_I18N = True

# Absolute path to the directory that holds media.
# Example: "/home/media/media.lawrence.com/"
#MEDIA_ROOT = '/home/sjohns/workspace/nester/media
MEDIA_ROOT = os.path.join(NESTER_ROOT, 'media/')
USER_FILE_ROOT = os.path.join(MEDIA_ROOT,'files/user-files/')

FILE_UPLOAD_TEMP_DIR = os.path.join(MEDIA_ROOT, 'files/tmp/')

# URL that handles the media served from MEDIA_ROOT. Make sure to use a
# trailing slash if there is a path component (optional in other cases).
# Examples: "http://media.lawrence.com", "http://example.com/media/"
MEDIA_URL = ''

# URL prefix for admin media -- CSS, JavaScript and images. Make sure to use a
# trailing slash.
# Examples: "http://foo.com/media/", "/media/".
#ADMIN_MEDIA_PREFIX = '/media/'
ADMIN_MEDIA_PREFIX = '/media/'

# Make this unique, and don't share it with anybody.
SECRET_KEY = 're@2!_tj(uzorgw+ytb)2@b!t59ipd*a@ere#anf&9j)d%4$sj'

# List of callables that know how to import templates from various sources.
TEMPLATE_LOADERS = (
    'django.template.loaders.filesystem.load_template_source',
    'django.template.loaders.app_directories.load_template_source',
#     'django.template.loaders.eggs.load_template_source',
)

MIDDLEWARE_CLASSES = (
    'django.middleware.common.CommonMiddleware',
    'django.contrib.sessions.middleware.SessionMiddleware',
    'django.contrib.auth.middleware.AuthenticationMiddleware',
    'djangologging.middleware.LoggingMiddleware',
)

ROOT_URLCONF = 'nester.urls'

TEMPLATE_DIRS = (
    os.path.join(NESTER_ROOT,"templates"),
    # Put strings here, like "/home/html/django_templates" or "C:/www/django/templates".
    # Always use forward slashes, even on Windows.
    # Don't forget to use absolute paths, not relative paths.
)

INSTALLED_APPS = (
    'django.contrib.auth',
    'django.contrib.contenttypes',
    'django.contrib.sessions',
    'django.contrib.sites',
    'django.contrib.admin',
    'nester.tools',
)

