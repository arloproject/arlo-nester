import csv

from django.contrib.auth.decorators import login_required
from django.http import HttpResponse

from tools.models import Project, TagSet, Tag
from poetrytagging.models import TagClassification


## Export Tag Classifications as a CSV.
# @param request The Django HTTP request object.
# @param projectId The Database Id of the selected Project.
# @param tagSetId The Database Id of the selected TagSet.
# @param requestUserOnly Export only the request user's classifications if True, all classifications otherwise.
# @return Django HTTP response object.

@login_required
def exportClassifications(request, projectId, tagSetId, requestUserOnly):
  user = request.user

  project = Project.objects.get(id=projectId)
  tagSet  =  TagSet.objects.get(id=tagSetId)

  ###
  # Setup CSV Export

  response = HttpResponse(content_type='text/csv')
  sFilename = "tagClassifications-%d-%d.csv" % (project.id, tagSet.id)
  response['Content-Disposition'] = 'attachment; filename="' + sFilename + '"'

  writer = csv.writer(response)
  writer.writerow([
    'userName',
    'tag_id',
    'dateClassified',
    'tempo',
    'tempoChange',
    'rhythm',
    'loudness',
    'loudnessChange',
    'pitch',
    'pitchRange',
    'scandent',
    'tension',
    'articulation',

    'whisp', 
    'breath', 
    'husk', 
    'creak', 
    'fals', 
    'reson', 
    'giggle', 
    'laugh', 
    'trem', 
    'sob', 
    'yawn', 
    'sigh',

    'notPoetry',

    'silent',
    'chatter',
    'applause', 
    'laughter', 
    'music', 
    'notRatable', 
    ])


  # Get Tags in TagSet
  tags = Tag.objects.filter(tagSet=tagSet).order_by("id")
  tagIds = []
  for tag in tags:
    tagIds.append(tag.id)

  tagClassifications = TagClassification.objects.all()
  if requestUserOnly:
    tagClassifications = tagClassifications.filter(user=user)

  for tc in tagClassifications:
    if tc.id in tagIds:
      writer.writerow([
        tc.user.username,
        tc.id,
        tc.dateClassified,
        tc.tempo,
        tc.tempoChange,
        tc.rhythm,
        tc.loudness,
        tc.loudnessChange,
        tc.pitch,
        tc.pitchRange,
        tc.scandent,
        tc.tension,
        tc.articulation,
        
        tc.whisp,
        tc.breath,
        tc.husk,
        tc.creak,
        tc.fals,
        tc.reson,
        tc.giggle,
        tc.laugh,
        tc.trem,
        tc.sob,
        tc.yawn,
        tc.sigh,
        
        tc.notPoetry,
        
        tc.silent,
        tc.chatter,
        tc.applause,
        tc.laughter,
        tc.music,
        tc.notRatable,
        ])

  return response
