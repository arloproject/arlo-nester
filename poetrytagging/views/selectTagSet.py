from django.contrib.auth.decorators import login_required
from django.http import HttpResponseNotAllowed
from django.shortcuts import render, redirect
from django import forms
from django.conf import settings

from tools.models import Project, TagSet


class SelectTagSetForm(forms.Form):
  tagSet = forms.ModelChoiceField(TagSet.objects.none(), required=True)

## Select a TagSet to use for the Poetry Tagging
# @param request The Django HTTP request object.
# @param projectId The Database Id of the selected Project.
# @return Django HTTP response object.

@login_required
def selectTagSet(request, projectId):
  user = request.user
  project = Project.objects.get(id=projectId)

  if request.method == 'GET':
    form = SelectTagSetForm()
    form.fields['tagSet'] = forms.ModelChoiceField(TagSet.objects.filter(project=project).order_by('name'), required=True)

    return render(request, 'selectTagSet.html', {
      'apacheRoot':settings.APACHE_ROOT,
      'user':user,
      'form':form,
      })

  elif request.method == 'POST':
    form = SelectTagSetForm(request.POST)
    form.fields['tagSet'] = forms.ModelChoiceField(TagSet.objects.filter(project=project).order_by('name'), required=True)

    if not form.is_valid():
      return render(request, 'selectTagSet.html', {
        'apacheRoot':settings.APACHE_ROOT,
        'user':user,
        'form':form,
        })

    data = form.cleaned_data
    tagSet = data['tagSet']

    return redirect('startTagging', projectId=project.id, tagSetId=tagSet.id)


  else:
    return HttpResponseNotAllowed(['GET', 'POST'])
