import logging
import urllib

from django.contrib.auth.decorators import login_required
from django.http import HttpResponseRedirect, HttpResponseNotAllowed, HttpResponseServerError
from django.shortcuts import render, redirect
from django import forms
from django.core.urlresolvers import reverse
from django.conf import settings

from tools.models import TagSet, UserSettings, Tag
from tools.views.spectra import SpectraImageSettings, SpectraImage
from poetrytagging.models import TagClassification


class ClassifyTagForm(forms.Form):

  tempo          = forms.ChoiceField(widget=forms.RadioSelect, choices=TagClassification.TEMPO_CHOICES)
  tempoChange    = forms.ChoiceField(widget=forms.RadioSelect, choices=TagClassification.TEMPOCHANGE_CHOICES, initial=TagClassification.TEMPOCHANGE_CHOICES[0][0])
  rhythm         = forms.ChoiceField(widget=forms.RadioSelect, choices=TagClassification.RHYTHM_CHOICES)
  loudness       = forms.ChoiceField(widget=forms.RadioSelect, choices=TagClassification.LOUDNESS_CHOICES)
  loudnessChange = forms.ChoiceField(widget=forms.RadioSelect, choices=TagClassification.LOUDNESSCHANGE_CHOICES, initial=TagClassification.LOUDNESSCHANGE_CHOICES[0][0])
  pitch          = forms.ChoiceField(widget=forms.RadioSelect, choices=TagClassification.PITCH_CHOICES)
  pitchRange     = forms.ChoiceField(widget=forms.RadioSelect, choices=TagClassification.PITCHRANGE_CHOICES, initial=TagClassification.PITCHRANGE_CHOICES[0][0])

  scandent       = forms.BooleanField(initial=False, required=False)

  tension        = forms.ChoiceField(widget=forms.RadioSelect, choices=TagClassification.TENSION_CHOICES)
  articulation   = forms.ChoiceField(widget=forms.RadioSelect, choices=TagClassification.ARTICULATION_CHOICES, initial=TagClassification.ARTICULATION_CHOICES[0][0])

  # voice
#  voice          = forms.ChoiceField(widget=forms.RadioSelect, choices=TagClassification.VOICE_CHOICES)
  whisp  = forms.BooleanField(initial=False, required=False, label="Whisper")
  breath = forms.BooleanField(initial=False, required=False, label="Breathy")
  husk   = forms.BooleanField(initial=False, required=False, label="Husky")
  creak  = forms.BooleanField(initial=False, required=False, label="Creaky")
  fals   = forms.BooleanField(initial=False, required=False, label="Falsetto")
  reson  = forms.BooleanField(initial=False, required=False, label="Resonant")
  giggle = forms.BooleanField(initial=False, required=False, label="Unvoiced Laugh or Giggle")
  laugh  = forms.BooleanField(initial=False, required=False, label="Voiced Laugh")
  trem   = forms.BooleanField(initial=False, required=False, label="Tremulous")
  sob    = forms.BooleanField(initial=False, required=False, label="Sobbing")
  yawn   = forms.BooleanField(initial=False, required=False, label="Yawning")
  sigh   = forms.BooleanField(initial=False, required=False, label="Sighing")


  notPoetry      = forms.BooleanField(initial=False, required=False, label="Not Poetry")
  silent         = forms.BooleanField(initial=False, required=False, label="Silent")
  chatter        = forms.BooleanField(initial=False, required=False, label="Chatter")
  applause       = forms.BooleanField(initial=False, required=False, label="Applause")
  laughter       = forms.BooleanField(initial=False, required=False, label="Laughter")
  music          = forms.BooleanField(initial=False, required=False, label="Music")
  notRatable     = forms.BooleanField(initial=False, required=False, label="Not Ratable")

  tagId   = forms.IntegerField(required=True, label="Tag Id", widget=forms.TextInput(attrs={'readonly':'1'}))


## Classify a Tag from the specified TagSet
# @param request The Django HTTP request object.
# @param projectId The Database Id of the selected Project.
# @param tagSetId The Database Id of the selected TagSet.
# @param tagIndex The index, starting from 0, of the Tag(Example) to classify.
# Tags from the TagSet are ordered by id.
# @return Django HTTP response object.

@login_required
def classifyTag(request, projectId, tagSetId, tagIndex):
  user = request.user

  tagSet  =  TagSet.objects.get(id=tagSetId)
  userSettings = UserSettings.objects.get(user=user, name='default')

  tags = Tag.objects.filter(tagSet=tagSet).order_by("id")
  tagIndex = int(tagIndex)

  overviewUrl = reverse('startTagging', args=(projectId, tagSetId))

  if (tagIndex >= tags.count()):
    # end of Tags
    return HttpResponseRedirect(overviewUrl)

  tag = tags[tagIndex]

  try:
    existingTagClassification = TagClassification.objects.get(user=user, tag=tag)
  except:
    existingTagClassification = None


  ##
  # Plot Settings

  spectraImageSettings = SpectraImageSettings(**{
      'userSettings':userSettings,

      'audioFile': tag.mediaFile,
      'startTime': tag.startTime,
      'endTime': tag.endTime,
      'gain':1,
      'numFramesPerSecond': userSettings.tagViewSpectraNumFramesPerSecond,
      'numFrequencyBands': userSettings.tagViewSpectraNumFrequencyBands,
      'dampingFactor': userSettings.tagViewSpectraDampingFactor,

      'minFrequency': userSettings.tagViewSpectraMinimumBandFrequency,
      'maxFrequency': userSettings.tagViewSpectraMaximumBandFrequency,

      'catalogSpectraNumFrequencyBands': userSettings.tagViewSpectraNumFrequencyBands,
      })

  spectraImage = SpectraImage.generateSpectraImage(spectraImageSettings)
  if spectraImage is None:
    logging.error("createAudioSpectra Unknown Failure While Generating Spectra")
    return HttpResponseServerError("createAudioSpectra Unknown Spectra Generation Failure")

  imageFilePath = spectraImage.imageFilePath
  audioSegmentFilePath = spectraImage.audioSegmentFilePath
  audioSegmentFilePathQuoted = urllib.quote(spectraImage.audioSegmentFilePath)



  if request.method == 'GET':
    if existingTagClassification is None:
      form = ClassifyTagForm(
        initial={'tagEd': tag.id})
    else:
      form = ClassifyTagForm(
        initial={
          'tagId': tag.id,
          'tempo': existingTagClassification.tempo,
          'tempoChange': existingTagClassification.tempoChange,
          'rhythm': existingTagClassification.rhythm,
          'loudness': existingTagClassification.loudness,
          'loudnessChange': existingTagClassification.loudnessChange,
          'pitch': existingTagClassification.pitch,
          'pitchRange': existingTagClassification.pitchRange,
          'scandent': existingTagClassification.scandent,
          'tension': existingTagClassification.tension,
          'articulation': existingTagClassification.articulation,
#          'voice': existingTagClassification.voice,

          'whisp': existingTagClassification.whisp,
          'breath': existingTagClassification.breath,
          'husk': existingTagClassification.husk,
          'creak': existingTagClassification.creak,
          'fals': existingTagClassification.fals,
          'reson': existingTagClassification.reson,
          'giggle': existingTagClassification.giggle,
          'laugh': existingTagClassification.laugh,
          'trem': existingTagClassification.trem,
          'sob': existingTagClassification.sob,
          'yawn': existingTagClassification.yawn,
          'sigh': existingTagClassification.sigh,

          'notPoetry': existingTagClassification.notPoetry,
          'silent': existingTagClassification.silent,
          'chatter': existingTagClassification.chatter,
          'applause': existingTagClassification.applause,
          'laughter': existingTagClassification.laughter,
          'music': existingTagClassification.music,
          'notRatable': existingTagClassification.notRatable,
          })

    return render(request, 'poetryTaggingClassifyTag.html', {
      'apacheRoot':settings.APACHE_ROOT,
      'user':user,
      'form':form,
      'tag': tag,
      'existingTagClassification': existingTagClassification,

      'audioSegmentFilePath': audioSegmentFilePathQuoted,
      'imageFilePath': imageFilePath,
      'overviewUrl': overviewUrl,
      })

  elif request.method == 'POST':
    form = ClassifyTagForm(request.POST)

    ###
    # verify this is still the right tag

    if not 'tagId' in request.POST:
      return HttpResponseServerError("Missing Tag Id")

    if (int(request.POST['tagId']) != tag.id):
      return HttpResponseServerError("Mismatched Tag Id")

    ###
    # Check if skipping this tagging

    tagClassification = getNullTagClassification(tag, user)

    if 'id_silent' in request.POST:
      tagClassification.silent = True
    elif 'id_chatter' in request.POST:
      tagClassification.chatter = True
    elif 'id_applause' in request.POST:
      tagClassification.applause = True
    elif 'id_laughter' in request.POST:
      tagClassification.laughter = True
    elif 'id_music' in request.POST:
      tagClassification.music = True
    elif 'id_notratable' in request.POST:
      tagClassification.notRatable = True
    else:
      ###
      # Not Skipping, try to save data
      if not form.is_valid():
        return render(request, 'poetryTaggingClassifyTag.html', {
          'apacheRoot':settings.APACHE_ROOT,
          'user':user,
          'form':form,
          'tag': tag,
          'existingTagClassification': existingTagClassification,
          'audioSegmentFilePath': audioSegmentFilePath,
          'imageFilePath': imageFilePath,
          'overviewUrl': overviewUrl,
          })
      # get data
      tagClassification = tagClassificationFromFormData(form.cleaned_data, tag, user)

    ###
    # Save Results

    if existingTagClassification is not None:
      existingTagClassification.delete()

    tagClassification.save()

    return redirect('classifyTag', projectId=projectId, tagSetId=tagSetId, tagIndex=tagIndex+1)


  else:
    return HttpResponseNotAllowed(['GET', 'POST'])

def getNullTagClassification(tag, user):
  return TagClassification(
      # Form Data
      tempo          = None,
      tempoChange    = None,
      rhythm         = None,
      loudness       = None,
      loudnessChange = None,
      pitch          = None,
      pitchRange     = None,
      scandent       = None,
      tension        = None,
      articulation   = None,

#      voice          = data['voice'],
      whisp  = None,
      breath = None,
      husk   = None,
      creak  = None,
      fals   = None,
      reson  = None,
      giggle = None,
      laugh  = None,
      trem   = None,
      sob    = None,
      yawn   = None,
      sigh   = None,

      notPoetry      = None,

      silent         = None,
      chatter        = None,
      applause       = None,
      laughter       = None,
      music          = None,
      notRatable    = None,

      # other data
      tag = tag,
      user = user,
      )

def tagClassificationFromFormData(data, tag, user):
  return TagClassification(
      # Form Data
      tempo          = data['tempo'],
      tempoChange    = data['tempoChange'],
      rhythm         = data['rhythm'],
      loudness       = data['loudness'],
      loudnessChange = data['loudnessChange'],
      pitch          = data['pitch'],
      pitchRange     = data['pitchRange'],
      scandent       = data['scandent'],
      tension        = data['tension'],
      articulation   = data['articulation'],

#      voice          = data['voice'],
      whisp  = data['whisp'],
      breath = data['breath'],
      husk   = data['husk'],
      creak  = data['creak'],
      fals   = data['fals'],
      reson  = data['reson'],
      giggle = data['giggle'],
      laugh  = data['laugh'],
      trem   = data['trem'],
      sob    = data['sob'],
      yawn   = data['yawn'],
      sigh   = data['sigh'],

      notPoetry = data['notPoetry'],

      silent         = False,
      chatter        = False,
      applause       = False,
      laughter       = False,
      music          = False,
      notRatable     = False,

      # other data
      tag = tag,
      user = user,
      )
