import json

from django.contrib.auth.decorators import login_required
from django.shortcuts import render
from django.conf import settings

from tools.models import TagSet, Tag
from poetrytagging.models import TagClassification


## Dump data into a JavaScript visualization page.
# @param request The Django HTTP request object.
# @param projectId The Database Id of the selected Project.
# @param tagSetId The Database Id of the selected TagSet.
# @return Django HTTP response object.

@login_required
def visualizeAllClassifications(request, projectId, tagSetId):
  user = request.user

  tagSet  = TagSet.objects.get(id=tagSetId)


  # Get Tags in TagSet
  tags = Tag.objects.filter(tagSet=tagSet).order_by("id")
  tagIds = []
  for tag in tags:
    tagIds.append(tag.id)


  # get Classifications and build JSON return
  tagClassifications = TagClassification.objects.all()
  classifications = []
  for tc in tagClassifications:
    if tc.id in tagIds:
      classifications.append({
        'UserID': tc.user.id,
        'tagId': tc.id,
        'dateClassified': tc.dateClassified.strftime('%Y-%m-%d %H:%M:%S'),
        #'dateClassified': datetime.datetime.strftime(tc.dateClassified,'%Y-%m-%d %H:%M:%S'),
        'tempo': tc.get_tempo_display(),
        'tempoChange': tc.get_tempoChange_display(),
        'rhythm': tc.get_rhythm_display(),
        'loudness': tc.get_loudness_display(),
        'loudnessChange': tc.get_loudnessChange_display(),
        'pitch': tc.get_pitch_display(),
        'pitchRange': tc.get_pitchRange_display(),
        'scandent': tc.scandent,
        'tension': tc.get_tension_display(),
        'articulation': tc.get_articulation_display(),

        'whisp': tc.whisp,
        'breath': tc.breath,
        'husk': tc.husk,
        'creak': tc.creak,
        'fals': tc.fals,
        'reson': tc.reson,
        'giggle': tc.giggle,
        'laugh': tc.laugh,
        'trem': tc.trem,
        'sob': tc.sob,
        'yawn': tc.yawn,
        'sigh': tc.sigh,

        'notPoetry': tc.notPoetry,

        'silent': tc.silent,
        'chatter': tc.chatter,
        'applause': tc.applause,
        'laughter': tc.laughter,
        'music': tc.music,
        'notRatable': tc.notRatable,
        })


  return render(request, 'visualizeClassifications.html', {
    'apacheRoot':settings.APACHE_ROOT,
    'user':user,
    'classifications': json.dumps(classifications),
    })
