from django.contrib.auth.decorators import login_required
from django.http import HttpResponseNotAllowed
from django.shortcuts import render, redirect
from django import forms
from django.conf import settings

from tools.models import Project


class SelectProjectForm(forms.Form):
  project = forms.ModelChoiceField(Project.objects.none(), required=True)

## Select a Project to use for the Poetry Tagging
# @param request The Django HTTP request object.
# @return Django HTTP response object.

@login_required
def selectProject(request):
  user = request.user

  if request.method == 'GET':
    form = SelectProjectForm()
    form.fields['project'] = forms.ModelChoiceField(Project.objects.getUserProjects(user).order_by('name'), required=True)

    return render(request, 'selectProject.html', {
      'apacheRoot':settings.APACHE_ROOT,
      'user':user,
      'form':form,
      })

  elif request.method == 'POST':
    form = SelectProjectForm(request.POST)
    form.fields['project'] = forms.ModelChoiceField(Project.objects.getUserProjects(user).order_by('name'), required=True)

    if not form.is_valid():
      return render(request, 'selectProject.html', {
        'apacheRoot':settings.APACHE_ROOT,
        'user':user,
        'form':form,
        })

    data = form.cleaned_data
    project = data['project']

    return redirect('selectTagSet', projectId=project.id)
#    return HttpResponseRedirect('selectTagSet/' + str(project.id) + '/' + str(project.id))

  else:
    return HttpResponseNotAllowed(['GET', 'POST'])
