from django.contrib.auth.decorators import login_required
from django.http import HttpResponseNotAllowed
from django.shortcuts import render, redirect
from django.core.urlresolvers import reverse
from django.conf import settings

from tools.models import TagSet, Tag

from poetrytagging.models import TagClassification


##
# @param request The Django HTTP request object.
# @param projectId The Database Id of the selected Project.
# @param tagSetId The Database Id of the selected TagSet.
# @return Django HTTP response object.

@login_required
def startTagging(request, projectId, tagSetId):
  user = request.user

  tagSet  = TagSet.objects.get(id=tagSetId)

  exportUserClassificationsUrl = reverse('exportUserClassifications', args=(projectId, tagSetId))
  exportAllClassificationsUrl = reverse('exportAllClassifications', args=(projectId, tagSetId))
  visualizeAllClassificationsUrl = reverse('visualizeAllClassifications', args=(projectId, tagSetId))


  if request.method == 'GET':

    # Show the overview page

    # Get Tags in TagSet
    tags = Tag.objects.filter(tagSet=tagSet).order_by("id")

    # count the number of classified Tags
    # find the first unclassified Tag
    # get all classifications at once to avoid pounding on the database
    existingUserTagClassifications = TagClassification.objects.filter(user=user)

    classifiedTagIds = []
    for tc in existingUserTagClassifications:
      classifiedTagIds.append(tc.id)

    firstUntaggedIndex = None
    numClassifiedTags = 0
    for i in range(tags.count()):
      tagId = tags[i].id

      if tagId in classifiedTagIds:
        numClassifiedTags += 1
      else:
        if firstUntaggedIndex is None:
          firstUntaggedIndex = i

    return render(request, 'startTagging.html', {
        'apacheRoot':settings.APACHE_ROOT,
        'user':user,
        'numTags': tags.count(),
        'numClassifiedTags': numClassifiedTags,
        'firstUntaggedIndex': firstUntaggedIndex,
        'exportUserClassificationsUrl': exportUserClassificationsUrl,
        'exportAllClassificationsUrl': exportAllClassificationsUrl,
        'visualizeAllClassificationsUrl': visualizeAllClassificationsUrl,
        })

  elif request.method == 'POST':

    if 'gotoIndex' in request.POST:
      tagIndex = int(request.POST['gotoIndex'])
      return redirect('classifyTag', projectId=projectId, tagSetId=tagSetId, tagIndex=tagIndex)

    # Jump to the first tag
    return redirect('classifyTag', projectId=projectId, tagSetId=tagSetId, tagIndex=0)

  else:
    return HttpResponseNotAllowed(['GET', 'POST'])
