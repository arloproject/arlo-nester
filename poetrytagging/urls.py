from django.conf.urls import url

from views import selectProject, selectTagSet, startTagging, classifyTag, exportClassifications, visualizeClassifications

urlpatterns = [
    url(r'^selectProject/$', selectProject.selectProject),
    url(r'^selectTagSet/(?P<projectId>\d+)$', selectTagSet.selectTagSet, name='selectTagSet'),
    url(r'^startTagging/(?P<projectId>\d+)/(?P<tagSetId>\d+)$', startTagging.startTagging, name='startTagging'),
    url(r'^classifyTag/(?P<projectId>\d+)/(?P<tagSetId>\d+)/(?P<tagIndex>\d+)$', classifyTag.classifyTag, name='classifyTag'),
    url(r'^exportUserClassifications/(?P<projectId>\d+)/(?P<tagSetId>\d+)$', exportClassifications.exportClassifications, {'requestUserOnly':True}, name='exportUserClassifications'),
    url(r'^exportAllClassifications/(?P<projectId>\d+)/(?P<tagSetId>\d+)$', exportClassifications.exportClassifications, {'requestUserOnly':False}, name='exportAllClassifications'),
    url(r'^visualizeAllClassifications/(?P<projectId>\d+)/(?P<tagSetId>\d+)$', visualizeClassifications.visualizeAllClassifications, name='visualizeAllClassifications'),
]
