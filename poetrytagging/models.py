import datetime

from django.db import models

from tools.models import Tag, TagExample

class TagClassification(models.Model):
  user = models.ForeignKey('auth.User')

  # NOTE - We're removing TagExample - see tools/model.py
  # TODO
  tagExample = models.ForeignKey(TagExample, null=True, default=None)
  tag = models.ForeignKey(Tag)
  dateClassified = models.DateTimeField(verbose_name='Date Classified', default=datetime.datetime.now, null=False)

  TEMPO_CHOICES = (
    ('VS', 'Very Slow'),
    ('S', 'Slow'),
    ('M', 'Moderate'),
    ('F', 'Fast'),
    ('VF', 'Very Fast'))
  tempo = models.CharField(
    verbose_name='Tempo',
    max_length=2,
    null=True,
    choices=TEMPO_CHOICES)

  TEMPOCHANGE_CHOICES = (
    ('-', 'N/A'),
    ('F', 'Getting Faster'),
    ('S', 'Getting Slower'),
    ('0', 'Same Tempo'))
  tempoChange = models.CharField(
    verbose_name='Tempo Change',
    max_length=1,
    null=True,
    choices=TEMPOCHANGE_CHOICES)

  RHYTHM_CHOICES = (
    ('B', 'Beatable'),
    ('M', 'Moderate'),
    ('A', 'Arrhythmic'))
  rhythm = models.CharField(
    verbose_name='Rhythm',
    max_length=1,
    null=True,
    choices=RHYTHM_CHOICES)

  LOUDNESS_CHOICES = (
    ('VS', 'Very Soft'),
    ('S', 'Soft'),
    ('M', 'Moderate'),
    ('L', 'Loud'),
    ('VL', 'Very Loud'))
  loudness = models.CharField(
    verbose_name='Loudness',
    max_length=2,
    null=True,
    choices=LOUDNESS_CHOICES)

  LOUDNESSCHANGE_CHOICES = (
    ('-', 'N/A'),
    ('L', 'Louder'),
    ('S', 'Softer'),
    ('N', 'Same'))  # No Change
  loudnessChange = models.CharField(
    verbose_name='Loudness Change',
    max_length=1,
    null=True,
    choices=LOUDNESSCHANGE_CHOICES)

  PITCH_CHOICES = (
    ('L', 'Low'),
    ('M', 'Middle'),
    ('H', 'High'))
  pitch = models.CharField(
    verbose_name='Pitch',
    max_length=1,
    null=True,
    choices=PITCH_CHOICES)

  PITCHRANGE_CHOICES = (
    ('-', 'N/A'),
    ('N', 'Narrow'),
    ('W', 'Wide'),
    ('A', 'Ascending'),
    ('D', 'Descending'))
  pitchRange = models.CharField(
    verbose_name='Pitch Range',
    max_length=1,
    null=True,
    choices=PITCHRANGE_CHOICES)

  scandent = models.NullBooleanField(verbose_name='Scandent', default=False)

  TENSION_CHOICES = (
    ('S', 'Slurred'),
    ('L', 'Lax'),
    ('M', 'Moderate'),
    ('T', 'Tense'),
    ('V', 'Very Precise'))
  tension = models.CharField(
    verbose_name='Tension',
    max_length=1,
    null=True,
    choices=TENSION_CHOICES)

  ARTICULATION_CHOICES = (
    ('-', 'N/A'),
    ('S', 'Staccato'),
    ('L', 'Legato'),
    ('N', 'None'))
  articulation = models.CharField(
    verbose_name='Articulation',
    max_length=1,
    null=True,
    choices=ARTICULATION_CHOICES)

#  VOICE_CHOICES = (
#    ('whisp', 'Whisper'),
#    ('breath', 'Breathy'),
#    ('husk', 'Husky'),
#    ('creak', 'Creaky'),
#    ('fals', 'Falsetto'),
#    ('reson', 'Resonant'),
#    ('giggle', 'Unvoiced Laugh or Giggle'),
#    ('laugh', 'Voiced Laugh'),
#    ('trem', 'Tremulous'),
#    ('sob', 'Sobbing'),
#    ('yawn', 'Yawning'),
#    ('sigh', 'Sighing'))
#  voice = models.CharField(
#    name='Voice',
#    max_length=6,
#    null=True,
#    choices=VOICE_CHOICES)

  # Voice
  whisp  = models.NullBooleanField(verbose_name='Whisper', default=False)
  breath = models.NullBooleanField(verbose_name='Breathy', default=False)
  husk   = models.NullBooleanField(verbose_name='Husky', default=False)
  creak  = models.NullBooleanField(verbose_name='Creaky', default=False)
  fals   = models.NullBooleanField(verbose_name='Falsetto', default=False)
  reson  = models.NullBooleanField(verbose_name='Resonant', default=False)
  giggle = models.NullBooleanField(verbose_name='Unvoiced Laugh or Giggle', default=False)
  laugh  = models.NullBooleanField(verbose_name='Voiced Laugh', default=False)
  trem   = models.NullBooleanField(verbose_name='Tremulous', default=False)
  sob    = models.NullBooleanField(verbose_name='Sobbing', default=False)
  yawn   = models.NullBooleanField(verbose_name='Yawning', default=False)
  sigh   = models.NullBooleanField(verbose_name='Sighing', default=False)

  notPoetry      = models.NullBooleanField(verbose_name='Not Poetry', default=False)
  silent         = models.NullBooleanField(verbose_name='Silent', default=False)
  chatter        = models.NullBooleanField(verbose_name='Chatter', default=False)
  applause       = models.NullBooleanField(verbose_name='Applause', default=False)
  laughter       = models.NullBooleanField(verbose_name='Laughter', default=False)
  music          = models.NullBooleanField(verbose_name='Music', default=False)
  notRatable     = models.NullBooleanField(verbose_name='Not Ratable', default=False)

  class Meta:
    unique_together = ('user', 'tag',)

