# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
from django.conf import settings


class UniqueTagNotFound(Exception):
    def __init__(self, value):
        super(UniqueTagNotFound, self).__init__()
        self.value = value
    def __str__(self):
        return repr(self.value)


def update_tag_from_tagexample(apps, schema_editor):
    TagClassification = apps.get_model("poetrytagging", "TagClassification")
    Tag = apps.get_model("tools", "Tag")

    # Update Tags from TagExample
    for tagClassification in TagClassification.objects.all():
        tagExample = tagClassification.tagExample

        # Find Tag that corresponds to this TagExample
        tags = Tag.objects.filter(tagExample=tagExample)
        if tags.count() != 1:
            raise UniqueTagNotFound('{} Tags Found for TagExample {}'.format(tags.count(), tagExample.id))

        tagClassification.tag = tags[0]
        tagClassification.tagExample = None
        tagClassification.save()


def clear_tagexample(apps, schema_editor):
    TagClassification = apps.get_model("poetrytagging", "TagClassification")
    TagClassification.objects.all().update(tagExample=None)


class Migration(migrations.Migration):

    dependencies = [
        ('tools', '0010_remove_imagePath'),
        ('poetrytagging', '0001_initial'),
    ]

    operations = [
        migrations.AddField(
            model_name='tagclassification',
            name='tag',
            field=models.ForeignKey(default=None, to='tools.Tag', null=True),
        ),
        migrations.AlterField(
            model_name='tagclassification',
            name='tagExample',
            field=models.ForeignKey(default=None, to='tools.TagExample', null=True),
        ),

        ###
        # Update the TagExample reference to Tag

        migrations.RunPython(update_tag_from_tagexample),

        ### Workarounds for changing the UniqueTogether...
        #

        migrations.AlterField(
            model_name='tagclassification',
            name='user',
            field=models.IntegerField(),
        ),

        migrations.AlterUniqueTogether(
            name='tagclassification',
            unique_together=set([('user', 'tag')]),
        ),

        migrations.RunPython(clear_tagexample),

        migrations.AlterField(
            model_name='tagclassification',
            name='user',
            field=models.ForeignKey(to=settings.AUTH_USER_MODEL),
        ),

        ###
        # Remove the Default / null options

        migrations.AlterField(
            model_name='tagclassification',
            name='tag',
            field=models.ForeignKey(to='tools.Tag', null=False),
        ),

    ]
