# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
import datetime
from django.conf import settings


class Migration(migrations.Migration):

    dependencies = [
        ('tools', '0001_initial'),
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
    ]

    operations = [
        migrations.CreateModel(
            name='TagClassification',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('dateClassified', models.DateTimeField(default=datetime.datetime.now, verbose_name=b'Date Classified')),
                ('tempo', models.CharField(max_length=2, null=True, verbose_name=b'Tempo', choices=[(b'VS', b'Very Slow'), (b'S', b'Slow'), (b'M', b'Moderate'), (b'F', b'Fast'), (b'VF', b'Very Fast')])),
                ('tempoChange', models.CharField(max_length=1, null=True, verbose_name=b'Tempo Change', choices=[(b'-', b'N/A'), (b'F', b'Getting Faster'), (b'S', b'Getting Slower'), (b'0', b'Same Tempo')])),
                ('rhythm', models.CharField(max_length=1, null=True, verbose_name=b'Rhythm', choices=[(b'B', b'Beatable'), (b'M', b'Moderate'), (b'A', b'Arrhythmic')])),
                ('loudness', models.CharField(max_length=2, null=True, verbose_name=b'Loudness', choices=[(b'VS', b'Very Soft'), (b'S', b'Soft'), (b'M', b'Moderate'), (b'L', b'Loud'), (b'VL', b'Very Loud')])),
                ('loudnessChange', models.CharField(max_length=1, null=True, verbose_name=b'Loudness Change', choices=[(b'-', b'N/A'), (b'L', b'Louder'), (b'S', b'Softer'), (b'N', b'Same')])),
                ('pitch', models.CharField(max_length=1, null=True, verbose_name=b'Pitch', choices=[(b'L', b'Low'), (b'M', b'Middle'), (b'H', b'High')])),
                ('pitchRange', models.CharField(max_length=1, null=True, verbose_name=b'Pitch Range', choices=[(b'-', b'N/A'), (b'N', b'Narrow'), (b'W', b'Wide'), (b'A', b'Ascending'), (b'D', b'Descending')])),
                ('scandent', models.NullBooleanField(default=False, verbose_name=b'Scandent')),
                ('tension', models.CharField(max_length=1, null=True, verbose_name=b'Tension', choices=[(b'S', b'Slurred'), (b'L', b'Lax'), (b'M', b'Moderate'), (b'T', b'Tense'), (b'V', b'Very Precise')])),
                ('articulation', models.CharField(max_length=1, null=True, verbose_name=b'Articulation', choices=[(b'-', b'N/A'), (b'S', b'Staccato'), (b'L', b'Legato'), (b'N', b'None')])),
                ('whisp', models.NullBooleanField(default=False, verbose_name=b'Whisper')),
                ('breath', models.NullBooleanField(default=False, verbose_name=b'Breathy')),
                ('husk', models.NullBooleanField(default=False, verbose_name=b'Husky')),
                ('creak', models.NullBooleanField(default=False, verbose_name=b'Creaky')),
                ('fals', models.NullBooleanField(default=False, verbose_name=b'Falsetto')),
                ('reson', models.NullBooleanField(default=False, verbose_name=b'Resonant')),
                ('giggle', models.NullBooleanField(default=False, verbose_name=b'Unvoiced Laugh or Giggle')),
                ('laugh', models.NullBooleanField(default=False, verbose_name=b'Voiced Laugh')),
                ('trem', models.NullBooleanField(default=False, verbose_name=b'Tremulous')),
                ('sob', models.NullBooleanField(default=False, verbose_name=b'Sobbing')),
                ('yawn', models.NullBooleanField(default=False, verbose_name=b'Yawning')),
                ('sigh', models.NullBooleanField(default=False, verbose_name=b'Sighing')),
                ('notPoetry', models.NullBooleanField(default=False, verbose_name=b'Not Poetry')),
                ('silent', models.NullBooleanField(default=False, verbose_name=b'Silent')),
                ('chatter', models.NullBooleanField(default=False, verbose_name=b'Chatter')),
                ('applause', models.NullBooleanField(default=False, verbose_name=b'Applause')),
                ('laughter', models.NullBooleanField(default=False, verbose_name=b'Laughter')),
                ('music', models.NullBooleanField(default=False, verbose_name=b'Music')),
                ('notRatable', models.NullBooleanField(default=False, verbose_name=b'Not Ratable')),
                ('tagExample', models.ForeignKey(to='tools.TagExample')),
                ('user', models.ForeignKey(to=settings.AUTH_USER_MODEL)),
            ],
        ),
        migrations.AlterUniqueTogether(
            name='tagclassification',
            unique_together=set([('user', 'tagExample')]),
        ),
    ]
