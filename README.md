ARLO Project - Nester Frontend
============

Nester is the Python/Django web-frontend for ARLO. In addition to the interface, 
this also defines the database schema, which is controlled exclusively 
through the Django ORM. 

### Required Modules

Python requirements are maintained in requirements.txt. 

To install with pip: 
`pip install -r requirements.txt`

### Docs

Sphinx formatted documentation is available in docs/


### License

ARLO is released under the University of Illinois/NCSA Open Source License. 

See the LICENSE file in the root of this repository for further details. 
