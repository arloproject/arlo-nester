from django.conf.urls import url, include
from rest_framework.schemas import get_schema_view
from rest_framework import urls as rest_framework_urls

from api.v0 import urls as api_v0_urls


schema_view = get_schema_view(title="ARLO API")

urlpatterns = [
    url('^$', schema_view),
    url(r'^api-auth/', include(rest_framework_urls, namespace='rest_framework')),
    url(r'^v0/', include(api_v0_urls)),
]
