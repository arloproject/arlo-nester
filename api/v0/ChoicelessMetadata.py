from rest_framework.metadata import BaseMetadata, SimpleMetadata

class ChoicelessMetadata(SimpleMetadata):
    """
    Override the SimpleMetadata default to not include 'choices' in the 
    OPTIONS.
    """
    def get_field_info(self, field):
        field_info = super(ChoicelessMetadata, self).get_field_info(field)
        field_info.pop('choices', None)
        return field_info
