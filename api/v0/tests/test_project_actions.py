import json

from django.test import TestCase
from rest_framework.test import APIClient

from .utils import Ensure_All_403_Or_Fail
from .config import *


class APIProjectMediaFilesTest(TestCase):
  fixtures = ['api_test_data.yaml']

  def test_unauthedRequests(self):
    client = APIClient()
    Ensure_All_403_Or_Fail(self, client, baseUrl + "project/2/mediaFiles/")

  def test_adminUserAccess(self):
    client = APIClient()
    self.assertEqual(
      client.login(username=adminUserName, password=adminUserPass),
      True)

    # list records
    response = client.get(baseUrl + "project/1/mediaFiles/")
    self.assertEqual(response.status_code, 200)
    data = json.loads(response.content)
    self.assertEqual(data['count'], 1)
    for result in data['results']:
      id = result['id']
      if id == 1:
        self.assertEqual(result['alias'], 'adminFakeFile')
      else:
        self.fail("Unknown ID Found ({})".format(id))

    # Add a non-existing mediafile should 404
    response = client.post(baseUrl + "project/1/mediaFiles/add/", {'id': 99999})
    self.assertEqual(response.status_code, 404)

    # Add a new MediaFile
    response = client.post(baseUrl + "project/1/mediaFiles/add/", {'id': 2})
    self.assertEqual(response.status_code, 201)
    response = client.get(baseUrl + "project/1/mediaFiles/")
    self.assertEqual(response.status_code, 200)
    data = json.loads(response.content)
    self.assertEqual(data['count'], 2)
    for result in data['results']:
      id = result['id']
      if id == 1:
        self.assertEqual(result['alias'], 'adminFakeFile')
      elif result['id'] == 2:
        self.assertEqual(result['alias'], 'notReallyAnUploadedFile')
      else:
        self.fail("Unknown ID Found ({})".format(id))

    # Re-add the same one should pass, with 200
    response = client.post(baseUrl + "project/1/mediaFiles/add/", {'id': 2})
    self.assertEqual(response.status_code, 200)
    response = client.get(baseUrl + "project/1/mediaFiles/")
    self.assertEqual(response.status_code, 200)
    data = json.loads(response.content)
    self.assertEqual(data['count'], 2)

    # Remove a mediafile
    response = client.post(baseUrl + "project/1/mediaFiles/remove/", {'id': 2})
    self.assertEqual(response.status_code, 204)
    response = client.get(baseUrl + "project/1/mediaFiles/")
    self.assertEqual(response.status_code, 200)
    data = json.loads(response.content)
    self.assertEqual(data['count'], 1)
    for result in data['results']:
      if result['id'] != 1:
        self.fail("Unknown ID Found ({})".format(id))

    # Remove non-existent should 404
    response = client.post(baseUrl + "project/1/mediaFiles/remove/", {'id': 99999})
    self.assertEqual(response.status_code, 404)

    # List records - Admin override
    response = client.get(baseUrl + "project/2/mediaFiles/")
    self.assertEqual(response.status_code, 200)
    data = json.loads(response.content)
    self.assertEqual(data['count'], 5)
    for result in data['results']:
      id = result['id']
      if id == 2:
        self.assertEqual(result['alias'], 'notReallyAnUploadedFile')
      elif id == 3:
        self.assertEqual(result['alias'], 'notReallyAnUploadedFile')
      elif id == 4:
        self.assertEqual(result['alias'], 'notReallyAnUploadedFile-id4')
      elif id == 5:
        self.assertEqual(result['alias'], 'notReallyAnUploadedFile-id5')
      elif id == 6:
        self.assertEqual(result['alias'], 'notReallyAnUploadedFile')
      else:
        self.fail("Unknown ID Found ({})".format(id))


  def test_normalUserAccess(self):
    client = APIClient()
    self.assertEqual(
      client.login(username=normalUserName, password=normalUserPass),
      True)

    # list records
    response = client.get(baseUrl + "project/2/mediaFiles/")
    self.assertEqual(response.status_code, 200)
    data = json.loads(response.content)
    self.assertEqual(data['count'], 5)
    for result in data['results']:
      if result['id'] == 2:
        self.assertEqual(result['alias'], 'notReallyAnUploadedFile')
      elif result['id'] == 3:
        self.assertEqual(result['alias'], 'notReallyAnUploadedFile')
      elif result['id'] == 4:
        self.assertEqual(result['alias'], 'notReallyAnUploadedFile-id4')
      elif result['id'] == 5:
        self.assertEqual(result['alias'], 'notReallyAnUploadedFile-id5')
      elif result['id'] == 6:
        self.assertEqual(result['alias'], 'notReallyAnUploadedFile')
      else:
        self.fail("Unknown ID Found ({})".format(id))

    # should not have access
    response = client.get(baseUrl + "project/1/mediaFiles/")
    self.assertEqual(response.status_code, 403)


    # Add to admin project should not be authorized
    response = client.post(baseUrl + "project/1/mediaFiles/add/", {'id': 2})
    self.assertEqual(response.status_code, 403)

    # Add admin file should fail
    response = client.post(baseUrl + "project/2/mediaFiles/add/", {'id': 1})
    self.assertEqual(response.status_code, 403)

    # Add a non-existing mediafile should 404
    response = client.post(baseUrl + "project/2/mediaFiles/add/", {'id': 99999})
    self.assertEqual(response.status_code, 404)

    # Remove non-existent should 404
    response = client.post(baseUrl + "project/2/mediaFiles/remove/", {'id': 99999})
    self.assertEqual(response.status_code, 404)

    # Remove a file
    response = client.post(baseUrl + "project/2/mediaFiles/remove/", {'id': 2})
    self.assertEqual(response.status_code, 204)
    response = client.get(baseUrl + "project/2/mediaFiles/")
    self.assertEqual(response.status_code, 200)
    data = json.loads(response.content)
    self.assertEqual(data['count'], 4)
    for result in data['results']:
      if result['id'] not in [3,4,5,6]:
        self.fail("Unknown ID Found ({})".format(id))

    # Add MediaFile
    response = client.post(baseUrl + "project/2/mediaFiles/add/", {'id': 2})
    self.assertEqual(response.status_code, 201)
    response = client.get(baseUrl + "project/2/mediaFiles/")
    self.assertEqual(response.status_code, 200)
    data = json.loads(response.content)
    self.assertEqual(data['count'], 5)
    for result in data['results']:
      if result['id'] not in [2,3,4,5,6]:
        self.fail("Unknown ID Found ({})".format(id))

    # Re-add the same one should pass
    response = client.post(baseUrl + "project/2/mediaFiles/add/", {'id': 2})
    self.assertEqual(response.status_code, 200)
    response = client.get(baseUrl + "project/2/mediaFiles/")
    self.assertEqual(response.status_code, 200)
    data = json.loads(response.content)
    self.assertEqual(data['count'], 5)


  def test_otherUserAccess(self):
    client = APIClient()
    self.assertEqual(
      client.login(username=user2Name, password=user2Pass),
      True)

    # List records - should not have access
    response = client.get(baseUrl + "project/1/mediaFiles/")
    self.assertEqual(response.status_code, 403)

    # list records
    response = client.get(baseUrl + "project/2/mediaFiles/")
    self.assertEqual(response.status_code, 200)
    data = json.loads(response.content)
    self.assertEqual(data['count'], 5)
    for result in data['results']:
      if result['id'] == 2:
        self.assertEqual(result['alias'], 'notReallyAnUploadedFile')
      elif result['id'] == 3:
        self.assertEqual(result['alias'], 'notReallyAnUploadedFile')
      elif result['id'] == 4:
        self.assertEqual(result['alias'], 'notReallyAnUploadedFile-id4')
      elif result['id'] == 5:
        self.assertEqual(result['alias'], 'notReallyAnUploadedFile-id5')
      elif result['id'] == 6:
        self.assertEqual(result['alias'], 'notReallyAnUploadedFile')
      else:
        self.fail("Unknown ID Found ({})".format(id))


    # Read-only, all these should fail

    response = client.post(baseUrl + "project/1/mediaFiles/add/", {'id': 2})
    self.assertEqual(response.status_code, 403)

    response = client.post(baseUrl + "project/2/mediaFiles/add/", {'id': 99999})
    self.assertEqual(response.status_code, 403)

    response = client.post(baseUrl + "project/2/mediaFiles/remove/", {'id': 2})
    self.assertEqual(response.status_code, 403)

    response = client.post(baseUrl + "project/2/mediaFiles/add/", {'id': 2})
    self.assertEqual(response.status_code, 403)
