import json

from django.test import TestCase
from rest_framework.test import APIClient

from .utils import Add_Update_Delete, Ensure_All_403_Or_Fail
from .config import *


class APIArloUserGroupTest(TestCase):
  fixtures = ['api_test_data.yaml']


  def test_unauthedRequests(self):
    client = APIClient()
    Ensure_All_403_Or_Fail(self, client, baseUrl + "arloUserGroup/")
    Ensure_All_403_Or_Fail(self, client, baseUrl + "arloUserGroup/1/")


  def test_adminUserAccess(self):
    client = APIClient()
    self.assertEqual(
      client.login(username=adminUserName, password=adminUserPass),
      True)

    # List records
    response = client.get(baseUrl + "arloUserGroup/")
    self.assertEqual(response.status_code, 200)
    data = json.loads(response.content)
    self.assertEqual(data.get('count'), 2)
    results = data.get('results')
    for result in results:
      if result['id'] == 1:
        self.assertEqual(result['name'], 'ArloGroup1')
        self.assertEqual(result['users'], [1,2,3])
        self.assertEqual(result['creationDate'], '2012-01-01T12:34:56')
        self.assertEqual(result['createdBy'], 1)
      elif result['id'] == 2:
        self.assertEqual(result['name'], 'ArloGroup2')
        self.assertEqual(result['users'], [1, 2])
        self.assertEqual(result['creationDate'], '2012-01-01T12:34:56')
        self.assertEqual(result['createdBy'], 2)
      else:
        self.fail("Unknown ID Found ({})".format(id))

    # retrieve an existing record
    # note we are using another user's for below test
    response = client.get(baseUrl + "arloUserGroup/3/")
    self.assertEqual(response.status_code, 200)
    data = json.loads(response.content)
    self.assertEqual(data['name'], 'ArloGroup3')
    self.assertEqual(data['users'], [2])
    self.assertEqual(data['creationDate'], '2012-01-01T12:34:56')
    self.assertEqual(data['createdBy'], 2)

    # modify existing record
    data['users'] = [1,2]

    # use JSON to handle the nulls
    response = client.put(baseUrl + "arloUserGroup/3/", json.dumps(data), content_type="application/json")
    self.assertEqual(response.status_code, 200)
    data = json.loads(response.content)
    self.assertEqual(data.get('users'), [1, 2])

    # re-retrieve existing record
    # NOTE: we want to ensure the the user of an existing record
    # isn't changed after the above update
    response = client.get(baseUrl + "arloUserGroup/3/")
    self.assertEqual(response.status_code, 200)
    data = json.loads(response.content)
    self.assertEqual(data['name'], 'ArloGroup3')
    self.assertEqual(data['users'], [1, 2])
    self.assertEqual(data['creationDate'], '2012-01-01T12:34:56')
    self.assertEqual(data['createdBy'], 2)

    # add a new record
    Add_Update_Delete(self, client, "users", [1, 2, 3], baseUrl + "arloUserGroup/", {
      'name': 'NewArloGroup',
      'users': [1, 2],
      })

    # also make sure we can create a Group with an empty set of users
    Add_Update_Delete(self, client, "users", [1, 2, 3], baseUrl + "arloUserGroup/", {
      'name': 'NewArloGroup',
      })
    Add_Update_Delete(self, client, "users", [1, 2, 3], baseUrl + "arloUserGroup/", {
      'name': 'NewArloGroup',
      'users': [],
      })
    Add_Update_Delete(self, client, "users", [], baseUrl + "arloUserGroup/", {
      'name': 'NewArloGroup',
      'users': [1, 2],
      })


    # Test the django-filter API query, lookup 'ArloGroup1'
    response = client.get(baseUrl + "arloUserGroup/?name=ArloGroup1")
    self.assertEqual(response.status_code, 200)
    data = json.loads(response.content)
    self.assertTrue('count' in data)
    self.assertEqual(data.get('count'), 1)
    self.assertTrue('results' in data)
    results = data.get('results')
    for result in results:
      self.assertTrue(result['id'], 1)
      self.assertEqual(result['name'], 'ArloGroup1')


# TODO Create test new user name

  def test_normalUserAccess(self):
    client = APIClient()
    self.assertEqual(
      client.login(username=normalUserName, password=normalUserPass),
      True)

    # List records
    response = client.get(baseUrl + "arloUserGroup/")
    self.assertEqual(response.status_code, 200)
    data = json.loads(response.content)
    self.assertEqual(data.get('count'), 3)
    results = data.get('results')
    for result in results:
      if result['id'] == 1:
        self.assertEqual(result['name'], 'ArloGroup1')
        self.assertEqual(result['users'], [1,2,3])
        self.assertEqual(result['creationDate'], '2012-01-01T12:34:56')
        self.assertEqual(result['createdBy'], 1)
      elif result['id'] == 2:
        self.assertEqual(result['name'], 'ArloGroup2')
        self.assertEqual(result['users'], [1, 2])
        self.assertEqual(result['creationDate'], '2012-01-01T12:34:56')
        self.assertEqual(result['createdBy'], 2)
      elif result['id'] == 3:
        self.assertEqual(result['name'], 'ArloGroup3')
        self.assertEqual(result['users'], [2])
        self.assertEqual(result['creationDate'], '2012-01-01T12:34:56')
        self.assertEqual(result['createdBy'], 2)
      else:
        self.fail("Unknown ID Found ({})".format(id))

    # retrieve a read-only record
    response = client.get(baseUrl + "arloUserGroup/1/")
    self.assertEqual(response.status_code, 200)
    data = json.loads(response.content)
    self.assertEqual(data['name'], 'ArloGroup1')
    self.assertEqual(data['users'], [1, 2, 3])
    self.assertEqual(data['creationDate'], '2012-01-01T12:34:56')
    self.assertEqual(data['createdBy'], 1)

    # retrieve an existing record
    response = client.get(baseUrl + "arloUserGroup/2/")
    self.assertEqual(response.status_code, 200)
    data = json.loads(response.content)
    self.assertEqual(data['name'], 'ArloGroup2')
    self.assertEqual(data['users'], [1, 2])
    self.assertEqual(data['creationDate'], '2012-01-01T12:34:56')
    self.assertEqual(data['createdBy'], 2)

    # modify existing record
    data['users'] = [2]

    # use JSON to handle the nulls
    response = client.put(baseUrl + "arloUserGroup/2/", json.dumps(data), content_type="application/json")
    self.assertEqual(response.status_code, 200)
    data = json.loads(response.content)
    self.assertEqual(data['users'], [2])

    # Make sure we can change name
    response = client.get(baseUrl + "arloUserGroup/2/")
    self.assertEqual(response.status_code, 200)
    data = json.loads(response.content)
    data['name'] = "ArloGroup2a"
    response = client.put(baseUrl + "arloUserGroup/2/", json.dumps(data), content_type="application/json")
    self.assertEqual(response.status_code, 200)
    data = json.loads(response.content)
    self.assertEqual(data['name'], 'ArloGroup2a')

    # re-retrieve existing record
    # NOTE: we want to ensure the the user of an existing record
    # isn't changed after the above update
    response = client.get(baseUrl + "arloUserGroup/2/")
    self.assertEqual(response.status_code, 200)
    data = json.loads(response.content)
    self.assertEqual(data['name'], 'ArloGroup2a')
    self.assertEqual(data['users'], [2])
    self.assertEqual(data['creationDate'], '2012-01-01T12:34:56')
    self.assertEqual(data['createdBy'], 2)

    # add a new record
    Add_Update_Delete(self, client, "users", [1, 2, 3], baseUrl + "arloUserGroup/", {
      'name': 'NewArloGroup',
      'users': [1, 2],
      })

    # Ensure unique name enforced
    response = client.post(baseUrl + "arloUserGroup/", json.dumps({'name': 'ArloGroup1', 'users': [1, 2]}), content_type="application/json")
    self.assertEqual(response.status_code, 400)

    # TODO Test the django-filter API query, lookup 'ArloGroup1'
    # Once permissions are in place, we need to test to ensure
    # groups that are not normally visible are not returned.



  def test_otherUserAccess(self):
    client = APIClient()
    self.assertEqual(
      client.login(username=user2Name, password=user2Pass),
      True)

    # List Records
    response = client.get(baseUrl + "arloUserGroup/")
    self.assertEqual(response.status_code, 200)
    data = json.loads(response.content)
    self.assertEqual(data.get('count'), 1)
    results = data.get('results')
    for result in results:
      if result['id'] == 1:
        self.assertEqual(result['name'], 'ArloGroup1')
        self.assertEqual(result['users'], [1,2,3])
        self.assertEqual(result['creationDate'], '2012-01-01T12:34:56')
        self.assertEqual(result['createdBy'], 1)
      else:
        self.fail("Unknown ID Found ({})".format(id))


    # test accessing the existing record from another user
    response = client.get(baseUrl + "arloUserGroup/2/")
    self.assertEqual(response.status_code, 403)

    # retrieve a read-only record
    response = client.get(baseUrl + "arloUserGroup/1/")
    self.assertEqual(response.status_code, 200)
    data = json.loads(response.content)
    self.assertEqual(data['name'], 'ArloGroup1')
    self.assertEqual(data['users'], [1, 2, 3])
    self.assertEqual(data['creationDate'], '2012-01-01T12:34:56')
    self.assertEqual(data['createdBy'], 1)

    # test update
    response = client.put(baseUrl + "arloUserGroup/1/", {})
    self.assertEqual(response.status_code, 403)


class APIArloUserGroupActionsTest(TestCase):
  """
  Tests for additional action links, /list_users, /add_user, and /remove_user
  """
  fixtures = ['api_test_data.yaml']


  def test_unauthedRequests(self):
    client = APIClient()
    Ensure_All_403_Or_Fail(self, client, baseUrl + "arloUserGroup/1/list_users/")
    Ensure_All_403_Or_Fail(self, client, baseUrl + "arloUserGroup/1/add_user/")
    Ensure_All_403_Or_Fail(self, client, baseUrl + "arloUserGroup/1/remove_user/")


  def test_adminUserAccess(self):
    client = APIClient()
    self.assertEqual(
      client.login(username=adminUserName, password=adminUserPass),
      True)

    # List users
    response = client.get(baseUrl + "arloUserGroup/1/list_users/")
    self.assertEqual(response.status_code, 200)
    data = json.loads(response.content)
    self.assertEqual(data.get('count'), 3)
    results = data.get('results')
    for result in results:
      if result['id'] == 1:
        self.assertEqual(result['username'], 'adminuser')
      elif result['id'] == 2:
        self.assertEqual(result['username'], 'normaluser')
      elif result['id'] == 3:
        self.assertEqual(result['username'], 'user2')
      else:
        self.fail("Unknown ID Found ({})".format(id))

    # Try adding an existing user, should 200
    response = client.post(baseUrl + "arloUserGroup/1/add_user/", json.dumps({'name':'user2'}), content_type="application/json")
    self.assertEqual(response.status_code, 200)

    # Remove existing user from group
    response = client.post(baseUrl + "arloUserGroup/1/remove_user/", json.dumps({'id':3}), content_type="application/json")
    self.assertEqual(response.status_code, 204)

    # List users again, ensuring '3' is gone
    response = client.get(baseUrl + "arloUserGroup/1/list_users/")
    self.assertEqual(response.status_code, 200)
    data = json.loads(response.content)
    self.assertEqual(data.get('count'), 2)
    results = data.get('results')
    for result in results:
      if result['id'] not in [1,2]:
        self.fail("Unknown ID Found ({})".format(id))

    # Add user back to group
    response = client.post(baseUrl + "arloUserGroup/1/add_user/", json.dumps({'name':'user2'}), content_type="application/json")
    self.assertEqual(response.status_code, 201)

    # List users again, ensuring '3' is back
    response = client.get(baseUrl + "arloUserGroup/1/list_users/")
    self.assertEqual(response.status_code, 200)
    data = json.loads(response.content)
    self.assertEqual(data.get('count'), 3)
    results = data.get('results')
    for result in results:
      if result['id'] not in [1,2, 3]:
        self.fail("Unknown ID Found ({})".format(id))

    # Remove again, this time with delete methond
    response = client.delete(baseUrl + "arloUserGroup/1/remove_user/", json.dumps({'id':3}), content_type="application/json")
    self.assertEqual(response.status_code, 204)

    # List users again, ensuring '3' is gone
    response = client.get(baseUrl + "arloUserGroup/1/list_users/")
    self.assertEqual(response.status_code, 200)
    data = json.loads(response.content)
    self.assertEqual(data.get('count'), 2)
    results = data.get('results')
    for result in results:
      if result['id'] not in [1,2]:
        self.fail("Unknown ID Found ({})".format(id))


  def test_normalUserAccess(self):
    client = APIClient()
    self.assertEqual(
      client.login(username=normalUserName, password=normalUserPass),
      True)

    # List users
    response = client.get(baseUrl + "arloUserGroup/1/list_users/")
    self.assertEqual(response.status_code, 200)
    data = json.loads(response.content)
    self.assertEqual(data.get('count'), 3)
    results = data.get('results')
    for result in results:
      if result['id'] == 1:
        self.assertEqual(result['username'], 'adminuser')
      elif result['id'] == 2:
        self.assertEqual(result['username'], 'normaluser')
      elif result['id'] == 3:
        self.assertEqual(result['username'], 'user2')
      else:
        self.fail("Unknown ID Found ({})".format(id))

    # Remove existing user from group
    response = client.post(baseUrl + "arloUserGroup/2/remove_user/", json.dumps({'id':1}), content_type="application/json")
    self.assertEqual(response.status_code, 204)

    # List users again, ensuring '1' is gone
    response = client.get(baseUrl + "arloUserGroup/2/list_users/")
    self.assertEqual(response.status_code, 200)
    data = json.loads(response.content)
    self.assertEqual(data.get('count'), 1)
    results = data.get('results')
    for result in results:
      if result['id'] not in [2]:
        self.fail("Unknown ID Found ({})".format(id))

    # Add user back to group
    response = client.post(baseUrl + "arloUserGroup/2/add_user/", json.dumps({'name':'adminuser'}), content_type="application/json")
    self.assertEqual(response.status_code, 201)

    # List users again, ensuring '1' is back
    response = client.get(baseUrl + "arloUserGroup/2/list_users/")
    self.assertEqual(response.status_code, 200)
    data = json.loads(response.content)
    self.assertEqual(data.get('count'), 2)
    results = data.get('results')
    for result in results:
      if result['id'] not in [1,2]:
        self.fail("Unknown ID Found ({})".format(id))

    # Remove again, this time with delete method
    response = client.delete(baseUrl + "arloUserGroup/2/remove_user/", json.dumps({'id':1}), content_type="application/json")
    self.assertEqual(response.status_code, 204)

    # List users again, ensuring '1' is gone
    response = client.get(baseUrl + "arloUserGroup/2/list_users/")
    self.assertEqual(response.status_code, 200)
    data = json.loads(response.content)
    self.assertEqual(data.get('count'), 1)
    results = data.get('results')
    for result in results:
      if result['id'] not in [2]:
        self.fail("Unknown ID Found ({})".format(id))

    # Remove existing user from group - Should fail from Admin's Group
    response = client.post(baseUrl + "arloUserGroup/1/remove_user/", json.dumps({'id':1}), content_type="application/json")
    self.assertEqual(response.status_code, 403)

    # Remove again, this time with delete method - Should fail from Admin's Group
    response = client.delete(baseUrl + "arloUserGroup/1/remove_user/", json.dumps({'id':1}), content_type="application/json")
    self.assertEqual(response.status_code, 403)



  def test_otherUserAccess(self):
    client = APIClient()
    self.assertEqual(
      client.login(username=user2Name, password=user2Pass),
      True)

# TODO eventually add more checks for proper permissions

    # List users
    response = client.get(baseUrl + "arloUserGroup/1/list_users/")
    self.assertEqual(response.status_code, 200)
    data = json.loads(response.content)
    self.assertEqual(data.get('count'), 3)
    results = data.get('results')
    for result in results:
      if result['id'] == 1:
        self.assertEqual(result['username'], 'adminuser')
      elif result['id'] == 2:
        self.assertEqual(result['username'], 'normaluser')
      elif result['id'] == 3:
        self.assertEqual(result['username'], 'user2')
      else:
        self.fail("Unknown ID Found ({})".format(id))

    # Remove existing user from group - should Fail
    response = client.post(baseUrl + "arloUserGroup/1/remove_user/", json.dumps({'id':3}), content_type="application/json")
    self.assertEqual(response.status_code, 403)

    # Add user to group - should fail
    response = client.post(baseUrl + "arloUserGroup/2/add_user/", json.dumps({'name':'user2'}), content_type="application/json")
    self.assertEqual(response.status_code, 403)

    # Remove again, this time with delete methond
    response = client.delete(baseUrl + "arloUserGroup/1/remove_user/", json.dumps({'id':3}), content_type="application/json")
    self.assertEqual(response.status_code, 403)
