import json

from django.test import TestCase
from rest_framework.test import APIClient

from utils import Add_Update_Delete, Ensure_All_403_Or_Fail
from .config import *


class APITagClassTest(TestCase):
  fixtures = ['api_test_data.yaml']

  def test_unauthedRequests(self):
    client = APIClient()
    Ensure_All_403_Or_Fail(self, client, baseUrl + "tagClass/")
    Ensure_All_403_Or_Fail(self, client, baseUrl + "tagClass/1/")

  def test_adminUserAccess(self):
    client = APIClient()
    self.assertEqual(
      client.login(username=adminUserName, password=adminUserPass), 
      True)

    # List records
    response = client.get(baseUrl + "tagClass/")
    self.assertEqual(response.status_code, 200)
    data = json.loads(response.content)
    self.assertEqual(data.get('count'), 1)
    results = data.get('results')
    for result in results:
      if result['id'] == 1:
        self.assertEqual(result['user'], 1)
        self.assertEqual(result['project'], 1)
        self.assertEqual(result['className'], 'adminTagClass')
        self.assertEqual(result['displayName'], 'adminTagClass')

    # retrieve an existing record - test using Admin access
    # note we are using another user's for below test
    response = client.get(baseUrl + "tagClass/2/")
    self.assertEqual(response.status_code, 200)
    data = json.loads(response.content)
    self.assertEqual(data['id'], 2)
    self.assertEqual(data['user'], 2)
    self.assertEqual(data['project'], 2)
    self.assertEqual(data['className'], 'userTagClass')
    self.assertEqual(data['displayName'], 'userTagClass')

    # modify existing record
    data['className'] = 'newTagClass'
    response = client.put(baseUrl + "tagClass/2/", data)
    self.assertEqual(response.status_code, 200)
    data = json.loads(response.content)
    self.assertEqual(data.get('className'), 'newTagClass')

    # re-retrieve existing record
    # NOTE: we want to ensure the the user of an existing record 
    # isn't changed after the above update
    response = client.get(baseUrl + "tagClass/2/")
    self.assertEqual(response.status_code, 200)
    data = json.loads(response.content)
    self.assertEqual(data.get('id'), 2)
    self.assertEqual(data.get('className'), 'newTagClass')
    self.assertEqual(data.get('user'), 2)	

    # add a new record
    Add_Update_Delete(self, client, "className", "new", baseUrl + "tagClass/", {
      'project': 1, 
      'className': 'newTagClass', 
      'displayName': 'newTagClass', 
      })
    Add_Update_Delete(self, client, "displayName", "new", baseUrl + "tagClass/", {
      'project': 1, 
      'className': 'newTagClass', 
      'displayName': 'newTagClass', 
      })


  def test_normalUserAccess(self):
    client = APIClient()
    self.assertEqual(
      client.login(username=normalUserName, password=normalUserPass), 
      True)

    # List records
    response = client.get(baseUrl + "tagClass/")
    self.assertEqual(response.status_code, 200)
    data = json.loads(response.content)
    self.assertEqual(data.get('count'), 1)
    results = data.get('results')
    result = results[0]
    self.assertEqual(result.get('id'), 2)
    self.assertEqual(result.get('user'), 2)
    self.assertEqual(result.get('project'), 2)
    self.assertEqual(result.get('className'), 'userTagClass')
    self.assertEqual(result.get('displayName'), 'userTagClass')

    # denied record
    response = client.get(baseUrl + "tagClass/1/")
    self.assertEqual(response.status_code, 403)

    # retrieve an existing record
    response = client.get(baseUrl + "tagClass/2/")
    self.assertEqual(response.status_code, 200)
    data = json.loads(response.content)
    self.assertEqual(data.get('id'), 2)
    self.assertEqual(data['user'], 2)
    self.assertEqual(data['project'], 2)
    self.assertEqual(data['className'], 'userTagClass')
    self.assertEqual(data['displayName'], 'userTagClass')

    # retrieve an existing record
    response = client.get(baseUrl + "tagClass/2/")
    self.assertEqual(response.status_code, 200)
    data = json.loads(response.content)
    self.assertEqual(data['id'], 2)
    self.assertEqual(data['user'], 2)
    self.assertEqual(data['project'], 2)
    self.assertEqual(data['className'], 'userTagClass')
    self.assertEqual(data['displayName'], 'userTagClass')

    # modify existing record
    data['className'] = 'newTagClass'
    response = client.put(baseUrl + "tagClass/2/", data)
    self.assertEqual(response.status_code, 200)
    data = json.loads(response.content)
    self.assertEqual(data.get('className'), 'newTagClass')

    # re-retrieve existing record
    # NOTE: we want to ensure the the user of an existing record 
    # isn't changed after the above update
    response = client.get(baseUrl + "tagClass/2/")
    self.assertEqual(response.status_code, 200)
    data = json.loads(response.content)
    self.assertEqual(data.get('id'), 2)
    self.assertEqual(data.get('className'), 'newTagClass')
    self.assertEqual(data.get('user'), 2)

    # Try creating a record in a different Project
    response = client.post(baseUrl + "tagClass/", {
      'project': 1, 
      'className': 'newTagClass', 
      'displayName': 'newTagClass', 
      })
    self.assertEqual(response.status_code, 403)

    # add a new record
    Add_Update_Delete(self, client, "className", "new", baseUrl + "tagClass/", {
      'project': 2, 
      'className': 'newTagClass', 
      'displayName': 'newTagClass', 
      })
    Add_Update_Delete(self, client, "displayName", "new", baseUrl + "tagClass/", {
      'project': 2, 
      'className': 'newTagClass', 
      'displayName': 'newTagClass', 
      })
      
  def test_otherUserAccess(self):
    client = APIClient()
    self.assertEqual(
      client.login(username=user2Name, password=user2Pass), 
      True)

    # test accessing the existing record from another user
    response = client.get(baseUrl + "tagClass/1/")
    self.assertEqual(response.status_code, 403)

    # test update
    response = client.put(baseUrl + "tagClass/1/", {})
    self.assertEqual(response.status_code, 403)

    # List records
    response = client.get(baseUrl + "tagClass/")
    self.assertEqual(response.status_code, 200)
    data = json.loads(response.content)
    self.assertEqual(data.get('count'), 1)
    results = data.get('results')
    result = results[0]
    self.assertEqual(result.get('id'), 2)
    self.assertEqual(result.get('user'), 2)
    self.assertEqual(result.get('project'), 2)
    self.assertEqual(result.get('className'), 'userTagClass')
    self.assertEqual(result.get('displayName'), 'userTagClass')

    # retrieve an existing record
    response = client.get(baseUrl + "tagClass/2/")
    self.assertEqual(response.status_code, 200)
    data = json.loads(response.content)
    self.assertEqual(data.get('id'), 2)
    self.assertEqual(data['user'], 2)
    self.assertEqual(data['project'], 2)
    self.assertEqual(data['className'], 'userTagClass')
    self.assertEqual(data['displayName'], 'userTagClass')

    # retrieve an existing record
    response = client.get(baseUrl + "tagClass/2/")
    self.assertEqual(response.status_code, 200)
    data = json.loads(response.content)
    self.assertEqual(data['id'], 2)
    self.assertEqual(data['user'], 2)
    self.assertEqual(data['project'], 2)
    self.assertEqual(data['className'], 'userTagClass')
    self.assertEqual(data['displayName'], 'userTagClass')

    # modify existing record - read-only access, should fail
    data['className'] = 'newTagClass'
    response = client.put(baseUrl + "tagClass/2/", data)
    self.assertEqual(response.status_code, 403)

    # Try creating a record in a different Project
    response = client.post(baseUrl + "tagClass/", {
      'project': 1,
      'className': 'newTagClass',
      'displayName': 'newTagClass',
      })
    self.assertEqual(response.status_code, 403)

