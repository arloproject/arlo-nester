import json

from django.test import TestCase
from rest_framework.test import APIClient

from .utils import Add_Update_Delete, Ensure_All_403_Or_Fail
from .config import *


class RandomWindowTest(TestCase):
  fixtures = ['api_test_data.yaml']

  def test_unauthedRequests(self):
    client = APIClient()
    Ensure_All_403_Or_Fail(self, client, baseUrl + "randomWindow/")
    Ensure_All_403_Or_Fail(self, client, baseUrl + "randomWindow/1/")

  def test_adminUserAccess(self):
    client = APIClient()
    self.assertEqual(
      client.login(username=adminUserName, password=adminUserPass),
      True)

    # List records - (empty)
    response = client.get(baseUrl + "randomWindow/")
    self.assertEqual(response.status_code, 200)
    data = json.loads(response.content)
    self.assertTrue('count' in data)
    self.assertEqual(data.get('count'), 0)

    # retrieve an existing record - admin override
    response = client.get(baseUrl + "randomWindow/1/")
    self.assertEqual(response.status_code, 200)
    data = json.loads(response.content)
    self.assertEqual(data.get('id'), 1)
    self.assertEqual(data.get('randomWindowTaggingJob'), 3)
    self.assertEqual(data.get('mediaFile'), 2)
    self.assertEqual(data.get('startTime'), 0.25)
    self.assertEqual(data.get('endTime'), 0.75)

    # modify existing record
    del data['id']
    data['startTime'] = 0.26
    response = client.put(baseUrl + "randomWindow/1/", data)
    self.assertEqual(response.status_code, 200)
    data = json.loads(response.content)
    self.assertEqual(data.get('startTime'), 0.26)

    # re-retrieve existing record
    response = client.get(baseUrl + "randomWindow/1/")
    self.assertEqual(response.status_code, 200)
    data = json.loads(response.content)
    self.assertEqual(data.get('id'), 1)
    self.assertEqual(data.get('randomWindowTaggingJob'), 3)
    self.assertEqual(data.get('mediaFile'), 2)
    self.assertEqual(data.get('startTime'), 0.26)
    self.assertEqual(data.get('endTime'), 0.75)

    # add a new record
    Add_Update_Delete(self, client, "endTime", 1.0, baseUrl + "randomWindow/", {
      "randomWindowTaggingJob": 3,
      "mediaFile": 2,
      "startTime": 0.20,
      "endTime": 0.70,
      })


  def test_normalUserAccess(self):
    client = APIClient()
    self.assertEqual(
      client.login(username=normalUserName, password=normalUserPass),
      True)

    # List records
    response = client.get(baseUrl + "randomWindow/")
    self.assertEqual(response.status_code, 200)
    data = json.loads(response.content)
    self.assertEqual(data.get('count'), 1)
    results = data.get('results')
    for result in results:
      id = result['id']
      if id == 1:
        self.assertEqual(result.get('id'), 1)
        self.assertEqual(result.get('randomWindowTaggingJob'), 3)
        self.assertEqual(result.get('mediaFile'), 2)
        self.assertEqual(result.get('startTime'), 0.25)
        self.assertEqual(result.get('endTime'), 0.75)
      else:
        self.fail("Unknown ID Found ({})".format(id))

    # retrieve an existing record
    response = client.get(baseUrl + "randomWindow/1/")
    self.assertEqual(response.status_code, 200)
    data = json.loads(response.content)
    self.assertEqual(data.get('id'), 1)
    self.assertEqual(data.get('randomWindowTaggingJob'), 3)
    self.assertEqual(data.get('mediaFile'), 2)
    self.assertEqual(data.get('startTime'), 0.25)
    self.assertEqual(data.get('endTime'), 0.75)

    # modify existing record
    del data['id']
    data['startTime'] = 0.26
    response = client.put(baseUrl + "randomWindow/1/", data)
    self.assertEqual(response.status_code, 200)
    data = json.loads(response.content)
    self.assertEqual(data.get('startTime'), 0.26)

    # re-retrieve existing record
    response = client.get(baseUrl + "randomWindow/1/")
    self.assertEqual(response.status_code, 200)
    data = json.loads(response.content)
    self.assertEqual(data.get('id'), 1)
    self.assertEqual(data.get('randomWindowTaggingJob'), 3)
    self.assertEqual(data.get('mediaFile'), 2)
    self.assertEqual(data.get('startTime'), 0.26)
    self.assertEqual(data.get('endTime'), 0.75)

    # add a new record
    Add_Update_Delete(self, client, "endTime", 1.0, baseUrl + "randomWindow/", {
      "randomWindowTaggingJob": 3,
      "mediaFile": 2,
      "startTime": 0.20,
      "endTime": 0.70,
      })


  def test_otherUserAccess(self):
    client = APIClient()
    self.assertEqual(
      client.login(username=user2Name, password=user2Pass), 
      True)

    # List records
    response = client.get(baseUrl + "randomWindow/")
    self.assertEqual(response.status_code, 200)
    data = json.loads(response.content)
    self.assertEqual(data.get('count'), 1)
    results = data.get('results')
    for result in results:
      id = result['id']
      if id == 1:
        self.assertEqual(result.get('id'), 1)
        self.assertEqual(result.get('randomWindowTaggingJob'), 3)
        self.assertEqual(result.get('mediaFile'), 2)
        self.assertEqual(result.get('startTime'), 0.25)
        self.assertEqual(result.get('endTime'), 0.75)
      else:
        self.fail("Unknown ID Found ({})".format(id))

    # retrieve an existing record
    response = client.get(baseUrl + "randomWindow/1/")
    self.assertEqual(response.status_code, 200)
    data = json.loads(response.content)
    self.assertEqual(data.get('id'), 1)
    self.assertEqual(data.get('randomWindowTaggingJob'), 3)
    self.assertEqual(data.get('mediaFile'), 2)
    self.assertEqual(data.get('startTime'), 0.25)
    self.assertEqual(data.get('endTime'), 0.75)

    # modify existing record - should fail since read-only user
    del data['id']
    data['startTime'] = 0.26
    response = client.put(baseUrl + "randomWindow/1/", data)
    self.assertEqual(response.status_code, 403)

    # add a new record - should fail since read-only user
    response = client.post(baseUrl + "randomWindow/", data)
    self.assertEqual(response.status_code, 403)

