import json

from django.test import TestCase
from rest_framework.test import APIClient

from utils import Add_Update_Delete, Ensure_All_403_Or_Fail
from .config import *


class APIMediaTypesTest(TestCase):
  fixtures = ['api_test_data']

  def test_unauthedRequests(self):
    client = APIClient()
    Ensure_All_403_Or_Fail(self, client, baseUrl + "mediaTypes/")
    Ensure_All_403_Or_Fail(self, client, baseUrl + "mediaTypes/1/")

  def userAccessTest(self, client):
    # Test the GET
    response = client.get(baseUrl + "mediaTypes/")
    self.assertEqual(response.status_code, 200)
    data = json.loads(response.content)
    self.assertEqual(data.get('count'), 5)
    results = data.get('results')
    for result in results:
      id = result['id']
      if id == 1:
        self.assertEqual(result['name'], 'audio')
      elif id == 2:
        self.assertEqual(result['name'], 'image')
      elif id == 3:
        self.assertEqual(result['name'], 'movement')
      elif id == 4:
        self.assertEqual(result['name'], 'text')
      elif id == 5:
        self.assertEqual(result['name'], 'video')
      else:
        self.fail("Unknown ID Found ({})".format(id))


    # POST should fail
    response = client.post(baseUrl + 'mediaTypes/', {})
    self.assertEqual(response.status_code, 405)
    
    # Test the GET
    response = client.get(baseUrl + "mediaTypes/1/")
    self.assertEqual(response.status_code, 200)
    data = json.loads(response.content)
    self.assertEqual(data.get('id'), 1)
    self.assertEqual(data.get('name'), 'audio')

    # PUT should fail
    response = client.post(baseUrl + 'mediaTypes/1/', {})
    self.assertEqual(response.status_code, 405)

  def test_AdminAccess(self):
    client = APIClient()
    self.assertEqual(
      client.login(username=adminUserName, password=adminUserPass),
      True)
    self.userAccessTest(client)

  def test_UserAccess(self):
    client = APIClient()
    self.assertEqual(
      client.login(username=normalUserName, password=normalUserPass),
      True)
    self.userAccessTest(client)

