import json

from django.test import TestCase
from rest_framework.test import APIClient

from .config import *
from .utils import Ensure_All_403_Or_Fail


class APITagNavigationByTimeTest(TestCase):
  fixtures = ['api_test_data.yaml']

  sampleParams = {
                  'date': '',
                  'tagClass': 2,
                  'tagSets': '2',
                  'relation': 'first',
                 }

  def test_unauthedRequests(self):
    client = APIClient()
    Ensure_All_403_Or_Fail(self, client, baseUrl + "tagNavigationByTime/1/")


  def test_permissionDenied(self):
    client = APIClient()
    self.assertEqual(
      client.login(username=normalUserName, password=normalUserPass),
      True)

    # Deny Other Project
    response = client.get(baseUrl + "tagNavigationByTime/1/", data=self.sampleParams)
    self.assertEqual(response.status_code, 401)


  def test_successes(self):
    client = APIClient()
    self.assertEqual(
      client.login(username=adminUserName, password=adminUserPass),
      True)

    # First
    response = client.get(baseUrl + "tagNavigationByTime/1/", data={
                 'tagClass': 1,
                 'tagSets': 1,
                 'relation': 'first',
                 })
    self.assertEqual(response.status_code, 200)
    data = json.loads(response.content)
    self.assertEqual(data.get('tagId'), 1)
    self.assertEqual(data.get('realStartTime'), '2012-01-01 12:34:57')
    self.assertEqual(data.get('duration'), 2)
    self.assertEqual(data.get('mediaFileId'), 1)

    # Last
    response = client.get(baseUrl + "tagNavigationByTime/1/", data={
                 'date': '',
                 'tagClass': 1,
                 'tagSets': '1',
                 'relation': 'last',
                 })
    self.assertEqual(response.status_code, 200)
    data = json.loads(response.content)
    self.assertEqual(data.get('tagId'), 1)

    # Next
    response = client.get(baseUrl + "tagNavigationByTime/1/", data={
                 'date': '2012-01-01 12:34:56',
                 'tagClass': 1,
                 'tagSets': '1',
                 'relation': 'next',
                 })
    self.assertEqual(response.status_code, 200)
    data = json.loads(response.content)
    self.assertEqual(data.get('tagId'), 1)

    # Previous
    response = client.get(baseUrl + "tagNavigationByTime/1/", data={
                 'date': '2012-01-01 12:34:58',
                 'tagClass': 1,
                 'tagSets': '1',
                 'relation': 'previous',
                 })
    self.assertEqual(response.status_code, 200)
    data = json.loads(response.content)
    self.assertEqual(data.get('tagId'), 1)

    ###
    # No Tags before of after

    # Next
    response = client.get(baseUrl + "tagNavigationByTime/1/", data={
                 'date': '2012-01-01 12:34:58',
                 'tagClass': 1,
                 'tagSets': '1',
                 'relation': 'next',
                 })
    self.assertEqual(response.status_code, 200)
    data = json.loads(response.content)
    self.assertEqual(data.get('tagId'), None)

    # Previous
    response = client.get(baseUrl + "tagNavigationByTime/1/", data={
                 'date': '2012-01-01 12:34:56',
                 'tagClass': 1,
                 'tagSets': '1',
                 'relation': 'previous',
                 })
    self.assertEqual(response.status_code, 200)
    data = json.loads(response.content)
    self.assertEqual(data.get('tagId'), None)


  def test_empties(self):
    client = APIClient()
    self.assertEqual(
      client.login(username=normalUserName, password=normalUserPass),
      True)


    ###
    # This project has no MediaFiles with start time - all should be empty

    # First
    response = client.get(baseUrl + "tagNavigationByTime/2/", data={
                 'date': '',
                 'tagClass': 2,
                 'tagSets': '2',
                 'relation': 'first',
                 })
    self.assertEqual(response.status_code, 200)
    data = json.loads(response.content)
    self.assertEqual(data.get('tagId'), None)
    self.assertEqual(data.get('realStartTime'), None)
    self.assertEqual(data.get('duration'), None)
    self.assertEqual(data.get('mediaFileId'), None)

    # Last
    response = client.get(baseUrl + "tagNavigationByTime/2/", data={
                 'tagClass': 2,
                 'tagSets': '2',
                 'relation': 'last',
                 })
    self.assertEqual(response.status_code, 200)
    data = json.loads(response.content)
    self.assertEqual(data.get('tagId'), None)

    # Next
    response = client.get(baseUrl + "tagNavigationByTime/2/", data={
                 'date': '2012-01-01 12:34:55',
                 'tagClass': 2,
                 'tagSets': '2',
                 'relation': 'next',
                 })
    self.assertEqual(response.status_code, 200)
    data = json.loads(response.content)
    self.assertEqual(data.get('tagId'), None)

    # Previous
    response = client.get(baseUrl + "tagNavigationByTime/2/", data={
                 'date': '2012-01-01 12:34:57',
                 'tagClass': 2,
                 'tagSets': '2',
                 'relation': 'previous',
                 })
    self.assertEqual(response.status_code, 200)
    data = json.loads(response.content)
    self.assertEqual(data.get('tagId'), None)


  def test_missingData(self):
    client = APIClient()
    self.assertEqual(
      client.login(username=normalUserName, password=normalUserPass),
      True)

    response = client.get(baseUrl + "tagNavigationByTime/2/", data={
                 'tagClass': 2,
                 'relation': 'first',
                 })
    self.assertEqual(response.status_code, 400)

    response = client.get(baseUrl + "tagNavigationByTime/2/", data={
                 'date': '',
                 'tagSets': '2',
                 'relation': 'first',
                 })
    self.assertEqual(response.status_code, 400)

    response = client.get(baseUrl + "tagNavigationByTime/2/", data={
                 'date': '',
                 'tagClass': 2,
                 'tagSets': '2',
                 })
    self.assertEqual(response.status_code, 400)

    response = client.get(baseUrl + "tagNavigationByTime/2/", data={
                 'date': '',
                 'tagClass': 2,
                 'tagSets': '2',
                 'relation': 'next',
                 })
    self.assertEqual(response.status_code, 400)

    response = client.get(baseUrl + "tagNavigationByTime/2/", data={
                 'tagClass': 2,
                 'tagSets': '2',
                 'relation': 'previous',
                 })
    self.assertEqual(response.status_code, 400)

    response = client.get(baseUrl + "tagNavigationByTime/2/", data={
                 'date': '',
                 'relation': 'first',
                 })
    self.assertEqual(response.status_code, 400)
