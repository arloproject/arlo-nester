import json

from django.test import TestCase
from rest_framework.test import APIClient

from .utils import Add_Update_Delete, Ensure_All_403_Or_Fail
from .config import *


class APISensorTest(TestCase):
  fixtures = ['api_test_data.yaml']

  def test_unauthedRequests(self):
    client = APIClient()
    Ensure_All_403_Or_Fail(self, client, baseUrl + "sensor/")
    Ensure_All_403_Or_Fail(self, client, baseUrl + "sensor/1/")

  def test_adminUserAccess(self):
    client = APIClient()
    self.assertEqual(
      client.login(username=adminUserName, password=adminUserPass),
      True)

    # List records
    response = client.get(baseUrl + "sensor/")
    self.assertEqual(response.status_code, 200)
    data = json.loads(response.content)
    self.assertEqual(data.get('count'), 1)
    results = data.get('results')
    for result in results:
      id = result['id']
      if id == 1:
        self.assertEqual(result['sensorArray'], 1)
        self.assertEqual(result['name'], 'AdminSensorA')
        self.assertEqual(result['x'], 1)
        self.assertEqual(result['y'], 2)
        self.assertEqual(result['z'], 3)
        self.assertEqual(result['layoutRow'], 1)
        self.assertEqual(result['layoutColumn'], 2)
      else:
        self.fail("Unknown ID Found ({})".format(id))


    # retrieve an existing record - admin override
    # note we are using another user's for below test
    response = client.get(baseUrl + "sensor/2/")
    self.assertEqual(response.status_code, 200)
    data = json.loads(response.content)
    self.assertEqual(data.get('id'), 2)
    self.assertEqual(data.get('name'), 'UserSensorA')

    # modify existing record
    data['name'] = 'newSensor'
    response = client.put(baseUrl + "sensor/2/", data)
    self.assertEqual(response.status_code, 200)
    data = json.loads(response.content)
    self.assertEqual(data.get('name'), 'newSensor')

    # re-retrieve existing record
    # NOTE: we want to ensure the the user of an existing record
    # isn't changed after the above update
    response = client.get(baseUrl + "sensor/2/")
    self.assertEqual(response.status_code, 200)
    data = json.loads(response.content)
    self.assertEqual(data.get('id'), 2)
    self.assertEqual(data.get('name'), 'newSensor')
    self.assertEqual(data.get('layoutRow'), 1)
    self.assertEqual(data.get('layoutColumn'), 2)

    # add a new record
    Add_Update_Delete(self, client, "name", "newName", baseUrl + "sensor/", {
      "sensorArray": 1,
      "name": "new",
      "x": 12,
      "y": 13,
      "z": 14,
      "layoutRow": 1,
      "layoutColumn": 2,
      })


  def test_normalUserAccess(self):
    client = APIClient()
    self.assertEqual(
      client.login(username=normalUserName, password=normalUserPass),
      True)

    # list records
    response = client.get(baseUrl + "sensor/")
    self.assertEqual(response.status_code, 200)
    data = json.loads(response.content)
    self.assertEqual(data.get('count'), 2)
    results = data.get('results')
    for result in results:
      id = result['id']
      if id == 2:
        self.assertEqual(result['sensorArray'], 2)
        self.assertEqual(result['name'], 'UserSensorA')
        self.assertEqual(result['x'], 4)
        self.assertEqual(result['y'], 5)
        self.assertEqual(result['z'], 6)
        self.assertEqual(result['layoutRow'], 1)
        self.assertEqual(result['layoutColumn'], 2)
      elif id == 3:
        self.assertEqual(result['sensorArray'], 2)
        self.assertEqual(result['name'], 'UserSensorB')
        self.assertEqual(result['x'], 7)
        self.assertEqual(result['y'], 8)
        self.assertEqual(result['z'], 9)
        self.assertEqual(result['layoutRow'], 3)
        self.assertEqual(result['layoutColumn'], 4)
      else:
        self.fail("Unknown ID Found ({})".format(id))

    # retrieve an existing record
    response = client.get(baseUrl + "sensor/2/")
    self.assertEqual(response.status_code, 200)
    data = json.loads(response.content)
    self.assertEqual(data.get('id'), 2)
    self.assertEqual(data.get('name'), 'UserSensorA')

    # add a new record
    Add_Update_Delete(self, client, "name", "newName", baseUrl + "sensor/", {
      "sensorArray": 2,
      "name": "new",
      "x": 12,
      "y": 13,
      "z": 14,
      "layoutRow": 1,
      "layoutColumn": 2,
      })

  def test_otherUserAccess(self):
    client = APIClient()
    self.assertEqual(
      client.login(username=user2Name, password=user2Pass),
      True)
    response = client.get(baseUrl + "sensor/")
    self.assertEqual(response.status_code, 200)
    data = json.loads(response.content)
    self.assertEqual(data.get('count'), 0)

    # test accessing the existing record from another user
    response = client.get(baseUrl + "sensor/1/")
    self.assertEqual(response.status_code, 403)

    # test update
    response = client.put(baseUrl + "sensor/1/", {})
    self.assertEqual(response.status_code, 403)

