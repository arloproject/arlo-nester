import json

from django.test import TestCase
from rest_framework.test import APIClient

from .utils import Add_Update_Delete, Ensure_All_403_Or_Fail
from .config import *


class APITagTest(TestCase):
  fixtures = ['api_test_data.yaml']

  def test_unauthedRequests(self):
    client = APIClient()
    Ensure_All_403_Or_Fail(self, client, baseUrl + "tag/")
    Ensure_All_403_Or_Fail(self, client, baseUrl + "tag/1/")

  def test_adminUserAccess(self):
    client = APIClient()
    self.assertEqual(
      client.login(username=adminUserName, password=adminUserPass),
      True)

    # List records
    response = client.get(baseUrl + "tag/")
    self.assertEqual(response.status_code, 200)
    data = json.loads(response.content)
    self.assertEqual(data.get('count'), 2)
    results = data.get('results')
    for result in results:
      if result['id'] == 1:
        self.assertEqual(result['user'], 1)
        self.assertEqual(result['creationDate'], '2012-01-01T01:23:45')
        self.assertEqual(result['tagClass'], 1)
        self.assertEqual(result['tagSet'], 1)
        self.assertEqual(result['mediaFile'], 1)
        self.assertEqual(result['startTime'], 1)
        self.assertEqual(result['endTime'], 3)
        self.assertEqual(result['minFrequency'], 20)
        self.assertEqual(result['maxFrequency'], 20000)
        self.assertEqual(result['parentTag'], None)
        self.assertEqual(result['randomlyChosen'], False)
        self.assertEqual(result['machineTagged'], False)
        self.assertEqual(result['userTagged'], True)
        self.assertEqual(result['strength'], 1)
      if result['id'] == 2:
        self.assertEqual(result['user'], 2)
        self.assertEqual(result['creationDate'], '2012-01-01T01:23:45')
        self.assertEqual(result['tagClass'], 2)
        self.assertEqual(result['tagSet'], 2)
        self.assertEqual(result['mediaFile'], 2)
        self.assertEqual(result['startTime'], 2)
        self.assertEqual(result['endTime'], 4)
        self.assertEqual(result['minFrequency'], 20)
        self.assertEqual(result['maxFrequency'], 20000)
        self.assertEqual(result['parentTag'], None)
        self.assertEqual(result['randomlyChosen'], False)
        self.assertEqual(result['machineTagged'], True)
        self.assertEqual(result['userTagged'], False)
        self.assertEqual(result['strength'], 0.5)

    # retrieve an existing record
    # note we are using another user's for below test
    response = client.get(baseUrl + "tag/2/")
    self.assertEqual(response.status_code, 200)
    data = json.loads(response.content)
    self.assertEqual(data['user'], 2)
    self.assertEqual(data['creationDate'], '2012-01-01T01:23:45')
    self.assertEqual(data['tagClass'], 2)
    self.assertEqual(data['tagSet'], 2)
    self.assertEqual(data['mediaFile'], 2)
    self.assertEqual(data['startTime'], 2)
    self.assertEqual(data['endTime'], 4)
    self.assertEqual(data['minFrequency'], 20)
    self.assertEqual(data['maxFrequency'], 20000)
    self.assertEqual(data['parentTag'], None)
    self.assertEqual(data['randomlyChosen'], False)
    self.assertEqual(data['machineTagged'], True)
    self.assertEqual(data['userTagged'], False)
    self.assertEqual(data['strength'], 0.5)

    # modify existing record
    data['strength'] = 0.75

    # use JSON to handle the nulls
    response = client.put(baseUrl + "tag/2/", json.dumps(data), content_type="application/json")
    self.assertEqual(response.status_code, 200)
    data = json.loads(response.content)
    self.assertEqual(data.get('strength'), 0.75)

    # re-retrieve existing record
    # NOTE: we want to ensure the the user of an existing record
    # isn't changed after the above update
    response = client.get(baseUrl + "tag/2/")
    self.assertEqual(response.status_code, 200)
    data = json.loads(response.content)
    self.assertEqual(data.get('id'), 2)
    self.assertEqual(data.get('strength'), 0.75)
    self.assertEqual(data.get('user'), 2)

    # add a new record
    Add_Update_Delete(self, client, "machineTagged", False, baseUrl + "tag/", {
      'tagClass': 1,
      'tagSet': 1,
      'mediaFile': 1,
      'startTime': 2,
      'endTime': 4,
      'minFrequency': 20,
      'maxFrequency': 20000,
      'parentTag': None,
      'randomlyChosen': False,
      'machineTagged': True,
      'userTagged': False,
      'strength': 0.50,
      })


  def test_normalUserAccess(self):
    client = APIClient()
    self.assertEqual(
      client.login(username=normalUserName, password=normalUserPass),
      True)

    # list records
    response = client.get(baseUrl + "tag/")
    self.assertEqual(response.status_code, 403)

    # denied record
    response = client.get(baseUrl + "tag/1/")
    self.assertEqual(response.status_code, 403)

    # retrieve an existing record
    response = client.get(baseUrl + "tag/2/")
    self.assertEqual(response.status_code, 200)
    data = json.loads(response.content)
    self.assertEqual(data['user'], 2)
    self.assertEqual(data['creationDate'], '2012-01-01T01:23:45')
    self.assertEqual(data['tagClass'], 2)
    self.assertEqual(data['tagSet'], 2)
    self.assertEqual(data['mediaFile'], 2)
    self.assertEqual(data['startTime'], 2)
    self.assertEqual(data['endTime'], 4)
    self.assertEqual(data['minFrequency'], 20)
    self.assertEqual(data['maxFrequency'], 20000)
    self.assertEqual(data['parentTag'], None)
    self.assertEqual(data['randomlyChosen'], False)
    self.assertEqual(data['machineTagged'], True)
    self.assertEqual(data['userTagged'], False)
    self.assertEqual(data['strength'], 0.5)

    # modify existing record
    data['strength'] = 0.75

    # use JSON to handle the nulls
    response = client.put(baseUrl + "tag/2/", json.dumps(data), content_type="application/json")
    self.assertEqual(response.status_code, 200)
    data = json.loads(response.content)
    self.assertEqual(data.get('strength'), 0.75)

    # re-retrieve existing record
    # NOTE: we want to ensure the the user of an existing record
    # isn't changed after the above update
    response = client.get(baseUrl + "tag/2/")
    self.assertEqual(response.status_code, 200)
    data = json.loads(response.content)
    self.assertEqual(data.get('id'), 2)
    self.assertEqual(data.get('strength'), 0.75)
    self.assertEqual(data.get('user'), 2)

    # add a new record
    Add_Update_Delete(self, client, "machineTagged", False, baseUrl + "tag/", {
      'tagClass': 2,
      'tagSet': 2,
      'mediaFile': 2,
      'startTime': 1,
      'endTime': 3,
      'minFrequency': 20,
      'maxFrequency': 20000,
      'parentTag': None,
      'randomlyChosen': False,
      'machineTagged': True,
      'userTagged': False,
      'strength': 0.50,
      })


  def test_otherUserAccess(self):
    client = APIClient()
    self.assertEqual(
      client.login(username=user2Name, password=user2Pass),
      True)
    response = client.get(baseUrl + "tag/")
    self.assertEqual(response.status_code, 403)

    # test accessing the existing record from another user
    response = client.get(baseUrl + "tag/1/")
    self.assertEqual(response.status_code, 403)

    # test update
    response = client.put(baseUrl + "tag/1/", {})
    self.assertEqual(response.status_code, 403)

