import json

from django.test import TestCase
from rest_framework.test import APIClient

from tools.models import JobStatusTypes
from .utils import Ensure_All_403_Or_Fail
from .config import *


# Note the use of TransactionTestCase to support select_for_update() tests.
# https://docs.djangoproject.com/en/1.8/topics/testing/tools/#django.test.TransactionTestCase
class APIGetQueuedJobTest(TestCase):
    fixtures = ['api_test_jobs.yaml']

    def test_unauthedRequests(self):
        client = APIClient()
        Ensure_All_403_Or_Fail(self, client, baseUrl + "getQueuedJob/")

    def test_adminUserAccess(self):

        # Admin user simulates our 'system' user, which should return Jobs
        # from ANY user.

        client = APIClient()
        self.assertEqual(
            client.login(username=adminUserName, password=adminUserPass),
            True)

        # Make sure missing parameters throw an error
        response = client.post(baseUrl + "getQueuedJob/")
        self.assertEqual(response.status_code, 400)

        # ensure GET fails
        response = client.get(baseUrl + "getQueuedJob/", {
            'jobTypes': [1],
            })
        self.assertEqual(response.status_code, 405)

        # looked for the one queued SupervisedTagDisc Job
        response = client.post(baseUrl + "getQueuedJob/", {
            'jobTypes': [1],
            })
        self.assertEqual(response.status_code, 200)
        data = json.loads(response.content)
        self.assertTrue(isinstance(data, dict))
        self.assertEqual(data.get('id'), 11)
        self.assertEqual(data.get('status'), JobStatusTypes.objects.get(name="Assigned").pk)

        # Now that the above Job is 'Assigned', we shouldn't get any Jobs
        response = client.post(baseUrl + "getQueuedJob/", {
            'jobTypes': [1],
            })
        self.assertEqual(response.status_code, 204)

        # ensure listing multiple jobTypes works
        response = client.post(baseUrl + "getQueuedJob/", {
            'jobTypes': [2,3],
            })
        self.assertEqual(response.status_code, 200)
        data = json.loads(response.content)
        self.assertTrue(isinstance(data, dict))
        self.assertEqual(data.get('id'), 12)

        ###
        # test Plugin jobs, where we have to provide a name

        # no name provided should return nothing, also test JSON format
        response = client.post(baseUrl + "getQueuedJob/", {
            'jobTypes': [15],
            }, format='json')
        self.assertEqual(response.status_code, 204)

        # non-matching name returns nothing, also JSON
        response = client.post(baseUrl + "getQueuedJob/", {
            'jobTypes': [15],
            'jobPluginNames': 'PluginNameThatDoesntExist',
            }, format='json')
        self.assertEqual(response.status_code, 204)

        # Shorten the name of the PluginNames, should only match exact
        response = client.post(baseUrl + "getQueuedJob/", {
            'jobTypes': [15],
            'jobPluginNames': ['Test'],
            })
        self.assertEqual(response.status_code, 204)

        # Extend the name of the PluginNames, should only match exact
        response = client.post(baseUrl + "getQueuedJob/", {
            'jobTypes': [15],
            'jobPluginNames': ['TestPluginNameOnly1OfThisPluginzzzzz'],
            })
        self.assertEqual(response.status_code, 204)

        # Only one Job should match
        response = client.post(baseUrl + "getQueuedJob/", {
            'jobTypes': [15],
            'jobPluginNames': ['TestPluginNameOnly1OfThisPlugin'],
            })
        self.assertEqual(response.status_code, 200)
        data = json.loads(response.content)
        self.assertTrue(isinstance(data, dict))
        self.assertEqual(data.get('id'), 21)

        # There are two instances of the following Plugin name - make sure we
        # get both, and try with and without a list parameter. '23' should be
        # first with priority

        response = client.post(baseUrl + "getQueuedJob/", {
            'jobTypes': [15],
            'jobPluginNames': ['TwoOfThesePluginNames'],
            })
        self.assertEqual(response.status_code, 200)
        data = json.loads(response.content)
        self.assertTrue(isinstance(data, dict))
        self.assertEqual(data.get('id'), 23)

        response = client.post(baseUrl + "getQueuedJob/", {
            'jobTypes': [15],
            'jobPluginNames': ['TwoOfThesePluginNames'],
            })
        self.assertEqual(response.status_code, 200)
        data = json.loads(response.content)
        self.assertTrue(isinstance(data, dict))
        self.assertEqual(data.get('id'), 22)

        # Should be none that match left
        response = client.post(baseUrl + "getQueuedJob/", {
            'jobTypes': [15],
            'jobPluginNames': ['TwoOfThesePluginNames'],
            })
        self.assertEqual(response.status_code, 204)


    def test_normalUserAccess(self):
        client = APIClient()
        self.assertEqual(
            client.login(username=normalUserName, password=normalUserPass),
            True)

        # Make sure we don't get assigned the Admin's Job
        response = client.post(baseUrl + "getQueuedJob/", {
            'jobTypes': [1],
            })
        self.assertEqual(response.status_code, 204)

    def test_otherUserAccess(self):
        client = APIClient()
        self.assertEqual(
            client.login(username=user2Name, password=user2Pass),
            True)

        # Make sure we don't get assigned any Job, as we have no perms
        response = client.post(baseUrl + "getQueuedJob/", {
            'jobTypes': range(1, 16),
            })
        self.assertEqual(response.status_code, 204)
