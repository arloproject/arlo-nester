import json

from django.test import TestCase
from rest_framework.test import APIClient

from .utils import Ensure_All_403_Or_Fail
from .config import *


###################################################
#                                                 #
#                    Functions                    #
#                                                 #
###################################################


class APIGetTagsInAudioSegmentTest(TestCase):
  fixtures = ['api_test_getTagsInAudioSegment.yaml']

  def test_unauthedRequests(self):
    client = APIClient()
    Ensure_All_403_Or_Fail(self, client, baseUrl + "getTagsInAudioSegment/2/")

  def test_adminUserAccess(self):

    client = APIClient()
    self.assertEqual(
      client.login(username=normalUserName, password=normalUserPass),
      True)

    # Make sure missing parameters throw an error
    response = client.get(baseUrl + "getTagsInAudioSegment/1/")
    self.assertEqual(response.status_code, 400)


    # List records in mediaFile 1 from Project 1
    response = client.get(baseUrl + "getTagsInAudioSegment/1/", {
      'mediaFile': 1,
      })
    self.assertEqual(response.status_code, 200)
    data = json.loads(response.content)
    self.assertEqual(len(data), 2)
    for result in data:
      if result['id'] == 1:
        self.assertEqual(result['userId'], 1)
      if result['id'] == 2:
        self.assertEqual(result['userId'], 2)

    # List records in mediaFile 1 from Project 2
    response = client.get(baseUrl + "getTagsInAudioSegment/2/", {
      'mediaFile': 1,
      })
    self.assertEqual(response.status_code, 200)
    data = json.loads(response.content)
    self.assertEqual(len(data), 0)

