import json

from django.test import TestCase
from rest_framework.test import APIClient

from .utils import Ensure_All_403_Or_Fail
from .config import *


class APIUserTest(TestCase):
  fixtures = ['api_test_data']

  def test_unauthedRequests(self):
    client = APIClient()
    Ensure_All_403_Or_Fail(self, client, baseUrl + "user/")
    Ensure_All_403_Or_Fail(self, client, baseUrl + "user/1/")

  def test_AdminAccess(self):
    client = APIClient()
    self.assertEqual(
      client.login(username=adminUserName, password=adminUserPass),
      True)

    self.run_tests(client)


  def test_normalUser(self):
    client = APIClient()
    self.assertEqual(
      client.login(username=normalUserName, password=normalUserPass),
      True)

    self.run_tests(client)


  def run_tests(self, client):
    # Test the GET
    response = client.get(baseUrl + "user/")
    self.assertEqual(response.status_code, 200)
    data = json.loads(response.content)
    self.assertTrue('count' in data)
    self.assertEqual(data.get('count'), 5)
    self.assertTrue('results' in data)
    results = data.get('results')
    for result in results:
      self.assertTrue('id' in result)
      self.assertTrue('username' in result)
      if (result['id'] == 1):
        self.assertEqual(result['username'], 'adminuser')
      if (result['id'] == 2):
        self.assertEqual(result['username'], 'normaluser')
      if (result['id'] == 3):
        self.assertEqual(result['username'], 'user2')

    # POST should fail
    response = client.post(baseUrl + 'user/', {})
    self.assertEqual(response.status_code, 403)

    # Test the GET
    response = client.get(baseUrl + "user/1/")
    self.assertEqual(response.status_code, 200)
    data = json.loads(response.content)
    self.assertTrue('id' in data)
    self.assertEqual(data.get('id'), 1)
    self.assertTrue('username' in data)
    self.assertEqual(data.get('username'), 'adminuser')

    # PUT should fail
    response = client.post(baseUrl + 'user/1/', {})
    self.assertEqual(response.status_code, 403)

    # Test the django-filter API query, lookup 'user2'
    response = client.get(baseUrl + "user/?username=user2")
    self.assertEqual(response.status_code, 200)
    data = json.loads(response.content)
    self.assertTrue('count' in data)
    self.assertEqual(data.get('count'), 1)
    self.assertTrue('results' in data)
    results = data.get('results')
    for result in results:
      self.assertTrue(result['id'], 3)
      self.assertEqual(result['username'], 'user2')

