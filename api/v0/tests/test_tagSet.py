import json

from django.test import TestCase
from rest_framework.test import APIClient

from .utils import Add_Update_Delete, Ensure_All_403_Or_Fail
from .config import *


class APITagSetTest(TestCase):
  fixtures = ['api_test_data.yaml']

  def test_unauthedRequests(self):
    client = APIClient()
    Ensure_All_403_Or_Fail(self, client, baseUrl + "tagSet/")
    Ensure_All_403_Or_Fail(self, client, baseUrl + "tagSet/1/")

  def test_adminUserAccess(self):
    client = APIClient()
    self.assertEqual(
      client.login(username=adminUserName, password=adminUserPass),
      True)

    # List records
    response = client.get(baseUrl + "tagSet/")
    self.assertEqual(response.status_code, 200)
    data = json.loads(response.content)
    self.assertTrue('count' in data)
    self.assertEqual(data.get('count'), 1)
    results = data.get('results')
    self.assertEqual(results[0]['id'], 1)
    self.assertEqual(results[0]['user'], 1)
    self.assertEqual(results[0]['name'], 'adminTagSet')
    self.assertEqual(results[0]['project'], 1)
    self.assertEqual(results[0]['creationDate'], '2012-01-01T01:23:45')

    # retrieve an existing record - ADMIN test
    # note we are using another user's for below test
    response = client.get(baseUrl + "tagSet/2/")
    self.assertEqual(response.status_code, 200)
    data = json.loads(response.content)
    self.assertEqual(data.get('id'), 2)
    self.assertEqual(data.get('name'), 'userTagSet')
    self.assertEqual(data.get('project'), 2)
    self.assertEqual(data.get('user'), 2)

    # modify existing record
    data['name'] = 'newTagSet'
    response = client.put(baseUrl + "tagSet/2/", data)
    self.assertEqual(response.status_code, 200)
    data = json.loads(response.content)
    self.assertTrue('name' in data)
    self.assertEqual(data.get('name'), 'newTagSet')

    # re-retrieve existing record
    # NOTE: we want to ensure the the user of an existing record
    # isn't changed after the above update
    response = client.get(baseUrl + "tagSet/2/")
    self.assertEqual(response.status_code, 200)
    data = json.loads(response.content)
    self.assertEqual(data.get('id'), 2)
    self.assertEqual(data.get('name'), 'newTagSet')
    self.assertEqual(data.get('user'), 2)

    # add a new record
    Add_Update_Delete(self, client, "name", "newName", baseUrl + "tagSet/", {
      "name": "new",
      "project": 1,
      })


  def test_normalUserAccess(self):
    client = APIClient()
    self.assertEqual(
      client.login(username=normalUserName, password=normalUserPass),
      True)

    # list records
    response = client.get(baseUrl + "tagSet/")
    self.assertEqual(response.status_code, 200)
    data = json.loads(response.content)
    self.assertTrue('count' in data)
    self.assertEqual(data.get('count'), 2)
    results = data.get('results')
    for result in results:
      id = result['id']
      if id == 2:
        self.assertEqual(results[0]['user'], 2)
        self.assertEqual(results[0]['name'], 'userTagSet')
        self.assertEqual(results[0]['project'], 2)
      elif id == 3:
        self.assertEqual(results[0]['project'], 2)
      else:
        self.fail("Unknown ID Found ({})".format(id))

    # denied record
    response = client.get(baseUrl + "tagSet/1/")
    self.assertEqual(response.status_code, 403)

    # retrieve an existing record
    response = client.get(baseUrl + "tagSet/2/")
    self.assertEqual(response.status_code, 200)
    data = json.loads(response.content)
    self.assertEqual(data.get('id'), 2)
    self.assertEqual(data.get('name'), 'userTagSet')
    self.assertEqual(data.get('project'), 2)
    self.assertEqual(data.get('user'), 2)

    # Try to create a TagSet in a different Project
    response = client.post(baseUrl + "tagSet/", {
      "name": "new",
      "project": 1,
      })
    self.assertEqual(response.status_code, 403)

    # add a new record
    Add_Update_Delete(self, client, "name", "newName", baseUrl + "tagSet/", {
      "name": "new",
      "project": 2,
      })

  def test_otherUserAccess(self):
    client = APIClient()
    self.assertEqual(
      client.login(username=user2Name, password=user2Pass),
      True)

    # list
    response = client.get(baseUrl + "tagSet/")
    self.assertEqual(response.status_code, 200)
    data = json.loads(response.content)
    self.assertEqual(data.get('count'), 2)
    results = data.get('results')
    for result in results:
      id = result['id']
      if id == 2:
        self.assertEqual(results[0]['user'], 2)
        self.assertEqual(results[0]['name'], 'userTagSet')
        self.assertEqual(results[0]['project'], 2)
      elif id == 3:
        self.assertEqual(results[0]['project'], 2)
      else:
        self.fail("Unknown ID Found ({})".format(id))

    # test accessing the existing record from another user
    response = client.get(baseUrl + "tagSet/1/")
    self.assertEqual(response.status_code, 403)

    # test update
    response = client.put(baseUrl + "tagSet/1/", {})
    self.assertEqual(response.status_code, 403)

