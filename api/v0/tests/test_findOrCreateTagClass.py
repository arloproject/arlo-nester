import json

from django.test import TestCase
from rest_framework.test import APIClient

from .config import *
from .utils import Ensure_All_403_Or_Fail


class APIFindOrCreateTagClassTest(TestCase):
  fixtures = ['api_test_data.yaml']

  def test_unauthedRequests(self):
    client = APIClient()
    Ensure_All_403_Or_Fail(self, client, baseUrl + "findOrCreateTagClass/1/")


  def test_permissionDenied(self):
    client = APIClient()
    self.assertEqual(
      client.login(username=normalUserName, password=normalUserPass),
      True)

    # Deny Existing TagClass
    response = client.post(baseUrl + "findOrCreateTagClass/1/", data={'tagClassName': 'adminTagClass'})
    self.assertEqual(response.status_code, 401)

    # Deny New TagClass
    response = client.post(baseUrl + "findOrCreateTagClass/1/", data={'tagClassName': 'newTagClass'})
    self.assertEqual(response.status_code, 401)


  def test_createTagClass(self):
    client = APIClient()
    self.assertEqual(
      client.login(username=normalUserName, password=normalUserPass),
      True)

    response = client.post(baseUrl + "findOrCreateTagClass/2/", data={'tagClassName': 'newTagClass'})
    self.assertEqual(response.status_code, 201)

    # Ensure we can retrieve this
    response = client.post(baseUrl + "findOrCreateTagClass/2/", data={'tagClassName': 'newTagClass'})
    self.assertEqual(response.status_code, 200)
    data = json.loads(response.content)
    self.assertEqual(data.get('user'), 2)
    self.assertEqual(data.get('project'), 2)
    self.assertEqual(data.get('className'), 'newTagClass')
    self.assertEqual(data.get('displayName'), 'newTagClass')


  def test_findExisting(self):
    client = APIClient()
    self.assertEqual(
      client.login(username=normalUserName, password=normalUserPass),
      True)

    response = client.post(baseUrl + "findOrCreateTagClass/2/", data={'tagClassName': 'userTagClass'})
    self.assertEqual(response.status_code, 200)
    data = json.loads(response.content)
    self.assertEqual(data.get('id'), 2)
    self.assertEqual(data.get('user'), 2)
    self.assertEqual(data.get('project'), 2)
    self.assertEqual(data.get('className'), 'userTagClass')
    self.assertEqual(data.get('displayName'), 'userTagClass')


  def test_missingData(self):
    client = APIClient()
    self.assertEqual(
      client.login(username=normalUserName, password=normalUserPass),
      True)

    response = client.post(baseUrl + "findOrCreateTagClass/2/", data={'badKeyName': ''})
    self.assertEqual(response.status_code, 400)

