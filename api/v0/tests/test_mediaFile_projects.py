import json

from django.test import TestCase
from rest_framework.test import APIClient

from .utils import Ensure_All_403_Or_Fail
from .config import *


class APIMediaFileProjectsTest(TestCase):
  fixtures = ['api_test_data.yaml']

  def test_unauthedRequests(self):
    client = APIClient()
    Ensure_All_403_Or_Fail(self, client, baseUrl + "mediaFile/1/projects/")

  def test_adminUserAccess(self):
    client = APIClient()
    self.assertEqual(
      client.login(username=adminUserName, password=adminUserPass),
      True)

    response = client.get(baseUrl + "mediaFile/1/projects/")
    self.assertEqual(response.status_code, 200)
    data = json.loads(response.content)
    self.assertTrue('count' in data)
    self.assertEqual(data.get('count'), 1)
    results = data.get('results')
    for result in results:
        self.assertEqual(result['id'], 1)
        self.assertEqual(result['name'], 'adminProject')

    # retrieve another user's for below test - admin should succeed
    response = client.get(baseUrl + "mediaFile/2/projects/")
    self.assertEqual(response.status_code, 200)
    data = json.loads(response.content)
    self.assertTrue('count' in data)
    self.assertEqual(data.get('count'), 2)
    results = data.get('results')
    for result in results:
      if result['id'] == 2:
        self.assertEqual(result['name'], 'testProject')
      elif result['id'] == 3:
        self.assertEqual(result['name'], 'notSharedWithOther')
      else:
        self.fail("Unknown ID Found ({})".format(result['id']))

    # PUT / POST / DELETE should fail
    response = client.put(baseUrl + "mediaFile/1/projects/", '', content_type="application/json")
    self.assertEqual(response.status_code, 405)
    response = client.post(baseUrl + "mediaFile/1/projects/", '', content_type="application/json")
    self.assertEqual(response.status_code, 405)
    response = client.delete(baseUrl + "mediaFile/1/projects/")
    self.assertEqual(response.status_code, 405)





  def test_normalUserAccess(self):
    client = APIClient()
    self.assertEqual(
      client.login(username=normalUserName, password=normalUserPass),
      True)

    # admin's should fail
    response = client.get(baseUrl + "mediaFile/1/projects/")
    self.assertEqual(response.status_code, 403)

    # retrieve another user's for below test - admin should succeed
    response = client.get(baseUrl + "mediaFile/2/projects/")
    self.assertEqual(response.status_code, 200)
    data = json.loads(response.content)
    self.assertTrue('count' in data)
    self.assertEqual(data.get('count'), 2)
    results = data.get('results')
    for result in results:
      if result['id'] == 2:
        self.assertEqual(result['name'], 'testProject')
      elif result['id'] == 3:
        self.assertEqual(result['name'], 'notSharedWithOther')
      else:
        self.fail("Unknown ID Found ({})".format(result['id']))

    # PUT / POST / DELETE should fail
    response = client.put(baseUrl + "mediaFile/2/projects/", '', content_type="application/json")
    self.assertEqual(response.status_code, 405)
    response = client.post(baseUrl + "mediaFile/2/projects/", '', content_type="application/json")
    self.assertEqual(response.status_code, 405)
    response = client.delete(baseUrl + "mediaFile/2/projects/")
    self.assertEqual(response.status_code, 405)


  def test_otherUserAccess(self):
    client = APIClient()
    self.assertEqual(
      client.login(username=user2Name, password=user2Pass),
      True)

    # admin's should fail
    response = client.get(baseUrl + "mediaFile/1/projects/")
    self.assertEqual(response.status_code, 403)

    # retrieve another user's for below test - admin should succeed
    response = client.get(baseUrl + "mediaFile/2/projects/")
    self.assertEqual(response.status_code, 200)
    data = json.loads(response.content)
    self.assertTrue('count' in data)
    self.assertEqual(data.get('count'), 2)
    results = data.get('results')
    for result in results:
      if result['id'] == 2:
        self.assertEqual(result['name'], 'testProject')
      elif result['id'] == 3:
        self.assertEqual(result['name'], 'notSharedWithOther')
      else:
        self.fail("Unknown ID Found ({})".format(result['id']))

    # PUT / POST / DELETE should fail
    response = client.put(baseUrl + "mediaFile/2/projects/", '', content_type="application/json")
    self.assertEqual(response.status_code, 405)
    response = client.post(baseUrl + "mediaFile/2/projects/", '', content_type="application/json")
    self.assertEqual(response.status_code, 405)
    response = client.delete(baseUrl + "mediaFile/2/projects/")
    self.assertEqual(response.status_code, 405)
