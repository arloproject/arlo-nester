import json

from django.test import TestCase
from rest_framework.test import APIClient

from .utils import Add_Update_Delete, Ensure_All_403_Or_Fail
from .config import *


class APIJobParametersTest(TestCase):
  fixtures = ['api_test_data.yaml']

  def test_unauthedRequests(self):
    client = APIClient()
    Ensure_All_403_Or_Fail(self, client, baseUrl + "jobParameters/")
    Ensure_All_403_Or_Fail(self, client, baseUrl + "jobParameters/1/")

  def test_adminUserAccess(self):
    client = APIClient()
    self.assertEqual(
      client.login(username=adminUserName, password=adminUserPass),
      True)

    # List records
    response = client.get(baseUrl + "jobParameters/")
    self.assertEqual(response.status_code, 200)
    data = json.loads(response.content)
    self.assertEqual(data.get('count'), 2)
    results = data.get('results')
    for result in results:
      id = result['id']
      if id == 1:
        self.assertEqual(result['job'], 1)
        self.assertEqual(result['name'], 'testParam1')
        self.assertEqual(result['value'], 'testValue1')
      elif id == 2:
        self.assertEqual(result['job'], 1)
        self.assertEqual(result['name'], 'testParam2')
        self.assertEqual(result['value'], 'testValue2')
      else:
        self.fail("Unknown ID Found ({})".format(id))

    # retrieve an existing record - admin override
    response = client.get(baseUrl + "jobParameters/3/")
    self.assertEqual(response.status_code, 200)
    data = json.loads(response.content)
    self.assertEqual(data['id'], 3)
    self.assertEqual(data['job'], 2)
    self.assertEqual(data['name'], 'userParam1')
    self.assertEqual(data['value'], 'userValue1')

    # add a new record
    Add_Update_Delete(self, client, "name", "newParamName", baseUrl + "jobParameters/", {
      'job': 1,
      'name': 'newParam',
      'value': 'newValue',
      })

    Add_Update_Delete(self, client, "value", "newValueName", baseUrl + "jobParameters/", {
      'job': 1,
      'name': 'newParam',
      'value': 'newValue',
      })

  def test_normalUserAccess(self):
    client = APIClient()
    self.assertEqual(
      client.login(username=normalUserName, password=normalUserPass),
      True)

    # List records
    response = client.get(baseUrl + "jobParameters/")
    self.assertEqual(response.status_code, 200)
    data = json.loads(response.content)
    self.assertEqual(data.get('count'), 1)
    results = data.get('results')
    for result in results:
      id = result['id']
      if id == 3:
        self.assertEqual(result['job'], 2)
        self.assertEqual(result['name'], 'userParam1')
        self.assertEqual(result['value'], 'userValue1')
      else:
        self.fail("Unknown ID Found ({})".format(id))

    # denied record
    response = client.get(baseUrl + "jobParameters/1/")
    self.assertEqual(response.status_code, 403)

    # retrieve an existing record
    response = client.get(baseUrl + "jobParameters/3/")
    self.assertEqual(response.status_code, 200)
    data = json.loads(response.content)
    self.assertEqual(data['id'], 3)
    self.assertEqual(data['job'], 2)
    self.assertEqual(data['name'], 'userParam1')
    self.assertEqual(data['value'], 'userValue1')

    # Try creating a record in a different user's Job
    response = client.post(baseUrl + "jobParameters/", {
      'job': 1,
      'name': 'newParam',
      'value': 'newValue',
      })
    self.assertEqual(response.status_code, 403)

    # add a new record
    Add_Update_Delete(self, client, "name", "newParamName", baseUrl + "jobParameters/", {
      'job': 2,
      'name': 'newParam',
      'value': 'newValue',
      })

    Add_Update_Delete(self, client, "value", "newValueName", baseUrl + "jobParameters/", {
      'job': 2,
      'name': 'newParam',
      'value': 'newValue',
      })

  def test_otherUserAccess(self):
    client = APIClient()
    self.assertEqual(
      client.login(username=user2Name, password=user2Pass),
      True)

    # List records
    response = client.get(baseUrl + "jobParameters/")
    self.assertEqual(response.status_code, 200)
    data = json.loads(response.content)
    self.assertEqual(data.get('count'), 1)
    results = data.get('results')
    for result in results:
      id = result['id']
      if id == 3:
        self.assertEqual(result['job'], 2)
        self.assertEqual(result['name'], 'userParam1')
        self.assertEqual(result['value'], 'userValue1')
      else:
        self.fail("Unknown ID Found ({})".format(id))

    # denied record
    response = client.get(baseUrl + "jobParameters/1/")
    self.assertEqual(response.status_code, 403)

    # test update to read-only permission access
    response = client.put(baseUrl + "jobParameters/3/", {})
    self.assertEqual(response.status_code, 403)

