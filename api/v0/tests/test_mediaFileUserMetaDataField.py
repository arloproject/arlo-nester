import json

from django.test import TestCase
from rest_framework.test import APIClient

from .utils import Add_Update_Delete, Ensure_All_403_Or_Fail
from .config import *


class APIMediaFileUserMetaDataFieldTest(TestCase):
  fixtures = ['api_test_data.yaml']

  def test_unauthedRequests(self):
    client = APIClient()
    Ensure_All_403_Or_Fail(self, client, baseUrl + "mediaFileUserMetaDataField/")
    Ensure_All_403_Or_Fail(self, client, baseUrl + "mediaFileUserMetaDataField/1/")

  def test_adminUserAccess(self):
    client = APIClient()
    self.assertEqual(
      client.login(username=adminUserName, password=adminUserPass),
      True)

    # List records
    response = client.get(baseUrl + "mediaFileUserMetaDataField/")
    self.assertEqual(response.status_code, 200)
    data = json.loads(response.content)
    self.assertTrue('count' in data)
    self.assertEqual(data.get('count'), 2)
    results = data.get('results')
    for result in results:
      id = result['id']
      if id == 1:
        self.assertEqual(result['name'], 'Library1TestField1')
        self.assertEqual(result['library'], 1)
        self.assertEqual(result['description'], 'Library1TestField1')
      elif id == 2:
        self.assertEqual(result['name'], 'Library1TestField2')
        self.assertEqual(result['library'], 1)
        self.assertEqual(result['description'], 'Library1TestField2')
      else:
        self.fail("Unknown ID Found ({})".format(id))

    # retrieve an existing record - admin override
    response = client.get(baseUrl + "mediaFileUserMetaDataField/1/")
    self.assertEqual(response.status_code, 200)
    data = json.loads(response.content)
    self.assertEqual(data.get('id'), 1)
    self.assertEqual(data['name'], 'Library1TestField1')
    self.assertEqual(data['library'], 1)
    self.assertEqual(data['description'], 'Library1TestField1')

    Add_Update_Delete(self, client, "name", "newValue", baseUrl + "mediaFileUserMetaDataField/", {
      'name': 'newName',
      'library': 1,
      'description': 'myDesc',
      })


  def test_normalUserAccess(self):
    client = APIClient()
    self.assertEqual(
      client.login(username=normalUserName, password=normalUserPass),
      True)

    # List records
    response = client.get(baseUrl + "mediaFileUserMetaDataField/")
    self.assertEqual(response.status_code, 200)
    data = json.loads(response.content)
    self.assertTrue('count' in data)
    self.assertEqual(data.get('count'), 3)
    results = data.get('results')
    for result in results:
      id = result['id']
      if id == 3:
        self.assertEqual(result['name'], 'Library2TestField1')
        self.assertEqual(result['library'], 2)
        self.assertEqual(result['description'], 'Library2TestField1')
      elif id == 4:
        self.assertEqual(result['name'], 'Library2TestField2')
        self.assertEqual(result['library'], 2)
        self.assertEqual(result['description'], 'Library2TestField2')
      elif id == 5:
        self.assertEqual(result['name'], 'Library2TestField3')
        self.assertEqual(result['library'], 2)
        self.assertEqual(result['description'], 'Library2TestField3')
      else:
        self.fail("Unknown ID Found ({})".format(id))

    # should have no access
    response = client.get(baseUrl + "mediaFileUserMetaDataField/1/")
    self.assertEqual(response.status_code, 403)

    # retrieve an existing record
    response = client.get(baseUrl + "mediaFileUserMetaDataField/3/")
    self.assertEqual(response.status_code, 200)
    data = json.loads(response.content)
    self.assertEqual(data.get('id'), 3)
    self.assertEqual(data['name'], 'Library2TestField1')
    self.assertEqual(data['library'], 2)
    self.assertEqual(data['description'], 'Library2TestField1')

    # Try to create metadata for a mediafile to which this user does not have access
    response = client.post(baseUrl + "mediaFileUserMetaDataField/", {
      'name': 'newName',
      'library': 1,
      'description': 'myDesc',
      })
    self.assertEqual(response.status_code, 403)

    # Try to create metadataField with the same name / library as an existing entry - should fail
    response = client.post(baseUrl + "mediaFileUserMetaDataField/", {
      'name': 'Library2TestField1',
      'library': 2,
      'description': 'Make Me Fail',
      })
    self.assertEqual(response.status_code, 400)

    Add_Update_Delete(self, client, "description", "changeValue", baseUrl + "mediaFileUserMetaDataField/", {
      'name': 'newName',
      'library': 2,
      'description': 'myDesc',
      })

  def test_otherUserAccess(self):
    client = APIClient()
    self.assertEqual(
      client.login(username=user2Name, password=user2Pass),
      True)

    # List records
    response = client.get(baseUrl + "mediaFileUserMetaDataField/")
    self.assertEqual(response.status_code, 200)
    data = json.loads(response.content)
    self.assertTrue('count' in data)
    self.assertEqual(data.get('count'), 0)

    # test accessing the existing record from another user
    response = client.get(baseUrl + "mediaFileUserMetaDataField/1/")
    self.assertEqual(response.status_code, 403)

    # test update
    response = client.put(baseUrl + "mediaFileUserMetaDataField/1/", {})
    self.assertEqual(response.status_code, 403)

