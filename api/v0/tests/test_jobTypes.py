import json

from django.test import TestCase
from rest_framework.test import APIClient

from utils import Add_Update_Delete, Ensure_All_403_Or_Fail
from .config import *


class APIJobTypesTest(TestCase):
  fixtures = ['api_test_data']

  def test_unauthedRequests(self):
    client = APIClient()
    Ensure_All_403_Or_Fail(self, client, baseUrl + "jobTypes/")
    Ensure_All_403_Or_Fail(self, client, baseUrl + "jobTypes/1/")

  def userAccessTest(self, client):
    # Test the GET
    response = client.get(baseUrl + "jobTypes/")
    self.assertEqual(response.status_code, 200)
    data = json.loads(response.content)
    self.assertTrue('count' in data)
    self.assertEqual(data.get('count'), 15)
    self.assertTrue('results' in data)
    results = data.get('results')
    self.assertTrue('id' in results[0])
    self.assertEqual(results[0]['id'], 1)
    self.assertTrue('name' in results[0])
    self.assertEqual(results[0]['name'], 'SupervisedTagDiscovery')

    # POST should fail
    response = client.post(baseUrl + 'jobTypes/', {})
    self.assertEqual(response.status_code, 405)
    
    # Test the GET
    response = client.get(baseUrl + "jobTypes/1/")
    self.assertEqual(response.status_code, 200)
    data = json.loads(response.content)
    self.assertTrue('id' in data)
    self.assertEqual(data.get('id'), 1)
    self.assertTrue('name' in data)
    self.assertEqual(data.get('name'), 'SupervisedTagDiscovery')

    # PUT should fail
    response = client.post(baseUrl + 'jobTypes/1/', {})
    self.assertEqual(response.status_code, 405)

  def test_AdminAccess(self):
    client = APIClient()
    self.assertEqual(
      client.login(username=adminUserName, password=adminUserPass),
      True)
    self.userAccessTest(client)

  def test_UserAccess(self):
    client = APIClient()
    self.assertEqual(
      client.login(username=normalUserName, password=normalUserPass),
      True)
    self.userAccessTest(client)

