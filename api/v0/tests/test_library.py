import json

from django.test import TestCase
from rest_framework.test import APIClient

from .utils import Add_Update_Delete, Ensure_All_403_Or_Fail
from .config import *


class APILibraryTest(TestCase):
  fixtures = ['api_test_data.yaml']

  def test_unauthedRequests(self):
    client = APIClient()
    Ensure_All_403_Or_Fail(self, client, baseUrl + "library/")
    Ensure_All_403_Or_Fail(self, client, baseUrl + "library/1/")

  def test_adminUserAccess(self):
    client = APIClient()
    self.assertEqual(
      client.login(username=adminUserName, password=adminUserPass),
      True)

    # List records
    response = client.get(baseUrl + "library/")
    self.assertEqual(response.status_code, 200)
    data = json.loads(response.content)
    self.assertEqual(data.get('count'), 1)
    results = data.get('results')
    for result in results:
      id = result['id']
      if id == 1:
        self.assertEqual(result['user'], 1)
        self.assertEqual(result['name'], 'AdminLib')
        self.assertEqual(result['notes'], "Admin Library")
      else:
        self.fail("Unknown ID Found ({})".format(id))

    # retrieve an existing record - admin override
    response = client.get(baseUrl + "library/2/")
    self.assertEqual(response.status_code, 200)
    data = json.loads(response.content)
    self.assertEqual(data.get('id'), 2)
    self.assertEqual(data.get('user'), 2)
    self.assertEqual(data.get('name'), 'UserLib')
    self.assertEqual(data.get('notes'), "User Library")

    # modify existing record
    data['name'] = 'newName'
    response = client.put(baseUrl + "library/2/", data)
    self.assertEqual(response.status_code, 200)
    data = json.loads(response.content)
    self.assertEqual(data.get('name'), 'newName')

    # re-retrieve existing record
    # NOTE: we want to ensure the the user of an existing record
    # isn't changed after the above update
    response = client.get(baseUrl + "library/2/")
    self.assertEqual(response.status_code, 200)
    data = json.loads(response.content)
    self.assertEqual(data.get('id'), 2)
    self.assertEqual(data.get('user'), 2)
    self.assertEqual(data.get('name'), 'newName')

    # add a new record
    Add_Update_Delete(self, client, "name", "newName", baseUrl + "library/", {
      "name": "newLib",
      "notes": "Test Notes",
      })

  def test_normalUserAccess(self):
    client = APIClient()
    self.assertEqual(
      client.login(username=normalUserName, password=normalUserPass),
      True)

    # List records
    response = client.get(baseUrl + "library/")
    self.assertEqual(response.status_code, 200)
    data = json.loads(response.content)
    self.assertEqual(data.get('count'), 1)
    results = data.get('results')
    for result in results:
      id = result['id']
      if id == 2:
        self.assertEqual(result['user'], 2)
        self.assertEqual(result['name'], 'UserLib')
        self.assertEqual(result['notes'], "User Library")
      else:
        self.fail("Unknown ID Found ({})".format(id))

    # retrieve an existing record
    response = client.get(baseUrl + "library/2/")
    self.assertEqual(response.status_code, 200)
    data = json.loads(response.content)
    self.assertEqual(data.get('id'), 2)
    self.assertEqual(data.get('user'), 2)
    self.assertEqual(data.get('name'), 'UserLib')
    self.assertEqual(data.get('notes'), "User Library")

    # modify existing record
    data['name'] = 'newName'
    response = client.put(baseUrl + "library/2/", data)
    self.assertEqual(response.status_code, 200)
    data = json.loads(response.content)
    self.assertEqual(data.get('name'), 'newName')

    # re-retrieve existing record
    response = client.get(baseUrl + "library/2/")
    self.assertEqual(response.status_code, 200)
    data = json.loads(response.content)
    self.assertEqual(data.get('id'), 2)
    self.assertEqual(data.get('user'), 2)
    self.assertEqual(data.get('name'), 'newName')

    # add a new record
    Add_Update_Delete(self, client, "name", "newName", baseUrl + "library/", {
      "name": "newLib",
      "notes": "Test Notes",
      })

  def test_otherUserAccess(self):
    client = APIClient()
    self.assertEqual(
      client.login(username=user2Name, password=user2Pass),
      True)

    # Test list
    response = client.get(baseUrl + "library/")
    self.assertEqual(response.status_code, 200)
    data = json.loads(response.content)
    self.assertEqual(data.get('count'), 0)

    # test accessing the existing record from another user
    response = client.get(baseUrl + "library/1/")
    self.assertEqual(response.status_code, 403)

    # test update
    response = client.put(baseUrl + "library/1/", {})
    self.assertEqual(response.status_code, 403)

