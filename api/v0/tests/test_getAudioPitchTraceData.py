from copy import deepcopy

from django.test import TestCase
from rest_framework.test import APIClient

from .config import *
from .utils import Ensure_All_403_Or_Fail
from tools.Meandre import AdaptInterface


def dummy_GetAudioPitchTraceData(self, mediaFile, pitchTraceType, startTime, endTime, numTimeFramesPerSecond, numFrequencyBands, dampingRatio, spectraMinFrequency, spectraMaxFrequency, numSamplePoints, minCorrelation, entropyThreshold, pitchTraceStartFreq, pitchTraceEndFreq, minEnergyThreshold, tolerance, inverseFreqWeight, maxPathLengthPerTransition, windowSize, extendRangeFactor):

  # Check Settings Common to FUNDAMENTAL and MAX_ENERGY
  if not all( (
        (mediaFile.id == 3),
        (startTime == 0),
        (endTime == 0.75),
        (numTimeFramesPerSecond == 32),
        (numFrequencyBands == 64),
        (dampingRatio == 0.02),
        (spectraMinFrequency == 20),
        (spectraMaxFrequency == 20000)
        )):
    return None

  if pitchTraceType == 'MAX_ENERGY':
    return {'pitchTraceData': {
      'params': {
          'pitchTraceType': 'MAX_ENERGY',
          'mediaFileId': 2,
          'startTime': 0,
          'endTime': 5,
          'numTimeFramesPerSecond': 32,
          'numFrequencyBands': 64,
          'dampingRatio': 0.02,
          'minFrequency': 20,
          'maxFrequency': 20000,
          },
      'freqBands': [
          75,
          300,
          ],
      'frames': {
          0: {
            "frequency": 75,
            "energy": 1,
            },
          0.5: {
            "frequency": 100,
            "energy": 0.5,
            },
          }
      }
    }
  elif pitchTraceType == 'FUNDAMENTAL':
    if not all( (
          (numSamplePoints == 2000),
          (minCorrelation == 0.01),
          (entropyThreshold == 99999),
          (pitchTraceStartFreq == 75),
          (pitchTraceEndFreq == 300),
          (minEnergyThreshold == 0.01),
          (tolerance == 0.15),
          (inverseFreqWeight == 0.9),
          (maxPathLengthPerTransition == 4),
          (windowSize == 9),
          (extendRangeFactor == 4),
          )):
      return None
    return {'pitchTraceData': {
      'params': {
          'pitchTraceType': 'MAX_ENERGY',
          'mediaFileId': 2,
          'startTime': 0,
          'endTime': 5,
          'numTimeFramesPerSecond': 32,
          'numFrequencyBands': 64,
          'dampingRatio': 0.02,
          'minFrequency': 20,
          'maxFrequency': 20000,
          # Fundamental Params
          'numSamplePoints': 2000,
          'minCorrelation': 0.01,
          'entropyThreshold': 99999,
          'pitchTraceStartFreq': 75,
          'pitchTraceEndFreq': 300,
          'minEnergyThreshold': 0.01,
          'tolerance': 0.15,
          'inverseFreqWeight': 0.9,
          'maxPathLengthPerTransition': 4,
          'windowSize': 9,
          'extendRangeFactor': 4,
          },
      'freqBands': [
          75,
          300,
          ],
      'frames': {
          0: {
            "frequency": 75,
            "energy": 1,
            },
          0.5: {
            "frequency": 100,
            "energy": 0.5,
            },
          }
      }
    }

  else:
    return None


class APIGetAudioPitchTraceDataTest(TestCase):
  fixtures = ['api_test_data.yaml']

  sampleMaxEnergyParams = {
      'pitchTraceType': 'MAX_ENERGY',
      'startTime': 0,
      'endTime': 0.75,
      'numTimeFramesPerSecond': 32,
      'numFrequencyBands': 64,
      'dampingFactor': 0.02,
      'spectraMinFrequency': 20,
      'spectraMaxFrequency': 20000,
  }

  sampleFundamentalParams = {
      'pitchTraceType': 'FUNDAMENTAL',
      'startTime': 0,
      'endTime': 0.75,
      'numTimeFramesPerSecond': 32,
      'numFrequencyBands': 64,
      'dampingFactor': 0.02,
      'spectraMinFrequency': 20,
      'spectraMaxFrequency': 20000,
      # Only for FUNDAMENTAL
      'numSamplePoints': 2000,
      'minCorrelation': 0.01,
      'entropyThreshold': 99999,
      'pitchTraceStartFreq': 75,
      'pitchTraceEndFreq': 300,
      'minEnergyThreshold': 0.01,
      'tolerance': 0.15,
      'inverseFreqWeight': 0.9,
      'maxPathLengthPerTransition': 4,
      'windowSize': 9,
      'extendRangeFactor': 4,
  }


  def test_unauthedRequests(self):
    client = APIClient()
    Ensure_All_403_Or_Fail(self, client, baseUrl + "getAudioPitchTraceData/1/")


  def test_permissionDenied(self):
    client = APIClient()
    self.assertEqual(
      client.login(username=normalUserName, password=normalUserPass),
      True)

    response = client.get(baseUrl + "getAudioPitchTraceData/1/", data=self.sampleMaxEnergyParams)
    self.assertEqual(response.status_code, 401)


  def test_missingData(self):
    client = APIClient()
    self.assertEqual(
      client.login(username=normalUserName, password=normalUserPass),
      True)

    params = deepcopy(self.sampleMaxEnergyParams)
    del params['pitchTraceType']
    response = client.get(baseUrl + "getAudioPitchTraceData/3/", data=params)
    self.assertEqual(response.status_code, 400)


  def test_getAudioPitchTraceData(self):
    client = APIClient()
    self.assertEqual(
      client.login(username=normalUserName, password=normalUserPass),
      True)

    # Stub out function
    original_GetAudioPitchTraceData = AdaptInterface.GetAudioPitchTraceData
    AdaptInterface.GetAudioPitchTraceData = dummy_GetAudioPitchTraceData

    # MAX_ENERGY
    response = client.get(baseUrl + "getAudioPitchTraceData/3/", data=self.sampleMaxEnergyParams)
    self.assertEqual(response.status_code, 200)

    # FUNDAMENTAL
    response = client.get(baseUrl + "getAudioPitchTraceData/3/", data=self.sampleFundamentalParams)
    self.assertEqual(response.status_code, 200)

    # Un-Stub function
    AdaptInterface.GetAudioPitchTraceData = original_GetAudioPitchTraceData
