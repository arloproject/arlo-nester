import json

from django.test import TestCase
from rest_framework.test import APIClient

from .utils import Add_Update_Delete, Ensure_All_403_Or_Fail
from .config import *


class APISensorArrayTest(TestCase):
  fixtures = ['api_test_data.yaml']

  def test_unauthedRequests(self):
    client = APIClient()
    Ensure_All_403_Or_Fail(self, client, baseUrl + "sensorArray/")
    Ensure_All_403_Or_Fail(self, client, baseUrl + "sensorArray/1/")

  def test_adminUserAccess(self):
    client = APIClient()
    self.assertEqual(
      client.login(username=adminUserName, password=adminUserPass),
      True)

    # List records
    response = client.get(baseUrl + "sensorArray/")
    self.assertEqual(response.status_code, 200)
    data = json.loads(response.content)
    self.assertEqual(data.get('count'), 1)
    results = data.get('results')
    for result in results:
      id = result['id']
      if id == 1:
        self.assertEqual(result['user'], 1)
        self.assertEqual(result['name'], 'AdminSensorArray')
        self.assertEqual(result['layoutRows'], 3)
        self.assertEqual(result['layoutColumns'], 3)
      else:
        self.fail("Unknown ID Found ({})".format(id))

    # retrieve an existing record - admin override
    response = client.get(baseUrl + "sensorArray/2/")
    self.assertEqual(response.status_code, 200)
    data = json.loads(response.content)
    self.assertEqual(data.get('id'), 2)
    self.assertEqual(data.get('user'), 2)
    self.assertEqual(data.get('name'), 'UserSensorArray')
    self.assertEqual(data.get('library'), 2)
    self.assertEqual(data.get('layoutRows'), 4)
    self.assertEqual(data.get('layoutColumns'), 4)

    # modify existing record
    data['name'] = 'newSensorArray'
    response = client.put(baseUrl + "sensorArray/2/", data)
    self.assertEqual(response.status_code, 200)
    data = json.loads(response.content)
    self.assertTrue('name' in data)
    self.assertEqual(data.get('name'), 'newSensorArray')

    # re-retrieve existing record
    # NOTE: we want to ensure the the user of an existing record
    # isn't changed after the above update
    response = client.get(baseUrl + "sensorArray/2/")
    self.assertEqual(response.status_code, 200)
    data = json.loads(response.content)
    self.assertEqual(data.get('id'), 2)
    self.assertEqual(data.get('user'), 2)
    self.assertEqual(data.get('name'), 'newSensorArray')
    self.assertEqual(data.get('library'), 2)
    self.assertEqual(data.get('layoutRows'), 4)
    self.assertEqual(data.get('layoutColumns'), 4)

    # add a new record
    Add_Update_Delete(self, client, "name", "newName", baseUrl + "sensorArray/", {
      "name": "new",
      "library": 1,
      "layoutRows": 1,
      "layoutColumns": 2,
      })


  def test_normalUserAccess(self):
    client = APIClient()
    self.assertEqual(
      client.login(username=normalUserName, password=normalUserPass),
      True)

    # List records
    response = client.get(baseUrl + "sensorArray/")
    self.assertEqual(response.status_code, 200)
    data = json.loads(response.content)
    self.assertTrue('count' in data)
    self.assertEqual(data.get('count'), 1)
    results = data.get('results')
    for result in results:
      id = result['id']
      if id == 2:
        self.assertEqual(result['user'], 2)
        self.assertEqual(result['name'], 'UserSensorArray')
        self.assertEqual(result['library'], 2)
        self.assertEqual(result['layoutRows'], 4)
        self.assertEqual(result['layoutColumns'], 4)
      else:
        self.fail("Unknown ID Found ({})".format(id))

    # retrieve an existing record
    response = client.get(baseUrl + "sensorArray/2/")
    self.assertEqual(response.status_code, 200)
    data = json.loads(response.content)
    self.assertEqual(data.get('id'), 2)
    self.assertEqual(data.get('user'), 2)
    self.assertEqual(data.get('name'), 'UserSensorArray')
    self.assertEqual(data.get('library'), 2)
    self.assertEqual(data.get('layoutRows'), 4)
    self.assertEqual(data.get('layoutColumns'), 4)

    # modify existing record
    data['name'] = 'newSensorArray'
    response = client.put(baseUrl + "sensorArray/2/", data)
    self.assertEqual(response.status_code, 200)
    data = json.loads(response.content)
    self.assertTrue('name' in data)
    self.assertEqual(data.get('name'), 'newSensorArray')

    # re-retrieve existing record
    response = client.get(baseUrl + "sensorArray/2/")
    self.assertEqual(response.status_code, 200)
    data = json.loads(response.content)
    self.assertEqual(data.get('id'), 2)
    self.assertEqual(data.get('user'), 2)
    self.assertEqual(data.get('name'), 'newSensorArray')
    self.assertEqual(data.get('library'), 2)
    self.assertEqual(data.get('layoutRows'), 4)
    self.assertEqual(data.get('layoutColumns'), 4)

    # should not have access
    response = client.get(baseUrl + "sensorArray/1/")
    self.assertEqual(response.status_code, 403)

    # add a new record
    Add_Update_Delete(self, client, "name", "newName", baseUrl + "sensorArray/", {
      "name": "newSensorArray",
      "library": 2,
      "layoutRows": 1,
      "layoutColumns": 2,
      })

  def test_otherUserAccess(self):
    client = APIClient()
    self.assertEqual(
      client.login(username=user2Name, password=user2Pass),
      True)
    response = client.get(baseUrl + "sensorArray/")
    self.assertEqual(response.status_code, 200)
    data = json.loads(response.content)
    self.assertEqual(data.get('count'), 0)

    # test accessing the existing record from another user
    response = client.get(baseUrl + "sensorArray/1/")
    self.assertEqual(response.status_code, 403)

    # test update
    response = client.put(baseUrl + "sensorArray/1/", {})
    self.assertEqual(response.status_code, 403)

