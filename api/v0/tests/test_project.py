import json

from django.test import TestCase
from rest_framework.test import APIClient

from utils import Add_Update_Delete, Ensure_All_403_Or_Fail
from .config import *


class APIProjectTest(TestCase):
  fixtures = ['api_test_data.yaml']

  def test_unauthedRequests(self):
    client = APIClient()
    Ensure_All_403_Or_Fail(self, client, baseUrl + "project/")
    Ensure_All_403_Or_Fail(self, client, baseUrl + "project/1/")

  def test_adminUserAccess(self):
    client = APIClient()
    self.assertEqual(
      client.login(username=adminUserName, password=adminUserPass),
      True)

    # List records
    response = client.get(baseUrl + "project/")
    self.assertEqual(response.status_code, 200)
    data = json.loads(response.content)
    self.assertTrue('count' in data)
    self.assertEqual(data.get('count'), 1)
    results = data.get('results')
    for result in results:
      self.assertTrue('id' in result)
      self.assertTrue('user' in result)
      self.assertTrue('name' in result)
      self.assertTrue('mediaFiles' in result)
      self.assertTrue('creationDate' in result)
      self.assertEqual(result['id'], 1)
      self.assertEqual(result['user'], 1)
      self.assertEqual(result['name'], 'adminProject')
      self.assertEqual(result['type'], 1)

    # retrieve an existing record - with ADMIN override
    response = client.get(baseUrl + "project/2/")
    self.assertEqual(response.status_code, 200)
    data = json.loads(response.content)
    self.assertTrue('id' in data)
    self.assertEqual(data.get('id'), 2)
    self.assertTrue('user' in data)
    self.assertEqual(data.get('user'), 2)
    self.assertTrue('name' in data)
    self.assertEqual(data.get('name'), 'testProject')
    self.assertTrue('type' in data)
    self.assertEqual(data.get('type'), 1)

    # modify existing record
    data['name'] = 'newProject'
    response = client.put(baseUrl + "project/2/", data)
    self.assertEqual(response.status_code, 200)
    data = json.loads(response.content)
    self.assertTrue('name' in data)
    self.assertEqual(data.get('name'), 'newProject')

    # re-retrieve existing record
    # NOTE: we want to ensure the the user of an existing record 
    # isn't changed after the above update
    response = client.get(baseUrl + "project/2/")
    self.assertEqual(response.status_code, 200)
    data = json.loads(response.content)
    self.assertTrue('id' in data)
    self.assertEqual(data.get('id'), 2)
    self.assertTrue('user' in data)
    self.assertEqual(data.get('user'), 2)
    self.assertTrue('name' in data)
    self.assertEqual(data.get('name'), 'newProject')

    # add a new record
    Add_Update_Delete(self, client, "name", "newName", baseUrl + "project/", {
      "name": "newProj",
      "type": 1,
      })

  def test_normalUserAccess(self):
    client = APIClient()
    self.assertEqual(
      client.login(username=normalUserName, password=normalUserPass),
      True)

    # List records
    response = client.get(baseUrl + "project/")
    self.assertEqual(response.status_code, 200)
    data = json.loads(response.content)
    self.assertTrue('count' in data)
    self.assertEqual(data.get('count'), 2)
    results = data.get('results')
    for result in results:
      self.assertTrue('id' in result)
      self.assertTrue('user' in result)
      self.assertTrue('name' in result)
      self.assertTrue('mediaFiles' in result)
      self.assertTrue('creationDate' in result)
      if id == 2:
        self.assertEqual(result['id'], 2)
        self.assertEqual(result['user'], 2)
        self.assertEqual(result['name'], 'testProject')
        self.assertEqual(result['type'], 1)
        self.assertEqual(len(result['mediaFiles']), 5)
      if id == 3:
        self.assertEqual(result['id'], 3)
        self.assertEqual(result['user'], 2)
        self.assertEqual(result['name'], 'notSharedWithOther')
        self.assertEqual(result['type'], 1)
        self.assertEqual(len(result['mediaFiles']), 5)

    # retrieve an existing record
    response = client.get(baseUrl + "project/2/")
    self.assertEqual(response.status_code, 200)
    data = json.loads(response.content)
    self.assertTrue('id' in data)
    self.assertEqual(data.get('id'), 2)
    self.assertTrue('user' in data)
    self.assertEqual(data.get('user'), 2)
    self.assertTrue('name' in data)
    self.assertEqual(data.get('name'), 'testProject')
    self.assertTrue('type' in data)
    self.assertEqual(data.get('type'), 1)

    # add a new record
    Add_Update_Delete(self, client, "name", "newName", baseUrl + "project/", {
      "name": "newProj",
      "type": 1,
      })

  def test_otherUserAccess(self):
    client = APIClient()
    self.assertEqual(
      client.login(username=user2Name, password=user2Pass),
      True)

    # List records
    response = client.get(baseUrl + "project/")
    self.assertEqual(response.status_code, 200)
    data = json.loads(response.content)
    self.assertTrue('count' in data)
    self.assertEqual(data.get('count'), 1)
    results = data.get('results')
    for result in results:
      self.assertTrue('id' in result)
      self.assertTrue('user' in result)
      self.assertTrue('name' in result)
      self.assertTrue('mediaFiles' in result)
      self.assertTrue('creationDate' in result)
      self.assertEqual(result['id'], 2)
      self.assertEqual(result['user'], 2)
      self.assertEqual(result['name'], 'testProject')
      self.assertEqual(result['type'], 1)
      self.assertEqual(len(result['mediaFiles']), 5)

    # test accessing the existing record from another user
    response = client.get(baseUrl + "project/1/")
    self.assertEqual(response.status_code, 403)

    # test update
    response = client.put(baseUrl + "project/1/", {})
    self.assertEqual(response.status_code, 403)

