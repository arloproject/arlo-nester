import json

from django.test import TestCase
from rest_framework.test import APIClient

from .config import *


class APITokenAuthTest(TestCase):
  fixtures = ['api_test_data.yaml']

  def test_tokenAuth(self):
    client = APIClient()

    # Test Authing with a bad password
    response = client.post(apiTokenAuthUrl, data={
        'username': normalUserName,
        'password': 'badPassword',
        })
    self.assertEqual(response.status_code, 400)
    data = json.loads(response.content)
    self.assertTrue('non_field_errors' in data)
    self.assertEqual(data['non_field_errors'][0], "Unable to log in with provided credentials.")

    # Get the Authentication Token
    response = client.post(apiTokenAuthUrl, data={
        'username': normalUserName,
        'password': normalUserPass,
        })
    self.assertEqual(response.status_code, 200)
    data = json.loads(response.content)
    self.assertTrue('token' in data)
