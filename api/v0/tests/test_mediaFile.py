import json

from django.test import TestCase
from rest_framework.test import APIClient

from .utils import Ensure_All_403_Or_Fail
from .config import *


class APIMediaFileTest(TestCase):
  fixtures = ['api_test_data.yaml']

  def test_unauthedRequests(self):
    client = APIClient()
    Ensure_All_403_Or_Fail(self, client, baseUrl + "mediaFile/")
    Ensure_All_403_Or_Fail(self, client, baseUrl + "mediaFile/1/")

  def test_adminUserAccess(self):
    client = APIClient()
    self.assertEqual(
      client.login(username=adminUserName, password=adminUserPass),
      True)

    # List records
    response = client.get(baseUrl + "mediaFile/")
    self.assertEqual(response.status_code, 200)
    data = json.loads(response.content)
    self.assertTrue('count' in data)
    self.assertEqual(data.get('count'), 1)
    results = data.get('results')
    for result in results:
      if result['id'] == 1:
        self.assertEqual(result['user'], 1)
        self.assertEqual(result['type'], 1)
        self.assertEqual(result['active'], True)
        self.assertEqual(result['alias'], 'adminFakeFile')
        self.assertEqual(result['uploadDate'], '2012-01-01T01:23:45')
        self.assertEqual(result['sensor'], 1)
        self.assertEqual(result['realStartTime'], '2012-01-01T12:34:56')
        self.assertEqual(result['md5'], 'c3b0d2ce4a9ec310fe46f0f6f25197fa')

        # check MediaFile MetaData entries
        self.assertEqual(len(result['mediafilemetadata_set']), 2)
        metaDataCount = 0
        for metaDatum in result['mediafilemetadata_set']:
          if metaDatum['id'] == 1:
            self.assertEqual(metaDatum['name'], 'testMetaData1')
            self.assertEqual(metaDatum['value'], 'testMetaDataValue1')
            self.assertEqual(metaDatum['userEditable'], True)
            metaDataCount += 1
          if metaDatum['id'] == 2:
            self.assertEqual(metaDatum['name'], 'durationInSeconds')
            self.assertEqual(metaDatum['value'], '10')
            self.assertEqual(metaDatum['userEditable'], False)
            metaDataCount += 1
        self.assertEqual(metaDataCount, 2)


    # retrieve an existing record
    # note we are using another user's for below test
    response = client.get(baseUrl + "mediaFile/2/")
    self.assertEqual(response.status_code, 200)
    data = json.loads(response.content)
    self.assertTrue('id' in data)
    self.assertEqual(data.get('id'), 2)

    # modify existing record
    del data['id']
    del data['user']
    del data['uploadDate']
    del data['file']
    data['alias'] = 'newAlias'
    # use JSON to handle the nulls
    response = client.put(baseUrl + "mediaFile/2/", json.dumps(data), content_type="application/json")
    self.assertEqual(response.status_code, 200)

    # re-retrieve, ensure user is still good
    response = client.get(baseUrl + "mediaFile/2/")
    self.assertEqual(response.status_code, 200)
    data = json.loads(response.content)
    self.assertEqual(data['id'], 2)
    self.assertEqual(data['user'], 2)
    self.assertEqual(data['alias'], 'newAlias')

    # add should fail
    response = client.post(baseUrl + "mediaFile/", data)
    self.assertEqual(response.status_code, 403)


  def test_normalUserAccess(self):
    client = APIClient()
    self.assertEqual(
      client.login(username=normalUserName, password=normalUserPass),
      True)

    # List records
    response = client.get(baseUrl + "mediaFile/")
    self.assertEqual(response.status_code, 200)
    data = json.loads(response.content)
    self.assertTrue('count' in data)
    self.assertEqual(data.get('count'), 5)
    results = data.get('results')
    for result in results:
      if result['id'] == 2:
        self.assertEqual(result['user'], 2)
        self.assertEqual(result['type'], 1)
        self.assertEqual(result['active'], True)
        self.assertEqual(result['alias'], 'notReallyAnUploadedFile')
        self.assertEqual(result['uploadDate'], '2012-01-01T01:23:45')
        self.assertEqual(result['sensor'], None)
        self.assertEqual(result['realStartTime'], None)
        self.assertEqual(result['md5'], None)

        # check MediaFile MetaData entries
        self.assertEqual(len(result['mediafilemetadata_set']), 2)
        metaDataCount = 0
        for metaDatum in result['mediafilemetadata_set']:
          if metaDatum['id'] == 3:
            self.assertEqual(metaDatum['name'], 'userMetaData1')
            self.assertEqual(metaDatum['value'], 'userMetaDataValue1')
            self.assertEqual(metaDatum['userEditable'], True)
            metaDataCount += 1
          if metaDatum['id'] == 4:
            self.assertEqual(metaDatum['name'], 'userMetaData2')
            self.assertEqual(metaDatum['value'], 'userMetaDataValue2')
            self.assertEqual(metaDatum['userEditable'], False)
            metaDataCount += 1
        self.assertEqual(metaDataCount, 2)


    # should have no access
    response = client.get(baseUrl + "mediaFile/1/")
    self.assertEqual(response.status_code, 403)

    # retrieve an existing record
    response = client.get(baseUrl + "mediaFile/2/")
    self.assertEqual(response.status_code, 200)
    data = json.loads(response.content)
    self.assertEqual(data.get('id'), 2)

    # modify existing record
    del data['id']
    del data['user']
    del data['uploadDate']
    del data['file']
    data['alias'] = 'newAlias'
    # use JSON to handle the nulls
    response = client.put(baseUrl + "mediaFile/2/", json.dumps(data), content_type="application/json")
    self.assertEqual(response.status_code, 200)

    # re-retrieve an existing record
    response = client.get(baseUrl + "mediaFile/2/")
    self.assertEqual(response.status_code, 200)
    data = json.loads(response.content)
    self.assertEqual(data.get('alias'), 'newAlias')

    # add should fail
    response = client.post(baseUrl + "mediaFile/", data)
    self.assertEqual(response.status_code, 403)

  def test_otherUserAccess(self):
    client = APIClient()
    self.assertEqual(
      client.login(username=user2Name, password=user2Pass),
      True)

    # List records
    response = client.get(baseUrl + "mediaFile/")
    self.assertEqual(response.status_code, 200)
    data = json.loads(response.content)
    self.assertTrue('count' in data)
    self.assertEqual(data.get('count'), 5)
    results = data.get('results')
    for result in results:
      if result['id'] == 2:
        self.assertEqual(result['user'], 2)
        self.assertEqual(result['type'], 1)
        self.assertEqual(result['active'], True)
        self.assertEqual(result['alias'], 'notReallyAnUploadedFile')
        self.assertEqual(result['uploadDate'], '2012-01-01T01:23:45')
        self.assertEqual(result['sensor'], None)
        self.assertEqual(result['realStartTime'], None)

        # check MediaFile MetaData entries
        self.assertEqual(len(result['mediafilemetadata_set']), 2)
        metaDataCount = 0
        for metaDatum in result['mediafilemetadata_set']:
          if metaDatum['id'] == 3:
            self.assertEqual(metaDatum['name'], 'userMetaData1')
            self.assertEqual(metaDatum['value'], 'userMetaDataValue1')
            self.assertEqual(metaDatum['userEditable'], True)
            metaDataCount += 1
          if metaDatum['id'] == 4:
            self.assertEqual(metaDatum['name'], 'userMetaData2')
            self.assertEqual(metaDatum['value'], 'userMetaDataValue2')
            self.assertEqual(metaDatum['userEditable'], False)
            metaDataCount += 1
        self.assertEqual(metaDataCount, 2)


    # should have no access
    response = client.get(baseUrl + "mediaFile/1/")
    self.assertEqual(response.status_code, 403)

    # retrieve an existing record
    response = client.get(baseUrl + "mediaFile/2/")
    self.assertEqual(response.status_code, 200)
    data = json.loads(response.content)
    self.assertEqual(data.get('id'), 2)

    # modify existing record - should fail since Read-Only User
    del data['id']
    del data['user']
    del data['uploadDate']
    del data['file']
    data['alias'] = 'newAlias'
    # use JSON to handle the nulls
    response = client.put(baseUrl + "mediaFile/2/", json.dumps(data), content_type="application/json")
    self.assertEqual(response.status_code, 403)

    # re-retrieve an existing record
    response = client.get(baseUrl + "mediaFile/2/")
    self.assertEqual(response.status_code, 200)
    data = json.loads(response.content)

    # add should fail
    response = client.post(baseUrl + "mediaFile/", data)
    self.assertEqual(response.status_code, 403)

    # test update to another user
    response = client.put(baseUrl + "mediaFile/1/", {})
    self.assertEqual(response.status_code, 403)

