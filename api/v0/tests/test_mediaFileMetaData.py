import json

from django.test import TestCase
from rest_framework.test import APIClient

from utils import Add_Update_Delete, Ensure_All_403_Or_Fail
from .config import *


class APIMediaFileMetaDataTest(TestCase):
  fixtures = ['api_test_data.yaml']

  def test_unauthedRequests(self):
    client = APIClient()
    Ensure_All_403_Or_Fail(self, client, baseUrl + "mediaFileMetaData/")
    Ensure_All_403_Or_Fail(self, client, baseUrl + "mediaFileMetaData/1/")

  def test_adminUserAccess(self):
    client = APIClient()
    self.assertEqual(
      client.login(username=adminUserName, password=adminUserPass), 
      True)

    # List records
    response = client.get(baseUrl + "mediaFileMetaData/")
    self.assertEqual(response.status_code, 200)
    data = json.loads(response.content)
    self.assertTrue('count' in data)
    self.assertEqual(data.get('count'), 2)
    results = data.get('results')
    for result in results:
      if result['id'] == 1:
        self.assertEqual(result['mediaFile'], 1)
        self.assertEqual(result['name'], 'testMetaData1')
        self.assertEqual(result['value'], 'testMetaDataValue1')
        self.assertEqual(result['userEditable'], True)
      else: # should be 2
        self.assertEqual(result['id'], 2)
        self.assertEqual(result['mediaFile'], 1)
        self.assertEqual(result['name'], 'durationInSeconds')
        self.assertEqual(result['value'], '10')
        self.assertEqual(result['userEditable'], False)

    # retrieve an existing record
    response = client.get(baseUrl + "mediaFileMetaData/1/")
    self.assertEqual(response.status_code, 200)
    data = json.loads(response.content)
    self.assertEqual(data.get('id'), 1)
    self.assertEqual(data['mediaFile'], 1)
    self.assertEqual(data['name'], 'testMetaData1')
    self.assertEqual(data['value'], 'testMetaDataValue1')
    self.assertEqual(data['userEditable'], True)

    # retrieve an existing record - Admin Access
    response = client.get(baseUrl + "mediaFileMetaData/3/")
    self.assertEqual(response.status_code, 200)
    data = json.loads(response.content)
    self.assertEqual(data.get('id'), 3)
    self.assertEqual(data['mediaFile'], 2)
    self.assertEqual(data['name'], 'userMetaData1')
    self.assertEqual(data['value'], 'userMetaDataValue1')
    self.assertEqual(data['userEditable'], True)

    Add_Update_Delete(self, client, "value", "changeValue", baseUrl + "mediaFileMetaData/", {
      'mediaFile': 1,
      'name': 'newName',
      'value': 'newValue',
      'userEditable': True,
      })

  def userList(self, client):
    # List records
    response = client.get(baseUrl + "mediaFileMetaData/")
    self.assertEqual(response.status_code, 200)
    data = json.loads(response.content)
    self.assertTrue('count' in data)
    self.assertEqual(data.get('count'), 6)
    results = data.get('results')
    for result in results:
      if result['id'] == 3:
        self.assertEqual(result['mediaFile'], 2)
        self.assertEqual(result['name'], 'userMetaData1')
        self.assertEqual(result['value'], 'userMetaDataValue1')
        self.assertEqual(result['userEditable'], True)
      elif result['id'] == 4:
        self.assertEqual(result['mediaFile'], 2)
        self.assertEqual(result['name'], 'userMetaData2')
        self.assertEqual(result['value'], 'userMetaDataValue2')
        self.assertEqual(result['userEditable'], False)
      elif result['id'] == 5:
        self.assertEqual(result['mediaFile'], 3)
        self.assertEqual(result['name'], 'durationInSeconds')
        self.assertEqual(result['value'], '1')
        self.assertEqual(result['userEditable'], False)
      elif result['id'] == 6:
        self.assertEqual(result['mediaFile'], 4)
        self.assertEqual(result['name'], 'durationInSeconds')
        self.assertEqual(result['value'], '1')
        self.assertEqual(result['userEditable'], False)
      elif result['id'] == 7:
        self.assertEqual(result['mediaFile'], 5)
        self.assertEqual(result['name'], 'durationInSeconds')
        self.assertEqual(result['value'], '1')
        self.assertEqual(result['userEditable'], False)
      elif result['id'] == 8:
        self.assertEqual(result['mediaFile'], 6)
        self.assertEqual(result['name'], 'durationInSeconds')
        self.assertEqual(result['value'], '1')
        self.assertEqual(result['userEditable'], False)
      else:
        self.fail("Unknown ID Found ({})".format(id))


  def test_normalUserAccess(self):
    client = APIClient()
    self.assertEqual(
      client.login(username=normalUserName, password=normalUserPass), 
      True)

    self.userList(client)

    # should have no access
    response = client.get(baseUrl + "mediaFileMetaData/1/")
    self.assertEqual(response.status_code, 403)

    # retrieve an existing record
    response = client.get(baseUrl + "mediaFileMetaData/3/")
    self.assertEqual(response.status_code, 200)
    data = json.loads(response.content)
    self.assertEqual(data.get('id'), 3)
    self.assertEqual(data['mediaFile'], 2)
    self.assertEqual(data['name'], 'userMetaData1')
    self.assertEqual(data['value'], 'userMetaDataValue1')
    self.assertEqual(data['userEditable'], True)

    # Try to create metadata for a mediafile to which this user does not have access
    response = client.post(baseUrl + "mediaFileMetaData/", {
      "mediaFile": 1,
      'name': 'newName',
      'value': 'newValue',
      'userEditable': True,
      })
    self.assertEqual(response.status_code, 403)
  
    Add_Update_Delete(self, client, "value", "changeValue", baseUrl + "mediaFileMetaData/", {
      'mediaFile': 2,
      'name': 'newName',
      'value': 'newValue',
      'userEditable': True,
      })
      
  def test_otherUserAccess(self):
    client = APIClient()
    self.assertEqual(
      client.login(username=user2Name, password=user2Pass), 
      True)

    # List Records
    self.userList(client)

    # test accessing the existing record from another user
    response = client.get(baseUrl + "mediaFileMetaData/1/")
    self.assertEqual(response.status_code, 403)

    # test update
    response = client.put(baseUrl + "mediaFileMetaData/1/", {})
    self.assertEqual(response.status_code, 403)


