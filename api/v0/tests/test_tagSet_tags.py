import json

from django.test import TestCase
from rest_framework.test import APIClient

from .utils import Ensure_All_403_Or_Fail
from .config import *


class APITagSetTagsTest(TestCase):
  fixtures = ['api_test_data.yaml']

  def test_unauthedRequests(self):
    client = APIClient()
    Ensure_All_403_Or_Fail(self, client, baseUrl + "tagSet/1/tags/")

  def test_adminUserAccess(self):
    client = APIClient()
    self.assertEqual(
      client.login(username=adminUserName, password=adminUserPass),
      True)

    # List records
    response = client.get(baseUrl + "tagSet/1/tags/")
    self.assertEqual(response.status_code, 200)
    data = json.loads(response.content)
    self.assertEqual(data.get('count'), 1)
    results = data.get('results')
    result = results[0]
    self.assertEqual(result['user'], 1)
    self.assertEqual(result['user'], 1)
    self.assertEqual(result['creationDate'], '2012-01-01T01:23:45')
    self.assertEqual(result['tagClass'], 1)
    self.assertEqual(result['tagSet'], 1)
    self.assertEqual(result['mediaFile'], 1)
    self.assertEqual(result['startTime'], 1)
    self.assertEqual(result['endTime'], 3)
    self.assertEqual(result['minFrequency'], 20)
    self.assertEqual(result['maxFrequency'], 20000)
    self.assertEqual(result['parentTag'], None)
    self.assertEqual(result['randomlyChosen'], False)
    self.assertEqual(result['machineTagged'], False)
    self.assertEqual(result['userTagged'], True)
    self.assertEqual(result['strength'], 1)

    # retrieve an existing record
    # note we are using another user's for below test
    response = client.get(baseUrl + "tagSet/2/tags/")
    self.assertEqual(response.status_code, 200)
    data = json.loads(response.content)
    self.assertEqual(data.get('count'), 1)
    results = data.get('results')
    result = results[0]
    self.assertEqual(result['user'], 2)
    self.assertEqual(result['creationDate'], '2012-01-01T01:23:45')
    self.assertEqual(result['tagClass'], 2)
    self.assertEqual(result['tagSet'], 2)
    self.assertEqual(result['mediaFile'], 2)
    self.assertEqual(result['startTime'], 2)
    self.assertEqual(result['endTime'], 4)
    self.assertEqual(result['minFrequency'], 20)
    self.assertEqual(result['maxFrequency'], 20000)
    self.assertEqual(result['parentTag'], None)
    self.assertEqual(result['randomlyChosen'], False)
    self.assertEqual(result['machineTagged'], True)
    self.assertEqual(result['userTagged'], False)
    self.assertEqual(result['strength'], 0.5)


  def test_normalUserAccess(self):
    client = APIClient()
    self.assertEqual(
      client.login(username=normalUserName, password=normalUserPass),
      True)

    # list records
    response = client.get(baseUrl + "tagSet/1/tags/")
    self.assertEqual(response.status_code, 403)

    response = client.get(baseUrl + "tagSet/2/tags/")
    self.assertEqual(response.status_code, 200)
    data = json.loads(response.content)
    self.assertEqual(data.get('count'), 1)
    results = data.get('results')
    result = results[0]
    self.assertEqual(result['user'], 2)
    self.assertEqual(result['creationDate'], '2012-01-01T01:23:45')
    self.assertEqual(result['tagClass'], 2)
    self.assertEqual(result['tagSet'], 2)
    self.assertEqual(result['mediaFile'], 2)
    self.assertEqual(result['startTime'], 2)
    self.assertEqual(result['endTime'], 4)
    self.assertEqual(result['minFrequency'], 20)
    self.assertEqual(result['maxFrequency'], 20000)
    self.assertEqual(result['parentTag'], None)
    self.assertEqual(result['randomlyChosen'], False)
    self.assertEqual(result['machineTagged'], True)
    self.assertEqual(result['userTagged'], False)
    self.assertEqual(result['strength'], 0.5)

  def test_otherUserAccess(self):
    client = APIClient()
    self.assertEqual(
      client.login(username=user2Name, password=user2Pass),
      True)

    response = client.get(baseUrl + "tagSet/1/tags/")
    self.assertEqual(response.status_code, 403)
