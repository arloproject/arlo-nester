import json

from django.test import TestCase
from rest_framework.test import APIClient

from .utils import Ensure_All_403_Or_Fail
from .config import *


class APIJobResultFileTest(TestCase):
  fixtures = ['api_test_data.yaml']

  def test_unauthedRequests(self):
    client = APIClient()
    Ensure_All_403_Or_Fail(self, client, baseUrl + "jobResultFile/")
    Ensure_All_403_Or_Fail(self, client, baseUrl + "jobResultFile/1/")

  def test_adminUserAccess(self):
    client = APIClient()
    self.assertEqual(
      client.login(username=adminUserName, password=adminUserPass),
      True)

    # list records
    response = client.get(baseUrl + "jobResultFile/")
    self.assertEqual(response.status_code, 200)
    data = json.loads(response.content)
    self.assertEqual(data['count'], 1)
    for result in data['results']:
      id = result['id']
      if id == 1:
        self.assertEqual(result['job'], 1)
        self.assertEqual(result['notes'], 'Fake Result File')
      else:
        self.fail("Unknown ID Found ({})".format(id))

    # detail record - Admin override
    response = client.get(baseUrl + "jobResultFile/2/")
    self.assertEqual(response.status_code, 200)
    result = json.loads(response.content)
    self.assertEqual(result['job'], 2)
    self.assertEqual(result['notes'], 'Fake Result File2')


  def test_normalUserAccess(self):
    client = APIClient()
    self.assertEqual(
      client.login(username=normalUserName, password=normalUserPass),
      True)

    # list records
    response = client.get(baseUrl + "jobResultFile/")
    self.assertEqual(response.status_code, 200)
    data = json.loads(response.content)
    self.assertEqual(data['count'], 1)
    for result in data['results']:
      id = result['id']
      if id == 2:
        self.assertEqual(result['job'], 2)
        self.assertEqual(result['notes'], 'Fake Result File2')
      else:
        self.fail("Unknown ID Found ({})".format(id))

    # detail record
    response = client.get(baseUrl + "jobResultFile/2/")
    self.assertEqual(response.status_code, 200)
    result = json.loads(response.content)
    self.assertEqual(result['job'], 2)
    self.assertEqual(result['notes'], 'Fake Result File2')

    # should not have access
    response = client.get(baseUrl + "jobResultFile/1/")
    self.assertEqual(response.status_code, 403)


  def test_otherUserAccess(self):
    client = APIClient()
    self.assertEqual(
      client.login(username=user2Name, password=user2Pass),
      True)

    # List records - should not have access
    response = client.get(baseUrl + "jobResultFile/1/")
    self.assertEqual(response.status_code, 403)

    # detail record
    response = client.get(baseUrl + "jobResultFile/2/")
    self.assertEqual(response.status_code, 200)
    result = json.loads(response.content)
    self.assertEqual(result['job'], 2)
    self.assertEqual(result['notes'], 'Fake Result File2')

