import json

from django.test import TestCase
from rest_framework.test import APIClient

from utils import Add_Update_Delete, Ensure_All_403_Or_Fail
from .config import *


class APIProjectPermissionsTest(TestCase):
  fixtures = ['api_test_data.yaml']

  def test_unauthedRequests(self):
    client = APIClient()
    Ensure_All_403_Or_Fail(self, client, baseUrl + "projectPermissions/")
    Ensure_All_403_Or_Fail(self, client, baseUrl + "projectPermissions/1/")

  def test_adminUserAccess(self):
    client = APIClient()
    self.assertEqual(
      client.login(username=adminUserName, password=adminUserPass), 
      True)

    # List records
    response = client.get(baseUrl + "projectPermissions/")
    self.assertEqual(response.status_code, 200)
    data = json.loads(response.content)
    self.assertTrue('count' in data)
    self.assertEqual(data.get('count'), 0)

    # retrieve an existing record - admin access
    response = client.get(baseUrl + "projectPermissions/1/")
    self.assertEqual(response.status_code, 200)
    data = json.loads(response.content)
    self.assertEqual(data.get('id'), 1)
    self.assertEqual(data.get('project'), 2)
    self.assertEqual(data.get('user'), 3)
    self.assertEqual(data.get('canRead'), True)
    self.assertEqual(data.get('canWrite'), False)

    # modify existing record
    data['canWrite'] = True
    response = client.put(baseUrl + "projectPermissions/1/", data)
    self.assertEqual(response.status_code, 200)
    data = json.loads(response.content)
    self.assertEqual(data.get('canWrite'), True)

    # re-retrieve existing record
    # NOTE: we want to ensure the the user of an existing record 
    # isn't changed after the above update
    response = client.get(baseUrl + "projectPermissions/1/")
    self.assertEqual(response.status_code, 200)
    data = json.loads(response.content)
    self.assertEqual(data.get('id'), 1)
    self.assertEqual(data.get('project'), 2)
    self.assertEqual(data.get('user'), 3)
    self.assertEqual(data.get('canRead'), True)
    self.assertEqual(data.get('canWrite'), True)

    # add a new record
    Add_Update_Delete(self, client, "canRead", True, baseUrl + "projectPermissions/", {
      "project": 2,
      "user": 1,
      "canRead": True,
      "canWrite": False,
      })

  def test_normalUserAccess(self):
    client = APIClient()
    self.assertEqual(
      client.login(username=normalUserName, password=normalUserPass), 
      True)

    # list records
    response = client.get(baseUrl + "projectPermissions/")
    self.assertEqual(response.status_code, 200)
    data = json.loads(response.content)
    self.assertTrue('count' in data)
    self.assertEqual(data.get('count'), 1)
    results = data.get('results')
    self.assertEqual(results[0]['id'], 1)
    self.assertEqual(results[0]['project'], 2)
    self.assertEqual(results[0]['user'], 3)
    self.assertEqual(results[0]['canRead'], True)
    self.assertEqual(results[0]['canWrite'], False)

    # retrieve an existing record
    response = client.get(baseUrl + "projectPermissions/1/")
    self.assertEqual(response.status_code, 200)
    data = json.loads(response.content)
    self.assertEqual(data.get('id'), 1)
    self.assertEqual(data.get('project'), 2)
    self.assertEqual(data.get('user'), 3)
    self.assertEqual(data.get('canRead'), True)
    self.assertEqual(data.get('canWrite'), False)

    # modify existing record
    data['canWrite'] = True
    response = client.put(baseUrl + "projectPermissions/1/", data)
    self.assertEqual(response.status_code, 200)
    data = json.loads(response.content)
    self.assertEqual(data.get('canWrite'), True)

    # add a new record
    Add_Update_Delete(self, client, "canRead", True, baseUrl + "projectPermissions/", {
      "project": 2,
      "user": 1,
      "canRead": True,
      "canWrite": False,
      })

  def test_otherUserAccess(self):
    client = APIClient()
    self.assertEqual(
      client.login(username=user2Name, password=user2Pass), 
      True)

    # list records
    response = client.get(baseUrl + "projectPermissions/")
    self.assertEqual(response.status_code, 200)
    data = json.loads(response.content)
    self.assertTrue('count' in data)
    self.assertEqual(data.get('count'), 0)

    # test accessing the existing record from another user
    response = client.get(baseUrl + "projectPermissions/1/")
    self.assertEqual(response.status_code, 403)

    # test update
    response = client.put(baseUrl + "projectPermissions/1/", {})
    self.assertEqual(response.status_code, 403)

