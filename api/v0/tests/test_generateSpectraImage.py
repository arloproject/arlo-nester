from copy import deepcopy

from django.test import TestCase
from rest_framework.test import APIClient

from .config import *
from .utils import Ensure_All_403_Or_Fail
from tools.views.spectra import SpectraImage


def dummy_generateSpectraImage(spectraImageSettings):

  # Check Settings
  if not all( (
        (spectraImageSettings.startTime == 0),
        (spectraImageSettings.endTime == 1),
        (spectraImageSettings.minFrequency == 20),
        (spectraImageSettings.maxFrequency == 20000),
        (spectraImageSettings.numFrequencyBands == 32),
        (spectraImageSettings.numFramesPerSecond == 32),
        (spectraImageSettings.dampingFactor == 0.02),
        (spectraImageSettings.gain == 1)
        )):
    return None


  spectraImage = SpectraImage()
  spectraImage.spectraImageSettings = spectraImageSettings
  spectraImage.imageFilePath = "TestImageFilePath"
  spectraImage.audioSegmentFilePath = "TestAudioSegmentFilePath"
  spectraImage.imageUrl = "TestImageUrl"
  spectraImage.audioSegmentUrl = "TestAudioSegmentUrl"
  spectraImage.imageWidth = 100
  spectraImage.imageHeight = 200


  return spectraImage


class APIGeneratePitchTraceTest(TestCase):
  fixtures = ['api_test_data.yaml']

  sampleSpectraParams = {
                         'startTime': 0,
                         'endTime': 1,
                         'minFrequency': 20,
                         'maxFrequency': 20000,
                         'numFrequencyBands': 32,
                         'numTimeFramesPerSecond': 32,
                         'dampingFactor': 0.02,
                         'gain': 1,
                         }


  def test_unauthedRequests(self):
    client = APIClient()
    Ensure_All_403_Or_Fail(self, client, baseUrl + "generateSpectraImage/1/")


  def test_permissionDenied(self):
    client = APIClient()
    self.assertEqual(
      client.login(username=normalUserName, password=normalUserPass),
      True)

    # Admin's file
    response = client.post(baseUrl + "generateSpectraImage/1/", data=self.sampleSpectraParams)
    self.assertEqual(response.status_code, 401)


  def test_missingData(self):
    client = APIClient()
    self.assertEqual(
      client.login(username=normalUserName, password=normalUserPass),
      True)

    params = deepcopy(self.sampleSpectraParams)
    del params['startTime']
    response = client.post(baseUrl + "generateSpectraImage/3/", data=params)
    self.assertEqual(response.status_code, 400)


  def test_generateSpectraImage(self):
    client = APIClient()
    self.assertEqual(
      client.login(username=normalUserName, password=normalUserPass),
      True)

    # Stub out function
    original_generateSpectraImage = SpectraImage.generateSpectraImage
    # not sure how this will work with Python 3...
    SpectraImage.generateSpectraImage = staticmethod(dummy_generateSpectraImage)

    # All Params
    response = client.post(baseUrl + "generateSpectraImage/3/", data=self.sampleSpectraParams)
    self.assertEqual(response.status_code, 200)

    # Works without Gain
    params = deepcopy(self.sampleSpectraParams)
    del params['gain']
    response = client.post(baseUrl + "generateSpectraImage/3/", data=params)
    self.assertEqual(response.status_code, 200)

    # Different Gain shouldn't match settings for the dummy function, raising an error
    params = deepcopy(self.sampleSpectraParams)
    params['gain'] = 2
    response = client.post(baseUrl + "generateSpectraImage/3/", data=params)
    self.assertEqual(response.status_code, 500)


    # Un-Stub function
    SpectraImage.generateSpectraImage = original_generateSpectraImage
