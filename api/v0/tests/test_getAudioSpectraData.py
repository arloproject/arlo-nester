from copy import deepcopy

from django.test import TestCase
from rest_framework.test import APIClient

from .config import *
from .utils import Ensure_All_403_Or_Fail
from tools.Meandre import AdaptInterface


def dummy_GetAudioSpectraData(self, mediaFile, startTime, endTime, numTimeFramesPerSecond, numFrequencyBands, dampingRatio, minFrequency, maxFrequency):

  # Check Settings Common to FUNDAMENTAL and MAX_ENERGY
  if not all( (
        (mediaFile.id == 3),
        (startTime == 0),
        (endTime == 0.75),
        (numTimeFramesPerSecond == 32),
        (numFrequencyBands == 64),
        (dampingRatio == 0.02),
        (minFrequency == 20),
        (maxFrequency == 20000)
        )):
    return None

  return {
    "spectraData": {
        "frames": {
            "227.0": [
                2582,
                42785,
                9456,
                9167
            ],
            "228.0": [
                131,
                282,
                622,
                891
            ],
            "227.5": [
                973,
                3303,
                1985,
                2048
            ]
        },
        "freqBands": [
            20.0,
            92.83177667225559,
            430.8869380063768,
            2000.0000000000005
        ],
        "params": {
            "minFrequency": 20.0,
            "numTimeFramesPerSecond": 2,
            "numFrequencyBands": 4,
            "maxFrequency": 2000.0,
            "dampingRatio": 0.02,
            "startTime": 227.0,
            "endTime": 228.46694,
            "mediaFileId": 179070
        }
    }
  }

class APIGetAudioSpectraDataTest(TestCase):
  fixtures = ['api_test_data.yaml']

  sampleParams = {
      'startTime': 0,
      'endTime': 0.75,
      'numTimeFramesPerSecond': 32,
      'numFrequencyBands': 64,
      'dampingFactor': 0.02,
      'minFrequency': 20,
      'maxFrequency': 20000,
  }


  def test_unauthedRequests(self):
    client = APIClient()
    Ensure_All_403_Or_Fail(self, client, baseUrl + "getAudioSpectraData/1/")


  def test_permissionDenied(self):
    client = APIClient()
    self.assertEqual(
      client.login(username=normalUserName, password=normalUserPass),
      True)

    response = client.get(baseUrl + "getAudioSpectraData/1/", data=self.sampleParams)
    self.assertEqual(response.status_code, 401)


  def test_missingData(self):
    client = APIClient()
    self.assertEqual(
      client.login(username=normalUserName, password=normalUserPass),
      True)

    params = deepcopy(self.sampleParams)
    del params['minFrequency']
    response = client.get(baseUrl + "getAudioSpectraData/3/", data=params)
    self.assertEqual(response.status_code, 400)


  def test_getAudioSpectraData(self):
    client = APIClient()
    self.assertEqual(
      client.login(username=normalUserName, password=normalUserPass),
      True)

    # Stub out function
    original_GetAudioSpectraData = AdaptInterface.GetAudioSpectraData
    AdaptInterface.GetAudioSpectraData = dummy_GetAudioSpectraData

    response = client.get(baseUrl + "getAudioSpectraData/3/", data=self.sampleParams)
    self.assertEqual(response.status_code, 200)

    # Un-Stub function
    AdaptInterface.GetAudioSpectraData = original_GetAudioSpectraData
