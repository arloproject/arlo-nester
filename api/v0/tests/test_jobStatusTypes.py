import json

from django.test import TestCase
from rest_framework.test import APIClient

from utils import Add_Update_Delete, Ensure_All_403_Or_Fail
from .config import *


class APIJobStatusTypesTest(TestCase):
  fixtures = ['api_test_data']

  def test_unauthedRequests(self):
    client = APIClient()
    Ensure_All_403_Or_Fail(self, client, baseUrl + "jobStatusTypes/")
    Ensure_All_403_Or_Fail(self, client, baseUrl + "jobStatusTypes/1/")

  def userAccessTest(self, client):
    # Test the GET
    response = client.get(baseUrl + "jobStatusTypes/")
    self.assertEqual(response.status_code, 200)
    data = json.loads(response.content)
    self.assertTrue('count' in data)
    self.assertEqual(data.get('count'), 7)
    self.assertTrue('results' in data)
    results = data.get('results')
    for result in results:
      id = result['id']
      if id == 1:
        self.assertEqual(result['name'], 'Unknown')
      elif id == 2:
        self.assertEqual(result['name'], 'Queued')
      elif id == 3:
        self.assertEqual(result['name'], 'Running')
      elif id == 4:
        self.assertEqual(result['name'], 'Error')
      elif id == 5:
        self.assertEqual(result['name'], 'Stopped')
      elif id == 6:
        self.assertEqual(result['name'], 'Complete')
      elif id == 7:
        self.assertEqual(result['name'], 'Assigned')
      else:
        self.fail("Unknown ID Found ({})".format(id))


    # POST should fail
    response = client.post(baseUrl + 'jobStatusTypes/', {})
    self.assertEqual(response.status_code, 405)
    
    # Test the GET
    response = client.get(baseUrl + "jobStatusTypes/1/")
    self.assertEqual(response.status_code, 200)
    data = json.loads(response.content)
    self.assertTrue('id' in data)
    self.assertEqual(data.get('id'), 1)
    self.assertTrue('name' in data)
    self.assertEqual(data.get('name'), 'Unknown')

    # PUT should fail
    response = client.post(baseUrl + 'jobStatusTypes/1/', {})
    self.assertEqual(response.status_code, 405)

  def test_AdminAccess(self):
    client = APIClient()
    self.assertEqual(
      client.login(username=adminUserName, password=adminUserPass),
      True)
    self.userAccessTest(client)

  def test_UserAccess(self):
    client = APIClient()
    self.assertEqual(
      client.login(username=normalUserName, password=normalUserPass),
      True)
    self.userAccessTest(client)

