import json

from django.test import TestCase
from rest_framework.test import APIClient

from .utils import Add_Update_Delete, Ensure_All_403_Or_Fail
from .config import *


class APIMediaFileUserMetaDataTest(TestCase):
  fixtures = ['api_test_data.yaml']

  def test_unauthedRequests(self):
    client = APIClient()
    Ensure_All_403_Or_Fail(self, client, baseUrl + "mediaFileUserMetaData/")
    Ensure_All_403_Or_Fail(self, client, baseUrl + "mediaFileUserMetaData/1/")

  def test_adminUserAccess(self):
    client = APIClient()
    self.assertEqual(
      client.login(username=adminUserName, password=adminUserPass),
      True)

    # List records
    response = client.get(baseUrl + "mediaFileUserMetaData/")
    self.assertEqual(response.status_code, 200)
    data = json.loads(response.content)
    self.assertTrue('count' in data)
    self.assertEqual(data.get('count'), 2)
    results = data.get('results')
    for result in results:
      pk = result['id']
      if pk == 1:
        self.assertEqual(result['mediaFile'], 1)
        self.assertEqual(result['metaDataField'], 1)
        self.assertEqual(result['value'], 'TestMetaData1')
      elif pk == 2:
        self.assertEqual(result['mediaFile'], 1)
        self.assertEqual(result['metaDataField'], 2)
        self.assertEqual(result['value'], 'TestMetaData2')
      else:
        self.fail("Unknown ID Found ({})".format(pk))

    # retrieve an existing record - admin override
    response = client.get(baseUrl + "mediaFileUserMetaData/3/")
    self.assertEqual(response.status_code, 200)
    data = json.loads(response.content)
    self.assertEqual(data.get('id'), 3)
    self.assertEqual(data['mediaFile'], 2)
    self.assertEqual(data['metaDataField'], 3)
    self.assertEqual(data['value'], 'TestMetaData3')

    Add_Update_Delete(self, client, "value", "changeValue", baseUrl + "mediaFileUserMetaData/", {
      'mediaFile': 2,
      'metaDataField': 5,
      'value': 'newValue',
      })

  def test_normalUserAccess(self):
    client = APIClient()
    self.assertEqual(
      client.login(username=normalUserName, password=normalUserPass),
      True)

    # List records
    response = client.get(baseUrl + "mediaFileUserMetaData/")
    self.assertEqual(response.status_code, 200)
    data = json.loads(response.content)
    self.assertTrue('count' in data)
    self.assertEqual(data.get('count'), 2)
    results = data.get('results')
    for result in results:
      pk = result['id']
      if pk == 3:
        self.assertEqual(result['mediaFile'], 2)
        self.assertEqual(result['metaDataField'], 3)
        self.assertEqual(result['value'], 'TestMetaData3')
      elif pk == 4:
        self.assertEqual(result['mediaFile'], 2)
        self.assertEqual(result['metaDataField'], 4)
        self.assertEqual(result['value'], 'TestMetaData4')
      else:
        self.fail("Unknown ID Found ({})".format(pk))

    # should have no access
    response = client.get(baseUrl + "mediaFileUserMetaData/1/")
    self.assertEqual(response.status_code, 403)

    # retrieve an existing record
    response = client.get(baseUrl + "mediaFileUserMetaData/3/")
    self.assertEqual(response.status_code, 200)
    data = json.loads(response.content)
    self.assertEqual(data.get('id'), 3)
    self.assertEqual(data['mediaFile'], 2)
    self.assertEqual(data['metaDataField'], 3)
    self.assertEqual(data['value'], 'TestMetaData3')

    # Try to create metadata for a mediafile to which this user does not have access
    response = client.post(baseUrl + "mediaFileUserMetaData/", {
      "mediaFile": 1,
      'metaDataField': 2,
      'value': 'newValue',
      })
    self.assertEqual(response.status_code, 403)

    Add_Update_Delete(self, client, "value", "changeValue", baseUrl + "mediaFileUserMetaData/", {
      'mediaFile': 2,
      'metaDataField': 5,
      'value': 'newValue',
      })

  def test_otherUserAccess(self):
    client = APIClient()
    self.assertEqual(
      client.login(username=user2Name, password=user2Pass),
      True)

    # List records
    response = client.get(baseUrl + "mediaFileUserMetaData/")
    self.assertEqual(response.status_code, 200)
    data = json.loads(response.content)
    self.assertTrue('count' in data)
    self.assertEqual(data.get('count'), 0)

    # test accessing the existing record from another user
    response = client.get(baseUrl + "mediaFileUserMetaData/1/")
    self.assertEqual(response.status_code, 403)

    # test update
    response = client.put(baseUrl + "mediaFileUserMetaData/1/", {})
    self.assertEqual(response.status_code, 403)
