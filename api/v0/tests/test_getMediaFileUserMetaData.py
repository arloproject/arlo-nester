import json

from django.test import TestCase
from rest_framework.test import APIClient

from utils import Add_Update_Delete, Ensure_All_403_Or_Fail
from .config import *


class APIgetMediaFileUserMetaDataTest(TestCase):
  fixtures = ['api_test_data.yaml']

  def test_unauthedRequests(self):
    client = APIClient()
    Ensure_All_403_Or_Fail(self, client, baseUrl + "mediaFile/2/userMetaData/")

  def test_adminUserAccess(self):
    client = APIClient()
    self.assertEqual(
      client.login(username=adminUserName, password=adminUserPass), 
      True)

    # List records
    response = client.get(baseUrl + "mediaFile/1/userMetaData/")
    self.assertEqual(response.status_code, 200)
    data = json.loads(response.content)
    self.assertEqual(data['count'], 2)
    for result in data['results']:
      id = result['id']
      if id == 1:
        self.assertEqual(result['metaDataField'], 1)
        self.assertEqual(result['value'], "TestMetaData1")
      elif id == 2:
        self.assertEqual(result['metaDataField'], 2)
        self.assertEqual(result['value'], "TestMetaData2")
      else:
        self.fail("Unknown ID Found ({})".format(id))

    # List records - Admin access
    response = client.get(baseUrl + "mediaFile/2/userMetaData/")
    self.assertEqual(response.status_code, 200)
    data = json.loads(response.content)
    self.assertEqual(data['count'], 2)
    for result in data['results']:
      id = result['id']
      if id == 3:
        self.assertEqual(result['metaDataField'], 3)
        self.assertEqual(result['value'], "TestMetaData3")
      elif id == 4:
        self.assertEqual(result['metaDataField'], 4)
        self.assertEqual(result['value'], "TestMetaData4")
      else:
        self.fail("Unknown ID Found ({})".format(id))


  def test_normalUserAccess(self):
    client = APIClient()
    self.assertEqual(
      client.login(username=normalUserName, password=normalUserPass), 
      True)

    # list records
    response = client.get(baseUrl + "mediaFile/2/userMetaData/")
    self.assertEqual(response.status_code, 200)
    data = json.loads(response.content)
    self.assertEqual(data['count'], 2)
    for result in data['results']:
      id = result['id']
      if id == 3:
        self.assertEqual(result['metaDataField'], 3)
        self.assertEqual(result['value'], "TestMetaData3")
      elif id == 4:
        self.assertEqual(result['metaDataField'], 4)
        self.assertEqual(result['value'], "TestMetaData4")
      else:
        self.fail("Unknown ID Found ({})".format(id))


  def test_otherUserAccess(self):
    client = APIClient()
    self.assertEqual(
      client.login(username=user2Name, password=user2Pass), 
      True)

    # List records - should not have access
    response = client.get(baseUrl + "mediaFile/1/userMetaData/")
    self.assertEqual(response.status_code, 401)

