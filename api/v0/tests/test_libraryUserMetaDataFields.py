import json

from django.test import TestCase
from rest_framework.test import APIClient

from .utils import Ensure_All_403_Or_Fail
from .config import *


class APIgetLibraryUserMetaDataFieldsTest(TestCase):
  fixtures = ['api_test_data.yaml']

  def test_unauthedRequests(self):
    client = APIClient()
    Ensure_All_403_Or_Fail(self, client, baseUrl + "library/2/userMetaDataFields/")

  def test_adminUserAccess(self):
    client = APIClient()
    self.assertEqual(
      client.login(username=adminUserName, password=adminUserPass),
      True)

    # list records
    response = client.get(baseUrl + "library/1/userMetaDataFields/")
    self.assertEqual(response.status_code, 200)
    data = json.loads(response.content)
    self.assertEqual(data['count'], 2)
    for result in data['results']:
      id = result['id']
      if id == 1:
        self.assertEqual(result['name'], 'Library1TestField1')
      elif id == 2:
        self.assertEqual(result['name'], 'Library1TestField2')
      else:
        self.fail("Unknown ID Found ({})".format(id))

    # List records - Admin access
    response = client.get(baseUrl + "library/2/userMetaDataFields/")
    self.assertEqual(response.status_code, 200)
    data = json.loads(response.content)
    self.assertEqual(data['count'], 3)
    for result in data['results']:
      id = result['id']
      if id == 3:
        self.assertEqual(result['name'], 'Library2TestField1')
      elif id == 4:
        self.assertEqual(result['name'], 'Library2TestField2')
      elif id == 5:
        self.assertEqual(result['name'], 'Library2TestField3')
      else:
        self.fail("Unknown ID Found ({})".format(id))


  def test_normalUserAccess(self):
    client = APIClient()
    self.assertEqual(
      client.login(username=normalUserName, password=normalUserPass),
      True)

    # List records - Admin access
    response = client.get(baseUrl + "library/2/userMetaDataFields/")
    self.assertEqual(response.status_code, 200)
    data = json.loads(response.content)
    self.assertEqual(data['count'], 3)
    for result in data['results']:
      id = result['id']
      if id == 3:
        self.assertEqual(result['name'], 'Library2TestField1')
      elif id == 4:
        self.assertEqual(result['name'], 'Library2TestField2')
      elif id == 5:
        self.assertEqual(result['name'], 'Library2TestField3')
      else:
        self.fail("Unknown ID Found ({})".format(id))

    # list other's records - should fail
    response = client.get(baseUrl + "library/1/userMetaDataFields/")
    self.assertEqual(response.status_code, 401)


  def test_otherUserAccess(self):
    client = APIClient()
    self.assertEqual(
      client.login(username=user2Name, password=user2Pass),
      True)

    # List records - should not have access
    response = client.get(baseUrl + "library/1/userMetaDataFields/")
    self.assertEqual(response.status_code, 401)

