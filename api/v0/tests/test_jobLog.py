import json

from django.test import TestCase
from rest_framework.test import APIClient

from .utils import Add_Update_Delete, Ensure_All_403_Or_Fail
from .config import *


class APIJobLogTest(TestCase):
  fixtures = ['api_test_data.yaml']

  def test_unauthedRequests(self):
    client = APIClient()
    Ensure_All_403_Or_Fail(self, client, baseUrl + "jobLog/")
    Ensure_All_403_Or_Fail(self, client, baseUrl + "jobLog/1/")

  def test_adminUserAccess(self):
    client = APIClient()
    self.assertEqual(
      client.login(username=adminUserName, password=adminUserPass),
      True)

    # List records
    response = client.get(baseUrl + "jobLog/")
    self.assertEqual(response.status_code, 200)
    data = json.loads(response.content)
    self.assertEqual(data.get('count'), 1)
    results = data.get('results')
    self.assertEqual(results[0]['job'], 1)
    self.assertEqual(results[0]['messageDate'], '2012-01-01T01:23:45')
    self.assertEqual(results[0]['message'], 'Test Message 1')

    # retrieve an existing record - Admin override
    response = client.get(baseUrl + "jobLog/2/")
    self.assertEqual(response.status_code, 200)
    data = json.loads(response.content)
    self.assertEqual(data['job'], 2)
    self.assertEqual(data['messageDate'], '2012-01-01T01:23:46')
    self.assertEqual(data['message'], 'Test Message 2')

    # add a new record
    Add_Update_Delete(self, client, "message", "new message", baseUrl + "jobLog/", {
      'job': 2,
      'message': 'SHOULD PASS',
      })

  def test_normalUserAccess(self):
    client = APIClient()
    self.assertEqual(
      client.login(username=normalUserName, password=normalUserPass),
      True)

    # list records
    response = client.get(baseUrl + "jobLog/")
    data = json.loads(response.content)
    self.assertEqual(data.get('count'), 1)
    results = data.get('results')
    data = results[0]
    self.assertEqual(data['job'], 2)
    self.assertEqual(data['messageDate'], '2012-01-01T01:23:46')
    self.assertEqual(data['message'], 'Test Message 2')

    # denied record
    response = client.get(baseUrl + "jobLog/1/")
    self.assertEqual(response.status_code, 403)

    # retrieve an existing record
    response = client.get(baseUrl + "jobLog/2/")
    self.assertEqual(response.status_code, 200)
    data = json.loads(response.content)
    self.assertEqual(data['job'], 2)
    self.assertEqual(data['messageDate'], '2012-01-01T01:23:46')
    self.assertEqual(data['message'], 'Test Message 2')

    # Try creating a record in a different user's Job
    response = client.post(baseUrl + "jobLog/", {
      'job': 1,
      'message': 'SHOULD FAIL',
      })
    self.assertEqual(response.status_code, 403)

    # add a new record
    Add_Update_Delete(self, client, "message", "new message", baseUrl + "jobLog/", {
      'job': 2,
      'message': 'SHOULD PASS',
      })

  def test_otherUserAccess(self):
    client = APIClient()
    self.assertEqual(
      client.login(username=user2Name, password=user2Pass),
      True)
    response = client.get(baseUrl + "jobLog/")
    self.assertEqual(response.status_code, 200)
    data = json.loads(response.content)
    self.assertEqual(data.get('count'), 1)

    # test accessing the existing record from another user
    response = client.get(baseUrl + "jobLog/1/")
    self.assertEqual(response.status_code, 403)

    # test update
    response = client.put(baseUrl + "jobLog/1/", {})
    self.assertEqual(response.status_code, 403)
