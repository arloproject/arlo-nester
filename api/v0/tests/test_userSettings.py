import json

from django.test import TestCase
from rest_framework.test import APIClient

from .utils import Add_Update_Delete, Ensure_All_403_Or_Fail
from .config import *


class APIUserSettingsTest(TestCase):
  fixtures = ['api_test_data.yaml']

  def test_unauthedRequests(self):
    client = APIClient()
    Ensure_All_403_Or_Fail(self, client, baseUrl + "userSettings/")
    Ensure_All_403_Or_Fail(self, client, baseUrl + "userSettings/1/")

  def test_adminUserAccess(self):
    client = APIClient()
    self.assertEqual(
      client.login(username=adminUserName, password=adminUserPass),
      True)

    # List records
    response = client.get(baseUrl + "userSettings/")
    self.assertEqual(response.status_code, 200)
    data = json.loads(response.content)
    self.assertEqual(data.get('count'), 1)
    results = data.get('results')
    for result in results:
      id = result['id']
      if id == 1:
        self.assertEqual(result['user'], 1)
        self.assertEqual(result['name'], 'AdminUserSettings')
        self.assertEqual(result['windowSizeInSeconds'], 4)
      else:
        self.fail("Unknown ID Found ({})".format(id))

    # retrieve an existing record
    response = client.get(baseUrl + "userSettings/1/")
    self.assertEqual(response.status_code, 200)
    data = json.loads(response.content)
    self.assertTrue('id' in data)
    self.assertEqual(data.get('id'), 1)
    self.assertTrue('user' in data)
    self.assertEqual(data.get('user'), 1)
    self.assertTrue('windowSizeInSeconds' in data)
    self.assertEqual(data.get('windowSizeInSeconds'), 4)

    # modify existing record
    data['windowSizeInSeconds'] = 10
    response = client.put(baseUrl + "userSettings/1/", data)
    self.assertEqual(response.status_code, 200)
    data = json.loads(response.content)
    self.assertTrue('name' in data)
    self.assertEqual(data.get('windowSizeInSeconds'), 10)

    # re-retrieve existing record
    # NOTE: we want to ensure the the user of an existing record
    # isn't changed after the above update
    response = client.get(baseUrl + "userSettings/1/")
    self.assertEqual(response.status_code, 200)
    data = json.loads(response.content)
    self.assertTrue('id' in data)
    self.assertEqual(data.get('id'), 1)
    self.assertTrue('user' in data)
    self.assertEqual(data.get('user'), 1)
    self.assertTrue('windowSizeInSeconds' in data)
    self.assertEqual(data.get('windowSizeInSeconds'), 10)

    # add a new record
    Add_Update_Delete(self, client, "name", "newName", baseUrl + "userSettings/", {
      'name': "NewUserSettings",
      'windowSizeInSeconds': 5,
      'spectraMinimumBandFrequency': 20,
      'spectraMaximumBandFrequency': 20000,
      'spectraDampingFactor': 0.02,
      'spectraNumFrequencyBands': 256,
      'spectraNumFramesPerSecond': 128,
      'showSpectra': True,
      'showWaveform': False,
      'loadAudio': True,
      'maxNumAudioFilesToList': 100,
      'fileViewWindowSizeInSeconds': 4,
      'fileViewSpectraMinimumBandFrequency': 20,
      'fileViewSpectraMaximumBandFrequency': 20000,
      'fileViewSpectraDampingFactor': 0.02,
      'fileViewSpectraNumFrequencyBands': 256,
      'fileViewSpectraNumFramesPerSecond': 128,
      'catalogViewWindowSizeInSeconds': 4,
      'catalogViewSpectraMinimumBandFrequency': 20,
      'catalogViewSpectraMaximumBandFrequency': 20000,
      'catalogViewSpectraDampingFactor': 0.02,
      'catalogViewSpectraNumFrequencyBands': 256,
      'catalogViewSpectraNumFramesPerSecond': 128,
      'tagViewWindowSizeInSeconds': 4,
      'tagViewSpectraMinimumBandFrequency': 20,
      'tagViewSpectraMaximumBandFrequency': 20000,
      'tagViewSpectraDampingFactor': 0.02,
      'tagViewSpectraNumFrequencyBands': 256,
      'tagViewSpectraNumFramesPerSecond': 128,
      })

  def test_normalUserAccess(self):
    client = APIClient()
    self.assertEqual(
      client.login(username=normalUserName, password=normalUserPass),
      True)

    # List records
    response = client.get(baseUrl + "userSettings/")
    self.assertEqual(response.status_code, 200)
    data = json.loads(response.content)
    self.assertEqual(data.get('count'), 1)
    results = data.get('results')
    for result in results:
      id = result['id']
      if id == 2:
        self.assertEqual(result['user'], 2)
        self.assertEqual(result['name'], 'default')
        self.assertEqual(result['windowSizeInSeconds'], 5)
      else:
        self.fail("Unknown ID Found ({})".format(id))

    # retrieve someone else's record
    response = client.get(baseUrl + "userSettings/1/")
    self.assertEqual(response.status_code, 403)

    # retrieve an existing record
    response = client.get(baseUrl + "userSettings/2/")
    self.assertEqual(response.status_code, 200)
    data = json.loads(response.content)
    self.assertTrue('id' in data)
    self.assertEqual(data.get('id'), 2)
    self.assertTrue('user' in data)
    self.assertEqual(data.get('user'), 2)
    self.assertTrue('windowSizeInSeconds' in data)
    self.assertEqual(data.get('windowSizeInSeconds'), 5)

    # add a new record
    Add_Update_Delete(self, client, "name", "newName", baseUrl + "userSettings/", {
      'name': "NewUserSettings",
      'windowSizeInSeconds': 5,
      'spectraMinimumBandFrequency': 20,
      'spectraMaximumBandFrequency': 20000,
      'spectraDampingFactor': 0.02,
      'spectraNumFrequencyBands': 256,
      'spectraNumFramesPerSecond': 128,
      'showSpectra': True,
      'showWaveform': False,
      'loadAudio': True,
      'maxNumAudioFilesToList': 100,
      'fileViewWindowSizeInSeconds': 4,
      'fileViewSpectraMinimumBandFrequency': 20,
      'fileViewSpectraMaximumBandFrequency': 20000,
      'fileViewSpectraDampingFactor': 0.02,
      'fileViewSpectraNumFrequencyBands': 256,
      'fileViewSpectraNumFramesPerSecond': 128,
      'catalogViewWindowSizeInSeconds': 4,
      'catalogViewSpectraMinimumBandFrequency': 20,
      'catalogViewSpectraMaximumBandFrequency': 20000,
      'catalogViewSpectraDampingFactor': 0.02,
      'catalogViewSpectraNumFrequencyBands': 256,
      'catalogViewSpectraNumFramesPerSecond': 128,
      'tagViewWindowSizeInSeconds': 4,
      'tagViewSpectraMinimumBandFrequency': 20,
      'tagViewSpectraMaximumBandFrequency': 20000,
      'tagViewSpectraDampingFactor': 0.02,
      'tagViewSpectraNumFrequencyBands': 256,
      'tagViewSpectraNumFramesPerSecond': 128,
      })

  def test_otherUserAccess(self):
    client = APIClient()
    self.assertEqual(
      client.login(username=user2Name, password=user2Pass),
      True)

    # Test list
    response = client.get(baseUrl + "userSettings/")
    self.assertEqual(response.status_code, 200)
    data = json.loads(response.content)
    self.assertEqual(data.get('count'), 0)

    # test accessing the existing record from another user
    response = client.get(baseUrl + "userSettings/1/")
    self.assertEqual(response.status_code, 403)

    # test update
    response = client.put(baseUrl + "userSettings/1/", {})
    self.assertEqual(response.status_code, 403)

