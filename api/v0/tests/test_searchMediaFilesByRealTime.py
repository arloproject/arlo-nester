from django.test import TestCase

class SampleTest(TestCase):
#  def test_sample(self):
#    self.assertEqual(0,1)
  def test_2(self):
    self.assertEqual(0,0)

from rest_framework.test import APIClient

import json

from .utils import Ensure_All_403_Or_Fail
from .config import *


class APISearchMediaFilesByRealTimeTest(TestCase):
  fixtures = ['api_test_data.yaml']

  def test_unauthedRequests(self):
    client = APIClient()
    Ensure_All_403_Or_Fail(self, client, baseUrl + "searchMediaFilesByRealTime/2/")

  def searchTest(self, client):
    searchStartDate = "2010-09-28 19:48:53"
    searchEndDate = "2010-09-28 19:48:58"

    # list records
    response = client.get(baseUrl + "searchMediaFilesByRealTime/2/", {
      'startDate': searchStartDate,
      'endDate': searchEndDate,
      })
    self.assertEqual(response.status_code, 200)
    data = json.loads(response.content)
    numFound = 0
    for result in data:
      if result['id'] == 4:
        self.assertEqual(result['alias'], "notReallyAnUploadedFile-id4")
        self.assertEqual(result['realStartTime'], "2010-09-28T19:48:55")
        self.assertEqual(result['sensorArrayId'], 2)
        self.assertEqual(result['sensorId'], 2)
        numFound += 1
      if result['id'] == 5:
        self.assertEqual(result['alias'], "notReallyAnUploadedFile-id5")
        self.assertEqual(result['realStartTime'], "2010-09-28T19:48:56")
        self.assertEqual(result['sensorArrayId'], None)
        self.assertEqual(result['sensorId'], None)
        numFound += 1
    self.assertEqual(numFound, 2)

  def test_adminUserAccess(self):
    searchStartDate = "2010-09-28 19:48:53"
    searchEndDate = "2010-09-28 19:48:58"

    client = APIClient()
    self.assertEqual(
      client.login(username=adminUserName, password=adminUserPass),
      True)

    # Make sure missing parameters throw an error
    response = client.get(baseUrl + "searchMediaFilesByRealTime/2/")
    self.assertEqual(response.status_code, 400)

    response = client.get(baseUrl + "searchMediaFilesByRealTime/2/",{
      'startDate': searchStartDate,
      })
    self.assertEqual(response.status_code, 400)

    response = client.get(baseUrl + "searchMediaFilesByRealTime/2/",{
      'endDate': searchEndDate,
      })
    self.assertEqual(response.status_code, 400)

    # Test improper format of parameters
    response = client.get(baseUrl + "searchMediaFilesByRealTime/2/",{
      'startDate': "2010-09-28 19:48:53z",
      'endDate': "2010-09-28 19:48:58",
      })
    self.assertEqual(response.status_code, 400)

    response = client.get(baseUrl + "searchMediaFilesByRealTime/2/",{
      'startDate': "2010-28-09 19:48:53",
      'endDate': "2010-09-28 19:48:58",
      })
    self.assertEqual(response.status_code, 400)

    # List records - Admin access
    self.searchTest(client)


  def test_normalUserAccess(self):
    searchStartDate = "2010-09-28 19:48:53"
    searchEndDate = "2010-09-28 19:48:58"

    client = APIClient()
    self.assertEqual(
      client.login(username=normalUserName, password=normalUserPass),
      True)

    # Make sure missing parameters throw an error
    response = client.get(baseUrl + "searchMediaFilesByRealTime/2/")
    self.assertEqual(response.status_code, 400)

    response = client.get(baseUrl + "searchMediaFilesByRealTime/2/", {
      'startDate': searchStartDate,
      })
    self.assertEqual(response.status_code, 400)

    response = client.get(baseUrl + "searchMediaFilesByRealTime/2/", {
      'endDate': searchEndDate,
      })
    self.assertEqual(response.status_code, 400)

    # Test improper format of parameters
    response = client.get(baseUrl + "searchMediaFilesByRealTime/2/", {
      'startDate': "2010-09-28 19:48:53z",
      'endDate': "2010-09-28 19:48:58",
      })
    self.assertEqual(response.status_code, 400)

    response = client.get(baseUrl + "searchMediaFilesByRealTime/2/", {
      'startDate': "2010-28-09 19:48:53",
      'endDate': "2010-09-28 19:48:58",
      })
    self.assertEqual(response.status_code, 400)

    # test successful search
    self.searchTest(client)


  def test_otherUserAccess(self):
    searchStartDate = "2010-09-28 19:48:53"
    searchEndDate = "2010-09-28 19:48:58"

    client = APIClient()
    self.assertEqual(
      client.login(username=user2Name, password=user2Pass),
      True)

    # Make sure missing parameters throw an error
    response = client.get(baseUrl + "searchMediaFilesByRealTime/3/")
    self.assertEqual(response.status_code, 400)

    response = client.get(baseUrl + "searchMediaFilesByRealTime/3/", {
      'startDate': searchStartDate,
      })
    self.assertEqual(response.status_code, 400)

    response = client.get(baseUrl + "searchMediaFilesByRealTime/3/", {
      'endDate': searchEndDate,
      })
    self.assertEqual(response.status_code, 400)

    # Test improper format of parameters
    response = client.get(baseUrl + "searchMediaFilesByRealTime/3/", {
      'startDate': "2010-09-28 19:48:53z",
      'endDate': "2010-09-28 19:48:58",
      })
    self.assertEqual(response.status_code, 400)

    response = client.get(baseUrl + "searchMediaFilesByRealTime/3/", {
      'startDate': "2010-28-09 19:48:53",  # Day / Month Flipped
      'endDate': "2010-09-28 19:48:58",
      })
    self.assertEqual(response.status_code, 400)

    # List records - should not have access
    response = client.get(baseUrl + "searchMediaFilesByRealTime/3/", {
      'startDate': searchStartDate,
      'endDate': searchEndDate,
      })
    self.assertEqual(response.status_code, 401)

