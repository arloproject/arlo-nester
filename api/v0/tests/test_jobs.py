import json

from django.test import TestCase
from rest_framework.test import APIClient

from utils import Add_Update_Delete, Ensure_All_403_Or_Fail
from .config import *


class APIJobsTest(TestCase):
  fixtures = ['api_test_data.yaml']

  def test_unauthedRequests(self):
    client = APIClient()
    Ensure_All_403_Or_Fail(self, client, baseUrl + "jobs/")
    Ensure_All_403_Or_Fail(self, client, baseUrl + "jobs/1/")

  def test_adminUserAccess(self):
    client = APIClient()
    self.assertEqual(
      client.login(username=adminUserName, password=adminUserPass), 
      True)

    # List records
    response = client.get(baseUrl + "jobs/")
    self.assertEqual(response.status_code, 200)
    data = json.loads(response.content)
    self.assertTrue('count' in data)
    self.assertEqual(data.get('count'), 1)
    results = data.get('results')
    self.assertEqual(results[0]['id'], 1)
    self.assertEqual(results[0]['user'], 1)
    self.assertEqual(results[0]['name'], 'adminJob')
    self.assertEqual(results[0]['type'], 1)
    for jp in results[0]['jobparameters_set']:
      id = jp['id']
      if id == 1:
        self.assertEqual(jp['name'], 'testParam1')
        self.assertEqual(jp['value'], 'testValue1')
      elif id == 2:
        self.assertEqual(jp['name'], 'testParam2')
        self.assertEqual(jp['value'], 'testValue2')
      else:
        self.fail("Unknown ID Found ({})".format(id))

    # retrieve an existing record - Admin Access
    response = client.get(baseUrl + "jobs/2/")
    self.assertEqual(response.status_code, 200)
    data = json.loads(response.content)
    self.assertEqual(data.get('id'), 2)
    self.assertEqual(data.get('user'), 2)
    self.assertEqual(data.get('name'), 'testJob')

    # modify existing record
    data['name'] = 'newJob'
    # use JSON to handle the nulls
    response = client.put(baseUrl + "jobs/2/", json.dumps(data), content_type="application/json")
    self.assertEqual(response.status_code, 200)
    data = json.loads(response.content)
    self.assertTrue('name' in data)
    self.assertEqual(data.get('name'), 'newJob')

    # re-retrieve existing record
    # NOTE: we want to ensure the the user of an existing record 
    # isn't changed after the above update
    response = client.get(baseUrl + "jobs/2/")
    self.assertEqual(response.status_code, 200)
    data = json.loads(response.content)
    self.assertEqual(data.get('id'), 2)
    self.assertEqual(data.get('user'), 2)
    self.assertEqual(data.get('name'), 'newJob')

    # add a new record
    # use JSON to handle the nulls
    response = client.post(baseUrl + "jobs/", json.dumps({
      "name": "newJob",
      "project": 1,
      "type": 1,
      "numToComplete": 0,
      "numCompleted": 0,
      "fractionCompleted": 0,
      "elapsedRealTime": 0,
      "timeToCompletion": 0,
      "isRunning": 0,
      "wasStopped": 0,
      "isComplete": 0,
      "wasDeleted": 0,
      "status": 1,
      "requestedStatus": 1,
      "startedDate": None,
      "lastStatusDate": None,
      "parentJob": None,
      "priority": 0,
      }), content_type="application/json")
    self.assertEqual(response.status_code, 201)
    data = json.loads(response.content)
    self.assertTrue('user' in data)
    self.assertEqual(data.get('user'), 1)
    self.assertTrue('id' in data)
    newId = data.get('id')

    # modify new record
    data['name'] = 'newJobName'
    # use JSON to handle the nulls
    response = client.put(baseUrl + "jobs/" + str(newId) + "/", json.dumps(data), content_type="application/json")
    self.assertEqual(response.status_code, 200)
    data = json.loads(response.content)
    self.assertTrue('name' in data)
    self.assertEqual(data.get('name'), 'newJobName')

    # delete new record
    response = client.delete(baseUrl + "jobs/" + str(newId) + "/")
    self.assertEqual(response.status_code, 204)

    # ensure it's gone
    response = client.get(baseUrl + "jobs/" + str(newId) + "/")
    self.assertEqual(response.status_code, 404)

  def userList(self, client):

    # List records
    response = client.get(baseUrl + "jobs/")
    self.assertEqual(response.status_code, 200)
    data = json.loads(response.content)
    self.assertEqual(data.get('count'), 2)
    results = data.get('results')
    for result in results:
      if result['id'] == 2:
        self.assertEqual(result['user'], 2)
        self.assertEqual(result['name'], 'testJob')
        self.assertEqual(result['type'], 1)
        for jp in result['jobparameters_set']:
          id = jp['id']
          if id == 3:
            self.assertEqual(jp['name'], 'userParam1')
            self.assertEqual(jp['value'], 'userValue1')
          else:
            self.fail("Unknown ID Found ({})".format(id))
      elif result['id'] == 3:
        self.assertEqual(result['id'], 3)
        self.assertEqual(result['user'], 2)
        self.assertEqual(result['name'], 'randomWindowTestJob')
        self.assertEqual(result['type'], 1)
      else:
        self.fail("Unknown ID Found ({})".format(result['id']))


  def test_normalUserAccess(self):
    client = APIClient()
    self.assertEqual(
      client.login(username=normalUserName, password=normalUserPass), 
      True)

    # list records
    self.userList(client)

    # retrieve an existing record
    response = client.get(baseUrl + "jobs/2/")
    self.assertEqual(response.status_code, 200)
    data = json.loads(response.content)
    self.assertEqual(data.get('id'), 2)
    self.assertEqual(data.get('user'), 2)
    self.assertEqual(data.get('name'), 'testJob')

    # add a new record
    # use JSON to handle the nulls
    response = client.post(baseUrl + "jobs/", json.dumps({
      "name": "newJob",
      "project": 2,
      "type": 1,
      "numToComplete": 0,
      "numCompleted": 0,
      "fractionCompleted": 0,
      "elapsedRealTime": 0,
      "timeToCompletion": 0,
      "isRunning": 0,
      "wasStopped": 0,
      "isComplete": 0,
      "wasDeleted": 0,
      "status": 1,
      "requestedStatus": 1,
      "startedDate": None,
      "lastStatusDate": None,
      "parentJob": None,
      "priority": 0,
      }), content_type="application/json")
    self.assertEqual(response.status_code, 201)
    data = json.loads(response.content)
    self.assertEqual(data.get('user'), 2)
    newId = data.get('id')

    # modify new record
    data['name'] = 'newJobName'
    # use JSON to handle the nulls
    response = client.put(baseUrl + "jobs/" + str(newId) + "/", json.dumps(data), content_type="application/json")
    self.assertEqual(response.status_code, 200)
    data = json.loads(response.content)
    self.assertTrue('name' in data)
    self.assertEqual(data.get('name'), 'newJobName')

    # delete new record
    response = client.delete(baseUrl + "jobs/" + str(newId) + "/")
    self.assertEqual(response.status_code, 204)

    # ensure it's gone
    response = client.get(baseUrl + "jobs/" + str(newId) + "/")
    self.assertEqual(response.status_code, 404)

  def test_otherUserAccess(self):
    client = APIClient()
    self.assertEqual(
      client.login(username=user2Name, password=user2Pass), 
      True)

    # List records
    self.userList(client)

    # test accessing the existing record from another user
    response = client.get(baseUrl + "jobs/1/")
    self.assertEqual(response.status_code, 403)

    # test update
    response = client.put(baseUrl + "jobs/1/", {})
    self.assertEqual(response.status_code, 403)

