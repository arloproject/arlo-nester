import json

def Ensure_All_403_Or_Fail(testCase, client, url):
  # test each method against the URL, ensuring that all 403

  response = client.get(url)
  testCase.assertEqual(response.status_code, 403)

  response = client.head(url)
  testCase.assertEqual(response.status_code, 403)

  # As of Git 8c46753 / ARLO-225 OPTIONS should return 200
  # even for Anonymous Requests.
  response = client.options(url)
  testCase.assertEqual(response.status_code, 200)

  response = client.post(url)
  testCase.assertEqual(response.status_code, 403)

  response = client.put(url)
  testCase.assertEqual(response.status_code, 403)

  response = client.delete(url)
  testCase.assertEqual(response.status_code, 403)

  response = client.patch(url)
  testCase.assertEqual(response.status_code, 403)


## Test Adding, Updating, and Deleting a Record
#
# This function Creates, (optionally) Updates, and Deletes a new record through
# the API.
# @param testCase the TestCase this test is running within
# @param client logged-in APIClient instance
# @param modify_key The field to test updating. If 'None', no update test
#                   is performed.
# @param modify_value The data used to test the update.
# @param root_url The base URL where POSTs are made; detail views are accessed
#                 by appending '{id}/' to the URL
# @param data_dict Dictionary of values to add and verify.

def Add_Update_Delete(testCase, client, modify_key, modify_value, root_url, data_dict):

  ##
  # add a new record

  response = client.post(root_url, json.dumps(data_dict), content_type="application/json")
  testCase.assertEqual(response.status_code, 201)
  data = json.loads(response.content)

  testCase.assertTrue('id' in data)
  newId = data.get('id')

  for k in data_dict:
    testCase.assertTrue(k in data)
    testCase.assertEqual(data.get(k), data_dict[k])

  ##
  # modify new record

  if modify_key != None:

    data[modify_key] = modify_value
    response = client.put(root_url + str(newId) + "/", json.dumps(data), content_type="application/json")
    testCase.assertEqual(response.status_code, 200)
    data = json.loads(response.content)
    testCase.assertTrue(modify_key in data)
    testCase.assertEqual(data.get(modify_key), modify_value)

    for k in data_dict:
      if k != modify_key:
        testCase.assertTrue(k in data)
        testCase.assertEqual(data.get(k), data_dict[k])

  # Ensure OPTIONS works on this detail view
  response = client.options(root_url + str(newId) + "/")
  testCase.assertEqual(response.status_code, 200)

  # delete new record
  response = client.delete(root_url + str(newId) + "/")
  testCase.assertEqual(response.status_code, 204)

  # ensure it's gone
  response = client.get(root_url + str(newId) + "/")
  testCase.assertEqual(response.status_code, 404)


