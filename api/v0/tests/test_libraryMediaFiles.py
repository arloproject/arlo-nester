import json

from django.test import TestCase
from rest_framework.test import APIClient

from .utils import Ensure_All_403_Or_Fail
from .config import *


class APIgetLibraryMediaFilesTest(TestCase):
  fixtures = ['api_test_data.yaml']

  def test_unauthedRequests(self):
    client = APIClient()
    Ensure_All_403_Or_Fail(self, client, baseUrl + "library/2/mediaFiles/")

  def test_adminUserAccess(self):
    client = APIClient()
    self.assertEqual(
      client.login(username=adminUserName, password=adminUserPass),
      True)

    # list records
    response = client.get(baseUrl + "library/1/mediaFiles/")
    self.assertEqual(response.status_code, 200)
    data = json.loads(response.content)
    self.assertEqual(data['count'], 1)
    for result in data['results']:
      id = result['id']
      if id == 1:
        self.assertEqual(result['alias'], 'adminFakeFile')
      else:
        self.fail("Unknown ID Found")

    # List records - Admin override
    response = client.get(baseUrl + "library/2/mediaFiles/")
    self.assertEqual(response.status_code, 200)
    data = json.loads(response.content)
    self.assertEqual(data['count'], 5)
    for result in data['results']:
      id = result['id']
      if id == 2:
        self.assertEqual(result['alias'], 'notReallyAnUploadedFile')
      elif id == 3:
        self.assertEqual(result['alias'], 'notReallyAnUploadedFile')
      elif id == 4:
        self.assertEqual(result['alias'], 'notReallyAnUploadedFile-id4')
      elif id == 5:
        self.assertEqual(result['alias'], 'notReallyAnUploadedFile-id5')
      elif id == 6:
        self.assertEqual(result['alias'], 'notReallyAnUploadedFile')
      else:
        self.fail("Unknown ID Found ({})".format(id))


  def test_normalUserAccess(self):
    client = APIClient()
    self.assertEqual(
      client.login(username=normalUserName, password=normalUserPass),
      True)

    # list records
    response = client.get(baseUrl + "library/2/mediaFiles/")
    self.assertEqual(response.status_code, 200)
    data = json.loads(response.content)
    self.assertEqual(data['count'], 5)
    for result in data['results']:
      if result['id'] == 2:
        self.assertEqual(result['alias'], 'notReallyAnUploadedFile')
      elif result['id'] == 3:
        self.assertEqual(result['alias'], 'notReallyAnUploadedFile')
      elif result['id'] == 4:
        self.assertEqual(result['alias'], 'notReallyAnUploadedFile-id4')
      elif result['id'] == 5:
        self.assertEqual(result['alias'], 'notReallyAnUploadedFile-id5')
      elif result['id'] == 6:
        self.assertEqual(result['alias'], 'notReallyAnUploadedFile')
      else:
        self.fail("Unknown ID Found ({})".format(id))

    # should not have access
    response = client.get(baseUrl + "library/1/mediaFiles/")
    self.assertEqual(response.status_code, 401)


  def test_otherUserAccess(self):
    client = APIClient()
    self.assertEqual(
      client.login(username=user2Name, password=user2Pass),
      True)

    # List records - should not have access
    response = client.get(baseUrl + "library/2/mediaFiles/")
    self.assertEqual(response.status_code, 401)

