import json

from django.test import TestCase
from rest_framework.test import APIClient

from .utils import Add_Update_Delete, Ensure_All_403_Or_Fail
from .config import *


class APIArloPermissionTest(TestCase):
  fixtures = ['api_test_data.yaml']


  def test_unauthedRequests(self):
    client = APIClient()
    Ensure_All_403_Or_Fail(self, client, baseUrl + "arloPermission/")
    Ensure_All_403_Or_Fail(self, client, baseUrl + "arloPermission/1/")


  def test_adminUserAccess(self):
    client = APIClient()
    self.assertEqual(
      client.login(username=adminUserName, password=adminUserPass),
      True)

    # List records
    response = client.get(baseUrl + "arloPermission/")
    self.assertEqual(response.status_code, 200)
    data = json.loads(response.content)
    self.assertEqual(data.get('count'), 1)
    results = data.get('results')
    for result in results:
      if result['id'] == 1:
        self.assertEqual(result['bearer_type'], 'user')
        self.assertEqual(result['bearer_id'], 4)
        self.assertEqual(result['target_type'], 'library')
        self.assertEqual(result['target_id'], 1)
        self.assertEqual(result['creationDate'], '2012-01-01T12:34:56')
        self.assertEqual(result['createdBy'], 1)
        self.assertEqual(result['read'], True)
        self.assertEqual(result['write'], True)
        self.assertEqual(result['launch_job'], False)
        self.assertEqual(result['admin'], False)
      else:
        self.fail("Unknown ID Found ({})".format(id))

    # retrieve an existing record
    # note we are using another user's for below test
    response = client.get(baseUrl + "arloPermission/2/")
    self.assertEqual(response.status_code, 200)
    data = json.loads(response.content)
    self.assertEqual(data['bearer_type'], 'user')
    self.assertEqual(data['bearer_id'], 4)
    self.assertEqual(data['target_type'], 'library')
    self.assertEqual(data['target_id'], 2)
    self.assertEqual(data['creationDate'], '2012-01-01T12:34:56')
    self.assertEqual(data['createdBy'], 2)
    self.assertEqual(data['read'], True)
    self.assertEqual(data['write'], False)
    self.assertEqual(data['launch_job'], False)
    self.assertEqual(data['admin'], False)

    # modify existing record
    data['write'] = True
    del data['creationDate']
    del data['createdBy']

    # use JSON to handle the nulls
    response = client.put(baseUrl + "arloPermission/2/", json.dumps(data), content_type="application/json")
    self.assertEqual(response.status_code, 200)
    data = json.loads(response.content)
    self.assertEqual(data.get('write'), True)

    # re-retrieve existing record
    # NOTE: we want to ensure the the createdBy user of an existing record
    # isn't changed after the above update
    response = client.get(baseUrl + "arloPermission/2/")
    self.assertEqual(response.status_code, 200)
    data = json.loads(response.content)
    self.assertEqual(data['createdBy'], 2)
    self.assertEqual(data['read'], True)
    self.assertEqual(data['write'], True)
    self.assertEqual(data['launch_job'], False)
    self.assertEqual(data['admin'], False)

    # add a new record
    Add_Update_Delete(self, client, "launch_job", True, baseUrl + "arloPermission/", {
      'bearer_type': 'user',
      'bearer_id': 5,
      'target_type': 'library',
      'target_id': 1,
      'read': True,
      'write': False,
      'launch_job': False,
      'admin': False
      })


  def test_normalUserAccess(self):
    client = APIClient()
    self.assertEqual(
      client.login(username=normalUserName, password=normalUserPass),
      True)


    response = client.get(baseUrl + "arloPermission/")
    self.assertEqual(response.status_code, 200)
    data = json.loads(response.content)
    self.assertEqual(data.get('count'), 1)
    results = data.get('results')
    for result in results:
      if result['id'] == 2:
        self.assertEqual(result['bearer_type'], 'user')
        self.assertEqual(result['bearer_id'], 4)
        self.assertEqual(result['target_type'], 'library')
        self.assertEqual(result['target_id'], 2)
        self.assertEqual(result['creationDate'], '2012-01-01T12:34:56')
        self.assertEqual(result['createdBy'], 2)
        self.assertEqual(result['read'], True)
        self.assertEqual(result['write'], False)
        self.assertEqual(result['launch_job'], False)
        self.assertEqual(result['admin'], False)
      else:
        self.fail("Unknown ID Found ({})".format(id))


    # denied record
    response = client.get(baseUrl + "arloPermission/1/")
    self.assertEqual(response.status_code, 403)

    # retrieve an existing record
    response = client.get(baseUrl + "arloPermission/2/")
    self.assertEqual(response.status_code, 200)
    data = json.loads(response.content)
    self.assertEqual(data['bearer_type'], 'user')
    self.assertEqual(data['bearer_id'], 4)
    self.assertEqual(data['target_type'], 'library')
    self.assertEqual(data['target_id'], 2)
    self.assertEqual(data['creationDate'], '2012-01-01T12:34:56')
    self.assertEqual(data['createdBy'], 2)
    self.assertEqual(data['read'], True)
    self.assertEqual(data['write'], False)
    self.assertEqual(data['launch_job'], False)
    self.assertEqual(data['admin'], False)

    # modify existing record
    data['write'] = True

    # use JSON to handle the nulls
    response = client.put(baseUrl + "arloPermission/2/", json.dumps(data), content_type="application/json")
    self.assertEqual(response.status_code, 200)
    data = json.loads(response.content)
    self.assertEqual(data['write'], True)

    # add a new record
    Add_Update_Delete(self, client, "launch_job", True, baseUrl + "arloPermission/", {
      'bearer_type': 'user',
      'bearer_id': 5,
      'target_type': 'library',
      'target_id': 2,
      'read': True,
      'write': False,
      'launch_job': False,
      'admin': False
      })



  def test_permUserAccess(self):
    client = APIClient()
    self.assertEqual(
      client.login(username='permsUser', password=normalUserPass),  # SPECIAL
      True)

    response = client.get(baseUrl + "arloPermission/")
    self.assertEqual(response.status_code, 200)
    data = json.loads(response.content)
    self.assertEqual(data.get('count'), 2)
    results = data.get('results')
    for result in results:
      if result['id'] == 1:
        self.assertEqual(result['bearer_type'], 'user')
        self.assertEqual(result['bearer_id'], 4)
        self.assertEqual(result['target_type'], 'library')
        self.assertEqual(result['target_id'], 1)
        self.assertEqual(result['creationDate'], '2012-01-01T12:34:56')
        self.assertEqual(result['createdBy'], 1)
        self.assertEqual(result['read'], True)
        self.assertEqual(result['write'], True)
        self.assertEqual(result['launch_job'], False)
        self.assertEqual(result['admin'], False)
      elif result['id'] == 2:
        self.assertEqual(result['bearer_type'], 'user')
        self.assertEqual(result['bearer_id'], 4)
        self.assertEqual(result['target_type'], 'library')
        self.assertEqual(result['target_id'], 2)
        self.assertEqual(result['creationDate'], '2012-01-01T12:34:56')
        self.assertEqual(result['createdBy'], 2)
        self.assertEqual(result['read'], True)
        self.assertEqual(result['write'], False)
        self.assertEqual(result['launch_job'], False)
        self.assertEqual(result['admin'], False)
      else:
        self.fail("Unknown ID Found ({})".format(id))


    # retrieve an existing record
    response = client.get(baseUrl + "arloPermission/2/")
    self.assertEqual(response.status_code, 200)
    data = json.loads(response.content)
    self.assertEqual(data['bearer_type'], 'user')
    self.assertEqual(data['bearer_id'], 4)
    self.assertEqual(data['target_type'], 'library')
    self.assertEqual(data['target_id'], 2)
    self.assertEqual(data['creationDate'], '2012-01-01T12:34:56')
    self.assertEqual(data['createdBy'], 2)
    self.assertEqual(data['read'], True)
    self.assertEqual(data['write'], False)
    self.assertEqual(data['launch_job'], False)
    self.assertEqual(data['admin'], False)

    # modify existing record, SHOULD FAIL
    data['write'] = True

    # use JSON to handle the nulls
    response = client.put(baseUrl + "arloPermission/2/", json.dumps(data), content_type="application/json")
    self.assertEqual(response.status_code, 403)


  def test_otherUserAccess(self):
    client = APIClient()
    self.assertEqual(
      client.login(username=user2Name, password=user2Pass),
      True)

    # List Records
    response = client.get(baseUrl + "arloPermission/")
    self.assertEqual(response.status_code, 200)
    data = json.loads(response.content)
    self.assertEqual(data.get('count'), 0)

    # test accessing the existing record from another user
    response = client.get(baseUrl + "arloPermission/1/")
    self.assertEqual(response.status_code, 403)

    # test update
    response = client.put(baseUrl + "arloPermission/1/", {})
    self.assertEqual(response.status_code, 403)


