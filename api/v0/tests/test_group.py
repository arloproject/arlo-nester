import json

from django.test import TestCase
from rest_framework.test import APIClient

from utils import Add_Update_Delete, Ensure_All_403_Or_Fail
from .config import *


class APIGroupTest(TestCase):
  fixtures = ['api_test_data']

  def test_unauthedRequests(self):
    client = APIClient()
    Ensure_All_403_Or_Fail(self, client, baseUrl + "group/")
    Ensure_All_403_Or_Fail(self, client, baseUrl + "group/1/")

  def test_AdminAccess(self):
    client = APIClient()
    self.assertEqual(
      client.login(username=adminUserName, password=adminUserPass),
      True)

    # Test the GET
    response = client.get(baseUrl + "group/")
    self.assertEqual(response.status_code, 200)
    data = json.loads(response.content)
    self.assertTrue('count' in data)
    self.assertEqual(data.get('count'), 0)
    self.assertTrue('results' in data)

    # POST should fail
    response = client.post(baseUrl + 'group/', {})
    self.assertEqual(response.status_code, 403)

    # Test the GET
    response = client.get(baseUrl + "group/1/")
    self.assertEqual(response.status_code, 404)

    # PUT should fail
    response = client.post(baseUrl + 'group/1/', {})
    self.assertEqual(response.status_code, 403)

  def test_UserDeny(self):
    client = APIClient()
    self.assertEqual(
      client.login(username=normalUserName, password=normalUserPass),
      True)
    Ensure_All_403_Or_Fail(self, client, baseUrl + "group/")

