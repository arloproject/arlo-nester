import json
from copy import deepcopy

from django.test import TestCase
from rest_framework.test import APIClient

from tools.models import Tag
from .config import *
from .utils import Ensure_All_403_Or_Fail


class APIBulkTagCreateTest(TestCase):
  fixtures = ['api_test_data.yaml']

  sampleTagsDict = [
    {
        "tagSet": 3,
        "mediaFile": 2,
        "startTime": 0.5,
        "endTime": 1.5,
        "minFrequency": 20,
        "maxFrequency": 20000,
        "tagClass": 2,
        "parentTag": 2,
        "randomlyChosen": False,
        "machineTagged": True,
        "userTagged": False,
        "strength": 1
    },
    {
        "tagSet": 2,
        "mediaFile": 2,
        "startTime": 2,
        "endTime": 3,
        "minFrequency": 20,
        "maxFrequency": 20000,
        "tagClass": 2,
        "userTagged": True,
        "strength": 0.01
    }
  ]

  def test_unauthedRequests(self):
    client = APIClient()
    Ensure_All_403_Or_Fail(self, client, baseUrl + "bulkTagCreate/1/")

  def test_successfulCreate(self):
    client = APIClient()
    self.assertEqual(
      client.login(username=normalUserName, password=normalUserPass),
      True)

    # First ensure our Target TagSet is empty
    self.assertEqual(Tag.objects.filter(tagSet=3).count(), 0)


    # Have to POST a List, which this test client doesn't allow us to do.
    # So instead, we can post under the key 'tags' which the function will
    # also look for. Well, this client also doesn't like a list of
    # dictionaries as it's data value, so go ahead and pass this in as
    # rendered JSON...

    data = {'tags': json.dumps(self.sampleTagsDict)}
    response = client.post(baseUrl + "bulkTagCreate/2/", data=data)
    self.assertEqual(response.status_code, 200)


    # We should have inserted 1 into this TagSet
    tags = Tag.objects.filter(tagSet=3)
    self.assertEqual(tags.count(),1)
    # Make sure the user is 'this' normal user's id
    self.assertEqual(tags[0].user.id, 2)


  def test_missingData(self):
    client = APIClient()
    self.assertEqual(
      client.login(username=normalUserName, password=normalUserPass),
      True)

    tagsDict = deepcopy(self.sampleTagsDict)
    del tagsDict[0]['mediaFile']
    response = client.post(baseUrl + "bulkTagCreate/2/", data={'tags': json.dumps(tagsDict)})
    self.assertEqual(response.status_code, 400)


  def test_adminUserOverrideSuccess(self):
    """ This Test should succeed because the 'admin' user can set the user
    to another user id.
    """

    client = APIClient()
    self.assertEqual(
      client.login(username=adminUserName, password=adminUserPass),
      True)

    # First ensure our Target TagSet is empty
    self.assertEqual(Tag.objects.filter(tagSet=3).count(), 0)

    tagsDict = deepcopy(self.sampleTagsDict)
    tagsDict[0]['user'] = 3
    response = client.post(baseUrl + "bulkTagCreate/2/", data={'tags': json.dumps(tagsDict)})
    self.assertEqual(response.status_code, 200)

    # We should have inserted 1 into this TagSet
    tags = Tag.objects.filter(tagSet=3)
    self.assertEqual(tags.count(),1)
    # Make sure the user is the overridden user, not the admin
    self.assertEqual(tags[0].user.id, 3)


  def test_userOverrideDenied(self):
    """ This Test should fail because the 'normal' user can not set the user
    to another user id.
    """

    client = APIClient()
    self.assertEqual(
      client.login(username=normalUserName, password=normalUserPass),
      True)

    # The normal user should not have access to override the user
    tagsDict = deepcopy(self.sampleTagsDict)
    tagsDict[0]['user'] = 3
    response = client.post(baseUrl + "bulkTagCreate/2/", data={'tags': json.dumps(tagsDict)})
    self.assertEqual(response.status_code, 403)


  def test_normalUserOverrideSuccess(self):
    """ This Test should succeed because the 'normal' user can set the user
    to their own user id.
    """

    client = APIClient()
    self.assertEqual(
      client.login(username=adminUserName, password=adminUserPass),
      True)

    # First ensure our Target TagSet is empty
    self.assertEqual(Tag.objects.filter(tagSet=3).count(), 0)

    tagsDict = deepcopy(self.sampleTagsDict)
    tagsDict[0]['user'] = 2
    response = client.post(baseUrl + "bulkTagCreate/2/", data={'tags': json.dumps(tagsDict)})
    self.assertEqual(response.status_code, 200)

    # We should have inserted 1 into this TagSet
    tags = Tag.objects.filter(tagSet=3)
    self.assertEqual(tags.count(),1)
    # Make sure the user is the overridden user, not the admin
    self.assertEqual(tags[0].user.id, 2)


  def test_adminUserOverrideNotFound(self):
    """ This Test should fail because the specified user does not exist.
    """

    client = APIClient()
    self.assertEqual(
      client.login(username=adminUserName, password=adminUserPass),
      True)

    # First ensure our Target TagSet is empty
    self.assertEqual(Tag.objects.filter(tagSet=3).count(), 0)

    tagsDict = deepcopy(self.sampleTagsDict)
    tagsDict[0]['user'] = 100
    response = client.post(baseUrl + "bulkTagCreate/2/", data={'tags': json.dumps(tagsDict)})
    self.assertEqual(response.status_code, 404)


  ###                                                             ##
  # Run the same tests again, but this time with JSON Content-Type #
  ###                                                             ##


  def test_successfulCreate_json(self):
    client = APIClient()
    self.assertEqual(
      client.login(username=normalUserName, password=normalUserPass),
      True)

    # First ensure our Target TagSet is empty
    self.assertEqual(Tag.objects.filter(tagSet=3).count(), 0)


    # Have to POST a List, which this test client doesn't allow us to do.
    # So instead, we can post under the key 'tags' which the function will
    # also look for. Well, this client also doesn't like a list of
    # dictionaries as it's data value, so go ahead and pass this in as
    # rendered JSON...

    data = {'tags': json.dumps(self.sampleTagsDict)}
    response = client.post(baseUrl + "bulkTagCreate/2/", data=data, format='json')
    self.assertEqual(response.status_code, 200)


    # We should have inserted 1 into this TagSet
    tags = Tag.objects.filter(tagSet=3)
    self.assertEqual(tags.count(),1)
    # Make sure the user is 'this' normal user's id
    self.assertEqual(tags[0].user.id, 2)


  def test_missingData_json(self):
    client = APIClient()
    self.assertEqual(
      client.login(username=normalUserName, password=normalUserPass),
      True)

    tagsDict = deepcopy(self.sampleTagsDict)
    del tagsDict[0]['mediaFile']
    response = client.post(baseUrl + "bulkTagCreate/2/", data={'tags': json.dumps(tagsDict)}, format='json')
    self.assertEqual(response.status_code, 400)


  def test_adminUserOverrideSuccess_json(self):
    """ This Test should succeed because the 'admin' user can set the user
    to another user id.
    """

    client = APIClient()
    self.assertEqual(
      client.login(username=adminUserName, password=adminUserPass),
      True)

    # First ensure our Target TagSet is empty
    self.assertEqual(Tag.objects.filter(tagSet=3).count(), 0)

    tagsDict = deepcopy(self.sampleTagsDict)
    tagsDict[0]['user'] = 3
    response = client.post(baseUrl + "bulkTagCreate/2/", data={'tags': json.dumps(tagsDict)}, format='json')
    self.assertEqual(response.status_code, 200)

    # We should have inserted 1 into this TagSet
    tags = Tag.objects.filter(tagSet=3)
    self.assertEqual(tags.count(),1)
    # Make sure the user is the overridden user, not the admin
    self.assertEqual(tags[0].user.id, 3)


  def test_userOverrideDenied_json(self):
    """ This Test should fail because the 'normal' user can not set the user
    to another user id.
    """

    client = APIClient()
    self.assertEqual(
      client.login(username=normalUserName, password=normalUserPass),
      True)

    # The normal user should not have access to override the user
    tagsDict = deepcopy(self.sampleTagsDict)
    tagsDict[0]['user'] = 3
    response = client.post(baseUrl + "bulkTagCreate/2/", data={'tags': json.dumps(tagsDict)}, format='json')
    self.assertEqual(response.status_code, 403)


  def test_normalUserOverrideSuccess_json(self):
    """ This Test should succeed because the 'normal' user can set the user
    to their own user id.
    """

    client = APIClient()
    self.assertEqual(
      client.login(username=adminUserName, password=adminUserPass),
      True)

    # First ensure our Target TagSet is empty
    self.assertEqual(Tag.objects.filter(tagSet=3).count(), 0)

    tagsDict = deepcopy(self.sampleTagsDict)
    tagsDict[0]['user'] = 2
    response = client.post(baseUrl + "bulkTagCreate/2/", data={'tags': json.dumps(tagsDict)}, format='json')
    self.assertEqual(response.status_code, 200)

    # We should have inserted 1 into this TagSet
    tags = Tag.objects.filter(tagSet=3)
    self.assertEqual(tags.count(),1)
    # Make sure the user is the overridden user, not the admin
    self.assertEqual(tags[0].user.id, 2)


  def test_adminUserOverrideNotFound_json(self):
    """ This Test should fail because the specified user does not exist.
    """

    client = APIClient()
    self.assertEqual(
      client.login(username=adminUserName, password=adminUserPass),
      True)

    # First ensure our Target TagSet is empty
    self.assertEqual(Tag.objects.filter(tagSet=3).count(), 0)

    tagsDict = deepcopy(self.sampleTagsDict)
    tagsDict[0]['user'] = 100
    response = client.post(baseUrl + "bulkTagCreate/2/", data={'tags': json.dumps(tagsDict)}, format='json')
    self.assertEqual(response.status_code, 404)
