from django.conf.urls import patterns, url, include

from rest_framework import routers
from rest_framework.urlpatterns import format_suffix_patterns


###
# Model URLs

from .models.user import UserViewSet
from .models.group import GroupViewSet
from .models.projectTypes import ProjectTypesViewSet
from .models.sensorArray import SensorArrayViewSet
from .models.sensor import SensorViewSet
from .models.mediaTypes import MediaTypesViewSet
from .models.mediaFile import MediaFileViewSet
from .models.mediaFileMetaData import MediaFileMetaDataViewSet
from .models.project import ProjectViewSet
from .models.projectPermissions import ProjectPermissionsViewSet
from .models.tagClass import TagClassViewSet
from .models.tagSet import TagSetViewSet
from .models.tag import TagViewSet
from .models.jobTypes import JobTypesViewSet
from .models.jobStatusTypes import JobStatusTypesViewSet
from .models.jobs import JobsViewSet
from .models.jobParameters import JobParametersViewSet
from .models.randomWindow import RandomWindowViewSet
from .models.userSettings import UserSettingsViewSet
from .models.library import LibraryViewSet
from .models.mediaFileUserMetaData import MediaFileUserMetaDataViewSet
from .models.mediaFileUserMetaDataField import MediaFileUserMetaDataFieldViewSet
from .models.jobResultFile import JobResultFileViewSet
from .models.jobLog import JobLogViewSet
from .models.arloUserGroup import ArloUserGroupViewSet
from .models.arloPermission import ArloPermissionViewSet

router = routers.DefaultRouter()
router.register(r'user', UserViewSet)
router.register(r'group', GroupViewSet)
router.register(r'projectTypes', ProjectTypesViewSet)
router.register(r'sensorArray', SensorArrayViewSet)
router.register(r'sensor', SensorViewSet)
router.register(r'mediaTypes', MediaTypesViewSet)
router.register(r'mediaFile', MediaFileViewSet)
router.register(r'mediaFileMetaData', MediaFileMetaDataViewSet)
router.register(r'project', ProjectViewSet)
router.register(r'projectPermissions', ProjectPermissionsViewSet)
router.register(r'tagClass', TagClassViewSet)
router.register(r'tagSet', TagSetViewSet)
router.register(r'tag', TagViewSet)
router.register(r'jobTypes', JobTypesViewSet)
router.register(r'jobStatusTypes', JobStatusTypesViewSet)
router.register(r'jobs', JobsViewSet)
router.register(r'jobParameters', JobParametersViewSet)
router.register(r'randomWindow', RandomWindowViewSet)
router.register(r'userSettings', UserSettingsViewSet, base_name="userSettings")
router.register(r'library', LibraryViewSet)
router.register(r'mediaFileUserMetaData', MediaFileUserMetaDataViewSet)
router.register(r'mediaFileUserMetaDataField', MediaFileUserMetaDataFieldViewSet)
router.register(r'jobResultFile', JobResultFileViewSet)
router.register(r'jobLog', JobLogViewSet)
router.register(r'arloUserGroup', ArloUserGroupViewSet)
router.register(r'arloPermission', ArloPermissionViewSet)


###
# Function URLs

from .functions.searchMediaFilesByRealTime import searchMediaFilesByRealTime
from .functions.tagNavigationByTime import tagNavigationByTime
from .functions.getTagsInAudioSegment import getTagsInAudioSegment
from .functions.getAudioSpectraData import getAudioSpectraData
from .functions.bulkTagCreate import bulkTagCreate
from .functions.findOrCreateTagClass import findOrCreateTagClass
from .functions.getAudioPitchTraceData import getAudioPitchTraceData
from .functions.generateSpectraImage import generateSpectraImage
from .functions.getQueuedJob import getQueuedJob

from .models.library_userMetaDataFields import library_userMetaDataFields
from .models.library_mediaFiles import library_mediaFiles

functionUrls = [
  url(r'^searchMediaFilesByRealTime/(?P<projectId>\d+)/$', searchMediaFilesByRealTime),
  url(r'^tagNavigationByTime/(?P<projectId>\d+)/$', tagNavigationByTime),
  url(r'^getTagsInAudioSegment/(?P<projectId>\d+)/$', getTagsInAudioSegment),
  url(r'^getAudioSpectraData/(?P<mediaFileId>\d+)/$', getAudioSpectraData),
  url(r'^bulkTagCreate/(?P<projectId>\d+)/$', bulkTagCreate),
  url(r'^findOrCreateTagClass/(?P<projectId>\d+)/$', findOrCreateTagClass),
  url(r'^getAudioPitchTraceData/(?P<mediaFileId>\d+)/$', getAudioPitchTraceData),
  url(r'^generateSpectraImage/(?P<mediaFileId>\d+)/$', generateSpectraImage),
  url(r'^getQueuedJob/$', getQueuedJob),

  url(r'^library/(?P<libraryId>\d+)/userMetaDataFields/$', library_userMetaDataFields),
  url(r'^library/(?P<libraryId>\d+)/mediaFiles/$', library_mediaFiles),
]

functionUrls = format_suffix_patterns(functionUrls)

# Wire up our API using automatic URL routing.
urlpatterns = [
    url(r'^', include(router.urls)),
    url(r'^', include(functionUrls)),
]

