from rest_framework.permissions import BasePermission

class IsAuthenticatedOrOptionsOnly(BasePermission):
    """
    The request is authenticated as a user, or is an OPTIONS request
    """

    def has_permission(self, request, view):
        return (
            request.method == 'OPTIONS' or
            request.user and
            request.user.is_authenticated()
        )
