###
# Search for Tags in relation to their Real-Time value. Look for either the
# 'first' or 'last' Tag or the 'next' or 'previous' from a specified time.
# This is to facilitate Tag navigation in the interface by selecting a Tag to
# jump to.

import datetime

from django import forms
from rest_framework import serializers
from rest_framework import status
from rest_framework.decorators import api_view
from rest_framework.response import Response

from tools.views.userPermissions import ARLO_PERMS
from tools.models import Project, MediaFileMetaData, TagSet, TagClass, Tag


class TagNavigationByTimeSerializer(serializers.Serializer):
  tagId         = serializers.IntegerField()
  realStartTime = serializers.DateTimeField(format="%Y-%m-%d %H:%M:%S")
  duration      = serializers.FloatField()
  mediaFileId   = serializers.IntegerField()


RELATION_CHOICES = (
  ('first', 'first'),
  ('next', 'next'),
  ('previous', 'previous'),
  ('last', 'last'))


class TagNavigationByTimeForm(forms.Form):
  date     = forms.DateTimeField(label='Date/Time (yyyy-mm-dd hh:mm:ss)',required=True)
  tagSets  = forms.ModelMultipleChoiceField(TagSet.objects.none(), required=True)
  tagClass = forms.ModelChoiceField(TagClass.objects.none(), empty_label=None, required=True)
  relation = forms.ChoiceField(choices=RELATION_CHOICES, required=True)


###
# API Call to Search for Tags based on 'Real'-Time relations.
#
# This expects a number of GET parameters:
#   - relation:
#       'first' or 'last' search for the respective Tag, based on it's 'Real' Time
#       'next' or 'previous' search for the respective Tag from a given 'date'
#   - tagSets: List of Database IDs of the TagSets in which to search
#   - tagClass: Database ID of the TagClass in which to search
#   - date:
#       If searching for 'next' or 'previous', the date from which to search
#       Formatted as "yyyy-mm-dd hh:mm:ss"
#       Not required for 'first' or 'last' searches.
# @param request REST Framework Request Object
# @param projectId Database ID of the Project in which we are searching
# @param format REST Framework format param for serializaion options.
# @return REST Framework Response Object with a JSON Dictionary:
#  { 'tagId': , 'realStartTime': , 'duration': , 'mediaFileId': }

@api_view(['GET'])
def tagNavigationByTime(request, projectId, format=None):
  """
  Search for Tags in relation to their Real-Time value. Look for either the
  'first' or 'last' Tag or the 'next' or 'previous' from a specified time.
  """

  user = request.user

  ###
  # Get Project Object

  project = None
  try:
    project = Project.objects.get(id=projectId)
  except:
    return Response("Unauthorized Project", status=status.HTTP_401_UNAUTHORIZED)


  ###
  # Check Permissions

  if not project.userHasPermissions(user, ARLO_PERMS.READ):
    return Response("Unauthorized Project", status=status.HTTP_401_UNAUTHORIZED)
  # TODO should also filter Tags based on permissions for TagSets

  ###
  # Get Parameters

  form = TagNavigationByTimeForm(request.query_params)

  # setup fields
  form.fields['tagSets'] = forms.ModelMultipleChoiceField(project.getTagSets(), required=True)
  form.fields['tagClass'] = forms.ModelChoiceField(project.getTagClasses(), required=True)

  # if 'first' or 'last' then date is not required
  if 'relation' in request.query_params:
    if (request.query_params['relation'] == 'first') or (request.query_params['relation'] == 'last'):
      form.fields['date'].required = False

  # validate form
  if not form.is_valid():
    return Response(
             "Invalid Parameters" + repr(dict((k, map(unicode, v)) for (k,v) in form.errors.iteritems())),
             status=status.HTTP_400_BAD_REQUEST)

  # get data
  data = form.cleaned_data

  searchDate = data['date']
  tagSets = data['tagSets']
  tagClass = data['tagClass']
  relation = data['relation']


  ###
  # Search
  #

  ###
  # Find all media files that contain Tags in the chosen TagClass / TagSet

  mediaFiles = project.getMediaFiles().filter(
                                             tag__tagSet__in=tagSets,
                                             tag__tagClass=tagClass)


  ###
  # Start by building a list of mediaFiles and their times

  # [ (mediaFile, startTime, endTime) ]
  foundMediaFiles = []

  for mediaFile in mediaFiles:
    try:
      fileStartDate = mediaFile.realStartTime
      if fileStartDate:
        durationInSeconds = MediaFileMetaData.objects.get(mediaFile=mediaFile, name='durationInSeconds').value
        fileEndDate = fileStartDate + datetime.timedelta(seconds=float(durationInSeconds))

        # Test if this is a possible file
        possibleFile = False
        if relation == "next":
          if fileEndDate > searchDate:
            possibleFile = True
        elif relation == "previous":
          if fileStartDate < searchDate:
            possibleFile = True
        elif relation == "first":
          possibleFile = True
        elif relation == "last":
          possibleFile = True

        if possibleFile:
          foundMediaFiles.append( (mediaFile, fileStartDate, fileEndDate) )

    except:
      pass


  ###
  # Look at all of the Tags, and see which best matches

  bestTag = None
  bestRealStartTime = None
  bestDuration = None
  bestMediaFile = None

  for (mediaFile, startTime, endTime) in foundMediaFiles:

    # Find all Tags corresponding to the chosen TagClass / TagSet
    tags = Tag.objects.filter(
                    tagClass=tagClass,
                    mediaFile=mediaFile,
                    tagSet__in=tagSets)

    for t in tags:
      tTime = startTime + datetime.timedelta(seconds=t.startTime)

      newBest = False
      if relation == 'next':
        if tTime > searchDate:
          if bestTag is None:
            newBest = True
          elif tTime < bestRealStartTime:
            newBest = True
      if relation == 'previous':
        if tTime < searchDate:
          if bestTag is None:
            newBest = True
          elif tTime > bestRealStartTime:
            newBest = True
      if relation == 'first':
        if bestTag is None:
          newBest = True
        elif tTime < bestRealStartTime:
          newBest = True
      if relation == 'last':
        if bestTag is None:
          newBest = True
        elif tTime > bestRealStartTime:
          newBest = True

      if newBest:
        bestTag = t
        bestRealStartTime = tTime
        bestDuration = t.endTime - t.startTime
        bestMediaFile = mediaFile


  ###
  # Serializer / Response

  if bestTag is None:
    resp_dict = { 'tagId': None,
                'realStartTime': None,
                'duration': None,
                'mediaFileId': None, }
  else:
    resp_dict = { 'tagId': bestTag.id,
                'realStartTime': bestRealStartTime,
                'duration': bestDuration,
                'mediaFileId': bestMediaFile.id, }


  serializer = TagNavigationByTimeSerializer(resp_dict)
  return Response(serializer.data)
