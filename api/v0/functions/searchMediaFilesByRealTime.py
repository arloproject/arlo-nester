import datetime

from rest_framework import serializers, status
from rest_framework.decorators import api_view, permission_classes
from rest_framework.response import Response

from django import forms

from tools.views.userPermissions import ARLO_PERMS
from tools.models import Project, MediaFileMetaData
from api.v0.IsAuthenticatedOrOptionsOnly import IsAuthenticatedOrOptionsOnly


class MediaFilesFoundSerializer(serializers.Serializer):
  id            = serializers.IntegerField()
  alias         = serializers.CharField()
  realStartTime = serializers.DateTimeField()
  sensorId      = serializers.IntegerField()
  sensorArrayId = serializers.IntegerField()


class searchMediaFilesByRealTimeForm(forms.Form):
  startDate = forms.DateTimeField(label='Start Date/Time (yyyy-mm-dd hh:mm:ss)',required=True)
  endDate = forms.DateTimeField(label='End Date/Time (yyyy-mm-dd hh:mm:ss)',required=True)


### Search for MediaFiles that are within a specified Time Period.
#
# This expects GET parameters:
#   - startDate: Formatted as "yyyy-mm-dd hh:mm:ss"
#   - endDate: Formatted as "yyyy-mm-dd hh:mm:ss"
#
# @param request REST Framework Request Object
# @param projectId Database ID of the Project in which we are searching
# @param format REST Framework format param for serializaion options.
# @return REST Framework Response Object, which returns a JSON list of MediaFile dictionaries:
#  [ {'id': , 'alias': , 'realStartTime': , 'sensorId': , 'sensorArrayId': }, ]


@api_view(['GET'])
@permission_classes( (IsAuthenticatedOrOptionsOnly,) )
def searchMediaFilesByRealTime(request, projectId, format=None):
  """
  Search for MediaFiles that are within a specified Time Period.
  """

  user = request.user

  ###
  # Get Start/End Date

  searchParamsForm = searchMediaFilesByRealTimeForm(request.query_params)

  if not searchParamsForm.is_valid():
    return Response(
             "Invalid Parameters" + repr(dict((k, map(unicode, v)) for (k,v) in searchParamsForm.errors.iteritems())),
             status=status.HTTP_400_BAD_REQUEST)

  data = searchParamsForm.cleaned_data
  startDate = data['startDate']
  endDate = data['endDate']

  ###
  # Get Project Object

  project = None
  try:
    project = Project.objects.get(id=projectId)
  except:
    return Response("Unauthorized Project", status=status.HTTP_401_UNAUTHORIZED)

  ###
  # Check Permissions

  if not project.userHasPermissions(user, ARLO_PERMS.READ):
    return Response("Unauthorized Project", status=status.HTTP_401_UNAUTHORIZED)

  ###
  # Get All MediaFiles in the Project

  mediaFiles = project.getMediaFiles()

  ###
  # Search through Files between the Start and End Time
  # if no date set, not part of this set

  foundMediaFiles = []

  for mediaFile in mediaFiles:
    try:
      fileStartDate = mediaFile.realStartTime
      if fileStartDate:
        durationInSeconds = MediaFileMetaData.objects.get(mediaFile=mediaFile, name='durationInSeconds').value
        fileEndDate = fileStartDate + datetime.timedelta(seconds=float(durationInSeconds))

        if (fileStartDate < endDate) and (fileEndDate > startDate):

          if mediaFile.sensor:
            sensorId = str(mediaFile.sensor.id)
            sensorArrayId = str(mediaFile.sensor.sensorArray.id)
          else:
            sensorId = None
            sensorArrayId = None

          foundMediaFiles.append( dict({
                'id': mediaFile.id,
                'alias': mediaFile.alias,
                'realStartTime': fileStartDate,
                'sensorId': sensorId,
                'sensorArrayId': sensorArrayId,
                }))

    except:
      pass

  ###
  # Serializer / Response

  serializer = MediaFilesFoundSerializer(foundMediaFiles, many=True)
  return Response(serializer.data)
