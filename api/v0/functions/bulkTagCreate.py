import json
import six

from rest_framework import serializers, permissions, status
from rest_framework.response import Response
from rest_framework.parsers import JSONParser, FormParser, MultiPartParser
from rest_framework.decorators import api_view, parser_classes
from rest_framework.exceptions import NotFound, PermissionDenied
from django.db import transaction
from django.contrib.auth.models import User
from django.core.exceptions import ObjectDoesNotExist

from tools.models import Tag, TagSet, TagClass, MediaFile, Project
from tools.views.userPermissions import ARLO_PERMS


class BulkTagCreatePerms(permissions.BasePermission):
  def has_object_permission(self, request, view, obj):
    if not request.user.is_authenticated():
      return False
    return obj.userHasPermissions(request.user, ARLO_PERMS.WRITE) and obj.mediaFile.userHasPermissions(request.user, ARLO_PERMS.READ)


# used for List/Create
class BulkTagCreateSerializer(serializers.Serializer):

# custom validation can be applied per-field as per http://www.django-rest-framework.org/api-guide/fields/ 'validators'

  tagSet = serializers.IntegerField()
  mediaFile = serializers.IntegerField()
  startTime = serializers.FloatField()
  endTime = serializers.FloatField()
  minFrequency = serializers.FloatField()
  maxFrequency = serializers.FloatField()
  tagClass = serializers.IntegerField()
  parentTag = serializers.IntegerField(allow_null=True, default=None)
  randomlyChosen = serializers.BooleanField(default=False)
  machineTagged = serializers.BooleanField(default=False)
  userTagged = serializers.BooleanField(default=False)
  strength = serializers.FloatField()
  user = serializers.IntegerField(default=None)  # Only System users may explicitly set the user


  def create(self, validated_data):
    # Override the normal save since we are splitting this up into two models

    # We'll need to split manually validate all data and build the objects.

    request = self.context['request']

    user = request.user

    project = self.context['project']

    # TagSet
    try:
      tagSet = TagSet.objects.get(pk=validated_data['tagSet'])
    except:
      raise NotFound("TagSet " + str(validated_data['tagSet']) + " Not Found")

    # MediaFile
    try:
      mediaFile = MediaFile.objects.get(pk=validated_data['mediaFile'])
    except:
      raise NotFound("MediaFile " + str(validated_data['mediaFile']) + " Not Found")

    if not mediaFile.userHasPermissions(user, ARLO_PERMS.READ):
      raise PermissionDenied("Permission Denied to MediaFile " + str(mediaFile.id))

# TODO

#    mediaFileDuration = float(getMetaDataEntry(mediaFile, 'durationInSeconds'))

#    logging.info("MediaFile Duration: " + str(mediaFileDuration))

# ensure MediaFile is part of Project

    # startTime / endTime
    startTime = float(validated_data['startTime'])
    if startTime < 0:
      raise serializers.ValidationError("startTime (" + str(startTime) + ") < 0")

    endTime = float(validated_data['endTime'])
    if endTime < 0:
      raise serializers.ValidationError("endTime (" + str(endTime) + ") < 0")
#    if endTime > mediaFileDuration:
#      raise serializers.ValidationError("endTime (" + str(endTime) + ") > mediaFileDuration (" + str(mediaFileDuration) + ")")

    if startTime > endTime:
      raise serializers.ValidationError("startTime (" + str(startTime) + ") > endTime (" + str(endTime) + ")")

    # minFrequency / maxFrequency
    minFrequency = float(validated_data['minFrequency'])
    if minFrequency < 0:
      raise serializers.ValidationError("minFrequency (" + str(minFrequency) + ") < 0")

    maxFrequency = float(validated_data['maxFrequency'])
    if maxFrequency < 0:
      raise serializers.ValidationError("maxFrequency (" + str(maxFrequency) + ") < 0")

    if minFrequency > maxFrequency:
      raise serializers.ValidationError("minFrequency (" + str(minFrequency) + ") > maxFrequency (" + str(maxFrequency) + ")")

    # tagClass
    try:
      tagClass = TagClass.objects.get(pk=validated_data['tagClass'])
    except:
      raise NotFound("TagClass " + str(validated_data['tagClass']) + " Not Found")

    if tagClass.project != project:
      raise serializers.ValidationError("tagClass (" + str(tagClass.id) + ") not in Project")

    # parentTag
    parentTagId = validated_data['parentTag']
    if parentTagId is None:
      parentTag = None
    else:
      try:
        parentTag = Tag.objects.get(pk=parentTagId)
      except:
        raise NotFound("parentTag " + str(parentTagId) + " Not Found")

    # misc
    randomlyChosen = bool(validated_data['randomlyChosen'])
    machineTagged = bool(validated_data['machineTagged'])
    userTagged = bool(validated_data['userTagged'])
    strength = float(validated_data['strength'])

    ###
    # User Override

    if validated_data['user'] is not None:
        # Are we overriding the user
        if validated_data['user'] != request.user.id:
          # Only System user's can override
          if not request.user.is_staff:
              raise PermissionDenied("Only System Admin users may override the Tag User")
          try:
            user = User.objects.get(pk=validated_data['user'])
          except ObjectDoesNotExist:
            raise NotFound("User " + str(validated_data['user']) + " Not Found")


    # create and save

    tag = Tag(
        user=user,
        tagSet=tagSet,
        mediaFile=mediaFile,
        startTime=startTime,
        endTime=endTime,
        minFrequency=minFrequency,
        maxFrequency=maxFrequency,
        tagClass=tagClass,
        parentTag=parentTag,
        randomlyChosen=randomlyChosen,
        machineTagged=machineTagged,
        userTagged=userTagged,
        strength=strength)
    tag.save()

    return None

@api_view(['POST'])
@parser_classes((JSONParser, FormParser, MultiPartParser))
@transaction.atomic
def bulkTagCreate(request, projectId, format=None):
  """
  This function allows the creation of multiple Tag objects
  in one request. This function expects a List of Dictionaries describing
  the Tags to create.

  [
    {
        "tagSet":
        "mediaFile":
        "startTime":
        "endTime":
        "minFrequency":
        "maxFrequency":
        "tagClass":
        "parentTag":
        "randomlyChosen":
        "machineTagged":
        "userTagged":
        "strength":
        "user": (Optional, only System Admin users may explicitly set the user)
    },
  ]
  """

  user = request.user

  ###
  # Get Project Object

  project = None
  try:
    project = Project.objects.get(id=projectId)
  except:
    return Response("Unauthorized Project", status=status.HTTP_401_UNAUTHORIZED)

  ###
  # Check Permissions
  # TODO We should check the individual TagSets, not the project
  if not project.userHasPermissions(user, ARLO_PERMS.WRITE):
    return Response("Unauthorized Project", status=status.HTTP_401_UNAUTHORIZED)

  ###
  # De-serialize data

  data = request.data

  # Workaround for clients (such as our built-in Test client) that require
  # using a dictionary, so we can't pass the list directly through the POST.
  # A 'tags' key in the data instead specifies a JSON string of the data which
  # will be parsed.
  if 'tags' in data:
    if isinstance(data['tags'], six.string_types):
      data = json.loads(data['tags'])
    else:
      data = data['tags']

  serializer = BulkTagCreateSerializer(data=data, many=True, context={'request': request, 'project': project})

  if not serializer.is_valid():
    return Response("Invalid Data: " + str(serializer.errors), status=status.HTTP_400_BAD_REQUEST)

  serializer.save()

  return Response("OK")


