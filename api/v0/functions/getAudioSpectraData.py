import logging

from django import forms

from rest_framework import status
from rest_framework.decorators import api_view
from rest_framework.response import Response

from tools.models import MediaFile
from tools.views.userPermissions import ARLO_PERMS
from tools.Meandre import AdaptInterface
from tools.views.viewMetaData import getMetaDataEntry


class getAudioSpectraDataForm(forms.Form):
  startTime       = forms.FloatField(label='File Start Time (seconds)',required=True)
  endTime         = forms.FloatField(label='File End Time (seconds)',required=True)
  numTimeFramesPerSecond = forms.IntegerField(label='Number of Time Frames per Second', required=True)
  numFrequencyBands = forms.IntegerField(label='Number of Frequency Bands', required=True)
  dampingFactor = forms.FloatField(label='Damping Factor', required=True)
  minFrequency = forms.FloatField(label='Minimum Frequency', required=True)
  maxFrequency = forms.FloatField(label='Maximum Frequency', required=True)


### Compute and return the Audio Spectra Data from a MediaFile.
#
# Required GET parameters:
#   - startTime: in seconds, relative to the beginning of the file.
#   - endTime: in seconds, relative to the beginning of the file.
#   - numTimeFramesPerSecond
#   - numFrequencyBands
#   - dampingFactor
#   - minFrequency
#   - maxFrequency
#
# Response Object:
#
#  'spectraData': {
#      'params': {
#        'mediaFileId': mediaFileId,
#        'startTime': startTime,
#        'endTime': endTime,
#        'numTimeFramesPerSecond': numTimeFramesPerSecond,
#        'numFrequencyBands': numFrequencyBands,
#        'dampingRatio': dampingRatio,
#        'minFrequency': minFrequency,
#        'maxFrequency': maxFrequency
#      },
#      'freqBands': [
#                     freq0,
#                     freq1,
#                     ...
#                   ],
#      'frames': {
#                  time0: [
#                           sample0,
#                           sample1,
#                           ...
#                         ],
#                  time1: [
#                           sample0,
#                           sample1,
#                           ...
#                         ],
#                  ...
#                }
#  }
#
# @param request REST Framework Request Object
# @param mediaFileId Database ID of the MediaFile for which we are computing spectra.
# @param format REST Framework format param for serialization options.
# @return REST Framework Response Object, which returns the JSON 'spectraData' object from the Adapt callMe() API.

@api_view(['GET'])
def getAudioSpectraData(request, mediaFileId, format=None):
  """
  Compute and return the Audio Spectra Data from a MediaFile.
  """

  user = request.user

  ###
  # Get MediaFile Object

  try:
    mediaFile = MediaFile.objects.get(id=mediaFileId)
  except:
    return Response("Unauthorized MediaFile", status=status.HTTP_401_UNAUTHORIZED)

  ###
  # Check Permissions

  if not mediaFile.userHasPermissions(user, ARLO_PERMS.READ):
    return Response("Unauthorized MediaFile", status=status.HTTP_401_UNAUTHORIZED)

  ###
  # Get Parameters

  paramsForm = getAudioSpectraDataForm(request.query_params)

  if not paramsForm.is_valid():
    return Response(
             "Invalid Parameters" + repr(dict((k, map(unicode, v)) for (k,v) in paramsForm.errors.iteritems())),
             status=status.HTTP_400_BAD_REQUEST)

  data = paramsForm.cleaned_data

  startTime       = data['startTime']
  endTime = data['endTime']
  numTimeFramesPerSecond = data['numTimeFramesPerSecond']
  numFrequencyBands = data['numFrequencyBands']
  dampingFactor = data['dampingFactor']
  minFrequency = data['minFrequency']
  maxFrequency = data['maxFrequency']


  ###
  # Validate Parameters

  fileDurationInSeconds = float(getMetaDataEntry(mediaFile, 'durationInSeconds'))

  if startTime < 0:
    return Response("Invalid Parameter: startTime < 0", status=status.HTTP_400_BAD_REQUEST)

  if startTime > fileDurationInSeconds:
    return Response(
               "Invalid Parameter: startTime (" + str(startTime) +
                 ") > file duration (" + str(fileDurationInSeconds) + ")",
               status=status.HTTP_400_BAD_REQUEST)

  if endTime < 0:
    return Response("Invalid Parameter: endTime < 0", status=status.HTTP_400_BAD_REQUEST)

  if endTime > fileDurationInSeconds:
    endTime = fileDurationInSeconds


  ###
  # Get Spectra Data

  adaptInterface = AdaptInterface()

  audioSpectraData = adaptInterface.GetAudioSpectraData(
                                      mediaFile,
                                      startTime,
                                      endTime,
                                      numTimeFramesPerSecond,
                                      numFrequencyBands,
                                      dampingFactor,
                                      minFrequency,
                                      maxFrequency)

  if not audioSpectraData:
    logging.error("getAudioSpectraData() - Unknown Error Computing Spectra: " + repr(data))
    return Response("Unknown Error Computing Spectra", status=status.HTTP_500_INTERNAL_SERVER_ERROR)


  ###
  # Response

  # Not using a Serializer for the response, just passing the raw dictionary back

  return Response(audioSpectraData)

