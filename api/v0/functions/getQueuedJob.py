import logging

from django import forms
from django.db import transaction
from rest_framework import status
from rest_framework.decorators import api_view, permission_classes, parser_classes
from rest_framework.response import Response
from rest_framework.parsers import JSONParser, FormParser, MultiPartParser

from tools.views.userPermissions import ARLO_PERMS
from tools.views.biasManipulation import GetJobParameters
from tools.models import Jobs, JobTypes, JobStatusTypes
from api.v0.IsAuthenticatedOrOptionsOnly import IsAuthenticatedOrOptionsOnly
from ..models.jobs import JobsSerializer


class GetQueuedJobForm(forms.Form):
    jobTypes = forms.ModelMultipleChoiceField(JobTypes.objects.all(), required=True)


### Get a Queued Job from the Database and assign it to the requesting client.
#
# This expects POST parameters:
#   - jobTypes: List of JobTypes which the client can handle.
#   - jobPluginNames (optional): List of JobPluginNames which the client
#         can process, required if using the PluginType.
#
# @param request REST Framework Request Object
# @param format REST Framework format param for serializaion options.
# @return REST Framework Response Object, return a JobsSerializer if a Job
#         Job is found, 204 NO CONTENT if none is available.

@api_view(['POST'])
@parser_classes((JSONParser, FormParser, MultiPartParser))
@permission_classes( (IsAuthenticatedOrOptionsOnly,) )
def getQueuedJob(request, format=None):
    """
    Get a Queued Job from the Database and assign it to the requesting client.
    """

    user = request.user

    ###
    # Get Request Data

    form = GetQueuedJobForm(request.data)
    if not form.is_valid():
        return Response(
               "Invalid Parameters" + repr(dict((k, map(unicode, v)) for (k,v) in form.errors.iteritems())),
               status=status.HTTP_400_BAD_REQUEST)
    data = form.cleaned_data

    jobTypes = data['jobTypes']
    jobPluginNames = request.data.get('jobPluginNames')  # bypass the form data
    if not isinstance(jobPluginNames, list):
        jobPluginNames = [jobPluginNames]


    ###
    # Get a list of potential Jobs, check permissions, and return the first
    # that matches.

    pluginJobType = JobTypes.objects.get(name="JobPlugin")
    queuedStatus = JobStatusTypes.objects.get(name="Queued")

    # WARNING: Lock the Jobs table for write so that concurrent requests
    # don't try to assign the same Job at once. Note we need to make sure that
    # regardless of what happens, this lock is released.

    with transaction.atomic():
        for job in Jobs.objects.select_for_update().filter(type__in=jobTypes, status=queuedStatus).order_by('priority'):
            # Check Permissions
            if not job.userHasPermissions(user, ARLO_PERMS.LAUNCH_JOB):
                continue

            # if this is a plugin Job, check if the plugin name
            # was included in the request.
            if job.type == pluginJobType:

                if not jobPluginNames:
                    # Must submit a plugin name to consider it
                    continue
                pluginName = GetJobParameters(job).get('PluginName', [])
                if not isinstance(pluginName, list) or len(pluginName) != 1:
                    logging.warning("Plugin Job %s has no/invalid PluginName parameter - skipping", job.id)
                    continue
                pluginName = pluginName[0]
                if pluginName not in jobPluginNames:
                    continue

            # all good, assign the Job and return it
            job.status = JobStatusTypes.objects.get(name="Assigned")
            job.save()
            serializer = JobsSerializer(job, many=False)
            return Response(serializer.data)

    return Response(None, status=status.HTTP_204_NO_CONTENT)
