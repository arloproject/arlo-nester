import logging

from django import forms
from django.core.exceptions import ObjectDoesNotExist

from rest_framework import status
from rest_framework.decorators import api_view
from rest_framework.response import Response

from tools.models import MediaFile
from tools.views.userPermissions import ARLO_PERMS
from tools.Meandre import AdaptInterface
from tools.views.viewMetaData import getMetaDataEntry


class GetAudioPitchTraceDataForm(forms.Form):

  pitchTraceType  = forms.ChoiceField(choices=(("MAX_ENERGY", "MAX_ENERGY"), ("FUNDAMENTAL", "FUNDAMENTAL")), required=True)
  startTime       = forms.FloatField(label='File Start Time (seconds)',required=True)
  endTime         = forms.FloatField(label='File End Time (seconds)',required=True)
  numTimeFramesPerSecond = forms.IntegerField(label='Number of Time Frames per Second', required=True)
  numFrequencyBands = forms.IntegerField(label='Number of Frequency Bands', required=True)
  dampingFactor = forms.FloatField(label='Damping Factor', required=True)
  spectraMinFrequency = forms.FloatField(label='Spectra Minimum Frequency', required=True)
  spectraMaxFrequency = forms.FloatField(label='Spectra Maximum Frequency', required=True)
  # Only for FUNDAMENTAL
  numSamplePoints     = forms.IntegerField(label='Number of Sample Points', required=False)
  minCorrelation      = forms.FloatField(label='Minimum Correlation', required=False)
  entropyThreshold    = forms.FloatField(label='Entropy Threshold', required=False)
  pitchTraceStartFreq = forms.FloatField(label='Pitch Trace Start Frequency', required=False)
  pitchTraceEndFreq   = forms.FloatField(label='Pitch Trace End Frequency', required=False)
  minEnergyThreshold  = forms.FloatField(label='Minimum Energy Threshold', required=False)
  tolerance           = forms.FloatField(label='Tolerance', required=False)
  inverseFreqWeight   = forms.FloatField(label='Inverse Frequency Weight', required=False)
  maxPathLengthPerTransition = forms.FloatField(label='Maximum Path Length Per Transition', required=False)
  windowSize          = forms.IntegerField(label='Window Size', required=False)
  extendRangeFactor   = forms.FloatField(label='Extend Range Factor', required=False)



  def __init__(self, data=None, *args, **kwargs):
    super(GetAudioPitchTraceDataForm, self).__init__(data, *args, **kwargs)

    # Override required fields for "FUNDAMENTAL" selection
    if data and data.get('pitchTraceType') == 'FUNDAMENTAL':
      self.fields['numSamplePoints'].required = True
      self.fields['minCorrelation'].required = True
      self.fields['entropyThreshold'].required = True
      self.fields['pitchTraceStartFreq'].required = True
      self.fields['pitchTraceEndFreq'].required = True
      self.fields['minEnergyThreshold'].required = True
      self.fields['tolerance'].required = True
      self.fields['inverseFreqWeight'].required = True
      self.fields['maxPathLengthPerTransition'].required = True
      self.fields['windowSize'].required = True
      self.fields['extendRangeFactor'].required = True



### Compute and return the Audio PitchTrace Data from a MediaFile
#
# Required GET parameters:
#   - pitchTraceType: One of "MAX_ENERGY" or "FUNDAMENTAL"
#   - startTime: in seconds, relative to the beginning of the file.
#   - endTime: in seconds, relative to the beginning of the file.
#   - numTimeFramesPerSecond
#   - numFrequencyBands
#   - dampingFactor
#   - spectraMinFrequency
#   - spectraMaxFrequency
#
# Response Object:
#
#  'pitchTraceData': {
#      'params': {
#        'pitchTraceType': pitchTraceType,
#        'mediaFileId': mediaFileId,
#        'startTime': startTime,
#        'endTime': endTime,
#        'numTimeFramesPerSecond': numTimeFramesPerSecond,
#        'numFrequencyBands': numFrequencyBands,
#        'dampingRatio': dampingRatio,
#        'minFrequency': minFrequency,
#        'maxFrequency': maxFrequency
#      },
#      'freqBands': [
#                     freq0,
#                     freq1,
#                     ...
#                   ],
#      'frames': {
#                  time0: {
#                           "frequency": freq,
#                           "energy": energy,
#                           ...
#                         },
#                  time1: {
#                           "frequency": freq,
#                           "energy": energy,
#                           ...
#                         },
#                  ...
#                }
#  }
#
# @param request REST Framework Request Object
# @param mediaFileId Database ID of the MediaFile for which we are computing PitchTrace data.
# @param format REST Framework format param for serialization options.
# @return REST Framework Response Object, which returns the JSON 'spectraData' object from the Adapt callMe() API.

@api_view(['GET'])
def getAudioPitchTraceData(request, mediaFileId, format=None):
  """
  Compute and return the Audio PitchTrace Data from a MediaFile
  """

  user = request.user

  ###
  # Get MediaFile Object

  try:
    mediaFile = MediaFile.objects.get(id=mediaFileId)
  except ObjectDoesNotExist:
    # Technically a 404, but return the same 401 to prevent any kind of information leakage
    return Response("Unauthorized MediaFile", status=status.HTTP_401_UNAUTHORIZED)

  ###
  # Check Permissions

  if not mediaFile.userHasPermissions(user, ARLO_PERMS.READ):
    return Response("Unauthorized MediaFile", status=status.HTTP_401_UNAUTHORIZED)

  ###
  # Get Parameters

  paramsForm = GetAudioPitchTraceDataForm(request.query_params)

  if not paramsForm.is_valid():
    return Response(
             "Invalid Parameters: " + repr(dict((k, map(unicode, v)) for (k,v) in paramsForm.errors.iteritems())),
             status=status.HTTP_400_BAD_REQUEST)

  data = paramsForm.cleaned_data

  pitchTraceType         = data['pitchTraceType']
  startTime              = data['startTime']
  endTime                = data['endTime']
  numTimeFramesPerSecond = data['numTimeFramesPerSecond']
  numFrequencyBands      = data['numFrequencyBands']
  dampingFactor          = data['dampingFactor']
  spectraMinFrequency    = data['spectraMinFrequency']
  spectraMaxFrequency    = data['spectraMaxFrequency']

  # FUNDAMENTAL Settings
  numSamplePoints = data.get('numSamplePoints', None)
  minCorrelation = data.get('minCorrelation', None)
  entropyThreshold = data.get('entropyThreshold', None)
  pitchTraceStartFreq = data.get('pitchTraceStartFreq', None)
  pitchTraceEndFreq = data.get('pitchTraceEndFreq', None)
  minEnergyThreshold = data.get('minEnergyThreshold', None)
  tolerance = data.get('tolerance', None)
  inverseFreqWeight = data.get('inverseFreqWeight', None)
  maxPathLengthPerTransition = data.get('maxPathLengthPerTransition', None)
  windowSize = data.get('windowSize', None)
  extendRangeFactor = data.get('extendRangeFactor', None)


  ###
  # Validate Parameters

  fileDurationInSeconds = float(getMetaDataEntry(mediaFile, 'durationInSeconds'))

  if startTime < 0:
    return Response("Invalid Parameter: startTime < 0", status=status.HTTP_400_BAD_REQUEST)

  if startTime > fileDurationInSeconds:
    return Response(
               "Invalid Parameter: startTime (" + str(startTime) +
                 ") > file duration (" + str(fileDurationInSeconds) + ")",
               status=status.HTTP_400_BAD_REQUEST)

  if endTime < 0:
    return Response("Invalid Parameter: endTime < 0", status=status.HTTP_400_BAD_REQUEST)

  if endTime > fileDurationInSeconds:
    endTime = fileDurationInSeconds


  ###
  # Get PitchTrace Data

  adaptInterface = AdaptInterface()

  pitchTraceData = adaptInterface.GetAudioPitchTraceData(
                                      mediaFile,
                                      pitchTraceType,
                                      startTime,
                                      endTime,
                                      numTimeFramesPerSecond,
                                      numFrequencyBands,
                                      dampingFactor,
                                      spectraMinFrequency,
                                      spectraMaxFrequency,
                                      numSamplePoints,
                                      minCorrelation,
                                      entropyThreshold,
                                      pitchTraceStartFreq,
                                      pitchTraceEndFreq,
                                      minEnergyThreshold,
                                      tolerance,
                                      inverseFreqWeight,
                                      maxPathLengthPerTransition,
                                      windowSize,
                                      extendRangeFactor)


  if not pitchTraceData:
    logging.error("getAudioPitchTraceData() - Unknown Error Computing PitchTrace: " + repr(data))
    return Response("Unknown Error Computing Spectra", status=status.HTTP_500_INTERNAL_SERVER_ERROR)


  ###
  # Response

  # Not using a Serializer for the response, just passing the raw dictionary back

  return Response(pitchTraceData)

