from django import forms
from rest_framework import status
from rest_framework.decorators import api_view
from rest_framework.response import Response

from tools.views.userPermissions import ARLO_PERMS
from tools.models import Project, TagClass

from api.v0.models.tagClass import TagClassDetailSerializer



class findOrCreateTagClassForm(forms.Form):
  tagClassName = forms.CharField(label='TagClass Name', required=True)


### Search for an existing or create a new TagClass within a Project.
#
# Required POST parameters:
#   - tagClassName: Name of the TagClass to find or create.
#
# @param request REST Framework Request Object
# @param projectId Database ID of the Project in which we are searching
# @param format REST Framework format param for serializaion options.
# @return REST Framework Response Object, which returns a JSON list of


@api_view(['POST'])
def findOrCreateTagClass(request, projectId, format=None):
  """
  Search for an existing or create a new TagClass within a Project.
  """

  user = request.user

  ###
  # Get Project Object

  project = None
  try:
    project = Project.objects.get(id=projectId)
  except:
    return Response("Unauthorized Project", status=status.HTTP_401_UNAUTHORIZED)

  ###
  # Need a minimum of Read permissions

  if not project.userHasPermissions(user, ARLO_PERMS.READ):
    return Response("Unauthorized Project", status=status.HTTP_401_UNAUTHORIZED)

  ###
  # Get Parameters

  form = findOrCreateTagClassForm(request.data)

  if not form.is_valid():
    return Response(
             "Invalid Parameters" + repr(dict((k, map(unicode, v)) for (k,v) in form.errors.iteritems())),
             status=status.HTTP_400_BAD_REQUEST)

  data = form.cleaned_data

  tagClassName = data['tagClassName']


  try:
    ###
    # Look for existing TagClass

    tagClass = TagClass.objects.get(project=project, className=tagClassName)
    serializer = TagClassDetailSerializer(tagClass)
    return Response(serializer.data, status=status.HTTP_200_OK)

  except:
    ###
    # Create a new TagClass

    # Need Write Permissions
    if not project.userHasPermissions(user, ARLO_PERMS.WRITE):
      return Response("Unauthorized Project", status=status.HTTP_401_UNAUTHORIZED)

    tagClass = TagClass(user=user, project=project, className=tagClassName, displayName=tagClassName)
    tagClass.save()
    serializer = TagClassDetailSerializer(tagClass)
    return Response(serializer.data, status=status.HTTP_201_CREATED)


