from django import forms

from rest_framework import serializers
from rest_framework import status
from rest_framework.decorators import api_view
from rest_framework.response import Response

from tools.views.userPermissions import ARLO_PERMS
from tools.models import Project, MediaFile, Tag, TagSet
from tools.views.tag import tagInfo


class TagsFoundSerializer(serializers.Serializer):
  id             = serializers.IntegerField()
  userId         = serializers.IntegerField()
  username       = serializers.CharField()
  # tag class data
  className      = serializers.CharField()
  displayName    = serializers.CharField()
  projectId      = serializers.IntegerField()
  # tag example data
  tagSetId       = serializers.IntegerField()
  mediaFileId    = serializers.IntegerField()
  mediaFileAlias = serializers.CharField()
  startTime      = serializers.FloatField()
  endTime        = serializers.FloatField()
  # tagInfo returns this as a String already
  #realTime       = serializers.DateTimeField(format="%Y-%m-%d %H:%M:%S")
  realTime       = serializers.CharField()
  minFrequency   = serializers.FloatField()
  maxFrequency   = serializers.FloatField()
  # back to Tag info
  userTagged     = serializers.BooleanField()
  machineTagged  = serializers.BooleanField()
  strength       = serializers.FloatField()


class getTagsInAudioSegmentForm(forms.Form):
  mediaFile       = forms.ModelChoiceField(MediaFile.objects.none(), label='MediaFile ID', required=True)
  startTime       = forms.FloatField(label='File Start Time (seconds)', required=False)
  endTime         = forms.FloatField(label='File End Time (seconds)', required=False)
  restrictTagSets = forms.ModelMultipleChoiceField(TagSet.objects.none(), label='TagSets', required=False)


### Search for Tags from a specific MediaFile within a specified Time Period.
#
# Required GET parameters:
#   - mediaFile: The file in which to search
#   - startTime: Start Time of search within the file (seconds from beginning)
#   - endTime: End Time of search within the file (seconds from beginning)
# Optional Parameters:
#   - restrictTagSets: Limits results to Tags within the specified TagSets
#
# @param request REST Framework Request Object
# @param projectId Database ID of the Project in which we are searching
# @param format REST Framework format param for serializaion options.
# @return REST Framework Response Object, which returns a JSON list of


@api_view(['GET'])
def getTagsInAudioSegment(request, projectId, format=None):
  """
  Search for Tags from a specific MediaFile within a specified Time Period.
  """

  user = request.user

  ###
  # Get Project Object

  project = None
  try:
    project = Project.objects.get(id=projectId)
  except:
    return Response("Unauthorized Project", status=status.HTTP_401_UNAUTHORIZED)


  ###
  # Get Parameters

  searchParamsForm = getTagsInAudioSegmentForm(request.query_params)

  searchParamsForm.fields['mediaFile'] = forms.ModelChoiceField(project.getMediaFiles(), label='MediaFile ID', required=True)
  searchParamsForm.fields['restrictTagSets'] = forms.ModelMultipleChoiceField(project.getTagSets(), label='TagSets', required=False)

  if not searchParamsForm.is_valid():
    return Response(
             "Invalid Parameters" + repr(dict((k, map(unicode, v)) for (k,v) in searchParamsForm.errors.iteritems())),
             status=status.HTTP_400_BAD_REQUEST)

  data = searchParamsForm.cleaned_data

  startTime       = data['startTime']
  endTime         = data['endTime']
  mediaFile       = data['mediaFile']
  restrictTagSets = data['restrictTagSets']

  ###
  # Check Permissions

  if not project.userHasPermissions(user, ARLO_PERMS.READ):
    return Response("Unauthorized Project", status=status.HTTP_401_UNAUTHORIZED)

  # TODO Should also restrict permissions based on TagSets

  ###
  # Get Tags

  results_list_of_dicts = []

  tags = Tag.objects.filter(mediaFile=mediaFile, tagSet__project=project)

  if startTime:
    tags = tags.exclude(endTime__lt=startTime)

  if endTime:
    tags = tags.exclude(startTime__gt=endTime)

  if len(restrictTagSets) > 0:
    tags = tags.filter(tagSet__in = restrictTagSets)

  ###
  # Build Results

  for tag in tags:
    results_list_of_dicts.append(tagInfo(tag))


  ###
  # Serializer / Response

  serializer = TagsFoundSerializer(results_list_of_dicts, many=True)
  return Response(serializer.data)
