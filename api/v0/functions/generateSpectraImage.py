from django import forms
from django.core.exceptions import ObjectDoesNotExist
from rest_framework import status
from rest_framework.decorators import api_view
from rest_framework.response import Response

from tools.models import MediaFile, UserSettings
from tools.views.userPermissions import ARLO_PERMS
from tools.views.viewMetaData import getMetaDataEntry
from tools.views.spectra import SpectraImageSettings, SpectraImage


class SpectraImageForm(forms.Form):

  startTime = forms.FloatField(label='File Start Time (seconds)',required=True)
  endTime = forms.FloatField(label='File End Time (seconds)',required=True)
  minFrequency = forms.FloatField(label='Spectra Minimum Frequency', required=True)
  maxFrequency = forms.FloatField(label='Spectra Maximum Frequency', required=True)
  numFrequencyBands = forms.IntegerField(label='Number of Frequency Bands', required=True)
  numTimeFramesPerSecond = forms.IntegerField(label='Number of Time Frames per Second', required=True)
  dampingFactor = forms.FloatField(label='Damping Factor', required=True)
  gain = forms.FloatField(label='Gain', required=False)


### Generate an Audio Spectra Image from a MediaFile
#
# Required POST parameters:
#   startTime
#   endTime
#   gain
#   minFrequency
#   maxFrequency
#   numFrequencyBands
#   numTimeFramesPerSecond
#   dampingFactor
#
# @param request REST Framework Request Object
# @param mediaFileId Database ID of the MediaFile for which we are generating Spectra.
# @param format REST Framework format param for serialization options.
# @return REST Framework Response Object, which returns A JSON dictionary of 
# {imageUrl, audioSegmentUrl, params, imageHeight, imageWidth }
# Example:
# {
#     "imageFilePath": "files/user-files/...7f5.jpg",
#     "params": {
#         "minFrequency": 20.0,
#         "numTimeFramesPerSecond": 20,
#         "dampingFactor": 0.02,
#         "numFrequencyBands": 32,
#         "showFrequencyScale": true,
#         "showTimeScale": true,
#         "maxFrequency": 20000.0,
#         "catalogSpectraNumFrequencyBands": null,
#         "mediaFile": 179070,
#         "topBorderHeight": 30,
#         "simpleNormalization": true,
#         "gain": 1,
#         "startTime": 0.0,
#         "timeScaleHeight": 22,
#         "endTime": 5.0,
#         "borderWidth": 50
#     },
#     "imageHeight": 84,
#     "audioSegmentFilePath": "files/user-files/...7f5.segment.wav",
#     "imageWidth": 200.0
# }

@api_view(['POST'])
def generateSpectraImage(request, mediaFileId, format=None):
  """
  Generate an Audio Spectra Image from a MediaFile. Note that
  not all of the parameters are user adjustable, only
    - startTime
    - endTime
    - minFrequency
    - maxFrequency
    - numFrequencyBands
    - numTimeFramesPerSecond
    - dampingFactor
    - gain (optional, default 1.0)
  """

  user = request.user

  ###
  # Get MediaFile Object

  try:
    mediaFile = MediaFile.objects.get(id=mediaFileId)
  except ObjectDoesNotExist:
    # Technically a 404, but return the same 401 to prevent any kind of information leakage
    return Response("Unauthorized MediaFile", status=status.HTTP_401_UNAUTHORIZED)

  ###
  # Check Permissions

  if not mediaFile.userHasPermissions(user, ARLO_PERMS.READ):
    return Response("Unauthorized MediaFile", status=status.HTTP_401_UNAUTHORIZED)

  ###
  # Get Parameters

  paramsForm = SpectraImageForm(request.data)

  if not paramsForm.is_valid():
    return Response(
             "Invalid Parameters: " + repr(dict((k, map(unicode, v)) for (k,v) in paramsForm.errors.iteritems())),
             status=status.HTTP_400_BAD_REQUEST)

  data = paramsForm.cleaned_data

  startTime              = data['startTime']
  endTime                = data['endTime']
  minFrequency           = data['minFrequency']
  maxFrequency           = data['maxFrequency']
  numFrequencyBands      = data['numFrequencyBands']
  numTimeFramesPerSecond = data['numTimeFramesPerSecond']
  dampingFactor          = data['dampingFactor']
  gain                   = data['gain']
  if gain is None:
    gain = 1

  ###
  # Validate Parameters

  fileDurationInSeconds = float(getMetaDataEntry(mediaFile, 'durationInSeconds'))

  if startTime < 0:
    return Response("Invalid Parameter: startTime < 0", status=status.HTTP_400_BAD_REQUEST)

  if startTime > fileDurationInSeconds:
    return Response(
               "Invalid Parameter: startTime (" + str(startTime) +
                 ") > file duration (" + str(fileDurationInSeconds) + ")",
               status=status.HTTP_400_BAD_REQUEST)

  if endTime < 0:
    return Response("Invalid Parameter: endTime < 0", status=status.HTTP_400_BAD_REQUEST)

  if endTime > fileDurationInSeconds:
    endTime = fileDurationInSeconds


  ###
  # Get UserSettings
  userSettings = None
  try:
    userSettings = UserSettings.objects.get(user=user, name='default')
  except ObjectDoesNotExist:
    # No default settings for the user - someday we'll maybe
    # just completely remove the UserSettings dependency
    return Response("No Default UserSettings found for User", status=status.HTTP_400_BAD_REQUEST)


  ###
  # Build Image Settings

  spectraImageSettings = SpectraImageSettings()

  spectraImageSettings.userSettings = userSettings

  spectraImageSettings.audioFile = mediaFile
  spectraImageSettings.startTime = startTime
  spectraImageSettings.endTime = endTime
  spectraImageSettings.gain = gain
  spectraImageSettings.minFrequency = minFrequency
  spectraImageSettings.maxFrequency = maxFrequency
  spectraImageSettings.numFrequencyBands = numFrequencyBands
  spectraImageSettings.numFramesPerSecond = numTimeFramesPerSecond
  spectraImageSettings.dampingFactor = dampingFactor

  spectraImageSettings.simpleNormalization = True
  spectraImageSettings.showTimeScale = True
  spectraImageSettings.showFrequencyScale = True
  spectraImageSettings.borderWidth = 50
  spectraImageSettings.topBorderHeight = 30
  spectraImageSettings.timeScaleHeight = 22
  spectraImageSettings.catalogSpectraNumFrequencyBands = None


  ###
  # Get Spectra Image

  spectraImage = SpectraImage.generateSpectraImage(spectraImageSettings)

  if not spectraImage:
    return Response("Unknown Error Computing Spectra", status=status.HTTP_500_INTERNAL_SERVER_ERROR)


  ###
  # Build params dict

  params = {
            'mediaFile': spectraImage.spectraImageSettings.audioFile.id,
            'startTime': spectraImage.spectraImageSettings.startTime,
            'endTime': spectraImage.spectraImageSettings.endTime,
            'gain': spectraImage.spectraImageSettings.gain,
            'minFrequency': spectraImage.spectraImageSettings.minFrequency,
            'maxFrequency': spectraImage.spectraImageSettings.maxFrequency,
            'numFrequencyBands': spectraImage.spectraImageSettings.numFrequencyBands,
            'numTimeFramesPerSecond': spectraImage.spectraImageSettings.numFramesPerSecond,
            'dampingFactor': spectraImage.spectraImageSettings.dampingFactor,
            'simpleNormalization': spectraImage.spectraImageSettings.simpleNormalization,
            'showTimeScale': spectraImage.spectraImageSettings.showTimeScale,
            'showFrequencyScale': spectraImage.spectraImageSettings.showFrequencyScale,
            'borderWidth': spectraImage.spectraImageSettings.borderWidth,
            'topBorderHeight': spectraImage.spectraImageSettings.topBorderHeight,
            'timeScaleHeight': spectraImage.spectraImageSettings.timeScaleHeight,
            'catalogSpectraNumFrequencyBands': spectraImage.spectraImageSettings.catalogSpectraNumFrequencyBands,
            }


  ###
  # Response

  # Not using a Serializer for the response, just passing the raw dictionary back

  response_dict = {
                   'params': params,
                   'audioSegmentUrl': spectraImage.audioSegmentUrl,
                   'imageUrl': spectraImage.imageUrl,
                   'imageWidth': spectraImage.imageWidth,
                   'imageHeight': spectraImage.imageHeight,
                   }

  return Response(response_dict)

