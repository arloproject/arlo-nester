from rest_framework import serializers, viewsets

from tools.models import ProjectTypes
from api.v0.IsAuthenticatedOrOptionsOnly import IsAuthenticatedOrOptionsOnly


class ProjectTypesSerializer(serializers.ModelSerializer):
  class Meta:
    model = ProjectTypes
    fields = '__all__'

class ProjectTypesViewSet(viewsets.ReadOnlyModelViewSet):
  permission_classes = (IsAuthenticatedOrOptionsOnly,)
  queryset = ProjectTypes.objects.all()
  serializer_class = ProjectTypesSerializer

