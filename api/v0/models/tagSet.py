from django.http import Http404
from django.shortcuts import get_object_or_404
from rest_framework import serializers, viewsets, permissions, exceptions
from rest_framework.decorators import detail_route
from rest_framework.response import Response

from tools.models import TagSet, Tag
from tools.views.userPermissions import ARLO_PERMS, getUserTagSets
from api.v0.IsAuthenticatedOrOptionsOnly import IsAuthenticatedOrOptionsOnly
from .tag import TagDetailSerializer

class TagSetListPerms(permissions.BasePermission):
  def has_permission(self, request, view):
    if not request.user.is_authenticated():
      return False
    return True

class TagSetDetailPerms(permissions.BasePermission):
  def has_object_permission(self, request, view, obj):
    if not request.user.is_authenticated():
      return False
    if request.method in permissions.SAFE_METHODS:
      return obj.userHasPermissions(request.user, ARLO_PERMS.READ)
    return obj.userHasPermissions(request.user, ARLO_PERMS.WRITE)


# used for List/Create
class TagSetListSerializer(serializers.ModelSerializer):
  class Meta:
    model = TagSet
    fields = (
      'id',
      'user',
      'name',
      'project',
      'creationDate',
      'hidden',
      )
    read_only_fields = ('id', 'user', 'creationDate')

# used for detail/update - don't allow modifying Project
class TagSetDetailSerializer(serializers.ModelSerializer):
  class Meta:
    model = TagSet
    fields = (
      'id',
      'user',
      'name',
      'project',
      'creationDate',
      'hidden',
      )
    read_only_fields = ('id', 'user', 'project', 'creationDate')


class TagSetViewSet(viewsets.ModelViewSet):
  queryset = TagSet.objects.all()
  serializer_class = TagSetDetailSerializer
  permission_classes = (IsAuthenticatedOrOptionsOnly, TagSetDetailPerms,)

  def list(self, request, format=None):  # pylint: disable=redefined-builtin
    self.serializer_class = TagSetListSerializer
    self.permission_classes = (TagSetListPerms,)
    self.check_permissions(request)
    self.queryset = getUserTagSets(request.user)
    return super(TagSetViewSet, self).list(request, format)

  def create(self, request, format=None):  # pylint: disable=redefined-builtin
    self.serializer_class = TagSetListSerializer
    self.permission_classes = (TagSetDetailPerms,)
    self.check_permissions(request)
    return super(TagSetViewSet, self).create(request, format)

  def perform_create(self, serializer):
    serializer.is_valid(raise_exception=True)
    # passed validation, so we're assuming that the Job exists:
    project = serializer.validated_data['project']
    if not project.userHasPermissions(self.request.user, ARLO_PERMS.WRITE):
      raise exceptions.PermissionDenied()
    # Include the user attribute directly, rather than from request data.
    serializer.save(user=self.request.user)

  def perform_update(self, serializer):
    serializer.is_valid(raise_exception=True)
    if not serializer.instance:
      raise Http404("Instance Not Found")
    self.check_object_permissions(self.request, serializer.instance)
    serializer.save()

  @detail_route(methods=['get'], serializer_class=TagDetailSerializer)
  def tags(self, request, pk=None):
      """
      Returns a list of Tag objects that are within the TagSet
      """
      tagSet = get_object_or_404(self.queryset, pk=pk)
      self.check_object_permissions(request, tagSet)

      tags = Tag.objects.filter(tagSet=tagSet)

      page = self.paginate_queryset(tags)
      if page is not None:
          serializer = self.get_serializer(page, many=True)
          return self.get_paginated_response(serializer.data)

      serializer = self.get_serializer(tags, many=True)
      return Response(serializer.data)
