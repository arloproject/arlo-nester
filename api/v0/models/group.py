from rest_framework import serializers, viewsets, permissions
from django.contrib.auth.models import Group

from api.v0.IsAuthenticatedOrOptionsOnly import IsAuthenticatedOrOptionsOnly


class GroupPerms(permissions.BasePermission):
  def has_permission(self, request, view):
    if request.method == 'OPTIONS':
      return True
    if not request.user.is_authenticated():
      return False
    if not (request.method in permissions.SAFE_METHODS):
      return False
    return request.user.is_staff

  def has_object_permission(self, request, view, obj):
    if not request.user.is_authenticated():
      return False
    if not (request.method in permissions.SAFE_METHODS):
      return False
    return request.user.is_staff

#class GroupSerializer(serializers.HyperlinkedModelSerializer):
class GroupSerializer(serializers.ModelSerializer):
    class Meta:
        model = Group
        fields = ('id', 'name')

class GroupViewSet(viewsets.ReadOnlyModelViewSet):
  permission_classes = (IsAuthenticatedOrOptionsOnly, GroupPerms,)
  queryset = Group.objects.all()
  serializer_class = GroupSerializer

