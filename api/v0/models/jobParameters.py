from rest_framework import serializers, viewsets, permissions, exceptions
from django.http import Http404

from tools.views.userPermissions import getUserJobParameters, ARLO_PERMS
from tools.models import JobParameters
from api.v0.IsAuthenticatedOrOptionsOnly import IsAuthenticatedOrOptionsOnly

###
# Permissions

class JobParametersListPerms(permissions.BasePermission):
  def has_permission(self, request, view):
    if not request.user.is_authenticated():
      return False
    return True

class JobParametersDetailPerms(permissions.BasePermission):
  def has_object_permission(self, request, view, obj):
    if not request.user.is_authenticated():
      return False
    if request.user.is_staff:
      return True
    if request.method in permissions.SAFE_METHODS:
      return obj.userHasPermissions(request.user, ARLO_PERMS.READ)
    else:
      return obj.userHasPermissions(request.user, ARLO_PERMS.WRITE)


###
# Serializer

# used for List/Create
class JobParametersListSerializer(serializers.ModelSerializer):
  class Meta:
    model = JobParameters
    fields = (
      'id',
      'job',
      'name',
      'value',
      )
    read_only_fields = ('id', )

# used for detail / update - basically, don't allow any updates
class JobParametersDetailSerializer(serializers.ModelSerializer):
  class Meta:
    model = JobParameters
    fields = (
      'id',
      'job',
      'name',
      'value',
      )
    read_only_fields = ('id', 'job')


###
# ViewSet

class JobParametersViewSet(viewsets.ModelViewSet):
  queryset = JobParameters.objects.all()
  serializer_class = JobParametersDetailSerializer
  permission_classes = (IsAuthenticatedOrOptionsOnly, JobParametersDetailPerms,)

  def list(self, request, format=None):
    self.queryset = getUserJobParameters(request.user)
    self.serializer_class = JobParametersListSerializer
    self.permission_classes = (JobParametersListPerms,)
    self.check_permissions(request)
    return super(JobParametersViewSet, self).list(request, format)

  def create(self, request, format=None):
    self.serializer_class = JobParametersListSerializer
    self.permission_classes = (JobParametersDetailPerms,)
    self.check_permissions(request)
    return super(JobParametersViewSet, self).create(request, format)

  # pre_save moved to perform_create/perform_update in Django Rest Framework 3
  #def pre_save(self, obj):
  #  self.check_object_permissions(self.request, obj)

  def perform_create(self, serializer):
    serializer.is_valid(raise_exception=True)
    # passed validation, so we're assuming that the Job exists:
    job = serializer.validated_data['job']
    if not job.userHasPermissions(self.request.user, ARLO_PERMS.WRITE):
      raise exceptions.PermissionDenied()
    serializer.save()

  def perform_update(self, serializer):
    serializer.is_valid(raise_exception=True)
    if not serializer.instance:
      raise Http404("Instance Not Found")
    self.check_object_permissions(self.request, serializer.instance)
    serializer.save()

