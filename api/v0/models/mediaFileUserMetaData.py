from rest_framework import serializers, viewsets, permissions, exceptions
from rest_framework.response import Response
from django.shortcuts import get_object_or_404
from django.http import Http404

from tools.models import MediaFileUserMetaData

from tools.views.userPermissions import ARLO_PERMS, getUserMediaFileUserMetaData
from api.v0.IsAuthenticatedOrOptionsOnly import IsAuthenticatedOrOptionsOnly


###
# Permissions

class MediaFileUserMetaDataListPerms(permissions.BasePermission):
  def has_permission(self, request, view):
    if not request.user.is_authenticated():
      return False
    return True

class MediaFileUserMetaDataDetailPerms(permissions.BasePermission):
  def has_object_permission(self, request, view, obj):
    if not request.user.is_authenticated():
      return False
    if request.user.is_staff:
      return True
    if request.method in permissions.SAFE_METHODS:
      return obj.userHasPermissions(request.user, ARLO_PERMS.READ)
    else:
      return obj.userHasPermissions(request.user, ARLO_PERMS.WRITE)


###
# Serializer

# used for List/Create
class MediaFileUserMetaDataListSerializer(serializers.ModelSerializer):
  metaDataFieldName = serializers.CharField(source='metaDataField.name', read_only=True)

  class Meta:
    model = MediaFileUserMetaData
    fields = (
      'id',
      'mediaFile',
      'metaDataField',
      'value',
      'metaDataFieldName',
      )
    read_only_fields = (
      'id',
      'metaDataFieldName',
      )

# many extra read-only fields for update
class MediaFileUserMetaDataDetailSerializer(serializers.ModelSerializer):
  metaDataFieldName = serializers.CharField(source='metaDataField.name', read_only=True)

  class Meta:
    model = MediaFileUserMetaData
    fields = (
      'id',
      'mediaFile',
      'metaDataField',
      'value',
      'metaDataFieldName',
      )
    read_only_fields = (
      'id',
      'mediaFile',
      'metaDataField',
      'metaDataFieldName',
      )

###
# ViewSet

class MediaFileUserMetaDataViewSet(viewsets.ModelViewSet):
  queryset = MediaFileUserMetaData.objects.all()
  serializer_class = MediaFileUserMetaDataDetailSerializer
  permission_classes = (IsAuthenticatedOrOptionsOnly, MediaFileUserMetaDataDetailPerms,)

  def list(self, request, format=None):
    self.queryset = getUserMediaFileUserMetaData(request.user)
    self.serializer_class = MediaFileUserMetaDataListSerializer
    self.permission_classes = (MediaFileUserMetaDataListPerms,)
    self.check_permissions(request)
    return super(MediaFileUserMetaDataViewSet, self).list(request, format)

  def create(self, request, format=None):
    self.serializer_class = MediaFileUserMetaDataListSerializer
    self.permission_classes = (MediaFileUserMetaDataDetailPerms,)
    self.check_permissions(request)
    return super(MediaFileUserMetaDataViewSet, self).create(request, format)

  def retrieve(self, request, pk=None, format=None):
    project = get_object_or_404(self.queryset, pk=pk)
    self.check_object_permissions(request, project)
    serializer = MediaFileUserMetaDataDetailSerializer(project)
    return Response(serializer.data)

  # pre_save moved to perform_create/perform_update in Django Rest Framework 3
  #def pre_save(self, obj):
#    if self.request.method == 'POST':
#      obj.user = self.request.user
  #  self.check_object_permissions(self.request, obj)

  def perform_create(self, serializer):
    serializer.is_valid(raise_exception=True)
    # passed validation, so we're assuming that the Job exists:
    mediaFile = serializer.validated_data['mediaFile']
    if not mediaFile.userHasPermissions(self.request.user, ARLO_PERMS.WRITE):
      if not self.request.user.is_staff:
        raise exceptions.PermissionDenied()
    serializer.save()

  def perform_update(self, serializer):
    serializer.is_valid(raise_exception=True)
    if not serializer.instance:
      raise Http404("Instance Not Found")
    self.check_object_permissions(self.request, serializer.instance)
    serializer.save()


