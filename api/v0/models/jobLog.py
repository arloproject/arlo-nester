from rest_framework import serializers, viewsets, permissions, exceptions
from django.http import Http404

from tools.models import JobLog
from tools.views.userPermissions import ARLO_PERMS, getUserJobLogs
from api.v0.IsAuthenticatedOrOptionsOnly import IsAuthenticatedOrOptionsOnly


###
# Permissions

class JobLogDetailPerms(permissions.BasePermission):
  def has_object_permission(self, request, view, obj):
    if not request.user.is_authenticated():
      return False
    if request.user.is_staff:
      return True
    if request.method in permissions.SAFE_METHODS:
      return obj.userHasPermissions(request.user, ARLO_PERMS.READ)
    else:
      # also ensure user has access to the mediaFile
      return obj.userHasPermissions(request.user, ARLO_PERMS.WRITE)


###
# Serializer

# used for List/Create
class JobLogSerializer(serializers.ModelSerializer):
  class Meta:
    model = JobLog
    fields = (
      'id',
      'job',
      'messageDate',
      'message',
      )
    read_only_fields = ('id', )


###
# ViewSet

class JobLogViewSet(viewsets.ModelViewSet):
  queryset = JobLog.objects.all()
  serializer_class = JobLogSerializer
  permission_classes = (IsAuthenticatedOrOptionsOnly, JobLogDetailPerms,)

  def list(self, request, format=None):
    self.queryset = getUserJobLogs(request.user)
    self.check_permissions(request)
    return super(JobLogViewSet, self).list(request, format)

  def create(self, request, format=None):
    self.check_permissions(request)
    return super(JobLogViewSet, self).create(request, format)

  # pre_save moved to perform_create/perform_update in Django Rest Framework 3
  #def pre_save(self, obj):
  #  if self.request.method == 'POST':
  #    obj.user = self.request.user
  #  self.check_object_permissions(self.request, obj)

  def perform_create(self, serializer):
    serializer.is_valid(raise_exception=True)
    # passed validation, so we're assuming that the Job exists:
    job = serializer.validated_data['job']
    if not job.userHasPermissions(self.request.user, ARLO_PERMS.WRITE):
      raise exceptions.PermissionDenied()
    serializer.save()

  def perform_update(self, serializer):
    serializer.is_valid(raise_exception=True)
    if not serializer.instance:
      raise Http404("Instance Not Found")
    self.check_object_permissions(self.request, serializer.instance)
    serializer.save()
