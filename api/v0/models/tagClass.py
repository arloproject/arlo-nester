from rest_framework import serializers, viewsets, permissions, exceptions
from django.http import Http404

from tools.models import TagClass
from tools.views.userPermissions import ARLO_PERMS, getUserTagClasses
from api.v0.IsAuthenticatedOrOptionsOnly import IsAuthenticatedOrOptionsOnly


class TagClassListPerms(permissions.BasePermission):
  def has_permission(self, request, view):
    if not request.user.is_authenticated():
      return False
    return True

class TagClassDetailPerms(permissions.BasePermission):
  def has_object_permission(self, request, view, obj):
    if not request.user.is_authenticated():
      return False
    if request.method in permissions.SAFE_METHODS:
      return obj.userHasPermissions(request.user, ARLO_PERMS.READ)
    else:
      return obj.userHasPermissions(request.user, ARLO_PERMS.WRITE)


# used for List/Create
class TagClassListSerializer(serializers.ModelSerializer):
  class Meta:
    model = TagClass
    fields = (
      'id',
      'user',
      'project',
      'className',
      'displayName',
      )
    read_only_fields = ('id', 'user')

# used for detail/update - don't allow modifying Project
class TagClassDetailSerializer(serializers.ModelSerializer):
  class Meta:
    model = TagClass
    fields = (
      'id',
      'user',
      'project',
      'className',
      'displayName',
      )
    read_only_fields = ('id', 'user', 'project')



class TagClassViewSet(viewsets.ModelViewSet):
  queryset = TagClass.objects.all()
  serializer_class = TagClassDetailSerializer
  permission_classes = (IsAuthenticatedOrOptionsOnly, TagClassDetailPerms,)

  def list(self, request, format=None):
    self.queryset = getUserTagClasses(request.user)
    self.serializer_class = TagClassListSerializer
    self.permission_classes = (TagClassListPerms,)
    self.check_permissions(request)
    return super(TagClassViewSet, self).list(request, format)

  def create(self, request, format=None):
    self.serializer_class = TagClassListSerializer
    self.permission_classes = (TagClassDetailPerms,)
    self.check_permissions(request)
    return super(TagClassViewSet, self).create(request, format)

#  def retrieve(self, request, pk=None, format=None):
#    obj = get_object_or_404(self.queryset, pk=pk)
#    self.check_object_permissions(request, obj)
#    serializer = TagClassSerializer(obj)
#    return Response(serializer.data)

  # pre_save moved to perform_create/perform_update in Django Rest Framework 3
  #def pre_save(self, obj):
  #  if self.request.method == 'POST':
  #    obj.user = self.request.user
  #  self.check_object_permissions(self.request, obj)

  def perform_create(self, serializer):
    serializer.is_valid(raise_exception=True)
    # passed validation, so we're assuming that the Job exists:
    project = serializer.validated_data['project']
    if not project.userHasPermissions(self.request.user, ARLO_PERMS.WRITE):
      raise exceptions.PermissionDenied()
    # Include the user attribute directly, rather than from request data.
    serializer.save(user=self.request.user)

  def perform_update(self, serializer):
    serializer.is_valid(raise_exception=True)
    if not serializer.instance:
      raise Http404("Instance Not Found")
    self.check_object_permissions(self.request, serializer.instance)
    serializer.save()


