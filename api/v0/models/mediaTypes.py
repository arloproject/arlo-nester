from rest_framework import serializers, viewsets

from tools.models import MediaTypes
from api.v0.IsAuthenticatedOrOptionsOnly import IsAuthenticatedOrOptionsOnly


class MediaTypesSerializer(serializers.ModelSerializer):
  class Meta:
    model = MediaTypes
    fields = '__all__'

class MediaTypesViewSet(viewsets.ReadOnlyModelViewSet):
  permission_classes = (IsAuthenticatedOrOptionsOnly, )
  queryset = MediaTypes.objects.all()
  serializer_class = MediaTypesSerializer

