from rest_framework import serializers, viewsets

from tools.models import JobTypes
from api.v0.IsAuthenticatedOrOptionsOnly import IsAuthenticatedOrOptionsOnly


class JobTypesSerializer(serializers.ModelSerializer):
  class Meta:
    model = JobTypes
    fields = '__all__'

class JobTypesViewSet(viewsets.ReadOnlyModelViewSet):
  permission_classes = (IsAuthenticatedOrOptionsOnly,)
  queryset = JobTypes.objects.all()
  serializer_class = JobTypesSerializer

