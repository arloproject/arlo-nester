from rest_framework import serializers, viewsets, permissions
from rest_framework.response import Response
from django.shortcuts import get_object_or_404

from tools.models import JobResultFile

from tools.views.userPermissions import ARLO_PERMS, getUserJobResultFiles
from api.v0.IsAuthenticatedOrOptionsOnly import IsAuthenticatedOrOptionsOnly

class JobResultFileListPerms(permissions.BasePermission):
  def has_permission(self, request, view):
    if not request.user.is_authenticated():
      return False
    return True

class JobResultFileDetailPerms(permissions.BasePermission):
  def has_object_permission(self, request, view, obj):
    if not request.user.is_authenticated():
      return False
    if request.user.is_staff:
      return True
    if request.method in permissions.SAFE_METHODS:
      return obj.userHasPermissions(request.user, ARLO_PERMS.READ)
    else:
      return obj.userHasPermissions(request.user, ARLO_PERMS.WRITE)


class JobResultFileListSerializer(serializers.ModelSerializer):
  class Meta:
    model = JobResultFile
    fields = (
      'id',
      'job',
      'creationDate',
      'resultFile',
      'notes',
      )
    read_only_fields = ('id', 'creationDate' )

class JobResultFileDetailSerializer(serializers.ModelSerializer):
  class Meta:
    model = JobResultFile
    fields = (
      'id',
      'job',
      'creationDate',
      'resultFile',
      'notes',
      )
    read_only_fields = ('id', 'job', 'creationDate', 'resultFile' )

class JobResultFileViewSet(viewsets.ModelViewSet):
  queryset = JobResultFile.objects.all()
  serializer_class = JobResultFileDetailSerializer
  permission_classes = (IsAuthenticatedOrOptionsOnly, JobResultFileDetailPerms,)

  def list(self, request, format=None):
    self.queryset = getUserJobResultFiles(request.user)
    self.serializer_class = JobResultFileListSerializer
    self.permission_classes = (JobResultFileListPerms,)
    self.check_permissions(request)
    return super(JobResultFileViewSet, self).list(request, format)

  def create(self, request, format=None):
    self.serializer_class = JobResultFileListSerializer
    self.permission_classes = (JobResultFileListPerms,)
    self.check_permissions(request)
    return super(JobResultFileViewSet, self).create(request, format)

  def retrieve(self, request, pk=None, format=None):
    obj = get_object_or_404(self.queryset, pk=pk)
    self.check_object_permissions(request, obj)
    serializer = JobResultFileDetailSerializer(obj)
    return Response(serializer.data)

  def perform_create(self, serializer):
    # Include the user attribute directly, rather than from request data.
    instance = serializer.save()

