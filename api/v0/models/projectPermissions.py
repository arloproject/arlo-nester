from rest_framework import serializers, viewsets, permissions, exceptions
from rest_framework.response import Response
from django.shortcuts import get_object_or_404
from django.http import Http404

from tools.models import ProjectPermissions
from tools.views.userPermissions import ARLO_PERMS, getUserAdminProjects
from api.v0.IsAuthenticatedOrOptionsOnly import IsAuthenticatedOrOptionsOnly


class ProjectPermissionsListPerms(permissions.BasePermission):
  def has_permission(self, request, view):
    if not request.user.is_authenticated():
      return False
    return True

class ProjectPermissionsDetailPerms(permissions.BasePermission):
  def has_object_permission(self, request, view, obj):
    if not request.user.is_authenticated():
      return False
    return obj.project.userHasPermissions(request.user, ARLO_PERMS.ADMIN)


class ProjectPermissionsSerializer(serializers.ModelSerializer):
  class Meta:
    model = ProjectPermissions
    fields = (
      'id',
      'user',
      'project',
      'canRead',
      'canWrite',
      )
    read_only_fields = ('id', )


class ProjectPermissionsViewSet(viewsets.ModelViewSet):
  queryset = ProjectPermissions.objects.all()
  serializer_class = ProjectPermissionsSerializer
  permission_classes = (IsAuthenticatedOrOptionsOnly, ProjectPermissionsDetailPerms,)

  def list(self, request, format=None):
    self.permission_classes = (ProjectPermissionsListPerms,)
    self.check_permissions(request)
    self.queryset = ProjectPermissions.objects.filter(project__in=getUserAdminProjects(request.user))
    return super(ProjectPermissionsViewSet, self).list(request, format)

  def create(self, request, format=None):
    self.permission_classes = (ProjectPermissionsListPerms,)
    self.check_permissions(request)
    return super(ProjectPermissionsViewSet, self).create(request, format)

  def retrieve(self, request, pk=None, format=None):
    obj = get_object_or_404(self.queryset, pk=pk)
    self.check_object_permissions(request, obj)
    serializer = ProjectPermissionsSerializer(obj)
    return Response(serializer.data)

  def perform_create(self, serializer):
    serializer.is_valid(raise_exception=True)
    project = serializer.validated_data['project']
    if not project.userHasPermissions(self.request.user, ARLO_PERMS.ADMIN):
      raise exceptions.PermissionDenied()
    serializer.save()

  def perform_update(self, serializer):
    serializer.is_valid(raise_exception=True)
    if not serializer.instance:
      raise Http404("Instance Not Found")
    self.check_object_permissions(self.request, serializer.instance)
    serializer.save()

