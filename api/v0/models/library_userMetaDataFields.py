from rest_framework import status
from rest_framework.decorators import api_view
from rest_framework.response import Response
from rest_framework.settings import api_settings

from tools.views.userMetaData import getLibraryUserMetaDataFields
from tools.views.userPermissions import ARLO_PERMS
from tools.models import Library
from .mediaFileUserMetaDataField import MediaFileUserMetaDataFieldListSerializer


### Get a list of UserMetaDataFields in a Library.
#
# @param request REST Framework Request Object
# @param libraryId Database ID of a Library.
# @param format REST Framework format param for serializaion options.
# @return REST Framework Response Object, which returns a JSON list of


#         [{id: <entry id>, fieldName: <name>, value: <value>}]

@api_view(['GET'])
def library_userMetaDataFields(request, libraryId, format=None):
  """Get a list of UserMetaDataFields in a Library"""

  ###
  # Get MediaFile Object

  library = None
  try:
    library = Library.objects.get(id=libraryId)
  except:
    return Response("Not Found", status=status.HTTP_404_NOT_FOUND)

  ###
  # Check Permissions

  if not library.userHasPermissions(request.user, ARLO_PERMS.READ):
    return Response("Unauthorized Library", status=status.HTTP_401_UNAUTHORIZED)

  ###
  # Get UserMetaData Entries

  fields = getLibraryUserMetaDataFields(library)

  ###
  # Apply Pagination

  pagination_class = api_settings.DEFAULT_PAGINATION_CLASS
  paginator = pagination_class()
  if paginator is None:
    fields = None
  else:
    fields = paginator.paginate_queryset(fields, request)

  ###
  # Serializer / Response

  serializer = MediaFileUserMetaDataFieldListSerializer(fields, many=True)
  return paginator.get_paginated_response(serializer.data)

