from rest_framework import serializers, viewsets, permissions
from rest_framework.response import Response
from django.shortcuts import get_object_or_404

from tools.models import Library

from tools.views.userPermissions import ARLO_PERMS
from api.v0.IsAuthenticatedOrOptionsOnly import IsAuthenticatedOrOptionsOnly

class LibraryListPerms(permissions.BasePermission):
  def has_permission(self, request, view):
    if not request.user.is_authenticated():
      return False
    return True

class LibraryDetailPerms(permissions.BasePermission):
  def has_object_permission(self, request, view, obj):
    if not request.user.is_authenticated():
      return False
    if request.user.is_staff:
      return True
    if request.method in permissions.SAFE_METHODS:
      return obj.userHasPermissions(request.user, ARLO_PERMS.READ)
    else:
      return obj.userHasPermissions(request.user, ARLO_PERMS.WRITE)


class LibrarySerializer(serializers.ModelSerializer):
  class Meta:
    model = Library
    fields = (
      'id',
      'user',
      'name',
      'notes',
      'default_permission_read',
      'default_permission_write',
      'default_permission_launch_job',
      'default_permission_admin',
      )
    read_only_fields = ('id', 'user')

class LibraryViewSet(viewsets.ModelViewSet):
  queryset = Library.objects.all()
  serializer_class = LibrarySerializer
  permission_classes = (IsAuthenticatedOrOptionsOnly, LibraryDetailPerms,)

  def list(self, request, format=None):
    self.queryset = Library.objects.getUserLibraries(request.user)
    self.permission_classes = (LibraryListPerms,)
    self.check_permissions(request)
    return super(LibraryViewSet, self).list(request, format)

  def create(self, request, format=None):
    self.permission_classes = (LibraryListPerms,)
    self.check_permissions(request)
    return super(LibraryViewSet, self).create(request, format)

  def retrieve(self, request, pk=None, format=None):
    obj = get_object_or_404(self.queryset, pk=pk)
    self.check_object_permissions(request, obj)
    serializer = LibrarySerializer(obj)
    return Response(serializer.data)

  # pre_save moved to perform_create/perform_update in Django Rest Framework 3
  #def pre_save(self, obj):
  #  if self.request.method == 'POST':
  #    obj.user = self.request.user

  def perform_create(self, serializer):
    # Include the user attribute directly, rather than from request data.
    instance = serializer.save(user=self.request.user)

