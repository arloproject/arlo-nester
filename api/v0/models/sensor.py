from rest_framework import serializers, viewsets, permissions
from rest_framework.response import Response
from django.shortcuts import get_object_or_404

from tools.models import Sensor
from api.v0.IsAuthenticatedOrOptionsOnly import IsAuthenticatedOrOptionsOnly
from tools.views.userPermissions import ARLO_PERMS


class SensorListPerms(permissions.BasePermission):
  def has_permission(self, request, view):
    if not request.user.is_authenticated():
      return False
    return True

class SensorDetailPerms(permissions.BasePermission):
  def has_object_permission(self, request, view, obj):
    if not request.user.is_authenticated():
      return False
    if request.user.is_staff:
      return True
    if request.method in permissions.SAFE_METHODS:
      return obj.userHasPermissions(request.user, ARLO_PERMS.READ)
    else:
      return obj.userHasPermissions(request.user, ARLO_PERMS.WRITE)

class SensorSerializer(serializers.ModelSerializer):
  class Meta:
    model = Sensor
    fields = (
        'id',
        'sensorArray',
        'name',
        'x',
        'y',
        'z',
        'layoutRow',
        'layoutColumn'
        )

    read_only_fields = ('id', )

class SensorViewSet(viewsets.ModelViewSet):
  queryset = Sensor.objects.all()
  serializer_class = SensorSerializer
  permission_classes = (IsAuthenticatedOrOptionsOnly, SensorDetailPerms, )

  def list(self, request, format=None):
    self.queryset = Sensor.objects.getUserSensors(request.user)
    self.permission_classes = (SensorListPerms,)
    self.check_permissions(request)
    return super(SensorViewSet, self).list(request, format)

  def create(self, request, format=None):
    self.permission_classes = (SensorListPerms,)
    self.check_permissions(request)
    return super(SensorViewSet, self).create(request, format)

  def retrieve(self, request, pk=None, format=None):
    obj = get_object_or_404(self.queryset, pk=pk)
    self.check_object_permissions(request, obj)
    serializer = SensorSerializer(obj)
    return Response(serializer.data)

