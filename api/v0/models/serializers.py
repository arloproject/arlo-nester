from rest_framework import serializers

from tools.models import MediaFile, MediaFileMetaData, Project


class MediaFileMetaDataSerializer(serializers.ModelSerializer):
  class Meta:
    model = MediaFileMetaData
    fields = (
      'id',
      'mediaFile',
      'name',
      'value',
      'userEditable',
      )
    read_only_fields = (
      'id',
      )


class MediaFileSerializer(serializers.ModelSerializer):
  # note this must match the auto-generated related_name from the model
  mediafilemetadata_set = MediaFileMetaDataSerializer(many=True, read_only=True)
  url = serializers.CharField(source='get_absolute_url', read_only=True)

  class Meta:
    model = MediaFile
    fields = (
      'id',
      'file',
      'user',
      'type',
      'active',
      'alias',
      'uploadDate',
      'sensor',
      'library',
      'realStartTime',
      'mediafilemetadata_set',
      'url',
      'md5',
      )
    read_only_fields = (
      'id',
      'file',
      'user',
      'uploadDate',
      'library',
      'md5',
      )


class ProjectSerializer(serializers.ModelSerializer):
  class Meta:
    model = Project
    fields = (
      'id',
      'user',
      'type',
      'name',
      'mediaFiles',
      'creationDate',
      'notes',
      'default_permission_read',
      'default_permission_write',
      'default_permission_launch_job',
      'default_permission_admin'
      )
    read_only_fields = ('id', 'user', 'mediaFiles')
