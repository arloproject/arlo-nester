from rest_framework import serializers, viewsets, permissions, exceptions
from rest_framework.response import Response
from django.shortcuts import get_object_or_404
from django.http import Http404

from tools.models import Jobs
from tools.views.userPermissions import getUserJobs, ARLO_PERMS
from api.v0.IsAuthenticatedOrOptionsOnly import IsAuthenticatedOrOptionsOnly
from .jobParameters import JobParametersListSerializer

class JobsListPerms(permissions.BasePermission):
  def has_permission(self, request, view):
    if not request.user.is_authenticated():
      return False
    return True

class JobsDetailPerms(permissions.BasePermission):
  def has_object_permission(self, request, view, obj):
    if not request.user.is_authenticated():
      return False
    if request.user.is_staff:
      return True
    if request.method in permissions.SAFE_METHODS:
      return obj.userHasPermissions(request.user, ARLO_PERMS.READ)
    else:
      return obj.userHasPermissions(request.user, ARLO_PERMS.WRITE)

class JobsSerializer(serializers.ModelSerializer):
  jobparameters_set = JobParametersListSerializer(many=True, read_only=True)

  class Meta:
    model = Jobs
    fields = (
      'id',
      'name',
      'user',
      'project',
      'project',
      'type',
      'creationDate',
      'numToComplete',
      'numCompleted',
      'fractionCompleted',
      'elapsedRealTime',
      'timeToCompletion',
      'isRunning',
      'wasStopped',
      'isComplete',
      'wasDeleted',
      'status',
      'requestedStatus',
      'startedDate',
      'lastStatusDate',
      'parentJob',
      'priority',
      'jobparameters_set',
      )
    read_only_fields = ('id', 'user', )

class JobsViewSet(viewsets.ModelViewSet):
  queryset = Jobs.objects.all()
  serializer_class = JobsSerializer
  permission_classes = (IsAuthenticatedOrOptionsOnly, JobsDetailPerms,)

  def list(self, request, format=None):
    self.permission_classes = (JobsListPerms,)
    self.check_permissions(request)
    self.queryset = getUserJobs(request.user)
    return super(JobsViewSet, self).list(request, format)

  def create(self, request, format=None):
    self.permission_classes = (JobsListPerms,)
    self.check_permissions(request)
    return super(JobsViewSet, self).create(request, format)

  def retrieve(self, request, pk=None, format=None):
    obj = get_object_or_404(self.queryset, pk=pk)
    self.check_object_permissions(request, obj)
    serializer = JobsSerializer(obj)
    return Response(serializer.data)

  # pre_save moved to perform_create/perform_update in Django Rest Framework 3
  #def pre_save(self, obj):
  #  if self.request.method == 'POST':
  #    obj.user = self.request.user

  def perform_create(self, serializer):
    serializer.is_valid(raise_exception=True)
    project = serializer.validated_data['project']
    if not project.userHasPermissions(self.request.user, ARLO_PERMS.WRITE):
      raise exceptions.PermissionDenied()
    # Include the user attribute directly, rather than from request data.
    instance = serializer.save(user=self.request.user)

  def perform_update(self, serializer):
    serializer.is_valid(raise_exception=True)
    if not serializer.instance:
      raise Http404("Instance Not Found")
    self.check_object_permissions(self.request, serializer.instance)
    serializer.save()

