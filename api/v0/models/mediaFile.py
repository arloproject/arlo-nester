from rest_framework import viewsets, permissions, status
from rest_framework.response import Response
from rest_framework.decorators import detail_route
from django.shortcuts import get_object_or_404

from api.v0.IsAuthenticatedOrOptionsOnly import IsAuthenticatedOrOptionsOnly
from tools.models import MediaFile
from tools.views.userPermissions import ARLO_PERMS, getUserMediaFiles
from tools.views.audio import deleteAudioFile
from tools.views.userMetaData import getMediaFileUserMetaData
from .serializers import MediaFileSerializer, ProjectSerializer
from .mediaFileUserMetaData import MediaFileUserMetaDataListSerializer


class MediaFileListPerms(permissions.BasePermission):
  def has_permission(self, request, view):
    if not request.user.is_authenticated():
      return False
    if request.method in permissions.SAFE_METHODS:
      return True
    return False

class MediaFileDetailPerms(permissions.BasePermission):
  def has_object_permission(self, request, view, obj):
    if not request.user.is_authenticated():
      return False
    if request.method in permissions.SAFE_METHODS:
      return obj.userHasPermissions(request.user, ARLO_PERMS.READ)
    return obj.userHasPermissions(request.user, ARLO_PERMS.WRITE)


class MediaFileViewSet(viewsets.ModelViewSet):
  """
  MediaFile Object API Endpoint
  """
  queryset = MediaFile.objects.all().prefetch_related('mediafilemetadata_set')
  serializer_class = MediaFileSerializer
  permission_classes = (IsAuthenticatedOrOptionsOnly, MediaFileDetailPerms,)

  def list(self, request, format=None):
    self.queryset = getUserMediaFiles(request.user).prefetch_related('mediafilemetadata_set')
    self.permission_classes = (MediaFileListPerms,)
    self.check_permissions(request)
    return super(MediaFileViewSet, self).list(request, format)

  def create(self, request, format=None):
    self.permission_classes = (MediaFileListPerms,)
    self.check_permissions(request)
    return super(MediaFileViewSet, self).create(request, format)

  def retrieve(self, request, pk=None, format=None):
    mediaFile = get_object_or_404(self.queryset, pk=pk)
    self.check_object_permissions(request, mediaFile)
    serializer = MediaFileSerializer(mediaFile)
    return Response(serializer.data)

  def destroy(self, request, pk=None, format=None):
    # use our custom function so that we delete the file on disk as well
    deleteAudioFile(pk)
    return Response(None, status=status.HTTP_204_NO_CONTENT)

  @detail_route(methods=['get'], serializer_class=ProjectSerializer)
  def projects(self, request, pk=None):
      """
      Returns a list of Project objects that contain the MediaFile
      """
      mediaFile = get_object_or_404(self.queryset, pk=pk)
      self.check_object_permissions(request, mediaFile)

      projects = mediaFile.getContainingProjects()

      page = self.paginate_queryset(projects)
      if page is not None:
          serializer = self.get_serializer(page, many=True)
          return self.get_paginated_response(serializer.data)

      serializer = self.get_serializer(projects, many=True)
      return Response(serializer.data)

  @detail_route(methods=['get'], serializer_class=MediaFileUserMetaDataListSerializer)
  def userMetaData(self, request, pk=None):
    """Get a list of UserMetaData assigned to a MediaFile"""

    user = request.user

    # Get MediaFile Object
    mediaFile = None
    try:
      mediaFile = MediaFile.objects.get(id=pk)
    except:
      return Response("Not Found", status=status.HTTP_404_NOT_FOUND)

    # Check Permissions
    if not mediaFile.userHasPermissions(user, ARLO_PERMS.READ):
      return Response("Unauthorized Project", status=status.HTTP_401_UNAUTHORIZED)

    # Get UserMetaData Entries
    metadata = getMediaFileUserMetaData(mediaFile)
    page = self.paginate_queryset(metadata)
    serializer = self.get_serializer(page, many=True)
    return self.get_paginated_response(serializer.data)
