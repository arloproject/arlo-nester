from rest_framework import status
from rest_framework.decorators import api_view
from rest_framework.response import Response
from rest_framework.settings import api_settings

from tools.views.userPermissions import ARLO_PERMS
from tools.models import Library

from .mediaFile import MediaFileSerializer


### Get a list of MediaFiles assigned to a Library.
#
# @param request REST Framework Request Object
# @param libraryId Database ID of a Library.
# @param format REST Framework format param for serializaion options.
# @return REST Framework Response Object, which returns a JSON list

@api_view(['GET'])
def library_mediaFiles(request, libraryId, format=None):
  """Get a list of MediaFiles assigned to a Library"""

  user = request.user

  ###
  # Get Library Object

  library = None
  try:
    library = Library.objects.get(id=libraryId)
  except:
    return Response("Not Found", status=status.HTTP_404_NOT_FOUND)

  ###
  # Check Permissions

  if not library.userHasPermissions(user, ARLO_PERMS.READ):
    return Response("Unauthorized Library", status=status.HTTP_401_UNAUTHORIZED)

  ###
  # Get MediaFiles

#   mediaFiles = MediaFile.objects.filter(library=library).prefetch_related('mediafilemetadata_set')
  mediaFiles = library.mediafile_set.all().prefetch_related('mediafilemetadata_set')


  ###
  # Apply Pagination

  pagination_class = api_settings.DEFAULT_PAGINATION_CLASS
  paginator = pagination_class()
  if paginator is None:
    mediaFiles = None
  else:
    mediaFiles = paginator.paginate_queryset(mediaFiles, request)

  ###
  # Serializer / Response

  serializer = MediaFileSerializer(mediaFiles, many=True)
  return paginator.get_paginated_response(serializer.data)
