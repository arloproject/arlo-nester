from rest_framework import serializers, viewsets, permissions, filters
from rest_framework.response import Response
from rest_framework.decorators import detail_route
from rest_framework import status
from django.shortcuts import get_object_or_404
from django.contrib.auth.models import User

from tools.models import ArloUserGroup
from tools.views.userPermissions import ARLO_PERMS
from api.v0.IsAuthenticatedOrOptionsOnly import IsAuthenticatedOrOptionsOnly
from .user import UserSerializer
from ..IdSerializer import IdSerializer
from ..NameSerializer import NameSerializer


class ArloUserGroupListPerms(permissions.BasePermission):
  def has_permission(self, request, view):
    if not request.user.is_authenticated():
      return False
    return True


class ArloUserGroupDetailPerms(permissions.BasePermission):
  def has_object_permission(self, request, view, obj):
    if not request.user.is_authenticated():
      return False
    if request.method in permissions.SAFE_METHODS:
      return obj.userHasPermissions(request.user, ARLO_PERMS.READ)
    else:
      return obj.userHasPermissions(request.user, ARLO_PERMS.WRITE)


# used for List/Create
class ArloUserGroupSerializer(serializers.ModelSerializer):
  class Meta:
    model = ArloUserGroup
    fields = (
      'id',
      'name',
      'users',
      'creationDate',
      'createdBy',
      )
    read_only_fields = ('id', 'creationDate', 'createdBy')


class ArloUserGroupViewSet(viewsets.ModelViewSet):
  """
  Manage ArloUserGroups
  """
  queryset = ArloUserGroup.objects.all()
  serializer_class = ArloUserGroupSerializer
  permission_classes = (IsAuthenticatedOrOptionsOnly, ArloUserGroupDetailPerms,)
  filter_backends = (filters.DjangoFilterBackend,)
  filter_fields = ('name',)

  def list(self, request, format=None):
    self.queryset = ArloUserGroup.objects.getUserArloUserGroups(request.user)
    self.permission_classes = (ArloUserGroupListPerms,)
    self.check_permissions(request)
    return super(ArloUserGroupViewSet, self).list(request, format)

  def create(self, request, format=None):
    self.permission_classes = (ArloUserGroupListPerms,)
    self.check_permissions(request)
    return super(ArloUserGroupViewSet, self).create(request, format)
    # TODO maybe need to add the API user as a default Admin

  def perform_create(self, serializer):
    # Include the user attribute directly, rather than from request data.
    serializer.save(createdBy=self.request.user)


  @detail_route(methods=['get'], serializer_class=UserSerializer)
  def list_users(self, request, pk=None):
      """
      Returns a list of User objects that are assigned to the specified group.
      """
      arloUserGroup = get_object_or_404(self.queryset, pk=pk)
      self.check_object_permissions(request, arloUserGroup)

      users = arloUserGroup.users.all()

      page = self.paginate_queryset(users)
      if page is not None:
          serializer = self.get_serializer(page, many=True)
          return self.get_paginated_response(serializer.data)

      serializer = self.get_serializer(users, many=True)
      return Response(serializer.data)


  @detail_route(methods=['post'], serializer_class=NameSerializer)
  def add_user(self, request, pk=None):
      arloUserGroup = get_object_or_404(self.queryset, pk=pk)
      self.check_object_permissions(request, arloUserGroup)

      serializer = self.get_serializer(data=request.data)
      serializer.is_valid(raise_exception=True)

      # New User to Add
      user = get_object_or_404(User.objects.all(), username=serializer.data['name'])

      # Is the User already in the group?
      if user in arloUserGroup.users.all():
          return Response("User Already in Group", status=status.HTTP_200_OK)

      arloUserGroup.users.add(user)

      return Response(serializer.data, status=status.HTTP_201_CREATED)


  @detail_route(methods=['post', 'delete'], serializer_class=IdSerializer)
  def remove_user(self, request, pk=None):
      arloUserGroup = get_object_or_404(self.queryset, pk=pk)
      self.check_object_permissions(request, arloUserGroup)

      serializer = self.get_serializer(data=request.data)
      serializer.is_valid(raise_exception=True)

      # New User to Add
      user = get_object_or_404(User.objects.all(), pk=serializer.data['id'])

      # Is the User already in the group?
      if not user in arloUserGroup.users.all():
          return Response("User Not in Group", status=status.HTTP_200_OK)

      arloUserGroup.users.remove(user)

      return Response(None, status=status.HTTP_204_NO_CONTENT)

