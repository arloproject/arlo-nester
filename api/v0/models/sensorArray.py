from rest_framework import serializers, viewsets, permissions
from rest_framework.response import Response
from django.shortcuts import get_object_or_404

from tools.models import SensorArray
from api.v0.IsAuthenticatedOrOptionsOnly import IsAuthenticatedOrOptionsOnly
from tools.views.userPermissions import ARLO_PERMS
from .sensor import SensorSerializer

class SensorArrayListPerms(permissions.BasePermission):
  def has_permission(self, request, view):
    if not request.user.is_authenticated():
      return False
    return True

class SensorArrayDetailPerms(permissions.BasePermission):
  def has_object_permission(self, request, view, obj):
    if not request.user.is_authenticated():
      return False
    if request.user.is_staff:
      return True
    if request.method in permissions.SAFE_METHODS:
      return obj.userHasPermissions(request.user, ARLO_PERMS.READ)
    else:
      return obj.userHasPermissions(request.user, ARLO_PERMS.WRITE)


class SensorArrayListSerializer(serializers.ModelSerializer):
  sensor_set = SensorSerializer(many=True, read_only=True)

  class Meta:
    model = SensorArray
    fields = (
        'id',
        'user',
        'name',
        'library',
        'layoutRows',
        'layoutColumns',
        'sensor_set',
        )

    read_only_fields = ('id', 'user')

class SensorArrayDetailSerializer(serializers.ModelSerializer):
  sensor_set = SensorSerializer(many=True, read_only=True)

  class Meta:
    model = SensorArray
    fields = (
        'id',
        'user',
        'name',
        'library',
        'layoutRows',
        'layoutColumns',
        'sensor_set',
        )

    read_only_fields = ('id', 'user', 'library')


class SensorArrayViewSet(viewsets.ModelViewSet):
  queryset = SensorArray.objects.all()
  serializer_class = SensorArrayDetailSerializer
  permission_classes = (IsAuthenticatedOrOptionsOnly, SensorArrayDetailPerms, )

  def list(self, request, format=None):
    self.serializer_class = SensorArrayListSerializer
    self.queryset = SensorArray.objects.getUserSensorArrays(request.user)
    self.permission_classes = (SensorArrayListPerms,)
    self.check_permissions(request)
    return super(SensorArrayViewSet, self).list(request, format)

  def create(self, request, format=None):
    self.serializer_class = SensorArrayListSerializer
    self.permission_classes = (SensorArrayListPerms,)
    self.check_permissions(request)
    return super(SensorArrayViewSet, self).create(request, format)

  def retrieve(self, request, pk=None, format=None):
    obj = get_object_or_404(self.queryset, pk=pk)
    self.check_object_permissions(request, obj)
    serializer = SensorArrayDetailSerializer(obj)
    return Response(serializer.data)

  def perform_create(self, serializer):
    # Include the user attribute directly, rather than from request data.
    instance = serializer.save(user=self.request.user)

