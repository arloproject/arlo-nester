from rest_framework import serializers, viewsets, permissions, filters
from django.contrib.auth.models import User


class UserPerms(permissions.BasePermission):
  def has_permission(self, request, view):
    if request.method == 'OPTIONS':
      return True
    if not request.user.is_authenticated():
      return False
    if not (request.method in permissions.SAFE_METHODS):
      return False
    return True

  def has_object_permission(self, request, view, obj):
    if not request.user.is_authenticated():
      return False
    if not (request.method in permissions.SAFE_METHODS):
      return False
    return True


#class UserSerializer(serializers.HyperlinkedModelSerializer):
class UserSerializer(serializers.ModelSerializer):
  class Meta:
    model = User
    fields = ('id', 'username')
    read_only_fields = fields


class UserViewSet(viewsets.ReadOnlyModelViewSet):
  permission_classes = (UserPerms,)
  queryset = User.objects.all()
  serializer_class = UserSerializer
  filter_backends = (filters.DjangoFilterBackend,)
  filter_fields = ('username',)
