from rest_framework import serializers, viewsets, permissions, exceptions
from rest_framework.response import Response
from django.shortcuts import get_object_or_404

from tools.models import RandomWindow
from api.v0.IsAuthenticatedOrOptionsOnly import IsAuthenticatedOrOptionsOnly
from tools.views.userPermissions import getUserRandomWindows, ARLO_PERMS


class RandomWindowListPerms(permissions.BasePermission):
  def has_permission(self, request, view):
    if not request.user.is_authenticated():
      return False
    return True

class RandomWindowDetailPerms(permissions.BasePermission):
  def has_object_permission(self, request, view, obj):
    if not request.user.is_authenticated():
      return False
    if request.user.is_staff:
      return True
    if request.method in permissions.SAFE_METHODS:
      return obj.userHasPermissions(request.user, ARLO_PERMS.READ)
    else:
      return obj.userHasPermissions(request.user, ARLO_PERMS.WRITE)

class RandomWindowSerializer(serializers.ModelSerializer):
  class Meta:
    model = RandomWindow
    fields = (
        'id',
        'randomWindowTaggingJob',
        'mediaFile',
        'startTime',
        'endTime',
        )

    read_only_fields = ('id', )

class RandomWindowViewSet(viewsets.ModelViewSet):
  queryset = RandomWindow.objects.all()
  serializer_class = RandomWindowSerializer
  permission_classes = (IsAuthenticatedOrOptionsOnly, RandomWindowDetailPerms, )

  def list(self, request, format=None):
    self.queryset = getUserRandomWindows(request.user)
    self.permission_classes = (RandomWindowListPerms,)
    self.check_permissions(request)
    return super(RandomWindowViewSet, self).list(request, format)

  def create(self, request, format=None):
    self.permission_classes = (RandomWindowListPerms,)
    self.check_permissions(request)
    return super(RandomWindowViewSet, self).create(request, format)

  def retrieve(self, request, pk=None, format=None):
    obj = get_object_or_404(self.queryset, pk=pk)
    self.check_object_permissions(request, obj)
    serializer = RandomWindowSerializer(obj)
    return Response(serializer.data)

  def perform_create(self, serializer):
    serializer.is_valid(raise_exception=True)
    # passed validation, so we're assuming that the Job exists:
    job = serializer.validated_data['randomWindowTaggingJob']
    if not job.userHasPermissions(self.request.user, ARLO_PERMS.WRITE):
      raise exceptions.PermissionDenied()
    # Include the user attribute directly, rather than from request data.
    serializer.save()
