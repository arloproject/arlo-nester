from rest_framework import serializers, viewsets, permissions, exceptions
from rest_framework.response import Response
from django.shortcuts import get_object_or_404
from django.http import Http404

from tools.models import MediaFileUserMetaDataField
from tools.views.userPermissions import ARLO_PERMS, getUserMediaFileUserMetaDataFields
from api.v0.IsAuthenticatedOrOptionsOnly import IsAuthenticatedOrOptionsOnly


###
# Permissions

class MediaFileUserMetaDataFieldListPerms(permissions.BasePermission):
  def has_permission(self, request, view):
    if not request.user.is_authenticated():
      return False
    return True

class MediaFileUserMetaDataFieldDetailPerms(permissions.BasePermission):
  def has_object_permission(self, request, view, obj):
    if not request.user.is_authenticated():
      return False
    if request.user.is_staff:
      return True
    if request.method in permissions.SAFE_METHODS:
      return obj.userHasPermissions(request.user, ARLO_PERMS.READ)
    else:
      return obj.userHasPermissions(request.user, ARLO_PERMS.WRITE)


###
# Serializer

# used for List/Create
class MediaFileUserMetaDataFieldListSerializer(serializers.ModelSerializer):
  class Meta:
    model = MediaFileUserMetaDataField
    fields = (
      'id',
      'name',
      'library',
      'description',
      )
    read_only_fields = (
      'id',
      )

# many extra read-only fields for update
class MediaFileUserMetaDataFieldDetailSerializer(serializers.ModelSerializer):
  class Meta:
    model = MediaFileUserMetaDataField
    fields = (
      'id',
      'name',
      'library',
      'description',
      )
    read_only_fields = (
      'id',
      )
    extra_kwargs = {'library': {'required': False, 'read_only': False}}

###
# ViewSet

class MediaFileUserMetaDataFieldViewSet(viewsets.ModelViewSet):
  queryset = MediaFileUserMetaDataField.objects.all()
  serializer_class = MediaFileUserMetaDataFieldDetailSerializer
  permission_classes = (IsAuthenticatedOrOptionsOnly, MediaFileUserMetaDataFieldDetailPerms,)

  def list(self, request, format=None):
    self.queryset = getUserMediaFileUserMetaDataFields(request.user)
    self.serializer_class = MediaFileUserMetaDataFieldListSerializer
    self.permission_classes = (MediaFileUserMetaDataFieldListPerms,)
    self.check_permissions(request)
    return super(MediaFileUserMetaDataFieldViewSet, self).list(request, format)

  def create(self, request, format=None):
    self.serializer_class = MediaFileUserMetaDataFieldListSerializer
    self.permission_classes = (MediaFileUserMetaDataFieldDetailPerms,)
    self.check_permissions(request)
    return super(MediaFileUserMetaDataFieldViewSet, self).create(request, format)

  def retrieve(self, request, pk=None, format=None):
    obj = get_object_or_404(self.queryset, pk=pk)
    self.check_object_permissions(request, obj)
    serializer = MediaFileUserMetaDataFieldDetailSerializer(obj)
    return Response(serializer.data)

  # pre_save moved to perform_create/perform_update in Django Rest Framework 3
  #def pre_save(self, obj):
#    if self.request.method == 'POST':
#      obj.user = self.request.user
  #  self.check_object_permissions(self.request, obj)

  def perform_create(self, serializer):
    serializer.is_valid(raise_exception=True)
    library = serializer.validated_data['library']
    if not library.userHasPermissions(self.request.user, ARLO_PERMS.WRITE):
      raise exceptions.PermissionDenied()
    serializer.save()

  def perform_update(self, serializer):
    serializer.is_valid(raise_exception=True)
    if not serializer.instance:
      raise Http404("Instance Not Found")
    self.check_object_permissions(self.request, serializer.instance)
    serializer.save()


