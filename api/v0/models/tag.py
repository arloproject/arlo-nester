from rest_framework import serializers, viewsets, permissions, exceptions
from django.http import Http404

from tools.models import Tag
from tools.views.userPermissions import ARLO_PERMS
from api.v0.IsAuthenticatedOrOptionsOnly import IsAuthenticatedOrOptionsOnly


###
# Permissions

class TagListPerms(permissions.BasePermission):
  def has_permission(self, request, view):
    if not request.user.is_authenticated():
      return False
    if request.user.is_staff:
      return True
    return False

class TagDetailPerms(permissions.BasePermission):
  def has_object_permission(self, request, view, obj):
    if not request.user.is_authenticated():
      return False
    if request.user.is_staff:
      return True
    if request.method in permissions.SAFE_METHODS:
      return obj.userHasPermissions(request.user, ARLO_PERMS.READ)
    else:
      # also ensure user has access to the mediaFile
      return obj.userHasPermissions(request.user, ARLO_PERMS.WRITE) and obj.mediaFile.userHasPermissions(request.user, ARLO_PERMS.READ)


###
# Serializer

# used for List/Create
class TagListSerializer(serializers.ModelSerializer):
  class Meta:
    model = Tag
    fields = (
      'id',
      'user',
      'creationDate',
      'tagSet',
      'mediaFile',
      'startTime',
      'endTime',
      'minFrequency',
      'maxFrequency',
      'tagClass',
      'parentTag',
      'randomlyChosen',
      'machineTagged',
      'userTagged',
      'strength',
      )
    read_only_fields = ('id', 'user', 'creationDate')

# many extra read-only fields for update
class TagDetailSerializer(serializers.ModelSerializer):
  class Meta:
    model = Tag
    fields = (
      'id',
      'user',
      'creationDate',
      'tagClass',
      'tagSet',
      'mediaFile',
      'startTime',
      'endTime',
      'minFrequency',
      'maxFrequency',
      'tagClass',
      'parentTag',
      'randomlyChosen',
      'machineTagged',
      'userTagged',
      'strength',
      )
    read_only_fields = ('id', 'user', 'creationDate', 'tagClass')


###
# ViewSet

class TagViewSet(viewsets.ModelViewSet):
  queryset = Tag.objects.all()
  serializer_class = TagDetailSerializer
  permission_classes = (IsAuthenticatedOrOptionsOnly, TagDetailPerms,)

  def list(self, request, format=None):
    self.serializer_class = TagListSerializer
    self.permission_classes = (TagListPerms,)
    self.check_permissions(request)
    return super(TagViewSet, self).list(request, format)

  def create(self, request, format=None):
    self.serializer_class = TagListSerializer
    self.permission_classes = (TagDetailPerms,)
    self.check_permissions(request)
    return super(TagViewSet, self).create(request, format)

#  def retrieve(self, request, pk=None, format=None):
#    obj = get_object_or_404(self.queryset, pk=pk)
#    self.check_object_permissions(request, obj)
#    serializer = TagSerializer(obj)
#    return Response(serializer.data)

  # pre_save moved to perform_create/perform_update in Django Rest Framework 3
  #def pre_save(self, obj):
  #  if self.request.method == 'POST':
  #    obj.user = self.request.user
  #  self.check_object_permissions(self.request, obj)

  def perform_create(self, serializer):
    serializer.is_valid(raise_exception=True)
    # passed validation, so we're assuming that the Job exists:
    tagSet = serializer.validated_data['tagSet']
    if not tagSet.userHasPermissions(self.request.user, ARLO_PERMS.WRITE):
      raise exceptions.PermissionDenied()
    # Include the user attribute directly, rather than from request data.
    serializer.save(user=self.request.user)

  def perform_update(self, serializer):
    serializer.is_valid(raise_exception=True)
    if not serializer.instance:
      raise Http404("Instance Not Found")
    self.check_object_permissions(self.request, serializer.instance)
    serializer.save()
