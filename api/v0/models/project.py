from rest_framework import viewsets, permissions, status
from rest_framework.response import Response
from rest_framework.decorators import detail_route
from django.shortcuts import get_object_or_404

from tools.models import Project, MediaFile
from tools.views.userPermissions import ARLO_PERMS
from api.v0.IsAuthenticatedOrOptionsOnly import IsAuthenticatedOrOptionsOnly
from .serializers import ProjectSerializer, MediaFileSerializer
from ..IdSerializer import IdSerializer

class ProjectListPerms(permissions.BasePermission):
  def has_permission(self, request, view):
    if not request.user.is_authenticated():
      return False
    return True

class ProjectDetailPerms(permissions.BasePermission):
  def has_object_permission(self, request, view, obj):
    if not request.user.is_authenticated():
      return False
    if request.user.is_staff:
      return True
    if request.method in permissions.SAFE_METHODS:
      return obj.userHasPermissions(request.user, ARLO_PERMS.READ)
    else:
      return obj.userHasPermissions(request.user, ARLO_PERMS.WRITE)


class ProjectViewSet(viewsets.ModelViewSet):
  queryset = Project.objects.all()
  serializer_class = ProjectSerializer
  permission_classes = (IsAuthenticatedOrOptionsOnly, ProjectDetailPerms,)

  def list(self, request, format=None):
    self.permission_classes = (ProjectListPerms,)
    self.check_permissions(request)
    self.queryset = Project.objects.getUserProjects(request.user)
    return super(ProjectViewSet, self).list(request, format)

  def create(self, request, format=None):
    self.permission_classes = (ProjectListPerms,)
    self.check_permissions(request)
    return super(ProjectViewSet, self).create(request, format)

  def retrieve(self, request, pk=None, format=None):
    project = get_object_or_404(self.queryset, pk=pk)
    self.check_object_permissions(request, project)
    serializer = ProjectSerializer(project)
    return Response(serializer.data)

  def perform_create(self, serializer):
    # Include the user attribute directly, rather than from request data.
    serializer.save(user=self.request.user)

  @detail_route(methods=['get'], serializer_class=MediaFileSerializer)
  def mediaFiles(self, request, pk=None):
      """
      Returns a list of MediaFile objects that are assigned to the specified group.
      """
      project = get_object_or_404(self.queryset, pk=pk)
      self.check_object_permissions(request, project)

      mediaFiles = project.getMediaFiles().prefetch_related('mediafilemetadata_set')

      page = self.paginate_queryset(mediaFiles)
      if page is not None:
          serializer = self.get_serializer(page, many=True)
          return self.get_paginated_response(serializer.data)

      serializer = self.get_serializer(mediaFiles, many=True)
      return Response(serializer.data)

  @detail_route(methods=['post'], url_path='mediaFiles/add', serializer_class=IdSerializer)
  def add_mediaFile(self, request, pk=None):
    """
    Add an existing MediaFile to a Project.
    """
    project = get_object_or_404(Project, pk=pk)
    self.check_object_permissions(request, project)

    serializer = self.get_serializer(data=request.data)
    serializer.is_valid(raise_exception=True)

    mediaFile = get_object_or_404(MediaFile, id=serializer.data['id'])

    if mediaFile in project.mediaFiles.all():
      return Response("MediaFile Already in Project", status=status.HTTP_200_OK)
    if not mediaFile.userHasPermissions(request.user, ARLO_PERMS.READ):
        return Response("Forbidden", status=status.HTTP_403_FORBIDDEN)

    project.mediaFiles.add(mediaFile)

    return Response(serializer.data, status=status.HTTP_201_CREATED)

  @detail_route(methods=['post'], url_path='mediaFiles/remove', serializer_class=IdSerializer)
  def remove_mediaFiles(self, request, pk=None):
    project = get_object_or_404(Project, pk=pk)
    self.check_object_permissions(request, project)

    serializer = self.get_serializer(data=request.data)
    serializer.is_valid(raise_exception=True)

    mediaFile = get_object_or_404(MediaFile, pk=serializer.data['id'])

    if not mediaFile in project.mediaFiles.all():
      return Response("MediaFile Not in Project", status=status.HTTP_200_OK)

    project.mediaFiles.remove(mediaFile)

    return Response(None, status=status.HTTP_204_NO_CONTENT)
