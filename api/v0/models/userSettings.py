from rest_framework import serializers, viewsets, permissions
from rest_framework.response import Response
from django.shortcuts import get_object_or_404

from tools.models import UserSettings
from api.v0.IsAuthenticatedOrOptionsOnly import IsAuthenticatedOrOptionsOnly


class UserSettingsListPerms(permissions.BasePermission):
  def has_permission(self, request, view):
    if not request.user.is_authenticated():
      return False
    return True

class UserSettingsDetailPerms(permissions.BasePermission):
  def has_object_permission(self, request, view, obj):
    if not request.user.is_authenticated():
      return False
    if request.user.is_staff:
      return True
    return request.user == obj.user

class UserSettingsSerializer(serializers.ModelSerializer):
  class Meta:
    model = UserSettings
    fields = (
        'id',
        'user',
        'name',
        'windowSizeInSeconds',
        'spectraMinimumBandFrequency',
        'spectraMaximumBandFrequency',
        'spectraDampingFactor',
        'spectraNumFrequencyBands',
        'spectraNumFramesPerSecond',
        'showSpectra',
        'showWaveform',
        'loadAudio',
        'maxNumAudioFilesToList',
        'fileViewWindowSizeInSeconds',
        'fileViewSpectraMinimumBandFrequency',
        'fileViewSpectraMaximumBandFrequency',
        'fileViewSpectraDampingFactor',
        'fileViewSpectraNumFrequencyBands',
        'fileViewSpectraNumFramesPerSecond',
        'catalogViewWindowSizeInSeconds',
        'catalogViewSpectraMinimumBandFrequency',
        'catalogViewSpectraMaximumBandFrequency',
        'catalogViewSpectraDampingFactor',
        'catalogViewSpectraNumFrequencyBands',
        'catalogViewSpectraNumFramesPerSecond',
        'tagViewWindowSizeInSeconds',
        'tagViewSpectraMinimumBandFrequency',
        'tagViewSpectraMaximumBandFrequency',
        'tagViewSpectraDampingFactor',
        'tagViewSpectraNumFrequencyBands',
        'tagViewSpectraNumFramesPerSecond',
        )

    read_only_fields = ('id', 'user', )
#    extra_kwargs = {'user': {'required': False}}

class UserSettingsViewSet(viewsets.ModelViewSet):
  queryset = UserSettings.objects.all()
  serializer_class = UserSettingsSerializer
  permission_classes = (IsAuthenticatedOrOptionsOnly, UserSettingsDetailPerms, )

  def list(self, request, format=None):
    self.queryset = UserSettings.objects.filter(user=request.user)
    self.permission_classes = (UserSettingsListPerms,)
    self.check_permissions(request)
    return super(UserSettingsViewSet, self).list(request, format)

  def create(self, request, format=None):
    self.permission_classes = (UserSettingsListPerms,)
    self.check_permissions(request)
    return super(UserSettingsViewSet, self).create(request, format)

  def retrieve(self, request, pk=None, format=None):
    userSettings = get_object_or_404(self.queryset, pk=pk)
    self.check_object_permissions(request, userSettings)
    serializer = UserSettingsSerializer(userSettings)
    return Response(serializer.data)

  # pre_save moved to perform_create in Django Rest Framework 3
  #def pre_save(self, obj):
  #  if self.request.method == 'POST':
  #    obj.user = self.request.user

  def perform_create(self, serializer):
    # Include the user attribute directly, rather than from request data.
    instance = serializer.save(user=self.request.user)


