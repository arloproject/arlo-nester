from rest_framework import serializers, viewsets, permissions
from django.contrib.contenttypes.models import ContentType
from django.db.models import Q

from tools.models import ArloPermission
from tools.views.userPermissions import ARLO_PERMS

from api.v0.IsAuthenticatedOrOptionsOnly import IsAuthenticatedOrOptionsOnly


class ContentTypeObjectRelatedField(serializers.RelatedField):
    """
    A custom field to use for representing ContentType objects as text.
    Initialize the valid Object classes via a ContentType QuerySet to the init.
    """

    _object_type_map = {} # {ModelClass: '<model name>',}

    def __init__(self, queryset, **kwargs):
        for object_type in queryset:
            self._object_type_map[object_type.model_class()] = object_type.model
        super(ContentTypeObjectRelatedField, self).__init__(queryset=queryset, **kwargs)


    def to_representation(self, value):
        """
        Serialize contenttype objects to a simple textual representation.
        """
        result = self._object_type_map.get(value.model_class())
        if result is None:
            raise Exception('Unexpected type of Target object')
        return result


    def to_internal_value(self, data):
        """
        De-Serialize into contenttype objects
        """
        for object_type, name in self._object_type_map.iteritems():
            if str(name).lower() == data:
                return ContentType.objects.get_for_model(object_type)
        raise Exception('Unexpected Target Type: ' + repr(data))


class ArloPermissionListPerms(permissions.BasePermission):
  def has_permission(self, request, view):
    if not request.user.is_authenticated():
      return False
    return True


class ArloPermissionDetailPerms(permissions.BasePermission):
  def has_object_permission(self, request, view, obj):
    if not request.user.is_authenticated():
      return False
    if request.method in permissions.SAFE_METHODS:
      return obj.userHasPermissions(request.user, ARLO_PERMS.READ)
    else:
      return obj.userHasPermissions(request.user, ARLO_PERMS.WRITE)


# used for List/Create
class ArloPermissionSerializer(serializers.ModelSerializer):
  bearer_type = ContentTypeObjectRelatedField(
                            queryset=ContentType.objects.filter(
                                Q(app_label='auth', model='user') | Q(app_label='tools', model='arlousergroup'))
                            )
  target_type = ContentTypeObjectRelatedField(queryset=ContentType.objects.filter(app_label='tools', model__in=['project','library','tagset']))

  class Meta:
    model = ArloPermission
    fields = (
      'id',
      'bearer_type',
      'bearer_id',
      'target_type',
      'target_id',
      'creationDate',
      'createdBy',
      'read',
      'write',
      'launch_job',
      'admin',
      )
    read_only_fields = ('id', 'creationDate', 'createdBy')


class ArloPermissionViewSet(viewsets.ModelViewSet):
  """
  Manage ArloPermissions
  """
  queryset = ArloPermission.objects.all()
  serializer_class = ArloPermissionSerializer
  permission_classes = (IsAuthenticatedOrOptionsOnly, ArloPermissionDetailPerms,)

  def list(self, request, format=None):
    self.queryset = ArloPermission.objects.getUserArloPermissions(request.user)
    self.permission_classes = (ArloPermissionListPerms,)
    self.check_permissions(request)
    return super(ArloPermissionViewSet, self).list(request, format)

  def create(self, request, format=None):
    self.permission_classes = (ArloPermissionListPerms,)
    self.check_permissions(request)
    return super(ArloPermissionViewSet, self).create(request, format)

  def perform_create(self, serializer):
    # Include the user attribute directly, rather than from request data.
    serializer.save(createdBy=self.request.user)

