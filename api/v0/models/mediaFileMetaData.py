from rest_framework import viewsets, permissions, exceptions
from rest_framework.response import Response
from django.shortcuts import get_object_or_404
from django.http import Http404

from tools.models import MediaFileMetaData
from tools.views.userPermissions import ARLO_PERMS, getUserMediaFileMetaData
from api.v0.IsAuthenticatedOrOptionsOnly import IsAuthenticatedOrOptionsOnly
from .serializers import MediaFileMetaDataSerializer

###
# Permissions

class MediaFileMetaDataListPerms(permissions.BasePermission):
  def has_permission(self, request, view):
    if not request.user.is_authenticated():
      return False
    return True

class MediaFileMetaDataDetailPerms(permissions.BasePermission):
  def has_object_permission(self, request, view, obj):
    if not request.user.is_authenticated():
      return False
    if request.user.is_staff:
      return True
    if request.method in permissions.SAFE_METHODS:
      return obj.userHasPermissions(request.user, ARLO_PERMS.READ)
    else:
      return obj.userHasPermissions(request.user, ARLO_PERMS.WRITE)


###
# ViewSet

class MediaFileMetaDataViewSet(viewsets.ModelViewSet):
  queryset = MediaFileMetaData.objects.all()
  serializer_class = MediaFileMetaDataSerializer
  permission_classes = (IsAuthenticatedOrOptionsOnly, MediaFileMetaDataDetailPerms,)

  def list(self, request, format=None):
    self.permission_classes = (MediaFileMetaDataListPerms,)
    self.check_permissions(request)
    self.queryset = getUserMediaFileMetaData(request.user)
    return super(MediaFileMetaDataViewSet, self).list(request, format)

  def create(self, request, format=None):
    self.permission_classes = (MediaFileMetaDataDetailPerms,)
    self.check_permissions(request)
    return super(MediaFileMetaDataViewSet, self).create(request, format)

  def retrieve(self, request, pk=None, format=None):
    obj = get_object_or_404(self.queryset, pk=pk)
    self.check_object_permissions(request, obj)
    serializer = MediaFileMetaDataSerializer(obj)
    return Response(serializer.data)

  # pre_save moved to perform_create/perform_update in Django Rest Framework 3
  #def pre_save(self, obj):
#    if self.request.method == 'POST':
#      obj.user = self.request.user
  #  self.check_object_permissions(self.request, obj)

  def perform_create(self, serializer):
    serializer.is_valid(raise_exception=True)
    mediaFile = serializer.validated_data['mediaFile']
    if not mediaFile.userHasPermissions(self.request.user, ARLO_PERMS.WRITE):
      raise exceptions.PermissionDenied()
    serializer.save()

  def perform_update(self, serializer):
    serializer.is_valid(raise_exception=True)
    if not serializer.instance:
      raise Http404("Instance Not Found")
    self.check_object_permissions(self.request, serializer.instance)
    serializer.save()
