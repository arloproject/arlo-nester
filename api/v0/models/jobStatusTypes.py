from rest_framework import serializers, viewsets

from tools.models import JobStatusTypes
from api.v0.IsAuthenticatedOrOptionsOnly import IsAuthenticatedOrOptionsOnly

class JobStatusTypesSerializer(serializers.ModelSerializer):
  class Meta:
    model = JobStatusTypes
    fields = '__all__'

class JobStatusTypesViewSet(viewsets.ReadOnlyModelViewSet):
  queryset = JobStatusTypes.objects.all()
  serializer_class = JobStatusTypesSerializer
  permission_classes = (IsAuthenticatedOrOptionsOnly, )

